object FormTxtObjProperty: TFormTxtObjProperty
  Left = 0
  Top = 0
  ActiveControl = Edit_name
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'mGameTextObjectProperties'
  ClientHeight = 452
  ClientWidth = 376
  Color = clBtnFace
  Constraints.MinHeight = 479
  Constraints.MinWidth = 384
  DefaultMonitor = dmMainForm
  ParentFont = True
  FormStyle = fsStayOnTop
  OnHide = FormHide
  OnShow = FormShow
  TextHeight = 15
  object StatusBar1: TStatusBar
    Left = 0
    Top = 433
    Width = 376
    Height = 19
    Panels = <
      item
        Style = psOwnerDraw
        Width = 24
      end>
    OnDrawPanel = StatusBar1DrawPanel
    ExplicitTop = 421
    ExplicitWidth = 368
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 376
    Height = 433
    ActivePage = TS_main
    Align = alClient
    TabOrder = 1
    ExplicitWidth = 368
    ExplicitHeight = 421
    object TS_main: TTabSheet
      Caption = 'mMainProperties'
      DesignSize = (
        368
        403)
      object Label5: TLabel
        Left = 3
        Top = 6
        Width = 43
        Height = 15
        Caption = 'mName'
      end
      object Edit_name: TEdit
        Left = 52
        Top = 3
        Width = 271
        Height = 23
        Anchors = [akLeft, akTop, akRight]
        CharCase = ecUpperCase
        MaxLength = 64
        TabOrder = 0
        OnEnter = Edit_nameEnter
        OnExit = Edit_nameExit
        OnKeyPress = Edit_nameKeyPress
        ExplicitWidth = 263
      end
      object GroupBox_pos: TGroupBox
        Left = 3
        Top = 30
        Width = 362
        Height = 370
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 2
        DesignSize = (
          362
          370)
        object Label2: TLabel
          Left = 47
          Top = 41
          Width = 10
          Height = 15
          Caption = 'X:'
        end
        object Label3: TLabel
          Left = 120
          Top = 41
          Width = 10
          Height = 15
          Caption = 'Y:'
        end
        object Label6: TLabel
          Left = 7
          Top = 14
          Width = 38
          Height = 15
          Caption = 'mZone'
        end
        object CSEd_x: TEdit
          Left = 61
          Top = 38
          Width = 51
          Height = 23
          NumbersOnly = True
          TabOrder = 1
          OnEnter = CSEd_xEnter
          OnExit = CSEd_xExit
          OnKeyPress = CSEd_xKeyPress
        end
        object CSEd_y: TEdit
          Left = 135
          Top = 38
          Width = 51
          Height = 23
          NumbersOnly = True
          TabOrder = 2
          OnEnter = CSEd_xEnter
          OnExit = CSEd_xExit
          OnKeyPress = CSEd_xKeyPress
        end
        object MemoStaticText: TMemo
          Left = 7
          Top = 298
          Width = 348
          Height = 66
          Anchors = [akLeft, akTop, akRight, akBottom]
          ReadOnly = True
          TabOrder = 4
          ExplicitWidth = 324
          ExplicitHeight = 54
        end
        object ComboBox_zone: TComboBox
          Left = 61
          Top = 11
          Width = 294
          Height = 23
          Style = csDropDownList
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnChange = ComboBox_zoneChange
          OnExit = ComboBox_zoneExit
        end
        object GroupBox1: TGroupBox
          Left = 7
          Top = 61
          Width = 348
          Height = 231
          Anchors = [akLeft, akTop, akRight]
          Caption = 'mProperties'
          TabOrder = 3
          ExplicitWidth = 324
          DesignSize = (
            348
            231)
          object Label8: TLabel
            Left = 9
            Top = 125
            Width = 74
            Height = 15
            Caption = 'mAlignmentV'
          end
          object Label9: TLabel
            Left = 9
            Top = 98
            Width = 76
            Height = 15
            Caption = 'mAlignmentH'
          end
          object Label10: TLabel
            Left = 9
            Top = 71
            Width = 35
            Height = 15
            Caption = 'mFont'
          end
          object Label1: TLabel
            Left = 9
            Top = 179
            Width = 43
            Height = 15
            Caption = 'mWidth'
          end
          object Label4: TLabel
            Left = 9
            Top = 206
            Width = 47
            Height = 15
            Caption = 'mHeight'
          end
          object Bevel4: TBevel
            Left = 315
            Top = 15
            Width = 27
            Height = 22
            Anchors = [akTop, akRight]
            ExplicitLeft = 314
          end
          object Bevel1: TBevel
            Left = 315
            Top = 148
            Width = 27
            Height = 22
            Anchors = [akTop, akRight]
            ExplicitLeft = 314
          end
          object ShapeColor: TShape
            Left = 136
            Top = 148
            Width = 180
            Height = 22
            Anchors = [akLeft, akTop, akRight]
            ExplicitWidth = 179
          end
          object Label7: TLabel
            Left = 137
            Top = 45
            Width = 80
            Height = 15
            Caption = 'mStringsCount'
          end
          object Label11: TLabel
            Left = 221
            Top = 45
            Width = 89
            Height = 15
            Caption = 'mCharsPerString'
          end
          object CB_Font: TComboBox
            Left = 136
            Top = 68
            Width = 206
            Height = 23
            Style = csDropDownList
            Anchors = [akLeft, akTop, akRight]
            MaxLength = 2
            TabOrder = 6
            OnChange = CB_FontChange
            OnExit = CB_FontExit
            ExplicitWidth = 182
          end
          object ComboBoxH: TComboBox
            Left = 136
            Top = 95
            Width = 206
            Height = 23
            Style = csDropDownList
            Anchors = [akLeft, akTop, akRight]
            MaxLength = 2
            TabOrder = 7
            OnChange = ComboBoxHChange
            OnExit = ComboBoxHExit
            Items.Strings = (
              'mAlignLeft'
              'mAlignRight'
              'mAlignHCenter')
            ExplicitWidth = 182
          end
          object ComboBoxV: TComboBox
            Left = 136
            Top = 122
            Width = 206
            Height = 23
            Style = csDropDownList
            Anchors = [akLeft, akTop, akRight]
            MaxLength = 2
            TabOrder = 8
            OnChange = ComboBoxHChange
            OnExit = ComboBoxHExit
            Items.Strings = (
              'mAlignTop'
              'mAlignBottom'
              'mAlignVCenter')
            ExplicitWidth = 182
          end
          object CheckBoxColor: TCheckBox
            Left = 9
            Top = 150
            Width = 97
            Height = 17
            Caption = 'mBackdrop'
            TabOrder = 9
            OnClick = CheckBoxColorClick
          end
          object CSEd_w: TEdit
            Left = 136
            Top = 176
            Width = 51
            Height = 23
            NumbersOnly = True
            TabOrder = 11
            OnEnter = CSEd_wEnter
            OnExit = CSEd_wExit
            OnKeyPress = CSEd_wKeyPress
          end
          object CSEd_h: TEdit
            Left = 136
            Top = 203
            Width = 51
            Height = 23
            NumbersOnly = True
            TabOrder = 12
            OnEnter = CSEd_wEnter
            OnExit = CSEd_wExit
            OnKeyPress = CSEd_wKeyPress
          end
          object RBStaticMode: TRadioButton
            Left = 5
            Top = 18
            Width = 123
            Height = 17
            Caption = 'mStaticText'
            Checked = True
            TabOrder = 0
            TabStop = True
            OnClick = RBStaticModeClick
          end
          object RBDynamicMode: TRadioButton
            Left = 5
            Top = 43
            Width = 132
            Height = 17
            Caption = 'mDynamicText'
            TabOrder = 3
            TabStop = True
            OnClick = RBStaticModeClick
          end
          object Bt_3dot: TButton
            Left = 316
            Top = 16
            Width = 24
            Height = 20
            Anchors = [akTop, akRight]
            Caption = '...'
            TabOrder = 2
            OnClick = Bt_3dotClick
            ExplicitLeft = 292
          end
          object Bt_3dotColor: TButton
            Left = 316
            Top = 149
            Width = 24
            Height = 20
            Anchors = [akTop, akRight]
            Caption = '...'
            TabOrder = 10
            OnClick = Bt_3dotColorClick
            ExplicitLeft = 292
          end
          object EditTextId: TEdit
            Left = 128
            Top = 15
            Width = 188
            Height = 23
            Anchors = [akLeft, akTop, akRight]
            CharCase = ecUpperCase
            TabOrder = 1
            OnEnter = EditTextIdEnter
            OnExit = ComboBox_TextIDExit
            OnKeyPress = EditTextIdKeyPress
            ExplicitWidth = 164
          end
          object EditDynamicTextStrings: TEdit
            Left = 180
            Top = 41
            Width = 35
            Height = 23
            MaxLength = 3
            NumbersOnly = True
            TabOrder = 4
            OnExit = EditDynamicTextStringsExit
          end
          object EditDynamicTextMaxChars: TEdit
            Left = 304
            Top = 41
            Width = 35
            Height = 23
            MaxLength = 3
            NumbersOnly = True
            TabOrder = 5
            OnExit = EditDynamicTextStringsExit
          end
        end
      end
      object BitBtn_find_obj: TBitBtn
        Left = 323
        Top = 3
        Width = 24
        Height = 23
        Hint = 'mFindOnMap'
        Anchors = [akTop, akRight]
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000FC02FC004985
          D40084C6F400C7D1D100FAEFC700B08D8800B5B8BC00FCFCDA0056595B005CB6
          F10072758400868F9200F5D7B000B6C8CE00D3B39E008E787C000BAA60000000
          0000BA58A6000000000091A58A6000000000D91AF8A6000000000291A58BDDDD
          000000291AF8AA88ABD0000291BF5ECEA8FD0000265C47774F8B000006C44777
          748A00000E4C7777775800000E44477777EA00000C44477777EA00000E44C477
          745B000003C7744C4CFD000000E47444C5D000000006EEEEE300}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BitBtn_find_objClick
        ExplicitLeft = 315
      end
    end
    object TS_Memo: TTabSheet
      Caption = 'mMemo'
      ImageIndex = 2
      DesignSize = (
        368
        403)
      object Memo: TMemo
        Left = 3
        Top = 3
        Width = 362
        Height = 397
        Anchors = [akLeft, akTop, akRight, akBottom]
        MaxLength = 200
        ScrollBars = ssVertical
        TabOrder = 0
        WordWrap = False
      end
    end
  end
  object ColorDialog: TColorDialog
    Left = 32
    Top = 352
  end
end
