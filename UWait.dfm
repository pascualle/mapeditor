object FWait: TFWait
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsNone
  ClientHeight = 83
  ClientWidth = 289
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDefault
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 289
    Height = 83
    Align = alClient
    BevelKind = bkFlat
    BevelOuter = bvNone
    ShowCaption = False
    TabOrder = 0
    object ProgressBar: TProgressBar
      Left = 48
      Top = 32
      Width = 193
      Height = 17
      Position = 60
      Style = pbstMarquee
      SmoothReverse = True
      Step = 5
      TabOrder = 0
    end
    object StaticText: TStaticText
      Left = 48
      Top = 14
      Width = 4
      Height = 4
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 1
    end
  end
  object Timer1: TTimer
    Interval = 300
    OnTimer = Timer1Timer
    Left = 248
    Top = 8
  end
end
