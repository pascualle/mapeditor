//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UBGOType.h"
#include "UStateList.h"
#include "MainUnit.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TFEditBGOType *FEditBGOType;

//---------------------------------------------------------------------------

__fastcall TFEditBGOType::TFEditBGOType(TComponent* Owner)
	: TForm(Owner)
{
	CSpinEditType = new TSpinEdit(FEditBGOType);
	CSpinEditType->Parent = FEditBGOType;
	CSpinEditType->Left = 18;
	CSpinEditType->Top = 74;
	CSpinEditType->Width = 49;
	CSpinEditType->Height = 22;
	CSpinEditType->MaxValue = 62;
	CSpinEditType->MinValue = 1;
	CSpinEditType->TabOrder = 2;
	CSpinEditType->Value = 1;
	CSpinEditType->Visible = true;

	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFEditBGOType::SetBackProperty(TBGobjProperties *prp)
{
  if(prp == NULL)return;
  if(prp->type == NONE_BGTYPE)
  {
   CheckBoxSolid->Checked = true;
   CheckBoxDirector->Checked = false;
   CSpinEditType->Value = 1;
  }
  else
  {
   CheckBoxSolid->Checked = prp->type >= 0;
   CheckBoxDirector->Checked = abs(prp->type) > DIRECTOR_TYPE;
   CSpinEditType->Value = abs(prp->type) - (CheckBoxDirector->Checked ? DIRECTOR_TYPE : 0);
  }
  Edit_name->Text = prp->name;

}
//---------------------------------------------------------------------------

void __fastcall TFEditBGOType::GetBackProperty(TBGobjProperties *prp)
{
  if(prp == NULL)return;
  prp->type = (char)(CSpinEditType->Value * ((CheckBoxSolid->Checked)?1:-1));
  prp->name = Edit_name->Text;
  if(CheckBoxDirector->Checked)
  {
        char sign = (prp->type >= 0 ?(char) 1 :(char) -1);
        prp->type = (char)((abs(prp->type) + DIRECTOR_TYPE)*sign);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFEditBGOType::Edit_nameChange(TObject */*Sender*/)
{
  Edit_nameChangeMain(Edit_name);
}
//---------------------------------------------------------------------------

void __fastcall TFEditBGOType::FormDestroy(TObject *)
{
 delete CSpinEditType;
}
//---------------------------------------------------------------------------

