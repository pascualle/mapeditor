//---------------------------------------------------------------------------

#ifndef UnitScaleH
#define UnitScaleH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFormScale : public TForm
{
__published:	// IDE-managed Components
        TButton *ButtonCancel;
        TButton *ButtonOk;
        TComboBox *ComboBox;
        TLabel *Label1;
        void __fastcall FormKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
        __fastcall TFormScale(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormScale *FormScale;
//---------------------------------------------------------------------------
#endif
