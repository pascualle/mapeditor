object Form_about: TForm_about
  Left = 393
  Top = 260
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  ClientHeight = 328
  ClientWidth = 621
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Pitch = fpFixed
  Font.Style = []
  KeyPreview = True
  Position = poMainFormCenter
  OnKeyPress = FormKeyPress
  TextHeight = 13
  object Button1: TButton
    Left = 542
    Top = 295
    Width = 75
    Height = 25
    Caption = 'mExit'
    ModalResult = 1
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 3
    Top = 5
    Width = 614
    Height = 284
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Color = clWhite
    Ctl3D = False
    ParentBackground = False
    ParentCtl3D = False
    TabOrder = 1
    object Label2: TLabel
      Left = 477
      Top = 265
      Width = 131
      Height = 13
      Caption = 'Papa Pascualle, 2004-2024'
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Pitch = fpFixed
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object Label1: TLabel
      Left = 390
      Top = 12
      Width = 210
      Height = 40
      Caption = 'Map editor'
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -35
      Font.Name = 'Courier New'
      Font.Pitch = fpFixed
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object Label12: TLabel
      Left = 551
      Top = 40
      Width = 48
      Height = 13
      Caption = 'ver 3.21'
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Pitch = fpFixed
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
  end
end
