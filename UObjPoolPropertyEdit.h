//---------------------------------------------------------------------------

#ifndef UObjPoolPropertyEditH
#define UObjPoolPropertyEditH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFOPPEdit : public TForm
{
__published:
	TButton *ButtonOk;
	TLabel *Label_name;
	TEdit *Edit_count;
	TLabel *Label_count;
	TComboBox *Ed_name;
	void __fastcall ButtonOkClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:
public:

	void __fastcall SetPoolName(UnicodeString txt);
	UnicodeString __fastcall GetPoolName() const;

	void __fastcall SetPoolCount(unsigned __int32 count);
	unsigned __int32 __fastcall GetPoolCount() const;

	__fastcall TFOPPEdit(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFOPPEdit *FOPPEdit;
//---------------------------------------------------------------------------
#endif
