//---------------------------------------------------------------------------

#ifndef UObjCodeH
#define UObjCodeH

//---------------------------------------------------------------------------
#include <stdio.h>
#include <map>
#include <list>
#include "UTypes.h"
#include "ExtCtrls.hpp"
#include "CategoryButtons.hpp"
#include "UTransformProperties.h"
#include "texts.h"

//---------------------------------------------------------------------------

class GPerson;
class TMapGPerson;
class TMapDirector;
class TFGOProperty;
class TMapCollideLine;
class TObjManager;
class TCommonTextManager;
class TCommonResObjManager;
class TSaveLoadStreamData;
class TSaveLoadStreamHandle;

//---------------------------------------------------------------------------
//������ PNG ������
//file_name- (in) ��� �����
//bmp_out-   (out) ��������� �� ��������������� �� png-������� ����������� (������������ 32bpp ����������� � �����-������� ������ ������������)
//transtarentDefColor (in) ���� ������� ������ ����������� ����� (���� ���� ������������) � fillTranstarentDefColor = true
bool read_png(const _TCHAR *file_name, Graphics::TBitmap* bmp_out);

bool validateSoundResource(UnicodeString path, unsigned __int32 *size);
bool validateFontResource(UnicodeString path);

UnicodeString forceCorrectResFileExtention(UnicodeString file);
UnicodeString forceCorrectSoundFileExtention(UnicodeString file);
UnicodeString forceCorrectFontFileExtention(UnicodeString file);

void __fastcall BmpHFlip(Graphics::TBitmap *bmp);
void __fastcall BmpVFlip(Graphics::TBitmap *bmp);

//---------------------------------------------------------------------------
inline  __int32     rgbTo__int32 ( __int32 red, __int32 green, __int32 blue )
{
	return blue | (green << 8) | (red << 16);
}
//---------------------------------------------------------------------------
inline  __int32     rgbaTo__int32 ( __int32 red, __int32 green, __int32 blue, __int32 alpha )
{
	return blue | (green << 8) | (red << 16) | (alpha << 24);
}
//---------------------------------------------------------------------------
// ������� ����������� ���� ������ �����
void __fastcall onChangeSpinEdit(TEdit *se, bool minusApply);
// ������� ����������� ���� ���� (������ ������� �����, ������������ ����� � �.�.)
void __fastcall Edit_nameChangeMain(TEdit* Edit_name);
//---------------------------------------------------------------------------

void __fastcall CBCategoryCollapase(TObject *Sender, const TButtonCategory *Category);
void __fastcall CBMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, __int32 X, __int32 Y);
void __fastcall CBDrawIcon(TObject *Sender, const TButtonItem *Button, TCanvas *Canvas, TRect &Rect,
                           TButtonDrawState State, __int32 &TextOffset);

bool nextPointAtLine(double x0, double y0, double tx, double ty, double speed, double* pos);

//---------------------------------------------------------------------------

class TGameObjectEditorData
{
 public:
	__fastcall TGameObjectEditorData() : locked(false), customvisible(true) {}

	bool locked;
	bool customvisible;
};

//---------------------------------------------------------------------------

void LocalizeGlobalConsts();

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

class TMyPanel : public TPanel
{
	public:
	virtual __fastcall TMyPanel(HWND ParentWindow) : TPanel(ParentWindow)
	{
	}

	virtual __fastcall ~TMyPanel()
	{
	}

	Graphics::TCanvas *__fastcall GetCanvas()
	{
		return Canvas;
	}
};

//---------------------------------------------------------------------------

//������� ��������� ��� ������� ������ TEDitObj
struct TEDitObjSelectionParams
{
	UnicodeString fileName;
	TRect selectionRect;
};

//---------------------------------------------------------------------------

//��������� ���� �������� �������
struct TEGrWndParams
{
	int	Left, Top, Width, Height;
	TRect LastSelection;
	__int32 scale;
	TColor TrColor;
	_TCHAR LastFileName[FILENAME_MAX];
	__int32 splitterLV;
};
//---------------------------------------------------------------------------

//��������� ���� TFObjParent
struct TEdPrObjWndParams
{
    bool init;
	TColor BgColor;
	__int32	Left;
	__int32	Top;
	__int32	Width;
	__int32	Height;
	__int32 scale;
	__int32 splitterLV;
	__int32 joinNodesCount;
};
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

class TListObject
{
 public:
	__fastcall TListObject() : mType(LISTOBJ_Undef){};
	virtual __fastcall ~TListObject(){};
	const TListObjectType& GetType() const {return mType;};
 protected:
	TListObjectType mType;
};

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//��������� �������� ����
//�������� ��� director (������ ��������� �������) (������� -- type>(DIRECTOR_TYPE)62 )
class TBGobjProperties : public TListObject
{
 public:
 
 UnicodeString	name;   //���������� ���
 char           type;   //��� (-/+)1..62, ���� ��������� solid/transparent

 __fastcall TBGobjProperties();
 __fastcall TBGobjProperties(const TBGobjProperties& obj);
 virtual __fastcall ~TBGobjProperties(){};
 void __fastcall Clear();
 void __fastcall CopyTo(TBGobjProperties &prp) const;
 bool __fastcall SaveToFile(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h);
 bool __fastcall LoadFromFile(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, int ver);
};


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


//��������� ��������� ������� (state)
class TState : public TListObject
{
 public:
 UnicodeString	name;           //���������� ���
 UnicodeString	animation;      //��� �������� ��� ���� ��������� (empty -- ���)
 Movement::Direction direction; //����������� �������� ��� ���� ���������

 __fastcall TState();
 __fastcall TState(const TState& obj);
 virtual __fastcall ~TState(){};
 void __fastcall Clear();
 void __fastcall CopyTo(TState *state) const;
 bool __fastcall SaveToFile(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h);
 bool __fastcall LoadFromFile(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, int ver);
};


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

struct BMPImage
{
 public:

	__fastcall BMPImage(Graphics::TBitmap* ipSrc);
	__fastcall BMPImage();
	__fastcall virtual ~BMPImage();
    void __fastcall Assign(const Graphics::TBitmap* ipSrc);
	void __fastcall Assign(const BMPImage *ipSrc);

	__property __int32 Width = {read = GetWidth};
	__property __int32 Height = {read = GetHeight};
	__property __int32 mWidth = {read = GetWidth};
	__property __int32 mHeight = {read = GetHeight};
	__property __int32 mWidth2n = {read = GetWidth2N};
	__property __int32 mHeight2n = {read = GetHeight2N};
	__property TCanvas *Canvas = {read = GetCanvas};

	void __fastcall SetSize(__int32 iWidth, __int32 iHeight);
	void __fastcall ResetAlpha();
	void __fastcall FillColorWithTransparent(TColor color);
	void __fastcall FillTransparentWithColor(TColor color);
	void __fastcall Clear(TColor color);
	const Graphics::TBitmap* __fastcall GetBitmapData() const;
    const bool __fastcall IsEmpty() const;
	unsigned char *__fastcall CreateRGBAData() const;

	__int32 mOpaqType;
	__int32 mType;

 private:

	__int32 __fastcall GetWidth() const;
	__int32 __fastcall GetHeight() const;
	__int32 __fastcall GetHeight2N() const;
	__int32 __fastcall GetWidth2N() const;
	TCanvas* __fastcall GetCanvas() const;
	void __fastcall CalculateSize2N();

	Graphics::TBitmap* mpBmp;
	__int32 mWidth2N;
	__int32 mHeight2N;
};

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

class TImageFont
{
 public:
	__fastcall TImageFont();
	__fastcall virtual ~TImageFont();

    __int32 __fastcall GetWidth() const ;

	const UnicodeString __fastcall GetName() const;

 	const UnicodeString __fastcall GetImageResourceName() const;

	bool __fastcall LoadFont(UnicodeString iName);

	const ::Font* __fastcall GetData() const;

 private:

	::Font mFont;
	UnicodeString mName;
	UnicodeString mResName;
};

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


class TFrameObj
{ 
  private:
	__int32			res_idx;
	__int32			img_source_w;
	__int32			img_source_h;
	UnicodeString	img_source_name;
	hash_t			initial_hash;
	bool			has_initial_hash;

  public:
	short				x, y;
	unsigned short		w, h;
    UnicodeString		bgPropertyName;
	UnicodeString		groupId;

 public:
	__fastcall TFrameObj();
	virtual __fastcall ~TFrameObj();

	void __fastcall Clear();
	void __fastcall setSourceRes(__int32 sw, __int32 sh, __int32 ires_idx, UnicodeString sname);
	void __fastcall getSourceRes(__int32 *sw, __int32 *sh, __int32 *ores_idx, UnicodeString *sname) const;
	const __int32 __fastcall getResIdx() const;
	void __fastcall copyTo(TFrameObj &tBGobj) const;
	bool __fastcall IsEmpty() const;
	bool __fastcall compareTo(TFrameObj &tBGobj) const;
	void __fastcall SetInitialHash(const hash_t& hash);
	bool __fastcall GetInitialHash(hash_t& hash) const;
	bool __fastcall IsValid() const;
	const hash_t __fastcall getHash() const;
};




//---------------------------------------------------------------------------
//---------------------------------------------------------------------------




class TObjPattern
{
	std::list<PropertyItem> vars;
	UnicodeString       fname;

 public:

  __fastcall TObjPattern();
  virtual __fastcall ~TObjPattern();

  void __fastcall Clear();
  UnicodeString __fastcall getFilename(){return fname;}

  bool __fastcall IsVarsEmpty(){return vars.empty();}
  const std::list<PropertyItem>* __fastcall GetVars() const;
  void __fastcall GetVars(std::list<PropertyItem>* outlist) const;
  void __fastcall SetVars(const std::list<PropertyItem>* list);

  //��������� ������ �� �����. ���� errMes �� NULL, �������� �� ������ ��������� �� �� �����, � � ��� ����������
  bool __fastcall loadPattern(UnicodeString filename, UnicodeString *errMes = NULL);
  bool __fastcall loadFromFile(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, int ver, UnicodeString *mesErr);
  bool __fastcall saveToFile(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h);

  void __fastcall CopyTo(TObjPattern *pt) const;
};



//---------------------------------------------------------------------------
//---------------------------------------------------------------------------



//����� ������ � ����������� ��� ���������� �� ����� � ������� ����������
class GPersonProperties
{
	std::list<PropertyItem> var_lst;
	void __fastcall del_var_lst();

public:
  GPersonProperties();
  virtual ~GPersonProperties();

  //�������������� ������������ ������ ���������� ����� ��������
  void __fastcall assignPattern(TObjPattern *newPt);
  //��������� ����� �������� �������
  void __fastcall assignProperties(const GPersonProperties &newPrp);

  //val (out) �������� ���������� var_name. ���� ����� ���������� �� ������� FALSE
  bool  __fastcall getVarValue(double *val, UnicodeString var_name) const;
  bool  __fastcall getDefValue(double *val, UnicodeString var_name) const;
  //val (in) �������� ���������� var_name. ���� ����� ���������� �� ������� FALSE
  bool  __fastcall setVarValue(double val, UnicodeString var_name);
  bool  __fastcall setDefValue(double val, UnicodeString var_name);
  // ���������� �������� ���������� �� ���������
  void  __fastcall setVarValuesToDef();

  bool  __fastcall getChecked(bool *var, UnicodeString var_name) const;
  bool  __fastcall setChecked(bool var, UnicodeString var_name);
};



//---------------------------------------------------------------------------
//---------------------------------------------------------------------------



class TAnimationFrameData
{
  private:

	unsigned short data;
	hash_t _frameId;

  public:

	__property hash_t frameId = { read = _frameId };

	LogicObjType type;
	double posX;
	double posY;
	__int32 rw;
	__int32 rh;
	unsigned char a;
	UnicodeString strm_filename;

	__fastcall TAnimationFrameData();
	__fastcall TAnimationFrameData(const TAnimationFrameData& afd);
	virtual __fastcall ~TAnimationFrameData(){}

	void __fastcall SetFrameId(hash_t fid);

	bool __fastcall isStreamFrame() const;
	void __fastcall setStreamFrame(bool val);

	unsigned short __fastcall getTransformAngle() const;
	void __fastcall setTransformAngle(unsigned short val);

	void __fastcall setData(unsigned short val);
	unsigned short __fastcall getData() const;

	void __fastcall Clear();
};
//---------------------------------------------------------------------------

class TAnimationFrame
{
 public:

	__fastcall TAnimationFrame();
	__fastcall TAnimationFrame(const TAnimationFrame &t);
	virtual __fastcall ~TAnimationFrame();

  //��������� ������ �� ������ TAnimationFrameData
  // ilist ����������, � �� �������������. ��� ������� ������ ���������.
	void __fastcall CreateFromList(std::list<TAnimationFrameData>const &list);

  //�������� ������ TAnimationFrameData
  // ������ ��������� � ������� (����� � �� ������ ���������), ����� ������������� ������� (delete)
	void __fastcall CopyFrameDataTo(std::list<TAnimationFrameData> &olist) const;

	void __fastcall Clear();
	bool __fastcall IsEmpty() const;

	void __fastcall CopyTo(TAnimationFrame *target) const;

	void __fastcall ReplaceBitmapFrame(const hash_t& oldHash, const TFrameObj &newObj);

	void __fastcall SetCollideRect(TRect &r);
	TRect __fastcall GetCollideRect() const;

	bool __fastcall SetJoinNodeCoordinate(__int32 idx, TPoint pt);
	bool __fastcall GetJoinNodeCoordinate(__int32 idx, TPoint &pt) const;

	__int32 __fastcall GetWidth() const;
	__int32 __fastcall GetHeight() const;
	__int32 __fastcall GetLeftOffset() const;
	__int32 __fastcall GetTopOffset() const;

	__int32 __fastcall GetDuration() const;
	void __fastcall SetDuration(__int32 duration);

	void __fastcall SetAlpha(unsigned char a);

	UnicodeString __fastcall GetEventId() const;
	void __fastcall SetEventId(UnicodeString eventId);

	bool __fastcall Intersect(__int32 left, __int32 top, __int32 right, __int32 bottom) const;

	bool __fastcall HasStreamObject() const;
	unsigned short __fastcall GetStreamObjectWidth() const;
	unsigned short __fastcall GetStreamObjectHeight() const;

 private:

	std::list<TAnimationFrameData> mObjects; //TAnimationFrameData
	TRect mCollideRect;
	__int32 mWidth;
	__int32 mHeight;
	__int32 mLeftOffset;
	__int32 mTopOffset;
	__int32 mDuration;
	unsigned short mStreamObjWidth;
	unsigned short mStreamObjHeight;
	UnicodeString mEventId;
	TPoint mJoinNodes[MAX_ANIMATON_JOIN_NODES];
};



//---------------------------------------------------------------------------
//---------------------------------------------------------------------------



class TAnimation
{
 public:

	__fastcall TAnimation(__int32 size);
	__fastcall TAnimation(const TAnimation *anim);
	__fastcall TAnimation(const TAnimation &anim);
	virtual __fastcall ~TAnimation();

	void __fastcall SetName(UnicodeString name);
	const UnicodeString __fastcall GetName() const;

	void __fastcall SetLooped(bool val);
	bool __fastcall GetLooped() const;

	__int32 __fastcall GetFrameCount() const;
	void __fastcall Resize(__int32 size);
	bool __fastcall SetFrame(const TAnimationFrame &obj, __int32 idx);
	bool __fastcall InsertFrame(const TAnimationFrame &obj, __int32 idx);
	bool __fastcall MoveFrame(__int32 idxFrom, __int32 idxTo);
	bool __fastcall ClearFrame(__int32 idx);
	bool __fastcall DeleteFrame(__int32 idx);
	const TAnimationFrame* __fastcall GetFrame(__int32 idx) const;
	void __fastcall ReplaceBitmapFrame(const hash_t& oldHash, const TFrameObj &newObj);
	void __fastcall RenameEventName(UnicodeString oldName, UnicodeString newName);

	void __fastcall SetViewRect(TRect &r);
	const TRect& __fastcall GetViewRect() const;

	void __fastcall CopyTo(TAnimation *target) const;

	bool __fastcall HasStreamObject() const;
	unsigned short __fastcall GetStreamObjectWidth() const;
	unsigned short __fastcall GetStreamObjectHeight() const;

 private:

	void __fastcall Clear();
	void __fastcall CheckForStreamObj();

	UnicodeString mName;
	bool mLooped;
	TRect mViewRect;
	unsigned short mStreamObjWidth;
	unsigned short mStreamObjHeight;
	std::list<TAnimationFrame> mFrames; //TAnimationFrame
};


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------




class GameObj
{
 protected:

  TMapObjType			objId;

 public:

  __fastcall GameObj();
  virtual __fastcall ~GameObj();

  TMapObjType __fastcall GetObjType(){return objId;}

  virtual bool __fastcall IsEmpty() const = 0; //������� TRUE, ���� ���������� ���� �������� -- �������

  virtual void __fastcall RebuildEmptyBmp() = 0; //����������� empty_bmp
  BMPImage* __fastcall GetDrawDefaultBitmap();
};




//---------------------------------------------------------------------------
//---------------------------------------------------------------------------



//����� �������� ���� �����
class GMapBGObj : public GameObj
{
	__int32			ew, eh;
	UnicodeString	epmty_caption;
	TList			*bmp_lst;

	void  __fastcall Create();

 public:

	UnicodeString    Memo;

	__fastcall GMapBGObj();
	__fastcall GMapBGObj(__int32 EmptyFrames);
	__fastcall GMapBGObj(const GMapBGObj& obj);
	virtual __fastcall ~GMapBGObj();

	//����������� empty_bmp
	void __fastcall CreateEmptyBmp(__int32 w, __int32 h, UnicodeString caption);
	virtual void __fastcall RebuildEmptyBmp();

	//������� ����� ���������
	// ���� fixedCopySize �� 0, ���������� ��������� ���������� ���������
	//	���� fixedCopySize ������ ������������� ���������� ������� ��������, ��� ��������
	//	�������� �� ������� ������� ��������� �������.
	//	���� fixedCopySize ������ ������������� ���������� ������� ��������
	//	���������� ������ ��������� ����������
	bool __fastcall CopyTo(GMapBGObj *Gpers,  __int32 fixedCopySize = 0) const;

	virtual bool __fastcall IsEmpty() const;

	TFrameObj* __fastcall GetFrameObj(__int32 idx) const;
	bool  __fastcall AssignFrameObj(TFrameObj *obj, __int32 idx);
	bool  __fastcall ClearFrameObj(__int32 img_idx);
	bool  __fastcall MoveFrameObj(__int32 from_idx, __int32 to_idx);
	void  __fastcall DelAllFrameObj();
	__int32 __fastcall GetFrameObjCount() const;

	bool __fastcall CompareTo(GMapBGObj* tBGobj);
};




//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//����� ���������
class GPerson : public GameObj
{
  private:

	UnicodeString		mName;     //��� �������������� (������ � �������� ��� �������� define-�����)
	GPersonProperties	properties; //������ ���������� item_val, ���������� �� ������ (������� �������� �������)
	TAnimationFrame		*mpIconData;
	std::map<const UnicodeString, TAnimation*> mAnimMap;
	UnicodeString		mDefaultStateName;
	bool                mResOptimization;// ��������� �� ������� � ����������� ��������
	bool                mUseGraphics; //���������� �� ������� � ���� (��� �� ��� ����� ������ � �������)
	bool                mIsDecoration;
	TMapObjDecorType	mDecorType;

	void __fastcall Clear();

 public:

	static UnicodeString __fastcall IdToName(const UnicodeString &id);
	static UnicodeString __fastcall NameToId(const UnicodeString &name);

	__int32 mGroupId;

	 GPerson();
	virtual __fastcall ~GPerson();

	UnicodeString __fastcall GetName() const {return mName;}
	void __fastcall SetName(UnicodeString str);

	virtual void __fastcall RebuildEmptyBmp(); //����������� empty_bmp
	virtual bool __fastcall IsEmpty() const;

	void __fastcall AssignPattern(TObjPattern *newPt);

	void __fastcall SetIconData(const TAnimationFrame* data);
	const TAnimationFrame* __fastcall GetIconData() const;

	bool __fastcall CopyTo(GPerson *Gpers) const;

	const GPersonProperties& __fastcall GetBaseProperties() const;
	GPersonProperties& __fastcall GetBaseProperties();

	void __fastcall AssignAnimation(TAnimation* obj);
	void __fastcall DeleteAnimation(const UnicodeString& name);
	TAnimation* __fastcall GetAnimation(const UnicodeString& name) const;

	void __fastcall GetBitmapFrameList(TList *olist);
	void __fastcall ReplaceBitmapFrame(const hash_t& oldHash, const TFrameObj &newObj);
	void __fastcall RenameEventNameInAnimations(const UnicodeString &oldName, const UnicodeString &newName);

	__property bool ResOptimization = {read = mResOptimization, write = mResOptimization};
	__property bool UseGraphics = {read = mUseGraphics, write = mUseGraphics};
	__property bool IsDecoration = {read = mIsDecoration, write = mIsDecoration};
	__property TMapObjDecorType DecorType = {read = mDecorType, write = mDecorType};
	__property UnicodeString DefaultStateName = {read = mDefaultStateName, write = mDefaultStateName};
};




//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


class TMapPersonCasheData
{
  public:
	const GPerson		*m_pParentRes;
	const TState		*m_pState;
	const TMapGPerson	*m_pParentLinkObj;
	const TMapGPerson	*m_pChildsLinkObj[MAX_ANIMATON_JOIN_NODES];
	const TMapDirector	*directors[4];
	__int32				m_Err_bmp_idx;
	hash_t				m_DynamicObjFrameHash;
	UnicodeString		m_Err_bmp_name;
	UnicodeString		m_DynamicObjResId;
	UnicodeString		m_DynamicObjTempFilename;
	UnicodeString		m_stateName;

	__fastcall TMapPersonCasheData();
	virtual __fastcall ~TMapPersonCasheData();
	void __fastcall Clear();
};

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

class TPrpFunctions : public TForm
{
 public:

	static const int LOCK_IMAGE_IDX;

	__fastcall TPrpFunctions(TComponent* Owner) : TForm(Owner){}

 	virtual void __fastcall SetTransform(const TTransformProperties& transform) = 0;
	virtual void __fastcall SetRemarks(UnicodeString str) = 0;
	virtual void __fastcall SetVariables(const GPersonProperties *prp) = 0;
	virtual void __fastcall SetScript(UnicodeString KeyName, UnicodeString ScriptName) = 0;
	virtual void __fastcall SetNode(UnicodeString NodeName) = 0;
	virtual void __fastcall SetZone(int zone) = 0;
	virtual void __fastcall SetState(UnicodeString name) = 0;
	virtual void __fastcall SetVarValue(double val, UnicodeString var_name) = 0;
	virtual void __fastcall SetSizes(int w, int h) = 0;
	virtual void __fastcall Lock(bool value) {};

	virtual void __fastcall refreshNodes() = 0;
	virtual void __fastcall refreshCplxList() = 0;
	virtual void __fastcall refresStates() = 0;
	virtual void __fastcall refreshVars() = 0;
	virtual void __fastcall refreshScripts() = 0;

	virtual void __fastcall SetNodeU(UnicodeString NodeName) = 0;
	virtual void __fastcall SetNodeD(UnicodeString NodeName) = 0;
	virtual void __fastcall SetNodeL(UnicodeString NodeName) = 0;
	virtual void __fastcall SetNodeR(UnicodeString NodeName) = 0;
	virtual void __fastcall SetTrigger(UnicodeString triggerName) = 0;
	virtual void __fastcall refreshTriggers() = 0;

	virtual void __fastcall Init(TCommonResObjManager *commonResObjManager) = 0;
	virtual void __fastcall Localize() = 0;
	virtual void __fastcall EmulatorMode(bool mode) = 0;

	virtual void __fastcall Reset() = 0;
};
//---------------------------------------------------------------------------

class TPropertyWindow
{
 public:

	__fastcall TPropertyWindow();
	virtual __fastcall ~TPropertyWindow(){}

	void __fastcall RegisterPrpWnd(TPrpFunctions *iprpWnd);
	void __fastcall UnregisterPrpWnd();

 protected:

   TPrpFunctions			*prpWnd;
};
//---------------------------------------------------------------------------

class TMapPerson
{
  private:

	TAnimation *mpAnimation;
	std::list<TAnimationFrameData> mAnimaData;

  protected:

  	TObjManager			*mpParent;
	TTransformProperties mTransform;
	TMapObjType			mObjId;
	double				mScreenTop;
	double				mScreenLeft;
	__int32				mWidth;
	__int32				mHeight;
	__int32				mOffPosX;
	__int32				mOffPosY;
	bool				mFixedSizes;
	__int32				mZoneIndex;
	bool				mVisible;
	bool				mCustomVisible;
	bool				mEnabled;
	bool				showRect;
	bool				shortHint;
	UnicodeString		mHint;
	UnicodeString		Memo;
	UnicodeString		Parent_Name;
	__int32				*off_x, *off_y;
	__int32				mCurrentFrame;
	bool				InterlacedTransparency;
	__int32				mCurrentFrameDuration;
	bool				mEndAnimation;
	bool                mLocked;

	void __fastcall CopyTo(TMapPerson *mgp) const;
	virtual void __fastcall inner_realign() = 0;

	void __fastcall ApplyAnimationFrame(const TAnimationFrame *af);
	const std::list<TAnimationFrameData>& __fastcall GetAnimationFrameData() const;
	const TAnimationFrame* __fastcall GetCurrentAnimationFrame() const;
	void __fastcall RestartAnimation();
	void __fastcall FreeAnimaData();
	virtual void __fastcall ClearSizes();
	virtual void __fastcall AssignAnimation(const TAnimation *anim);
	__fastcall TMapPerson(){};
	__fastcall TMapPerson(TObjManager* Owner);
	__fastcall TMapPerson(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString GPName, TObjManager* Owner);

 public:

	virtual __fastcall ~TMapPerson();

	UnicodeString	Name;
	__int32			CasheDataIndex;

	const TMapObjType __fastcall GetObjType() const {return mObjId;}

	UnicodeString __fastcall getGParentName() const;

	bool __fastcall IsRected(){return showRect;}

	const TTransformProperties& __fastcall GetTransform() const;

	virtual void __fastcall setCoordinates(double x, double y, __int32 dx, __int32 dy) = 0;
	void __fastcall getCoordinates(double *x, double *y) const;
	void __fastcall getScreenCoordinates(double *x, double *y) const;
	const __int32& __fastcall GetZone() const {return mZoneIndex;}

	const UnicodeString& __fastcall getRemarks() const { return Memo; }
	void __fastcall SetShortHint(bool val);
	void __fastcall changeHint();

	void __fastcall ShowInterlacedTransparency(bool val);
	bool __fastcall IsInterlacedTransparency() const {return InterlacedTransparency;}

	virtual void __fastcall Lock(bool val);
	bool __fastcall IsLocked() const { return mLocked; }

	const TAnimation* __fastcall GetAnimation() const;

	virtual bool __fastcall Intersect(__int32 left, __int32 top, __int32 right, __int32 bottom);

	virtual void __fastcall SetVisible(bool val);
	virtual void __fastcall SetCustomVisible(bool val);

	virtual void __fastcall ShowRect(bool val) = 0;
	virtual void __fastcall SetZone(__int32 zone) = 0;
	virtual void __fastcall OnScrollBarChange() = 0;
	virtual void __fastcall OnGlobalScale() = 0;
	virtual void __fastcall setRemarks(UnicodeString str) = 0;

	__property UnicodeString Hint = {read = mHint};
	__property bool Visible = {read = mVisible };
	__property bool CustomVisible = {read = mCustomVisible };
	__property __int32 Width = {read = mWidth};
	__property __int32 Height = {read = mHeight};
	__property bool Enabled = {read = mEnabled, write = mEnabled};
	__property TObjManager* Parent = {read = mpParent};
};
//---------------------------------------------------------------------------



class TMapGPerson : public TMapPerson, public TPropertyWindow
{
  friend class TObjManager;

  private:

	TRect					mCollideRect;
	TRect					mViewRect;

	bool                   hasLinks;
	TStringList            *dirList;
	TStringList            *scriptList;

	bool                   showEnabled;
	TMapObjDecorType	   mDecorType;
	UnicodeString		   FState_Name;
	UnicodeString		   FNextLinkedDirector;
	UnicodeString		   linkSeek;
	TStringList			   *mpChilds;
	GPersonProperties      properties;

	virtual void __fastcall inner_realign() override;
	virtual void __fastcall AssignAnimation(const TAnimation *anim) override;
	void __fastcall SetStateName(UnicodeString name);

  protected:

	virtual void __fastcall ClearSizes() override;
	void __fastcall DoSetCoordinates(double x, double y, __int32 dx, __int32 dy);

	__fastcall TMapGPerson(TObjManager* Owner);
	__fastcall TMapGPerson(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString GpersonParentName, TObjManager* Owner);
	virtual __fastcall ~TMapGPerson();

  public:

	UnicodeString			ParentObj;
	UnicodeString			PrevLinkedDirector;
	TTransformProperties	mPrevTransform;

	__int32                 DirStateIdx;
	bool                  	InspectStates,
							StopOnStateChange,
							TraceTriggerActions;
	bool                  	InspectEndAnimation;

	virtual void __fastcall CopyTo(TMapGPerson *mgp) const;

	void  __fastcall ResetPrevTransform();

	UnicodeString __fastcall getDirStateItem(__int32 idx) const;
	void   __fastcall addDirStateItem(UnicodeString StateName);
	void   __fastcall setDirStateItem(__int32 idx, UnicodeString StateName);
	void   __fastcall deleteDirStateItem(__int32 idx);
	__int32 __fastcall getDirStatesCount() const;
	__int32 __fastcall clearDirStates();

	UnicodeString __fastcall getScriptName(UnicodeString KeyName) const;
	void   __fastcall setScriptName(UnicodeString KeyName, UnicodeString ScriptName);
	__int32    __fastcall clearScripts();
	__int32    __fastcall getScriptsCount() const;

	void __fastcall ShowEnabled(bool val);
	void __fastcall assignNewParent(UnicodeString iGPParent);

	virtual void __fastcall Lock(bool val) override;

	void __fastcall ProcessAnimation(unsigned __int32 ms);
	bool __fastcall IsEndAnimation();

	const TRect& __fastcall GetCollideRect() const;
	const TRect& __fastcall GetViewRect() const;

	virtual void __fastcall setCoordinates(double x, double y, __int32 dx, __int32 dy) override;
	virtual void __fastcall OnScrollBarChange() override;
	virtual void __fastcall OnGlobalScale() override;
	virtual void __fastcall SetZone(__int32 zone) override;
	virtual void __fastcall ShowRect(bool val) override;
	virtual void __fastcall setRemarks(UnicodeString str) override;

	virtual void __fastcall changeHint();

	bool __fastcall SetDecorType(TMapObjDecorType type);
	TMapObjDecorType __fastcall GetDecorType() const;

	GPersonProperties& __fastcall GetProperties(){return properties;}
	const GPersonProperties& __fastcall GetProperties() const {return properties;}
	bool  __fastcall GetVarValue(double *val, UnicodeString var_name);
	bool  __fastcall SetVarValue(double val, UnicodeString var_name);
	bool  __fastcall GetDefValue(double *val, UnicodeString var_name);
	void  __fastcall SetVarValuesToDef();
	bool  __fastcall SetCheckedValue(bool var, UnicodeString var_name);
	bool  __fastcall GetCheckedValue(bool *var, UnicodeString var_name);

	void __fastcall AddChild(__int32 joinnode, UnicodeString obj);
	UnicodeString __fastcall GetChild(__int32 joinnode) const;
	bool __fastcall IsChildExist() const;

	void __fastcall SetProperties(const GPersonProperties &new_prp);

	void __fastcall SetNodeName(UnicodeString NodeName);

	__property UnicodeString State_Name = {read = FState_Name, write = SetStateName};
	__property UnicodeString NextLinkedDirector = {read = FNextLinkedDirector, write = SetNodeName};
};




//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


class TMapSpGPerson : public TMapGPerson
{
 private:

   void __fastcall CopyTo(TMapGPerson *mgp) const{};
   virtual void __fastcall inner_realign();

 protected:

 __fastcall TMapSpGPerson(double x, double y, __int32 *pos_x,__int32 *pos_y, UnicodeString iGPParent, TObjManager* Owner);
 __fastcall TMapSpGPerson(TObjManager* Owner);
 virtual __fastcall ~TMapSpGPerson();

 public:

   UnicodeString		objName;
   UnicodeString		objTeleport;
   TStringList			*stateList;
   __int32				tileTeleport;
   __int32				mapDirection;
   __int32				mapIndex;
   __int32				mapCount;
   __int32				Type;

   UnicodeString		nearNodes[4];
   UnicodeString		nodeTrigger;

 virtual void __fastcall CopyTo(TMapSpGPerson *mgp) const;
 void __fastcall Clear();
};

//---------------------------------------------------------------------------

//����� ������� �� �����
class TMapTrigger : public TMapSpGPerson
{
  friend class TObjManager;

  __fastcall TMapTrigger(double x, double y, __int32 *pos_x,__int32 *pos_y,UnicodeString GParentName, TObjManager* Owner);
  __fastcall TMapTrigger(TObjManager* Owner);
  virtual __fastcall ~TMapTrigger(){}
};
//---------------------------------------------------------------------------

//����� ��������� �� �����
class TMapDirector : public TMapSpGPerson
{
  friend class TObjManager;

  private:

	__fastcall TMapDirector(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString GParentName, TObjManager* Owner);
	__fastcall TMapDirector(TObjManager* Owner);
	virtual __fastcall ~TMapDirector(){}

  public:

	void __fastcall SetEventName(UnicodeString eventName);
	UnicodeString __fastcall GetEventName() const;
};
//---------------------------------------------------------------------------

class TMapTextBox : public TMapGPerson
{
  friend class TObjManager;

 public:

	enum TMapTexBoxType
	{
		MTBT_Static = 0,
		MTBT_Dynamic
	};

	virtual void __fastcall CopyTo(TMapTextBox *mgp) const;

	void __fastcall SetTextType(const TMapTexBoxType);
	const TMapTexBoxType __fastcall GetTextType() const;
	void __fastcall SetDynamicTextSizes(__int32 maxStringsCount, __int32 maxCharsPerStringCount);
	void __fastcall GetDynamicTextSizes(__int32 &maxStringsCount, __int32 &maxCharsPerStringCount) const;

	void __fastcall SetFillColor(TColor color);
	const TColor& __fastcall GetFillColor() const;

	void __fastcall SetAlignment(TextAlignment align);
	TextAlignment __fastcall GetAlignment() const;

	void __fastcall SetFontName(UnicodeString name);
	const UnicodeString __fastcall GetFontName() const;

	void __fastcall SetTextId(UnicodeString id);
	const UnicodeString __fastcall GetTextId() const;

	void __fastcall SetTextWidth(__int32 val);
	void __fastcall SetTextHeight(__int32 val);
	const __int32 __fastcall GetTextWidth() const;
	const __int32 __fastcall GetTextHeight() const;
	const __int32 __fastcall GetMargin() const;

 private:

	__fastcall TMapTextBox(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString GParentName, TObjManager* Owner);
	__fastcall TMapTextBox(TObjManager* Owner);
	virtual __fastcall ~TMapTextBox();

	void __fastcall CopyTo(TMapGPerson *mgp) const{};
	virtual void __fastcall inner_realign();

	TMapTexBoxType mType;
	UnicodeString mTextId;
	UnicodeString mFontName;
	__int32 mTextWidth;
	__int32 mTextHeight;
	__int32 mMaxStringsCount;
	__int32 mMaxCharsPerStringCount;
	::TextAttributes mAttributes;
};
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------
class SoundSchemePropertyItem
{
 public:

	enum SoundSchemeResourceType
	{
		SSRT_NONE = 0,
		SSRT_SFX,
		SSRT_BG
	};

	__fastcall SoundSchemePropertyItem();
	__fastcall SoundSchemePropertyItem(const SoundSchemePropertyItem& sspi);
	virtual __fastcall ~SoundSchemePropertyItem();

	UnicodeString mName;
	UnicodeString mFileName;
	SoundSchemeResourceType mType;
	unsigned __int32 mFileSize;
};
//---------------------------------------------------------------------------

class TMapSoundScheme : public TMapGPerson
{
	friend class TObjManager;

 public:

	virtual void __fastcall CopyTo(TMapSoundScheme *mgp) const;

	void __fastcall AddItem(const SoundSchemePropertyItem& item);
	bool __fastcall GetItemByIndex(__int32 idx, SoundSchemePropertyItem& item) const;
	bool __fastcall GetItemByName(UnicodeString name, SoundSchemePropertyItem& item) const;
	bool __fastcall ChangeItem(UnicodeString name, SoundSchemePropertyItem& item);
	bool __fastcall DeleteItem(UnicodeString name);

 	__int32 __fastcall GetItemsCount() const;

	void __fastcall Clear();

 private:

	__fastcall TMapSoundScheme(double x, double y, __int32 *pos_x, __int32 *pos_y,
										UnicodeString GParentName, TObjManager* Owner);
	__fastcall TMapSoundScheme(TObjManager* Owner);
	virtual __fastcall ~TMapSoundScheme();
	SoundSchemePropertyItem* __fastcall FindItem(UnicodeString name);
 	const SoundSchemePropertyItem* __fastcall FindItem(UnicodeString name) const;

	void __fastcall CopyTo(TMapGPerson *mgp) const{};
	virtual void __fastcall inner_realign();

	std::list<SoundSchemePropertyItem> mItems;
};
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------
class ObjPoolPropertyItem
{
 public:
	__fastcall ObjPoolPropertyItem();
	virtual __fastcall ~ObjPoolPropertyItem();

	UnicodeString mName;
	unsigned __int32 mCount;
};
//---------------------------------------------------------------------------

class TMapObjPool : public TMapGPerson
{
	friend class TObjManager;

 public:

	virtual void __fastcall CopyTo(TMapObjPool *mgp) const;

	void __fastcall AddItem(ObjPoolPropertyItem& item);
	bool __fastcall GetItemByIndex(__int32 idx, ObjPoolPropertyItem& item) const;
	bool __fastcall GetItemByName(UnicodeString name, ObjPoolPropertyItem& item) const;
	bool __fastcall ChangeItem(UnicodeString name, ObjPoolPropertyItem& item);
	bool __fastcall DeleteItem(UnicodeString name);
	__int32 __fastcall MoveItem(__int32 idx_from, __int32 idx_to);
	__int32 __fastcall GetItemsCount() const;
	__int32 __fastcall GetTolalObjCount() const;

	void __fastcall Clear();

 private:

	__fastcall TMapObjPool(double x, double y, __int32 *pos_x, __int32 *pos_y,
										UnicodeString GParentName, TObjManager* Owner);
	__fastcall TMapObjPool(TObjManager* Owner);
	virtual __fastcall ~TMapObjPool();
	ObjPoolPropertyItem* __fastcall FindItem(UnicodeString name);
	const ObjPoolPropertyItem* __fastcall FindItem(UnicodeString name) const;

	void __fastcall CopyTo(TMapGPerson *mgp) const{};
	virtual void __fastcall inner_realign();

	std::list<ObjPoolPropertyItem> mItems;
};
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------
class TPropertiesControl
{
 public:

	typedef void __fastcall (__closure *TOnPropertiesControlValueValidate)(const UnicodeString, double);
	typedef void __fastcall (__closure *TOnPropertiesControlOnCheck)(const UnicodeString, bool);

	enum TPropertiesControlMode
	{
		PCM_NORMAL,
		PCM_CHECKERS,
    };

	__fastcall TPropertiesControl(TScrollBox *parent);
	virtual __fastcall ~TPropertiesControl();

	void __fastcall Init(const std::list<PropertyItem>* varlist, TPropertiesControlMode mode);
	void __fastcall Clear();

	void __fastcall SetValue(double val, UnicodeString var_name);
	void __fastcall SetChecked(bool val, UnicodeString var_name);
	void __fastcall SetOnValidate(TOnPropertiesControlValueValidate fn);
	void __fastcall SetOnCheck(TOnPropertiesControlOnCheck fn);

 private:

	void __fastcall OnSEChange(TObject *Sender);
	void __fastcall OnSEEnter(TObject *Sender);
	void __fastcall OnSEExit(TObject *Sender);
	void __fastcall OnSEKeyPress(TObject *Sender, WideChar &Key);
	void __fastcall OnCheckBoxClick(TObject *Sender);

	UnicodeString mOldVal;
	TScrollBox *mpScrollBox;
	TPropertiesControlMode mMode;
	TOnPropertiesControlValueValidate mOnValidate;
	TOnPropertiesControlOnCheck mOnCheck;
};
//---------------------------------------------------------------------------

#endif
