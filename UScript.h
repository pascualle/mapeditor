//---------------------------------------------------------------------------

#ifndef UScriptH
#define UScriptH
//---------------------------------------------------------------------------
#include <stdio.h>

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <Vcl.Samples.Spin.hpp>

//---------------------------------------------------------------------------

enum //<= == >= !=
{
  mtLESSEQ,
  mtEQUAL,
  mtMOREEQ,
  mtNOT
};

class TSaveLoadStreamData;

class TGScript
{
 public:

 __fastcall TGScript ();
 __fastcall ~TGScript();
/*
 UnicodeString	Name;           //���
 UnicodeString	ChangeCtrlTo;   //����������� ����������
 TStringList    *TrigNameList;  //������� ���������
 int            Zone;           //������� ����
 int            GoToZone;       //������� �� ����
 int            EndLevel;       //��������� �������  (-1 -- �� ���������)
 UnicodeString	Message;		//�������� ���������
 bool           Multiple;       //����������� ��� ������������ ������
 char           Mode;           //��� ������� (sptONCOLLIDE,sptONACTIVATE,sptONCHANGE)
 bool           Enabled;        //��� ������, ��� ���������� ������������

 //sptONCOLLIDE
 TStringList    *CldGroupNameList;      //������
 TStringList    *CldObjNameList;        //�������
 UnicodeString	CldPropNameInitiator;   // �������� ����������
 UnicodeString	CldPropNameSecond;      // �������� ������� ���������
 float          CldParamValueInitiator; // �������� ���������
 float          CldParamValueSecond;    //
 UnicodeString	CldStateNameInitiator;  // ���������
 UnicodeString	CldStateNameSecond;     //
 char           CldMathOperPropInitiator; //�������������� �������� ���������
 char           CldMathOperPropSecond;
 char           CldMathOperStateInitiator; //�������������� �������� ���������
 char           CldMathOperStateSecond;

 //sptONCHANGE
 UnicodeString	ChgPropName;    //��������
 char           ChgMathOper;    //�������������� ��������
 float          ChgValue;      //��������

 */
	void __fastcall CopyTo(TGScript *spt) const;

	void __fastcall SaveToFile(BYTE mode, TSaveLoadStreamData& f);
	void __fastcall LoadFromFile(TSaveLoadStreamData& f, int ver);
};
//---------------------------------------------------------------------------

class TFScript : public TForm
{
__published:	// IDE-managed Components
	TEdit *Edit_name;
	TPageControl *PageControl;
	TTabSheet *TabCollide;
	TTabSheet *TabChange;
	TLabel *Label1;
	TLabel *Label2;
	TButton *ButtonCancel;
	TButton *ButtonOk;
	TListBox *ListBox_gr;
	TComboBox *CB_col_obj;
	TComboBox *CB_col_gr;
	TComboBox *CB_param;
	TComboBox *CB_Op;
	TLabel *Label3;
	TLabel *Label5;
	TLabel *Label4;
	TLabel *Label6;
	TLabel *Label7;
	TBevel *Bevel1;
	TBitBtn *Bt_gradd;
	TBitBtn *Bt_grdel;
	TBitBtn *Bt_objdel;
	TBitBtn *Bt_objadd;
	TBevel *Bevel2;
	TListBox *ListBox_obj;
	TLabel *Label11;
	TTabControl *TabControl;
	TComboBox *CB_trig;
	TListBox *ListBox_trig;
	TBitBtn *Bt_trdel;
	TBitBtn *Bt_tradd;
	TBevel *Bevel3;
	TBitBtn *Bt_trdown;
	TBitBtn *Bt_trup;
	TLabel *Label12;
	TGroupBox *GroupBox1;
	TLabel *Label13;
	TComboBox *CB_new_hero;
	TCheckBox *CheckBox_end;
	TCheckBox *CheckBox_multi;
	TCheckBox *CheckBox_zone;
	TLabel *Label14;
	TComboBox *ComboBox_zone;
	TCheckBox *CheckBoxMessageID;
	TBevel *Bevel4;
	TButton *Bt_3dot;
	TLabel *Label16;
	TBevel *Bevel5;
	TGroupBox *GroupBoxInitiator;
	TEdit *EditStateInitiator;
	TComboBox *CB_act_ParamInitiator;
	TComboBox *CB_Op_ParamInitiator;
	TComboBox *CB_Op_StateInitiator;
	TComboBox *CB_StateNameInitiator;
	TGroupBox *GroupBox2;
	TEdit *EditStateSecond;
	TComboBox *CB_act_ParamSecond;
	TComboBox *CB_Op_ParamSecond;
	TComboBox *CB_Op_StateSecond;
	TComboBox *CB_StateNameSecond;
	TTabSheet *TabEndAnim;
	TTabSheet *TabTileCollide;
	TGroupBox *GroupBox3;
	TEdit *EditStateInitiatorTile;
	TComboBox *CB_act_ParamInitiatorTile;
	TComboBox *CB_Op_ParamInitiatorTile;
	TComboBox *CB_Op_StateInitiatorTile;
	TComboBox *CB_StateNameInitiatorTile;
	TGroupBox *GroupBox4;
	TLabel *Label8;
	TLabel *Label9;
	TBitBtn *Bt_find_trig;
	TLabel *Label10;
	TTabSheet *TabChangeDirector;
	TLabel *Label15;
	TGroupBox *GroupBox5;
	TEdit *EditStateInitiatorNode;
	TComboBox *CB_act_ParamInitiatorNode;
	TComboBox *CB_Op_ParamInitiatorNode;
	TComboBox *CB_Op_StateInitiatorNode;
	TComboBox *CB_StateNameInitiatorNode;
	TGroupBox *GroupBox6;
	TComboBox *CB_node;
	TEdit *EditMessageName;
	void __fastcall CheckBox_zoneClick(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall ComboBox_zoneChange(TObject *Sender);
	void __fastcall Edit_nameChange(TObject *Sender);
	void __fastcall Bt_trupClick(TObject *Sender);
	void __fastcall Bt_trdownClick(TObject *Sender);
	void __fastcall Bt_traddClick(TObject *Sender);
	void __fastcall Bt_trdelClick(TObject *Sender);
	void __fastcall Bt_graddClick(TObject *Sender);
	void __fastcall Bt_grdelClick(TObject *Sender);
	void __fastcall Bt_objaddClick(TObject *Sender);
	void __fastcall Bt_objdelClick(TObject *Sender);
	void __fastcall CheckBoxMessageIDClick(TObject *Sender);
	void __fastcall Bt_3dotClick(TObject *Sender);
	void __fastcall Bt_find_trigClick(TObject *Sender);
	void __fastcall CB_trigExit(TObject *Sender);
	void __fastcall ListBox_trigExit(TObject *Sender);
	void __fastcall CheckBox_endClick(TObject *Sender);

private:

	TGScript        *script;
	bool            inited;
	void            *LastFocus;
	int             mOnlyZone;
	int             mOnlyMode;
	TStringList  	*mpZonesNames;
	TSpinEdit		*CE_act_valInitiator;
	TSpinEdit		*CE_act_valSecond;
	TSpinEdit		*CE_act_valInitiatorNode;
	TSpinEdit		*CE_val;
	TSpinEdit		*CE_zone;
	TSpinEdit		*CE_end;

	void __fastcall ShowCheckBox(TCheckBox *cb, TSpinEdit *ed);
	void __fastcall FillForm();
	void __fastcall SaveFormToStruct();
	void __fastcall ChangeZone(int zone);
	void __fastcall Init();

	int __fastcall GetSelectedZone();
	void __fastcall CreateCBList();

public:

	void __fastcall ActiveOnlyZone(int zone);
	void __fastcall ActiveOnlyMode(int mode);
	//void __fastcall SetScript(TGScript* spt);
	//void __fastcall GetScript(TGScript* spt);
	__fastcall TFScript(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFScript *FScript;
//---------------------------------------------------------------------------
#endif
