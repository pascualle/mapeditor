//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UFileSelect.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFFileSelect *FFileSelect;
//---------------------------------------------------------------------------
__fastcall TFFileSelect::TFFileSelect(TComponent* Owner)
	: TForm(Owner)
{
	Localization::Localize(this);
}
//---------------------------------------------------------------------------
