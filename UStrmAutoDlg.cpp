//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "UStrmAutoDlg.h"
#include "ULocalization.h"

//---------------------------------------------------------------------

#pragma resource "*.dfm"
TFStrmAutoDlg *FStrmAutoDlg;

//--------------------------------------------------------------------- 
__fastcall TFStrmAutoDlg::TFStrmAutoDlg(TComponent* AOwner)
	: TForm(AOwner)
{
	CheckEnabledTime();
	Localization::Localize(this);
}
//---------------------------------------------------------------------

void __fastcall TFStrmAutoDlg::CheckEnabledTime()
{
	if(CheckBox1->Checked)
	{
		Edit_time->Enabled = true;
		Edit_time->Color = clWindow;
	}
	else
	{
		Edit_time->Enabled = false;
		Edit_time->Color = clBtnFace;
    }
}
//---------------------------------------------------------------------

void __fastcall TFStrmAutoDlg::CheckBox1Click(TObject *)
{
	CheckEnabledTime();
}
//---------------------------------------------------------------------------

