object FCollideNodeProperty: TFCollideNodeProperty
  Left = 0
  Top = 0
  ActiveControl = Edit_name
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'mCollideNodeProperties'
  ClientHeight = 452
  ClientWidth = 376
  Color = clBtnFace
  Constraints.MinHeight = 479
  Constraints.MinWidth = 384
  DefaultMonitor = dmMainForm
  ParentFont = True
  FormStyle = fsStayOnTop
  OnHide = FormHide
  OnShow = FormShow
  TextHeight = 15
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 376
    Height = 433
    ActivePage = TS_main
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 368
    ExplicitHeight = 421
    object TS_main: TTabSheet
      Caption = 'mMainProperties'
      DesignSize = (
        368
        403)
      object Label5: TLabel
        Left = 3
        Top = 6
        Width = 43
        Height = 15
        Caption = 'mName'
      end
      object Edit_name: TEdit
        Left = 60
        Top = 3
        Width = 281
        Height = 23
        Anchors = [akLeft, akTop, akRight]
        CharCase = ecUpperCase
        MaxLength = 64
        TabOrder = 0
        OnEnter = Edit_nameEnter
        OnExit = Edit_nameExit
        OnKeyPress = Edit_nameKeyPress
      end
      object GroupBox_pos: TGroupBox
        Left = 3
        Top = 30
        Width = 362
        Height = 370
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 2
        object Label2: TLabel
          Left = 10
          Top = 11
          Width = 10
          Height = 15
          Caption = 'X:'
        end
        object Label3: TLabel
          Left = 83
          Top = 11
          Width = 10
          Height = 15
          Caption = 'Y:'
        end
        object CSEd_x: TEdit
          Left = 26
          Top = 8
          Width = 51
          Height = 23
          NumbersOnly = True
          TabOrder = 0
          OnEnter = CSEd_xEnter
          OnExit = CSEd_xExit
          OnKeyPress = CSEd_xKeyPress
        end
        object CSEd_y: TEdit
          Left = 98
          Top = 8
          Width = 51
          Height = 23
          NumbersOnly = True
          TabOrder = 1
          OnEnter = CSEd_xEnter
          OnExit = CSEd_xExit
          OnKeyPress = CSEd_xKeyPress
        end
      end
      object BitBtn_find_obj: TBitBtn
        Left = 341
        Top = 3
        Width = 24
        Height = 23
        Hint = 'mFindOnMap'
        Anchors = [akTop, akRight]
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000FC02FC004985
          D40084C6F400C7D1D100FAEFC700B08D8800B5B8BC00FCFCDA0056595B005CB6
          F10072758400868F9200F5D7B000B6C8CE00D3B39E008E787C000BAA60000000
          0000BA58A6000000000091A58A6000000000D91AF8A6000000000291A58BDDDD
          000000291AF8AA88ABD0000291BF5ECEA8FD0000265C47774F8B000006C44777
          748A00000E4C7777775800000E44477777EA00000C44477777EA00000E44C477
          745B000003C7744C4CFD000000E47444C5D000000006EEEEE300}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BitBtn_find_objClick
      end
    end
    object TS_Memo: TTabSheet
      Caption = 'mMemo'
      ImageIndex = 2
      DesignSize = (
        368
        403)
      object Memo: TMemo
        Left = 3
        Top = 3
        Width = 362
        Height = 398
        Anchors = [akLeft, akTop, akRight, akBottom]
        MaxLength = 200
        ScrollBars = ssVertical
        TabOrder = 0
        WordWrap = False
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 433
    Width = 376
    Height = 19
    Panels = <
      item
        Style = psOwnerDraw
        Width = 24
      end>
    OnDrawPanel = StatusBar1DrawPanel
    ExplicitTop = 421
    ExplicitWidth = 368
  end
end
