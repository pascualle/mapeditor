object FOPEdit: TFOPEdit
  Left = 420
  Top = 306
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  ClientHeight = 172
  ClientWidth = 343
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Pitch = fpFixed
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 118
    Height = 13
    Caption = 'mNameVariableLatinOnly'
  end
  object Label_def: TLabel
    Left = 18
    Top = 73
    Width = 81
    Height = 13
    Caption = 'mValueByDefault'
  end
  object Labelinfo: TLabel
    Left = 18
    Top = 79
    Width = 3
    Height = 13
  end
  object Ed_name: TEdit
    Left = 59
    Top = 32
    Width = 269
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 64
    TabOrder = 0
    OnChange = Ed_nameChange
  end
  object ButtonOk: TButton
    Left = 134
    Top = 134
    Width = 75
    Height = 25
    Caption = 'mApply'
    TabOrder = 2
    OnClick = ButtonOkClick
  end
  object Edit_def: TEdit
    Left = 160
    Top = 70
    Width = 56
    Height = 21
    TabOrder = 1
    Text = '0'
    OnChange = Edit_defChange
  end
  object EditPrefix: TEdit
    Left = 15
    Top = 32
    Width = 41
    Height = 21
    Cursor = crArrow
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    ReadOnly = True
    TabOrder = 3
    Text = 'PRP_'
    OnChange = Ed_nameChange
  end
end
