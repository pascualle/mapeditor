// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainUnit.h"
#include "UWait.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFWait *FWait;

// ---------------------------------------------------------------------------
__fastcall TFWait::TFWait(TComponent* Owner) : TForm(Owner)
{
	mText = _T("");
}
// ---------------------------------------------------------------------------

void __fastcall TFWait::FormKeyDown(TObject * /* Sender */ , WORD &Key, TShiftState /* Shift */ )
{
	Key = 0;
}
// ---------------------------------------------------------------------------

void __fastcall TFWait::Timer1Timer(TObject *)
{
	StaticText->Caption = mText;
}
//---------------------------------------------------------------------------

