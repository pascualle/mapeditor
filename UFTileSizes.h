//---------------------------------------------------------------------------

#ifndef UFTileSizesH
#define UFTileSizesH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Vcl.Samples.Spin.hpp>

//---------------------------------------------------------------------------
class TFTileSizes : public TForm
{
__published:
	TGroupBox *GroupBox_f;
	TLabel *Label1;
	TLabel *Label2;
	TButton *ButtonCancel;
	TButton *ButtonOk;
	TGroupBox *GroupBoxLayers;
	TLabel *Label13;
	TLabel *Label14;
	TLabel *Label15;
	TCheckBox *CheckBoxRemainMapSize;
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);

private:

	void __fastcall OnWHChange(TObject *);
	__int32 mW, mH;

public:
		TSpinEdit *CSpinEditPH;
		TSpinEdit *CSpinEditPW;
		TSpinEdit *CSpinEditLayers;
		__fastcall TFTileSizes(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFTileSizes *FTileSizes;
//---------------------------------------------------------------------------
#endif
