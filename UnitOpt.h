//---------------------------------------------------------------------------

#ifndef UnitOptH
#define UnitOptH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <Grids.hpp>
#include <ActnList.hpp>
#include <Menus.hpp>
#include <Buttons.hpp>
#include <Vcl.Samples.Spin.hpp>
#include "UTypes.h"

//---------------------------------------------------------------------------
class TForm_opt : public TForm
{
__published:
	TButton *Button1;
	TButton *Button2;
	TPageControl *PageControl;
	TTabSheet *TabMain;
	TGroupBox *GroupBox_m;
	TLabel *Label7;
	TLabel *Label8;
	TLabel *Label10;
	TLabel *Label11;
	TGroupBox *GroupBox_d;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TLabel *Label6;
	TGroupBox *GroupBox_zone;
	TLabel *Label12;
	TLabel *Label9;
	TTabSheet *TabFirstTime;
	TLabel *Label18;
	TLabel *Label19;
	TEdit *EdFirstScript;
	TLabel *Label20;
	TCheckBox *CheckBoxTileCameraOnStart;
	TEdit *EditPaletteColor;
	TLabel *Label1;
	TGroupBox *GroupBox_res;
	TLabel *Label2;
	TCheckBox *CBmIgnoreUnoptimizedResoures;
	void __fastcall CSpinEditPHChange(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall EdFirstScriptChange(TObject *Sender);
	void __fastcall CSpinEditZoneChange(TObject *Sender);
	void __fastcall CSpinEditZoneOnStartChange(TObject *Sender);
	void __fastcall CheckBoxTileCameraOnStartClick(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);

private:
		EditMode mMode;
		UnicodeString namePattern;
		void __fastcall CheckCheckBoxTileCameraOnStart();
public:

		TSpinEdit* CSpinEditZone;
		TSpinEdit* CSpinEditZoneOnStart;
		TSpinEdit* CE_TileCameraOnStart;
		TSpinEdit* CSpinEditDH;
		TSpinEdit* CSpinEditDW;
		TSpinEdit* CSpinEditMH;
		TSpinEdit* CSpinEditMW;

		UnicodeString __fastcall getNamePattern(){return namePattern;}
		__fastcall TForm_opt(TComponent* Owner);

		void __fastcall SetMode(const EditMode mode);
		const EditMode __fastcall GetMode() const;
};
//---------------------------------------------------------------------------
extern PACKAGE TForm_opt *Form_opt;
//---------------------------------------------------------------------------
#endif
