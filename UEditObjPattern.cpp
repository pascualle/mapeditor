//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UEditObjPattern.h"
#include "MainUnit.h"
#include "UObjCode.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFOPEdit *FOPEdit;
//---------------------------------------------------------------------------

__fastcall TFOPEdit::TFOPEdit(TComponent* Owner)
        : TForm(Owner)
{
	showWarning = isError = false;
	mMode = TFOPEMProperties;

	Localization::Localize(this);
}                           
//---------------------------------------------------------------------------

void __fastcall TFOPEdit::Edit_defChange(TObject */*Sender*/)
{
	onChangeSpinEdit(Edit_def, true);
}
//---------------------------------------------------------------------------

void __fastcall TFOPEdit::Ed_nameChange(TObject */*Sender*/)
{
	int i,sel;
	sel = Ed_name->SelStart;
	UnicodeString str = Ed_name->Text.Trim().UpperCase();
	if(str.IsEmpty())
	{
		Ed_name->Text = str;
		return;
	}
	while(str[1]>=L'0'&&str[1]<=L'9')
	{ //������ ������ �� ������ ���� ������
		str.Delete(1,1);
		if(str.IsEmpty())
		{
			Ed_name->Text = str;
			return;
		}
	}
	for(i=1;i<=str.Length();i++)
	{    //���������� �������
		if((str[i]>=L'A'&&str[i]<=L'Z') ||
			(str[i]>=L'0'&&str[i]<=L'9')||
			str[i]==L'_') continue;
		else {str.Delete(i,1);if(i>=sel)sel--;}
	}
	Ed_name->Text=str;
	if(sel<=str.Length())
	{
		if(sel<0)sel=0;
		Ed_name->SelStart=sel;
		Ed_name->SelLength=0;
	}
}
//---------------------------------------------------------------------------
void __fastcall TFOPEdit::ButtonOkClick(TObject */*Sender*/)
{
	showWarning=true;
	showWarning=false;
	if(isError)ModalResult=mrNone;
	else ModalResult=mrOk;
	isError=false;
}
//---------------------------------------------------------------------------
void __fastcall TFOPEdit::SetMode(TFOPEditMode mode)
{
	mMode = mode;
	switch(mMode)
	{
		case TFOPEMProperties:
			ButtonOk->OnClick = ButtonOkClick;
			Label_def->Visible = true;
			Edit_def->Visible = true;
			Labelinfo->Visible = true;
			Label1->Caption = Localization::Text(_T("mNameVariableLatinOnly"));
			EditPrefix->Text = _T("PRP_");
		break;
		case TFOPEMAnimations:
			ButtonOk->OnClick = NULL;
			Label_def->Visible = false;
			Edit_def->Visible = false;
			Labelinfo->Visible = false;
			Label1->Caption = Localization::Text(_T("mNamelatinOnly"));
			EditPrefix->Text = _T("ANIM_");
		break;
		case TFOPEMEvents:
			ButtonOk->OnClick = NULL;
			Label_def->Visible = false;
			Edit_def->Visible = false;
			Labelinfo->Visible = false;
			Label1->Caption = Localization::Text(_T("mNamelatinOnly"));
			EditPrefix->Text = _T("EVENT_");
		break;
		case TFOPEMLanguages:
			FOPEdit->ButtonOk->OnClick = NULL;
			FOPEdit->Label_def->Visible = false;
			FOPEdit->Edit_def->Visible = false;
			FOPEdit->Labelinfo->Visible = false;
			FOPEdit->Label1->Caption = Localization::Text(_T("mNameLanguageLatinOnly"));
			EditPrefix->Text = _T("");
			EditPrefix->Visible = false;
			Ed_name->Left = EditPrefix->Left + EditPrefix->Width / 2;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFOPEdit::SetVarName(UnicodeString txt)
{
	if(!EditPrefix->Text.IsEmpty())
	{
		const int pos = txt.Pos(EditPrefix->Text);
		if(pos > 0)
		{
			txt.Delete(pos, EditPrefix->Text.Length());
		}
	}
	Ed_name->Text = txt;
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TFOPEdit::GetVarName()
{
	return EditPrefix->Text + Ed_name->Text.Trim();
}
//---------------------------------------------------------------------------

