//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("WizMapEditor.res");
USEFORM("MainUnit.cpp", Form_m);
USEFORM("UnitOpt.cpp", Form_opt);
USEUNIT("png\pngread.c");
USEUNIT("png\png.c");
USEUNIT("png\pngerror.c");
USEUNIT("png\pngget.c");
USEUNIT("png\pngmem.c");
USEUNIT("png\pngpread.c");
USEUNIT("png\pngrio.c");
USEUNIT("png\pngrtran.c");
USEUNIT("png\pngrutil.c");
USEUNIT("png\pngset.c");
USEUNIT("png\pngtrans.c");
USEUNIT("png\pngwio.c");
USEUNIT("png\pngwrite.c");
USEUNIT("png\pngwtran.c");
USEUNIT("png\pngwutil.c");
USEUNIT("png\zlib\CRC32.C");
USEUNIT("png\zlib\INFUTIL.C");
USEUNIT("png\zlib\INFCODES.C");
USEUNIT("png\zlib\INFFAST.C");
USEUNIT("png\zlib\INFLATE.C");
USEUNIT("png\zlib\INFTREES.C");
USEUNIT("png\zlib\INFBLOCK.C");
USEUNIT("png\zlib\ADLER32.C");
USEUNIT("png\zlib\COMPRESS.C");
USEUNIT("png\zlib\DEFLATE.C");
USEUNIT("png\zlib\GZIO.C");
USEUNIT("png\zlib\TREES.C");
USEUNIT("png\zlib\UNCOMPR.C");
USEUNIT("png\zlib\ZUTIL.C");
USEFORM("Unit_chres.cpp", Form_res);
USEFORM("UEditObj.cpp", FEditGrObj);
USEFORM("Unit2.cpp", Form2);
USEFORM("UPath.cpp", FPath);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Wizards map editor";
                 Application->CreateForm(__classid(TForm_m), &Form_m);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
