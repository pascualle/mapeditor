//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "USndObjPropertyEdit.h"
#include "UObjCode.h"
#include "MainUnit.h"
#include "UFileSelect.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
TFSOPEdit *FSOPEdit;

//---------------------------------------------------------------------------

__fastcall TFSOPEdit::TFSOPEdit(TComponent* Owner)
	: TForm(Owner)
{
	Localization::Localize(this);
}
//---------------------------------------------------------------------------
void __fastcall TFSOPEdit::Ed_nameChange(TObject *Sender)
{
	Edit_nameChangeMain(static_cast<TEdit*>(Sender));
}
//---------------------------------------------------------------------------

void __fastcall TFSOPEdit::SetName(UnicodeString txt)
{
	if(!EditPrefix->Text.IsEmpty())
	{
		int pos = txt.Pos(EditPrefix->Text);
		if(pos > 0)
		{
			txt.Delete(pos, EditPrefix->Text.Length());
		}
	}
	Ed_name->Text = txt;
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TFSOPEdit::GetName()
{
	return EditPrefix->Text + Ed_name->Text.Trim();
}
//---------------------------------------------------------------------------

void __fastcall TFSOPEdit::BtG_3dotClick(TObject *)
{
	UnicodeString path;
	UnicodeString messageWndCaption = Localization::Text(_T("mResPathWindow"));
	do
	{
		path = Form_m->PathPngRes;
		if (!path.IsEmpty())
			if (!DirectoryExists(Form_m->PathPngRes))
				path = _T("");
		if (path.IsEmpty())
		{
			if (Application->MessageBox(Localization::Text(_T("mAskResPath")).c_str(),
					messageWndCaption.c_str(), MB_YESNO | MB_ICONQUESTION) == ID_YES)
			{
				Form_m->ActPathExecute(NULL);
			}
			else
			{
				return;
			}
		}
	}
	while (path.IsEmpty());
	Application->CreateForm(__classid(TFFileSelect), &FFileSelect);
	FFileSelect->Caption = Label_namefile->Caption;
	UnicodeString ext = _T("*.xxx");
	FFileSelect->FileListBox->Mask = forceCorrectSoundFileExtention(ext);
	FFileSelect->FileListBox->Directory = Form_m->PathPngRes;
	if(FFileSelect->ShowModal() == mrOk)
	{
		if(!FFileSelect->FileListBox->FileName.IsEmpty())
		{
			Edit_filename->Text = ExtractFileName(FFileSelect->FileListBox->FileName);
        }
	}
	FFileSelect->Free();
	FFileSelect = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFSOPEdit::ButtonOkClick(TObject *)
{
	Edit_filename->Text = forceCorrectSoundFileExtention(Edit_filename->Text);

	if(Ed_name->Text.IsEmpty())
	{
		Ed_name->SetFocus();
		return;
    }
	if(Edit_filename->Text.IsEmpty())
	{
		Edit_filename->SetFocus();
		return;
	}
	ModalResult = mrOk;
}
//---------------------------------------------------------------------------

