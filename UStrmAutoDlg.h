//----------------------------------------------------------------------------
#ifndef UStrmAutoDlgH
#define UStrmAutoDlgH
//----------------------------------------------------------------------------
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <StdCtrls.hpp>
#include <Controls.hpp>
#include <Forms.hpp>
#include <Graphics.hpp>
#include <Classes.hpp>
#include <SysUtils.hpp>
#include <Windows.hpp>
#include <System.hpp>
//----------------------------------------------------------------------------
class TFStrmAutoDlg : public TForm
{
 __published:
	TRadioGroup *RadioGroupFrames;
	TRadioGroup *RadioGroupDirection;
	TCheckBox *CheckBox1;
	TEdit *Edit_time;
	TButton *ButtonCancel;
	TButton *ButtonOk;
	void __fastcall CheckBox1Click(TObject *Sender);

 private:

	void __fastcall CheckEnabledTime();

 public:
	virtual __fastcall TFStrmAutoDlg(TComponent* AOwner);
};
//----------------------------------------------------------------------------
extern PACKAGE TFStrmAutoDlg *FStrmAutoDlg;
//----------------------------------------------------------------------------
#endif    
