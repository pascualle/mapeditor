//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UDirector.h"
#include "MainUnit.h"
#include "UObjManager.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TFDirectorProperty *FDirectorProperty;

//---------------------------------------------------------------------------
__fastcall TFDirectorProperty::TFDirectorProperty(TComponent* Owner)
        : TPrpFunctions(Owner)
{
	CSEd_x = new TSpinEdit(GroupBoxPos);
	CSEd_x->Parent = GroupBoxPos;
	CSEd_x->Left = 18;
	CSEd_x->Top = 19;
	CSEd_x->Width = 55;
	CSEd_x->Height = 22;
	CSEd_x->TabOrder = 0;
	CSEd_x->OnChange = CSEd_xChange;
	CSEd_x->OnEnter = CSEd_xEnter;
	CSEd_x->OnExit = CSEd_xExit;
	CSEd_x->Visible = true;
	CSEd_y = new TSpinEdit(GroupBoxPos);
	CSEd_y->Parent = GroupBoxPos;
	CSEd_y->Left = 90;
	CSEd_y->Top = 19;
	CSEd_y->Width = 53;
	CSEd_y->Height = 22;
	CSEd_y->TabOrder = 1;
	CSEd_y->OnChange = CSEd_xChange;
	CSEd_y->OnEnter = CSEd_xEnter;
	CSEd_y->OnExit = CSEd_xExit;
	CSEd_y->Visible = true;

	t_MapGP = NULL;
	disableChanges = true;
	mOldName = _T("");
	mOldValue = _T("");
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::Localize()
{
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::Init(TCommonResObjManager *m)
{
    (void)m;
	if (t_MapGP != nullptr)
	{
		AssignDirector(t_MapGP);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::AssignDirector(TMapDirector* in)
{
  int i;
  double x, y;

  if(t_MapGP == in)return;

  if(t_MapGP != NULL)
  {
	 if(Visible)
	 {
	   //FakeExitActiveControl();
		mOldValue = _T("");
		SaveAllValues(this);
	 }
	 t_MapGP->UnregisterPrpWnd();
  }

  t_MapGP = in;
  if(t_MapGP != NULL)
  {
	 t_MapGP->RegisterPrpWnd(this);
  }
  else
  {
	 return;
  }

  Caption = Localization::Text(_T("mGameObjectProperties")) + _T(" [") + t_MapGP->getGParentName() + _T("]");

  disableChanges = true;

  in->getCoordinates(&x, &y);
  CSEd_x->Value = (int)x;
  CSEd_y->Value = (int)y;
  Edit_name->Text = t_MapGP->Name;
  Memo->Text = t_MapGP->getRemarks();
  mNameObj = t_MapGP->Name;
  mOldName = t_MapGP->Name;

  refreshNodes();
  refreshTriggers();
  refreshEvents();

  Lock(t_MapGP->IsLocked());

  disableChanges = false;

  if(Visible)
	FakeEnterActiveControl();
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::Bt_find3Click(TObject *Sender)
{
  TComboBox *cb;
  TBitBtn   *bt;
  bt = (TBitBtn *)Sender;
  if(bt == Bt_find0)cb = CB_Up;
  if(bt == Bt_find1)cb = CB_Left;
  if(bt == Bt_find2)cb = CB_Right;
  if(bt == Bt_find3)cb = CB_Down;
  if(bt == Bt_find4)cb = CB_Trigger;
  if(bt == Bt_find_obj)
  {
	Form_m->FindAndShowObjectByName(mNameObj, false);
	return;
  }

  if(cb->ItemIndex <= 0)
  {
	return;
  }
  Form_m->FindAndShowObjectByName(cb->Text, false);
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::SetRemarks(UnicodeString str)
{
  if(!Visible)return;
  Memo->Text = str;
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::SetTransform(const TTransformProperties& transform)
{
	if(!Visible)
	{
		return;
	}
	double x, y;
	transform.GetPosition(x, y);
	CSEd_x->Value = static_cast<int>(x);
	CSEd_y->Value = static_cast<int>(y);
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::SetNodeU(UnicodeString NodeName)
{
  if(!Visible)return;
  int i = CB_Up->Items->IndexOf(NodeName);
  if(i < 0)i = 0;
  CB_Up->ItemIndex = i;
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::SetNodeD(UnicodeString NodeName)
{
  if(!Visible)return;
  int i = CB_Down->Items->IndexOf(NodeName);
  if(i < 0)i = 0;
  CB_Down->ItemIndex = i;
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::SetNodeL(UnicodeString NodeName)
{
  if(!Visible)return;
  int i = CB_Left->Items->IndexOf(NodeName);
  if(i < 0)i = 0;
  CB_Left->ItemIndex = i;
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::SetNodeR(UnicodeString NodeName)
{
  if(!Visible)return;
  int i = CB_Right->Items->IndexOf(NodeName);
  if(i < 0)i = 0;
  CB_Right->ItemIndex = i;
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::SetTrigger(UnicodeString triggerName)
{
  if(!Visible)return;
  int i =  CB_Trigger->Items->IndexOf(triggerName);
  if(i < 0)i = 0;
  CB_Trigger->ItemIndex = i;
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::refreshNodes()
{
 int len,i;
 len = Form_m->m_pObjManager->GetObjTypeCount(TObjManager::DIRECTOR);
 CB_Up->Items->Clear();
 CB_Down->Items->Clear();
 CB_Left->Items->Clear();
 CB_Right->Items->Clear();
 for(i=0; i<len; i++)
 {
   if(t_MapGP == Form_m->m_pObjManager->GetObjByIndex(TObjManager::DIRECTOR, i))
   {
		continue;
   }
   CB_Up->Items->Add(((TMapDirector*)Form_m->m_pObjManager->GetObjByIndex(TObjManager::DIRECTOR, i))->Name);
 }
 CB_Up->Items->Insert(0, TEXT_STR_NONE);
 CB_Down->Items->Assign(CB_Up->Items);
 CB_Left->Items->Assign(CB_Up->Items);
 CB_Right->Items->Assign(CB_Up->Items);

 CB_Up->Text = TEXT_STR_NONE;
 CB_Left->Text = TEXT_STR_NONE;
 CB_Right->Text = TEXT_STR_NONE;
 CB_Down->Text = TEXT_STR_NONE;

 if(t_MapGP != NULL)
 {
	i = CB_Up->Items->IndexOf( t_MapGP->nearNodes[Movement::dirUP/2 - 1] );
	if(i < 0)i = 0;
	CB_Up->ItemIndex = i;
	i = CB_Left->Items->IndexOf( t_MapGP->nearNodes[Movement::dirLEFT/2 - 1]);
	if(i < 0)i = 0;
	CB_Left->ItemIndex = i;
	i = CB_Right->Items->IndexOf( t_MapGP->nearNodes[Movement::dirRIGHT/2 - 1]);
	if(i < 0)i = 0;
	CB_Right->ItemIndex = i;
	i = CB_Down->Items->IndexOf( t_MapGP->nearNodes[Movement::dirDOWN/2 - 1]);
	if(i < 0)i = 0;
	CB_Down->ItemIndex = i;
 }
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::refreshTriggers()
{
  int i;
  int len = Form_m->m_pObjManager->GetObjTypeCount(TObjManager::TRIGGER);
  CB_Trigger->Clear();
  for(i=0; i<len; i++)
  {
	CB_Trigger->Items->Add(((TMapTrigger*)Form_m->m_pObjManager->GetObjByIndex(TObjManager::TRIGGER, i))->Name);
  }
  CB_Trigger->Items->Insert(0, TEXT_STR_NONE);
  CB_Trigger->Text = TEXT_STR_NONE;
  if(t_MapGP != NULL)
  {
	i = CB_Trigger->Items->IndexOf(t_MapGP->nodeTrigger);
	if(i < 0)i = 0;
	CB_Trigger->ItemIndex = i;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::refreshEvents()
{
	ComboBox_Event->Clear();
	ComboBox_Event->Items->Insert(0, TEXT_STR_NONE);
	ComboBox_Event->Text = TEXT_STR_NONE;
	if(t_MapGP != NULL)
	{
		ComboBox_Event->Items->AddStrings(Form_m->m_pObjManager->commonRes->eventNameTypes);
		int i = ComboBox_Event->Items->IndexOf(t_MapGP->objTeleport);
		if(i < 0)i = 0;
		ComboBox_Event->ItemIndex = i;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::FormHide(TObject */*Sender*/)
{
	mOldValue = _T("");
	SaveAllValues(this);
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::FormShow(TObject */*Sender*/)
{
  AssignDirector(t_MapGP);
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::SaveAllValues(TWinControl *iParent)
{
  if(iParent == NULL)
	return;

  TControl *c;
  for(int i = 0; i < iParent->ControlCount; i++)
  {
	c = iParent->Controls[i];
	if(c == NULL)continue;

	if( c->ClassNameIs(_T("TPanel")) ||
		c->ClassNameIs(_T("TGroupBox")) ||
		c->ClassNameIs(_T("TTabSheet")) ||
		c->ClassNameIs(_T("TPageControl")) ||
		c->ClassNameIs(_T("TScrollBox")) ||
		c->ClassNameIs(_T("TForm")) )
	{
		SaveAllValues((TWinControl*)c);
	}
	else
	{
		if(c->ClassNameIs(_T("TSpinEdit")))
		{
			if(((TSpinEdit*)c)->OnExit != NULL)
				((TSpinEdit*)c)->OnExit(c);
		}
		else
			if(c->ClassNameIs(_T("TEdit")))
		{
			if(((TEdit*)c)->OnExit != NULL)
				((TEdit*)c)->OnExit(c);
		}
		else
		if(c->ClassNameIs(_T("TMemo")))
		{
			if(((TMemo*)c)->OnExit != NULL)
				((TMemo*)c)->OnExit(c);
		}
		else
		if(c->ClassNameIs(_T("TComboBox")))
		{
			if(((TComboBox*)c)->OnExit != NULL)
				((TComboBox*)c)->OnExit(c);
		}
	}
  }
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::FakeEnterActiveControl()
{
   TWinControl* c = ActiveControl;
   if(c == NULL)return;
   if(c->ClassNameIs(_T("TSpinEdit")))
   {
	 ((TSpinEdit*)c)->OnEnter(c);
	 return;
   }
   if(c->ClassNameIs(_T("TEdit")))
   {
	 ((TEdit*)c)->OnEnter(c);
	 return;
   }
   if(c->ClassNameIs(_T("TMemo")))
   {
	 ((TMemo*)c)->OnEnter(c);
	 return;
   }
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::Edit_nameChange(TObject */*Sender*/)
{
   Edit_nameChangeMain(Edit_name);
}
//---------------------------------------------------------------------------
void __fastcall TFDirectorProperty::Edit_nameEnter(TObject */*Sender*/)
{
   mOldName = Edit_name->Text;
}
//---------------------------------------------------------------------------
void __fastcall TFDirectorProperty::Edit_nameExit(TObject */*Sender*/)
{
	if(t_MapGP == NULL)
	{
		return;
	}

	UnicodeString str = Edit_name->Text.Trim();
	if(str.Compare(mOldName) == 0)
	{
		return;
	}

	if(str.IsEmpty())
	{
		Application->MessageBox(Localization::Text(_T("mObjNameError")).c_str(),
		Localization::Text(_T("mError")).c_str(),
							MB_OK | MB_ICONERROR);
		Edit_name->Text = mOldName;
		return;
	}
	if(Form_m->m_pObjManager->CheckIfMGONameExists(str, t_MapGP) == true)
	{
		Application->MessageBox(Localization::Text(_T("mObjNameExist")).c_str(),
								Localization::Text(_T("mError")).c_str(),
								MB_OK | MB_ICONERROR);
		Edit_name->Text = mOldName;
		return;
	}
	if(!disableChanges && mOldName.Compare(str) != 0)
	{
		t_MapGP->Name = str;
		mNameObj = str;
		Form_m->OnChangeDirectorName(t_MapGP, mOldName);
		t_MapGP->changeHint();
		mOldName = str;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::CB_UpExit(TObject *Sender)
{
  TComboBox *cb = (TComboBox *)Sender;
  if(cb->Items->IndexOf(cb->Text.Trim()) < 0)
  {
	cb->Text = TEXT_STR_NONE;
  }
  UnicodeString val = _T("");
  if(!disableChanges)
	if(t_MapGP != NULL)
	 {
		if(cb == CB_Up)
		{
			if(cb->ItemIndex > 0)
			{
				val = cb->Text;
			}
			t_MapGP->nearNodes[Movement::dirUP/2 - 1] = val;
		}
		else
		if(cb == CB_Down)
		{
			if(cb->ItemIndex > 0)
			{
				val = cb->Text;
			}
			t_MapGP->nearNodes[Movement::dirDOWN/2 - 1] = val;
		}
		else
		if(cb == CB_Left)
		{
			if(cb->ItemIndex > 0)
			{
				val = cb->Text;
			}
			t_MapGP->nearNodes[Movement::dirLEFT/2 - 1] = val;
		}
		else
		if(cb == CB_Right)
		{
			if(cb->ItemIndex > 0)
			{
				val = cb->Text;
			}
			t_MapGP->nearNodes[Movement::dirRIGHT/2 - 1] = val;
		}

		Form_m->OnChangeDirectorNode(t_MapGP);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::CB_UpKeyPress(TObject *Sender, char &Key)
{
	if(Key == VK_RETURN)
	{
		TComboBox *cb = (TComboBox *)Sender;
		cb->OnExit(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::CB_TriggerExit(TObject *Sender)
{
  TComboBox *cb = (TComboBox *)Sender;
  if(cb->Items->IndexOf(cb->Text.Trim()) < 0)
  {
  	cb->Text = TEXT_STR_NONE;
  }

  if(!disableChanges)
	if(t_MapGP != NULL)
	{
	  if(CB_Trigger->ItemIndex > 0)
	  {
		t_MapGP->nodeTrigger = CB_Trigger->Text;
	  }
	  else
	  {
		t_MapGP->nodeTrigger = _T("");
	  }
	}
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::ComboBox_EventExit(TObject *Sender)
{
  TComboBox *cb = (TComboBox *)Sender;
  if(cb->Items->IndexOf(cb->Text.Trim()) < 0)
  {
	cb->Text = TEXT_STR_NONE;
  }

  if(!disableChanges)
	if(t_MapGP != NULL)
	{
	  if(cb->ItemIndex > 0)
	  {
		t_MapGP->objTeleport = cb->Text;
	  }
	  else
	  {
		t_MapGP->objTeleport = _T("");
	  }
	}
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::CSEd_xChange(TObject *Sender)
{
  if(((TSpinEdit *)Sender)->Text.IsEmpty())
  {
	return;
  }
  try
  {
	StrToInt(((TSpinEdit *)Sender)->Text);
  }
  catch(...)
  {
	return;
  }

  if(!disableChanges)
	if(t_MapGP != NULL)
	  Form_m->OnChangeObjPosition(t_MapGP, CSEd_x->Value, CSEd_y->Value);
}
//---------------------------------------------------------------------------
void __fastcall TFDirectorProperty::CSEd_xEnter(TObject *Sender)
{
  mOldValue = ((TSpinEdit *)Sender)->Text;
}
//---------------------------------------------------------------------------
void __fastcall TFDirectorProperty::CSEd_xExit(TObject *Sender)
{
  UnicodeString str = ((TSpinEdit *)Sender)->Text.Trim();

  if(mOldValue.IsEmpty())
	return;

  if(mOldValue.Compare(str) != 0)
   if(!disableChanges)
	 if(t_MapGP != NULL)
	   Form_m->OnChangeObjPosition(t_MapGP, CSEd_x->Value, CSEd_y->Value);
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::MemoEnter(TObject */*Sender*/)
{
   mOldValue = Memo->Text;
}
//---------------------------------------------------------------------------
void __fastcall TFDirectorProperty::MemoExit(TObject */*Sender*/)
{
  if(!disableChanges)
  if(t_MapGP != NULL)
  {
	 UnicodeString str = Memo->Text.Trim();
	 if(mOldValue.Compare(str) != 0)
	 {
	   t_MapGP->setRemarks(str);
	   Form_m->OnChangeObjParameter();
	 }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::FormDestroy(TObject *)
{
	delete CSEd_y;
	delete CSEd_x;
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::Reset()
{
	AssignDirector(nullptr);
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::EmulatorMode(bool mode)
{
	(void)mode;
}
//---------------------------------------------------------------------------

void __fastcall TFDirectorProperty::Lock(bool value)
{
	ImageLock->Visible = value;
}
//---------------------------------------------------------------------------

