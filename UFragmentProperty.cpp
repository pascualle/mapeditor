//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainUnit.h"
#include "UFragmentProperty.h"
#include "UBGOType.h"
#include "UObjCode.h"
#include "UEditObj.h"
#include "UObjManager.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"

TFBOProperty *FBOProperty;

//---------------------------------------------------------------------------

__fastcall TFBOProperty::TFBOProperty(TComponent* Owner) : TForm(Owner)
{
	int                    i;
	TBGobjProperties       *prp;

	tGMapBGObj = new GMapBGObj();
	ImageList->Width = Form_m->getBG_W();
	ImageList->Height = Form_m->getBG_H();
	mpCurrentObj = NULL;
	readOnly = false;

	CB_Type->Clear();
	CB_Type->Items->Add(UnicodeString());
	for(i = 0; i < Form_m->mainCommonRes->bgotypes->Count; i++)
	{
		prp =(TBGobjProperties *)Form_m->mainCommonRes->bgotypes->Items[i];
		CB_Type->Items->Add(prp->name);
	}
	CB_Type->ItemIndex = -1;

	Localization::Localize(ActionList);
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::FormDestroy(TObject *)
{
	delete tGMapBGObj;
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::setReadOnly(bool val)
{
   Memo->Enabled = !val;
   CB_Type->Enabled = !val;
   ButtonOk->Enabled = !val;
   readOnly = val;
   if(readOnly)
   {
	  ListView->PopupMenu = NULL;
   }
   else
   {
	  ListView->PopupMenu = PopupMenu;
   }
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::ListViewKeyDown(TObject */*Sender*/, WORD &Key,
	  TShiftState Shift)
{
  if(Shift.Contains(ssShift))
  {
	if(Key==VK_LEFT){Key=0;ActShiftLeft->Execute();}
	else
	if(Key==VK_RIGHT){Key=0;ActShiftRight->Execute();}
	return;
  }
  if(Key==VK_RETURN){Key=0;ActEdit->Execute();}
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::assignBGobj(GMapBGObj *BGobj)
{
	bool res;

	assert(BGobj);

	Memo->Clear();
	mpCurrentObj = BGobj;
	BGobj->CopyTo(tGMapBGObj);
	refreshImg();
	res = false;

	Memo->Text = tGMapBGObj->Memo;
	if(tGMapBGObj->GetFrameObjCount() > 0)
	{
		TFrameObj *currBGobj = tGMapBGObj->GetFrameObj(0);
		if(currBGobj != NULL)
		{
			int idx = CB_Type->Items->IndexOf(currBGobj->bgPropertyName);
			if(idx >= 0 && idx != NONE_BGTYPE)
			{
				CB_Type->ItemIndex = idx;
			}
			res = true;
		}
	}
	if(!res)
	{
	   CB_Type->Enabled = false;
	   Bt_3dot->Enabled = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::applyChangesToBGobj()
{
	if(mpCurrentObj == NULL)
	{
		return;
	}
	tGMapBGObj->Memo = Memo->Text;
	CB_TypeExit(NULL);
	tGMapBGObj->CopyTo(mpCurrentObj);
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::refreshImg()
{
	int p_w,p_h;
	BMPImage ibmp;
	TFrameObj* BGobj;
	TListItem* li;
	UnicodeString caption;

	ImageList->Clear();
	ListView->Items->Clear();
	p_w = ImageList->Width;
	p_h = ImageList->Height;
	ImageList->AllocBy = tGMapBGObj->GetFrameObjCount();

	for(int i = 0; i < tGMapBGObj->GetFrameObjCount(); i++)
	{
		BGobj = tGMapBGObj->GetFrameObj(i);
		if(BGobj != NULL && BGobj->getResIdx() >= 0)
		{
			BLENDFUNCTION bf;
			bf.BlendOp = AC_SRC_OVER;
			bf.BlendFlags = 0;
			bf.SourceConstantAlpha = 255;
			bf.AlphaFormat = AC_SRC_ALPHA;
			Graphics::TBitmap *dbmp = const_cast<Graphics::TBitmap*>(Form_m->mainCommonRes->GetGraphicResource(BGobj->getResIdx())->GetBitmapData());
			assert(dbmp);
			ibmp.Assign(new Graphics::TBitmap());
			ibmp.SetSize(BGobj->w, BGobj->h);
			ibmp.Clear(TColor(0x00000000));
			::AlphaBlend(ibmp.Canvas->Handle, 0, 0, BGobj->w, BGobj->h,
							dbmp->Canvas->Handle, BGobj->x, BGobj->y, BGobj->w, BGobj->h, bf);
		}
		else
		{
			ibmp.Assign(static_cast<Graphics::TBitmap*>(NULL));
        }
		caption = IntToStr(i);
		if(ibmp.IsEmpty())
		{
			Form_m->makeEmptyPreview(&ibmp, caption, p_w, p_h); //���� ��� �� �����������
		}
		else
		{
			if(ibmp.Width != p_w || ibmp.Height != p_h)
			{
				Form_m->resizeBmpForListView(&ibmp, p_w, p_h, gsGlobalScale);
			}
		}
		ImageList->Add(const_cast<Graphics::TBitmap*>(ibmp.GetBitmapData()), NULL);

		li = ListView->Items->Add();
		li->Caption = caption;
		li->ImageIndex = i;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::GrResEdit(TListItem* li,int /*mode*/)
{
	TFEditGrObj* ed = Form_m->getSelectionBlock(Form_m->mainCommonRes, false);
	if(ed != NULL)
	{
		if(Form_m->mpGrBGWndParams != NULL)
		{
			ed->SetWndParams(Form_m->mpGrBGWndParams);
		}
		int idx = li == NULL ? -1: li->ImageIndex;
		ed->SetBGObj(tGMapBGObj, idx, Form_m->getBG_W(), Form_m->getBG_H());
		if(ed->ShowModal() == mrOk)
		{
			ed->ApplyChangesToBGObj();
			refreshImg();
		}
		if(Form_m->mpGrBGWndParams == NULL)
		{
			Form_m->mpGrBGWndParams = new TEGrWndParams();
		}
		ed->GetWndParams(Form_m->mpGrBGWndParams); //���������� ��������� ����
		ed->Free();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::ActNewExecute(TObject */*Sender*/)
{
	GrResEdit(NULL,mAdd);
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::ActEditExecute(TObject */*Sender*/)
{
	TListItem* li = ListView->Selected;
	GrResEdit(li, mEdit);
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::ActDelExecute(TObject */*Sender*/)
{
 TListItem* li=ListView->Selected;
 GrResEdit(li,mDel);
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::ActShiftRightExecute(TObject */*Sender*/)
{
  if(readOnly)return;

 TListItem* li;
 int idx;
 li=ListView->Selected;
 if(li==NULL)return;
 idx=li->ImageIndex;
 if(tGMapBGObj->MoveFrameObj(idx, idx + 1))
 {
  refreshImg();
  li=ListView->Items->Item[idx+1];
 }
 ListView->Selected=li;
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::ActShiftLeftExecute(TObject */*Sender*/)
{
 if(readOnly)return;

 TListItem* li;
 int idx;
 li=ListView->Selected;
 if(li==NULL)return;
 idx=li->ImageIndex;
 if(tGMapBGObj->MoveFrameObj(idx, idx - 1))
 {
  refreshImg();
  li=ListView->Items->Item[idx-1];
 }
 ListView->Selected=li;
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::ListViewDblClick(TObject */*Sender*/)
{
 ActEdit->Execute();	
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::ListViewEnter(TObject */*Sender*/)
{
	ListView->Color = clWindow;
	TListItem* li;
	bool act;
	li = ListView->Selected;
	act = li != NULL;
	ActEdit->Enabled = act;
	ActShiftRight->Enabled = act;
	ActShiftLeft->Enabled = act;
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::ListViewExit(TObject */*Sender*/)
{
	ListView->Color = cl3DLight;
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::MemoExit(TObject */*Sender*/)
{
  tGMapBGObj->Memo=Memo->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::ShowTypePreview(UnicodeString name)
{
 int			i;
 TBGobjProperties 	*prp;
 bool			res;
 res = false;
 for(i = 0; i < Form_m->mainCommonRes->bgotypes->Count; i++)
 {
	prp =(TBGobjProperties *)Form_m->mainCommonRes->bgotypes->Items[i];
  	if(prp->name == name)
        {
          res = true;
          break;
        }
 }
 if(res)
 {
 	Application->CreateForm(__classid(TFEditBGOType), &FEditBGOType);
 	FEditBGOType->SetBackProperty(prp);//������������ � ������� ��������
 	FEditBGOType->CheckBoxSolid->Enabled = false;
 	FEditBGOType->CSpinEditType->Enabled = false;
 	FEditBGOType->Edit_name->Enabled = false;
	FEditBGOType->CheckBoxDirector->Enabled = false;
 	FEditBGOType->ButtonOk->Enabled = false;
 	FEditBGOType->ShowModal();
 	FEditBGOType->Free();
 	FEditBGOType = NULL;
 }
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::Bt_3dotClick(TObject */*Sender*/)
{
	if(!CB_Type->Text.IsEmpty())
	{
		ShowTypePreview(CB_Type->Text);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::CB_TypeExit(TObject */*Sender*/)
{
	TFrameObj *currBGobj = tGMapBGObj->GetFrameObj(0);
	if(currBGobj != NULL)
	{
		if(CB_Type->Text.IsEmpty())
		{
			currBGobj->bgPropertyName = _T("");
		}
		else
		{
			bool find = false;
			for(int i = 0; i < Form_m->mainCommonRes->bgotypes->Count; i++)
			{
				TBGobjProperties *prp = (TBGobjProperties *)Form_m->mainCommonRes->bgotypes->Items[i];
				if(prp->name.Compare(CB_Type->Text) == 0)
				{
					currBGobj->bgPropertyName = prp->name;
					find = true;
					break;
				}
			}
			if(find == false)
			{
				currBGobj->bgPropertyName = _T("");
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFBOProperty::ListViewClick(TObject*)
{
	ListViewEnter(NULL);
}
//---------------------------------------------------------------------------

