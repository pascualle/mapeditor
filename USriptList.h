//---------------------------------------------------------------------------

#ifndef USriptListH
#define USriptListH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include "UScript.h"
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------

class TObjManager;

class TFScriptList : public TForm
{
__published:
	TComboBox *CB_zones;
	TListBox *ListBox;
	TBevel *Bevel3;
	TBitBtn *Bt_add;
	TButton *ButtonOk;
	TButton *ButtonCancel;
	TBitBtn *Bt_edit;
	TPanel *Panel1;
	TBitBtn *Bt_start;
	TStatusBar *StatusBar;
	TBitBtn *Bt_del;
	TEdit *EdFind;
	TBitBtn *BtCrearFilter;
	TLabel *Label1;
	TLabel *Label2;
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall ListBoxDblClick(TObject *Sender);
	void __fastcall Bt_addClick(TObject *Sender);
	void __fastcall Bt_editClick(TObject *Sender);
	void __fastcall Bt_delClick(TObject *Sender);
	void __fastcall ListBoxKeyPress(TObject *Sender, char &Key);
	void __fastcall CB_zonesChange(TObject *Sender);
	void __fastcall Bt_startClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall ListBoxClick(TObject *Sender);
	void __fastcall BtCrearFilterClick(TObject *Sender);
	void __fastcall EdFindChange(TObject *Sender);

private:

	TList			*sripts;
	TList			*items;
	TStringList     *changeList;

	void __fastcall CreateList();
	void __fastcall IndexOf(TGScript* spt);
	void __fastcall AddEditDel(int mode);
	int  __fastcall GetScript(int zone, UnicodeString name, TGScript** oscript);

public:

    void __fastcall SetZone(int zone);
    void __fastcall SetEmulatorMode();
	void __fastcall SetScripts(const TObjManager* objManager);
	void __fastcall GetScripts(TObjManager* objManager, TStringList *changelist);
	__fastcall TFScriptList(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFScriptList *FScriptList;
//---------------------------------------------------------------------------
#endif

