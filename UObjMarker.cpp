//---------------------------------------------------------------------------

#pragma hdrstop

#include "UObjMarker.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

__fastcall TObjMarker::TObjMarker(TObjMarkerType type, int duration, TMapPerson* obj)
 : mType(type)
 , mDuration(duration)
 , mInitialDuration(duration)
 , mpObj(obj)
{
}
//---------------------------------------------------------------------------

 const TMapPerson* __fastcall TObjMarker::GetContainedObject() const
 {
	return mpObj;
 }
 //---------------------------------------------------------------------------

  const int& __fastcall TObjMarker::GetInitialDuratuion() const
 {
	return mInitialDuration;
 }
//---------------------------------------------------------------------------

 const int& __fastcall TObjMarker::GetCurrentCounter() const
 {
	return mDuration;
 }
//---------------------------------------------------------------------------

 const TObjMarker::TObjMarkerType __fastcall TObjMarker::GetType() const
 {
	return mType;
 }
//---------------------------------------------------------------------------

 bool __fastcall TObjMarker::Tick()
 {
	return --mDuration < 0;
 }
 //---------------------------------------------------------------------------

