//---------------------------------------------------------------------------

#ifndef UnitEditorOptH
#define UnitEditorOptH

#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <StdCtrls.hpp>
#include <CheckLst.hpp>
#include <Vcl.Samples.Spin.hpp>

//---------------------------------------------------------------------------

class BMPImage;

class TFEditorOptions : public TForm
{
__published:

    TButton *ButtonCancel;
    TButton *ButtonOk;
    TPageControl *PageControl;
    TTabSheet *TabSheet1;
    TGroupBox *GroupBoxAnim;
    TLabel *Label2;
    TGroupBox *GroupBoxView;
    TLabel *Label1;
    TTabSheet *TabSheetFMessages;
    TGroupBox *GroupBox1;
    TLabel *Label3;
    TLabel *Label4;
    TLabel *Label11;
    TComboBox *ComboBox;
	TGroupBox *GroupBoxGraphicsResTypes;
    TLabel *Label13;
    TComboBox *ComboBox_GrType;
	TGroupBox *GroupBoxLanguages;
	TButton *Button1;
	TCheckListBox *ListBoxLang;
	TGroupBox *GroupBoxFx32;
	TLabel *Label5;
	TComboBox *ComboBoxFx32;
	TTabSheet *PageBackdrop;
	TGroupBox *GroupBox_b;
	TButton *ButtonBTC;
	TColorDialog *ColorDialog;
	TGroupBox *GroupBoxSoundResTypes;
	TLabel *Label6;
	TComboBox *ComboBox_SndType;
	TLabel *Label7;
	TComboBox *ComboBoxLang;
	TLabel *Label8;
	TComboBox *ComboBoxWchar;
	TButton *ButtonGC;
	TLabel *LabelBTC;
	TLabel *Label9;
	TImage *Image;
    void __fastcall ComboBox_GrTypeChange(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall ListBoxLangClickCheck(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall ButtonBTCClick(TObject *Sender);
	void __fastcall ComboBox_SndTypeChange(TObject *Sender);

private:

	BMPImage *mpBackPatternBmp;
	TColor mCurrentPatternColor;
    TColor mCurrentGridColor;

	void __fastcall PatternPreviewRepaint();

public:

	static const TColor DEF_PATTERN_COLOR;
	static const TColor DEF_GRID_COLOR;

	TSpinEdit *CEd_sz;
	TSpinEdit *CEd_anim_duration;
	TSpinEdit *CSE_dispW;
	TSpinEdit *CSE_dispH;

	bool __fastcall SaveOptions();
	bool __fastcall LoadOptions();
	void __fastcall AssignLanguages(TStringList* list);
	void __fastcall GetLanguages(TStringList* list);
	TColor __fastcall GetPatternColor();
	TColor __fastcall GetGridColor();

    __fastcall TFEditorOptions(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFEditorOptions *FEditorOptions;
//---------------------------------------------------------------------------
#endif
