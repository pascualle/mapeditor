﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UCreateMessages.h"
#include "UnitEditorOpt.h"
#include "UObjCode.h"
#include "UObjManager.h"
#include "MainUnit.h"
#include "texts.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"

TFMessages *FMessages;

//---------------------------------------------------------------------------
__fastcall TFMessages::TFMessages(TComponent* Owner)
	: TForm(Owner)
{
    dispW = dispH = 0;
    currPage = currMessage = -1;
    UnderConstruction = true;
	mpFont = NULL;
	mpCurrText = NULL;
	mpCanvas = new BMPImage(new Graphics::TBitmap());
	::InitText(&mRenderText);
	mRenderText.mpCanvas = mpCanvas;

	Localization::Localize(ActionList1);
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::FormDestroy(TObject *)
{
	::ReleaseText(&mRenderText);
	delete mpCanvas;
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::FormShow(TObject */*Sender*/)
{
    Image->Top = 0;
    Image->Left = 0;
}
//---------------------------------------------------------------------------

bool __fastcall TFMessages::Init(TUnicodeText *ipCurrText)
{
  bool res;
  UnderConstruction = true;
  mpCurrText = ipCurrText;

	CB_Font->Items->Clear();
	Form_m->mpTexts->GetFontNames((TStringList*)CB_Font->Items);
	if(CB_Font->Items->Count > 0)
	{
		int i = CB_Font->Items->IndexOf(ipCurrText->GetFontName());
		if(i >= 0)
		{
			AssignFont(CB_Font->Items->Strings[i]);
			CB_Font->ItemIndex = i;
		}
		else
		{
			AssignFont(_T(""));
			CB_Font->ItemIndex = -1;
		}
	}
	else
	{
		CB_Font->ItemIndex = -1;
	}

	if(ipCurrText->IsEmpty())
	{
		Application->CreateForm(__classid(TFEditorOptions), &FEditorOptions);
		res = FEditorOptions->LoadOptions();
		if(res)
		{
			setDisplaySize(FEditorOptions->CSE_dispW->Value, FEditorOptions->CSE_dispH->Value);
		}
		FEditorOptions->Free();
		FEditorOptions = NULL;
	}
	else
	{
		res = true;
		setDisplaySize(ipCurrText->GetCanvasWidth(), ipCurrText->GetCanvasHeight());
	}

	RefreshAlignment();

	Memo->Clear();
	if(mpCurrText->GetTextData() != NULL)
	{
		for(int j = 0; j < mpCurrText->GetTextData()->mStringsCount - 1; j++)
		{
			Memo->Lines->Add(UnicodeString());
		}
		for(int j = 0; j < mpCurrText->GetTextData()->mStringsCount; j++)
		{
			wchar_t *a = mpCurrText->GetTextData()->mppStrings[j];
			Memo->Lines->Strings[j] = UnicodeString(a);
		}
	}
  UnderConstruction = false;
  MemoChange(NULL);
  return res;
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::ActColorExecute(TObject *)
{
	::TextAttributes attr = mpCurrText->GetAttributes();
	ColorDialog->Color = attr.mCanvasColor;
	if(ColorDialog->Execute())
	{
		attr.mCanvasColor = ColorDialog->Color;
		mpCurrText->SetAttributes(attr);
		mRenderText.mAttributes = attr;
        CreateListView();
        MemoChange(NULL);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::EmptyMessage()
{
	ActNewMessage->Execute();
}
//---------------------------------------------------------------------------

bool __fastcall TFMessages::ViewOnly(int /*id*/)
{
    return true;
}
//---------------------------------------------------------------------------

bool __fastcall TFMessages::AssignFont(UnicodeString ifontname)
{
	mpFont = Form_m->mpTexts->GetFont(ifontname);
	mpCurrText->SetFontName(ifontname);
	if(mpFont)
	{
		mRenderText.mpFont = mpFont->GetData();
	}
	else
	{
		mRenderText.mpFont = NULL;
	}
	CreateListView();
    MemoChange(NULL);
    return mpFont != NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::setDisplaySize(int w, int h)
{
    dispW = w;
    dispH = h;
	Image->Width = dispW;
	Image->Height = dispH;
	mpCurrText->SetCanvasSize(w, h);
	::SetTextCanvas((u16)w, (u16)h, &mRenderText);
	MemoChange(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::MemoChange(TObject */*Sender*/)
{
    if(UnderConstruction)
    {
    	return;
	}
	mRenderText.mAttributes = mpCurrText->GetAttributes();
	if(Memo->Text.IsEmpty())
	{
		wchar_t a[1];
		a[0] = L'\0';
		mpCurrText->SetText(a);
		::SetText(a, &mRenderText);
	}
	else
	{
		mpCurrText->SetText(Memo->Text.c_str());
		::SetText(Memo->Text.c_str(), &mRenderText);
	}
	if(mpFont == NULL)
	{
		::TextAttributes attr = mpCurrText->GetAttributes();
		Image->Picture->Assign(NULL);
		Image->Canvas->Brush->Color = attr.mCanvasColor;
		Image->Canvas->FillRect(Image->ClientRect);
	}
	else
	{
		Image->Picture->Assign(const_cast<Graphics::TBitmap*>(mpCanvas->GetBitmapData()));
    }
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::CreateListView()
{
    int i, len, x, y, w, h, add_h;
    Graphics::TBitmap *ibmp;

    TButtonItem *bi;
    TButtonCollection *bc = Alphabet->Categories->Items[0]->Items;

    ImageList->Clear();
    ImageList->Masked = false;
	bc->Clear();

	if(mpFont == NULL)
    {
		return;
    }

    add_h = 0;
    ibmp = new Graphics::TBitmap;
    ibmp->Width = mpFont->GetWidth();
    ibmp->Height = mpFont->GetWidth() + add_h;
    ImageList->Width = mpFont->GetWidth();
    ImageList->Height = mpFont->GetWidth() + add_h;
	Alphabet->ButtonHeight = mpFont->GetWidth() + 8 + add_h;
    Alphabet->ButtonWidth = mpFont->GetWidth() + 8;
	len = mpFont->GetData()->mAlphabet.mAlphabetSize;
    ImageList->AllocBy = len;
	ibmp->Canvas->Brush->Color = mpCurrText->GetAttributes().mCanvasColor;
	BLENDFUNCTION bf;
	bf.BlendOp = AC_SRC_OVER;
	bf.BlendFlags = 0;
	bf.SourceConstantAlpha = 255;
	bf.AlphaFormat = AC_SRC_ALPHA;
	for(i = 0; i < len; i++)
	{
		x = mpFont->GetData()->mAlphabet.mpAlphabetTextureOffsetMap[i].mX;
		y = mpFont->GetData()->mAlphabet.mpAlphabetTextureOffsetMap[i].mY;
		w = mpFont->GetData()->mAlphabet.mpAlphabetTextureOffsetMap[i].mW;
		h = mpFont->GetData()->mAlphabet.mpAlphabetTextureOffsetMap[i].mH;
		ibmp->Canvas->FillRect(Rect(0, 0, mpFont->GetWidth(), mpFont->GetWidth() + add_h));
		if(w > 0 && h > 0)
		{
			::AlphaBlend(ibmp->Canvas->Handle,
						(ibmp->Width - w) / 2 ,
						mpFont->GetData()->mAlphabet.mpAlphabetTextureOffsetMap[i].mY_off,
						w,
						h,
						mpFont->GetData()->mpAbcImage->Canvas->Handle,
						x,
						y,
						w,
						h,
						bf);
		}
		ImageList->Add(ibmp, NULL);
        bi = bc->Add();
        bi->ImageIndex = i;
    }
    delete ibmp;
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::DoDblClick(TObject */*Sender*/)
{
	TBaseItem *bi;
    wchar_t wch[2];
	bi = Alphabet->SelectedItem;
	if(bi == NULL)
	{
		return;
    }
    wch[0] = mpFont->GetData()->mAlphabet.mpAlphabet[bi->Index];
    wch[1] = L'\0';
    UnicodeString wstr = UnicodeString(wch);
    Memo->Text = Memo->Text + wstr;
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::ListViewKeyPress(TObject */*Sender*/, char &Key)
{
     if(Key == VK_RETURN || Key == VK_SPACE){Key=NULL; DoDblClick(NULL);}
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::CB_FontChange(TObject */*Sender*/)
{
	AssignFont(CB_Font->Text);
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::ActSizesExecute(TObject */*Sender*/)
{
	Application->CreateForm(__classid(TFEditorOptions),&FEditorOptions);
	FEditorOptions->TabSheet1->TabVisible=false;
	FEditorOptions->GroupBoxLanguages->Visible = false;
	FEditorOptions->LoadOptions();
	if(FEditorOptions->ShowModal() == mrOk)
	{
		setDisplaySize(FEditorOptions->CSE_dispW->Value, FEditorOptions->CSE_dispH->Value);
		FEditorOptions->SaveOptions();
	}
	FEditorOptions->Free();
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::ActLoadExecute(TObject */*Sender*/)
{
	 UnicodeString     file_name;
     //SaveText();
     Form_m->OpenDialog->Title=ActSave->Caption;
     Form_m->OpenDialog->InitialDir=ExtractFilePath(Form_m->proj_file_name);
     Form_m->OpenDialog->Filter = _T("Map text files (*.mtf)|*.mtf");
     if(!Form_m->OpenDialog->Execute())return;
     file_name=ChangeFileExt(Form_m->OpenDialog->FileName, _T(".mtf"));
     LoadFile(file_name);
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::LoadFile(UnicodeString /*file_name*/, UnicodeString */*errMessage*/)
{
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::ApplicationEvents1Message(tagMSG &Msg, bool &Handled)
{
      switch(Msg.message)
      {
       //при двойном щелчке мышки -- редактировать свойства фонового объекта
       case WM_LBUTTONDBLCLK:
       if(Alphabet->Focused())
       {
         Handled = true;
         DoDblClick(NULL);
       }
      }
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::AlphabetDrawIcon(TObject *Sender,
      const TButtonItem *Button, TCanvas *Canvas, TRect &Rect,
      TButtonDrawState State, int &TextOffset)
{
     CBDrawIcon(Sender,Button,Canvas,Rect,State,TextOffset);
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::AlphabetCategoryCollapase(TObject *Sender,
      const TButtonCategory *Category)
{
      CBCategoryCollapase(Sender,Category);
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::AlphabetMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
     CBMouseDown(Sender, Button, Shift, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::RefreshAlignment()
{
	::TextAttributes a = mpCurrText->GetAttributes();
	if((a.mAlignment & TEXT_ALIGNMENT_HCENTER) == TEXT_ALIGNMENT_HCENTER)
    {
		NAHCenter->Checked = true;
    }
    else
	if((a.mAlignment & TEXT_ALIGNMENT_LEFT) == TEXT_ALIGNMENT_LEFT)
    {
		NALeft->Checked = true;
    }
    else
	if((a.mAlignment & TEXT_ALIGNMENT_RIGHT) == TEXT_ALIGNMENT_RIGHT)
    {
		NARight->Checked = true;
    }

	if((a.mAlignment & TEXT_ALIGNMENT_VCENTER) == TEXT_ALIGNMENT_VCENTER)
    {
		NAVCenter->Checked = true;
    }
    else
	if((a.mAlignment & TEXT_ALIGNMENT_TOP) == TEXT_ALIGNMENT_TOP)
    {
		NATop->Checked = true;
    }
    else
	if((a.mAlignment & TEXT_ALIGNMENT_BOTTOM) == TEXT_ALIGNMENT_BOTTOM)
    {
		NABottom->Checked = true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::NALeftClick(TObject*)
{
    int ta = 0;
	if(NALeft->Checked)
    {
    	ta = TEXT_ALIGNMENT_LEFT;
    }
    else
	if(NARight->Checked)
    {
    	ta = TEXT_ALIGNMENT_RIGHT;
    }
    else
	if(NAHCenter->Checked)
    {
    	ta = TEXT_ALIGNMENT_HCENTER;
    }

    if(NATop->Checked)
    {
    	ta |= TEXT_ALIGNMENT_TOP;
    }
    else
	if(NABottom->Checked)
    {
    	ta |= TEXT_ALIGNMENT_BOTTOM;
    }
    else
	if(NAVCenter->Checked)
    {
    	ta |= TEXT_ALIGNMENT_VCENTER;
    }
	::TextAttributes a = mpCurrText->GetAttributes();
	a.mAlignment = ta;
	mpCurrText->SetAttributes(a);
	MemoChange(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFMessages::FormKeyDown(TObject */*Sender*/, WORD &Key, TShiftState Shift)
{
	if(Key == VK_RETURN && Shift.Contains(ssCtrl))
	{
		Close();
    }
}
//---------------------------------------------------------------------------

