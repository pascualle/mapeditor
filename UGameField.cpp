#pragma hdrstop

#include "UGameField.h"
#include "UObjCode.h"
#include "MainUnit.h"
#include "UScript.h"
#include "UObjManager.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

const int  CHECK_UP   = 1,
           CHECK_DOWN = -1,
           CHECK_ON   = 0;

const bool FACE = false,
           BACK = true;

const int CHECK_OFFSET = 1;

enum
{
  orientLEFT  = 0,
  orientRIGHT
};

enum
{
  TYPE     = 0,
  DIRECTOR = 1,
  POSITION = 2,
  MAP_TYPE_COUNT = 3
};

//---------------------------------------------------------------------------

 __fastcall GameField::GameField()
: sz_mapW(0)
, sz_mapH(0)
, pnt_count(0)
, p_h(0)
, p_w(0)
, map_h(0)
, map_w(0)
, sz_map(0)
, m_curZone(0)
, scale(1)
 {
  memset(&m_data, 0, sizeof(m_data));
  randomize();
 }
//---------------------------------------------------------------------------

 __fastcall GameField::~GameField(){}

//---------------------------------------------------------------------------

void __fastcall GameField::SetData(const TGameData *data)
{
  assert(data);
  m_data = *data;
  m_curZone = m_data.StartZone;
  p_w = m_data.p_w;
  p_h = m_data.p_h;
  map_w = m_data.m_pObjManager->mp_w;
  map_h = m_data.m_pObjManager->mp_h;
  sz_mapH = map_h * p_h;
  sz_mapW = map_w * p_w;
  sz_map = map_w * map_h;
}
//---------------------------------------------------------------------------

UnicodeString __fastcall GameField::getPropString(int idx)
{
  return UnicodeString(PredefProperties::PropertyNames[idx]);
}
//---------------------------------------------------------------------------

int __fastcall GameField::GetStateDirection(UnicodeString name)
{
  int i;
  TState *state;
  for(i = 0; i < m_data.m_pCommonResObjManager->states->Count; i++)
  {
		state = (TState*)m_data.m_pCommonResObjManager->states->Items[i];
		if(name.Compare(state->name)==0)
		{
			return state->direction;
        }
  }
  return -1;
}
//---------------------------------------------------------------------------




//TPlatformGameField  ------------------------------------------------------------
//TPlatformGameField  ------------------------------------------------------------
//TPlatformGameField  ------------------------------------------------------------




//----------------------------------------------------------------------------------
//������
void __fastcall TPlatformGameField::Process(TMapGPerson *pr, bool *next_frame)
{
	if(DoActions(pr, next_frame))
	{
		DoLogic(pr);
    }
}
//----------------------------------------------------------------------------------

void __fastcall TPlatformGameField::checkToMove(TMapGPerson *pr, int *mp, int x, int y, int check)
{
	(void)pr;
	(void)mp;
	(void)x;
	(void)y;
	(void)check;
	/*__int32 type, pos;
    switch(check)
    {
       case CHECK_DOWN: y += CHECK_OFFSET;
       break;
       case CHECK_UP:   y -= CHECK_OFFSET;
    }

    mp[TYPE] = 0;     //�� ��������� -- ����������
    mp[DIRECTOR] = 0;
    mp[POSITION] = -1;

	if(m_data.m_pObjManager->map == NULL || x < 0 || x > sz_mapW || y < 0 || y > sz_mapH)
	{
		return;
    }

    //��� ����������� ��� �������-�����
    if(pr->GetCollideRect().Right == 0 || pr->GetCollideRect().Bottom == 0)
    {
      mp[TYPE] = NONE_BGTYPE;
      return;
    }

    mp[POSITION] = pos = (y/p_h)*map_w+x/p_w;
    if(pos < sz_map)
    {
		type = m_data.m_pObjManager->map[pos];
    }
	else
	{
		return;
	}

	const __int32 backObjSize = static_cast<__int32>(m_data.m_pCommonResObjManager->BackObj.size());
	if(type < backObjSize)
	{
		const GMapBGObj* mo = m_data.m_pCommonResObjManager->GetMapBGObj(type);
		assert(mo);
		type = mo->GetFrameObj(0)->properties.type;
    }
    else //����� NONE_P
    {
     if(type == NONE_P)
       mp[TYPE] = NONE_BGTYPE;
     return;
    }

    mp[TYPE] = type;
    if(type != NONE_BGTYPE)
    {
      if(abs(type) > DIRECTOR_TYPE)
      {
        if(type > 0)mp[TYPE] = type - DIRECTOR_TYPE;
        else        mp[TYPE] = type + DIRECTOR_TYPE;
        mp[DIRECTOR] = 1;
      }
      if(!tileOnceCollide)
      {
        OnCollide(pr, NULL, abs(mp[TYPE]));
      }
	}
	*/
}
//---------------------------------------------------------------------------

int __fastcall TPlatformGameField::getYFragment(int y, unsigned char align)
{
 int yy = (y/p_h)*p_h;
 switch(align)
 {
  case alDOWN: yy+=p_h;
  break;
  case alLEFT:
  case alRIGHT:
  case alCENTER: yy+=p_h>>1;
 }
 return yy;
}
//---------------------------------------------------------------------------
int __fastcall TPlatformGameField::getXFragment(int x, unsigned char align)
{
 int xx = (x/p_w)*p_w;
 switch(align)
 {
  case alRIGHT: xx+=p_w;
  break;
  case alUP:
  case alDOWN:
  case alCENTER: xx+=p_w>>1;
 }
 return xx;
}
//---------------------------------------------------------------------------
/*
void __fastcall TPlatformGameField::prSetTop(int val, TMapGPerson *p)
{
  int x, y;
  int rect[4];
  p->getCoordinates(&x, &y);
  getPrCollideRect(p, rect);
  switch(p->Align)
  {
   case alLEFT:
   case alRIGHT:
   case alCENTER:
        val += (rect[PredefProperties::RECT_BOTTOM] - rect[PredefProperties::RECT_TOP])>>1;//(p->RealHeight>>1);
   break;
   case alUP:
   break;
   case alDOWN:
        val += rect[PredefProperties::RECT_BOTTOM] - rect[PredefProperties::RECT_TOP];//p->RealHeight;
   break;
 }
 p->setCoordinates(x, val, 0, 0);
 Form_m->ChangeChildPosition(p, 0, val - y);
}
//---------------------------------------------------------------------------
void __fastcall TPlatformGameField::prSetLeft(int val, TMapGPerson *p)
{
  int x, y;
  int rect[4];
  p->getCoordinates(&x, &y);
  getPrCollideRect(p, rect);
  switch(p->Align)
  {
   case alLEFT:
   break;
   case alRIGHT:
        val += rect[PredefProperties::RECT_RIGHT] - rect[PredefProperties::RECT_LEFT];//p->RealWidth;
   break;
   case alCENTER:
   case alUP:
   case alDOWN:
        val += (rect[PredefProperties::RECT_RIGHT] - rect[PredefProperties::RECT_LEFT])>>1;//(p->RealWidth>>1);
   break;
 }
 p->setCoordinates(val, y, 0, 0);
 Form_m->ChangeChildPosition(p, val-x, 0);
}
//----------------------------------------------------------------------------------
*/
void __fastcall TPlatformGameField::prSetState(TMapGPerson *p, UnicodeString stateName)
{
	p->State_Name = stateName;
	Form_m->DbgOnChangeState(p);
}
//----------------------------------------------------------------------------------

void __fastcall TPlatformGameField::prSetVarLalue(TMapGPerson *p, UnicodeString varname, double val)
{
	p->SetVarValue(val, varname);
	OnChangeValue(p, varname);
}
//----------------------------------------------------------------------------------

void __fastcall TPlatformGameField::prSetOppositeState(TMapGPerson *p, UnicodeString state)
{
 int i,len;
 len = m_data.m_pCommonResObjManager->stateOppositeTable->Count;
 for(i=0; i<len; i++)
 {
  if(m_data.m_pCommonResObjManager->stateOppositeTable->Names[i].Compare(state)==0)
  {
   prSetState(p, m_data.m_pCommonResObjManager->stateOppositeTable->ValueFromIndex[i]);
   break;
  }
 }
}
//----------------------------------------------------------------------------------

bool __fastcall TPlatformGameField::prCheckBGDirector(int /*pos*/, TMapGPerson */*pr*/)
{
  /*if(pr->LastBGDir != pos)
  {
     int dirListCount = pr->getDirStatesCount();
     if(dirListCount > 0)
     {
      if(++pr->DirStateIdx >= dirListCount)pr->DirStateIdx = 0;
      UnicodeString st = pr->getDirStateItem(pr->DirStateIdx);
      prSetState(pr, pr->getDirStateItem(pr->DirStateIdx));
      pr->LastBGDir = pos;
      return true;
     }
  }*/
  return false;
}
//----------------------------------------------------------------------------------

void __fastcall TPlatformGameField::prAddXYToPosition(TMapGPerson *pr, double x, double y)
{
  double ftx,fty;
  pr->getCoordinates(&ftx, &fty);
  ftx+=x;
  fty+=y;
  pr->setCoordinates(ftx, fty, 0, 0);
  //Form_m->ChangeChildPosition(pr, x, y);
}
//----------------------------------------------------------------------------------

void __fastcall TPlatformGameField::getPrCollideRect(TMapGPerson *p, int *rect)
{
	rect[PredefProperties::RECT_TOP] = p->GetCollideRect().Top;
	rect[PredefProperties::RECT_LEFT] = p->GetCollideRect().Left;
	rect[PredefProperties::RECT_RIGHT] = p->GetCollideRect().Right;
	rect[PredefProperties::RECT_BOTTOM] = p->GetCollideRect().Bottom;
}
//----------------------------------------------------------------------------------

void __fastcall TPlatformGameField::getPrViewRect(TMapGPerson *p, int *rect)
{
	rect[PredefProperties::RECT_TOP] = p->GetViewRect().Top;
	rect[PredefProperties::RECT_LEFT] = p->GetViewRect().Left;
	rect[PredefProperties::RECT_RIGHT] = p->GetViewRect().Right;
	rect[PredefProperties::RECT_BOTTOM] = p->GetViewRect().Bottom;
}
//----------------------------------------------------------------------------------

bool __fastcall TPlatformGameField::prLookForObj(TMapGPerson *opr, int footY, int faceY)
{
  int   opr_rect[4];
  getPrCollideRect(opr, opr_rect);
  return(footY >= opr_rect[PredefProperties::RECT_TOP] &&
         faceY <= opr_rect[PredefProperties::RECT_BOTTOM]);
}
//----------------------------------------------------------------------------------

int __fastcall TPlatformGameField::prDistanseForObj(TMapGPerson *pr, TMapGPerson *opr)
{
  int pr_left, pr_right, opr_left, opr_right;
  int   pr_rect[4];
  int   opr_rect[4];

  getPrCollideRect(pr, pr_rect);
  getPrCollideRect(opr, opr_rect);

  pr_right = pr_rect[PredefProperties::RECT_RIGHT];
  opr_left = opr_rect[PredefProperties::RECT_LEFT];

  if(pr_right < opr_left)
  {
   return opr_left - pr_right;
  }

  pr_left = pr_rect[PredefProperties::RECT_LEFT];
  opr_right = opr_rect[PredefProperties::RECT_RIGHT];
  if(pr_left > opr_right)
  {
   return pr_left - opr_right;
  }
  return 0;
}
//----------------------------------------------------------------------------------

bool __fastcall TPlatformGameField::DoActions(TMapGPerson *mp, bool *next_frame)
{
  double enabled, gravity, speed;
  int   sx, sy, face, back, top, bottom, x, y;
  double   dx, dy;
  int	width, height;
  bool  need_return;
  int   mt[MAP_TYPE_COUNT];
  int   pr_rect[4];

  assert(mp);

  TMapGPerson *ppr;
  if(!mp->ParentObj.IsEmpty())
  {
    ppr = (TMapGPerson *)Form_m->FindObjectPointer(mp->ParentObj);
    ppr->getCoordinates(&dx, &dy);
  }
  else
  {
   ppr = mp;
  }

  tileOnceCollide = false;

  //���������� �������� ENABLED
  if(!ppr->GetVarValue(&enabled, getPropString(PredefProperties::PRP_ENABLE)))
  {
	enabled = 1.0f;
  }
  mp->ShowEnabled(enabled > 0.0f);
  if(enabled == 0.0f)
  {
   *next_frame = false;
   return false;
  }

  if(!ppr->GetVarValue(&gravity, getPropString(PredefProperties::PRP_GRAVITY)))
  {
	gravity = 0.0f;
  }
  // SPEED
  if(mp == ppr)
  {
	if(!ppr->GetVarValue(&speed, getPropString(PredefProperties::PRP_SPEED)))
	{
		speed = 0.0f;
	}
  }
  else
  {
    speed = 0.0f;
  }
  //direction = GetStateDirection(ppr->State_Name);

  if(Form_m->DbgGetFocusedObj() == mp)
  {
	speed = 0.0f;
  }

	if(!ppr->NextLinkedDirector.IsEmpty())
	{
		if(speed > 0.0f)
		{
			TMapDirector *next_node;
			next_node = (TMapDirector *)Form_m->FindObjectPointer(ppr->NextLinkedDirector);

			if(next_node != NULL)
			{
				double tx, ty, px, py;
				double out_moving[2];
				next_node->getCoordinates(&tx, &ty);
				ppr->mPrevTransform.GetPosition(dx, dy);
				ppr->getCoordinates(&px, &py);
				out_moving[IX] = px;
				out_moving[IY] = py;
				if(nextPointAtLine(dx, dy, tx, ty, speed, out_moving))
				{
					px = out_moving[IX];
					py = out_moving[IY];
					ppr->setCoordinates(px, py, 0, 0);
				}
				else
				{
					px = out_moving[IX];
					py = out_moving[IY];
					ppr->setCoordinates(px, py, 0, 0);
					if(!SetNextDirectorPos(ppr))
					{
						ppr->setCoordinates(tx, ty, 0, 0);
					}
				}
			}
		}
	}

	if(!ppr->GetVarValue(&enabled, getPropString(PredefProperties::PRP_ENABLE)))
	{
		enabled = 1.0f;
	}

	mp->ShowEnabled(enabled > 0.0f);

	if(enabled == 0.0f)
	{
		*next_frame = false;
		return false;
	}

	*next_frame = true;
	return true;
}
//----------------------------------------------------------------------------------

bool __fastcall TPlatformGameField::SetNextDirectorPos(TMapGPerson *pr)
{
	TMapDirector *node;
	int direction;
	bool atSameNode, traceScript;
	UnicodeString next_node;

	if(pr->NextLinkedDirector.IsEmpty())
	{
		return false;
	}

	node = (TMapDirector *)Form_m->FindObjectPointer(pr->NextLinkedDirector);
	if(node == NULL)return false;

	atSameNode = pr->NextLinkedDirector.Compare(pr->PrevLinkedDirector) == 0;

	traceScript = pr->TraceTriggerActions;

	if(!atSameNode)
	{
		double x, y;
		OnChangeNode(pr, node);
		pr->getCoordinates(&x, &y);
		pr->mPrevTransform.SetPosition(x, y);
		if(!node->nodeTrigger.IsEmpty())
		{
		  if(traceScript)
			 Form_m->AddDebugMessage(_T("----------------------------------------------------------------------------------"), NULL);
		  DoTrigger(node->nodeTrigger, pr, NULL);
		}
	}

	if(pr->NextLinkedDirector.IsEmpty())
	{
		return false;
	}
	node = (TMapDirector *)Form_m->FindObjectPointer(pr->NextLinkedDirector);
	if(node == NULL)
	{
		return false;
	}
	direction = GetStateDirection(pr->State_Name);
	if(direction > 0)
	{
		next_node = node->nearNodes[direction / 2 - 1];
	}
	else
	{
		double x, y;
		next_node = pr->NextLinkedDirector;
		pr->getCoordinates(&x, &y);
        pr->mPrevTransform.SetPosition(x, y);
	}
	pr->PrevLinkedDirector = pr->NextLinkedDirector;
	pr->NextLinkedDirector = next_node;

	return pr->PrevLinkedDirector.Compare(pr->NextLinkedDirector) != 0;
}
//----------------------------------------------------------------------------------

// onCOLLIDE
void __fastcall TPlatformGameField::DoLogic(TMapGPerson *pr)
{
 int         i, len, pr_ctop, pr_cbottom, /*pr_vtop, pr_vbottom,*/ distance;
 bool        i_see_you;
 int         pr_rect[4];
 TMapGPerson *opr;
 len = m_data.m_pObjManager->map_u->Count;

 getPrCollideRect(pr, pr_rect);
 pr_ctop = pr_rect[PredefProperties::RECT_TOP];
 pr_cbottom = pr_rect[PredefProperties::RECT_BOTTOM];
 getPrViewRect(pr, pr_rect);
 //pr_vtop = pr_rect[PredefProperties::RECT_TOP];
 //pr_vbottom = pr_rect[PredefProperties::RECT_BOTTOM];

 if(pr->GetCollideRect().Right > 0 && pr->GetCollideRect().Bottom > 0)
  //���� ��� �������-�����, ��� � ��������
  for(i=0; i<len; i++)
  {
   opr = (TMapGPerson *)m_data.m_pObjManager->map_u->Items[i];
   if(pr == opr)continue;

   /*i_see_you = prLookForObj(opr, pr_vbottom, pr_vtop);
   distance = prDistanseForObj(pr, opr);
   if(i_see_you && distance < pr->ViewRect.Right)
   {

   }*/

   distance = prDistanseForObj(pr, opr);
   i_see_you = prLookForObj(opr, pr_cbottom, pr_ctop);
   if(i_see_you && distance == 0)
   {
	 OnCollide(pr, opr, -1);
   }
  }
}
//----------------------------------------------------------------------------------

void __fastcall TPlatformGameField::StartScript(UnicodeString name, UnicodeString headerString,
												void* Owner, void* Second, bool checkMultiple, bool forceTrace)
{
  /*
  int i,len;
  TGScript* script;
  bool traceScript, found = false;
  len = m_data.m_pObjManager->scripts->Count;

  traceScript = forceTrace;
  if(!traceScript)
  {
	 if(Owner != NULL)
	 {
		traceScript = ((TMapGPerson*)Owner)->TraceTriggerActions;
	 }
	 if(!traceScript)
	 {
		if(Second != NULL)
		{
		   traceScript = ((TMapGPerson*)Second)->TraceTriggerActions;
		}
	 }
  }

  for(i=0; i<len; i++)
  {
   script = (TGScript*)m_data.m_pObjManager->scripts->Items[i];
   if(name.Compare(script->Name)==0)
   {
	 found = true;
	 break;
   }
  }
  if(found)
  {
   if(checkMultiple)
   {
	 if(!script->Enabled)return;
	 if(!script->Multiple && script->Enabled)script->Enabled = false;
   }

   if(traceScript)
   {
	 Form_m->AddDebugMessage(_T("----------------------------------------------------------------------------------"), NULL);
	 if(!headerString.IsEmpty())
	 {
	   Form_m->AddDebugMessage( headerString, NULL );
	 }
	 UnicodeString mes = Localization::Text(_T("mMiniscriptExecuted")) + _T(": ") + name;
	 Form_m->AddDebugMessage(mes.c_str(), NULL);
	 mes = _T(" - ") + Localization::Text(_T("mActiveZone")) + _T(": ") + IntToStr(script->Zone) +
			_T("; ") + Localization::Text(_T("mMultipleUsed")) + ": " +
			((script->Multiple) ? Localization::Text(_T("mYes")) : Localization::Text(_T("mNo")));
	 Form_m->AddDebugMessage(mes.c_str(), NULL);
   }
   len = script->TrigNameList->Count;
   for(i=0; i<len; i++)
   {
	 DoTrigger(script->TrigNameList->Strings[i], Owner, Second);
   }
   if(script->GoToZone >= 0)
   {
	//PostMessage(NULL, MES_CHANGEZONE, script->GoToZone, 0);
	Form_m->ChangeZone(script->GoToZone);
   }
   if(!script->ChangeCtrlTo.IsEmpty())
   {
	Form_m->FindAndShowObjectByName(script->ChangeCtrlTo, true);
   }

   if(traceScript)
   {
	if(!script->ChangeCtrlTo.IsEmpty())
		Form_m->AddDebugMessage((_T(" - ") + Localization::Text(_T("mChangeHeroTo")) + _T(": ") + script->ChangeCtrlTo), Form_m->FindObjectPointer(script->ChangeCtrlTo));
	if(script->GoToZone >= 0)
		Form_m->AddDebugMessage((_T(" - ") + Localization::Text(_T("mGotoZone")) + _T(": ") + IntToStr(script->GoToZone)), NULL);
	if(!script->Message.IsEmpty())
		Form_m->AddDebugMessage((_T(" - ") + Localization::Text(_T("mShowText")) + _T(" (ID): ") + script->Message), NULL);
	if(script->EndLevel >= 0)
		Form_m->AddDebugMessage(_T(" - ") + Localization::Text(_T("mGotoLevel")) + _T(": ") + IntToStr(script->EndLevel), NULL);
   }
   if(script->EndLevel >= 0)
   {
		Form_m->DbgStopTimer();
   }

   if(traceScript)
	Form_m->AddDebugMessage(_T("----------------------------------------------------------------------------------"), NULL);

   if(stopOnStartScript)
   {
	Form_m->DbgStopTimer();
   }
  }
  else
  {
   if(traceScript)Form_m->AddDebugMessage(_T(" - ") + Localization::Text(_T("mError")) + _T(": ") + Localization::Text(_T("mObjFindWarning")), NULL);
  }
  */
}
//----------------------------------------------------------------------------------


void __fastcall TPlatformGameField::DoTrigger(UnicodeString name, void *Owner, void *Second, bool forceTrace)
{
  int           i,len;
  bool          found, traceScript;
  TMapTrigger   *trigger;
  len = m_data.m_pObjManager->triggers->Count;
  found = false;
  for(i=0; i<len; i++)
  {
   trigger = (TMapTrigger*)m_data.m_pObjManager->triggers->Items[i];
   if(trigger->Name.Compare(name) == 0)
   {
	found = true;
	break;
   }
  }

  traceScript = forceTrace;
  if(!traceScript)
  {
	 if(Owner != NULL)
	 {
		traceScript = ((TMapGPerson*)Owner)->TraceTriggerActions;
	 }
	 if(!traceScript)
	 {
		if(Second != NULL)
		{
		   traceScript = ((TMapGPerson*)Second)->TraceTriggerActions;
		}
	 }
  }

  if(traceScript)
  {
   UnicodeString mes = _T(" - ") + Localization::Text(_T("mTrigger")) + _T(": ") + name;
   if(!found)mes = mes + _T(". ") + Localization::Text(_T("mError")) + _T(": ") + Localization::Text(_T("mObjFindWarning"));
   Form_m->AddDebugMessage(mes, (found ? trigger : NULL));
  }

  if(found)
  {
   int idx;
   double x, y;
   switch(trigger->Type)
   {
	case trMAP:
     trigger->getCoordinates(&x, &y);
     if(traceScript)
	 {
	   UnicodeString mes = _T("    - ") + Localization::Text(_T("mEditTileMapDescroption1")) + _T(" (") +
							IntToStr((int)x) + _T(",") +
							IntToStr((int)y) + _T("),");
	   mes = mes + _T(" ") + Localization::Text(_T("mEditTileMapDescroption2")) + _T(" ") + IntToStr(trigger->mapIndex);
	   mes = mes + _T(" ") + Localization::Text(_T("mEditTileMapDescroption3")) + _T(" ") + trigger->mapCount +
	   				_T(" ") + Localization::Text(_T("mEditTileMapDescroption4")) + _T(": ");
       switch(trigger->mapDirection)
       {
		 case trUP: mes = mes + Localization::Text(_T("mUp")) + _T(".");
		 break;
		 case trDOWN: mes = mes + Localization::Text(_T("mDown")) + _T(".");
		 break;
		 case trLEFT: mes = mes + Localization::Text(_T("mLeft")) + _T(".");
		 break;
		 case trRIGHT: mes = mes + Localization::Text(_T("mRight")) + _T(".");
       }
       Form_m->AddDebugMessage(mes, trigger);
     }
     len = trigger->mapCount;

	 idx = ((int)y / m_data.p_h) * m_data.m_pObjManager->mp_w + (int)x / m_data.p_w;
	 for(i = 0; i < len; i++)
	 {
	   if(idx >= 0 && idx < m_data.m_pObjManager->mp_w * m_data.m_pObjManager->mp_h)
	   {
		 m_data.m_pObjManager->map[idx] =(unsigned short) trigger->mapIndex;
	   }
	   switch(trigger->mapDirection)
	   {
		 case trUP: idx -= m_data.m_pObjManager->mp_w;
		 break;
		 case trDOWN: idx += m_data.m_pObjManager->mp_w;
		 break;
		 case trLEFT: idx--;
		 break;
		 case trRIGHT: idx++;
	   }
	 }
	 //PostMessage(NULL, MES_REDRAWALLMAP, 0, 0);
	 Form_m->DrawMap(true);
	break;

	case trPROPERTIES:
	{
	 TMapGPerson *pt;
	 UnicodeString mes;
	 if(traceScript)mes = _T("    - ") + Localization::Text(_T("mSendPrpValues1"));
	 if(trigger->objName.IsEmpty())
	 {
	   pt = NULL;
	   if(traceScript)mes += _T(", ") + Localization::Text(_T("mError")) + _T(": ") + Localization::Text(_T("mObjFindWarning"));
	 }
	 else
	 {
	   if( trigger->objName.Compare( UnicodeString(SpecialObjConstants::VarObjNameInitiator) )== 0)
	   {
		 pt = (TMapGPerson*)Owner;
		 if(traceScript)
		 {
		   if(pt == NULL)mes += _T(", ") + Localization::Text(_T("mError")) + _T(": ") + Localization::Text(_T("mFirstInitiatorOfEventError"));
		   else mes += _T(": ") + pt->Name + _T(" (") + Localization::Text(_T("mFirstInitiatorOfEvent")) + _T(")");
		 }
	   }
	   else
	   if( trigger->objName.Compare( UnicodeString(SpecialObjConstants::VarObjNameSecond) )== 0)
	   {
		 pt = (TMapGPerson*)Second;
		 if(traceScript)
		 {
		   if(pt == NULL)mes += _T(", ") + Localization::Text(_T("mError")) + _T(": ") + Localization::Text(_T("mSecondInitiatorOfEventError"));
		   else mes += _T(": ") + pt->Name + _T(" (") + Localization::Text(_T("mSecondInitiatorOfEvent")) + _T(")");
		 }
	   }
	   else
	   {
		 pt = (TMapGPerson*)Form_m->FindObjectPointer(trigger->objName);
		 if(traceScript)
		 {
		   if(pt == NULL)mes += _T(", ") + Localization::Text(_T("mError")) + _T(": ") + Localization::Text(_T("mObjFindWarning"));
		   else mes += _T(": ") + pt->Name;
		 }
	   }
	 }
	 if(traceScript)Form_m->AddDebugMessage(mes, pt);

	 if(pt != NULL)
	 {
	   UnicodeString  prpname;
	   if(trigger->stateList->Count > 0)
	   {
		UnicodeString rndState;
		rndState  = trigger->stateList->Strings[rand()%trigger->stateList->Count];
		if(rndState.Compare( UnicodeString(SpecialObjConstants::VarStateName) ) == 0)
		{
			 found = false;
			 len = m_data.m_pCommonResObjManager->stateOppositeTable->Count;
			 rndState = pt->State_Name;
			 for(i=0; i<len; i++)
			 {
			   mes = m_data.m_pCommonResObjManager->stateOppositeTable->Names[i];
			   if(mes.Compare(rndState) == 0)
			   {
					prSetState(pt, m_data.m_pCommonResObjManager->stateOppositeTable->ValueFromIndex[i]);
					found = true;
					break;
			   }
			 }
			 if(traceScript)
			 {
				mes = _T("     - ") + Localization::Text(_T("mChangeState")) + _T(" (") + rndState + _T(") ") + Localization::Text(_T("mToOpposite"));
				if(!found)
					mes += _T(" : ") + Localization::Text(_T("mOppositeStatesDescriptionError"));
				else
					mes += _T(" (") + pt->State_Name + _T(")");
				Form_m->AddDebugMessage(mes, pt);
			 }
		}
		else
		{
          if(traceScript)
          {
			mes = _T("     - ") + Localization::Text(_T("mChangeStateTo1")) + _T(" '") + rndState + _T("'");
            Form_m->AddDebugMessage(mes, pt);
          }
          prSetState(pt, rndState);
        }
       }

	   for(i=0; i < ::Actions::sptCOUNT; i++)
       {
		 UnicodeString key = UnicodeString(::Actions::Names[i]);
         prpname = trigger->getScriptName(key);
		 if(!prpname.IsEmpty())
         {
           UnicodeString empt_key = UnicodeString(SpecialObjConstants::VarBlankActionName);
           if(prpname.Compare(empt_key)==0)
           {
			 prpname = _T("");
             if(traceScript)
             {
			  mes = _T("     - ") + Localization::Text(_T("mRedefineAction")) + _T(" '") + UnicodeString(::Actions::Names[i]) + _T("' ") + Localization::Text(_T("mEmptyValue1"));
			  Form_m->AddDebugMessage(mes, pt);
			 }
		   }
		   else
		   {
			 if(traceScript)
			 {
			  mes = _T("     - ") + Localization::Text(_T("mRedefineAction")) + _T(" '") + UnicodeString(::Actions::Names[i]) + _T("' ") + Localization::Text(_T("mWithScript")) + _T(" '") + prpname + _T("'");
			  Form_m->AddDebugMessage(mes, pt);
             }
           }
		   pt->setScriptName(key, prpname);
         }
       }

       if(!trigger->objTeleport.IsEmpty())
       {
         TMapGPerson *tobj = (TMapGPerson *)Form_m->FindObjectPointer(trigger->objTeleport);
         if(traceScript)
         {
		   mes = _T("     - ") + Localization::Text(_T("mTeleportToObject")) + _T(" ") + trigger->objTeleport;
		   if(tobj == NULL)mes += _T(". ") + Localization::Text(_T("mError")) + _T(": ") + Localization::Text(_T("mObjFindWarning"));
           Form_m->AddDebugMessage(mes, tobj);
         }
         if(tobj != NULL)
         {
           double x, y, ox, oy;
           tobj->getCoordinates(&x, &y);
           pt->getCoordinates(&ox, &oy);
           pt->setCoordinates(x, y, 0, 0);
		   pt->ResetPrevTransform();
		   pt->NextLinkedDirector = trigger->objTeleport;
         }
       }

       if(trigger->tileTeleport >= 0)
       {
		 double x, y, xx, yy;
		 y = (double)(trigger->tileTeleport / m_data.m_pObjManager->mp_w);
		 x = (double)(trigger->tileTeleport % m_data.m_pObjManager->mp_w);
		 xx = (double)(x * m_data.p_w);
		 yy = (double)(y * m_data.p_h);// + p_h/2;
		 yy = (double)(getYFragment((int)yy, /*pt->Align*/ alDOWN));
		 xx = (double)(getXFragment((int)xx, /*pt->Align*/ alDOWN));
		 if(traceScript)
         {
		   mes = _T("     - ") + Localization::Text(_T("mTeleportToTileNo1")) + _T(" ") + IntToStr(trigger->tileTeleport);
		   mes += _T(" (x=") + IntToStr((int)xx) + _T(":y=") + IntToStr((int)yy) + _T(")");
		   if((int)x > m_data.m_pObjManager->mp_w || (int)x < 0 || (int)y < 0 || (int)y > m_data.m_pObjManager->mp_h)
		   {
			mes += _T(". ") + Localization::Text(_T("mError")) + _T(": ") + Localization::Text(_T("mObjFindWarning"));
		   }
		   Form_m->AddDebugMessage(mes, pt);
		 }
		 if((int)x <= m_data.m_pObjManager->mp_w && (int)x>=0 && (int)y>=0 && (int)y <= m_data.m_pObjManager->mp_h)
		 {
		   double ox, oy;
		   pt->getCoordinates(&ox, &oy);
		   pt->setCoordinates(xx, yy, 0, 0);
		   //Form_m->ChangeChildPosition(pt, xx-ox, yy-oy);
		   //pt->LastBGDir = -1; // ����������� ������ ��������� ��������
		   //pt->PrevLinkedDirector = "";
		   pt->ResetPrevTransform();
		   pt->NextLinkedDirector = _T("");
		 }
	   }

	   {
		bool        var;
		double      val, val1;
		const std::list<PropertyItem>* lvars = m_data.m_pCommonResObjManager->MainGameObjPattern->GetVars();
		std::list<PropertyItem>::const_iterator iv;
		for (iv = lvars->begin(); iv != lvars->end(); ++iv)
		{
		 prpname = iv->name;
		 if(!trigger->GetCheckedValue(&var, prpname))
			var = false;
		 if(var)
		 {
           //����� mapIndex ������������ ��� ��� �������� ������ �������
		   switch(trigger->mapIndex)
           {
            case SpecialObjConstants::trOBJ_CHANGE_PRP_TRANSMIT:
			  if(trigger->GetVarValue(&val, prpname))
              {
			   if(traceScript)
               {
				mes = _T("     - ") + Localization::Text(_T("mToProperty")) + _T(" '") + prpname + _T("' ") + Localization::Text(_T("mAssignValue")) + _T(" ") + FloatToStr(val);
				Form_m->AddDebugMessage(mes, pt);
			   }
			   prSetVarLalue(pt, prpname, val);
			  }
			  else
			  {
			   mes = _T("     - ") + Localization::Text(_T("mToProperty")) + _T(" '") + prpname + _T("' ") + Localization::Text(_T("mAssignValueError"));
               Form_m->AddDebugMessage(mes, pt);
              }
            break;

            case SpecialObjConstants::trOBJ_CHANGE_PRP_DECREASE:
			  if(trigger->GetVarValue(&val, prpname))
              {
               if(traceScript)
               {
				mes = _T("     - ") + Localization::Text(_T("mSubtractPrpValue")) + " '" + prpname + _T("' ") + Localization::Text(_T("mToValue")) + _T(" ") + FloatToStr(val);
				Form_m->AddDebugMessage(mes, pt);
               }
			   pt->GetVarValue(&val1, prpname);
			   val1 -= val;
               //if(val1<=-128)val1 = -128;
			   prSetVarLalue(pt, prpname, val1);
              }
			  else
              {
			   mes = _T("     - ") + Localization::Text(_T("mSubtractPrpValue")) + _T(" '") + prpname + _T("'. ") + Localization::Text(_T("mFindError"));
               Form_m->AddDebugMessage(mes, pt);
              }
            break;

            case SpecialObjConstants::trOBJ_CHANGE_PRP_INCREASE:
			  if(trigger->GetVarValue(&val, prpname))
              {
               if(traceScript)
               {
				mes = _T("     - ") + Localization::Text(_T("mAddPrpValue")) + " '" + prpname + _T("' ") + Localization::Text(_T("mToValue")) + _T(" ") + FloatToStr(val);
                Form_m->AddDebugMessage(mes, pt);
               }
			   pt->GetVarValue(&val1, prpname);
               val1 += val;
			   //if(val1>=127)val1 = 127;
               prSetVarLalue(pt, prpname, val1);
              }
              else
              {
			   mes = _T("     - ") + Localization::Text(_T("mAddPrpValue")) + _T(" '") + prpname + _T("'. ") + Localization::Text(_T("mFindError"));
			   Form_m->AddDebugMessage(mes, pt);
              }
			break;
		   }
		 }
		}
	   }
     }
    } //end case trPROPERTIES
    break;
   } //end switch(trigger->Type)
  } //end if
}
//----------------------------------------------------------------------------------

bool __fastcall TPlatformGameField::OnCollide(TMapGPerson *pr, TMapGPerson *opr, int TileUniqIdx)
{
 /*
	int         i,len;
	UnicodeString  spt_name, key, mes, str;
	bool        find;
	TGScript    *st;

	if(TileUniqIdx < 0)
	{
		key = UnicodeString(::Actions::Names[::Actions::sptONCOLLIDE]);
	}
	else
	{
		key = UnicodeString(::Actions::Names[::Actions::sptONTILECOLLIDE]);
	}

	//���� ��� ����� �������� ������� -- ��������� ��������� �������� �������
	//if(!pr->ParentObj.IsEmpty())
	//{
	//  pr = (TMapGPerson *)Form_m->FindObjectPointer(pr->ParentObj);
	//}

	if(opr != NULL)
	{
		if(!opr->ParentObj.IsEmpty())
		{
			opr = (TMapGPerson *)Form_m->FindObjectPointer(opr->ParentObj);
		}
	}

	spt_name = pr->getScriptName( key );
	len = m_data.m_pObjManager->scripts->Count;
	if(!spt_name.IsEmpty())
	{
		find = false;
		for(i = 0; i < len; i++)
		{
			st = (TGScript*)m_data.m_pObjManager->scripts->Items[i];
			if(st->Name.Compare(spt_name)==0)
			{
				find = st->Enabled;
				break;
			}
		}

		if(find)
		{
			if(TileUniqIdx >= 0)
			{
			  find = TileUniqIdx == st->CldParamValueSecond;
			}
			else
			{
				find = false;
				len = st->CldGroupNameList->Count;
				str = opr->getGParentName(); //��� �������� �������, � ������� �����������

				if(len == 0)find = true;
				else
				for(i=0; i<len; i++)
				if(str.Compare(st->CldGroupNameList->Strings[i])==0)
				{
				find = true;
				break;
				}

				if(find)
				{
				find = false;
				len = st->CldObjNameList->Count;
				str = opr->Name; //��� �������, � ������� �����������

				  if(len == 0)find = true;
				  else
				  for(i=0; i<len; i++)
				  {
				   UnicodeString objName = st->CldObjNameList->Strings[i];
				   if(str.Compare(objName) == 0)
				   {
					find = true;
					break;
				   }
				  }
				}
			}

       //��������� �������
       if(find)
       if(!st->CldPropNameInitiator.IsEmpty())
       {
         if(pr != NULL)
         {
		  double prp_val;
		  find = false;
		  if(pr->GetVarValue(&prp_val, st->CldPropNameInitiator))
		  switch(st->CldMathOperPropInitiator)
          {
           case mtLESSEQ:
            find = prp_val <= st->CldParamValueInitiator;
           break;
           case mtEQUAL:
            find = prp_val == st->CldParamValueInitiator;
           break;
           case mtMOREEQ:
            find = prp_val >= st->CldParamValueInitiator;
           break;
           case mtNOT:
            find = prp_val != st->CldParamValueInitiator;
          }
         }
         else find = false;
       }
       
       if(find)
       if(!st->CldPropNameSecond.IsEmpty())
       {
         if(opr != NULL)
         {
		  double prp_val;
          find = false;
		  if(opr->GetVarValue(&prp_val, st->CldPropNameSecond))
          switch(st->CldMathOperPropSecond)
          {
           case mtLESSEQ:
            find = prp_val <= st->CldParamValueSecond;
           break;
           case mtEQUAL:
            find = prp_val == st->CldParamValueSecond;
           break;
           case mtMOREEQ:
            find = prp_val >= st->CldParamValueSecond;
           break;
           case mtNOT:
            find = prp_val != st->CldParamValueSecond;
          }
         }
         else find = false;
       }

       if(find)
       if(!st->CldStateNameInitiator.IsEmpty())
       {
         if(pr != NULL)
         {
          switch(st->CldMathOperStateInitiator)
          {
           case mtEQUAL:
            find = st->CldStateNameInitiator.Compare( pr->State_Name ) == 0;
           break;
           case mtNOT:
            find = st->CldStateNameInitiator.Compare( pr->State_Name ) != 0;
          }
         }
         else find = false;
       }

       if(find)
       if(!st->CldStateNameSecond.IsEmpty())
       {
         if(opr != NULL)
         {
          switch(st->CldMathOperStateSecond)
          {
           case mtEQUAL:
            find = st->CldStateNameSecond.Compare( opr->State_Name ) == 0;
           break;
           case mtNOT:
            find = st->CldStateNameSecond.Compare( opr->State_Name ) != 0;
          }
         }
         else find = false;
       }

       if(find)
       {
		mes = pr->Name + _T(" : ") + Localization::Text(_T("mMiniscriptExecuted"))+ _T(" '") + spt_name + _T("' ") + Localization::Text(_T("mOnEvent")) + _T(" '") + key + _T("'");
		StartScript(spt_name, mes, pr, opr, true, false);

        if(TileUniqIdx >= 0)
		{
         tileOnceCollide = true;
        }
       }
     }
 }
 */
	return true;
}
//----------------------------------------------------------------------------------

bool __fastcall TPlatformGameField::OnEndAnimation(TMapGPerson *mp)
{
  if(mp != NULL)
      if(mp->GetObjType() == objMAPOBJ && mp->ParentObj.IsEmpty())
      {
       int         i, len;
       UnicodeString  spt_name, key, mes;
       TGScript    *st;
       bool        find = false;
	   key = UnicodeString(::Actions::Names[::Actions::sptONENDANIM]);
       spt_name = mp->getScriptName(key);

	   /*
	   if(!spt_name.IsEmpty())
	   {
		len = m_data.m_pObjManager->scripts->Count;
		for(i=0; i<len; i++)
		{
		 st = (TGScript*)m_data.m_pObjManager->scripts->Items[i];
		 if(st->Name.Compare(spt_name)==0)
		 {
		   find = st->Enabled;
		   break;
		 }
		}
		if(find)
		{
		  mes = mp->Name + _T(" : ") + Localization::Text(_T("mMiniscriptExecuted"))+ _T(" '") + spt_name + _T("' ") + Localization::Text(_T("mOnAnimationEndEvent"));
		  mp->setScriptName(::Actions::Names[::Actions::sptONENDANIM], ""); //��������� ������� ������ �� �������
		  StartScript(spt_name, mes, mp, NULL, true, false);
		}
	   }
	   */

	  }
 return true;
}
//----------------------------------------------------------------------------------

bool __fastcall TPlatformGameField::OnChangeValue(TMapGPerson *mp, UnicodeString prpname)
{
  if(mp != NULL)
    if(mp->GetObjType() == objMAPOBJ && mp->ParentObj.IsEmpty())
    {
       int         actName, i, len;
       UnicodeString  spt_name, key, mes;
       TGScript    *st;
       bool        find = false;
       /*
	   for(actName = ::Actions::sptONCHANGE1; actName <= ::Actions::sptONCHANGE3; actName++)
	   {
		key = UnicodeString(::Actions::Names[actName]);
        spt_name = mp->getScriptName(key);
        if(!spt_name.IsEmpty())
        {
         len = m_data.m_pObjManager->scripts->Count;
         for(i=0; i<len; i++)
         {
          st = (TGScript*)m_data.m_pObjManager->scripts->Items[i];
          if(st->Name.Compare(spt_name)==0)
          {
            find = st->Enabled;
            break;
          }
         }
         if(find)
         {
		   double prp_val;
		   GPersonProperties *prp;
           find = st->ChgPropName.Compare(prpname) == 0;
           if(find)
			if(mp->GetVarValue(&prp_val, st->ChgPropName))
			{
              switch(st->ChgMathOper)
              {
			   case mtLESSEQ:
                find = prp_val <= st->ChgValue;
               break;
               case mtEQUAL:
                find = prp_val == st->ChgValue;
               break;
               case mtMOREEQ:
                find = prp_val >= st->ChgValue;
               break;
               case mtNOT:
				find = prp_val != st->ChgValue;
			  }
			}
		   if(find)
		   {
			 mes = mp->Name + _T(" : ") + Localization::Text(_T("mMiniscriptExecuted"))+
					_T(" '") + spt_name + _T("' ") + Localization::Text(_T("mOnChangeValueEvent"))+ _T(" '") + prpname + _T("'");
			 StartScript(spt_name, mes, mp, NULL, true, false);
		   }
		 }
		}
	   }
	   */
	}
  return true;
}
//----------------------------------------------------------------------------------

bool __fastcall TPlatformGameField::OnChangeNode(TMapGPerson *pr, TMapDirector *dir)
{
	int        i, len;
	bool       find;
	TGScript   *st;
	UnicodeString spt_name, key, mes;
	key = UnicodeString(::Actions::Names[::Actions::sptONCHANGENODE]);
	spt_name = pr->getScriptName(key);
	len = m_data.m_pObjManager->scripts->Count;

	/*
	if(!spt_name.IsEmpty())
	{
		find = false;
		for(i=0; i<len; i++)
		{
		  st = (TGScript*)m_data.m_pObjManager->scripts->Items[i];
		  if(st->Name.Compare(spt_name)==0)
		  {
		   find = st->Enabled;
		   break;
		  }
		}

		if(find)
		if(!st->CldPropNameInitiator.IsEmpty())
		{
			 if(pr != NULL)
			 {
			  double prp_val;
			  find = false;
			  if(pr->GetVarValue(&prp_val, st->CldPropNameInitiator))
			  switch(st->CldMathOperPropInitiator)
			  {
			   case mtLESSEQ:
				find = prp_val <= st->CldParamValueInitiator;
			   break;
			   case mtEQUAL:
				find = prp_val == st->CldParamValueInitiator;
			   break;
			   case mtMOREEQ:
				find = prp_val >= st->CldParamValueInitiator;
			   break;
			   case mtNOT:
				find = prp_val != st->CldParamValueInitiator;
			  }
			 }
			 else find = false;
		}

		if(find)
		if(!st->CldStateNameInitiator.IsEmpty())
		{
			 if(pr != NULL)
			 {
			  switch(st->CldMathOperStateInitiator)
			  {
			   case mtEQUAL:
				find = st->CldStateNameInitiator.Compare( pr->State_Name ) == 0;
			   break;
			   case mtNOT:
				find = st->CldStateNameInitiator.Compare( pr->State_Name ) != 0;
			  }
			 }
			 else find = false;
		}

		if(find)
		if(!st->CldPropNameSecond.IsEmpty())
		{
		 find = dir->Name.Compare(st->CldPropNameSecond) == 0;
		}

		if(find)
		{
			mes = pr->Name + _T(" : ") + Localization::Text(_T("mMiniscriptExecuted"))+
						_T(" '") + spt_name + _T("' ") + Localization::Text(_T("mOnEvent"))+ _T(" '") + key + _T("'");
			StartScript(spt_name, mes, pr, NULL, true, false);
		}
	}
    */
	return true;
}
//----------------------------------------------------------------------------------

