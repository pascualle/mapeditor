//---------------------------------------------------------------------------

#ifndef URenderThreadH
#define URenderThreadH
//---------------------------------------------------------------------------
#include <Classes.hpp>
//---------------------------------------------------------------------------

typedef void __fastcall (__closure *TThreadFunction)();

class TRenderThread : public TThread
{
private:
	TThreadFunction mpFn;

protected:
	virtual void __fastcall Execute();
	void __fastcall DoProcess();

public:
	__fastcall TRenderThread(TThreadFunction fn);

};
//---------------------------------------------------------------------------
#endif
