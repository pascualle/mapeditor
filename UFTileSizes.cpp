//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UTypes.h"
#include "UFTileSizes.h"
#include "MainUnit.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFTileSizes *FTileSizes;

//---------------------------------------------------------------------------
__fastcall TFTileSizes::TFTileSizes(TComponent* Owner)
        : TForm(Owner)
{
	CSpinEditPH = new TSpinEdit(GroupBox_f);
	CSpinEditPH->Parent = GroupBox_f;
	CSpinEditPH->Left = 269;
	CSpinEditPH->Top = 24;
	CSpinEditPH->Width = 81;
	CSpinEditPH->Height = 22;
	CSpinEditPH->MaxValue = MAX_TILE_SIZE;
	CSpinEditPH->MinValue = MIN_TILE_SIZE;
	CSpinEditPH->TabOrder = 0;
	CSpinEditPH->Value = DEF_TILE_SIZE;
	CSpinEditPH->OnChange = OnWHChange;
	CSpinEditPH->Visible = true;
	CSpinEditPW = new TSpinEdit(GroupBox_f);
	CSpinEditPW->Parent = GroupBox_f;
	CSpinEditPW->Left = 269;
	CSpinEditPW->Top = 48;
	CSpinEditPW->Width = 81;
	CSpinEditPW->Height = 22;
	CSpinEditPW->MaxValue = MAX_TILE_SIZE;
	CSpinEditPW->MinValue = MIN_TILE_SIZE;
	CSpinEditPW->TabOrder = 1;
	CSpinEditPW->Value = DEF_TILE_SIZE;
	CSpinEditPW->OnChange = OnWHChange;
	CSpinEditPW->Visible = true;
	CSpinEditLayers = new TSpinEdit(GroupBoxLayers);
	CSpinEditLayers->Parent = GroupBoxLayers;
	CSpinEditLayers->Left = 269;
	CSpinEditLayers->Top = 15;
	CSpinEditLayers->Width = 81;
	CSpinEditLayers->Height = 22;
	CSpinEditLayers->MaxValue = MAX_TILE_BG_LAYERS;
	CSpinEditLayers->MinValue = 1;
	CSpinEditLayers->TabOrder = 0;
	CSpinEditLayers->Value = 1;
	CSpinEditLayers->Visible = true;
	mW = mH = 0;

	Localization::Localize(this);
}
//---------------------------------------------------------------------------
void __fastcall TFTileSizes::FormDestroy(TObject *)
{
	delete CSpinEditPH;
	delete CSpinEditPW;
	delete CSpinEditLayers;
}
//---------------------------------------------------------------------------

void __fastcall TFTileSizes::FormShow(TObject *)
{
	mW = CSpinEditPW->Value;
	mH = CSpinEditPH->Value;
	OnWHChange(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFTileSizes::OnWHChange(TObject *)
{
	if(mW > 0 && mH > 0)
	{
		__int32 fw, sw, fh, sh;

		__int32 mw;
		__int32 mh;
		__int32 m_w, m_h, d_w, d_h;
		Form_m->GetMapSize(m_w, m_h);
		Form_m->GetDisplaySize(d_w, d_h);

		mw = CSpinEditPW->Value * (m_w / mW);
		if(mw > MAX_MAP_SIZE_PX)
		{
			CSpinEditPW->Value = (MAX_MAP_SIZE_PX / (m_w / mW));
		}
		mw = CSpinEditPW->Value * (m_w / mW);
		while(mw > MAX_MAP_SIZE_PX)
		{
			CSpinEditPW->Value = CSpinEditPW->Value - 1;
			mw = CSpinEditPW->Value * (m_w / mW);
		}

		mh = CSpinEditPH->Value * (m_h / mH);
		if(mh > MAX_MAP_SIZE_PX)
		{
			CSpinEditPH->Value = (MAX_MAP_SIZE_PX / (m_h / mH));
		}
		mh = CSpinEditPH->Value * (m_h / mH);
		while(mh > MAX_MAP_SIZE_PX)
		{
			CSpinEditPH->Value = CSpinEditPH->Value - 1;
			mh = CSpinEditPH->Value * (m_h / mH);
		}

		if(CSpinEditPW->Value > mW)
		{
			fw = CSpinEditPW->Value;
			sw = mW;
		}
		else
		{
			sw = CSpinEditPW->Value;
			fw = mW;
		}
		if(CSpinEditPH->Value > mH)
		{
			fh = CSpinEditPH->Value;
			sh = mH;
		}
		else
		{
			sh = CSpinEditPH->Value;
			fh = mH;
		}
		bool val = (fw % sw) == 0 &&
					(fh % sh) == 0 &&
			(d_w % CSpinEditPW->Value) == 0 &&
			(d_h % CSpinEditPH->Value) == 0 &&
			d_w >= CSpinEditPW->Value &&
			d_h >= CSpinEditPH->Value;
		CheckBoxRemainMapSize->Enabled = val;
		if(val == false && CheckBoxRemainMapSize->Checked)
		{
			CheckBoxRemainMapSize->Checked = false;
		}
	}
}
//---------------------------------------------------------------------------

