object FObjPoolProperty: TFObjPoolProperty
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'mGameObjectProperties'
  ClientHeight = 466
  ClientWidth = 385
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  KeyPreview = True
  Position = poDefault
  ScreenSnap = True
  OnHide = FormHide
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  TextHeight = 13
  object ImageLock: TImage
    Left = 3
    Top = 446
    Width = 19
    Height = 19
    Picture.Data = {
      07544269746D617032050000424D320500000000000032040000280000001000
      000010000000010008000000000000010000120B0000120B0000FF000000FF00
      00000303020026262700036902006D340E000877C6000909F300C6C0B700B520
      810035030600D68F6B000BF7FB003D3D3D007B7B7A00188FBB00526ECA00CBCB
      CA00F2A83C00490809003344AD009A9A9A0052525200C18129004B50AA004859
      CF007A6B5400333333001DA1DE00F9F7C500896C340015154C007B922E007075
      9D006E6EE800DEDCDC00F8E80600D050D8005F60240002CCFE00929393000853
      07001C1D1D003A3AAF008B8B8A0067472D00FBFBFB0094918E00FBF907002A2A
      0200F1F02200FEFEFE00FAF29D00A695570059592400808081008F704400F9BC
      02004D4D4D00BAF2F80066420800C1BDB80084C2C9004E6D710047480E00D6D6
      D400755B54004A4845002E3B9C00B1B1B000A9A9A8005656550091ACB100F1F1
      F000656564009888600030310200A2A2F4007474740079622E00292984004A47
      2B006B6B6B0019272C0001A0FC00A89D9000383837003B3B000000007B0000B7
      00000C0A9000C1F0FC00452F0000FAFAE60059595A00361E060004033500E177
      3000FBCA060090481800BE938500EFD5C500B2A5A200FCF4F200E2CCC7004C45
      4500DBC0AF00FDFBFB0054452F0018180100926E6600796A6900FDFDFD00928D
      7400EF9D2800676768004A341F00424648004C7FB4003C2C1B00DAD4CE00ACA2
      980045829100572B10006C6C310000990000D6CB14002F303000FAF26C003363
      9B009E928F00F3EDE70077DAF300716161000A9DE1008D8558006EA5E0001EAD
      E900AAAAB700665844005A54350093867A002C3D40007C736A00EBE1AB004F84
      8A0060AEF9005D9BA2008057090010151300415A5D0089544400BFB580005888
      F600DCD89E00CECD8600B3865100E7DCD900A6BF1F00B6CAF800DACDC9008F8F
      4900DDBE8200DBBEAA007F603C00BCBCB80012206100BE9F5400C75B710000CC
      000000330000CFB64F0011B01A00C19F4C000099FF0015A80D009B8F82002233
      88000FFF0300F9F5AF005F808500E8D8CD00564210002BE31300E8DBA3007F82
      8100A77B5D008C837700AC87560087BEDB0077214700BBB2A70051511B00FF9B
      1A00FB6002000D5CBF009B923700FBDC090055410F00FDFDFB0000F75600A6A9
      4C00D4F80F00DFDF8F0093934100FFFFFF0099FFFF008389BE00808F9200565B
      60007DF68400F8BC8A0033449900AAA17100C2CFFC00939AD60003BAFE008B9C
      9E0058301800412A1200DFC3B300F6F7F800F29DF700DAAB79000705C2007272
      3E009A9AFE00D8CC6200EFEFEF005C3F3E00716F6C00AA9A8B00222266005D72
      2200AF8B33008CBAE400F6D4F800FFD74C000A9CD20086CAEA00E8D09700FFFF
      FF00F0F0F000F1F1F100F2F2F200F3F3F300F4F4F400F5F5F500F6F6F600F7F7
      F700F8F8F800F9F9F900FAFAFA00FBFBFB00FCFCFC00FDFDFD00FEFEFE002323
      2323232323232323232323232323232323232D2D0C0C0C0C2D2D232323232323
      232D41540101010154412D2323232323232D54010154540101542D2323232323
      230C0101010B0B0101010C2323232323230C0101010B0B0101010C2323232323
      230C0101010B0B0101010C2323232323230C01010101010101010C2323232323
      230C01010101010101010C2323232323230C0B0B0B0B0B0B0B0B0C2323232323
      23230C4123232323410C23232323232323230C4123232323410C232323232323
      23230C4123232323410C23232323232323232D410C23230C412D232323232323
      2323232D414141412D232323232323232323232323232323232323232323}
    Transparent = True
  end
  object GroupBoxPos: TGroupBox
    Left = 214
    Top = 392
    Width = 167
    Height = 51
    Caption = 'mPositionOnMapLabel'
    TabOrder = 1
    object Label12: TLabel
      Left = 5
      Top = 23
      Width = 10
      Height = 13
      Caption = 'X:'
    end
    object Label14: TLabel
      Left = 73
      Top = 23
      Width = 10
      Height = 13
      Caption = 'Y:'
    end
  end
  object ListView: TListView
    Left = 0
    Top = 26
    Width = 385
    Height = 361
    Align = alTop
    Columns = <
      item
        Caption = 'mObjGroupType'
        Width = 180
      end
      item
        Caption = 'mCount'
        Width = 150
      end>
    ColumnClick = False
    GridLines = True
    ReadOnly = True
    RowSelect = True
    TabOrder = 2
    ViewStyle = vsReport
    OnClick = ListViewClick
    OnDblClick = ListViewDblClick
    OnEnter = ListViewEnter
    OnExit = ListViewExit
    OnKeyDown = ListViewKeyDown
    ExplicitWidth = 364
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 385
    Height = 26
    Caption = 'ToolBar1'
    EdgeBorders = [ebTop, ebBottom]
    Images = Form_m.ImageList1
    TabOrder = 3
    ExplicitWidth = 364
    object ToolButton5: TToolButton
      Left = 0
      Top = 0
      Action = ActNewItem
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton2: TToolButton
      Left = 23
      Top = 0
      Action = ActEditItem
    end
    object ToolButton1: TToolButton
      Left = 46
      Top = 0
      Action = ActDelItem
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton3: TToolButton
      Left = 69
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 77
      Top = 0
      Action = ActionDown
    end
    object ToolButton6: TToolButton
      Left = 100
      Top = 0
      Action = ActionUp
    end
  end
  object GroupBox_name: TGroupBox
    Left = 2
    Top = 392
    Width = 207
    Height = 51
    Caption = 'mPrpObjName'
    TabOrder = 0
    DesignSize = (
      207
      51)
    object BitBtn_find_obj: TBitBtn
      Left = 180
      Top = 19
      Width = 24
      Height = 21
      Hint = 'mFindOnMap'
      Anchors = [akTop, akRight]
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000FC02FC004985
        D40084C6F400C7D1D100FAEFC700B08D8800B5B8BC00FCFCDA0056595B005CB6
        F10072758400868F9200F5D7B000B6C8CE00D3B39E008E787C000BAA60000000
        0000BA58A6000000000091A58A6000000000D91AF8A6000000000291A58BDDDD
        000000291AF8AA88ABD0000291BF5ECEA8FD0000265C47774F8B000006C44777
        748A00000E4C7777775800000E44477777EA00000C44477777EA00000E44C477
        745B000003C7744C4CFD000000E47444C5D000000006EEEEE300}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BitBtn_find_objClick
    end
    object Edit_name: TEdit
      Left = 4
      Top = 19
      Width = 174
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      CharCase = ecUpperCase
      MaxLength = 64
      TabOrder = 0
      OnChange = Edit_nameChange
      OnEnter = Edit_nameEnter
      OnExit = Edit_nameExit
      OnKeyPress = Edit_nameKeyPress
    end
  end
  object ActionList1: TActionList
    Images = Form_m.ImageList1
    Left = 32
    Top = 288
    object ActNewItem: TAction
      Caption = 'mAdd'
      Hint = 'mAdd'
      ImageIndex = 5
      OnExecute = ActNewItemExecute
    end
    object ActDelItem: TAction
      Caption = 'mDoDelete'
      Hint = 'mDoDelete'
      ImageIndex = 6
      OnExecute = ActDelItemExecute
    end
    object ActEditItem: TAction
      Caption = 'mEdit'
      Hint = 'mEdit'
      ImageIndex = 7
      OnExecute = ActEditItemExecute
    end
    object ActionUp: TAction
      Caption = 'mUp'
      Hint = 'mUp'
      ImageIndex = 36
      OnExecute = ActionUpExecute
    end
    object ActionDown: TAction
      Caption = 'mDown'
      Hint = 'mDown'
      ImageIndex = 35
      OnExecute = ActionUpExecute
    end
  end
end
