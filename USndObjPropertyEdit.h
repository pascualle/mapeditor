//---------------------------------------------------------------------------

#ifndef USndObjPropertyEditH
#define USndObjPropertyEditH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Dialogs.hpp>

//---------------------------------------------------------------------------
class TFSOPEdit : public TForm
{
__published:
	TLabel *Label_name;
	TEdit *EditPrefix;
	TEdit *Ed_name;
	TLabel *Label_namefile;
	TButton *ButtonOk;
	TEdit *Edit_filename;
	TComboBox *ComboBox_type;
	TLabel *Label_type;
	TBevel *Bevel1;
	TButton *BtG_3dot;
	void __fastcall Ed_nameChange(TObject *Sender);
	void __fastcall BtG_3dotClick(TObject *Sender);
	void __fastcall ButtonOkClick(TObject *Sender);

 private:

 public:

	void __fastcall SetName(UnicodeString txt);
	UnicodeString __fastcall GetName();

	__fastcall TFSOPEdit(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFSOPEdit *FSOPEdit;
//---------------------------------------------------------------------------
#endif
