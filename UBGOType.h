//---------------------------------------------------------------------------

#ifndef UBGOTypeH
#define UBGOTypeH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Vcl.Samples.Spin.hpp>
#include "UObjCode.h"
//---------------------------------------------------------------------------
class TFEditBGOType : public TForm
{
__published:	// IDE-managed Components
	TButton *ButtonCancel;
	TButton *ButtonOk;
	TCheckBox *CheckBoxSolid;
	TCheckBox *CheckBoxDirector;
	TLabel *Label;
	TLabel *Label5;
	TEdit *Edit_name;
	void __fastcall Edit_nameChange(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
private:

public:
	TSpinEdit *CSpinEditType;
	void __fastcall SetBackProperty(TBGobjProperties *prp);
	void __fastcall GetBackProperty(TBGobjProperties *prp);
	__fastcall TFEditBGOType(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFEditBGOType *FEditBGOType;
//---------------------------------------------------------------------------
#endif

