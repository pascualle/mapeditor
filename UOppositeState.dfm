object FOppState: TFOppState
  Left = 366
  Top = 307
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'mCondition'
  ClientHeight = 163
  ClientWidth = 323
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 37
    Top = 18
    Width = 67
    Height = 13
    Caption = 'mPrpObjState'
  end
  object Label2: TLabel
    Left = 37
    Top = 58
    Width = 110
    Height = 13
    Caption = 'mPrpObjOppositeState'
  end
  object ButtonCancel: TButton
    Left = 65
    Top = 124
    Width = 76
    Height = 25
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 0
  end
  object ButtonOk: TButton
    Left = 147
    Top = 124
    Width = 110
    Height = 25
    Caption = 'mApply'
    TabOrder = 1
    OnClick = ButtonOkClick
  end
  object ComboBoxState: TComboBox
    Left = 37
    Top = 32
    Width = 248
    Height = 21
    Style = csDropDownList
    TabOrder = 2
  end
  object ComboBoxInvState: TComboBox
    Left = 37
    Top = 72
    Width = 248
    Height = 21
    Style = csDropDownList
    TabOrder = 3
  end
end
