﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <malloc.h>
#include "Clipbrd.hpp"
#include "MainUnit.h"
#include "UnitOpt.h"
#include "inifiles.hpp"
#include "UFragmentProperty.h"
#include "UObjProperty.h"
#include "UAbour.h"
#include "UFPath.h"
#include "USaveVarList.h"
#include "UObjParent.h"
#include "UnitEditorOpt.h"
#include "UObjOrder.h"
#include "UCreateMessages.h"
#include "UScript.h"
#include "USriptList.h"
#include "USpObjProperty.h"
#include "UStateList.h"
#include "UObjCode.h"
#include "UMapCollideLines.h"
#include "UEditObj.h"
#include "UObjPattern.h"
#include "UGameField.h"
#include "UObjManager.h"
#include "UnitScale.h"
#include "UDirector.h"
#include "UFTileSizes.h"
#include "UBGEditWindow.h"
#include "URenderGL.h"
#include "UTextManager.h"
#include "UWait.h"
#include "URenderThread.h"
#include "UTxtObjProperty.h"
#include "UEditObjPattern.h"
#include <DateUtils.hpp>
#include "UnitFindObj.h"
#include "USndObjProperty.h"
#include "UObjPoolProperty.h"
#include "UCollideNodeProperty.h"
#include "UnitJNCount.h"
#include "ULocalization.h"
#include "USave.h"
#include "ULoad.h"
#include <math.hpp>

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"

static const __int32 MARKER_DURATION = 5;

enum
{
	drSTATIC = 1,
	drOBJECTS,
	drCOLLIDELINES,
	drNONE
};

enum
{
	tagIsMAP = 999,
	tagOBJECTS
};

static const __int32 HINT_SHOW_TIME = 10;

static const __int32 MINIMAL_TIME_TO_UPDATE_ANIMATIONS = 10;

static const __int32 IMAGELIST_TRIGGER_IDX = 0;
static const __int32 IMAGELIST_DIRECTOR_IDX = 1;
static const __int32 IMAGELIST_TEXTBOX_IDX = 2;
static const __int32 IMAGELIST_SOUNDSCHEME_IDX = 3;
static const __int32 IMAGELIST_OBJPOOL_IDX = 4;

static const __int32 DELAY_TO_LOAD_TEXTURES_GL_TICKS = 5;

static bool mnuChange = false;

const _TCHAR* GLOBAL_OJB_NAME = _T("mGlobalZoneIndependend");
const _TCHAR PARENT_TYPE_SYSTEM_NAME[] = _T("mGlobalSystem");
const _TCHAR PARENT_TYPE_DEFAULT_NAME_PREFIX[] = _T("mGlobalCommon");

static TStringList *mpgGlogalStrings;

TForm_m *Form_m;
static TLMBtn LMBtn_main;

double gsGlobalScale = 1.0f;

static bool ignoreTabLevelsChange = false;

#define CALC_IMAGE_SIZE(ww, hh)(ww * hh * 4)

#define TEMP_BG_TILE_RES_NAME _T("?temp_bg_tile\0")
#define TEMP_OBJ_ERROR_RES_NAME _T("?obj_err_bmp\0")

#define RAWRES_NAME L"rawres"

static const int gsPrpWindowsCount = 7;
static TPrpFunctions* gsPrpWindows[gsPrpWindowsCount] = {};

TFx32size mFx32size;
unsigned __int32 mWcharsizeExternal = 0;
unsigned __int32 mWcharsize = 4;
TDateTime gCurrentTimeMark = 0;

//---------------------------------------------------------------------------

//class TForm_m
__fastcall TForm_m::TForm_m(TComponent *Owner)
:	TForm(Owner)
,	p_w(DEF_TILE_SIZE)
,	p_h(DEF_TILE_SIZE)
,	d_w(0)
,	d_h(0)
,	m_w(0)
,	m_h(0)
,	m_px(-1)
,	m_py(-1)
,	lrCount(1)
,	proj_exist(false)
,	map_exist(false)
,	drag_on_PaintBox(false)
,	is_save(false)
,	showGrid(true)
,   gridColor(TFEditorOptions::DEF_GRID_COLOR)
,	showRectGameObj(true)
,	showObjects(true)
,   showCollideMap(true)
,	m_pTempObjManager(NULL)
,	m_pObjManager(NULL)
,	m_CopyBGBuffer(NULL)
,	ScriptsWndAlreadyExist(false)
,	mDirAssetIndex(-1)
,	mDelayCounterLoadTexturesToVRAMGL(-1)
,	mpCurrentGLForm(NULL)
,   mpBGBmp(NULL)
,   mpBackBufferBmp(NULL)
{
	gUsingOpenGL = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::FormCreate(TObject *)
{
	Lang = INTERFACE_LANG_DEFAULT;

	FObjParent = NULL;
	FObjOrder = NULL;

	TabSetObjInit();

	wchar_t buf[MAX_PATH];
	GetCurrentDirectoryW(MAX_PATH, buf);
	PathIni = CheckPathWithDelimeter(UnicodeString(buf), false);

	mDoNotDraw = mUpdateCursor = mNeedRebuildBack = false;

	mTileTmpBmp_idx = -1;
	mainCommonRes = new TCommonResObjManager();
	mpTexts = new TCommonTextManager();
	markerList = new TList();
	images_u = new TCustomImageList(this);
	m_pLevelList = new TList();

	mpFinalRenderCanvas = ((TMyPanel*)PaintBox1)->GetCanvas();
	mpBackPatternBmp = CreatePatternImage(clGray);
	mOldPaintBox1WndProc = PaintBox1->WindowProc;
	PaintBox1->WindowProc = CustomPaintBox1WndMethod;

	this->Tag = NativeInt(tagIsMAP);
	ListView_u->Tag = NativeInt(drOBJECTS);
	ListBoxZone->Tag = NativeInt(drNONE);

	map_file_name = "";
	h_file_name = "";
	proj_file_name = PathIni;
	xml_proj_file_name = PathIni;
	PathPngRes = _T("");
	PathPngRes0 = _T("");
	PathPattern = _T("");
	PathObjPalette = _T("");
	lvWHSizeIcon = 32;
	images_u->Masked = false;
	images_u->BkColor = clNone;
	images_u->BlendColor = clNone;
	images_u->ColorDepth = cd32Bit;
	images_u->SetSize(lvWHSizeIcon, lvWHSizeIcon);
	ListView_u->LargeImages = images_u;

	DragObjList = new TList();
	DragObjListClear();
	disabled_controlls = true;
	PaintBox1->Top = ToolBar->Height + TabLevels->Height;
	PaintBox1->Left = 0;

	crCursor.Left = -1;
	crCursor.Top = -1;
	crVisible = false;
	objDrag = false;

	mpGrObjWndParams = mpGrBGWndParams = NULL;
	mpEdPrObjWndParams = new TEdPrObjWndParams();
	mpEdPrObjWndParams->init = false;
	mpEdPrObjWndParams->joinNodesCount = DEF_ANIMATON_JOIN_NODES;

	tickDuration = DEFAULT_TICK_DURATION;

	oldZone = znCurZone = -1;
	HorzScrollBar_Position = VertScrollBar_Position = 0;
	prev_dr_mode = dr_mode = drNONE;

	ListView_u->Enabled = false;
	m_pGameField = NULL;
	LoadSaveProcess = false;
	showOBJDirectors = true;

	ActStopOnScript->Checked = false;

	CBMouseEnter = false;
	DrawMapProcess = PaintBox1MouseMove_process = false;

	SetScale(1.0f);
	DeleteCopyBGBuffer();

	mHintTime = -1;
	mpHintWnd = new THintWindow(this);
	mpHintWnd->Color = Application->HintColor;

	CF_ME = (WORD)RegisterClipboardFormat(_T("MapEditorFormat"));
	mpCopyBuffer = new TMemoryStream();

	FakeControl->Left = -100;
	FakeControl->Top = -100;

	ClearCopySelection();

	mFPSCounter = 0;
	mFPSVal = 0;
	mTickPreviousTime = 0;
	mTickDuration = 0;
	mRenderTickDuration = 0;
	mFPSTimer = 0;
	mShowFPS = false;
	mExitApp = false;

	ActAniEnableObjExecute(NULL);
	mAnimationStateBeforeEmul = mAnimationEnable;

	mCurrentLanguage = TEXT_NAME_DEFAULT;

	Application->OnMessage = OnMessage;
	Application->OnIdle = IdleLoop;
	Application->OnMinimize = AppMinimize;
	Application->OnRestore = AppRestore;

    mSaveProgDataXMLFormat = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::FormDestroy(TObject * /* Sender */ )
{
	if (mainCommonRes != NULL)
	{
		mainCommonRes->ClearGraphicResources();
	}
	ReleaseRenderGL();

	Timer_cr->Enabled = false;
	if (m_pGameField != NULL)
	{
		delete m_pGameField;
    }
	m_pGameField = NULL;

	if(mpBGBmp != NULL)
	{
		delete mpBackBufferBmp;
		delete mpBGBmp;
		mpBGBmp = NULL;
		mpBackBufferBmp = NULL;
	}

	ListView_u->Items->Clear();
	images_u->Clear();
	delete images_u;
	images_u = NULL;

	for (__int32 i = 0; i < m_pLevelList->Count; i++)
	{
		delete (TObjManager*)m_pLevelList->Items[i];
	}
	delete m_pLevelList;

	mpHintWnd->ReleaseHandle();
	delete mpHintWnd;

	if (NULL != m_pTempObjManager)
	{
		delete m_pTempObjManager;
	}
	if (mpGrBGWndParams != NULL)
	{
		delete mpGrBGWndParams;
	}
	if (mpGrObjWndParams != NULL)
	{
		delete mpGrObjWndParams;
	}
	delete mpEdPrObjWndParams;
	if (NULL != mpTexts)
	{
		delete mpTexts;
	}
	if (mainCommonRes != NULL)
	{
		delete mainCommonRes;
	}
	delete mpBackPatternBmp;
	FreeDebugPopupScriptList();
	FreePopupMoveTo();
	FreeLanguageMenu();
	ClearMarkers();
	delete markerList;
	DeleteCopyBGBuffer();
	delete DragObjList;
	delete mpCopyBuffer;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActExitExecute(TObject * /* Sender */ )
{
	Close();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::AppMinimize(TObject*)
{
	TRenderGL::Enable(false);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::AppRestore(TObject*)
{
	TRenderGL::Enable(true);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DeleteCopyBGBuffer()
{
	if (m_CopyBGBuffer != NULL)
	{
		for (__int32 i = 0; i < m_CopyRectH; i++)
		{
			delete[]m_CopyBGBuffer[i];
        }
		delete[]m_CopyBGBuffer;
		m_CopyBGBuffer = NULL;
		m_CopyRectW = m_CopyRectH = 0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ClearCopySelection()
{
	m_CopyRectSet = false;
	m_CopyRectCX = m_CopyRectCY = -1;
	m_CopyRect.Left = 0;
	m_CopyRect.Top = 0;
	m_CopyRect.Bottom = 0;
	m_CopyRect.Right = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::changeSizes()
{
	if (mpBGBmp == NULL && gUsingOpenGL == false)
	{
		return;
	}

	__int32 ov, oh;
	ov = VertScrollBar->Position;
	oh = HorzScrollBar->Position;
	VertScrollBar->Position = 0;
	HorzScrollBar->Position = 0;
	HorzScrollBar->Left = 0;
	VertScrollBar->Top = ToolBar->Height + TabLevels->Height; //0
	HorzScrollBar->Width = /* Splitter2->Left- */ this->ClientWidth - VertScrollBar->Width;
	VertScrollBar->Height = Splitter->Top - HorzScrollBar->Height - ToolBar->Height - TabLevels->Height;
	HorzScrollBar->Top = VertScrollBar->Height + VertScrollBar->Top;
	VertScrollBar->Left = HorzScrollBar->Width;
	Sh_sqr->Top = VertScrollBar->Height + VertScrollBar->Top;
	Sh_sqr->Left = HorzScrollBar->Width + HorzScrollBar->Left;
	Sh_sqr->Width = VertScrollBar->Width + 1;
	Sh_sqr->Height = HorzScrollBar->Height + 1;
	if (HorzScrollBar->Max < HorzScrollBar->Width)
	{
		HorzScrollBar->PageSize = 0;
		HorzScrollBar->Position = 0;
		HorzScrollBar->Enabled = true;
		HorzScrollBar->Enabled = false;
	}
	else
	{
		HorzScrollBar->Enabled = true;
		HorzScrollBar->PageSize = HorzScrollBar->Width;
	}

	if (VertScrollBar->Max < VertScrollBar->Height)
	{
		VertScrollBar->PageSize = 0;
		VertScrollBar->Position = 0;
		VertScrollBar->Enabled = true;
		VertScrollBar->Enabled = false;
	}
	else
	{
		VertScrollBar->Enabled = true;
		VertScrollBar->PageSize = VertScrollBar->Height;
	}

	if (!gUsingOpenGL)
	{
		if (p_h > 0)
		{
			__int32 t = VertScrollBar->Height + Ceil((double)p_h * gsGlobalScale) -
						(VertScrollBar->Height % Ceil((double)p_h * gsGlobalScale));
			if (mpBGBmp->Height != t)
			{
				mpBGBmp->Height = t;
			}
		}
		else
		{
			mpBGBmp->Height = 0;
		}

		if (p_w > 0)
		{
			__int32 t = HorzScrollBar->Width + Ceil((double)p_w * gsGlobalScale) -
						(HorzScrollBar->Width % Ceil((double)p_w * gsGlobalScale));
			if (mpBGBmp->Width != t)
			{
				mpBGBmp->Width = t;
			}
		}
		else
		{
			mpBGBmp->Width = 0;
		}

		if (mpBackBufferBmp->Height != VertScrollBar->Height)
		{
			mpBackBufferBmp->Height = VertScrollBar->Height;
		}
		if (mpBackBufferBmp->Width != HorzScrollBar->Width)
		{
			mpBackBufferBmp->Width = HorzScrollBar->Width;
		}
	}

	if (VertScrollBar->Enabled)
	{
		if (ov + VertScrollBar->PageSize > VertScrollBar->Max)
			ov = VertScrollBar->Max - VertScrollBar->PageSize;
		VertScrollBar->Position = ov;
	}
	if (HorzScrollBar->Enabled)
	{
		if (oh + HorzScrollBar->PageSize > HorzScrollBar->Max)
			oh = HorzScrollBar->Max - HorzScrollBar->PageSize;
		HorzScrollBar->Position = oh;
	}

	VertScrollBar_Position = VertScrollBar->Position;
	HorzScrollBar_Position = HorzScrollBar->Position;

	if (PaintBox1->Height != VertScrollBar->Height)
	{
		PaintBox1->Height = VertScrollBar->Height;
	}
	if (PaintBox1->Width != HorzScrollBar->Width)
	{
		PaintBox1->Width = HorzScrollBar->Width;
	}

	DrawMap(true);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::Splitter2Moved(TObject * /* Sender */ )
{
	changeSizes();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::FormResize(TObject * /* Sender */ )
{
	changeSizes();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CustomPaintBox1WndMethod(Winapi::Messages::TMessage &Message)
{
	switch(Message.Msg)
	{
		case WM_ERASEBKGND:
			Message.Result = 1;
		break;
		case WM_SIZE:
		{
			if (gUsingOpenGL)
			{
				TRect v;
				v.Top = 0; //ToolBar->Height + TabLevels->Height;
				v.Left = 0;
				v.Bottom = HorzScrollBar->Top;
				v.Right = VertScrollBar->Left;
				TRenderGL::Resize(v, PaintBox1->ClientRect);
			}
			if (map_exist && m_pObjManager != NULL)
			{
				m_pObjManager->RealignPosAll();
				DrawMap(true);
			}
			Message.Result = 0;
		}
		break;
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			if(TRenderGL::IsEnabled())
			{
				HDC hdc = BeginPaint(PaintBox1->Handle, &ps);
				DoTick();
				ApplyRenderBuffer(hdc);
				EndPaint(PaintBox1->Handle, &ps);
			}
			else
			{
				BeginPaint(PaintBox1->Handle, &ps);
				EndPaint(PaintBox1->Handle, &ps);
			}
			Message.Result = 0;
		}
		break;
		default:
			mOldPaintBox1WndProc(Message);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::FormClose(TObject * /* Sender */ , TCloseAction &Action)
{
	if (AskToSave() == ID_CANCEL)
	{
		Action = caNone;
		return;
	}

    mExitApp = true;
	mDoNotDraw = true;

	Action = caFree;
}
//---------------------------------------------------------------------------

__int32 __fastcall TForm_m::AskToSave()
{
  __int32 res;
	if (!is_save)
  {
		return ID_NO;
  }
	res = Application->MessageBox(Localization::Text(_T("mAskBeforeSave")).c_str(),
                                  Localization::Text(_T("mMessageBoxSave")).c_str(),
                                  MB_YESNOCANCEL | MB_ICONQUESTION);
	switch(res)
	{
	case ID_YES:
		ActProjSaveAs->Execute();
		return ID_NO;
	default:
		return res;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::InitRenderGL()
{
	if (gUsingOpenGL)
	{
		TRect v;
		v.Top = 0;
		v.Left = 0;
		v.Bottom = HorzScrollBar->Top;
		v.Right = VertScrollBar->Left;
		TRenderGL::InitWindow(PaintBox1->Handle, v, PaintBox1->ClientRect, true);
		LoadTexturesToVRAMGL();
		TRenderGL::SetActiveCommonResObjManager(mainCommonRes);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ReleaseRenderGL()
{
    mDelayCounterLoadTexturesToVRAMGL = -1;
	TRenderGL::ReleaseWindows();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::SetChildRenderGLForm(TRenderGLForm* pForm)
{
	mpCurrentGLForm = pForm;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::LoadTexturesToVRAMGL()
{
	if(TRenderGL::IsInited())
	{
		mDelayCounterLoadTexturesToVRAMGL = DELAY_TO_LOAD_TEXTURES_GL_TICKS;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::FormShow(TObject * /* Sender */ )
{
	TPrpFunctions* const _windowsPtr[] =
	{
		FGOProperty,
		FDirectorProperty,
		FSpObjProperty,
		FormTxtObjProperty,
		FSndObjProperty,
		FObjPoolProperty,
		FCollideNodeProperty
	};

	for(int i = 0; i < gsPrpWindowsCount; i++)
	{
		gsPrpWindows[i] = _windowsPtr[i];
	}

	bool init = false;
	PaintBox1->Visible = false;
	TRenderGL::Enable(false);

	Localization::Init();
	ReadIniData(true);
	if(Localization::GetLangName(Lang).IsEmpty() == true)
	{
		Lang = INTERFACE_LANG_DEFAULT;
	}
	Localization::Load(Lang);

    LocalizeGlobalConsts();

	// localize TForm_m
	Localization::Localize(ActionList1);
	Localization::Localize(PopupMenu_u);
	Localization::Localize(PopupMenu_img);
	Localization::Localize(PopupMenu_s);
	Localization::Localize(PopupMenuParentTabs);

	// localize autocreated windows
	FBGObjWindow->Localize();
	FormFindObj->Localize();

	int prpwCount = sizeof(gsPrpWindows) / sizeof(TForm*);
	for (int i = 0; i < prpwCount; i++)
	{
		gsPrpWindows[i]->Localize();
	}

	FObjOrder->Localize();

	TabSetObjInit();

	SetDrMode(drNONE);
	ActEditMode->Execute();

	//загрузка из командной строки
	wchar_t *args = GetCommandLine();
	if (args != NULL)
	{
		__int32 i;
		const UnicodeString outdir = _T("-o");
		const UnicodeString inifile = _T("-i");
		const UnicodeString logfile = _T("-l");
		const UnicodeString projfile = _T("-p");
		const UnicodeString wchar_sz = _T("-wchar_size");
		UnicodeString outdir_path;
		UnicodeString inifile_path;
		UnicodeString logfile_path;
		UnicodeString projfile_path;
		UnicodeString errmes;
		bool consoleMode = false;
		bool startFromCL = false;
		bool forceClose = false;
		TStringList *arglist = new TStringList();
		arglist->Delimiter = _T(' ');
		arglist->DelimitedText = UnicodeString(args).LowerCase().Trim();
		for(i = 1; i < arglist->Count; i++)
		{
			UnicodeString arg = arglist->Strings[i].Trim();
			if(arg.IsEmpty() == false)
			{
				if(arg.Pos(outdir) == 1)
				{
					consoleMode = true;
					outdir_path = arg.SubString(3, arg.Length() - 2);
					if(outdir_path[outdir_path.Length()] != L'\\' &&
						outdir_path[outdir_path.Length()] != L'/')
						{
							outdir_path = outdir_path + _T("\\");
						}
				}
				if(arg.Pos(inifile) == 1)
				{
					consoleMode = true;
					inifile_path = arg.SubString(3, arg.Length() - 2);
					if(inifile_path[inifile_path.Length()] != L'\\' &&
						inifile_path[inifile_path.Length()] != L'/')
						{
							inifile_path = inifile_path + _T("\\");
						}
				}
				if(arg.Pos(logfile) == 1)
				{
					consoleMode = true;
					logfile_path = arg.SubString(3, arg.Length() - 2);
				}
				if(arg.Pos(projfile) == 1)
				{
					projfile_path = arg.SubString(3, arg.Length() - 2);
					const UnicodeString ext = ExtractFileExt(projfile_path).LowerCase();

					if(ext == _T(".mpf"))
					{
						projfile_path = ChangeFileExt(projfile_path, _T(".mpf"));
					}
					else if(ext == _T(".meproj"))
					{
						projfile_path = ChangeFileExt(projfile_path, _T(".meproj"));
					}
					else
					{
						forceClose = true;
					}
				}
				if(arg.Pos(wchar_sz) == 1)
				{
					const UnicodeString wchar_sz_value = arg.SubString(wchar_sz.Length() + 1, arg.Length() - 2);
					try
					{
						mWcharsizeExternal = wchar_sz_value.ToInt();
						if(mWcharsizeExternal != 2 && mWcharsizeExternal != 4)
						{
							mWcharsizeExternal = 0;
						}
					}
					catch(...)
					{
						mWcharsizeExternal = 0;
					}
				}
			}
		}
		i = arglist->Count;
		delete arglist;

		if(!projfile_path.IsEmpty() && !inifile_path.IsEmpty() && outdir_path.IsEmpty() && i > 1)
		{
			startFromCL = true;
			consoleMode = false;
			if (!FileExists(projfile_path))
			{
				forceClose = true;
			}
			if(!DirectoryExists(inifile_path))
			{
				forceClose = true;
			}

			if(forceClose == false)
			{
				if(IsRelativePath(projfile_path))
				{
					projfile_path = ExpandFileName(projfile_path);
				}
				if(IsRelativePath(inifile_path))
				{
					inifile_path = ExpandFileName(inifile_path);
				}
			}
		}
		else
		if((projfile_path.IsEmpty() || inifile_path.IsEmpty() || outdir_path.IsEmpty()) && i > 1)
		{
			consoleMode = true;
			errmes = Localization::Text(_T("mCommandLineHelp"));
			Application->MessageBox(errmes.c_str(),
                              Localization::Text(_T("mMessageBoxInfo")).c_str(),
                              MB_OK | MB_ICONINFORMATION);
		}

		errmes = _T("\0");
		if(consoleMode)
		{
			if(FileExists(projfile_path) == false)
			{
				errmes = errmes + Localization::Text(_T("mErrorProjPath")) + projfile_path + _T("\n\0");
			}
			if(DirectoryExists(inifile_path) == false)
			{
				errmes = errmes + Localization::Text(_T("mErrorIniPath")) + inifile_path + _T("\n\0");
			}
			if(DirectoryExists(outdir_path) == false)
			{
				errmes = errmes + Localization::Text(_T("mErrorMpfFolderPath")) + outdir_path + _T("\n\0");
				outdir_path = _T("\0");
      		}
			if(DirectoryExists(ExtractFilePath(logfile_path)) == false)
			{
				logfile_path = _T("\0");
			}
		}
		else
		{
			if(projfile_path.IsEmpty() == false && FileExists(projfile_path) == false && startFromCL == false)
			{
				errmes = _T("error");
			}
		}

		if(errmes.IsEmpty())
		{
			if(projfile_path.IsEmpty())
			{
				ReadIniData();
				RefreshFileName();
				changeSizes();
				ActEditMode->Execute();
				MainMenu1Change(NULL, NULL, false);
			}
			else
			{
				if(consoleMode)
				{
					PathIni = CheckPathWithDelimeter(inifile_path, false);
					ReadIniData();
					RefreshFileName();
				}
				else
				{
					if(startFromCL)
					{
						PathIni = CheckPathWithDelimeter(inifile_path, false);
					}
					ReadIniData();
					RefreshFileName();
					changeSizes();
					ActEditMode->Execute();
				}

				init = true;
				LoadSaveProcess = true;
				ProjLoad(projfile_path, true, &errmes);
				LoadSaveProcess = false;

				if(consoleMode)
				{
					if(errmes.IsEmpty())
					{
						MapSave(&outdir_path, true, &errmes);
					}
				}
				else
				{
					DrawMap(true);
					MainMenu1Change(NULL, NULL, false);
				}
			}
		}

		if(errmes.IsEmpty())
		{
			errmes = Localization::Text(_T("mErrorNone"));
		}

		if(startFromCL && forceClose)
		{
			Close();
			return;
		}

		if(logfile_path.IsEmpty() == false)
		{
			FILE *f;
			if((f = _wfopen(logfile_path.c_str(), _T("wb"))) != NULL)
			{
				fwrite(AnsiString(errmes).c_str(), sizeof(char), errmes.Length(), f);
				fclose(f);
			}
		}

		if(consoleMode)
		{
			Close();
			return;
		}
	}
	else
	{
		Close();
		return;
	}

	PaintBox1->Visible = true;
	TRenderGL::Enable(true);

	if(init == false)
	{
		InitRenderGL();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::clearAllObjects()
{
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ShowOpt(const EditMode mode)
{
	if (Form_opt != NULL || !proj_exist || (proj_exist && mode == mEdit && !map_exist))
	{
		return;
	}

	Application->CreateForm(__classid(TForm_opt), &Form_opt);
	Form_opt->SetMode(mode);
	if (mode == mAdd)
	{
		Form_opt->Caption = Localization::Text(_T("mFormOptNew"));
	}
	else
	if (mode == mEdit)
	{
		Form_opt->Caption = Localization::Text(_T("mFormOptEdit"));

		Form_opt->CSpinEditDW->Value = d_w / p_w;
		Form_opt->CSpinEditDH->Value = d_h / p_h;
		Form_opt->CSpinEditMW->Value = m_w / d_w;
		Form_opt->CSpinEditMH->Value = m_h / d_h;

		Form_opt->CSpinEditZone->Value = m_pObjManager->znCount;
		Form_opt->CSpinEditZoneOnStart->Value = m_pObjManager->startZone;
		Form_opt->EdFirstScript->Text = m_pObjManager->StartScriptName;

		if (m_pObjManager->tileCameraPosOnStart >= 0)
		{
			Form_opt->CE_TileCameraOnStart->Value = m_pObjManager->tileCameraPosOnStart;
			Form_opt->CheckBoxTileCameraOnStart->Checked = true;
		}
		else
		{
			Form_opt->CheckBoxTileCameraOnStart->Checked = false;
		}

		Form_opt->CBmIgnoreUnoptimizedResoures->Checked = m_pObjManager->ignoreUnoptimizedResoures;
	}

	Form_opt->Show();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ApplyOpt()
{
	if(Form_opt != NULL)
	{
		__int32 tp_w, tp_h, i;
		TObjManager *objManager, *prevObjManager;
		TMapGPerson *gp;
		bool needRebuildBackObjListView = false;

		if(gUsingOpenGL)
		{
			TRenderGL::ReleaseBG();
		}

		objManager = NULL;
		if (Form_opt->GetMode() == mAdd)
		{
			objManager = new TObjManager(mainCommonRes);
			prevObjManager = m_pObjManager;
		}
		else if (Form_opt->GetMode() == mEdit)
		{
			objManager = m_pObjManager;
			prevObjManager = NULL;
		}

		tp_w = p_w;
		tp_h = p_h;
		d_w = Form_opt->CSpinEditDW->Value * p_w;
		d_h = Form_opt->CSpinEditDH->Value * p_h;
		m_w = Form_opt->CSpinEditMW->Value * d_w;
		m_h = Form_opt->CSpinEditMH->Value * d_h;
		objManager->old_mp_w = objManager->mp_w;
		objManager->old_mp_h = objManager->mp_h;
		objManager->mp_w = m_w / p_w;
		objManager->mp_h = m_h / p_h;
		objManager->old_md_w = m_w / d_w;
		objManager->old_md_h = m_h / d_h;
		objManager->ignoreUnoptimizedResoures = Form_opt->CBmIgnoreUnoptimizedResoures->Checked;

		map_exist = true;
		if (Form_opt->GetMode() == mAdd)
		{
			if(m_pLevelList->Count == 0)
			{
				//first empty map: assign graphics for icons
                LoadTexturesToVRAMGL();
			}
			AddNewLevelToProj(objManager);
			SetActiveProjLevel(m_pLevelList->Count - 1);
			if (prevObjManager != NULL)
			{
				prevObjManager->ClearCashe();
            }
		}

		objManager->ClearCashe();

		//при уменьшении/увеличении фрагмента карты, пропорционально сдвинуть и объекты карты
		if ((tp_h > 0 && tp_w > 0) && (tp_h != p_h || tp_w != p_w))
		{
			objManager->ScalePosAll(p_w, p_h, tp_w, tp_h, false);
			needRebuildBackObjListView = true;
		}

		//пересоздаем если надо зоны и их влияние
		if (objManager->znCount != Form_opt->CSpinEditZone->Value)
		{
			objManager->znCount = Form_opt->CSpinEditZone->Value;
			for (i = 0; i < TObjManager::OBJ_TYPE_COUNT; i++)
			{
				__int32 len = objManager->GetObjTypeCount(static_cast<TObjManager::TObjType>(i));
				for (__int32 j = 0; j < len; j++)
				{
					gp = (TMapGPerson*)objManager->GetObjByIndex(static_cast<TObjManager::TObjType>(i), j);
					assert(gp);
					if (gp->GetZone() >= objManager->znCount)
						gp->SetZone(objManager->znCount - 1);
				}
			}

			if (objManager->znCount <= znCurZone && znCurZone > 0)
				znCurZone = objManager->znCount - 1;
		}

		CreateZoneList();

		CreateMap(Form_opt->GetMode() == mEdit);
		if(gUsingOpenGL && Form_opt->GetMode() == mEdit)
		{
			TRenderGL::InitBG();
		}

		objManager->startZone = Form_opt->CSpinEditZoneOnStart->Value;
		objManager->StartScriptName = Form_opt->EdFirstScript->Text;

		if (Form_opt->CheckBoxTileCameraOnStart->Checked)
		{
			objManager->tileCameraPosOnStart = Form_opt->CE_TileCameraOnStart->Value;
		}
		else
		{
			objManager->tileCameraPosOnStart = -1;
		}

		AddGlobalScriptsToAllLevels(true);
		AddGlobalTriggerToAllLevels(true);
		ReorderPanelComponents();

		is_save = true;
		RefreshFileName();

		checkMenuBackObj();
		checkMenuGameObj();
		CheckMenuCollideLines();

		InitPrpWindows(mainCommonRes);

		if (needRebuildBackObjListView)
		{
			rebuildBackObjListView();
		}

		disabled_controlls = false;

		DrawMap(true);

		Form_opt->Free();
		Form_opt = NULL;

		for (int m = 0; m < m_pLevelList->Count; m++)
		{
			TObjManager *mo = (TObjManager*)m_pLevelList->Items[m];
			if (mo == objManager)
			{
				continue;
			}
			mo->old_mp_w = mo->mp_w;
			mo->old_mp_h = mo->mp_h;
			mo->mp_w = mo->old_md_w * d_w / p_w;
			mo->mp_h = mo->old_md_h * d_h / p_h;
			CreateMap(true, mo);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CancelOpt()
{
	if(Form_opt != NULL)
	{
		Form_opt->Free();
		Form_opt = NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::MainMenu1Change(TObject * /* Sender */ , TMenuItem * /* Source */ , bool /* Rebuild */ )
{
	if (!Visible || mnuChange)
	{
		return;
	}

	mnuChange = true;

	bool EmStart = ActStartEmul->Checked;
	bool a = map_exist && !EmStart && !disabled_controlls;
	bool b = proj_exist && !EmStart && !disabled_controlls;

	ActInfo->Enabled = a;
	ActProjSaveAs->Enabled = a;
	ActProjSave->Enabled = a;
	ActMapSave->Enabled = a;
	ActMapSaveTo->Enabled = a;
	ActXMLSave->Enabled = a;
	ActClearMap->Enabled = a;
	ActCloneMap->Enabled = a;
	ActObjOrder->Enabled = map_exist;
	ActLockDecor->Enabled = a;
	ActScript->Enabled = /*a*/ false;
	ActFindNext->Enabled = map_exist && showObjects;
	ActShowFocused->Enabled = map_exist && showObjects;
	ActFindObj->Enabled = map_exist && showObjects;
	ActCmplxEnableDragChilds->Enabled = map_exist && showObjects;
	ActDelSelection->Enabled = b;
	ActSelectAll->Enabled = b;
	ActNewMap->Enabled = proj_exist && !EmStart;
	ActAnimTypes->Enabled = b;
	ActAnimJNCount->Enabled = b;
	ActEventTypes->Enabled = b;
	ActStates->Enabled = b;
	ActOppositeStates->Enabled = b;
	ActTypes->Enabled = b;
	ActObjPattern->Enabled = b;
	ActTileOptions->Enabled = proj_exist && !EmStart;
	ActGoToBGReference->Enabled = b;
	ActBGCopy->Enabled = b;
	ActBGPaste->Enabled = b;
	ActUndo->Enabled = b;
	ActBGPasteMulti->Enabled = b;
	ActLockGameObject->Enabled = b;

	ActMapDel->Enabled = a;
	ActMapChangeIndex->Enabled = a;
	ActMapOptions->Enabled = a;
	ActVarList->Enabled = a;

	if (map_exist)
	{
		ActSetVatDef->Enabled = mainCommonRes->IsMainGameObjPatternEmpty() && m_pObjManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT) > 0;
	}
	else
	{
		ActSetVatDef->Enabled = false;
	}

	checkMenuGameObj();
	checkMenuBackObj();
    CheckMenuCollideLines();

#ifdef LITE_VER
	ActMapSave->Enabled = false;
	ActMapSave->Visible = false;
	ActMapSaveTo->Enabled = false;
	ActMapSaveTo->Visible = false;
#endif
#ifndef XML_VER
	ActXMLSave->Enabled = false;
	ActXMLSave->Visible = false;
#endif

	ActAnimation->Enabled = a;
	ActHideObjects->Enabled = !ActStartEmul->Checked;

	ActNew->Enabled = !EmStart;
	ActProjLoad->Enabled = !EmStart;
	ActMessages->Enabled = b;
	ActPath->Enabled = !EmStart;
	ActEditorOpt->Enabled = !EmStart;

	ActShowRectObj->Checked = showRectGameObj;
	ActShowBGTransMap->Checked = showCollideMap;
	ActGrid->Checked = showGrid;

	MVLanguages->Enabled = map_exist;

	mnuChange = false;
}
//---------------------------------------------------------------------------

//Сохранить проект
void __fastcall TForm_m::ActProjSaveAsExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	LoadSaveProcess = true;
	ProjSave(true);
	LoadSaveProcess = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActProjSaveExecute(TObject *)
{
	DropObjOnDrag();
	LoadSaveProcess = true;
	ProjSave(false);
	LoadSaveProcess = false;
}
//---------------------------------------------------------------------------

//Загрузить проект
void __fastcall TForm_m::ActProjLoadExecute(TObject * /* Sender */ )
{
	mDoNotDraw = false;

	CancelCollideLine();
	DropObjOnDrag();
	LoadSaveProcess = true;
	ProjLoad(_T(""));
	LoadSaveProcess = false;
	DrawMap(true);
	MainMenu1Change(NULL, NULL, false);

	mDoNotDraw = false;
	mNeedRebuildBack = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActMapSaveExecute(TObject *)
{
	CancelCollideLine();
	DropObjOnDrag();
	if(map_file_name.IsEmpty())
	{
		MapSave();
	}
	else
	{
		MapSave(&map_file_name);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActMapSaveToExecute(TObject *)
{
	CancelCollideLine();
	DropObjOnDrag();
	MapSave();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActXMLSaveExecute(TObject *)
{
#ifdef XML_VER
	CancelCollideLine();
	DropObjOnDrag();
	SaveXML();
#endif
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::LoadMainPattern(UnicodeString name, UnicodeString *mes)
{
	__int32 i;
	TObjPattern *pt = new TObjPattern();
	if (!pt->loadPattern(name, mes))
	{
		return;
	}
	mainCommonRes->AssignMainPattern(pt);
	delete pt;

	for (i = 0; i < m_pLevelList->Count; i++)
	{
		TObjManager *mo = (TObjManager*)m_pLevelList->Items[i];
   		//все существующие объекты игры переопределить этим шаблоном
		mo->AssignMainPattern(mainCommonRes->MainGameObjPattern);
		mo->ClearCashe();
	}

	InitPrpWindows(mainCommonRes);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::PaintMap()
{
	BitBlt(mpFinalRenderCanvas->Handle, 0, 0, PaintBox1->Width, PaintBox1->Height,
			mpBackBufferBmp->Canvas->Handle, 0, 0, SRCCOPY);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::VertScrollBarScroll(TObject *Sender, TScrollCode ScrollCode, __int32 &ScrollPos)
{
	__int32 oScrollPos;
	TScrollBar *sb = (TScrollBar*)Sender;
	oScrollPos = sb->Position;
	switch(ScrollCode)
	{
		case System::Uitypes::TScrollCode::scTrack:
		case System::Uitypes::TScrollCode::scPosition:
		case System::Uitypes::TScrollCode::scLineUp:
		case System::Uitypes::TScrollCode::scPageUp:
			if (ScrollPos <= sb->Min)
				ScrollPos = sb->Min;
		case System::Uitypes::TScrollCode::scLineDown:
		case System::Uitypes::TScrollCode::scPageDown:
			if (ScrollPos + sb->PageSize >= sb->Max)
				ScrollPos = sb->Max - sb->PageSize;
		default:
			break;
	}
	if (oScrollPos == ScrollPos)
	{
		return;
	}

	if (sb->Kind == sbVertical)
	{
		VertScrollBar_Position = ScrollPos;
	}
	else
	{
		HorzScrollBar_Position = ScrollPos;
	}

	if (map_exist && m_pObjManager != NULL)
	{
		m_pObjManager->RealignPosAll();
		if(TRenderGL::IsEnabled())
		{
			DrawMap(true);
			DoTick();
			ApplyRenderBuffer();
		}
	}
}
//---------------------------------------------------------------------------

//Видим ли этот объект
bool __fastcall TForm_m::is_Visible(const TMapPerson *mo)
{
	double x, y;
	__int32 w, h;
	w = HorzScrollBar->Width;
	h = VertScrollBar->Height;
	mo->getScreenCoordinates(&x, &y);
	return (__int32)x + mo->Width  >= 0 && (__int32)x < w &&
		   (__int32)y + mo->Height >= 0 && (__int32)y < h;
}
//---------------------------------------------------------------------------

//перерисовать фрагмент карты
void __fastcall TForm_m::DrawBackMap(__int32 xx, __int32 yy, bool paint)
{
	TRect r;
	__int32 idx, pt, i;
	bool empty;

	if (!map_exist)
		return;

	pt = m_pObjManager->mp_w * yy + xx;

	if (pt < m_pObjManager->mp_w * m_pObjManager->mp_h)
	{
		idx = *(m_pObjManager->map + pt);
		r.Left = (__int32)((double)xx * (double)p_w * gsGlobalScale - (double)HorzScrollBar_Position);
		r.Top = (__int32)((double)yy * (double)p_h * gsGlobalScale - (double)VertScrollBar_Position);
		r.Right = r.Left + Ceil((double)p_w * gsGlobalScale);
		r.Bottom = r.Top + Ceil((double)p_h * gsGlobalScale);

		if (!gUsingOpenGL)
		{
			mpBGBmp->Canvas->Brush->Bitmap = const_cast<Graphics::TBitmap*>(mpBackPatternBmp->GetBitmapData());
			mpBGBmp->Canvas->FillRect(r);
			mpBGBmp->Canvas->Brush->Bitmap = NULL;
		}

		__int32 bc = static_cast<__int32>(mainCommonRes->BackObj.size());
		if (bc > idx && idx > -1)
		{
			empty = true;
			GMapBGObj &MapBGobj = mainCommonRes->BackObj[idx];
			__int32 ct = MapBGobj.GetFrameObjCount();
			for (i = 0; i < ct; i++)
			{
				TFrameObj *BGobj = MapBGobj.GetFrameObj(i);
				if (BGobj != NULL && BGobj->getResIdx() >= 0)
				{
					if (gUsingOpenGL)
					{
						DrawImageType tp;
						switch(i)
						{
							case 0:
								tp = DIT_TileBG1;
							break;
							case 1:
								tp = DIT_TileBG2;
							break;
							case 2:
								tp = DIT_TileBG3;
							break;
							case 3:
								tp = DIT_TileBG4;
							break;
							case 4:
								tp = DIT_TileBG5;
							break;
							default:
								assert(0);
								continue;
						}
						TRenderGL::RenderImage(BGobj->getResIdx(), tp,
												BGobj->x, BGobj->y, BGobj->w, BGobj->h, r.Left,
																r.Top, r.Width(), r.Height(), 1.0f);
					}
					else
					{
						BLENDFUNCTION bf;
						bf.BlendOp = AC_SRC_OVER;
						bf.BlendFlags = 0;
						bf.SourceConstantAlpha = 255;
						bf.AlphaFormat = AC_SRC_ALPHA;

						::AlphaBlend(mpBGBmp->Canvas->Handle, //handle to destination DC
							r.Left, //x-coord of destination upper-left corner
							r.Top, //y-coord of destination upper-left corner
							r.Width(), //width of destination rectangle
							r.Height(), //height of destination rectangle
							mainCommonRes->GetGraphicResource(BGobj->getResIdx())->Canvas->Handle, BGobj->x, //x-coord of source upper-left corner
							BGobj->y, //y-coord of source upper-left corner
							BGobj->w, //width of source rectangle
							BGobj->h, //height of source rectangle
							bf);
					}
					empty = false;
				}
			}

			if (empty)
			{
				if (gUsingOpenGL)
				{
					TRenderGL::RenderImage(mTileTmpBmp_idx, DIT_TileBG1, 0, 0,
											p_w, p_h,
											r.Left, r.Top, r.Width(), r.Height(), 1.0f);
				}
				else
				{
					const BMPImage *tbmp = mainCommonRes->GetGraphicResource(mTileTmpBmp_idx);
					assert(tbmp);
					mpBGBmp->Canvas->StretchDraw(r, const_cast<Graphics::TBitmap*>(tbmp->GetBitmapData()));
				}
			}
		}
		else
		{
			idx = NONE_P;
		}

		*(m_pObjManager->map + pt) = (unsigned short)idx;

		if (paint)
		{
			DrawMap(false);
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DrawBackMap()
{
	if (!map_exist)
		return;

	double xx, yy, h, w, x, y;
	x = (double)HorzScrollBar_Position / ((double)p_w * gsGlobalScale);
	y = (double)VertScrollBar_Position / ((double)p_h * gsGlobalScale);
	if (VertScrollBar->Max > PaintBox1->Height)
	{
		h = (double)PaintBox1->Height / ((double)p_h * gsGlobalScale) + y;
	}
	else
	{
		h = (double)VertScrollBar->Max / ((double)p_h * gsGlobalScale) + y;
	}
	if (HorzScrollBar->Max > PaintBox1->Width)
	{
		w = (double)PaintBox1->Width / ((double)p_w * gsGlobalScale) + x;
	}
	else
	{
		w = (double)HorzScrollBar->Max / ((double)p_w * gsGlobalScale) + x;
	}
	w += 1.0f;
	h += 1.0f;

	for (xx = x; xx <= w; ++xx)
		for (yy = y; yy <= h; ++yy)
			DrawBackMap((__int32)xx, (__int32)yy, false);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::OnMessage(tagMSG &Msg, bool &Handled)
{
	(void)Handled;
	if(Msg.message == MEWM_WNDOPT)
	{
		if(Msg.lParam == mrOk)
		{
			ApplyOpt();
		}
		else
		if(Msg.lParam == mrCancel)
		{
			CancelOpt();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::IdleLoop(TObject*, bool& done)
{
	if(TRenderGL::IsEnabled())
	{
		done = false;

		DoTick();
		ApplyRenderBuffer();

		if(mFPSVal > 0)
		{
			StatusBar1->Panels->Items[4]->Text = IntToStr(mFPSVal);
			mFPSVal = -1;
		}

		if(mDelayCounterLoadTexturesToVRAMGL > 0)
		{
			if(--mDelayCounterLoadTexturesToVRAMGL == 0)
			{
				if (gUsingOpenGL)
				{
					TRenderGL::InitBG();
					TRenderGL::SetBackdropImage(mpBackPatternBmp, clNone);
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DoTick()
{
	TDateTime time = GetTime();
	unsigned __int32 ms = static_cast<unsigned int>(MilliSecondsBetween(time, mTickPreviousTime));
	mTickPreviousTime = time;

	//mShowFPS = true;
	if(mShowFPS)
	{
		mFPSTimer += ms;
		mFPSCounter++;
		if(mFPSTimer >= 1000)
		{
			mFPSVal = mFPSCounter;
			mFPSTimer = 0;
			mFPSCounter = 0;
		}
	}

	mRenderTickDuration += ms;

	if(mpCurrentGLForm != NULL)
	{
		mpCurrentGLForm->DoTick(ms);
	}
	else
	{
		if (!mDoNotDraw)
		{
			if(map_exist && m_pObjManager)
			{
				//logic + animation
				ProcessLogicGameObjectsTick(ms);

				if (mUpdateCursor)
				{
					mUpdateCursor = false;
					if (markerList->Count > 0)//рисуем маркера...
					{
						for (__int32 k = 0; k < markerList->Count; k++)
						{
							TObjMarker *m = static_cast<TObjMarker*>(markerList->Items[k]);
							try
							{
								if (m->Tick())
								{
									DelMarker(k);
									k--;
								}
							}
							catch(...)
							{
								ClearMarkers();
							}
						}
					}
				}
			}

			if(mRenderTickDuration >= MINIMAL_TIME_TO_UPDATE_ANIMATIONS)
			{
				CreateRenderFrame(mNeedRebuildBack);
				if (!gUsingOpenGL)
				{
					mNeedRebuildBack = false;
				}
			}
		}
	}

	if(mRenderTickDuration >= MINIMAL_TIME_TO_UPDATE_ANIMATIONS)
	{
		mRenderTickDuration = 0;
	}
}
//---------------------------------------------------------------------------

//Aнимация (поток-таймер)

//Show|hide cursor
void __fastcall TForm_m::Timer_crTimer(TObject *)
{
	mUpdateCursor = true;

	if (mHintTime >= 0)
	{
		mHintTime++;
		if (mHintTime > HINT_SHOW_TIME)
		{
			mHintTime = -1;
			ShowObjHint(true);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DrawMap(bool rebuildBack)
{
	if (!mNeedRebuildBack && rebuildBack)
	{
		mNeedRebuildBack = rebuildBack;
    }
}
//---------------------------------------------------------------------------

//перерисовать всю карту
void __fastcall TForm_m::CreateRenderFrame(bool rebuildBack)
{
	TRect r;
	const TMapPerson *mo;
	TCanvas* backBufferBmpCanvas = NULL;

	if (DrawMapProcess || FObjParent != NULL)
	{
		return;
	}

	DrawMapProcess = true;

	if (!gUsingOpenGL)
	{
		backBufferBmpCanvas = mpBackBufferBmp->Canvas;
	}

	if (map_exist && !LoadSaveProcess)
	{
		//back
		if (gUsingOpenGL)
		{
			DrawBackMap();
		}
		else
		{
			if (rebuildBack)
			{
				DrawBackMap();
			}
			BitBlt(backBufferBmpCanvas->Handle, 0, 0, mpBackBufferBmp->Width, mpBackBufferBmp->Height,
					mpBGBmp->Canvas->Handle, 0, 0, SRCCOPY);
		}

		//game objects
		repaintGameObjects();

		//markers
		for (int k = 0; k < markerList->Count; k++)
		{
			const TObjMarker *m = static_cast<TObjMarker*>(markerList->Items[k]);
			mo = m->GetContainedObject();

			switch(m->GetType())
			{
				case TObjMarker::TObjMarkerType::Lock:
					if (!gUsingOpenGL)
					{
						backBufferBmpCanvas->Brush->Style = bsClear;
						backBufferBmpCanvas->Pen->Color = clYellow;
						backBufferBmpCanvas->Pen->Mode = pmMaskPenNot;
						backBufferBmpCanvas->Pen->Style = psSolid;
					}
					if (is_Visible(mo))
					{
						double x1, y1, x2, y2;
						int d = m->GetCurrentCounter();
						mo->getScreenCoordinates(&x1, &y1);
						x2 = x1 + static_cast<double>(mo->Width);
						y2 = y1 + static_cast<double>(mo->Height);
						if(d % 2 == 0)
						{
							if (gUsingOpenGL)
							{
								TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
								TRenderGL::SetColor(clYellow);
								TRenderGL::RenderLine(x1, y1, x2, y2);

								TRenderGL::SetLineStyle(TRenderGL::RGLLSDot);
								TRenderGL::SetColor(clBlack);
								TRenderGL::RenderLine(x1, y1, x2, y2);

								TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
								TRenderGL::SetColor(clYellow);
								TRenderGL::RenderLine(x2, y1, x1, y2);

								TRenderGL::SetLineStyle(TRenderGL::RGLLSDot);
								TRenderGL::SetColor(clBlack);
								TRenderGL::RenderLine(x2, y1, x1, y2);
							}
							else
							{
								backBufferBmpCanvas->MoveTo(x1, y1);
								backBufferBmpCanvas->LineTo(x2, y2);
								backBufferBmpCanvas->MoveTo(x2, y1);
								backBufferBmpCanvas->LineTo(x1, y2);
							}
						}
					}
				break;

				case TObjMarker::TObjMarkerType::Attention:
					if (!gUsingOpenGL)
					{
						backBufferBmpCanvas->Brush->Style = bsClear;
						backBufferBmpCanvas->Pen->Color = clSilver;
						backBufferBmpCanvas->Pen->Mode = pmMaskPenNot;
						backBufferBmpCanvas->Pen->Style = psSolid;
					}
					if (is_Visible(mo))
					{
						double x, y, r;
						double sx, sy;
						double d = static_cast<double>(m->GetCurrentCounter());
						mo->getScreenCoordinates(&sx, &sy);
						x = (double)sx + (double)(mo->Width >> 1);
						y = (double)sy + (double)(mo->Height >> 1);
						r = (double)((mo->Width + mo->Height) >> 1);
						if (gUsingOpenGL)
						{
							TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
							TRenderGL::SetColor(clBlack);
							TRenderGL::RenderCircle(x, y, r + d);
							TRenderGL::SetLineStyle(TRenderGL::RGLLSDot);
							TRenderGL::SetColor(clWhite);
							TRenderGL::RenderCircle(x, y, r + d);
							TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
						}
						else
						{
							backBufferBmpCanvas->Ellipse((int)(x - r - d),
														(int)(y - r - d),
														(int)(x + r + d),
														(int)(y + r + d));
						}
					}
					break;
				default:
                	break;
			}
		}

		//rulers
		if (showGrid)
		{
			r.Left = HorzScrollBar_Position;
			r.Top = VertScrollBar_Position;
			DrawGrids(backBufferBmpCanvas, &r);
		}

		//selections
		if (gUsingOpenGL)
		{
			double sx, sy;
			for (__int32 i = 0; i < DragObjList->Count; ++i)
			{
				const TMapPerson* currDragObj = GetDragObjFromList(i);
				currDragObj->getScreenCoordinates(&sx, &sy);
				r.Left = static_cast<__int32>(sx) - 2;
				r.Top = static_cast<__int32>(sy) - 2;
				r.Bottom = r.Top + 4 + currDragObj->Height;
				r.Right = r.Left + 4 + currDragObj->Width;
				TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
				TRenderGL::SetColor(clBlack);
				TRenderGL::RenderRect(r.Left, r.Top, r.Width(), r.Height());
				TRenderGL::SetLineStyle(TRenderGL::RGLLSDot);
				TRenderGL::SetColor(clWhite);
				TRenderGL::RenderRect(r.Left, r.Top, r.Width(), r.Height());
			}
			TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
		}
		else
		{
			double sx, sy;
			backBufferBmpCanvas->Brush->Style = bsClear;
			for (__int32 i = 0; i < DragObjList->Count; ++i)
			{
				GetDragObjFromList(i)->getScreenCoordinates(&sx, &sy);
				r.Left = (__int32)sx - 2;
				r.Top = (__int32)sy - 2;
				r.Bottom = r.Top + 4 + GetDragObjFromList(i)->Height;
				r.Right = r.Left + 4 + GetDragObjFromList(i)->Width;
				backBufferBmpCanvas->Pen->Style = psSolid;
				backBufferBmpCanvas->Pen->Color = clBlack;
				backBufferBmpCanvas->Pen->Mode = pmCopy;
				backBufferBmpCanvas->Rectangle(r);
				backBufferBmpCanvas->Pen->Style = psDot;
				backBufferBmpCanvas->Pen->Color = clWhite;
				backBufferBmpCanvas->Pen->Mode = pmNot;
				backBufferBmpCanvas->Rectangle(r);
			}
		}

		if (m_CopyRectCX >= 0)
		{
			if (dr_mode == drSTATIC)
			{
				r.Left = (__int32)((double)m_CopyRect.left * (double)p_w * gsGlobalScale - (double)HorzScrollBar_Position);
				r.Right = (__int32)((double)m_CopyRect.Right * (double)p_w * gsGlobalScale - (double)HorzScrollBar_Position);
				r.Top = (__int32)((double)m_CopyRect.Top * (double)p_h * gsGlobalScale - (double)VertScrollBar_Position);
				r.Bottom = (__int32)((double)m_CopyRect.Bottom * (double)p_h * gsGlobalScale - (double)VertScrollBar_Position);
			}
			else
			{
				r = m_CopyRect;
			}
			if (gUsingOpenGL)
			{
				TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
				TRenderGL::SetColor(clBlack);
				TRenderGL::RenderRect(r.Left, r.Top, r.Width(), r.Height());
				TRenderGL::SetLineStyle(TRenderGL::RGLLSDash);
				TRenderGL::SetColor(clWhite);
				TRenderGL::RenderRect(r.Left, r.Top, r.Width(), r.Height());
				TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
			}
			else
			{
				backBufferBmpCanvas->Brush->Color = clWhite;
				backBufferBmpCanvas->DrawFocusRect(r);
			}
		}
	}

	if (crVisible)
	{
		DrawCursor(backBufferBmpCanvas);
	}

	fillGrayIfEmpty(backBufferBmpCanvas);

	DrawMapProcess = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ApplyRenderBuffer(HDC hdc)
{
	if (gUsingOpenGL)
	{
		if(mDelayCounterLoadTexturesToVRAMGL == 0)
		{
			TRenderGL::DrawScene(hdc);
		}
		else
		{
			TRenderGL::ClearRenderList();
		}
		Splitter->Repaint();
	}
	else
	{
		PaintMap();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DrawGrids(TCanvas *c, TRect *r)
{
	if (!map_exist)
	{
		return;
	}

	if (gUsingOpenGL)
	{
		unsigned char red   = (gridColor & 0x00ff0000) >> 16;
		unsigned char green = (gridColor & 0x0000ff00) >> 8;
		unsigned char blue  = (gridColor & 0x000000ff);
		TRenderGL::SetColor(red, green, blue, 128);
	}
	else
	{
		c->Pen->Color = clSilver;
		c->Pen->Mode = pmNotCopy;
		c->Pen->Style = psSolid;
	}
	__int32 beg = -r->Left % ((__int32)((double)d_w * gsGlobalScale));
	__int32 step = (__int32)((double)d_w * gsGlobalScale);
	for (__int32 i = beg; i <= PaintBox1->Width; i += step)
	{
		if (gUsingOpenGL)
		{
			TRenderGL::RenderLine(i, 0, i, PaintBox1->Height);
		}
		else
		{
			c->MoveTo(i, 0);
			c->LineTo(i, PaintBox1->Height);
		}
	}
	beg = -r->Top % ((__int32)((double)d_h * gsGlobalScale));
	step = (__int32)((double)d_h * gsGlobalScale);
	for (__int32 i = beg; i <= PaintBox1->Height; i += step)
	{
		if (gUsingOpenGL)
		{
			TRenderGL::RenderLine(0, i, PaintBox1->Width, i);
		}
		else
		{
			c->MoveTo(0, i);
			c->LineTo(PaintBox1->Width, i);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::fillGrayIfEmpty(TCanvas *c)
{
	TRect r;
	r.Right = VertScrollBar->Left;
	r.Bottom = HorzScrollBar->Top;
	if (VertScrollBar->Max + 1 < PaintBox1->Height) // ?? but it worka
	{
		r.left = 0;
		r.Top = VertScrollBar->Max + 1;
		if (gUsingOpenGL)
		{
			TRenderGL::SetColor(clGray);
			TRenderGL::FillRect(r.Left, r.Top, r.Width(), r.Height());
		}
		else
		{
			c->Brush->Style = bsSolid;
			c->Brush->Color = clGray;
			c->FillRect(r);
		}
	}
	if (HorzScrollBar->Max < PaintBox1->Width)
	{
		r.left = HorzScrollBar->Max;
		r.Top = 0;
		if (gUsingOpenGL)
		{
			TRenderGL::SetColor(clGray);
			TRenderGL::FillRect(r.Left, r.Top, r.Width(), r.Height());
		}
		else
		{
			c->Brush->Style = bsSolid;
			c->Brush->Color = clGray;
			c->FillRect(r);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActClearMapExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	if (!map_exist)
  {
    return;
  }
	if (Application->MessageBox(Localization::Text(_T("mAskClearMap")).c_str(),
                              Localization::Text(_T("mMessageBoxClear")).c_str(),
                              MB_OKCANCEL | MB_ICONQUESTION) == IDOK)
	{
		m_pObjManager->ClearAllObjects();
		HideObjPrpToolWindow();
		if (m_pObjManager->map != NULL)
		{
			MakeMapBackup();
			for (__int32 i = 0; i < m_pObjManager->mp_w * m_pObjManager->mp_h; i++)
				m_pObjManager->map[i] = NONE_P;
		}
		DrawMap(true);
		is_save = true;
		FObjOrder->RebuildList();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActCloneMapExecute(TObject *)
{
	DropObjOnDrag();
	if (!map_exist)
		return;

	TObjManager *objManager = new TObjManager(mainCommonRes);
	m_pObjManager->CopyTo(objManager);
	objManager->ClearCashe();
	AddNewLevelToProj(objManager);
}
//---------------------------------------------------------------------------

//CreateMap
void __fastcall TForm_m::CreateMap(bool rebuid, TObjManager *mo /*= NULL*/)
{
	//изменились размеры карты (в байтах)
	if (map_exist)
	{
		if(mo == NULL)
		{
			mo = m_pObjManager;
		}

		if (mo->old_mp_w != mo->mp_w || mo->old_mp_h != mo->mp_h)
		{
			unsigned short *t_map; // *t_collidemap;
			__int32 len, len_old;
			if (rebuid && mo->map != NULL)
			{
				len_old = mo->old_mp_w * mo->old_mp_h;
				t_map = new unsigned short[len_old];
				//t_collidemap = new unsigned short[len_old];
				memcpy(t_map, mo->map, len_old*sizeof(short));
				//memcpy(t_collidemap, mo->collidemap, len_old*sizeof(short));
			}
			if (mo->map != NULL)
			{
				delete[]mo->map;
				delete[]mo->map_undo;
				//delete[]mo->collidemap;
			}
			len = mo->mp_w * mo->mp_h;
			mo->map = new unsigned short[len];
			mo->map_undo = new unsigned short[len];
			//mo->collidemap = new unsigned short[len];
			for (__int32 i = 0; i < len; i++)
			{
				mo->map[i] = NONE_P;
				mo->map_undo[i] = NONE_P;
				//mo->collidemap[i] = NONE_P;
			}
			if (rebuid)
			{
				__int32 ww, pos_s, pos_d;
				ww = mo->old_mp_w > mo->mp_w ? mo->mp_w : mo->old_mp_w; //меньшее
				for (pos_d = pos_s = 0; pos_s < len_old && pos_d < len; )
				{
					memcpy((mo->map + pos_d), (t_map + pos_s), ww * sizeof(short));
					//memcpy((mo->collidemap + pos_d), (t_collidemap + pos_s), ww*sizeof(short));
					pos_d += mo->mp_w;
					pos_s += mo->old_mp_w;
				}
				MakeMapBackup();
				delete[]t_map;
			}
			mo->CheckAndDeleteBrokenCollidelines(p_w, p_h);
		}
		{
			BMPImage *tileTmpBmp = new BMPImage(new Graphics::TBitmap());
			makeEmptyPreview(tileTmpBmp, _T("err"), p_w, p_h);
			tileTmpBmp->ResetAlpha();
			mainCommonRes->ReplaceGraphicResource(tileTmpBmp, mTileTmpBmp_idx);
			delete tileTmpBmp;
		}

		if(mo == m_pObjManager)
		{
			ClearCopySelection();
			m_px = m_py = -1;
			HorzScrollBar->PageSize = 0;
			VertScrollBar->PageSize = 0;
			HorzScrollBar->SetParams(0, 0, (__int32)((double)m_w * gsGlobalScale));
			VertScrollBar->SetParams(0, 0, (__int32)((double)m_h * gsGlobalScale));
			HorzScrollBar->SmallChange = (short)((double)p_w * gsGlobalScale);
			VertScrollBar->SmallChange = (short)((double)p_h * gsGlobalScale);
			HorzScrollBar->LargeChange = (short)((double)p_w * 3.0f * gsGlobalScale);
			VertScrollBar->LargeChange = (short)((double)p_h * 3.0f * gsGlobalScale);
		}

		mo->old_mp_w = mo->mp_w;
		mo->old_mp_h = mo->mp_h;

		if(mo == m_pObjManager)
		{
			PaintBox1->Left = 0;
			PaintBox1->Top = ToolBar->Height + TabLevels->Height;
			disabled_controlls = m_pObjManager->map == NULL;
		}
	}
	else
	{
		{
			BMPImage *tileTmpBmp = new BMPImage(new Graphics::TBitmap());
			makeEmptyPreview(tileTmpBmp, _T("err"), DEF_TILE_SIZE, DEF_TILE_SIZE);
			tileTmpBmp->ResetAlpha();
			mainCommonRes->ReplaceGraphicResource(tileTmpBmp, mTileTmpBmp_idx);
			delete tileTmpBmp;
		}
 		ClearCopySelection();
		m_px = m_py = -1;
		HorzScrollBar->PageSize = 0;
		VertScrollBar->PageSize = 0;
		HorzScrollBar->SetParams(0, 0, 0);
		VertScrollBar->SetParams(0, 0, 0);
		HorzScrollBar->SmallChange = 0;
		VertScrollBar->SmallChange = 0;
		HorzScrollBar->LargeChange = 0;
		VertScrollBar->LargeChange = 0;
		PaintBox1->Left = 0;
		PaintBox1->Top = ToolBar->Height + TabLevels->Height;
		disabled_controlls = true;
	}

	MainMenu1Change(nullptr, nullptr, false);

	changeSizes();
}
//---------------------------------------------------------------------------
/*
void __fastcall TForm_m::DeleteIfOutOfMap(TList *lst)
{
	double x, y;
	__int32 i;
	TMapGPerson *gp;
	for (i = 0; i < lst->Count; i++)
	{
		gp = (TMapGPerson*)lst->Items[i];
		gp->getCoordinates(&x, &y);
		if ((int)x > (int)m_w || (int)y > (int)m_h)
		{
			m_pObjManager->DeleteObject(gp);
			i--;
		}
	}
}
//---------------------------------------------------------------------------
*/
void __fastcall TForm_m::MakeMapBackup()
{
	if (!map_exist)
	{
		return;
	}
	memcpy(m_pObjManager->map_undo, m_pObjManager->map, m_pObjManager->mp_w * m_pObjManager->mp_h*sizeof(unsigned short));
}
//---------------------------------------------------------------------------

bool __fastcall TForm_m::AddStaticToMap(bool del)
{
	if (ActStartEmul->Checked)
	{
		return false;
	}
	unsigned char objType;

	if (!map_exist)
	{
		return false;
	}

	if (m_px < 0 || m_py < 0 || m_px >= m_w || m_py >= m_h)
	{
		return false;
	}

	if (dr_mode == drSTATIC)
	{
		__int32 idx = FBGObjWindow->GetSelectedIndex();
		if(mainCommonRes->BackObj[idx].IsEmpty() || del)
		{
			idx = NONE_P;
		}
		if (*(m_pObjManager->map + m_pObjManager->mp_w * m_py + m_px) == idx)
		{
			return true;
		}
		MakeMapBackup();
		*(m_pObjManager->map + m_pObjManager->mp_w * m_py + m_px) = (unsigned short)idx;
		DrawBackMap(m_px, m_py, true);
		is_save = true;
		return true;
	}
	else if (dr_mode == drOBJECTS)
	{
		if (!showObjects)
		{
			UnicodeString ws = Localization::TextWithVar(_T("mSwitchOffOptionWarning_s"), ActHideObjects->Caption);
			Application->MessageBox(ws.c_str(), Localization::Text(_T("mMessageBoxWarning")).c_str(), MB_OK | MB_ICONERROR);
			return false;
		}
		GPerson *gp;
		TListItem *li;
		if ((li = ListView_u->ItemFocused) != NULL)
		{
			if (ListView_u->Selected != li)
			{
				ListView_u->Selected = li;
			}
		}
		if ((li = ListView_u->Selected) == NULL)
		{
			return false;
		}
		gp = (GPerson*)mainCommonRes->GParentObj->Items[(int)li->Data];
		__int32 type = objMAPOBJ;
		if (gp->GetName().Compare(UnicodeString(TRIGGER_NAME)) == 0)
		{
			type = objMAPTRIGGER;
		}
		else if (gp->GetName().Compare(UnicodeString(DIRECTOR_NAME)) == 0)
		{
			type = objMAPDIRECTOR;
		}
		else if (gp->GetName().Compare(UnicodeString(TEXTBOX_NAME)) == 0)
		{
			type = objMAPTEXTBOX;
		}
		else if (gp->GetName().Compare(UnicodeString(SOUNDSCHEME_NAME)) == 0)
		{
			type = objMAPSOUNDSCHEME;
		}
		else if (gp->GetName().Compare(UnicodeString(OBJPOOL_NAME)) == 0)
		{
			type = objMAPOBJPOOL;
		}
		if (newGObject(m_pObjManager, type, gp->GetName(), m_px, m_py, ListBoxZone->ItemIndex == m_pObjManager->znCount ? ZONES_INDEPENDENT_IDX : ListBoxZone->ItemIndex, _T(""), ObjCreateModeNormal) != NULL)
		{
			is_save = true;

			DrawMap(false);
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (dr_mode == drCOLLIDELINES)
	{
		if (newGObject(m_pObjManager,
						objMAPCOLLIDENODE,
						mCurrentCollideChainShapeName,
						m_px, m_py,
						ZONES_INDEPENDENT_IDX,
						_T(""),
						ObjCreateModeNormal) != NULL)
		{
			if (GetLastSelectedDragObj() != NULL &&
				GetLastSelectedDragObj()->GetObjType() == objMAPCOLLIDENODE)
			{
				TMapCollideLineNode *cn = static_cast<TMapCollideLineNode*>(GetLastSelectedDragObj());
				mCurrentCollideChainShapeName = cn->getGParentName();
				const TCollideChainShape* sh = static_cast<const TCollideChainShape*>(m_pObjManager->FindObjectPointer(mCurrentCollideChainShapeName));
				assert(sh->Name.Compare(mCurrentCollideChainShapeName) == 0);
				if(sh->GetNodesCount() == 1)
				{
					if (newGObject(m_pObjManager,
									objMAPCOLLIDENODE,
									mCurrentCollideChainShapeName,
									m_px, m_py,
									ZONES_INDEPENDENT_IDX,
									_T(""),
									ObjCreateModeNormal) != NULL)
					{
						RemoveDragObjFromList(cn);
						cn->ApplyCoordinates();
					}
					else
					{
                    	return false;
					}
				}
				is_save = true;
				DrawMap(false);
				return true;
			}
			else
			{
				assert(0);
			}
		}
	}
	return false;
}
//---------------------------------------------------------------------------

//новый объект -- на карту
TMapPerson *__fastcall TForm_m::newGObject(TObjManager *objManager, __int32 type,
											UnicodeString iParentName,
											double x, double y,
											__int32 zone,
											UnicodeString iName,
											TObjCreateMode iMode)
{
	TMapPerson *i;
	TMapPerson *obj_upd = NULL;
	GPerson *gp;

	if (objManager == NULL)
	{
		return NULL;
	}

	//заменить только тогда, когда совпадают имя и тип, иначе добавить как новый и использовать автоимя
	if (ObjCreateModeClipboard == iMode)
	{
		obj_upd = (TMapPerson*)objManager->FindObjectPointer(iName);
		if (obj_upd != NULL)
		{
			if (obj_upd->GetObjType() != type)
			{
				obj_upd = NULL;
				iName = _T("");
			}
			else if (obj_upd->GetObjType() == objMAPCOLLIDENODE && obj_upd->getGParentName().Compare(iParentName) != 0)
			{
				obj_upd = NULL;
				iName = _T("");
			}
		}
	}

	switch(type)
	{
		case objMAPOBJ:
		{
			TMapGPerson *p;
			if (objManager->GetTotalMapObjectsCount() >= MAX_CHARACTER_ON_MAP && obj_upd == NULL)
			{
				return NULL;
			}
			gp = (GPerson*)mainCommonRes->GetParentObjByName(iParentName);
			p = objManager->CreateMapGPerson(x, y, &HorzScrollBar_Position, &VertScrollBar_Position, gp->GetName());
			p->SetProperties(gp->GetBaseProperties());
			if (!gp->IsDecoration)
			{
				p->SetDecorType(decorNONE);
			}
			else
			{
				p->SetDecorType(gp->DecorType);
			}
			p->State_Name = gp->DefaultStateName;
			i = p;
		}
		break;

		case objMAPTRIGGER:
		{
			TMapTrigger *p;
			if (objManager->GetObjTypeCount(TObjManager::TRIGGER) >= MAX_TRIGGER_ON_MAP && obj_upd == NULL)
			{
				return NULL;
			}
			gp = (GPerson*)mainCommonRes->GetParentObjByName(iParentName);
			p = objManager->CreateMapTrigger(x, y, &HorzScrollBar_Position, &VertScrollBar_Position, gp->GetName());
			p->SetProperties(gp->GetBaseProperties());
			i = p;
		}
		break;

		case objMAPDIRECTOR:
		{
			if (objManager->GetObjTypeCount(TObjManager::DIRECTOR) >= MAX_DIRECTOR_ON_MAP && obj_upd == NULL)
			{
				return NULL;
			}
			gp = (GPerson*)mainCommonRes->GetParentObjByName(iParentName);
			i = objManager->CreateMapDirector(x, y, &HorzScrollBar_Position, &VertScrollBar_Position, gp->GetName());
		}
		break;

		case objMAPTEXTBOX:
		{
			if (objManager->GetTotalMapObjectsCount() >= MAX_CHARACTER_ON_MAP && obj_upd == NULL)
			{
				return NULL;
			}
			gp = (GPerson*)mainCommonRes->GetParentObjByName(iParentName);
			i = objManager->CreateMapTextBox(x, y, &HorzScrollBar_Position, &VertScrollBar_Position, gp->GetName());
		}
		break;

		case objMAPSOUNDSCHEME:
		{
			__int32 count = objManager->GetTotalMapObjectsCount();
			if (count > MAX_CHARACTER_ON_MAP && obj_upd == NULL)
			{
				return NULL;
			}
			if (objManager->FindObjectPointer(UnicodeString(SOUNDSCHEME_NAME)) && obj_upd == NULL)
			{
				return NULL;
			}
			gp = (GPerson*)mainCommonRes->GetParentObjByName(iParentName);
			i = objManager->CreateMapSoundScheme(x, y, &HorzScrollBar_Position, &VertScrollBar_Position, gp->GetName());
			iName = UnicodeString(SOUNDSCHEME_NAME);
		}
		break;

		case objMAPOBJPOOL:
		{
			if (objManager->GetTotalMapObjectsCount() >= MAX_CHARACTER_ON_MAP && obj_upd == NULL)
			{
				return NULL;
			}
			gp = (GPerson*)mainCommonRes->GetParentObjByName(iParentName);
			i = objManager->CreateMapObjPool(x, y, &HorzScrollBar_Position, &VertScrollBar_Position, gp->GetName());
		}
		break;

		case objMAPCOLLIDENODE:
		{
			if (objManager->GetCollideNodesCount() >= MAX_OBJ_COLLIDENODES && obj_upd == NULL)
			{
				return NULL;
			}
			i = objManager->CreateMapCollideNode(x, y, &HorzScrollBar_Position, &VertScrollBar_Position, iParentName);
		}
	}

	i->SetZone(zone);

	if (iName.IsEmpty())
	{
		i->Name = objManager->AutoNameObj(i);
	}
	else
	{
		i->Name = iName;
	}
	i->changeHint();

	if (obj_upd == NULL)
	{
		switch(i->GetObjType())
		{
		case objMAPOBJ:
			{
				TMapGPerson *p = (TMapGPerson*)i;
				if (p->IsRected() != showRectGameObj)
				{
					p->ShowRect(showRectGameObj);
                }
			}break;

		case objMAPTRIGGER:
			i->ShowRect(false);
			break;

		case objMAPDIRECTOR:
			i->ShowRect(false);
			i->SetZone(ZONES_INDEPENDENT_IDX);
			break;

		case objMAPTEXTBOX:
			i->ShowRect(false);
			break;

		case objMAPSOUNDSCHEME:
			i->ShowRect(false);
			i->SetZone(ZONES_INDEPENDENT_IDX);
			break;

		case objMAPOBJPOOL:
			i->ShowRect(false);
			break;

		case objMAPCOLLIDENODE:
			//nope
			break;

		default:
			break;
		}
	}
	else
	{
		switch(i->GetObjType())
		{
		case objMAPOBJ:
			objManager->ClearCasheFor((TMapGPerson*)obj_upd);
			((TMapGPerson*)i)->CopyTo((TMapGPerson*)obj_upd);
			break;
		case objMAPTEXTBOX:
			objManager->ClearCasheFor((TMapTextBox*)obj_upd);
			((TMapTextBox*)i)->CopyTo((TMapTextBox*)obj_upd);
			break;
		case objMAPSOUNDSCHEME:
			objManager->ClearCasheFor((TMapSoundScheme*)obj_upd);
			((TMapSoundScheme*)i)->CopyTo((TMapSoundScheme*)obj_upd);
			break;
		case objMAPOBJPOOL:
			objManager->ClearCasheFor((TMapObjPool*)obj_upd);
			((TMapObjPool*)i)->CopyTo((TMapObjPool*)obj_upd);
			break;
		case objMAPTRIGGER:
			objManager->ClearCasheFor((TMapTrigger*)obj_upd);
			((TMapTrigger*)i)->CopyTo((TMapTrigger*)obj_upd);
			break;
		case objMAPDIRECTOR:
			objManager->ClearCasheFor((TMapDirector*)obj_upd);
			((TMapDirector*)i)->CopyTo((TMapDirector*)obj_upd);
			break;
		case objMAPCOLLIDENODE:
			objManager->ClearCasheFor((TMapCollideLineNode*)obj_upd);
			((TMapCollideLineNode*)i)->CopyTo((TMapCollideLineNode*)obj_upd);
			break;
		default:
			break;
		}
		objManager->DeleteObject(i);
		i = obj_upd;
	}

	if (iMode != ObjCreateModeLoading)
	{
		i->OnGlobalScale();
		ReorderPanelComponents();
		CreateZoneList();
		DragObjListAdd(i);
		if(ObjCreateModeClipboard != iMode)
		{
			FObjOrder->RebuildList();
			FGOProperty->refreshCplxList();
			FGOProperty->refreshNodes();
			FDirectorProperty->refreshNodes();
			FDirectorProperty->refreshTriggers();
            FDirectorProperty->refreshEvents();
			FSpObjProperty->refreshNodes();
		}
	}

	is_save = true;
	return i;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::NormalizeMapCursor(double& x, double& y)
{
	if (x < 0.0f)
	{
		x = 0.0f;
	}
	if (y < 0.0f)
	{
		y = 0.0f;
	}
	if (x >= (double)m_w)
	{
		x = (double)m_w - 1.0f;
	}
	if (y >= (double)m_h)
	{
		y = (double)m_h - 1.0f;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::Shape_crMouseDown(TObject *sender, TMouseButton Button, TShiftState Shift, int scrX, int scrY)
{
	if (disabled_controlls || (ListView_u == sender && dr_mode != drOBJECTS))
	{
		return;
	}

	if(FakeControl->Visible == false)
	{
		FakeControl->Visible = true;
	}
	if(FakeControl->Focused() == false)
	{
		FakeControl->SetFocus();
	}

	const int cpx = (int)((double)(scrX + HorzScrollBar_Position) / gsGlobalScale);
	const int cpy = (int)((double)(scrY + VertScrollBar_Position) / gsGlobalScale);
	if (cpx < m_w && cpx >= 0 && cpy < m_h && cpy >= 0)
	{
		if (dr_mode == drSTATIC)
		{
			LMBtn_main.snap_x = LMBtn_main.snap_y = false;
            LMBtn_main.locked = false;
			UpdateMapCursorCoordinates(scrX, scrY, false, false);
			if (Shift.Contains(ssCtrl) && crVisible)
			{
				AddStaticToMap(Button == mbRight);
			}
			else if (!m_CopyRectSet && !ActStartEmul->Checked && crVisible)
			{
				m_CopyRectSet = true;
				m_CopyRect.Left = m_CopyRectCX = m_px;
				m_CopyRect.Top = m_CopyRectCY = m_py;
				m_CopyRect.Right = m_CopyRect.Left + 1;
				m_CopyRect.Bottom = m_CopyRect.Top + 1;
				DrawMap(false);
			}
		}
		else if (dr_mode == drCOLLIDELINES)
		{
			int dx = 0;
			int dy = 0;
			const int snapStepX = p_w;
			const int snapStepY = p_h;
			if(LMBtn_main.collidenode == true)
			{
				PaintBox1MouseMove(sender, Shift, scrX, scrY);
			}
			if (LMBtn_main.snap_y)
			{
				scrY = SnapCoordinate(LMBtn_main.prev.y, snapStepY);
			}
			else if(LMBtn_main.snap_x)
			{
				scrX = SnapCoordinate(LMBtn_main.prev.x, snapStepX);
			}
			UpdateMapCursorCoordinates(scrX, scrY, LMBtn_main.snap_x, LMBtn_main.snap_y);
			TMapPerson *pt = static_cast<TMapPerson*>(FindObjectPointer(scrX, scrY, dx, dy, false));
			bool hasLockedObject = false;
			const bool unselect = ProcessSelectionWithMouse(pt, Button, Shift, scrX, scrY, hasLockedObject);
			if (Button == mbLeft)
			{
				LMBtn_main.locked = hasLockedObject;
				LMBtn_main.pos.x = 0;
				LMBtn_main.pos.y = 0;
				LMBtn_main.prev.x = scrX;
				LMBtn_main.prev.y = scrY;
				LMBtn_main.moved = false;
				LMBtn_main.pressed = true;
				if(LMBtn_main.collidenode == true)
				{
					MoveOrCreateCollideNode(MoveOrCreateCollideNodeMode::mccnAddNode);
				}
				if (pt != nullptr && IsAnyPrpWindowVisible())
				{
					ShowObjPrpToolWindow(pt, pt->GetObjType());
				}
			}
			else
			if (Button == mbRight && LMBtn_main.collidenode == false)
			{
				LMBtn_main.locked = false;
				CancelCollideLine();
				double x = static_cast<double>(m_px);
				double y = static_cast<double>(m_py);
				const double findRadius = TMapCollideLineNode::gscCollideLineNodeDefSize;
				const TObjManager::TCreateCollideNodeType ccnt = m_pObjManager->TryCreateCollideNode(x, y, findRadius, nullptr, nullptr, nullptr, nullptr);
				LMBtn_main.pressed = false;
				POINT p;
				GetCursorPos(&p);
				DbgSeparator->Visible = false;
				DbgSeparator1->Visible = false;
				DbgStopOnStateChange->Visible = false;
				DbgStateTrace->Visible = false;
				DbgResetDirList->Visible = false;
				NPopupStartScript->Visible = false;
				DbgTrigActionsTrace->Visible = false;
				ActMapObjClone->Visible = false;
				ActMapObjEdit->Enabled = pt != NULL;
				ActMapObjDel->Enabled = pt != NULL;

				NPMI_NewShape->Visible = true;
				NPMI_NewNode->Visible = true;
				ActNewCollideNode->Enabled = ccnt == TObjManager::CCNT_ADDNODETOSHAPE ||
										ccnt == TObjManager::CCNT_INSERTNODETOSHAPE;
				ActNewCollideShape->Enabled = ccnt == TObjManager::CCNT_NEWSHAPE;
				CheckMenuCollideLines();
				if (!unselect && (ActNewCollideNode->Enabled || ActNewCollideShape->Enabled))
				{
					PopupMenu_img->Popup(p.x, p.y);
				}
			}
		}
		else if (dr_mode == drOBJECTS)
		{
			int dx = 0;
			int dy = 0;
			LMBtn_main.snap_x = LMBtn_main.snap_y = false;
			UpdateMapCursorCoordinates(scrX, scrY, false, false);
			TMapGPerson *pt = sender == ListView_u ?
								static_cast<TMapGPerson*>(GetLastSelectedDragObj()) :
								(TMapGPerson*)FindObjectPointer(scrX, scrY, dx, dy, true);
			bool hasLockedObject = false;
			const bool unselect = ProcessSelectionWithMouse(pt, Button, Shift, scrX, scrY, hasLockedObject);
			if (pt != NULL)
			{
				if (Button == mbLeft)
				{
					LMBtn_main.locked = hasLockedObject;
					LMBtn_main.pos.x = dx;
					LMBtn_main.pos.y = dy;
					LMBtn_main.prev.x = scrX;
					LMBtn_main.prev.y = scrY;
					LMBtn_main.pressed = true;
					LMBtn_main.moved = false;
					if (IsAnyPrpWindowVisible())
					{
						ShowObjPrpToolWindow(pt, pt->GetObjType());
					}
				}
				else if (Button == mbRight)
				{
					bool debug;
					POINT p;
					LMBtn_main.pressed = false;
                    LMBtn_main.locked = false;
					debug = ActStartEmul->Checked && pt->GetObjType() == objMAPOBJ;
					DbgSeparator->Visible = debug;
					DbgSeparator1->Visible = debug;
					DbgStopOnStateChange->Visible = debug;
					DbgStateTrace->Visible = debug;
					DbgResetDirList->Visible = debug;
					NPopupStartScript->Visible = debug;
					DbgTrigActionsTrace->Visible = debug;
					N41->Visible = true;
					N42->Visible = !debug;
					NPMI_NewShape->Visible = false;
					NPMI_NewNode->Visible = false;
					ActMapObjClone->Visible = !debug && pt->GetObjType() == objMAPOBJ;
					if (debug)
					{
						DbgStopOnStateChange->Checked = pt->StopOnStateChange;
						DbgStateTrace->Checked = pt->InspectStates;
						DbgResetDirList->Enabled = pt->DirStateIdx > 0;
						DbgTrigActionsTrace->Checked = pt->TraceTriggerActions;
						CreateDebugPopupScriptList();
					}
					GetCursorPos(&p);
					if (ActStartEmul->Checked)
					{
						ActPause->Execute();
					}
					//CreateRenderFrame(false); //насильно рисовать перед Popup
					if (!unselect)
					{
						PopupMenu_img->Popup(p.x, p.y); //иначе вызываем попап-меню
					}
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CancelCollideLine()
{
	TMapCollideLineNode *cn = NULL;
	LMBtn_main.collidenode = false;
	mCurrentCollideChainShapeName = _T("");
	std::vector<TMapPerson*>t_array;
	const __int32 ct = DragObjListCount();
	for (__int32 i = 0; i < ct; i++)
	{
		TMapPerson *dragGameObj = GetDragObjFromList(i);
		if(dragGameObj->GetObjType() == objMAPCOLLIDENODE)
		{
			cn = static_cast<TMapCollideLineNode*>(dragGameObj);
			double x, y;
			if(cn->GetPrevCoordinates(x, y) == false)
			{
				m_pObjManager->DeleteObject(cn);
			}
			else
			{
				cn->setCoordinates(x, y, 0, 0);
				t_array.push_back(dragGameObj);
			}
		}
		else
		{
			t_array.push_back(dragGameObj);
		}
	}
	DragObjListClear();
	if(t_array.empty() == false)
	{
		for(std::vector<TMapPerson*>::iterator it = t_array.begin(); it != t_array.end(); ++it)
		{
			DragObjListAdd(*it);
		}
	}
	if(cn != NULL)
	{
		m_pObjManager->ResetCrosslinesErrorMarkers();
		FObjOrder->RebuildList();
		DrawMap(true);
	}
	CheckMenuCollideLines();
}
//---------------------------------------------------------------------------

bool __fastcall TForm_m::ProcessSelectionWithMouse(TMapPerson* pt, TMouseButton Button, TShiftState Shift,
													const int& srcX, const int& srcY, bool& hasLockedObject)
{
	bool unselect = false;

	const bool isShiftPressed = Shift.Contains(ssShift);
	const bool isCtrlPressed = Shift.Contains(ssCtrl);

	hasLockedObject = false;

	if (pt == nullptr)
	{
		if (!isCtrlPressed && !isShiftPressed)
		{
			DragObjListClear();
			if (FObjOrder->Visible)
			{
				FObjOrder->RebuildSelectionList();
			}
		}
		if (Button == mbLeft)
		{
			m_CopyRectCX = srcX;
			m_CopyRectCY = srcY;
			m_CopyRectW = m_CopyRectH = 0;
			m_CopyRect.Left = m_CopyRectCX;
			m_CopyRect.Top = m_CopyRectCY;
			m_CopyRect.Right = m_CopyRect.Left + 1;
			m_CopyRect.Bottom = m_CopyRect.Top + 1;
			m_CopyRectSet = true;
		}
		DrawMap(false);
	}
	else
	{
		if (isCtrlPressed || isShiftPressed)
		{
			if (DragObjList->Count > 1)
			{
				if (DragObjListContains(pt))
				{
					if(isCtrlPressed)
					{
						RemoveDragObjFromList(pt);
						unselect = true;
						if (FObjOrder->Visible)
						{
							FObjOrder->RebuildSelectionList();
						}
					}
				}
				else
				{
					DragObjListAdd(pt);
					if (FObjOrder->Visible)
					{
						FObjOrder->RebuildSelectionList();
					}
				}
			}
			else
			{
				DragObjListAdd(pt);
				if (FObjOrder->Visible)
				{
					FObjOrder->RebuildSelectionList();
				}
			}
		}
		else
		{
			if (!DragObjListContains(pt))
			{
				DragObjListClear();
				DragObjListAdd(pt);
				if (FObjOrder->Visible)
				{
					FObjOrder->RebuildSelectionList();
                }
			}
		}

		for (int i = 0; i < DragObjList->Count; i++)
		{
			TMapGPerson* p = static_cast<TMapGPerson*>(DragObjList->Items[i]);
			if(p->IsLocked())
			{
				hasLockedObject = true;
                break;
            }
		}

		DrawMap(false);
	}
	return unselect;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::PaintBox1MouseUp(TObject * /* Sender */ , TMouseButton /* Button */ , TShiftState Shift, __int32 /* X */ , __int32 /* Y */ )
{
	switch(dr_mode)
	{
		case drCOLLIDELINES:
			if(LMBtn_main.collidenode == false)
			{
				ApplyDraggedCollideNodes();
			}
		case drOBJECTS:
			if (m_CopyRectSet)
			{
				TMapPerson *pt = DragObjListAddMulti(m_CopyRect, Shift.Contains(ssCtrl));
				if (pt && (dr_mode == drOBJECTS || dr_mode == drCOLLIDELINES))
				{
					if(IsAnyPrpWindowVisible())
					{
						ShowObjPrpToolWindow(pt, pt->GetObjType());
					}
				}
				ClearCopySelection();
				DrawMap(false);
				if (FObjOrder->Visible)
				{
					FObjOrder->RebuildSelectionList();
				}
			}
			break;

		case drSTATIC:
			m_CopyRectSet = false;
			break;

		default:
			break;
	}
	LMBtn_main.moved = false;
	LMBtn_main.pressed = false;
	LMBtn_main.locked = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::PaintBox1DblClick(TObject * /* Sender */ )
{
	if (disabled_controlls)
	{
		return;
	}
	if (dr_mode == drOBJECTS)
	{
		DragObjListClear();
		AddStaticToMap(false);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::UpdateMapCursorCoordinates(__int32& scrX, __int32& scrY, bool snapX, bool snapY)
{
	if (dr_mode == drSTATIC)
	{
		m_px = (__int32)(((double)(scrX + HorzScrollBar_Position) / gsGlobalScale) / (double)p_w);
		m_py = (__int32)(((double)(scrY + VertScrollBar_Position) / gsGlobalScale) / (double)p_h);
	}
	else
	{
		m_px = (__int32)((double)(scrX + HorzScrollBar_Position) / gsGlobalScale);
		m_py = (__int32)((double)(scrY + VertScrollBar_Position) / gsGlobalScale);
	}
	const __int32 snapStepX = p_w;
	const __int32 snapStepY = p_h;
	if(snapY)
	{
		m_py = SnapCoordinate(m_py, snapStepY);
		if(m_py >= m_h)
		{
			m_py = m_h - snapStepY;
		}
	}
	if(snapX)
	{
		m_px = SnapCoordinate(m_px, snapStepX);
		if(m_px >= m_w)
		{
			m_px = m_w - snapStepX;
		}
    }
}
//---------------------------------------------------------------------------

bool __fastcall TForm_m::ProcessTilesSelection()
{
	if (m_CopyRectSet)
	{
		if (m_px > m_CopyRectCX)
		{
			m_CopyRect.Left = m_CopyRectCX;
			m_CopyRect.Right = m_px + 1;
		}
		else
		{
			m_CopyRect.Right = m_CopyRectCX + 1;
			m_CopyRect.Left = m_px;
		}
		if (m_py > m_CopyRectCY)
		{
			m_CopyRect.Top = m_CopyRectCY;
			m_CopyRect.Bottom = m_py + 1;
		}
		else
		{
			m_CopyRect.Bottom = m_CopyRectCY + 1;
			m_CopyRect.Top = m_py;
		}
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TForm_m::ProcessObjectsSelection(const __int32& scrX, __int32& scrY)
{
	if (m_CopyRectSet)
	{
		if (scrX > m_CopyRectCX)
		{
			m_CopyRect.Left = m_CopyRectCX;
			m_CopyRect.Right = scrX;
		}
		else
		{
			m_CopyRect.Right = m_CopyRectCX;
			m_CopyRect.Left = scrX;
		}
		if (scrY > m_CopyRectCY)
		{
			m_CopyRect.Top = m_CopyRectCY;
			m_CopyRect.Bottom = scrY;
		}
		else
		{
			m_CopyRect.Bottom = m_CopyRectCY;
			m_CopyRect.Top = scrY;
		}
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

#define SNAP_HELPER_MACRO() \
{ \
	if (LMBtn_main.snap_x) \
	{ \
		x = (double)SnapCoordinate((__int32)(x + xm), snapStepX); \
	} \
	else \
	{ \
		x += xm; \
	} \
	if (LMBtn_main.snap_y) \
	{ \
		y = (double)SnapCoordinate((__int32)(y + ym), snapStepY); \
	} \
	else \
	{ \
		y += ym; \
	} \
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::PaintBox1MouseMove(TObject *sender, TShiftState Shift, __int32 scrX, __int32 scrY)
{
	if (disabled_controlls || (ListView_u == sender && dr_mode != drOBJECTS))
	{
		return;
	}

	if (::abs(static_cast<__int32>(mHintCoordinates.x - scrX)) > 2 ||
		::abs(static_cast<__int32>(mHintCoordinates.y - scrY)) > 2)
	{
		mHintTime = 0;
		mHintCoordinates.x = scrX;
		mHintCoordinates.y = scrY;
		mpHintWnd->ReleaseHandle();
	}

	if (dr_mode == drNONE && PaintBox1MouseMove_process)
	{
		return;
	}

	const bool isShiftPressed = Shift.Contains(ssShift);
	const bool isCtrlPressed = Shift.Contains(ssCtrl);
	const bool isAltPressed = Shift.Contains(ssAlt);

	PaintBox1MouseMove_process = true;

	if (isCtrlPressed && dr_mode == drSTATIC && crVisible)
	{
		if (Shift.Contains(ssLeft))
		{
			AddStaticToMap(false);
		}
		else if (Shift.Contains(ssRight))
		{
			AddStaticToMap(true);
		}
	}

	const __int32 snapStepX = p_w;
	const __int32 snapStepY = p_h;
	if(isShiftPressed && isAltPressed && !isCtrlPressed)
	{
		if(((LMBtn_main.pressed && dr_mode == drOBJECTS) ||
				(LMBtn_main.moved && dr_mode == drCOLLIDELINES)) &&
				LMBtn_main.snap_y == false && LMBtn_main.snap_x == false)
		{
			__int32 md_y = ::abs(scrY % snapStepY);
			if(md_y > snapStepY / 2)
			{
				md_y = snapStepY - md_y;
			}
			__int32 md_x = ::abs(scrX % snapStepX);
			if(md_x > snapStepX / 2)
			{
				md_x = snapStepX - md_x;
			}
			if (md_y < md_x)
			{
				LMBtn_main.snap_y = true;
				LMBtn_main.snap_x = false;
			}
			else
			{
				LMBtn_main.snap_x = true;
				LMBtn_main.snap_y = false;
			}
		}
	}
	else
	{
		LMBtn_main.snap_x = LMBtn_main.snap_y = false;
	}

	if (LMBtn_main.snap_y)
	{
		scrY = SnapCoordinate(LMBtn_main.prev.y, snapStepY);
	}
	else if(LMBtn_main.snap_x)
	{
		scrX = SnapCoordinate(LMBtn_main.prev.x, snapStepX);
	}

	const int cpx = (int)((double)(scrX + HorzScrollBar_Position) / gsGlobalScale);
	const int cpy = (int)((double)(scrY + VertScrollBar_Position) / gsGlobalScale);
	if (cpx < m_w && cpx >= 0 && cpy < m_h && cpy >= 0)
	{
		if (dr_mode == drSTATIC)
		{
			if (!crVisible && m_pObjManager->map != NULL)
			{
				crVisible = true;
			}
			UpdateMapCursorCoordinates(scrX, scrY, false, false);
			const int cL = m_px * (int)((double)p_w * gsGlobalScale) - HorzScrollBar_Position;
			const int cT = m_py * (int)((double)p_h * gsGlobalScale) - VertScrollBar_Position;
			if (crCursor.Left != cL || crCursor.Top != cT)
			{
				ProcessTilesSelection();
				SetCursorPosition(cL, cT, 0, 0);
				DrawMap(false);
			}
		}
		else if (dr_mode == drOBJECTS)
		{
			UpdateMapCursorCoordinates(scrX, scrY, LMBtn_main.snap_x, LMBtn_main.snap_y);
			if(ProcessObjectsSelection(scrX, scrY) == true)
			{
				DrawMap(false);
			}
			else if (LMBtn_main.pressed)
			{
				const double xm = (double)(scrX - LMBtn_main.prev.x) / gsGlobalScale;
				const double ym = (double)(scrY - LMBtn_main.prev.y) / gsGlobalScale;
				for (int i = 0; i < DragObjListCount(); i++)
				{
					TMapPerson *obj = GetDragObjFromList(i);
					if (obj->GetObjType() == objMAPOBJ ||
							obj->GetObjType() == objMAPTEXTBOX ||
							obj->GetObjType() == objMAPSOUNDSCHEME ||
							obj->GetObjType() == objMAPOBJPOOL)
					{
						TMapGPerson *g = static_cast<TMapGPerson*>(obj);
						if (!g->ParentObj.IsEmpty() && DragObjListCount() > 1)
							continue;
						if (ActCmplxEnableDragChilds->Checked && !g->ParentObj.IsEmpty())
							continue;
						if (ActLockDecor->Checked && g->GetDecorType() != decorNONE)
							continue;
					}

					if(LMBtn_main.moved == false && LMBtn_main.locked == true)
					{
						AddMarker(obj, TObjMarker::TObjMarkerType::Lock);
					}
                    else if(LMBtn_main.locked == false)
					{
						double x, y;
						obj->getCoordinates(&x, &y);
						SNAP_HELPER_MACRO();
						obj->setCoordinates(x, y, 0, 0);
					}
				}
				LMBtn_main.moved = true;
				LMBtn_main.prev.x = scrX;
				LMBtn_main.prev.y = scrY;
				if (!ActStartEmul->Checked)
				{
					is_save = true;
				}
				DrawMap(false);
			}
		}
		else if (dr_mode == drCOLLIDELINES)
		{
			UpdateMapCursorCoordinates(scrX, scrY, LMBtn_main.snap_x, LMBtn_main.snap_y);
			if(LMBtn_main.collidenode == true)
			{
				LMBtn_main.moved = true;
				const double xm = m_px;
				const double ym = m_py;
				if(m_CopyRectSet)
				{
					ClearCopySelection();
				}
				TMapPerson *mcn = GetLastSelectedDragObj();
				if(mcn != NULL && mcn->GetObjType() == objMAPCOLLIDENODE)
				{
					double x, y;
					TMapCollideLineNode *cn = static_cast<TMapCollideLineNode*>(mcn);
					if(cn->GetPrevCoordinates(x, y) == false)
					{
						SNAP_HELPER_MACRO();
						NormalizeMapCursor(x, y);
						cn->setCoordinates(x, y, 0, 0);
						m_pObjManager->MarkCrosslines(cn);
					}
					else
					{
                    	assert(0);
					}
				}
			}
			if(ProcessObjectsSelection(scrX, scrY) == true)
			{
				DrawMap(false);
			}
			else if (LMBtn_main.pressed)
			{
				const double xm = (double)(scrX - LMBtn_main.prev.x) / gsGlobalScale;
				const double ym = (double)(scrY - LMBtn_main.prev.y) / gsGlobalScale;
				for (int i = 0; i < DragObjListCount(); i++)
				{
					TMapPerson *obj = GetDragObjFromList(i);
					if (obj->GetObjType() == objMAPCOLLIDENODE)
					{
						if(LMBtn_main.moved == false && LMBtn_main.locked == true)
						{
							AddMarker(obj, TObjMarker::TObjMarkerType::Lock);
						}
						else if(LMBtn_main.locked == false)
						{
							double x, y;
							obj->getCoordinates(&x, &y);
							SNAP_HELPER_MACRO();
							NormalizeMapCursor(x, y);
							obj->setCoordinates(x, y, 0, 0);
							m_pObjManager->MarkCrosslines(static_cast<TMapCollideLineNode*>(obj));
						}
					}
				}
				LMBtn_main.moved = true;
			}
			if(LMBtn_main.moved)
			{
				LMBtn_main.prev.x = scrX;
				LMBtn_main.prev.y = scrY;
				if (!crVisible && m_pObjManager->map != NULL)
				{
					crVisible = true;
				}
				DrawMap(false);
			}
		}

		UnicodeString str = _T("x=") + IntToStr(m_px) + _T(":y=") + IntToStr(m_py);
		if (dr_mode == drSTATIC)
		{
			str += _T(":№=") + IntToStr(m_py * m_pObjManager->mp_w + m_px);
		}
		StatusBar1->Panels->Items[0]->Text = str;
		StatusBar1->Panels->Items[0]->Width = StatusBar1->Canvas->TextWidth(str) + GetSystemMetrics(SM_CXFRAME) * 4;
		str = Localization::Text(_T("mScreen")) +
								_T(": x=") + IntToStr((m_px / (d_w / p_w)) / ((dr_mode == drSTATIC) ? 1 : p_w) + 1) +
								_T(":y=") + IntToStr((m_py / (d_h / p_h)) / ((dr_mode == drSTATIC) ? 1 : p_w) + 1);
		StatusBar1->Panels->Items[1]->Text = str;
		StatusBar1->Panels->Items[1]->Width = StatusBar1->Canvas->TextWidth(str) + GetSystemMetrics(SM_CXFRAME) * 4;
	}

	PaintBox1MouseMove_process = false;
}
//---------------------------------------------------------------------------

__int32 __fastcall TForm_m::SnapCoordinate(const __int32& coordinate, const __int32& snapSize)
{
	const __int32 modulo = (coordinate % snapSize);
	const __int32 snap = (modulo < (snapSize / 2)) ? 0 : snapSize;
	return(coordinate - modulo) + snap;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::PaintBox1MouseLeave(TObject * /* Sender */ )
{
	if (!disabled_controlls)
	{
		DrawMap(false);
	}

	mHintTime = -1;
	if (mpHintWnd != NULL)
	{
		mpHintWnd->ReleaseHandle();
	}

	crVisible = false;
	crCursor.Left = -1;
	crCursor.Top = -1;
	StatusBar1->Panels->Items[0]->Text = _T("");
	StatusBar1->Panels->Items[1]->Text = _T("");
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::SetCursorPosition(__int32 cL, __int32 cT, __int32 cR, __int32 cB)
{
	crCursor.Left = cL;
	crCursor.Top = cT;
	crCursor.Right = cR;
	crCursor.Bottom = cB;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DrawCursor(TCanvas *c)
{
	TRect r;
	switch(dr_mode)
	{
	case drSTATIC:
		r = crCursor;
		r.Right = (__int32)((double)crCursor.Left + (double)p_w * gsGlobalScale);
		r.Bottom = (__int32)((double)crCursor.Top + (double)p_h * gsGlobalScale);

		if (gUsingOpenGL)
		{
			TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
			TRenderGL::SetColor(clGray);
			TRenderGL::RenderRect(r.Left, r.Top, r.Width(), r.Height());
			TRenderGL::SetLineStyle(TRenderGL::RGLLSDot);
			TRenderGL::SetColor(clWhite);
			TRenderGL::RenderRect(r.Left, r.Top, r.Width(), r.Height());
			TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
		}
		else
		{
			c->Brush->Color = clGray;
			c->Brush->Style = bsClear; //bsDiagCross;
			c->Pen->Mode = pmNot; //pmMaskPenNot;
			c->Pen->Style = psSolid;
			c->Pen->Color = clGray;
			c->Rectangle(r);
			c->Pen->Style = psDot;
			c->Pen->Color = clBlack;
			c->Pen->Mode = pmCopy;
			c->Rectangle(r);
		}
		break;
	default:
	break;
	}
}
//---------------------------------------------------------------------------

bool __fastcall TForm_m::SetDrMode(__int32 mode)
{
	if (mode == drNONE)
	{
		HideObjPrpToolWindow();
		CancelCollideLine();
		ListView_u->Color = clBtnFace;
		PaintBox1MouseLeave(NULL);
		dr_mode = mode;
		TBObjects->Down = false;
		TBBackground->Down = false;
		TBCollideNodes->Down = false;
		FakeControl->Visible = true;
		FakeControl->SetFocus();
		ClearCopySelection();
		DragObjListClear();
		if (FBGObjWindow)
			if (FBGObjWindow->Visible)
				FBGObjWindow->Hide();
	}
	else if (mode == drCOLLIDELINES)
	{
		CancelCollideLine();
		HideObjPrpToolWindow();
		ListView_u->Color = clBtnFace;
		PaintBox1MouseLeave(NULL);
		dr_mode = mode;
        CheckMenuCollideLines();
		TBObjects->Down = false;
		TBBackground->Down = false;
		TBCollideNodes->Down = true;
		FakeControl->Visible = true;
		FakeControl->SetFocus();
		DragObjListClear();
		if (FBGObjWindow->Visible)
			FBGObjWindow->Hide();
		DrawMap(true);
	}
	else if (mode == drSTATIC)
	{
		HideObjPrpToolWindow();
		CancelCollideLine();
		ClearCopySelection();
		ListView_u->Color = clBtnFace;
		PaintBox1MouseLeave(NULL);
		dr_mode = mode;
		TBObjects->Down = false;
		TBBackground->Down = true;
		TBCollideNodes->Down = false;
		if (!FBGObjWindow->Visible && proj_exist && !ActStartEmul->Checked)
			FBGObjWindow->Show();
	}
	else if (mode == drOBJECTS)
	{
		CancelCollideLine();
		ClearCopySelection();
		ListView_u->Color = clWindow;
		PaintBox1MouseLeave(NULL);
		dr_mode = mode;
		TBBackground->Down = false;
		TBObjects->Down = true;
		TBCollideNodes->Down = false;
		if (FBGObjWindow->Visible)
			FBGObjWindow->Hide();
	}
	DragObjListClear();
	SetZoneVisibility();
	return true;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActObjModeExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	TBObjects->Down = true;
	ListView_u->SetFocus();
	SetDrMode(drOBJECTS);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActBackModeExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	if (!SetDrMode(drSTATIC))
	{
		return;
	}
	TBBackground->Down = true;
	//CategoryButtons_s->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActCollideNodesModeExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	if (!SetDrMode(drCOLLIDELINES))
	{
		return;
    }
	TBCollideNodes->Down = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::SetZoneVisibility()
{
	__int32 i, j;
	bool visible;
	if (!map_exist)
	{
		return;
	}

    const int lcount = 6;

	TObjManager::TObjType tlist[lcount] =
	{
		TObjManager::CHARACTER_OBJECT,
		TObjManager::BACK_DECORATION,
		TObjManager::FORE_DECORATION,
		TObjManager::TRIGGER,
		TObjManager::DIRECTOR,
		TObjManager::COLLIDESHAPE
	};
	bool b1list[lcount] =
	{
		true,
		true,
		true,
		true,
		showOBJDirectors,
		/*showCollideMap*/false
	};

	for (j = 0; j < lcount; j++)
	{
		__int32 len = m_pObjManager->GetObjTypeCount(tlist[j]);
		for (i = 0; i < len; i++)
		{
			TMapPerson *mo = (TMapPerson*)m_pObjManager->GetObjByIndex(tlist[j], i);

			bool cond = showObjects && dr_mode == drOBJECTS && (znCurZone == mo->GetZone() || mo->GetZone() < 0);
			bool b2list[lcount] =
			{
				cond,
				cond,
				cond,
				cond,
				showObjects && showOBJDirectors && dr_mode == drOBJECTS,
				showObjects && showCollideMap && dr_mode == drCOLLIDELINES
			};
			cond = znCurZone != mo->GetZone() && mo->GetZone() >= 0;
			bool trlist[lcount] =
			{
				cond,
				cond,
				cond,
				cond,
				false,
				false
			};

			if (ActStartEmul->Checked)
			{
				mo->SetVisible(b1list[j]);
				mo->ShowInterlacedTransparency(trlist[j]);
			}
			else
			{
				visible = b2list[j];
				mo->ShowInterlacedTransparency(!visible);
				mo->SetVisible(visible);
			}
		}
	}
	DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CategoryButtons_sEnter(TObject * /* Sender */ )
{
  //SetDrMode(((TCategoryButtons*)Sender)->Tag);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ListView_sEnter(TObject *Sender)
{
	SetDrMode(static_cast<TListView*>(Sender)->Tag);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ListView_uMouseDown(TObject *Sender, TMouseButton /* Button */ , TShiftState /* Shift */ , __int32 /* X */ , __int32 /* Y */ )
{
	__int32 cdm = static_cast<TListView*>(Sender)->Tag;
	if (dr_mode != cdm)
	{
		SetDrMode(cdm);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ListBoxZoneEnter(TObject * /* Sender */ )
{
	SetDrMode(drOBJECTS);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::PopupMenu_uPopup(TObject * /* Sender */ )
{
	checkMenuGameObj();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::PopupMenu_sPopup(TObject * /* Sender */ )
{
	checkMenuBackObj();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//ACTIONS
void __fastcall TForm_m::ActNewExecute(TObject *)
{
	__int32 i;
	CancelCollideLine();
	DropObjOnDrag();

	if (is_save && AskToSave() == ID_CANCEL)
	{
		return; //спросить, не сохранить ли изм.
	}

	map_exist = false;
	disabled_controlls = true;
	m_pObjManager = NULL;
	TabSetObjInit();
	for (i = 0; i < m_pLevelList->Count; i++)
	{
		delete (TObjManager*)m_pLevelList->Items[i];
	}
	m_pLevelList->Clear();
	TabLevels->Tabs->Clear();
	TabLevels->Tabs->Add(_T("0"));

	if (mainCommonRes != NULL)
	{
		mainCommonRes->ClearGraphicResources();
		ReleaseRenderGL();
		delete mainCommonRes;
	}
	else
	{
		ReleaseRenderGL();
	}
	mainCommonRes = new TCommonResObjManager();
	InitRenderGL();

	CreateServiceData();
	AddSpecialObj();
	p_w = DEF_TILE_SIZE;
	p_h = DEF_TILE_SIZE;
	lrCount = 1;
	CreateZoneList();

	proj_exist = true;
	rebuildBackObjListView();
	rebuildGameObjListView();
	CreateMap(true);

	InitPrpWindows(mainCommonRes);

	CreateLanguageMenu();
	is_save = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActGridExecute(TObject *)
{
	showGrid = !showGrid;
	ActGrid->Checked = showGrid;

	TIniFile *ini;
	ini = new TIniFile(PathIni + _T("editor.ini"));
	ini->WriteBool(_T("Options"), _T("showGrid"), showGrid);
	if(showGrid)
	{
		gridColor = static_cast<TColor>(ini->ReadInteger(_T("Options"), _T("gridColor"), static_cast<int>(gridColor)));
	}
	delete ini;

	DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActInfoExecute(TObject *)
{
	if (!map_exist)
	{
		return;
	}

	Application->CreateForm(__classid(TFSaveVarList), &FSaveVarList);
	FSaveVarList->Caption = ActInfo->Caption;
	FSaveVarList->Bt_save->Visible = false;
	FSaveVarList->Bt_exit->Left = FSaveVarList->Memo->Left + FSaveVarList->Memo->Width - FSaveVarList->Bt_exit->Width;

	Application->CreateForm(__classid(TFWait), &FWait);
	FWait->Left = Form_m->Left + (Form_m->Width - FWait->Width) / 2;
	FWait->Top = Form_m->Top + (Form_m->Height - FWait->Height) / 2;
	FWait->Enabled = true;

	mpgGlogalStrings = new TStringList();

	//DoThreadProcessMapInfo();
	new TRenderThread(&DoThreadProcessMapInfo); //free res on finish automatically

	if(FWait->Enabled)
	{
		FWait->ShowModal();
	}

	FWait->Free();
	FWait = NULL;

	FSaveVarList->Memo->Lines->AddStrings(mpgGlogalStrings);
	delete mpgGlogalStrings;
	mpgGlogalStrings = NULL;

	FSaveVarList->ShowModal();
	FSaveVarList->Free();
	FSaveVarList = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DoThreadProcessMapInfo()
{
	__int32 tile_count;
	UnicodeString res_str, res_name;
	__int32 mem = 0;

	std::list<TResourceItem>::iterator it;
	std::list<TResourceItem> *str_lst;
	std::list<TResourceItem> *str_strm_lst;

	str_lst = createImgListForSave(lstMap, m_pObjManager);
	mpgGlogalStrings->Add(Localization::Text(_T("mResourcesList")) + _T(":"));
	mpgGlogalStrings->Add(UnicodeString());
	if (str_lst->empty())
	{
		res_str = Localization::Text(_T("mResourcesListEmpty"));
		mpgGlogalStrings->Add(res_str);
	}
	else
	{
		for (it = str_lst->begin(); it != str_lst->end(); ++it)
		{
			if (it->mType == RESOURCE_TYPE_FONTDATA || it->mName.IsEmpty())
			{
				continue;
			}
			mem += it->mSize;
			mpgGlogalStrings->Add(it->mName);
		}
		mpgGlogalStrings->Add(UnicodeString());
		mpgGlogalStrings->Add(Localization::Text(_T("mFontsList")) + _T(":"));
		mpgGlogalStrings->Add(UnicodeString());
		int fonts_count = 0;
		for (it = str_lst->begin(); it != str_lst->end(); ++it)
		{
			if (it->mType != RESOURCE_TYPE_FONTDATA || it->mName.IsEmpty())
			{
				continue;
			}
			mem += it->mSize;
			mpgGlogalStrings->Add(it->mName);
			fonts_count++;
		}
		if(fonts_count == 0)
		{
			mpgGlogalStrings->Add(Localization::Text(_T("mFontsListEmpty")));
		}
	}
	mpgGlogalStrings->Add(UnicodeString());
	str_strm_lst = createStreamObjBufferList(m_pObjManager);
	for (it = str_strm_lst->begin(); it != str_strm_lst->end(); ++it)
	{
		mem += it->mSize;
		mpgGlogalStrings->Add(it->mName);
	}
	{
		TList *tlist = new TList();
		CreateOptimizedBackObjListAndMap(tlist, NULL, m_pObjManager, nullptr, nullptr);
		tile_count = tlist->Count;
		delete tlist;
	}
	if(m_pObjManager->ignoreUnoptimizedResoures)
	{
		mpgGlogalStrings->Add(Localization::Text(_T("mIgnoreUnoptimizedResouresWarning")));
	}
	mpgGlogalStrings->Add(Localization::Text(_T("mResourcesListCount")) + _T(": ") + IntToStr((__int32)str_lst->size()));
	mpgGlogalStrings->Add(Localization::Text(_T("mStreamingBuffers")) + _T(": ") + IntToStr((__int32)str_strm_lst->size()));
	mpgGlogalStrings->Add(Localization::Text(_T("mApproxMemoryUsing")) + _T(": ") + IntToStr(mem) + _T(" ")+ Localization::Text(_T("mBytes")));
	mpgGlogalStrings->Add(UnicodeString());
	mpgGlogalStrings->Add(Localization::Text(_T("mBackgroung")) + _T(":"));
	mpgGlogalStrings->Add(UnicodeString());
	mpgGlogalStrings->Add(Localization::Text(_T("mMapInScreens")) + _T(": ") + IntToStr(m_w / d_w) + _T(" x ") + IntToStr(m_h / d_h));
	mpgGlogalStrings->Add(Localization::Text(_T("mMapInBgObjects")) + _T(": ") + IntToStr(m_pObjManager->mp_w) + _T(" x ") + IntToStr(m_pObjManager->mp_h));
	mpgGlogalStrings->Add(Localization::Text(_T("mUniqBgObjCount")) + _T(": ") + IntToStr(tile_count));
	mpgGlogalStrings->Add(Localization::Text(_T("mLayersMax")) + _T(": ") + IntToStr(lrCount));

	str_strm_lst->clear();
	delete str_strm_lst;
	str_lst->clear();
	delete str_lst;

	str_lst = createImgListForSave(lstBackObjOnly, m_pObjManager);
	res_str = _T("");
	if (str_lst->empty())
	{
		res_str = Localization::Text(_T("mMapResourcesListEmpty"));
	}
	else
	{
		for (it = str_lst->begin();;)
		{
			if (it->mName.IsEmpty())
			{
				continue;
			}
			res_str += it->mName;
			++it;
			if (it != str_lst->end())
			{
				res_str += _T(", ");
			}
			else
			{
				break;
			}
		}
    }
	mpgGlogalStrings->Add(Localization::Text(_T("mMapResourcesList")) + _T(": ") + res_str);
	mpgGlogalStrings->Add(Localization::Text(_T("mMapResourcesListCount")) + _T(": ") + IntToStr((int)str_lst->size()));
	mpgGlogalStrings->Add(UnicodeString());
	mpgGlogalStrings->Add(Localization::Text(_T("mMapObjects")) + _T(":"));
	mpgGlogalStrings->Add(UnicodeString());
	mpgGlogalStrings->Add(Localization::Text(_T("mMapBgObjets")) + _T(": ") + IntToStr(m_pObjManager->GetObjTypeCount(TObjManager::BACK_DECORATION)));
	__int32 objCount = m_pObjManager->GetTotalMapCharacterObjectsCount();
	mpgGlogalStrings->Add(Localization::Text(_T("mMapPrObjets")) + _T(": ") + IntToStr(objCount));
	__int32 o_count = m_pObjManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
	objCount = 0;
	for (__int32 j = 0; j < o_count; j++)
	{
		const TMapGPerson *gp = (TMapGPerson*)m_pObjManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, j);
		if(gp->GetObjType() == objMAPOBJPOOL)
		{
			const TMapObjPool *op = (TMapObjPool*)gp;
			objCount += op->GetTolalObjCount();
		}
	}
	mpgGlogalStrings->Add(Localization::TextWithVar(_T("mMapPrObjetsPool_s"), IntToStr(objCount)));
	mpgGlogalStrings->Add(Localization::Text(_T("mMapFgObjets")) + _T(": ") + IntToStr(m_pObjManager->GetObjTypeCount(TObjManager::FORE_DECORATION)));
	mpgGlogalStrings->Add(Localization::Text(_T("mMapTriggers")) + _T(": ") + IntToStr(m_pObjManager->GetObjTypeCount(TObjManager::TRIGGER)));
	mpgGlogalStrings->Add(Localization::Text(_T("mMapDirectors")) + _T(": ") + IntToStr(m_pObjManager->GetObjTypeCount(TObjManager::DIRECTOR)));
	mpgGlogalStrings->Add(Localization::Text(_T("mCollideShapes")) + _T(": ") + IntToStr(m_pObjManager->GetObjTypeCount(TObjManager::COLLIDESHAPE)));
	mpgGlogalStrings->Add(Localization::Text(_T("mZonesCount")) + _T(": ") + IntToStr(m_pObjManager->znCount));

	str_lst->clear();
	delete str_lst;

	str_lst = createImgListForSave(lstMapObjOnly, m_pObjManager);
	res_str = _T("\0");
	if (str_lst->empty())
	{
		res_str = Localization::Text(_T("mMapResourcesListEmpty"));
	}
	else
	{
		for (it = str_lst->begin();;)
		{
			if (it->mName.IsEmpty())
			{
				continue;
			}
			res_str += it->mName;
			++it;
			if (it != str_lst->end())
			{
				res_str += _T(", \0");
			}
			else
			{
				break;
			}
		}
  }

	mpgGlogalStrings->Add(Localization::Text(_T("mMapResourcesList")) + _T(": ") + res_str);
	mpgGlogalStrings->Add(Localization::Text(_T("mMapResourcesListCount")) + _T(": ") + IntToStr((__int32)str_lst->size()));

	str_lst->clear();
	delete str_lst;

	TStringList *tstrings = new TStringList();
	CheckLevelForWarningsAndErrors(tstrings);
	if (tile_count >= MAX_BG_TILETYPES)
	{
		if (tstrings->Count > 0)
		{
			tstrings->Add(UnicodeString());
		}
		tstrings->Add(Localization::Text(_T("mMaximumBgObjectsExceeded")));
		tstrings->Add(Localization::TextWithVar(_T("mMaximumBgObjects_s"), IntToStr(MAX_BG_TILETYPES)));
	}
	const __int32 len = tstrings->Count;
	if (len > 0)
	{
		mpgGlogalStrings->Add(UnicodeString());
		mpgGlogalStrings->Add(Localization::Text(_T("mMessageBoxWarning")));
		mpgGlogalStrings->Add(UnicodeString());
		for (__int32 i = 0; i < len; i++)
		{
			mpgGlogalStrings->Add(tstrings->Strings[i]);
		}
	}
	delete tstrings;

	FWait->Enabled = false;
	FWait->Close();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CheckLevelForWarningsAndErrors(TStringList *str_list)
{
	m_pObjManager->CheckForWarningsAndErrors(str_list);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActHideObjectsExecute(TObject * /* Sender */ )
{
	showObjects = !showObjects;
	DragObjListClear();
	ActHideObjects->Checked = !showObjects;
	rebuildGameObjects();
	SetZoneVisibility();
	MainMenu1Change(NULL, NULL, false);
	if (FObjOrder->Visible)
	{
		FObjOrder->RebuildSelectionList();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActHidePathExecute(TObject * /* Sender */ )
{
	ActHidePath->Checked = !ActHidePath->Checked;
	showOBJDirectors = !ActHidePath->Checked;
	SetZoneVisibility();
}
//---------------------------------------------------------------------------

//Обводить края объекта карты
void __fastcall TForm_m::ActShowRectObjExecute(TObject * /* Sender */ )
{
	showRectGameObj = !showRectGameObj;
	ActShowRectObj->Checked = showRectGameObj;

	TIniFile *ini;
	ini = new TIniFile(PathIni + _T("editor.ini"));
	ini->WriteBool(_T("Options"), _T("showRectGameObj"), showRectGameObj);
	delete ini;

	DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActShowBGTransMapExecute(TObject * /* Sender */ )
{
	ActShowBGTransMap->Checked = !ActShowBGTransMap->Checked;
	showCollideMap = ActShowBGTransMap->Checked;

	TIniFile *ini;
	ini = new TIniFile(PathIni + _T("editor.ini"));
	ini->WriteBool(_T("Options"), _T("showCollideLines"), showCollideMap);
	delete ini;

	DrawMap(true);
}
//---------------------------------------------------------------------------

//Показывать объекты неактивных зон
void __fastcall TForm_m::ShowNonActiveZoneObjExecute(TObject * /* Sender */ )
{
	ShowNonActiveZoneObj->Checked = !ShowNonActiveZoneObj->Checked;
	SetZoneVisibility();
}
//---------------------------------------------------------------------------

//Эвенты (TImage*) картинки

void __fastcall TForm_m::Image_dDblClick(TObject * /* Sender */ )
{
	if (dr_mode == drOBJECTS)
	{
		AddStaticToMap(false);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ReadIniData(bool langmode)
{
	TIniFile *ini;
	ini = new TIniFile(PathIni + _T("editor.ini"));

	if(langmode)
	{
		Lang = ini->ReadString(_T("Options"), _T("Language"), INTERFACE_LANG_DEFAULT);
		delete ini;
	}
	else
	{
		PathPngRes0 = ini->ReadString(_T("Path"), _T("PathPngRes"), PathIni);
		PathPngRes0 = CheckPathWithDelimeter(PathPngRes0, true);
		if(IsRelativePath(PathPngRes0))
		{
			PathPngRes = PathIni + PathPngRes0;
		}
		if(!DirectoryExists(PathPngRes))
		{
			PathPngRes0 = PathPngRes = PathIni;
		}

		map_file_name = ini->ReadString(_T("Path"), _T("LastMapFilesPath"), "");
		if(!map_file_name.IsEmpty() && DirectoryExists(map_file_name))
		{
			if(IsRelativePath(map_file_name))
			{
				map_file_name = PathIni + map_file_name;
			}
		}
		else
		{
			map_file_name = "";
		}

		h_file_name = ini->ReadString(_T("Path"), _T("LastConstantsPath"), "");
		if(!h_file_name.IsEmpty() && FileExists(h_file_name))
		{
			if(IsRelativePath(h_file_name))
			{
				h_file_name = PathIni + h_file_name;
			}
		}
		else
		{
			h_file_name = "";
		}

		PathPattern = ini->ReadString(_T("Path"), _T("PathPattern"), PathIni);
		PathObjPalette = ini->ReadString(_T("Path"), _T("PathObjPalette"), PathIni);
		showRectGameObj = ini->ReadBool(_T("Options"), _T("showRectGameObj"), true);
		showCollideMap = ini->ReadBool(_T("Options"), _T("showCollideLines"), true);
		showGrid = ini->ReadBool(_T("Options"), _T("showGrid"), true);
		gridColor = static_cast<TColor>(ini->ReadInteger(_T("Options"), _T("gridColor"), static_cast<int>(gridColor)));
		gUsingOpenGL = ini->ReadBool(_T("Options"), _T("UsingOpenGL"), true);
		mSaveProgDataXMLFormat = ini->ReadBool(_T("Options"), _T("SaveAsXML"), true);
		gsGlobalScale = ini->ReadFloat(_T("Options"), _T("Scale"), 1.0);
		delete ini;

		if(DirectoryExists(PathPngRes) == false)
		{
			UnicodeString tPathPngRes = CheckPathWithDelimeter(PathIni + RAWRES_NAME, false);
			if(DirectoryExists(tPathPngRes))
			{
				PathPngRes0 = PathPngRes = tPathPngRes;
			}
		}

		PathPngRes = CheckPathWithDelimeter(PathPngRes, true);

		if(!gUsingOpenGL && mpBGBmp == NULL)
		{
			mpBGBmp = new Graphics::TBitmap();
			mpBGBmp->PixelFormat = pf32bit;
			mpBackBufferBmp = new Graphics::TBitmap();
			mpBackBufferBmp->PixelFormat = pf32bit;
		}

		EditorOptions(true);
		SetScale(gsGlobalScale);
	}
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TForm_m::CheckPathWithDelimeter(UnicodeString path, bool useDefaultPathifEmpty)
{
	path = path.Trim();
	if(path.Length() > 0)
	{
		__int32 pos = path.LastDelimiter("\\/");
		if(pos != path.Length())
		{
			path += _T("\\");
		}
		return path;
	}
	else if(useDefaultPathifEmpty)
	{
		return PathIni;
	}
	return path;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::RefreshFileName()
{
	if (proj_exist)
	{
		const UnicodeString str = Localization::Text(_T("mProject")) + _T(": ") + ExtractFileName(file_name);
		StatusBar1->Panels->Items[2]->Text = str;
		StatusBar1->Panels->Items[2]->Width = StatusBar1->Canvas->TextWidth(str) + GetSystemMetrics(SM_CXFRAME) * 4;
	}
	else
	{
		StatusBar1->Panels->Items[2]->Width = 0;
		StatusBar1->Panels->Items[2]->Text = _T("");
	}
}
//---------------------------------------------------------------------------

//УСТАНОВКА АКТИВНОСТИ МЕНЮ
void __fastcall TForm_m::checkMenuBackObj()
{
	/* bool EmStart = ActStartEmul->Checked;
			 bool a = CategoryButtons_s->SelectedItem != NULL;
			 ActBackEdit->Enabled = a && !EmStart;
			 ActBackDel->Enabled = a && !EmStart;
			 ActBackAdd->Enabled = proj_exist && !EmStart;
	 */
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::checkMenuGameObj()
{
	bool EmStart = ActStartEmul->Checked;
	bool isParentObjSelected = ListView_u->Selected != nullptr;
	bool isSystemObjTab = TabSetObj->TabIndex == 0;  // system obj tab
	if (isParentObjSelected && isSystemObjTab)
	{
		isParentObjSelected = false;
	}
	ActObjEdit->Enabled = isParentObjSelected && !EmStart;
	ActObjDel->Enabled = isParentObjSelected && !EmStart;
	ActObjAdd->Enabled = proj_exist && !EmStart && !isSystemObjTab;
	ActSetVarDefGroup->Enabled = isParentObjSelected && !EmStart && !isSystemObjTab;
	ActSetVarparentToDef->Enabled = isParentObjSelected && !EmStart && !isSystemObjTab;
	CreatePopupMoveTo();
	NPMUMoveto->Enabled = isParentObjSelected && !EmStart;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//РЕДАКТИРОВАНИЕ ГРАФИКИ ОБЪЕКТОВ ИГРЫ
//---------------------------------------------------------------------------
TFEditGrObj *__fastcall TForm_m::getSelectionBlock(TCommonResObjManager *commonResManager, bool allowChangePath)
{
	UnicodeString path;
	assert(!TRenderGL::IsInited());
	Application->CreateForm(__classid(TFEditGrObj), &FEditGrObj);
	FEditGrObj->Init(commonResManager);
	do
	{
		const UnicodeString messageWndCaption = Localization::Text(_T("mResPathWindow"));
		FEditGrObj->FileListBox->Clear();
		path = PathPngRes;
		if (!path.IsEmpty())
		{
			if (!DirectoryExists(path))
			{
				path = _T("");
			}
		}
		if (path.IsEmpty())
		{
			if (allowChangePath)
			{
				if (Application->MessageBox(Localization::Text(_T("mAskResPath")).c_str(),
									messageWndCaption.c_str(),
									MB_YESNO | MB_ICONQUESTION) == ID_YES)
				{
				  FEditGrObj->FileListBox->Directory = PathIni;
				  ActPathExecute(NULL);
				}
				else
				{
					FEditGrObj->Free();
					return NULL;
				}
			}
			else
			{
				Application->MessageBox(Localization::Text(_T("mResPathWarning")).c_str(),
                                messageWndCaption.c_str(),
                                MB_ICONERROR);
				return NULL;
			}
		}
	}
	while (path.IsEmpty());
	UnicodeString ext = _T("*.xxx");
	FEditGrObj->FileListBox->Mask = forceCorrectResFileExtention(ext);
	FEditGrObj->FileListBox->Directory = path;

	return FEditGrObj;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::AssignGraphicToObjects(UnicodeString path, std::list<TResourceItem> *list, UnicodeString *errorlist)
{
	assert(mainCommonRes);
	mainCommonRes->ClearGraphicResources();
    CreateServiceData();
	AddSpecialObj();
	mainCommonRes->AssignGraphicToObjects(list, path , errorlist);
}
//---------------------------------------------------------------------------

//НАСТРОЙКИ ПУТЕЙ
void __fastcall TForm_m::ActPathExecute(TObject *)
{
	TIniFile *ini;
	UnicodeString tPathPngRes, tPathMessages, errorlist;
	tPathPngRes = _T("");
	tPathMessages = _T("");

	Application->CreateForm(__classid(TFProjPath), &FProjPath);
	FProjPath->Caption = ActPath->Caption;
	FProjPath->EdPngPath->Text = PathPngRes0;
	if (FProjPath->ShowModal() == mrOk)
	{
		tPathPngRes = FProjPath->EdPngPath->Text;
		tPathPngRes = CheckPathWithDelimeter(tPathPngRes, false);
	}
	FProjPath->Free();
	FProjPath = NULL;

	if (tPathPngRes != PathPngRes0 && !tPathPngRes.IsEmpty())
	{
		TList *str_lst;
		PathPngRes0 = tPathPngRes;
		if(IsRelativePath(PathPngRes0))
		{
			PathPngRes = PathIni + PathPngRes0;
		}
		//перезагружаем всю графику
		if (map_exist)
		{
			std::list<TResourceItem> *list = createImgListForSave(lstForReload, m_pObjManager);
			m_pObjManager->ClearCashe();
			AssignGraphicToObjects(PathPngRes, list, &errorlist);
            list->clear();
			delete list;
			RefreshFonts();
			rebuildBackObjListView();
			rebuildGameObjListView(); //пересобрать окна выбора объектов карты
			rebuildGameObjects(); //пересобираем все объекты игры на карте
			InitPrpWindows(mainCommonRes);
			m_pObjManager->RefreshAllTextsObjects();
			LoadTexturesToVRAMGL();
			DrawMap(true); //рисуем
		}
	}
	ini = new TIniFile(PathIni + _T("editor.ini"));
	ini->WriteString(_T("Path"), _T("PathPngRes"), ExtractRelativePath(PathIni, PathPngRes));
	delete ini;
	ShowLoadErrorMessages(errorlist);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ShowLoadErrorMessages(UnicodeString &errstr)
{
	if(!errstr.IsEmpty())
	{
		Application->CreateForm(__classid(TFSaveVarList), &FSaveVarList);
		FSaveVarList->Caption = Localization::Text(_T("mErrorReport"));
		FSaveVarList->SaveDialog->Title = Localization::Text(_T("mSaveErrorReportToFile"));
		FSaveVarList->Memo->Lines->Text = errstr;
		FSaveVarList->ShowModal();
		FSaveVarList->Free();
		FSaveVarList = NULL;
	}
}
//---------------------------------------------------------------------------

//ШАБЛОНЫ ОБЪЕКТОВ КАРТЫ
void __fastcall TForm_m::ActObjPatternExecute(TObject * /* Sender */ )
{
	__int32 i;
	DropObjOnDrag();
	Application->CreateForm(__classid(TFObjPattern), &FObjPattern);
	FObjPattern->Caption = ActObjPattern->Caption;
	FObjPattern->SetPattern(mainCommonRes->MainGameObjPattern);
	if (FObjPattern->ShowModal() == mrOk)
	{
		HideObjPrpToolWindow();
		FObjPattern->GetPattern(mainCommonRes->MainGameObjPattern);
		for (i = 0; i < m_pLevelList->Count; i++)
		{
			TObjManager *om = (TObjManager*)m_pLevelList->Items[i];
			om->AssignMainPattern(mainCommonRes->MainGameObjPattern);
		}
		is_save = true;
		InitPrpWindows(mainCommonRes);
	}
	FObjPattern->Free();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//ФРАГМЕНТЫ ФОНА ИГРЫ
//---------------------------------------------------------------------------

//добавить объект фона
void __fastcall TForm_m::ActBackAddExecute(TObject * /* Sender */ )
{
	backObjAddEditDel(mAdd);
	checkMenuBackObj();
}

//---------------------------------------------------------------------------
//редактировать объект фона
void __fastcall TForm_m::ActBackEditExecute(TObject * /* Sender */ )
{
	backObjAddEditDel(mEdit);
	DrawMap(true); //перерисовать
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActBackDelExecute(TObject * /* Sender */ )
{
	backObjAddEditDel(mDel);
	checkMenuBackObj();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CategoryButtons_sKeyDown(TObject * /* Sender */ , WORD &Key, TShiftState /* Shift */ )
{
	checkMenuBackObj();
	if (Key == VK_INSERT)
	{
		Key = 0;
		ActBackAdd->Execute();
	}
	else if (Key == VK_RETURN)
	{
		Key = 0;
		ActBackEdit->Execute();
	}
	else if (Key == VK_DELETE)
	{
		Key = 0;
		ActBackDel->Execute();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::backObjAddEditDel(__int32 /* mode */ )
{
}
//---------------------------------------------------------------------------
/*
__int32 __fastcall TForm_m::checkIfExists(GMapBGObj *tesTFrameObj)
{
	GMapBGObj *BGobj;
	for (__int32 i = 0; i < mainCommonRes->BackObj->Count; i++)
	{
		if (mainCommonRes->BackObj->Items[i] == (void*)tesTFrameObj)
		{
			continue;
		}
		BGobj = (GMapBGObj*)mainCommonRes->BackObj->Items[i];
		if (tesTFrameObj->CompareTo(BGobj) == true)
		{
			return i;
		}
	}
	return -1;
}
//---------------------------------------------------------------------------
*/
void __fastcall TForm_m::editBackObj(__int32 /* mode */ , __int32 /* idx */ , GMapBGObj * /* MapBGobj */ )
{
}
//---------------------------------------------------------------------------

//добавляем картинку в превьюшку листвьюва
void __fastcall TForm_m::editBackObjListViewBmp(__int32 /* mode */ , __int32 /* idx */ , const GMapBGObj&, UnicodeString /* caption */ )
{
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TForm_m::createCaption(const GMapBGObj &MapBGobj)
{
	UnicodeString caption;
	const TFrameObj *FrameObj = MapBGobj.GetFrameObj(0);
	if (FrameObj != NULL)
	{
		caption = _T(" (") + FrameObj->bgPropertyName + _T(")");
	}
	else
	{
		caption = _T("");
	}
	return caption;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::rebuildBackObjListView()
{
	FBGObjWindow->SetGridSize(p_w, p_h);
	FBGObjWindow->ArrangePositions();
}
//---------------------------------------------------------------------------

//подогнать картинку под размеры листвьюва
void __fastcall TForm_m::resizeBmpForListView(BMPImage *ibmp, __int32 w, __int32 h, double scale)
{
	TRect rect, irect;
	BMPImage *t_bmp;
	assert(ibmp);

	t_bmp = new BMPImage(NULL);
	t_bmp->Assign(ibmp);

	scale = 1.0f;

	ibmp->SetSize(w, h);
	rect.Left = 0;
	rect.Top = 0;
	rect.Right = w;
	rect.Bottom = h;
	ibmp->Clear(TColor(0x00000000));

	rect.Right = (__int32)((double)t_bmp->Width / scale);
	rect.Bottom = (__int32)((double)t_bmp->Height / scale);

	if (rect.Height() < h && rect.Width() < w)
	{
		irect.Left = (w - rect.Width()) / 2;
		irect.Top = (h - rect.Height()) / 2;
		irect.Right = irect.Left + rect.Width();
		irect.Bottom = irect.Top + rect.Height();
		ibmp->Canvas->StretchDraw(irect, const_cast<Graphics::TBitmap*>(t_bmp->GetBitmapData()));
	}
	else
	{
		double ratio;
		__int32 var;
		if (rect.Width() > rect.Height())
		{
			ratio = (double)w / (double)rect.Width();
			irect.Left = 0;
			irect.Right = w;
			var = (int)((double)rect.Height() * ratio);
			irect.Top = (h - var) / 2;
			irect.Bottom = irect.Top + var;
		}
		else
		{
			ratio = (double)h / (double)rect.Height();
			irect.Top = 0;
			irect.Bottom = h;
			var = (int)((double)rect.Width() * ratio);
			irect.Left = (w - var) / 2;
			irect.Right = irect.Left + var;
		}
		ibmp->Canvas->StretchDraw(irect, const_cast<Graphics::TBitmap*>(t_bmp->GetBitmapData()));
	}
	delete t_bmp;
}
//---------------------------------------------------------------------------

//создать пустое (каркас) изображение листвьюва
void __fastcall TForm_m::makeEmptyPreview(BMPImage* ibmp, UnicodeString caption, __int32 w, __int32 h)
{
	assert(ibmp);
	TRect rect;
	rect.Left = 0;
	rect.Top = 0;
	ibmp->SetSize(w, h);
	rect.Bottom = h;
	rect.Right = w;
	ibmp->Canvas->Brush->Color = clWhite;
	ibmp->Canvas->FillRect(rect);
	ibmp->Canvas->Font->Size = 5;
	ibmp->Canvas->Font->Color = clRed;
	ibmp->Canvas->Font->Name = _T("Small Fonts");
	ibmp->Canvas->TextOut(1, -1, caption);
	ibmp->Canvas->Brush->Color = clGray;
	ibmp->Canvas->FrameRect(rect);
}
//---------------------------------------------------------------------------

//ОБЪЕКТЫ ИГРЫ

void __fastcall TForm_m::ActLockGameObjectExecute(TObject *Sender)
{
	if(!disabled_controlls && proj_exist)
	{
		bool hasChanges = false;
		for (int i = 0; i < DragObjListCount(); i++)
		{
			TMapPerson *mp = GetDragObjFromList(i);
			if (mp->GetObjType() != objMAPCOLLIDESHAPE)
			{
				mp->Lock(!mp->IsLocked());
				hasChanges = true;
			}
		}
		if(hasChanges)
		{
			LMBtn_main.locked = true;
			is_save = true;
			FObjOrder->RebuildList();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActObjAddExecute(TObject * /* Sender */ )
{
	gameObjAddEditDel(mAdd);
	checkMenuGameObj();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActObjDelExecute(TObject * /* Sender */ )
{
	gameObjAddEditDel(mDel);
	checkMenuGameObj();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActObjEditExecute(TObject * /* Sender */ )
{
	gameObjAddEditDel(mEdit);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ListView_uKeyDown(TObject * /* Sender */ , WORD &Key, TShiftState Shift)
{
	if (disabled_controlls)
	{
		return;
	}

	checkMenuGameObj();

	if (TabSetObj->TabIndex == 0)
	{
		return;
	}

	if (Key == VK_INSERT)
	{
		Key = 0;
		ActObjAdd->Execute();
	}
	else if (Key == VK_RETURN)
	{
		Key = 0;
		ActObjEdit->Execute();
	}
	else if (Key == VK_DELETE)
	{
		if (Shift.Contains(ssCtrl))
		{
			Key = 0;
			ActObjDel->Execute();
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ListView_uDblClick(TObject * /* Sender */ )
{
	if (disabled_controlls)
		return;

	checkMenuGameObj();

	if (TabSetObj->TabIndex == 0)
	{
		return;
	}

	ActObjEdit->Execute();
}
//---------------------------------------------------------------------------

bool __fastcall TForm_m::ParentObjAddEditDel(UnicodeString &gpname, __int32 mode)
{
	GPerson *tGpers;
	TList *lst, *lst_d;
	bool res, cancel;
	__int32 pos, i, k, j, gpidx;
	UnicodeString oldname;
	TMapObjDecorType oldType;

	FGOProperty->Hide();
	FGOProperty->assignMapGobj(NULL);
	FObjPoolProperty->Hide();
	FObjPoolProperty->AssignObjPoolObj(NULL);

	gpidx = mainCommonRes->GetParentObjID(gpname);

	// удалить
	if (mode == mDel)
	{
		assert(!gpname.IsEmpty());
		assert(gpidx >= 0);
		if (map_exist)
		{
			if (Application->MessageBox(Localization::Text(_T("mDeleteWarning")).c_str(),
                                  Localization::Text(_T("mDelete")).c_str(),
                                  MB_YESNO | MB_ICONQUESTION) == ID_YES)
			{
				for (j = 0; j < m_pLevelList->Count; j++)
				{
					TObjManager *mo = (TObjManager*)m_pLevelList->Items[j];
					mo->DeleteGroup(gpname);
					mo->ClearCashe();
				}
				delete (GPerson*)mainCommonRes->GParentObj->Items[gpidx];
				mainCommonRes->GParentObj->Delete(gpidx);
				FObjOrder->RebuildList();
				DragObjListClear();
				FormFindObj->Init();
				return true;
			}
		}
		else
		{
			delete (GPerson*)mainCommonRes->GParentObj->Items[gpidx];
			mainCommonRes->GParentObj->Delete(gpidx);
			DragObjListClear();
			FormFindObj->Init();
			return true;
		}
		return false;
	}

	tGpers = new GPerson();
	mainCommonRes->AssignObjMainPattern(tGpers);
	InitPrpWindows(mainCommonRes);

	if (mode == mEdit)
	{
		GPerson *gp = mainCommonRes->GetParentObjByName(gpname);
		assert(gp);
		gp->CopyTo(tGpers);
		oldname = gp->GetName();
		oldType = gp->DecorType;
	}
	else
	{
		gpname = _T("\0");
	}

	ReleaseRenderGL();

	Application->CreateForm(__classid(TFObjParent), &FObjParent);
	FObjParent->SetWndParams(mpEdPrObjWndParams);
	FObjParent->SetGameObj(tGpers); //асоциировали с текущим объектом
	do
	{
		res = true;
		if (FObjParent->ShowModal() == mrOk)
		{
			cancel = false;
			if (FObjParent->Edit_name->Text.IsEmpty())
			{
				FObjParent->TabSet->TabIndex = 0;
				FObjParent->ActiveControl = FObjParent->Edit_name;
				Application->MessageBox(Localization::Text(_T("mObjNameError")).c_str(),
                                Localization::Text(_T("mError")).c_str(),
                                MB_OK | MB_ICONERROR);
				res = false;
			}
			else
			{
				for (pos = 0; pos < mainCommonRes->GParentObj->Count; pos++)
				{
					GPerson *cgp = static_cast<GPerson*>(mainCommonRes->GParentObj->Items[pos]);
					if (cgp->GetName().Compare(FObjParent->Edit_name->Text) == 0 && gpidx != pos)
					{
						FObjParent->TabSet->TabIndex = 0;
						FObjParent->ActiveControl = FObjParent->Edit_name;
						Application->MessageBox(Localization::Text(_T("mObjNameExist")).c_str(),
                                    Localization::Text(_T("mError")).c_str(),
                                    MB_OK | MB_ICONERROR);
						res = false;
						break;
					}
				}
			}
			if (res)//если все нормально
			{
				FObjParent->ApplyChangesToGameObj(); //получили измененный объект
				if (mode == mAdd)
				{
					mainCommonRes->GParentObj->Add(tGpers);
					gpname = tGpers->GetName();
					FormFindObj->Init();
				}
				else
				{
					GPerson *gp = mainCommonRes->GetParentObjByName(gpname);
					tGpers->CopyTo(gp);
					gpname = gp->GetName();
				}
			}
		}
		else
		{
			delete tGpers;
			tGpers = NULL;
			res = true;
			cancel = true;
		}
	}
	while (!res);
	FObjParent->ReleaseResources();
	FObjParent->GetWndParams(mpEdPrObjWndParams);
	FObjParent->Free();
	FObjParent = NULL;

	InitRenderGL();

	if (mode == mEdit && tGpers != NULL)
	{
		if (map_exist)
			for (j = 0; j < m_pLevelList->Count; j++)
			{
				TMapGPerson *mo;
				TObjManager *mg = (TObjManager*)m_pLevelList->Items[j];

				//если имя изменилось
				if (oldname.Compare(tGpers->GetName()) != 0)
				{
					TGScript *gst;
					//изменить имя во всех скриптах
					__int32 slen = mg->GetScriptsCount();
					for (i = 0; i < slen; i++)
					{
						/*
						gst = mg->GetScriptByIndex(i);
						for (k = 0; k < gst->CldGroupNameList->Count; k++)
						{
							if (gst->CldGroupNameList->Strings[k].Compare(oldname) == 0)
								gst->CldGroupNameList->Strings[k] = tGpers->GetName();
						}
                        */
					}
				}

				//пересвязываем объекты, поставленные на карту
				for (k = 0; k < mg->GetObjTypeCount(TObjManager::CHARACTER_OBJECT); k++)
				{
					mo = (TMapGPerson*)mg->GetObjByIndex(TObjManager::CHARACTER_OBJECT, k);

					if(mo->GetObjType() == objMAPOBJPOOL)
					{
						TMapObjPool *mop = (TMapObjPool*)mo;
						for(__int32 mop_pi = 0; mop_pi < mop->GetItemsCount(); mop_pi++)
						{
							ObjPoolPropertyItem mopit;
							mop->GetItemByIndex(mop_pi, mopit);
							if(mopit.mName.Compare(oldname) == 0)
							{
								if(oldType == decorNONE && oldType != tGpers->DecorType)
								{
									mop->DeleteItem(oldname);
									mop_pi--;
								}
								else
								{
									mopit.mName = tGpers->GetName();
									mop->ChangeItem(oldname, mopit);
                                }
							}
                        }
					}

					if (mo->getGParentName().Compare(oldname) == 0)
					{
						mo->assignNewParent(gpname);
						if(mo->SetDecorType(tGpers->DecorType))
						{
							k--;
						}
					}
				}
				for (k = 0; k < mg->GetObjTypeCount(TObjManager::BACK_DECORATION); k++)
				{
					mo = (TMapGPerson*)mg->GetObjByIndex(TObjManager::BACK_DECORATION, k);
					if (mo->getGParentName().Compare(oldname) == 0)
					{
						mo->assignNewParent(gpname);
						if(mo->SetDecorType(tGpers->DecorType))
						{
							k--;
						}
					}
				}
				for (k = 0; k < mg->GetObjTypeCount(TObjManager::FORE_DECORATION); k++)
				{
					mo = (TMapGPerson*)mg->GetObjByIndex(TObjManager::FORE_DECORATION, k);
					if (mo->getGParentName().Compare(oldname) == 0)
					{
						mo->assignNewParent(gpname);
						if(mo->SetDecorType(tGpers->DecorType))
						{
              k--;
            }
					}
				}
				mg->ClearCashe();
			}
	}

	if (tGpers != NULL && mode != mAdd)
	{
		delete tGpers;
	}

	return !cancel;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::gameObjAddEditDel(__int32 mode)
{
	TListItem *li;
	UnicodeString gpname;
	__int32 idx;

	if (!proj_exist)
	{
		return;
	}

	li = ListView_u->Selected;

	if (mode == mDel || mode == mEdit)
	{
		if (li == NULL)
		{
			return;
		}
		gpname = static_cast<GPerson*>(mainCommonRes->GParentObj->Items[(int)li->Data])->GetName();
		if (gpname.Compare(UnicodeString(TRIGGER_NAME)) == 0 ||
			gpname.Compare(UnicodeString(DIRECTOR_NAME)) == 0 ||
			gpname.Compare(UnicodeString(TEXTBOX_NAME)) == 0 ||
			gpname.Compare(UnicodeString(SOUNDSCHEME_NAME)) == 0 ||
			gpname.Compare(UnicodeString(OBJPOOL_NAME)) == 0)
		{
			return;
		}
	}
	else
	{
		if (mainCommonRes->GParentObj->Count >= MAX_OBJ_PARENTS)
		{
			Application->MessageBox(Localization::Text(_T("mMaximumTypeObjectsExceeded")).c_str(),
                              Localization::Text(_T("mError")).c_str(),
                              MB_OK | MB_ICONERROR);
			return;
		}
		gpname = _T("\0");
	}

	if (ParentObjAddEditDel(gpname, mode))
	{
		LoadTexturesToVRAMGL();
		if (mode == mAdd)
		{
			images_u->AllocBy = mainCommonRes->GParentObj->Count;
			idx = mainCommonRes->GetParentObjID(gpname);
			GPerson *gp = mainCommonRes->GetParentObjByName(gpname);
			assert(gp);
			gp->mGroupId = TabSetObj->TabIndex;
			editGameObjListViewBmp(NULL, gp, gpname + _T(" (") + IntToStr(idx) + _T(")"));
		}
		else if (mode == mEdit)
		{
			ReorderPanelComponents(); //расставляем в нужной последовательности
			CreateZoneList();
			GPerson *gp = mainCommonRes->GetParentObjByName(gpname);
			editGameObjListViewBmp(li, gp, gpname + _T(" (") + IntToStr((int)li->Data) + _T(")"));
		}
		else if (mode == mDel)
		{
			rebuildGameObjListView();
		}

		rebuildGameObjects(); //пересобираем объекты на карте
		checkMenuGameObj();

		DrawMap(false);

		is_save = true;

		FObjOrder->RebuildList();
	}
}
//---------------------------------------------------------------------------

//добавить триггер и директор
void __fastcall TForm_m::AddSpecialObj()
{
	__int32 i, gr_idx;
	GPerson *Gpers;
	TFrameObj b;
	Graphics::TBitmap *tgbmp;
	BMPImage *tbmp;
	TAnimation *a;
	hash_t hashFrameArr[1];
	__int32 animArr[1];
	UnicodeString name;
	hash_t h;

	for (i = 0; i < mainCommonRes->GParentObj->Count; i++)
	{
		Gpers = (GPerson*)mainCommonRes->GParentObj->Items[i];
		name = Gpers->GetName().UpperCase();
		if (name.Compare(UnicodeString(DIRECTOR_NAME)) == 0)
		{
			delete Gpers;
			mainCommonRes->GParentObj->Delete(i);
			i--;
		}
		else if (name.Compare(UnicodeString(TRIGGER_NAME)) == 0)
		{
			delete Gpers;
			mainCommonRes->GParentObj->Delete(i);
			i--;
		}
		else if (name.Compare(UnicodeString(TEXTBOX_NAME)) == 0)
		{
			delete Gpers;
			mainCommonRes->GParentObj->Delete(i);
			i--;
		}
		else if (name.Compare(UnicodeString(SOUNDSCHEME_NAME)) == 0)
		{
			delete Gpers;
			mainCommonRes->GParentObj->Delete(i);
			i--;
		}
		else if (name.Compare(UnicodeString(OBJPOOL_NAME)) == 0)
		{
			delete Gpers;
			mainCommonRes->GParentObj->Delete(i);
			i--;
		}
	}

	Gpers = new GPerson();
	Gpers->SetName(UnicodeString(OBJPOOL_NAME));
	tgbmp = new Graphics::TBitmap();
	ImageListSpIcons->GetBitmap(IMAGELIST_OBJPOOL_IDX, tgbmp);
	b.w = (unsigned short)ImageListSpIcons->Width;
	b.h = (unsigned short)ImageListSpIcons->Height;
	b.x = 0;
	b.y = 0;
	tbmp = new BMPImage(tgbmp);
	tbmp->ResetAlpha();
	tbmp->FillColorWithTransparent(clTRANSPARENT);
	name = _T("?objpool");
	gr_idx = mainCommonRes->AddGraphicResource(tbmp, name);
	delete tbmp;
	b.setSourceRes(ImageListSpIcons->Width, ImageListSpIcons->Height, gr_idx, name);
	h = mainCommonRes->AddBitmapFrame(b);
	hashFrameArr[0] = h;
	animArr[0] = 0;
	a = mainCommonRes->CreateSimplyAnimation(UnicodeString(ANIM_OBJPOOL_NAME), hashFrameArr, 1, animArr, 1, alDOWN);
	assert(a);
	Gpers->AssignAnimation(a);
	mainCommonRes->AssignObjMainPattern(Gpers);
	mainCommonRes->GParentObj->Insert(0, Gpers);
	Gpers->mGroupId = PARENT_TYPE_SYSTEM;

	Gpers = new GPerson();
	Gpers->SetName(UnicodeString(SOUNDSCHEME_NAME));
	tgbmp = new Graphics::TBitmap();
	ImageListSpIcons->GetBitmap(IMAGELIST_SOUNDSCHEME_IDX, tgbmp);
	b.w = (unsigned short)ImageListSpIcons->Width;
	b.h = (unsigned short)ImageListSpIcons->Height;
	b.x = 0;
	b.y = 0;
	tbmp = new BMPImage(tgbmp);
	tbmp->ResetAlpha();
	tbmp->FillColorWithTransparent(clTRANSPARENT);
	name = _T("?soundscheme");
	gr_idx = mainCommonRes->AddGraphicResource(tbmp, name);
	delete tbmp;
	b.setSourceRes(ImageListSpIcons->Width, ImageListSpIcons->Height, gr_idx, name);
	h = mainCommonRes->AddBitmapFrame(b);
	hashFrameArr[0] = h;
	animArr[0] = 0;
	a = mainCommonRes->CreateSimplyAnimation(UnicodeString(ANIM_SOUNDSCHEME_NAME), hashFrameArr, 1, animArr, 1, alDOWN);
	assert(a);
	Gpers->AssignAnimation(a);
	mainCommonRes->AssignObjMainPattern(Gpers);
	mainCommonRes->GParentObj->Insert(0, Gpers);
	Gpers->mGroupId = PARENT_TYPE_SYSTEM;

	Gpers = new GPerson();
	Gpers->SetName(UnicodeString(TEXTBOX_NAME));
	tgbmp = new Graphics::TBitmap();
	ImageListSpIcons->GetBitmap(IMAGELIST_TEXTBOX_IDX, tgbmp);
	b.w = (unsigned short)ImageListSpIcons->Width;
	b.h = (unsigned short)ImageListSpIcons->Height;
	b.x = 0;
	b.y = 0;
	tbmp = new BMPImage(tgbmp);
	tbmp->ResetAlpha();
	tbmp->FillColorWithTransparent(clTRANSPARENT);
	name = _T("?textbox");
	gr_idx = mainCommonRes->AddGraphicResource(tbmp, name);
	delete tbmp;
	b.setSourceRes(ImageListSpIcons->Width, ImageListSpIcons->Height, gr_idx, name);
	h = mainCommonRes->AddBitmapFrame(b);
	hashFrameArr[0] = h;
	animArr[0] = 0;
	a = mainCommonRes->CreateSimplyAnimation(UnicodeString(ANIM_TEXTBOX_ICON_NAME), hashFrameArr, 1, animArr, 1, alDOWN);
	assert(a);
	Gpers->AssignAnimation(a);
	mainCommonRes->AssignObjMainPattern(Gpers);
	mainCommonRes->GParentObj->Insert(0, Gpers);
	Gpers->mGroupId = PARENT_TYPE_SYSTEM;

	Gpers = new GPerson();
	Gpers->SetName(UnicodeString(DIRECTOR_NAME));
	tgbmp = new Graphics::TBitmap();
	ImageListDir->GetBitmap(0, tgbmp);
	b.w = (unsigned short)ImageListDir->Width;
	b.h = (unsigned short)ImageListDir->Height;
	b.x = 0;
	b.y = 0;
	tbmp = new BMPImage(tgbmp);
	tbmp->ResetAlpha();
	tbmp->FillColorWithTransparent(clTRANSPARENT);
	name = _T("?dir1");
	gr_idx = mainCommonRes->AddGraphicResource(tbmp, name);
	mDirAssetIndex = gr_idx;
	delete tbmp;
	b.setSourceRes(ImageListDir->Width, ImageListDir->Height, gr_idx, name);
	h = mainCommonRes->AddBitmapFrame(b);
	hashFrameArr[0] = h;
	animArr[0] = 0;
	a = mainCommonRes->CreateSimplyAnimation(UnicodeString(ANIM_DIRECTOR_NAME), hashFrameArr, 1, animArr, 1, alDOWN);
	assert(a);
	Gpers->AssignAnimation(a);
	mainCommonRes->AssignObjMainPattern(Gpers);
	mainCommonRes->GParentObj->Insert(0, Gpers);
	Gpers->mGroupId = PARENT_TYPE_SYSTEM;

	Gpers = new GPerson();
	Gpers->SetName(UnicodeString(TRIGGER_NAME));
	tgbmp = new Graphics::TBitmap();
	ImageListTrig->GetBitmap(0, tgbmp);
	b.w = (short)ImageListTrig->Width;
	b.h = (short)ImageListTrig->Height;
	b.x = 0;
	b.y = 0;
	tbmp = new BMPImage(tgbmp);
	tbmp->ResetAlpha();
	tbmp->FillColorWithTransparent(clTRANSPARENT);
	name = _T("?trig1");
	gr_idx = mainCommonRes->AddGraphicResource(tbmp, name);
	delete tbmp;
	b.setSourceRes(ImageListTrig->Width, ImageListTrig->Height, gr_idx, name);
	h = mainCommonRes->AddBitmapFrame(b);
	hashFrameArr[0] = h;
	animArr[0] = 0;
	a = mainCommonRes->CreateSimplyAnimation(UnicodeString(ANIM_TRIGGER_R_NAME), hashFrameArr, 1, animArr, 1, alDOWN);
	assert(a);
	Gpers->AssignAnimation(a);
	tgbmp = new Graphics::TBitmap();
	ImageListTrig->GetBitmap(1, tgbmp);
	b.x = 0;
	b.y = 0;
	b.w = (short)ImageListTrig->Width;
	b.h = (short)ImageListTrig->Height;
	tbmp = new BMPImage(tgbmp);
	tbmp->ResetAlpha();
	tbmp->FillColorWithTransparent(clTRANSPARENT);
	name = _T("?trig2");
	gr_idx = mainCommonRes->AddGraphicResource(tbmp, name);
	delete tbmp;
	b.setSourceRes(ImageListTrig->Width, ImageListTrig->Height, gr_idx, name);
	h = mainCommonRes->AddBitmapFrame(b);
	hashFrameArr[0] = h;
	animArr[0] = 0;
	a = mainCommonRes->CreateSimplyAnimation(UnicodeString(ANIM_TRIGGER_G_NAME), hashFrameArr, 1, animArr, 1, alDOWN);
	assert(a);
	Gpers->AssignAnimation(a);
	mainCommonRes->AssignObjMainPattern(Gpers);
	mainCommonRes->GParentObj->Insert(0, Gpers);
	Gpers->mGroupId = PARENT_TYPE_SYSTEM;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CreateServiceData()
{
	Graphics::TBitmap *tgbmp;
	BMPImage *tbmp;
	__int32 pos = mainCommonRes->GetGraphicResourceIndex(TEMP_BG_TILE_RES_NAME);
	if(pos < 0)
	{
		tgbmp = new Graphics::TBitmap();
		tbmp = new BMPImage(tgbmp);
		mTileTmpBmp_idx = mainCommonRes->AddGraphicResource(tbmp, TEMP_BG_TILE_RES_NAME);
		delete tbmp;
	}
	pos = mainCommonRes->GetGraphicResourceIndex(TEMP_OBJ_ERROR_RES_NAME);
	if(pos < 0)
	{
		tgbmp = new Graphics::TBitmap();
		TRect r;
		r.Top = 0;
		r.Left = 0;
		r.Right = ERROR_IMAGE_WIDTH;
		r.Bottom = ERROR_IMAGE_HEIGHT;
		tbmp = new BMPImage(tgbmp);
		tbmp->SetSize(r.Width(), r.Height());
		tbmp->Canvas->Brush->Color = clRed;
		tbmp->Canvas->FillRect(r);
		tbmp->Canvas->Brush->Color = clGray;
		tbmp->Canvas->Brush->Style = bsDiagCross;
		tbmp->Canvas->Pen->Mode = pmMaskPenNot;
		tbmp->Canvas->Pen->Style = psDot;
		tbmp->Canvas->Pen->Color = clGray;
		tbmp->Canvas->Rectangle(r);
		tbmp->ResetAlpha();
		mObjGeneralErrorBmp_idx = mainCommonRes->AddGraphicResource(tbmp, TEMP_OBJ_ERROR_RES_NAME);
		delete tbmp;
	}
}
//---------------------------------------------------------------------------

__int32 __fastcall TForm_m::GetGeneralObjErrorResIdx()
{
	return mObjGeneralErrorBmp_idx;
}
//---------------------------------------------------------------------------

bool __fastcall TForm_m::makeObjIcon(const GPerson *Gpers, BMPImage *bmp, TCommonResObjManager *cr)
{
	bool res = false;
	const TAnimationFrame *o = Gpers->GetIconData();
	assert(bmp);
	assert(cr);
	if (o)
	{
		std::list<TAnimationFrameData> lst;
		BMPImage *tbmp = new BMPImage(new Graphics::TBitmap());
		o->CopyFrameDataTo(lst);
		if (!lst.empty())
		{
			std::list<TAnimationFrameData>::iterator it;
			BLENDFUNCTION bf;
			bf.BlendOp = AC_SRC_OVER;
			bf.BlendFlags = 0;
			bf.SourceConstantAlpha = 255;
			bf.AlphaFormat = AC_SRC_ALPHA;
			__int32 xo = -o->GetLeftOffset();
			__int32 yo = -o->GetTopOffset();
			tbmp->SetSize(o->GetWidth(), o->GetHeight());
			tbmp->Clear(TColor(0x00000000));
			res = true;
			for (it = lst.begin(); it != lst.end(); ++it)
			{
                __int32 x, y, h, w;
				bool del_img = false;
				const BMPImage* i2d = NULL;
				if(it->isStreamFrame())
				{
					if(!it->strm_filename.IsEmpty())
					{
						UnicodeString path = Form_m->PathPngRes + it->strm_filename;
						if(FileExists(path))
						{
							Graphics::TBitmap *strmtbmp = new Graphics::TBitmap();
							if(read_png(path.c_str(), strmtbmp))
							{
                                del_img = true;
								i2d = new BMPImage(strmtbmp);
								h = it->rh;
								w = it->rw;
								x = y = 0;
							}
							else
							{
								delete strmtbmp;
							}
						}
                    }
				}
				else
				{
					const TFrameObj *fobj = cr->GetBitmapFrame(it->frameId);
					assert(fobj);
					if(fobj != NULL)
					{
						i2d = cr->GetGraphicResource(fobj->getResIdx());
						h = fobj->h;
						w = fobj->w;
						x = fobj->x;
						y = fobj->y;
                    }
				}
				if(i2d != NULL)
				{
					Graphics::TBitmap *fbmp = const_cast<Graphics::TBitmap*>(i2d->GetBitmapData());
					::AlphaBlend(tbmp->Canvas->Handle, xo + ((__int32)it->posX),
														yo + ((__int32)it->posY), it->rw, it->rh, fbmp->Canvas->Handle, x, y, w, h, bf);
				}
				else
				{
					res = false;
					break;
				}
				if(del_img)
				{
					delete i2d;
                }
			}
		}
		if(res)
		{
			bmp->Assign(tbmp);
		}
		delete tbmp;
	}
	return res;
}
//---------------------------------------------------------------------------

//добавляем картинку в превьюшку листвьюва
void __fastcall TForm_m::editGameObjListViewBmp(TListItem *li, GPerson *Gpers, UnicodeString caption)
{
	if (Gpers->mGroupId != TabSetObj->TabIndex)
	{
		return;
	}

	BMPImage *t_ibmp;
	BMPImage *ibmp;
	double scale;

	if (Gpers->GetName().Compare(UnicodeString(TRIGGER_NAME)) == 0)
	{
		Graphics::TBitmap *t_ibmp = new Graphics::TBitmap();
		ImageListSpIcons->GetBitmap(IMAGELIST_TRIGGER_IDX, t_ibmp);
		ibmp = new BMPImage(t_ibmp);
		ibmp->ResetAlpha();
		ibmp->FillColorWithTransparent(clTRANSPARENT);
		scale = 1.0f;
	}
	else if (Gpers->GetName().Compare(UnicodeString(DIRECTOR_NAME)) == 0)
	{
		Graphics::TBitmap *t_ibmp = new Graphics::TBitmap();
		ImageListSpIcons->GetBitmap(IMAGELIST_DIRECTOR_IDX, t_ibmp);
		ibmp = new BMPImage(t_ibmp);
		ibmp->ResetAlpha();
		ibmp->FillColorWithTransparent(clTRANSPARENT);
		scale = 1.0f;
	}
	else if (Gpers->GetName().Compare(UnicodeString(TEXTBOX_NAME)) == 0)
	{
		Graphics::TBitmap *t_ibmp = new Graphics::TBitmap();
		ImageListSpIcons->GetBitmap(IMAGELIST_TEXTBOX_IDX, t_ibmp);
		ibmp = new BMPImage(t_ibmp);
		ibmp->ResetAlpha();
		ibmp->FillColorWithTransparent(clTRANSPARENT);
		scale = 1.0f;
	}
	else if (Gpers->GetName().Compare(UnicodeString(SOUNDSCHEME_NAME)) == 0)
	{
		Graphics::TBitmap *t_ibmp = new Graphics::TBitmap();
		ImageListSpIcons->GetBitmap(IMAGELIST_SOUNDSCHEME_IDX, t_ibmp);
		ibmp = new BMPImage(t_ibmp);
		ibmp->ResetAlpha();
		ibmp->FillColorWithTransparent(clTRANSPARENT);
		scale = 1.0f;
	}
	else if (Gpers->GetName().Compare(UnicodeString(OBJPOOL_NAME)) == 0)
	{
		Graphics::TBitmap *t_ibmp = new Graphics::TBitmap();
		ImageListSpIcons->GetBitmap(IMAGELIST_OBJPOOL_IDX, t_ibmp);
		ibmp = new BMPImage(t_ibmp);
		ibmp->ResetAlpha();
		ibmp->FillColorWithTransparent(clTRANSPARENT);
		scale = 1.0f;
	}
	else
	{
		ibmp = new BMPImage(NULL);
		makeObjIcon(Gpers, ibmp, mainCommonRes);
		scale = gsGlobalScale;
	}

	if (ibmp->IsEmpty())//если еще не установлена
	{
		delete ibmp;
		ibmp = new BMPImage(new Graphics::TBitmap());
		makeEmptyPreview(ibmp, Gpers->GetName(), lvWHSizeIcon, lvWHSizeIcon);
	}
	else if (ibmp->Width != lvWHSizeIcon || ibmp->Height != lvWHSizeIcon)
	{
		resizeBmpForListView(ibmp, lvWHSizeIcon, lvWHSizeIcon, scale);
	}

	if (li == NULL)
	{
		images_u->Add(const_cast<Graphics::TBitmap*>(ibmp->GetBitmapData()), NULL);
	}
	else
	{
		images_u->Replace(li->Index, const_cast<Graphics::TBitmap*>(ibmp->GetBitmapData()), NULL);
	}

	delete ibmp;

	if (li == NULL)
	{
		li = ListView_u->Items->Add();
		li->Data = reinterpret_cast<void*>(mainCommonRes->GetParentObjID(Gpers->GetName()));
	}
	li->Caption = caption;
	li->ImageIndex = li->Index;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::rebuildGameObjListView()
{
	ListView_u->Items->Clear();
	images_u->Clear();

	if (proj_exist)
	{
		__int32 i;

		images_u->AllocBy = mainCommonRes->GParentObj->Count > 4 ? mainCommonRes->GParentObj->Count : 4;
		images_u->Width = lvWHSizeIcon;
		images_u->Height = lvWHSizeIcon;

		GPerson *Gpers;
		for (i = 0; i < mainCommonRes->GParentObj->Count; i++)
		{
			Gpers = (GPerson*)mainCommonRes->GParentObj->Items[i];
			editGameObjListViewBmp(NULL, Gpers, Gpers->GetName() + _T(" (") + IntToStr(i) + _T(")"));
		}

		if(TabSetObj->TabIndex >= 0)
		{
			i = mGameObjListViewCursorPos[TabSetObj->TabIndex];
			if (i >= 0)
			{
				__int32 lct = ListView_u->Items->Count;
				if (lct <= i)
				{
					i = lct - 1;
					mGameObjListViewCursorPos[TabSetObj->TabIndex] = i;
				}
				if (i >= 0)
				{
					ListView_u->Selected = ListView_u->Items->Item[i];
					ListView_u->Selected->MakeVisible(true);
				}
			}
		}
	}
	else
	{
		clearAllObjects();
		images_u->AllocBy = 4;
		images_u->Width = lvWHSizeIcon;
		images_u->Height = lvWHSizeIcon;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ProcessLogicGameObjectsTick(unsigned __int32 ms)
{
	bool next_frame, tanim, glogic;
	__int32 i, k, len;
	TMapGPerson *mo;
	TObjManager::TObjType type;

	glogic = ToolButtonSStart->Down;
	bool bsd = ToolButtonSStep->Down;
	if(bsd)
	{
		ms = tickDuration;
		tanim = mAnimationEnable;
		mAnimationEnable = true;
		glogic = true;
	}

	mTickDuration += ms;

	for (k = 0; k < 3; k++)
	{
		switch(k)
		{
		case 0:
			type = TObjManager::BACK_DECORATION;
			break; //сначала рисуем декорации BACK
		case 1:
			type = TObjManager::CHARACTER_OBJECT;
			break; //потом объекты игры
		case 2:
			type = TObjManager::FORE_DECORATION;
			break; //потом декорации FORE
		}
		len = m_pObjManager->GetObjTypeCount(type);
		for (i = 0; i < len; i++)
		{
			next_frame = true;
			mo = (TMapGPerson*)m_pObjManager->GetObjByIndex(type, i);
			bool atZone = znCurZone == mo->GetZone() || mo->GetZone() == ZONES_INDEPENDENT_IDX || mo->GetZone() == LEVELS_INDEPENDENT_IDX;
			if (atZone)
			{
				//emulator part
				if(glogic && m_pGameField != NULL && k == 1 && mTickDuration >= tickDuration)
				{
                    //next_frame -- можно ли переключать анимацию
					m_pGameField->Process(mo, &next_frame);
				}

				//animation processing
				if(mAnimationEnable)
				{
					if (next_frame && drawObjMode != drawOnlyFirstFrame)
					{
						mo->ProcessAnimation(ms);
						//emulator part
						if (mo->IsEndAnimation() && m_pGameField != NULL)
						{
							m_pGameField->OnEndAnimation(mo);
						}
					}
                }
			}
		}
	}

	if(bsd)
	{
		ActPause->Execute();
		mAnimationEnable = tanim;
	}

	if(mTickDuration >= tickDuration)
	{
		mTickDuration = 0;
    }
}
//---------------------------------------------------------------------------

//рисовать на карте (используется в DrawMap())
void __fastcall TForm_m::repaintGameObjects()
{
	if (!showObjects)
	{
		return;
	}

	__int32 i, k, ct;
	TMapPerson *mo;
	TCanvas* backBufferBmpCanvas = NULL;
	if (!gUsingOpenGL)
	{
		backBufferBmpCanvas = mpBackBufferBmp->Canvas;
	}

	const TObjManager::TObjType tlist[5] =
	{
		TObjManager::BACK_DECORATION,
		TObjManager::CHARACTER_OBJECT,
		TObjManager::FORE_DECORATION,
		TObjManager::TRIGGER,
		TObjManager::DIRECTOR
	};
	for (k = 0; k < 5; k++)
	{
		if (tlist[k] == TObjManager::DIRECTOR && !showOBJDirectors)
		{
			for (i = 0; i < DragObjList->Count; i++)
			{
				if (GetDragObjFromList(i)->GetObjType() == objMAPDIRECTOR)
				{
					RemoveDragObjFromList(GetDragObjFromList(i));
				}
			}
			if (FObjOrder->Visible)
			{
				FObjOrder->RebuildSelectionList();
			}
			return;
		}

		__int32 len = m_pObjManager->GetObjTypeCount(tlist[k]);
		for (i = 0; i < len; i++)
		{
			mo = (TMapPerson*)m_pObjManager->GetObjByIndex(tlist[k], i);
			if (mo->CustomVisible)
			{
				bool atZone = znCurZone == mo->GetZone() || mo->GetZone() == ZONES_INDEPENDENT_IDX || mo->GetZone() == LEVELS_INDEPENDENT_IDX;

				if (atZone || //если объект активной зоны
					ActStartEmul->Checked || //или когда в режиме эмулятора
					ShowNonActiveZoneObj->Checked)//или когда выбрана опция показывать объекты неактивных зон (призраками)
				{
					if (!is_Visible(mo))
					{
						switch(tlist[k])
						{
							case TObjManager::DIRECTOR:
								if (showOBJDirectors)
								{
									TMapDirector *active = NULL;
									if (DragObjListCount() == 1)
									{
										active = (TMapDirector*)GetDragObjFromList(0);
										if (active->GetObjType() != objMAPDIRECTOR)
											active = NULL;
									}
									m_pObjManager->DrawDirectorLines((TMapDirector*)mo, active, backBufferBmpCanvas, HorzScrollBar_Position, VertScrollBar_Position);
								}
								break;
							default:
								break;
						}
						continue;
					}

					m_pObjManager->DrawMapObject(static_cast<TMapGPerson*>(mo), backBufferBmpCanvas);

					switch(tlist[k])
					{
						case TObjManager::CHARACTER_OBJECT:
							if (mo->GetObjType() != objMAPTEXTBOX &&
								mo->GetObjType() != objMAPSOUNDSCHEME &&
								mo->GetObjType() != objMAPOBJPOOL &&
								showRectGameObj &&
								 (znCurZone == mo->GetZone() || mo->GetZone() == ZONES_INDEPENDENT_IDX || mo->GetZone() == LEVELS_INDEPENDENT_IDX) && mo->Visible)
							{
								TMapGPerson *p = (TMapGPerson*)mo;
								TPoint pt[5];

								if (!gUsingOpenGL)
								{
									backBufferBmpCanvas->Brush->Style = bsClear;
									backBufferBmpCanvas->Pen->Style = psDot;
									backBufferBmpCanvas->Pen->Mode = pmCopy;
								}
								if (p->GetCollideRect().Width() > 0 && p->GetCollideRect().Height() > 0)//обводить только объекты
								{
									if (gUsingOpenGL)
									{
										TRenderGL::SetColor(clBlack);
										TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
										TRenderGL::RenderRect((double)p->GetCollideRect().Left * gsGlobalScale - (double)HorzScrollBar_Position,
													(double)p->GetCollideRect().Top * gsGlobalScale - (double)VertScrollBar_Position,
													(double)p->GetCollideRect().Width() * gsGlobalScale,
													(double)p->GetCollideRect().Height() * gsGlobalScale);
										TRenderGL::SetColor(clRed);
										TRenderGL::SetLineStyle(TRenderGL::RGLLSDash);
										TRenderGL::RenderRect((double)p->GetCollideRect().Left * gsGlobalScale - (double)HorzScrollBar_Position,
													(double)p->GetCollideRect().Top * gsGlobalScale - (double)VertScrollBar_Position,
													(double)p->GetCollideRect().Width() * gsGlobalScale,
													(double)p->GetCollideRect().Height() * gsGlobalScale);
									}
									else
									{
										pt[4].x = pt[3].x = pt[0].x = (__int32)((double)p->GetCollideRect().Left * gsGlobalScale - (double)HorzScrollBar_Position);
										pt[4].y = pt[1].y = pt[0].y = (__int32)((double)p->GetCollideRect().Top * gsGlobalScale - (double)VertScrollBar_Position);
										pt[2].x = pt[1].x = (__int32)((double)pt[0].x + (double)p->GetCollideRect().Width() * gsGlobalScale);
										pt[3].y = pt[2].y = (__int32)((double)pt[0].y + (double)p->GetCollideRect().Height() * gsGlobalScale);
										backBufferBmpCanvas->Pen->Color = clRed;
										backBufferBmpCanvas->Polyline(pt, 4);
									}
								}
								if (p->GetViewRect().Width() > 0 && p->GetViewRect().Height() > 0)//обводить только объекты
								{
									if (gUsingOpenGL)
									{
										TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
										TRenderGL::SetColor(clBlack);
										TRenderGL::RenderRect((__int32)((double)p->GetViewRect().Left * gsGlobalScale - (double)HorzScrollBar_Position),
													(__int32)((double)p->GetViewRect().Top * gsGlobalScale - (double)VertScrollBar_Position),
													(__int32)((double)p->GetViewRect().Width() * gsGlobalScale),
													(__int32)((double)p->GetViewRect().Height() * gsGlobalScale));
										TRenderGL::SetLineStyle(TRenderGL::RGLLSDash);
										TRenderGL::SetColor(clSkyBlue);
										TRenderGL::RenderRect((__int32)((double)p->GetViewRect().Left * gsGlobalScale - (double)HorzScrollBar_Position),
													(__int32)((double)p->GetViewRect().Top * gsGlobalScale - (double)VertScrollBar_Position),
													(__int32)((double)p->GetViewRect().Width() * gsGlobalScale),
													(__int32)((double)p->GetViewRect().Height() * gsGlobalScale));
									}
									else
									{
										pt[4].x = pt[3].x = pt[0].x = (__int32)((double)p->GetViewRect().Left * gsGlobalScale - (double)HorzScrollBar_Position);
										pt[4].y = pt[1].y = pt[0].y = (__int32)((double)p->GetViewRect().Top * gsGlobalScale - (double)VertScrollBar_Position);
										pt[2].x = pt[1].x = (__int32)((double)pt[0].x + (double)p->GetViewRect().Width() * gsGlobalScale);
										pt[3].y = pt[2].y = (__int32)((double)pt[0].y + (double)p->GetViewRect().Height() * gsGlobalScale);
										backBufferBmpCanvas->Pen->Color = clSkyBlue;
										backBufferBmpCanvas->Polyline(pt, 4);
									}
								}
								if (gUsingOpenGL)
								{
									TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
								}
							}
							break;

						case TObjManager::DIRECTOR:
							if (showOBJDirectors)
							{
								TMapDirector *active = NULL;
								if (DragObjListCount() == 1)
								{
									active = (TMapDirector*)GetDragObjFromList(0);
									if (active->GetObjType() != objMAPDIRECTOR)
										active = NULL;
								}
								m_pObjManager->DrawDirectorLines((TMapDirector*)mo, active, backBufferBmpCanvas, HorzScrollBar_Position, VertScrollBar_Position);
							}
							break;

						default:
							break;
					}
				}
			}
		}
	}

	if(dr_mode == drCOLLIDELINES || showCollideMap)
	{
		const TMapCollideLineNode *active = NULL;
		if (DragObjListCount() == 1)
		{
			active = (TMapCollideLineNode*)GetDragObjFromList(0);
			if (active->GetObjType() != objMAPCOLLIDENODE)
				active = NULL;
		}
		const bool dragpoints = dr_mode == drCOLLIDELINES;
		m_pObjManager->DrawCollideShapes(active,
										dragpoints,
										backBufferBmpCanvas,
										HorzScrollBar_Position,
										VertScrollBar_Position);
	}
}
//---------------------------------------------------------------------------

//пересобрать объекты (что-то изменилось)
void __fastcall TForm_m::rebuildGameObjects()
{
	if (!map_exist)
	{
		return;
	}
	TObjManager::TObjType tlist[6] =
	{
		TObjManager::BACK_DECORATION,
		TObjManager::CHARACTER_OBJECT,
		TObjManager::FORE_DECORATION,
		TObjManager::TRIGGER,
		TObjManager::DIRECTOR,
		TObjManager::COLLIDESHAPE
	};
 	__int32 i, k;
	for (k = 0; k < 6; k++)
	{
		__int32 len = m_pObjManager->GetObjTypeCount(tlist[k]);
		for (i = 0; i < len; i++)
		{
			TMapPerson *mo = (TMapPerson*)m_pObjManager->GetObjByIndex(tlist[k], i);
			if (k < 3 &&
					mo->GetObjType() != objMAPTEXTBOX &&
					mo->GetObjType() != objMAPSOUNDSCHEME &&
					mo->GetObjType() != objMAPOBJPOOL)
			{
				mo->ShowRect(showRectGameObj); //не обводить специальные объекты
			}
			else
			{
				mo->ShowRect(false);
			}
			if (!showObjects && mo->Visible && k != 5)
			{
				mo->SetVisible(false);
			}
			mo->OnGlobalScale();
			m_pObjManager->RebuildCasheFor(mo);
		}
	}
}
//---------------------------------------------------------------------------

//ОБЪЕКТЫ НА КАРТЕ

void __fastcall TForm_m::ActMapObjEditExecute(TObject *)
{
	UnicodeString name, oldParentObj;
	TMapPerson *Sender = GetLastSelectedDragObj();
	if (Sender == NULL)
	{
		return;
	}
	m_pObjManager->ClearCasheFor(Sender);
	ShowObjPrpToolWindow((void*)Sender, Sender->GetObjType());
	DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CheckAndSwapPrpWindows(TForm *newForm)
{
	int count = sizeof(gsPrpWindows) / sizeof(TForm*);
	for (int i = 0; i < count; i++)
	{
		if (gsPrpWindows[i]->Visible && gsPrpWindows[i] != newForm)
		{
			gsPrpWindows[i]->Hide();
			newForm->Left = gsPrpWindows[i]->Left;
			newForm->Top = gsPrpWindows[i]->Top;
		}
	}

	if (newForm != nullptr && newForm->Visible == false)
	{
		newForm->Show();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ShowObjPrpToolWindow(void *obj, __int32 objType)
{
	DragObjListAdd((TMapGPerson*)obj);
	TForm *newForm = NULL;

	switch(objType)
	{
		case objMAPOBJ:
				newForm = FGOProperty;
				FGOProperty->EmulatorMode(ActStartEmul->Checked);
				FGOProperty->assignMapGobj(((TMapGPerson*)obj));
				CheckAndSwapPrpWindows(newForm);
			break;

		case objMAPTRIGGER:
				newForm = FSpObjProperty;
				FSpObjProperty->CSEd_x->MaxValue = m_w;
				FSpObjProperty->CSEd_y->MaxValue = m_h;
				FSpObjProperty->AssignSpMapGobj((TMapSpGPerson*)obj);
				CheckAndSwapPrpWindows(newForm);
			break;

		case objMAPDIRECTOR:
				newForm = FDirectorProperty;
				FDirectorProperty->CSEd_x->MaxValue = m_w;
				FDirectorProperty->CSEd_y->MaxValue = m_h;
				FDirectorProperty->AssignDirector((TMapDirector*)obj);
				CheckAndSwapPrpWindows(newForm);
			break;

		case objMAPTEXTBOX:
				newForm = FormTxtObjProperty;
				FormTxtObjProperty->AssignTxtObj((TMapTextBox*)obj);
				CheckAndSwapPrpWindows(newForm);
			break;

		case objMAPSOUNDSCHEME:
				newForm = FSndObjProperty;
				FSndObjProperty->AssignSoundSchemeObj((TMapSoundScheme*)obj);
				CheckAndSwapPrpWindows(newForm);
			break;

		case objMAPOBJPOOL:
				newForm = FObjPoolProperty;
				FObjPoolProperty->AssignObjPoolObj((TMapObjPool*)obj);
				CheckAndSwapPrpWindows(newForm);
			break;

		case objMAPCOLLIDENODE:
				newForm = FCollideNodeProperty;
				FCollideNodeProperty->AssignCollideLineNodeObj((TMapCollideLineNode*)obj);
				CheckAndSwapPrpWindows(newForm);
		break;
		default:
		break;
	}

	if (newForm != NULL && newForm->Visible == false)
	{
		newForm->Show();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::HideObjPrpToolWindow()
{
	int prpwCount = sizeof(gsPrpWindows) / sizeof(TForm*);
	for (int i = 0; i < prpwCount; i++)
	{
		if(gsPrpWindows[i]->Visible)
		{
			gsPrpWindows[i]->Hide();
		}
		gsPrpWindows[i]->Reset();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::InitPrpWindows(TCommonResObjManager *mcr)
{
	int prpwCount = sizeof(gsPrpWindows) / sizeof(TForm*);
	for (int i = 0; i < prpwCount; i++)
	{
		gsPrpWindows[i]->Init(mcr);
	}

	FormFindObj->Init();
}
//---------------------------------------------------------------------------

bool __fastcall TForm_m::IsAnyPrpWindowVisible()
{
	int prpwCount = sizeof(gsPrpWindows) / sizeof(TForm*);
	for (int i = 0; i < prpwCount; i++)
	{
		if(gsPrpWindows[i]->Visible)
		{
            return true;
		}
	}
    return false;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::OnChangeTriggerName(TMapSpGPerson *obj, UnicodeString OldName)
{
	if (OldName.Compare(obj->Name) != 0)
	{
		TGScript *gst;
		TMapSpGPerson *sgp;
		__int32 i, j, len, m;
		for (m = 0; m < m_pLevelList->Count; m++)
		{
			TObjManager *mo = (TObjManager*)m_pLevelList->Items[m];
			len = mo->GetObjTypeCount(TObjManager::TRIGGER);
			for (j = 0; j < len; j++)
			{
				sgp = (TMapSpGPerson*)mo->GetObjByIndex(TObjManager::TRIGGER, j);
				if (sgp->Name.Compare(OldName) == 0)
				{
					sgp->Name = obj->Name;
				}
			}
			len = mo->GetScriptsCount();
			for (i = 0; i < len; i++)
			{
				gst = mo->GetScriptByIndex(i);
				/*
				for (j = 0; j < gst->TrigNameList->Count; j++)
				{
					if (gst->TrigNameList->Strings[j].Compare(OldName) == 0)
					{
						gst->TrigNameList->Strings[j] = obj->Name;
					}
				}
                */
			}
			len = mo->GetObjTypeCount(TObjManager::DIRECTOR);
			for (i = 0; i < len; i++)
			{
				sgp = (TMapSpGPerson*)mo->GetObjByIndex(TObjManager::DIRECTOR, i);
				if (sgp->nodeTrigger.Compare(OldName) == 0)
				{
					sgp->nodeTrigger = obj->Name;
				}
			}
		}
		FObjOrder->RebuildList();
		OnChangeObjParameter();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::OnChangeDirectorName(TMapSpGPerson *obj, UnicodeString OldName)
{
	if (OldName.Compare(obj->Name) != 0)
	{
		__int32 i, j, len;
		TMapDirector *sgp;
		TMapGPerson *mgp;
		TMapSpGPerson *tr;
		len = m_pObjManager->GetObjTypeCount(TObjManager::DIRECTOR);
		for (i = 0; i < len; i++)
		{
			sgp = (TMapDirector*)m_pObjManager->GetObjByIndex(TObjManager::DIRECTOR, i);
			for (j = 0; j < 4; j++)
			{
				if (sgp->nearNodes[j].Compare(OldName) == 0)
				{
					sgp->nearNodes[j] = obj->Name;
				}
			}
		}
		len = m_pObjManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
		for (i = 0; i < len; i++)
		{
			mgp = (TMapGPerson*)m_pObjManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, i);
			if (mgp->NextLinkedDirector.Compare(OldName) == 0)
			{
				mgp->NextLinkedDirector = obj->Name;
			}
		}
		len = m_pObjManager->GetObjTypeCount(TObjManager::TRIGGER);
		for (i = 0; i < len; i++)
		{
			tr = (TMapSpGPerson*)m_pObjManager->GetObjByIndex(TObjManager::TRIGGER, i);
			if (tr->objName.Compare(OldName) == 0)
			{
				tr->objName = obj->Name;
			}
		}
		FObjOrder->RebuildList();
		OnChangeObjParameter();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::OnChangeDirectorNode(TMapSpGPerson *obj)
{
	m_pObjManager->ClearCasheFor(obj);
	DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::OnChangeCplxObjList(TMapGPerson *obj, UnicodeString /* oldParentObj */ )
{
	OnChangeObjZone(obj);
	OnChangeObjParameter();
	FObjOrder->RebuildList();
}
//---------------------------------------------------------------------------

bool __fastcall TForm_m::OnChangeObjName(TMapPerson *obj, UnicodeString oldName)
{
	if (oldName.Compare(obj->Name) != 0)
	{
		__int32 i, j, len;
		m_pObjManager->ClearCasheFor(obj);
		len = m_pObjManager->GetScriptsCount();
		for (i = 0; i < len; i++)
		{
			TGScript *gst = m_pObjManager->GetScriptByIndex(i);
			/*
			for (j = 0; j < gst->CldObjNameList->Count; j++)
			{
				if (gst->CldObjNameList->Strings[j].Compare(oldName) == 0)
					gst->CldObjNameList->Strings[j] = obj->Name;
			}
			if (gst->ChangeCtrlTo.Compare(oldName) == 0)
				gst->ChangeCtrlTo = obj->Name;
            */
		}
		len = m_pObjManager->GetObjTypeCount(TObjManager::TRIGGER);
		for (i = 0; i < len; i++)
		{
			TMapSpGPerson *gst = (TMapSpGPerson*)m_pObjManager->GetObjByIndex(TObjManager::TRIGGER, i);
			if (gst->objName.Compare(oldName) == 0)
				gst->objName = obj->Name;
		}
		len = m_pObjManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
		for (i = 0; i < len; i++)
		{
			TMapGPerson *gst = (TMapGPerson*)m_pObjManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, i);
			if (gst->IsChildExist())
			{
				for (j = 0; j < MAX_ANIMATON_JOIN_NODES; j++)
				{
					UnicodeString chname = gst->GetChild(j);
					if (!chname.IsEmpty())
					{
						if (chname.Compare(oldName) == 0)
						{
							gst->AddChild(j, obj->Name);
						}
					}
				}
			}
			if (gst->ParentObj.Compare(oldName) == 0)
			{
				gst->ParentObj = obj->Name;
			}
		}
		OnChangeObjParameter();
		FGOProperty->refreshCplxList();
		FObjOrder->RebuildList();
        return true;
	}
	return false;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::OnChangeObjZone(TMapGPerson *obj)
{
	//поменяли всем остальным зону, стейт и значение переменных
	if (obj->IsChildExist())
	{
		for (__int32 i = 0; i < MAX_ANIMATON_JOIN_NODES; i++)
		{
			UnicodeString chname = obj->GetChild(i);
			if (!chname.IsEmpty())
			{
				TMapGPerson *mp = (TMapGPerson*)FindObjectPointer(chname);
				if (mp != NULL)
				{
					mp->SetZone(obj->GetZone());
				}
			}
		}
	}
	//if(!ActStartEmul->Checked)
	if (obj->GetZone() != ZONES_INDEPENDENT_IDX && obj->GetZone() != LEVELS_INDEPENDENT_IDX)
	{
		znCurZone = obj->GetZone();
		//if(znCurZone == m_pObjManager->znCount)znCurZone = ZONES_INDEPENDENT_IDX;
		//ListBoxZone->ItemIndex = znCurZone == m_pObjManager->znCount ? m_pObjManager->znCount-1 : znCurZone;
		ListBoxZone->ItemIndex = znCurZone;
		SetZoneVisibility();
		CreateZoneList();
	}
	m_pObjManager->ClearCasheFor(obj);
	OnChangeObjParameter();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::OnChangeTextId(TMapTextBox *obj)
{
	m_pObjManager->ClearCasheFor(obj);
	OnChangeObjParameter();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::OnChangeTextObjSizes(TMapTextBox *obj, __int32 w, __int32 h)
{
	obj->SetTextWidth(w);
	obj->SetTextHeight(h);
	m_pObjManager->ClearCasheFor(obj);
	OnChangeObjParameter();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::OnChangeObjParameter()
{
	if (!ActStartEmul->Checked)
	{
		is_save = true;
	}
	DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::OnChangeObjPosition(TMapGPerson *obj, double x, double y)
{
	double xm, ym;
	obj->getCoordinates(&xm, &ym);
	obj->setCoordinates(x, y, 0, 0);
	if(obj->GetObjType() == objMAPCOLLIDENODE)
	{
		m_pObjManager->MarkCrosslines(static_cast<TMapCollideLineNode*>(obj));
        ApplyDraggedCollideNodes();
	}
	OnChangeObjParameter();
}
//---------------------------------------------------------------------------

//удалить
void __fastcall TForm_m::ActMapObjDelExecute(TObject *)
{
	__int32 i, k, m;
	bool res;
	TMapPerson *Sender, *mo;

	const int tlistCount = 6;

	TObjManager::TObjType tlist[tlistCount] =
	{
		TObjManager::BACK_DECORATION,
		TObjManager::CHARACTER_OBJECT,
		TObjManager::FORE_DECORATION,
		TObjManager::TRIGGER,
		TObjManager::DIRECTOR
	};

	res = false;

	for (m = 0; m < DragObjList->Count; m++)
	{
		Sender = GetDragObjFromList(m);

		if (Sender == NULL)
			continue;

		m_pObjManager->ClearCasheFor(Sender);
		RemoveDragObjFromList(Sender);
		m--;

		if(Sender->GetObjType() == objMAPCOLLIDESHAPE)
		{
			const TMapCollideLineNode *n = FCollideNodeProperty->GetAssignedCollideLineNode();
			if(n != nullptr)
			{
				TCollideChainShape* sh = static_cast<TCollideChainShape*>(Sender);
				if(n == sh->FindNode(n->Name))
				{
					HideObjPrpToolWindow();
                }
			}
			m_pObjManager->DeleteObject(Sender);
			res = true;
		}
		else
		if(Sender->GetObjType() == objMAPCOLLIDENODE)
		{
			if(Sender == FCollideNodeProperty->GetAssignedCollideLineNode())
			{
				HideObjPrpToolWindow();
			}
			m_pObjManager->DeleteObject(Sender);
			res = true;
		}
        else
		for (k = 0; k < tlistCount; k++)
		{
			for (i = 0; i < m_pObjManager->GetObjTypeCount(tlist[k]); i++)
			{
				mo = (TMapPerson*)m_pObjManager->GetObjByIndex(tlist[k], i);
				if (Sender == mo)
				{
					if (mo == FGOProperty->GetAssignedObj() ||
						mo == FSpObjProperty->GetAssignedObj() ||
						mo == FDirectorProperty->GetAssignedObj() ||
						mo == FormTxtObjProperty->GetAssignedObj() ||
						mo == FSndObjProperty->GetAssignedObj() ||
						mo == FObjPoolProperty->GetAssignedObj())
					{
						HideObjPrpToolWindow();
					}

					if (mo->GetObjType() == objMAPOBJ)
					{
						TMapGPerson *g = (TMapGPerson*)mo;
						if (g->IsChildExist())
						{
							for (__int32 j = 0; j < MAX_ANIMATON_JOIN_NODES; j++)
							{
								UnicodeString chname = g->GetChild(j);
								if (!chname.IsEmpty())
								{
									TMapGPerson *ch = (TMapGPerson*)FindObjectPointer(chname);
									for (__int32 n = 0; n < m_pObjManager->GetObjTypeCount(tlist[k]); n++)
									{
										if (ch == m_pObjManager->GetObjByIndex(tlist[k], n))
										{
											ch->ParentObj = _T("");
											break;
										}
									}
								}
							}
						}
						if (!g->ParentObj.IsEmpty())
						{
							TMapGPerson *ch = (TMapGPerson*)FindObjectPointer(g->ParentObj);
							if (ch != NULL)
							{
								for (__int32 j = 0; j < MAX_ANIMATON_JOIN_NODES; j++)
								{
									UnicodeString chname = ch->GetChild(j);
									if (!chname.IsEmpty() && g->Name.Compare(chname) == 0)
									{
										ch->AddChild(j, UnicodeString());
									}
								}
							}
						}
					}
					__int32 zi = mo->GetZone();
					m_pObjManager->DeleteObject(mo);
					res = true;
					//если глобальный
					if (zi == LEVELS_INDEPENDENT_IDX)
					{
						if (k == 3)
						{
							AddGlobalTriggerToAllLevels(false);
						}
					}

					k = tlistCount;
					break; //exit
				}
			}
		}
	}

	if (res)
	{
		is_save = true;
		m_pObjManager->ClearCashe();
		CreateZoneList();
		FObjOrder->RebuildList();
		DrawMap(false);

		FGOProperty->refreshCplxList();
		FGOProperty->refreshNodes();

		FDirectorProperty->refreshNodes();
		FDirectorProperty->refreshTriggers();
		FDirectorProperty->refreshEvents();

		FSpObjProperty->refreshNodes();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActMapObjCloneExecute(TObject *)
{
	double x, y;
	__int32 i;
	TMapPerson *mp, *nmp;
	UnicodeString prefix;

	if (dr_mode != drOBJECTS)
	{
		return;
	}

	mp = GetLastSelectedDragObj();

	if (mp == NULL || DragObjListCount() != 1)
	{
		return;
	}
	if (m_pObjManager->GetTotalMapObjectsCount() >= MAX_CHARACTER_ON_MAP)
	{
		return;
	}

	mp->getCoordinates(&x, &y);

	switch(mp->GetObjType())
	{
		case objMAPOBJ:
			{
				TMapGPerson *pt = m_pObjManager->CreateMapGPerson();
				((TMapGPerson*)mp)->CopyTo(pt);
				pt->ParentObj = _T("");
				if (pt->IsChildExist())
				{
					for (__int32 jn = 0; jn < MAX_ANIMATON_JOIN_NODES; jn++)
					{
						pt->AddChild(jn, UnicodeString());
					}
				}
				pt->CasheDataIndex = -1;
				nmp = pt;
			}
		break;
		case objMAPTEXTBOX:
			{
				TMapTextBox *pt = m_pObjManager->CreateMapTextBox();
				((TMapTextBox*)mp)->CopyTo(pt);
				pt->ParentObj = _T("");
				if (pt->IsChildExist())
				{
					for (__int32 jn = 0; jn < MAX_ANIMATON_JOIN_NODES; jn++)
					{
						pt->AddChild(jn, UnicodeString());
					}
				}
				pt->CasheDataIndex = -1;
				nmp = pt;
			}
		break;
		case objMAPSOUNDSCHEME:
			{
				TMapSoundScheme *pt = m_pObjManager->CreateMapSoundScheme();
				((TMapSoundScheme*)mp)->CopyTo(pt);
				pt->ParentObj = _T("");
				pt->CasheDataIndex = -1;
				nmp = pt;
			}
		break;
		case objMAPOBJPOOL:
			{
				TMapObjPool *opl = m_pObjManager->CreateMapObjPool();
				((TMapObjPool*)mp)->CopyTo(opl);
				opl->ParentObj = _T("");
				opl->CasheDataIndex = -1;
				nmp = opl;
			}
		break;
		case objMAPDIRECTOR:
			{
				TMapDirector *pt = m_pObjManager->CreateMapDirector();
				((TMapDirector*)mp)->CopyTo(pt);
				pt->CasheDataIndex = -1;
				nmp = pt;
			}
			break;
		case objMAPTRIGGER:
			{
				TMapTrigger *pt = m_pObjManager->CreateMapTrigger();
				((TMapTrigger*)mp)->CopyTo(pt);
				pt->CasheDataIndex = -1;
				nmp = pt;
			}
		break;
		case objMAPCOLLIDENODE:
			return;
		default:
			break;
	}
	nmp->SetVisible(true);
	nmp->setCoordinates(x + 1.0f, y + 1.0f, 0, 0);
	nmp->Name = m_pObjManager->AutoNameObj(nmp);
	nmp->changeHint();

	if (mp->GetObjType() == objMAPOBJ)
	{
		TMapGPerson *c, *n;
		TMapGPerson *p = (TMapGPerson*)nmp;
		TMapGPerson *cp = ((TMapGPerson*)mp);
		if (cp->IsChildExist())
		{
			for (i = 0; i < MAX_ANIMATON_JOIN_NODES; i++)
			{
				UnicodeString chname = cp->GetChild(i);
				if (!chname.IsEmpty())
				{
					c = (TMapGPerson*)FindObjectPointer(chname);
					if (m_pObjManager->GetTotalMapObjectsCount() >= MAX_CHARACTER_ON_MAP)
					{
						continue;
					}
					assert(c);
					switch(c->GetObjType())
					{
						case objMAPOBJ:
							n = m_pObjManager->CreateMapGPerson();
							c->CopyTo(n);
						break;
						case objMAPTEXTBOX:
							n = m_pObjManager->CreateMapTextBox();
							((TMapTextBox*)c)->CopyTo((TMapTextBox*)n);
						break;
						case objMAPSOUNDSCHEME:
							n = m_pObjManager->CreateMapSoundScheme();
							((TMapSoundScheme*)c)->CopyTo((TMapSoundScheme*)n);
						break;
						case objMAPOBJPOOL:
							n = m_pObjManager->CreateMapObjPool();
							((TMapObjPool*)c)->CopyTo((TMapObjPool*)n);
						break;
						default:
							assert(0);
					}
					c->getCoordinates(&x, &y);
					n->ParentObj = p->Name;
					n->CasheDataIndex = -1;
					n->SetVisible(true);
					n->setCoordinates(x + 1.0f, y + 1.0f, 0, 0);
					n->Name = m_pObjManager->AutoNameObj(n);
					p->AddChild(i, n->Name);
					n->changeHint();
				}
			}
		}
	}
	DragObjListClear();
	DragObjListAdd(nmp);

	ReorderPanelComponents();
	CreateZoneList();
	FObjOrder->RebuildList();
	DrawMap(false);
}
//---------------------------------------------------------------------------

//установить все значения переменных всех объектов по умолчанию
void __fastcall TForm_m::ActSetVatDefExecute(TObject * /* Sender */ )
{
	if (Application->MessageBox(Localization::Text(_T("mAskAllMapObjectPropertiesToDefault")).c_str(),
                              ActSetVatDef->Caption.c_str(),
                              MB_OKCANCEL | MB_ICONQUESTION) == ID_OK)
	{
		m_pObjManager->SetToParentPropertiesAll();
		is_save = true;
	}
}
//---------------------------------------------------------------------------

//свойства подчиненных-- по умолчанию
void __fastcall TForm_m::ActSetVarDefGroupExecute(TObject *)
{
	GPerson *Gpers;
	TMapGPerson *mp;
	if (mainCommonRes->GParentObj == NULL || ListView_u->Selected == NULL)
	{
		return;
	}

	if (Application->MessageBox(Localization::Text(_T("mAskAllThisTypeMapObjectPropertiesToDefault")).c_str(),
                              ActSetVarDefGroup->Caption.c_str(),
                              MB_OKCANCEL | MB_ICONQUESTION) == ID_OK)
	{
		bool find = false;
		Gpers = (GPerson*)mainCommonRes->GParentObj->Items[(int)ListView_u->Selected->Data];
		__int32 len = m_pObjManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
		for (__int32 i = 0; i < len; i++)
		{
			mp = (TMapGPerson*)m_pObjManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, i);
			if (mp->getGParentName().Compare(Gpers->GetName()) == 0)
			{
				m_pObjManager->SetToParentProperties(mp);
				find = true;
			}
		}
		if (find)
		{
			is_save = true;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActSetVarparentToDefExecute(TObject * /* Sender */ )
{
	if (mainCommonRes->GParentObj == NULL || ListView_u->Selected == NULL)
	{
		return;
	}

	if (Application->MessageBox(Localization::Text(_T("mAskThisTypeMapObjectPropertiesToDefault")).c_str(),
                              ActSetVarparentToDef->Caption.c_str(),
                              MB_OKCANCEL | MB_ICONQUESTION) == ID_OK)
	{
		GPerson *Gpers = (GPerson*)mainCommonRes->GParentObj->Items[(int)ListView_u->Selected->Data];
		Gpers->GetBaseProperties().setVarValuesToDef();
		is_save = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CreatePopupMoveTo()
{
	TMenuItem *mi;
	UnicodeString name;
	__int32 i, len;
	len = TabSetObj->Tabs->Count;
	FreePopupMoveTo();
	NPMUMoveto->AutoHotkeys = maManual;
	if (TabSetObj->TabIndex == 0)
	{
		return;
	}
	for (i = 1; i < len; i++)
	{
		if (i == TabSetObj->TabIndex)
		{
			continue;
		}
		mi = new TMenuItem(NPMUMoveto);
		name = TabSetObj->Tabs->Strings[i];
		mi->AutoHotkeys = maManual;
		mi->Caption = name;
		mi->Hint = name;
		mi->OnClick = NPopupMoveToClick;
		mi->Tag = NativeInt(i);
		NPMUMoveto->Add(mi);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::NPopupMoveToClick(TObject *Sender)
{
	TListItem *li;
	if (!proj_exist)
	{
		return;
	}
	li = ListView_u->Selected;
	if (li != NULL)
	{
		GPerson *gp = static_cast<GPerson*>(mainCommonRes->GParentObj->Items[(int)li->Data]);
		gp->mGroupId = static_cast<TMenuItem*>(Sender)->Tag;
		rebuildGameObjListView();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::FreePopupMoveTo()
{
	__int32 i;
	for (i = 0; i < NPMUMoveto->Count; i++)
	{
		delete NPMUMoveto->Items[i];
	}
	NPMUMoveto->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActEditorOptExecute(TObject * /* Sender */ )
{
	EditorOptions(false);
}
//---------------------------------------------------------------------------

double __fastcall TForm_m::GetScaleByCBIndex(__int32 cb_idx)
{
	switch(cb_idx)
	{
		case 0: //0.125
			return 0.125f;
		case 1: //0.25
			return 0.25f;
		case 2: //0.5
			return 0.5f;
		case 3: //1.0
			return 1.0f;
		case 4: //2
			return 2.0f;
		case 5: //4
			return 4.0f;
	}
	return 1.0f;
}
//---------------------------------------------------------------------------

__int32 __fastcall TForm_m::GetCBIndexByScale(double scale)
{
	if(scale == 0.125f)
	{
		return 0;
	}
	else
	if(scale == 0.25f)
	{
		return 1;
	}
	else
	if(scale == 0.5f)
	{
		return 2;
	}
	else
	if(scale == 1.0f)
	{
		return 3;
	}
	else
	if(scale == 2.0f)
	{
		return 4;
	}
	else
	if(scale == 4.0f)
	{
		return 5;
	}
	else
	return 3;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::EditorOptions(bool readOnly)
{
	GraphicsTypes t_gr_type;
	TModalResult res;

	DropObjOnDrag();

	t_gr_type = GlobalGraphicsType;

	TStringList *str_lst = new TStringList();
	Application->CreateForm(__classid(TFEditorOptions), &FEditorOptions);
	FEditorOptions->CEd_sz->Value = lvWHSizeIcon;
	FEditorOptions->CEd_anim_duration->Value = tickDuration;
	FEditorOptions->ComboBox->ItemIndex = GetCBIndexByScale(gsGlobalScale);
	FEditorOptions->LoadOptions();
	mpTexts->GetLanguages(str_lst);
	FEditorOptions->AssignLanguages(str_lst);
	if (!readOnly)
	{
		res = FEditorOptions->ShowModal();
	}
	else
	{
		res = mrOk;
	}
	if (res == mrOk)
	{
		UnicodeString newLang = *((UnicodeString*)FEditorOptions->ComboBoxLang->Items->Objects[FEditorOptions->ComboBoxLang->ItemIndex]);

		if(readOnly == false && Lang.Compare(newLang) != 0)
		{
			__int32 res = Application->MessageBox(Localization::Text(_T("mChangeLanguageWarning")).c_str(),
									Localization::Text(_T("mLocalizations")).c_str(),
									MB_OK | MB_ICONINFORMATION);
			Lang = newLang;
		}

		Localization::Localize(this);
		double scale = GetScaleByCBIndex(FEditorOptions->ComboBox->ItemIndex);
		if (gsGlobalScale != (double)scale)
		{
			SetScale(scale);
		}
		if (lvWHSizeIcon != FEditorOptions->CEd_sz->Value)
		{
			lvWHSizeIcon = FEditorOptions->CEd_sz->Value;
			rebuildGameObjListView();
			is_save = true;
		}
		tickDuration = FEditorOptions->CEd_anim_duration->Value;
		mFx32size.setType(static_cast<TFx32size::TFx32Type>(FEditorOptions->ComboBoxFx32->ItemIndex));
		mWcharsize = (FEditorOptions->ComboBoxWchar->ItemIndex == 0) ? 4 : 2;
		if(tickDuration < MINIMAL_TIME_TO_UPDATE_ANIMATIONS)
		{
			tickDuration = MINIMAL_TIME_TO_UPDATE_ANIMATIONS;
		}
		FEditorOptions->SaveOptions();
		FEditorOptions->GetLanguages(str_lst);
		if (str_lst->Count > 0)
		{
			if(str_lst->IndexOf(mCurrentLanguage) < 0)
			{
				mCurrentLanguage = str_lst->Strings[0];
			}
		}
		else
		{
			mCurrentLanguage = TEXT_NAME_DEFAULT;
		}
		mpTexts->SetLanguages(str_lst);
		CreateLanguageMenu();

		gridColor = FEditorOptions->GetGridColor();

		if (gUsingOpenGL)
		{
			TRenderGL::SetBackdropImage(NULL, clNone);
		}
		else
		{
			mpBGBmp->Canvas->Brush->Bitmap = NULL;
		}
		delete mpBackPatternBmp;
		mpBackPatternBmp = CreatePatternImage(FEditorOptions->GetPatternColor());
		if (gUsingOpenGL)
		{
			if(TRenderGL::IsInited())
			{
				TRenderGL::SetBackdropImage(mpBackPatternBmp, clNone);
            }
		}
		else
		{
			DrawMap(true);
		}
	}
	FEditorOptions->Free();
	FEditorOptions = NULL;
	delete str_lst;

	if(m_pObjManager)
	{
    	m_pObjManager->ClearCashe();
	}

	if (t_gr_type != GlobalGraphicsType)
	{
		//перезагружаем всю графику
		if (map_exist)
		{
            UnicodeString errorlist;
			std::list<TResourceItem> *list = createImgListForSave(lstForReload, m_pObjManager);
			AssignGraphicToObjects(PathPngRes, list, &errorlist);
			list->clear();
			delete list;
			RefreshFonts();
			rebuildBackObjListView();
			rebuildGameObjListView(); //пересобрать окна выбора объектов карты
			rebuildGameObjects(); //пересобираем все объекты игры на карте
			m_pObjManager->ClearCashe();
			InitPrpWindows(mainCommonRes);
			m_pObjManager->RefreshAllTextsObjects();
			LoadTexturesToVRAMGL();
			DrawMap(true); //рисуем
			ShowLoadErrorMessages(errorlist);
		}
	}
}
//---------------------------------------------------------------------------

BMPImage* __fastcall TForm_m::CreatePatternImage(TColor color)
{
	const __int32 size = 8;
	const __int32 gradient = 40;
	Graphics::TBitmap* bmp = new Graphics::TBitmap();
	__int32 cl[3], sign;
	cl[0] = (unsigned char)color;
	cl[1] = (unsigned char)(color >> 8);
	cl[2] = (unsigned char)(color >> 16);
	sign = 1;
	for(__int32 i = 0; i < 3; i++)
	{
		if(cl[i] > 128)
		{
			sign = -1;
		}
	}
	for(__int32 i = 0; i < 3; i++)
	{
		if(cl[i] + gradient * sign >= 0 && cl[i] + gradient * sign <= 255)
		{
			cl[i] += gradient * sign;
		}
		else
		{
			if(sign > 0)
			{
				cl[i] = 255;
			}
			else
			{
				cl[i] = 0;
            }
		}
	}
	bmp->SetSize(size, size);
	TRect r;
	r.Left = 0;
	r.Top = 0;
	r.Right = size;
	r.Bottom = size;
	bmp->Canvas->Brush->Color = color;
	bmp->Canvas->FillRect(r);
	color = (TColor)(cl[0] | (cl[1] << 8) | (cl[2] << 16));
	r.Right = size / 2;
	r.Bottom = size / 2;
	bmp->Canvas->Brush->Color = color;
	bmp->Canvas->FillRect(r);
	r.Top = size / 2;
	r.Left = size / 2;
	r.Right = size;
	r.Bottom = size;
	bmp->Canvas->Brush->Color = color;
	bmp->Canvas->FillRect(r);
	BMPImage *img = new BMPImage(bmp);
	img->ResetAlpha();
	return img;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::N45Click(TObject * /* Sender */ )
{
	Application->CreateForm(__classid(TForm_about), &Form_about);
    Form_about->Caption = Localization::Text(_T("mAbout"));
	Form_about->ShowModal();
	Form_about->Free();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActVarListExecute(TObject * /* Sender */ )
{
	__int32 i, j;
	UnicodeString str;

	if (!map_exist)
		return;

	Application->CreateForm(__classid(TFSaveVarList), &FSaveVarList);
	FSaveVarList->Caption = ActVarList->Caption;

	FSaveVarList->Memo->Lines->Add(_T("#ifndef _GE_CONSTANTS_H_"));
	FSaveVarList->Memo->Lines->Add(_T("#define _GE_CONSTANTS_H_"));
	FSaveVarList->Memo->Lines->Add(UnicodeString());
	FSaveVarList->Memo->Lines->Add(_T("enum GEConstants"));
	FSaveVarList->Memo->Lines->Add(_T("{"));

	{
		FSaveVarList->Memo->Lines->Add(_T("// Map tiles count"));
		__int32 lct = m_pLevelList->Count;
		for(__int32 l = 0; l < lct; l++)
		{
			TObjManager *m = (TObjManager*)m_pLevelList->Items[l];
			FSaveVarList->Memo->Lines->Add(_T(" M") + IntToStr(l) + _T("_TILES_COUNT") + _T(" = ") + IntToStr(m->mp_w * m->mp_h) + _T(","));
		}
		FSaveVarList->Memo->Lines->Add(UnicodeString());
	}

	if (mainCommonRes->bgotypes->Count > 0)
	{
		FSaveVarList->Memo->Lines->Add(_T("// Map tiles types:"));
		for (i = 0; i < mainCommonRes->bgotypes->Count; i++)
		{
			TBGobjProperties *bp = (TBGobjProperties*)mainCommonRes->bgotypes->Items[i];
			str = _T(" ") + bp->name + _T(" = ") + IntToStr(bp->type) + _T(",");
			FSaveVarList->Memo->Lines->Add(str);
		}
		FSaveVarList->Memo->Lines->Add(UnicodeString());
	}
	if (m_pObjManager->GetScriptsCount() > 0)
	{
		bool first = true;
		for (i = 0; i < m_pObjManager->GetScriptsCount(); i++)
		{
			TGScript *spt = m_pObjManager->GetScriptByIndex(i);
			/*
			if (spt->Zone == LEVELS_INDEPENDENT_IDX)
			{
				if (first)
				{
					FSaveVarList->Memo->Lines->Add(_T("// Scripts:"));
					first = false;
				}
				str = _T(" ") + spt->Name + _T(" = ") + IntToStr(i) + _T(",");
				FSaveVarList->Memo->Lines->Add(str);
			}
            */
		}
		__int32 lct = m_pLevelList->Count;
		for(__int32 l = 0; l < lct; l++)
		{
			TObjManager *m = (TObjManager*)m_pLevelList->Items[l];
			__int32 oct = m->GetScriptsCount();
			__int32 idx = FSaveVarList->Memo->Lines->Count;
			bool exist = false;
			for(__int32 i = 0; i < oct; i++)
			{
				TGScript *spt = m->GetScriptByIndex(i);
				/*
				if (spt->Zone != LEVELS_INDEPENDENT_IDX)
				{
					if (first)
					{
						FSaveVarList->Memo->Lines->Add(_T("// Scripts:"));
						idx = FSaveVarList->Memo->Lines->Count;
						first = false;
					}
					FSaveVarList->Memo->Lines->Add(_T(" M") + IntToStr(l) + _T("_") + spt->Name + _T(" = ") + IntToStr(i) + _T(","));
					exist = true;
				}
                */
			}
			if(exist)
			{
				FSaveVarList->Memo->Lines->Insert(idx, _T("// Map") + IntToStr(l) + _T(" scripts list"));
        	}
		}
		FSaveVarList->Memo->Lines->Add(UnicodeString());
	}
	{
		TStringList *strlst = new TStringList();
		mpTexts->GetLanguages(strlst);
		if (strlst->Count > 0)
		{
			FSaveVarList->Memo->Lines->Add(_T("// Languages:"));
			j = strlst->Count;
			str = _T("");
			for (i = 0; i < j; i++)
			{
				if(!strlst->Strings[i].IsEmpty())
				{
					if(!str.IsEmpty())
					{
						//str += _T(",");
						FSaveVarList->Memo->Lines->Add(str);
					}
					str = _T(" LANGUAGE_") + strlst->Strings[i] + _T(" = ") + IntToStr(i) + _T(",");
				}
			}
			if(!str.IsEmpty())
			{
				//str += _T(";");
				FSaveVarList->Memo->Lines->Add(str);
			}
			FSaveVarList->Memo->Lines->Add(UnicodeString());
		}
		strlst->Clear();
		mpTexts->GetNames(strlst);
		if (strlst->Count > 0)
		{
			FSaveVarList->Memo->Lines->Add(_T("// Text strings:"));
			j = strlst->Count;
			for (i = 0; i < j; i++)
			{
				str = _T(" ") + strlst->Strings[i] + _T(" = ") + IntToStr(i) + ((i == j - 1) ? _T(",") : _T(","));
				FSaveVarList->Memo->Lines->Add(str);
			}
			FSaveVarList->Memo->Lines->Add(UnicodeString());
		}
		delete strlst;
	}
	if (mainCommonRes->GParentObj->Count > 0)
	{
		FSaveVarList->Memo->Lines->Add(_T("// Game object types:"));
		for (i = 0; i < mainCommonRes->GParentObj->Count; i++)
		{
			GPerson *gp = (GPerson*)mainCommonRes->GParentObj->Items[i];
			if(gp->GetName().Compare(UnicodeString(SOUNDSCHEME_NAME)) == 0 ||
				gp->GetName().Compare(UnicodeString(OBJPOOL_NAME)) == 0)
			{
				continue;
            }
			str = _T(" ") + gp->GetName() + _T(" = ") + IntToStr(i) + (i == mainCommonRes->GParentObj->Count - 1 ? _T(",") : _T(","));
			FSaveVarList->Memo->Lines->Add(str);
		}
	}
	if (!mainCommonRes->IsMainGameObjPatternEmpty())
	{
		FSaveVarList->Memo->Lines->Add(UnicodeString());
		FSaveVarList->Memo->Lines->Add(_T("// Properties of game object:"));
		const std::list<PropertyItem> *vars = mainCommonRes->GetVariablesMainGameObjPattern();
		std::list<PropertyItem>::const_iterator iv;
		i = 0;
		for (iv = vars->begin(); iv != vars->end(); ++iv)
		{
			str = _T(" ") + iv->name + _T(" = ") + IntToStr(i) + _T(",");
			FSaveVarList->Memo->Lines->Add(str);
			i++;
		}
		FSaveVarList->Memo->Lines->Add(UnicodeString());
	}
	{
		__int32 lct = m_pLevelList->Count;
		for(__int32 l = 0; l < lct; l++)
		{
			TObjManager *m = (TObjManager*)m_pLevelList->Items[l];
			{
				__int32 font_res_count = 0;
				std::list<TResourceItem>::iterator it;
				std::list<TResourceItem> *res_lst = createImgListForSave(lstMap, m);
				for (it = res_lst->begin(); it != res_lst->end(); ++it)
				{
					if(it->mType == ResourceTypes::RESOURCE_TYPE_FONTDATA)
					{
						font_res_count++;
					}
				}
				if(font_res_count > 0) // если есть фонты
				{
					__int32 font_no = 0;
					FSaveVarList->Memo->Lines->Add(_T("// Map") + IntToStr(l) + _T(" fonts list"));
					for (it = res_lst->begin(); it != res_lst->end(); ++it)
					{
						if(it->mType == ResourceTypes::RESOURCE_TYPE_FONTDATA)
						{
							UnicodeString sname = it->mName;
							const __int32 dpos = sname.LastDelimiter(_T(".\0"));
							if (dpos > 0)
							{
								sname = sname.SubString(1, dpos - 1);
							}
							FSaveVarList->Memo->Lines->Add(_T(" M") + IntToStr(l) + _T("_FONT_") +
															sname.UpperCase() +
															_T(" = ") + IntToStr(font_no) + _T(","));
							font_no++;
						}
					}
					FSaveVarList->Memo->Lines->Add(UnicodeString());
				}
			}
			FSaveVarList->Memo->Lines->Add(_T("// Map") + IntToStr(l) + _T(" objects list"));
			GenerateNameListForAutoNamePoolObj(m);
			__int32 oct = m->GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
			__int32 obj_no = 0;
			for(__int32 iobj = 0; iobj < oct; iobj++)
			{
				const TMapGPerson *o = (TMapGPerson *)m->GetObjByIndex(TObjManager::CHARACTER_OBJECT, iobj);
				//sound scheme не является объектом
				if (o->getGParentName().Compare(UnicodeString(SOUNDSCHEME_NAME)) == 0)
				{
					continue;
				}
				else if(o->GetObjType() == objMAPOBJPOOL)
				{
					const TMapObjPool *op = (TMapObjPool*)o;
					__int32 poct = op->GetItemsCount();
					for(__int32 pit = 0; pit < poct; pit++)
					{
						ObjPoolPropertyItem it;
						op->GetItemByIndex(pit, it);
						GPerson *gmp = (GPerson*)mainCommonRes->GetParentObjByName(it.mName);
						assert(gmp);
						FSaveVarList->Memo->Lines->Add(_T(" M") + IntToStr(l) + _T("_") + op->Name +
													_T("_") + IntToStr(pit) + _T("_POOL_FIRST = ") +
													IntToStr(obj_no) + _T(","));
						for(unsigned __int32 pite = 0; pite < it.mCount; pite++)
						{
							UnicodeString aname = AutoNamePoolObj(gmp->GetName());
							FSaveVarList->Memo->Lines->Add(_T(" M") + IntToStr(l) + _T("_") + aname + _T(" = ") + IntToStr(obj_no) + _T(","));
							obj_no++;
						}
						FSaveVarList->Memo->Lines->Add(_T(" M") + IntToStr(l) + _T("_") + op->Name +
													_T("_") + IntToStr(pit) + _T("_POOL_COUNT = ") +
													IntToStr((__int32)it.mCount) + _T(","));
					}
				}
				else
				{
					FSaveVarList->Memo->Lines->Add(_T(" M") + IntToStr(l) + _T("_") + o->Name +
														_T(" = ") + IntToStr(obj_no) + _T(","));
					obj_no++;
				}
			}
			FSaveVarList->Memo->Lines->Add(UnicodeString());
			ClearNameListForAutoNamePoolObj();

			oct = m->GetObjTypeCount(TObjManager::DIRECTOR);
			if(oct > 0)
			{
				FSaveVarList->Memo->Lines->Add(_T("// Map") + IntToStr(l) + _T(" pathnodes list"));
				for(__int32 i = 0; i < oct; i++)
				{
					TMapGPerson *o = (TMapGPerson *)m->GetObjByIndex(TObjManager::DIRECTOR, i);
					FSaveVarList->Memo->Lines->Add(_T(" M") + IntToStr(l) + _T("_") + o->Name + _T(" = ") + IntToStr(i) + _T(","));
				}
				FSaveVarList->Memo->Lines->Add(UnicodeString());
			}

			oct = m->GetObjTypeCount(TObjManager::COLLIDESHAPE);
			if(oct > 0)
			{
				FSaveVarList->Memo->Lines->Add(_T("// Map") + IntToStr(l) + _T(" collide chain shapes list"));
				for(__int32 i = 0; i < oct; i++)
				{
					TMapCollideLineNode *o = (TMapCollideLineNode *)m->GetObjByIndex(TObjManager::COLLIDESHAPE, i);
					FSaveVarList->Memo->Lines->Add(_T(" M") + IntToStr(l) + _T("_") + o->Name + _T(" = ") + IntToStr(i) + _T(","));
				}
				FSaveVarList->Memo->Lines->Add(UnicodeString());
			}

			TMapSoundScheme *ssh = (TMapSoundScheme*)m->FindObjectPointer(UnicodeString(SOUNDSCHEME_NAME));
			if(ssh)
			{
				__int32 sfx_idx, bgm_idx;
				sfx_idx = 0;
				bgm_idx = 0;
				FSaveVarList->Memo->Lines->Add(_T("// Map") + IntToStr(l) + _T(" sound list"));
				__int32 oct = ssh->GetItemsCount();
				for(__int32 i = 0; i < oct; i++)
				{
					SoundSchemePropertyItem sit;
					ssh->GetItemByIndex(i, sit);
					if(sit.mType == SoundSchemePropertyItem::SSRT_SFX)
					{
						FSaveVarList->Memo->Lines->Add(_T(" M") + IntToStr(l) + _T("_") + sit.mName + _T(" = ") + IntToStr(sfx_idx) + _T(", //SFX"));
						sfx_idx++;
					}
					else
					{
						FSaveVarList->Memo->Lines->Add(_T(" M") + IntToStr(l) + _T("_") + sit.mName + _T(" = ") + IntToStr(bgm_idx) + _T(", //BGM"));
                    	bgm_idx++;
					}
				}
				FSaveVarList->Memo->Lines->Add(UnicodeString());
			}
		}
	}
	if(mainCommonRes->aniNameTypes->Count > 0)
	{
		FSaveVarList->Memo->Lines->Add(_T("// Animations: (for drawSingleFrame() function only)"));
		for (i = 0; i < mainCommonRes->aniNameTypes->Count; i++)
		{
			str = _T(" ") + mainCommonRes->aniNameTypes->Strings[i] + _T(" = ") + IntToStr(i) + _T(",");
			FSaveVarList->Memo->Lines->Add(str);
		}
		FSaveVarList->Memo->Lines->Add(UnicodeString());
	}
	if (mainCommonRes->eventNameTypes->Count > 0)
	{
		FSaveVarList->Memo->Lines->Add(_T("// Events:"));
		for (i = 0; i < mainCommonRes->eventNameTypes->Count; i++)
		{
			str = _T(" ") + mainCommonRes->eventNameTypes->Strings[i] + _T(" = ") + IntToStr(i) + _T(",");
			FSaveVarList->Memo->Lines->Add(str);
		}
		FSaveVarList->Memo->Lines->Add(UnicodeString());
	}
	if (mainCommonRes->states->Count > 0)
	{
		FSaveVarList->Memo->Lines->Add(_T("// States:"));
		for (i = 0; i < mainCommonRes->states->Count; i++)
		{
			TState *st = (TState*)mainCommonRes->states->Items[i];
			str = _T(" ") + st->name + _T(" = ") + IntToStr(i) + _T(",");
			FSaveVarList->Memo->Lines->Add(str);
		}
		FSaveVarList->Memo->Lines->Add(UnicodeString());
	}
	FSaveVarList->Memo->Lines->Add(_T("// Movement :"));
	FSaveVarList->Memo->Lines->Add(_T(" DIR_IDLE = ") + IntToStr(Movement::dirIDLE) + _T(","));
	FSaveVarList->Memo->Lines->Add(_T(" DIR_UP = ") + IntToStr(Movement::dirUP) + _T(","));
	FSaveVarList->Memo->Lines->Add(_T(" DIR_LEFT = ") + IntToStr(Movement::dirLEFT) + _T(","));
	FSaveVarList->Memo->Lines->Add(_T(" DIR_RIGHT = ") + IntToStr(Movement::dirRIGHT) + _T(","));
	FSaveVarList->Memo->Lines->Add(_T(" DIR_DOWN = ") + IntToStr(Movement::dirDOWN) + _T(","));
	FSaveVarList->Memo->Lines->Add(_T(" DIR_COUNT = ") + IntToStr(Movement::dirCOUNT) + _T(" "));
	FSaveVarList->Memo->Lines->Add(UnicodeString());

	FSaveVarList->Memo->Lines->Add(_T("};"));
	FSaveVarList->Memo->Lines->Add(UnicodeString());
	FSaveVarList->Memo->Lines->Add(_T("#endif"));

	FSaveVarList->ShowModal();
	FSaveVarList->Free();
}
//---------------------------------------------------------------------------

/*struct eqwstr
{
  bool operator()(const UnicodeString& s1, const UnicodeString& s2) const
  {
	return s1.Compare(s2) == 0;
  }
};*/

//создания списка графических файлов, принимающий участие в карте
std::list<TResourceItem> *__fastcall TForm_m::createImgListForSave(__int32 typeCreate, const TObjManager *objManager)
{
	__int32 hh, ww, i, j, k, map_len, m, check_bmp;
	UnicodeString sname, find_sname;
	GPerson *gp;
	bool is_used, useBO, useMO, useOptimization, needOptimization, allMaps;
	std::map<const UnicodeString, const void*> namesMap;
	std::map<const UnicodeString, const void*>::iterator nit;

	std::list<TResourceItem> *arr_lst = new std::list<TResourceItem>;
	find_sname = _T("");
	allMaps = false;

	if (lstAllMaps == typeCreate)
	{
		objManager = NULL;
		typeCreate = lstMap;
		allMaps = true;
	}
    else
	if (lstProj == typeCreate)
	{
		objManager = NULL;
		allMaps = true;
	}

	for (m = 0; m < m_pLevelList->Count; m++)
	{
		if (allMaps)
		{
			objManager = (TObjManager*)m_pLevelList->Items[m];
			if (objManager == NULL)
			{
				continue;
			}
		}
		else
		{
			m = m_pLevelList->Count;
		}

		useBO = useMO = true;
		if (typeCreate == lstBackObjOnly)
		{
			useMO = false;
		}
		if (typeCreate == lstMapObjOnly)
		{
			useBO = false;
		}
		needOptimization = typeCreate == lstMap || typeCreate == lstBackObjOnly || typeCreate == lstMapObjOnly;

		if (useBO)
		{
			unsigned short idx;
			//графика, используемая на карте (при сохранении проекта эта оптимизация не нужна)
			if(needOptimization)
			{
				map_len = objManager->mp_w * objManager->mp_h;
				for (i = 0; i < map_len; i++)
				{
					idx = objManager->map[i];
					if (idx == NONE_P)
					{
						continue;
					}
					else
					{
						const GMapBGObj &mo = mainCommonRes->BackObj[idx];
						if(!mo.IsEmpty())
						{
							for (k = 0; k < mo.GetFrameObjCount(); k++)
							{
								const TFrameObj *bo = mo.GetFrameObj(k);
								if (bo == NULL)
								{
									continue;
								}
								bo->getSourceRes(&ww, &hh, &check_bmp, &sname);
								if (!sname.IsEmpty())
								{
									nit = namesMap.find(sname);
									if (nit == namesMap.end())
									{
										if (check_bmp >= 0 || typeCreate == lstForReload)
										{
											TResourceItem it;
											it.mType = RESOURCE_TYPE_IMAGE;
											it.mName = sname;
											it.mWidth = static_cast<unsigned short>(ww);
											it.mHeight = static_cast<unsigned short>(hh);
											it.mSize = CALC_IMAGE_SIZE(ww, hh);
											arr_lst->push_back(it);
											namesMap[sname] = NULL;
										}
									}
								}
							}
						}
					}
				}
			}
			else
			{
				__int32 ct = static_cast<__int32>(mainCommonRes->BackObj.size());
				for (j = 0; j < ct; j++)
				{
					const GMapBGObj &mo = mainCommonRes->BackObj[j];
					if(!mo.IsEmpty())
					{
						for (k = 0; k < mo.GetFrameObjCount(); k++)
						{
							const TFrameObj *bo = mo.GetFrameObj(k);
							if (bo == NULL)
							{
								continue;
							}
							bo->getSourceRes(&ww, &hh, &check_bmp, &sname);
							if (!sname.IsEmpty())
							{
								nit = namesMap.find(sname);
								if (nit == namesMap.end())
								{
									if (check_bmp >= 0 || typeCreate == lstForReload)
									{
										TResourceItem it;
										it.mType = RESOURCE_TYPE_IMAGE;
										it.mName = sname;
										it.mWidth = static_cast<unsigned short>(ww);
										it.mHeight = static_cast<unsigned short>(hh);
										it.mSize = CALC_IMAGE_SIZE(ww, hh);
										arr_lst->push_back(it);
										namesMap[sname] = NULL;
									}
								}
							}
						}
                    }
				}
			}
		}

		if (useMO)
		{
			TMapGPerson *mgp;
			TObjManager::TObjType otype;
			__int32 ct = mainCommonRes->GParentObj->Count;
			for (j = 0; j < ct; j++)
			{
				is_used = false;
				gp = (GPerson*)mainCommonRes->GParentObj->Items[j];
				if (gp->GetName().Compare(UnicodeString(DIRECTOR_NAME)) == 0 ||
					gp->GetName().Compare(UnicodeString(TRIGGER_NAME)) == 0 ||
					gp->GetName().Compare(UnicodeString(TEXTBOX_NAME)) == 0 ||
					gp->GetName().Compare(UnicodeString(SOUNDSCHEME_NAME)) == 0 ||
					gp->GetName().Compare(UnicodeString(OBJPOOL_NAME)) == 0)
				{
					continue; //отсеиваем специальные
				}
				useOptimization = false;
				if ((objManager->ignoreUnoptimizedResoures || gp->ResOptimization) && needOptimization)
				{
					useOptimization = true;
				}
				if (gp->UseGraphics)
				{
					for (k = 0; k < 3; k++)
					{ //используется ли это в карте?
						switch(k)
						{
						case 0:
							otype = TObjManager::BACK_DECORATION;
							break;
						case 1:
							otype = TObjManager::CHARACTER_OBJECT;
							break;
						case 2:
							otype = TObjManager::FORE_DECORATION;
						}
						if (is_used)
						{
							break;
						}
						if(gp->GetName().Compare(UnicodeString(TEXTBOX_NAME)) == 0)
						{
							is_used = true;
							break;
						}
						else
						{
							__int32 olen = objManager->GetObjTypeCount(otype);
							for (i = 0; i < olen; i++)
							{
								mgp = (TMapGPerson*)objManager->GetObjByIndex(otype, i);
								if(mgp->GetObjType() == objMAPOBJPOOL)
								{
									TMapObjPool *mop = (TMapObjPool*)mgp;
									for(__int32 mop_pi = 0; mop_pi < mop->GetItemsCount(); mop_pi++)
									{
										ObjPoolPropertyItem mopit;
										mop->GetItemByIndex(mop_pi, mopit);
										if(mopit.mName.Compare(gp->GetName()) == 0)
										{
											is_used = true;
											break;
										}
									}
								}
								else
								if (mgp->getGParentName() == gp->GetName())
								{
									is_used = true;
									break;
								}
							}
						}
					}
				}
				else
				{
					is_used = !needOptimization;
				}

				if (is_used || (!is_used && !useOptimization))//при сохранении проекта эта оптимизация не нужна
				{
					TList *abflist = new TList();
					gp->GetBitmapFrameList(abflist);
					for (i = 0; i < abflist->Count; i++)
					{
						const TFrameObj *bo = mainCommonRes->GetBitmapFrame((hash_t)abflist->Items[i]);
						if (bo == NULL)
						{
							continue;
						}
						sname = _T("");
						if (gp->UseGraphics || !needOptimization)
						{
							bo->getSourceRes(&ww, &hh, &check_bmp, &sname);
						}
						if (!sname.IsEmpty())
						{
							nit = namesMap.find(sname);
							if (nit == namesMap.end())
							{
								if (check_bmp >= 0 || typeCreate == lstForReload)
								{
									TResourceItem it;
									it.mType = RESOURCE_TYPE_IMAGE;
									it.mName = sname;
									it.mWidth = static_cast<unsigned short>(ww);
									it.mHeight = static_cast<unsigned short>(hh);
									it.mSize = CALC_IMAGE_SIZE(ww, hh);
									arr_lst->push_back(it);
									namesMap[sname] = NULL;
								}
							}
						}
					}
					delete abflist;
				}
			}

			otype = TObjManager::CHARACTER_OBJECT;
			__int32 olen = objManager->GetObjTypeCount(otype);
			for (i = 0; i < olen; i++)
			{
				mgp = (TMapGPerson*)objManager->GetObjByIndex(otype, i);
				if (mgp->getGParentName().Compare(UnicodeString(TEXTBOX_NAME)) == 0)
				{
					TMapTextBox *tgp = static_cast<TMapTextBox *>(mgp);
					UnicodeString fontname = tgp->GetFontName();
					if(!fontname.IsEmpty())
					{
						const TImageFont* font = GetCommonTextManager()->GetFont(fontname);
						if(font != NULL)
						{
							sname = forceCorrectFontFileExtention(font->GetName());
							nit = namesMap.find(sname);
							if (nit == namesMap.end())
							{
								TResourceItem it;
								it.mType = RESOURCE_TYPE_FONTDATA;
								it.mName = sname;
								it.mWidth = 0;
								it.mHeight = 0;
								it.mSize = 0;
								arr_lst->push_back(it);
								namesMap[sname] = NULL;
							}
							sname = font->GetImageResourceName();
							nit = namesMap.find(sname);
							if (nit == namesMap.end())
							{
								TResourceItem it;
								it.mType = RESOURCE_TYPE_IMAGE;
								it.mName = sname;
								it.mWidth = static_cast<unsigned short>(font->GetData()->mpAbcImage->Width);
								it.mHeight = static_cast<unsigned short>(font->GetData()->mpAbcImage->Height);
								it.mSize = CALC_IMAGE_SIZE(it.mWidth, it.mHeight);
								arr_lst->push_back(it);
								namesMap[sname] = NULL;
							}
						}
                    }
				}
			}
		}

		if(lstMap == typeCreate || lstProj == typeCreate)
		{
			TMapSoundScheme* ssh = static_cast<TMapSoundScheme*>(objManager->FindObjectPointer(UnicodeString(SOUNDSCHEME_NAME)));
			if(ssh != NULL)
			{
				__int32 olen = ssh->GetItemsCount();
				for (i = 0; i < olen; i++)
				{
					SoundSchemePropertyItem sitem;
					ssh->GetItemByIndex(i, sitem);
					if(!sitem.mName.IsEmpty() && !sitem.mFileName.IsEmpty())
					{
						sname = sitem.mFileName;
						nit = namesMap.find(sname);
						if (nit == namesMap.end())
						{
							TResourceItem it;
							it.mType = RESOURCE_TYPE_SOUNDDATA;
							it.mName = sname;
							it.mWidth = 0;
							it.mHeight = 0;
							it.mSize = sitem.mFileSize;
							arr_lst->push_back(it);
							namesMap[sname] = NULL;
						}
					}
				}
			}
		}
	}

	//упоряд. по старшинству
	std::list<TResourceItem>::iterator it1;
	std::list<TResourceItem>::iterator it2;
	for (it1 = arr_lst->begin(); it1 != arr_lst->end(); ++it1)
	{
		unsigned __int32 max = it1->mSize;
		it2 = it1;
		it2++;
		for (; it2 != arr_lst->end(); ++it2)
		{
			if (it2->mSize > max)
			{
				max = it2->mSize;
				TResourceItem it = *it1;
				*it1 = *it2;
				*it2 = it;
			}
		}
	}

	return arr_lst;
}
//---------------------------------------------------------------------------

 std::list<TResourceItem> *__fastcall TForm_m::createStreamObjBufferList(TObjManager *objManager)
{
	__int32 j, k, i;
	TObjManager::TObjType otype;
	std::list<TResourceItem> *arr_lst = new std::list<TResourceItem>;
	for(k = 0; k < 3; k++)
	{
		switch(k)
		{
			case 0:
				otype = TObjManager::BACK_DECORATION;
				break;
			case 1:
				otype = TObjManager::CHARACTER_OBJECT;
				break;
			case 2:
				otype = TObjManager::FORE_DECORATION;
		}
		__int32 olen = objManager->GetObjTypeCount(otype);
		for(i = 0; i < olen; i++)
		{
			TMapGPerson *mgp = (TMapGPerson*)objManager->GetObjByIndex(otype, i);
			GPerson *gp = mainCommonRes->GetParentObjByName(mgp->getGParentName());
			if(gp)
			{
				for(j = 0; j < mainCommonRes->aniNameTypes->Count; j++)
				{
					TAnimation *anm = gp->GetAnimation(mainCommonRes->aniNameTypes->Strings[j]);
					if(anm && anm->HasStreamObject())
					{
						TResourceItem it;
						it.mType = RESOURCE_TYPE_IMAGE;
						it.mWidth = static_cast<unsigned short>(anm->GetStreamObjectWidth());
						it.mHeight = static_cast<unsigned short>(anm->GetStreamObjectHeight());
						it.mSize = CALC_IMAGE_SIZE(it.mWidth, it.mHeight);
						it.mName = _T("stream buffer ") + IntToStr(it.mWidth) + _T("x") + IntToStr(it.mHeight);
						arr_lst->push_back(it);
					}
				}
			}
		}
	}
	return arr_lst;
}
//---------------------------------------------------------------------------

//SAVE-RESTORE OBJECTS TO MEMORY

//---------------------------------------------------------------------------
void __fastcall TForm_m::SaveAllToMemory()
{
	m_pTempObjManager = new TObjManager(mainCommonRes);
	m_pObjManager->CopyTo(m_pTempObjManager);
	m_pObjManager->ClearCashe();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::RestoreAllFromMemory(bool redraw)
{
	if (NULL != m_pTempObjManager)
	{
		TObjManager *tm = m_pObjManager;
		for (__int32 i = 0; i < m_pLevelList->Count; i++)
			if (m_pLevelList->Items[i] == m_pObjManager)
			{
				m_pLevelList->Items[i] = m_pTempObjManager;
				break;
			}
		m_pObjManager = m_pTempObjManager;
		m_pTempObjManager = tm;
		m_pTempObjManager->ClearCashe();
		delete m_pTempObjManager;
		ReorderPanelComponents();
	}
	m_pTempObjManager = NULL;
	m_pObjManager->ClearCashe();

	FObjOrder->Caption = ActObjOrder->Caption;
	FObjOrder->SetObjManager(m_pObjManager);
	FObjOrder->RebuildList();

	if (redraw)
	{
		DrawMap(true);
    }
}
//---------------------------------------------------------------------------

//АНИМАЦИЯ
void __fastcall TForm_m::ActAniEnableObjExecute(TObject *)
{
	mAnimationEnable = true;
	ActAnimation->Checked = true;
	drawObjMode = drawAnimation;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActAniDisableObjExecute(TObject *)
{
	mAnimationEnable = false;
	ActAnimation->Checked = false;
	drawObjMode = drawOnlyFirstFrame;
	DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActAnimationExecute(TObject *)
{
	if (ActAnimation->Checked)
	{
		ActAniDisableObj->Execute();
	}
	else
	{
		ActAniEnableObj->Execute();
	}
}
//---------------------------------------------------------------------------

//ЗОНЫ
void __fastcall TForm_m::ListBoxZoneClick(TObject * /* Sender */ )
{
	if (znCurZone != ListBoxZone->ItemIndex)
	{
		DragObjListClear();
		znCurZone = ListBoxZone->ItemIndex;
		if (znCurZone == m_pObjManager->znCount)
			znCurZone = ZONES_INDEPENDENT_IDX;
		SetDrMode(drOBJECTS);
		if (FObjOrder->Visible)
			FObjOrder->RebuildSelectionList();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CreateZoneList()
{
	__int32 i, n, len;
	__int32 **arr, *uzarr;
	TMapGPerson *mp;
	UnicodeString caption;
	TObjManager::TObjType otype;

	ListBoxZone->Items->Clear();
	if (!map_exist)
	{
		return;
	}
	arr = new int*[3];
	arr[0] = new int[m_pObjManager->znCount];
	arr[1] = new int[m_pObjManager->znCount];
	arr[2] = new int[m_pObjManager->znCount];
	uzarr = new int[3];

	for (n = 0; n < 3; n++)
	{
		switch(n)
		{
		case 0:
			otype = TObjManager::BACK_DECORATION;
			break;
		case 1:
			otype = TObjManager::CHARACTER_OBJECT;
			break;
		case 2:
			otype = TObjManager::FORE_DECORATION;
			break;
		}
		memset(arr[n], 0, sizeof(int) * m_pObjManager->znCount);
		uzarr[n] = 0;
		len = m_pObjManager->GetObjTypeCount(otype);
		for (i = 0; i < len; i++)
		{
			mp = (TMapGPerson*)m_pObjManager->GetObjByIndex(otype, i);
			if(mp->GetObjType() == objMAPOBJPOOL)
			{
				TMapObjPool *mop = (TMapObjPool*)mp;
				__int32 poct = mop->GetItemsCount();
				for (__int32 jpi = 0; jpi < poct; jpi++)
				{
					ObjPoolPropertyItem pi;
					mop->GetItemByIndex(jpi, pi);
					if (mp->GetZone() >= 0)
					{
						arr[n][mp->GetZone()] += pi.mCount;
					}
					else
					{
						uzarr[n] += pi.mCount;
					}
				}
			}
			else
			{
				if (mp->GetZone() >= 0)
				{
					arr[n][mp->GetZone()]++;
				}
				else
				{
					uzarr[n]++;
				}
			}
		}
	}
	for (i = 0; i < m_pObjManager->znCount; i++)
	{
		caption = Localization::Text(_T("mZone")) + IntToStr(i) + _T(" obj:") + IntToStr(arr[1][i]) + _T(" db:") + IntToStr(arr[0][i]) + _T(" df:") + IntToStr(arr[2][i]);
		ListBoxZone->Items->Add(caption);
	}
	caption = Localization::Text(_T("mZoneIndependent")) + _T(": obj:") + IntToStr(uzarr[1]) + _T(" db:") + IntToStr(uzarr[0]) + _T(" df:") + IntToStr(uzarr[2]);
	ListBoxZone->Items->Add(caption);
	if (znCurZone < 0 && m_pObjManager->znCount > 0)
	{
		znCurZone = ZONES_INDEPENDENT_IDX;
		ListBoxZone->ItemIndex = m_pObjManager->znCount;
	}
	else if (ListBoxZone->Items->Count > 0)
		ListBoxZone->ItemIndex = znCurZone;
	delete[]arr[0];
	delete[]arr[1];
	delete[]arr[2];
	delete[]arr;
	delete[]uzarr;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActObjOrderExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	if (FObjOrder->Visible)
	{
		FObjOrder->Hide();
		return;
	}
	FObjOrder->Show();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ObjectsChanged()
{
	ReorderPanelComponents();
	DrawMap(false);
	is_save = true;
}
//---------------------------------------------------------------------------

//Пересобрать порядок расположения TMapGPerson на Panel
void __fastcall TForm_m::ReorderPanelComponents()
{
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActLockDecorExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	ActLockDecor->Checked = !ActLockDecor->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActMessagesExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	TextMessagesManager(NULL);
}
//---------------------------------------------------------------------------

const TCommonTextManager *__fastcall TForm_m::GetCommonTextManager()
{
	return mpTexts;
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TForm_m::GetCurrentLanguage()
{
	return mCurrentLanguage;
}
//---------------------------------------------------------------------------

bool __fastcall TForm_m::TextMessagesManager(UnicodeString *id, bool buttons)
{
	bool res = false;
	RefreshFonts();
	Application->CreateForm(__classid(TFormTextManager), &FormTextManager);
	FormTextManager->ShowButtons(buttons);
	FormTextManager->Caption = ActMessages->Caption;
	if (id != NULL)
	{
		FormTextManager->Find(*id);
	}
	if (FormTextManager->ShowModal() == mrOk)
	{
		if (id != NULL)
		{
			*id = FormTextManager->GetCurrentName();
		}
		res = true;
	}
	FormTextManager->Free();
	FormTextManager = NULL;
	m_pObjManager->RefreshAllTextsObjects();
	return res;
}
//---------------------------------------------------------------------------

//SCRIPTS
void __fastcall TForm_m::ActScriptExecute(TObject *)
{
	/*
	DropObjOnDrag();
	Application->CreateForm(__classid(TFScriptList), &FScriptList);
	FScriptList->Caption = ActScript->Caption;
	TStringList *changes = new TStringList();
	FScriptList->SetScripts(m_pObjManager);
	if (FScriptList->ShowModal() == mrOk)
	{
		TGScript *spt;
   		//удаляем глобальные скрипты в соседних уровнях
		for (__int32 m = 0; m < m_pLevelList->Count; m++)
		{
			__int32 j;
			TObjManager *mo = (TObjManager*)m_pLevelList->Items[m];
			if (mo == m_pObjManager)
			{
				continue;
			}
			for (j = 0; j < mo->GetScriptsCount(); j++)
			{
				spt = mo->GetScriptByIndex(j);
				if (spt->Zone == LEVELS_INDEPENDENT_IDX)
				{
					mo->DeleteScript(spt);
					j--;
				}
			}
		}

		FScriptList->GetScripts(m_pObjManager, changes);
		for (__int32 i = 0; i < changes->Count; i++)
		{
			__int32 idx = m_pObjManager->GetScriptID(changes->Values[changes->Names[i]]);
			__int32 zone = 0;
			__int32 mode = 0;
			if (idx >= 0)
			{
				spt = m_pObjManager->GetScriptByIndex(idx);
				zone = spt->Zone;
				mode = spt->Mode;
			}
			m_pObjManager->ChangeScriptNameInAllObjects(changes->Names[i], changes->Values[changes->Names[i]], zone, mode);
		}

		AddGlobalScriptsToAllLevels(false);

		if (FGOProperty->Visible)
		{
			FGOProperty->refreshScripts();
		}
		if (FSpObjProperty->Visible)
		{
			FSpObjProperty->refreshScripts();
		}
		is_save = true;
	}
	FScriptList->Free();
	FScriptList = NULL;
	delete changes;
    */
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::AddGlobalScriptsToAllLevels(bool newMap)
{
	__int32 m;
	TObjManager *objManager;

	if (newMap)
	{
		objManager = (TObjManager*)m_pLevelList->Items[0];
	}
	else
	{
		objManager = m_pObjManager;
	}
	if (objManager == NULL)
	{
		return;
	}

	objManager->SortAllGlobalScriptsByOrder();

	for (m = 0; m < m_pLevelList->Count; m++)
	{
		__int32 j;
		TObjManager *mo = (TObjManager*)m_pLevelList->Items[m];
		if (mo == objManager)
		{
			continue;
		}
		mo->SychronizeGlobalScripts(objManager);
	}
	is_save = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::AddGlobalTriggerToAllLevels(bool newMap)
{
	TObjManager *objManager;
	__int32 m, j;

	if (newMap)
	{
		objManager = (TObjManager*)m_pLevelList->Items[0];
	}
	else
	{
		objManager = m_pObjManager;
	}
	if (objManager == NULL)
	{
		return;
	}

	//устанавливаем все триггеры в начало списка (для совместимости со старыми версиями)
	objManager->SortAllGlobalTriggersByOrder();

	TStringList *errorList = new TStringList();
	for (m = 0; m < m_pLevelList->Count; m++)
	{
		__int32 tmp_y = ImageListTrig->Height + 1;
		__int32 tmp_x = ImageListTrig->Width + 1;
		TObjManager *mo = (TObjManager*)m_pLevelList->Items[m];
		if (mo == objManager)
		{
			continue;
		}
		mo->SychronizeGlobalTriggers(objManager, errorList, tmp_x, tmp_y);
	}
    // в случае неудачи создание где либо глобального триггера, удаляем этот триггер везде
	if(errorList->Count > 0)
	{
		for (m = 0; m < m_pLevelList->Count; m++)
		{
			TObjManager *mo = (TObjManager*)m_pLevelList->Items[m];
			if (mo == objManager)
			{
				continue;
            }
			for (j = 0; j < mo->GetObjTypeCount(TObjManager::TRIGGER); j++)
			{
				TMapSpGPerson *tr = (TMapSpGPerson*)mo->GetObjByIndex(TObjManager::TRIGGER ,j);
				if (errorList->IndexOf(tr->Name) >= 0 && tr->GetZone() == LEVELS_INDEPENDENT_IDX)
				{
					if(mo->DeleteObject(tr))
					{
						j--;
					}
				}
			}
		}
	}
    delete errorList;
	FObjOrder->RebuildList();
	is_save = true;
}
//---------------------------------------------------------------------------

//STATES
void __fastcall TForm_m::ActStatesExecute(TObject *)
{
	DropObjOnDrag();
	bool res;
	res = false;
	TStringList *changes = new TStringList();

	Application->CreateForm(__classid(TFStateList), &FStateList);
	FStateList->Caption = ActStates->Caption;
	FStateList->SetInfo(Localization::Text(_T("mStateDescription")));
	FStateList->SetStates(mainCommonRes->states);
	if (FStateList->ShowModal() == mrOk)
	{
		FStateList->GetStates(mainCommonRes->states, changes);
		res = is_save = true;
	}
	FStateList->Free();
	FStateList = NULL;
	//пересоздать имена состояний у каждого из объектов на карте
	if (res)
	{
		__int32 i, j, k, m;
		TMapGPerson *p;
		//изменяем согласно changelist текущее состояние
		if (map_exist)
			for (m = 0; m < m_pLevelList->Count; m++)
			{
				TObjManager *mo = (TObjManager*)m_pLevelList->Items[m];
				for (j = 0; j < mo->GetObjTypeCount(TObjManager::CHARACTER_OBJECT); j++)
				{
					p = (TMapGPerson*)mo->GetObjByIndex(TObjManager::CHARACTER_OBJECT ,j);
					for (i = 0; i < changes->Count; i++)
					{
						if (p->State_Name.Compare(changes->Names[i]) == 0)
						{
							UnicodeString val = changes->Values[changes->Names[i]];
							//state
							if (val.IsEmpty())
							{
								p->State_Name = _T(""); //никакого состояния
                            }
							else if (val.Compare(changes->Names[i]) != 0)//если изменилось имя
							{
								p->State_Name = val;
							}
							break;
						}
					}
     				//список реагирований
					for (i = 0; i < changes->Count; i++)
						for (k = 0; k < p->getDirStatesCount(); k++)
						{
							UnicodeString s = p->getDirStateItem(k);
							if (p->getDirStateItem(k).Compare(changes->Names[i]) == 0)
							{
								UnicodeString val = changes->Values[changes->Names[i]];
								if (val.IsEmpty())
								{
									p->deleteDirStateItem(k);
									k--;
								}
								else if (val.Compare(changes->Names[i]) != 0)
									p->setDirStateItem(k, val);
							}
						}
				}
				mo->ClearCashe(); //обязательно очистить кеш!!!
			}
  		//список инверсивных соответствий
		for (i = 0; i < changes->Count; i++)
		{
			UnicodeString chname = changes->Names[i];
			for (k = 0; k < mainCommonRes->stateOppositeTable->Count; k++)
			{
				UnicodeString name = mainCommonRes->stateOppositeTable->Names[k];
				UnicodeString key = mainCommonRes->stateOppositeTable->ValueFromIndex[k];
				if (name.Compare(chname) == 0 || key.Compare(chname) == 0)
				{
					UnicodeString val = changes->Values[chname];
					if (val.IsEmpty())
					{
						mainCommonRes->stateOppositeTable->Delete(k);
						k--;
					}
					else
					{
						if (name.Compare(chname) == 0 && val != chname)
							mainCommonRes->stateOppositeTable->Strings[k] = val + _T("=") + key;
						else if (key.Compare(chname) == 0 && val != chname)
							mainCommonRes->stateOppositeTable->Strings[k] = name + _T("=") + val;
					}
				}
			}
		}

		for (i = 0; i < changes->Count; i++)
		{
			UnicodeString chname = changes->Names[i];
			for (k = 0; k < mainCommonRes->GParentObj->Count; k++)
			{
				GPerson *gp = (GPerson*)mainCommonRes->GParentObj->Items[k];
				if (gp->DefaultStateName.Compare(chname) == 0)
				{
					gp->DefaultStateName = changes->Values[chname];
				}
			}
		}

		RelinkGameObjStates();
		FGOProperty->refresStates();
		FSpObjProperty->refresStates();
	}
	delete changes;
	DrawMap(false); //и перерисовать все что получилось
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::RelinkGameObjStates()
{
}
//---------------------------------------------------------------------------

//BACKGROUND TYPES
void __fastcall TForm_m::ActTypesExecute(TObject * /* Sender */ )
{
	bool res;
	res = false;
	TStringList *changes = new TStringList();

	Application->CreateForm(__classid(TFStateList), &FStateList);
	FStateList->Caption = ActTypes->Caption;
	FStateList->SetInfo(Localization::Text(_T("mBgTileTypeDescription")));
	FStateList->SetBackProperties(mainCommonRes->bgotypes);
	if (FStateList->ShowModal() == mrOk)
	{
		FStateList->GetBackProperties(mainCommonRes->bgotypes, changes);
		res = is_save = true;
	}
	FStateList->Free();
	FStateList = NULL;
 //пересоздать свойства фона у каждого из элементов фона
	if (res)
	{
		__int32 i, j;
  //изменяем согласно changelist
		const __int32 ct = static_cast<__int32>(mainCommonRes->BackObj.size());
		for (j = 0; j < ct; j++)
		{
			TFrameObj *bo = mainCommonRes->BackObj[j].GetFrameObj(0);
			for (i = 0; i < changes->Count; i++)
				if (bo->bgPropertyName.Compare(changes->Names[i]) == 0)
				{
					UnicodeString val = changes->Values[changes->Names[i]];
					if (val.IsEmpty())
						bo->bgPropertyName = _T("");
					else if (val.Compare(changes->Names[i]) != 0)
						bo->bgPropertyName = val;
					break;
				}
		}
		RelinkBGobjProperties(true);
	}
	delete changes;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActOppositeStatesExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	Application->CreateForm(__classid(TFStateList), &FStateList);
	FStateList->Caption = ActOppositeStates->Caption;
	FStateList->SetInfo(Localization::Text(_T("mOppositeStatesDescription")));
	FStateList->SetOppositeStates(mainCommonRes->stateOppositeTable);
	if (FStateList->ShowModal() == mrOk)
	{
		FStateList->GetOppositeStates(mainCommonRes->stateOppositeTable, NULL);
	}
	FStateList->Free();
	FStateList = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActAnimTypesExecute(TObject *)
{
	DropObjOnDrag();
	Application->CreateForm(__classid(TFStateList), &FStateList);
	FStateList->Caption = ActAnimTypes->Caption;
	FStateList->SetInfo(Localization::Text(_T("mAnimationTypesDescription")));
	FStateList->SetAnimTypes(mainCommonRes->aniNameTypes);
	if (FStateList->ShowModal() == mrOk)
	{
		FStateList->GetAnimTypes(mainCommonRes->aniNameTypes, NULL);
		//удаляем из состояний уже несуществующие типы анимаций
		for (__int32 i = 0; i < mainCommonRes->states->Count; i++)
		{
			TState *st = (TState*)mainCommonRes->states->Items[i];
			if (mainCommonRes->aniNameTypes->IndexOf(st->animation) < 0)
				st->animation = _T("");
		}
	}
	FStateList->Free();
	FStateList = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActAnimJNCountExecute(TObject *)
{
	DropObjOnDrag();
	Application->CreateForm(__classid(TFJNCount), &FJNCount);
	FJNCount->Caption = ActAnimJNCount->Caption;
	//FJNCount->SetInfo(Localization::Text(_T("mAnimationTypesDescription")));
	FJNCount->CSpinCount->Value = mpEdPrObjWndParams->joinNodesCount;
	if (FJNCount->ShowModal() == mrOk)
	{
		mpEdPrObjWndParams->joinNodesCount = FJNCount->CSpinCount->Value;
	}
	FJNCount->Free();
	FJNCount = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActEventTypesExecute(TObject *)
{
	DropObjOnDrag();
	Application->CreateForm(__classid(TFStateList), &FStateList);
	FStateList->Caption = ActEventTypes->Caption;
	FStateList->SetInfo(Localization::Text(_T("mEventTypesDescription")));
	FStateList->SetEventTypes(mainCommonRes->eventNameTypes);
	if (FStateList->ShowModal() == mrOk)
	{
		TStringList *changes = new TStringList();
		FStateList->GetEventTypes(mainCommonRes->eventNameTypes, changes);

		//rename all events in nodes and object animations
		if (map_exist)
		{
			for (__int32 m = 0; m < m_pLevelList->Count; m++)
			{
				TObjManager *mo = (TObjManager*)m_pLevelList->Items[m];
				for (__int32 j = 0; j < mo->GetObjTypeCount(TObjManager::DIRECTOR); j++)
				{
					TMapDirector *p = (TMapDirector*)mo->GetObjByIndex(TObjManager::DIRECTOR, j);
					for (__int32 i = 0; i < changes->Count; i++)
					{
						UnicodeString chname = changes->Names[i];
						if (p->objTeleport.Compare(chname) == 0)
						{
							UnicodeString val = changes->Values[chname];
							if (val.Compare(chname) != 0)
							{
								p->objTeleport = val;
							}
							break;
						}
					}
				}
			}
		}

		if(proj_exist)
		{
			for (__int32 i = 0; i < changes->Count; i++)
			{
				UnicodeString chname = changes->Names[i];
				UnicodeString val = changes->Values[chname];
				if (val.Compare(chname) != 0)
				{
					mainCommonRes->RenameEventNameInAllAnimations(chname, val);
				}
			}
		}

		delete changes;
		FDirectorProperty->refreshEvents();
	}
	FStateList->Free();
	FStateList = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::RelinkBGobjProperties(bool rebuildListView)
{
	TBGobjProperties *bp;
	__int32 i, j;
	//устанавливаем заново связи
	const __int32 ct = static_cast<__int32>(mainCommonRes->BackObj.size());
	for (j = 0; j < ct; j++)
	{
		GMapBGObj& mgo = mainCommonRes->BackObj[j];
		TFrameObj *bo = mgo.GetFrameObj(0);
		if(bo && bo->IsEmpty() == false && bo->bgPropertyName.IsEmpty() == false)
		{
			bool find = false;
			for (i = 0; i < mainCommonRes->bgotypes->Count; i++)
			{
				bp = (TBGobjProperties*)mainCommonRes->bgotypes->Items[i];
				if (bo->bgPropertyName.Compare(bp->name) == 0)
				{
					find = true;
					break;
				}
			}
			if(find == false)
			{
				bo->bgPropertyName = _T("");
			}
		}
		if (rebuildListView)
		{
			editBackObjListViewBmp(mEdit, j, mainCommonRes->BackObj[j], IntToStr(j) + createCaption(mainCommonRes->BackObj[j]));
		}
	}
}
//---------------------------------------------------------------------------

//Режимы
void __fastcall TForm_m::ActEditModeExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	if (ActEditMode->Checked)
		return;

	mDoNotDraw = true;

	if(mAnimationStateBeforeEmul)
	{
		ActAniEnableObjExecute(NULL);
	}
	else
	{
		ActAniDisableObjExecute(NULL);
    }

	ActEditMode->Checked = true;
	ActStartEmul->Checked = false;

	mNeedRebuildBack = false;

	CancelCollideLine();
	SetDrMode(prev_dr_mode);
	DragObjListClear();
	ClearMarkers();
	ClearCopySelection();
	if (FObjOrder != NULL)
		if (FObjOrder->Visible)
			FObjOrder->RebuildSelectionList();

	if (proj_exist)
	{
		HideObjPrpToolWindow();
	}

	Application->ProcessMessages();

	OnScriptsWndHide();

	ListView_u->Enabled = true;
	Panel_layer->Enabled = true;
	Panel_layer->Visible = true;
	ActStart->Visible = false;
	ActPause->Visible = false;
	ActStep->Visible = false;
	ToolButtonSSeparator->Visible = false;
	ListBoxDebug->Align = alNone;
	ListBoxDebug->Enabled = false;
	ListBoxDebug->Visible = false;
	TabSetObj->Visible = true;
	SplitterObjPal_Zones->Visible = true;
	SplitterObjPal_Zones->Enabled = true;
	SplitterObjPal_Zones->Left = ListBoxDebug->Left - SplitterObjPal_Zones->Width;
	TabLevels->Enabled = true;

	ActObjMode->Enabled = true;
	ActBackMode->Enabled = true;
	ActCollideNodesMode->Enabled = true;

	DbgSeparator->Visible = false;
	DbgStopOnStateChange->Visible = false;
	DbgStateTrace->Visible = false;
	DbgTrigActionsTrace->Visible = false;
	MDebug->Visible = false;

	if (m_pGameField != NULL)
	{
		ActAniDisableObj->Execute();
		delete m_pGameField;
		RestoreAllFromMemory(true);
	}
	m_pGameField = NULL;
	znCurZone = oldZone;
	SetZoneVisibility();
	MainMenu1Change(NULL, NULL, true);
	changeSizes();

	mDoNotDraw = false;
	mNeedRebuildBack = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActStartEmulExecute(TObject * /* Sender */ )
{
	TGameData data;

	DropObjOnDrag();

	if (!map_exist)
	{
		ActEditMode->Checked = true;
		ActStartEmul->Checked = false;
		ToolButtonEdit->Down = true;
		return;
	}

	mAnimationStateBeforeEmul = mAnimationEnable;
	ActAniEnableObjExecute(NULL);

	mDoNotDraw = true;

	if (FBGObjWindow->Visible)
	{
		FBGObjWindow->Hide();
	}
	if (ActStartEmul->Checked)
	{
		return;
	}
	ActStartEmul->Checked = true;
	ActEditMode->Checked = false;

	mNeedRebuildBack = false;

	CancelCollideLine();
	DragObjListClear();
	ClearMarkers();
	ClearCopySelection();
	if (FObjOrder->Visible)
	{
		FObjOrder->RebuildSelectionList();
	}
	HideObjPrpToolWindow();

	Application->ProcessMessages();

	SetDrMode(drOBJECTS);
	ListView_u->Enabled = false;
	Panel_layer->Enabled = false;
	Panel_layer->Visible = false;
	SplitterObjPal_Zones->Visible = false;
	SplitterObjPal_Zones->Enabled = false;
	ListBoxDebug->Clear();
	ListBoxDebug->Align = alClient;
	ListBoxDebug->Enabled = true;
	ListBoxDebug->Visible = true;
	TabLevels->Enabled = false;
	TabSetObj->Visible = false;

	DbgSeparator->Visible = true;
	DbgStopOnStateChange->Visible = true;
	DbgStateTrace->Visible = true;
	DbgTrigActionsTrace->Visible = true;

	ActStart->Visible = true;
	ActPause->Visible = true;
	ActStep->Visible = true;
	ToolButtonSSeparator->Visible = true;

	ActObjMode->Enabled = false;
	ActBackMode->Enabled = false;
	ActCollideNodesMode->Enabled = false;

	SaveAllToMemory();

	m_pGameField = new TPlatformGameField();
	memset(&data, 0, sizeof(data));

	data.StartZone = m_pObjManager->startZone;
	data.p_w = p_w;
	data.p_h = p_h;
	data.m_pObjManager = m_pObjManager;
	data.m_pCommonResObjManager = mainCommonRes;
	m_pGameField->SetData(&data);
	m_pGameField->stopOnStartScript = ActStopOnScript->Checked;
	MDebug->Visible = true;
	MainMenu1Change(NULL, NULL, true);
	changeSizes();

	AddDebugMessage(Localization::Text(_T("mStartEvent")) + _T(":"), NULL);
	AddDebugMessage(_T(" - ") + Localization::Text(_T("mActiveZone")) + _T(": ") + IntToStr(m_pObjManager->startZone), NULL);
	AddDebugMessage(_T(" - ") + Localization::Text(_T("mScript")) + _T(": ") + m_pObjManager->StartScriptName, NULL);
	oldZone = znCurZone;
	znCurZone = m_pObjManager->startZone;
	SetZoneVisibility();
	m_pGameField->StartScript(m_pObjManager->StartScriptName, _T(""), NULL, NULL, false, true);
	ActPause->Execute();

	mDoNotDraw = false;
	mNeedRebuildBack = true;
}
//---------------------------------------------------------------------------

//GAME EMULATION

//---------------------------------------------------------------------------
void __fastcall TForm_m::ActStartExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	ToolButtonSStart->Down = true;
	ToolButtonSPause->Down = false;
	ToolButtonSStep->Down = false;
	ActStep->Enabled = false;
	ActAniEnableObj->Execute();
}

//---------------------------------------------------------------------------
void __fastcall TForm_m::ActPauseExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	ToolButtonSStart->Down = false;
	ToolButtonSPause->Down = true;
	ToolButtonSStep->Down = false;
	ActStep->Enabled = true;
	ActAniDisableObj->Execute();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActStepExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	ToolButtonSStart->Down = false;
	ToolButtonSPause->Down = false;
	ToolButtonSStep->Down = true;
}
//---------------------------------------------------------------------------

//DEBUG

//---------------------------------------------------------------------------
void __fastcall TForm_m::AddDebugMessage(UnicodeString mes, void *obj)
{
	if (ListBoxDebug->Count > 1000)
		ListBoxDebug->Items->Delete(0);
	ListBoxDebug->AddItem(mes, (TObject*)obj);
	ListBoxDebug->ItemIndex = ListBoxDebug->Count - 1;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActDbgChangeZoneExecute(TObject * /* Sender */ )
{
	UnicodeString res;
	UnicodeString val = IntToStr(znCurZone);
	__int32 int_res;
	res = InputBox(ActDbgChangeZone->Caption, Localization::Text(_T("mAskEnterZoneNumber")) + _T(":"), val);
	try
	{
		int_res = StrToInt(res);
	}
	catch(...)
	{
		return;
	}

	if (val == res || res.IsEmpty())
		return;

	ChangeZone(int_res);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ChangeZone(__int32 newZone)
{
	if (newZone == znCurZone || newZone < 0 || newZone >= m_pObjManager->znCount)
		return;
	znCurZone = newZone;
	SetZoneVisibility();
	if (FScriptList != NULL)
		FScriptList->SetZone(znCurZone);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DbgOnChangeState(TMapGPerson *mp)
{
	if (mp != NULL && m_pGameField != NULL)
		if (mp->GetObjType() == objMAPOBJ)
		{
			if (mp->InspectStates)
			{
				UnicodeString mes = mp->Name + _T(" : ") + Localization::Text(_T("mChangeStateTo")) + _T(" ") + mp->State_Name; // Индекс: mp->LastBGDir
				AddDebugMessage(mes, mp);
			}
			if (mp->StopOnStateChange)
				ActPause->Execute();
		}
}

//---------------------------------------------------------------------------
void __fastcall TForm_m::DbgStopTimer()
{
	if (m_pGameField != NULL)
	{
		ActPause->Execute();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DbgTrigActionsTraceClick(TObject * /* Sender */ )
{
	TMapPerson *mp = GetLastSelectedDragObj();
	if (mp == NULL || mp->GetObjType() != objMAPOBJ)
		return;
	((TMapGPerson*)mp)->TraceTriggerActions = !((TMapGPerson*)mp)->TraceTriggerActions;
}

//---------------------------------------------------------------------------
void __fastcall TForm_m::DbgStateTraceClick(TObject * /* Sender */ )
{
	TMapPerson *mp = GetLastSelectedDragObj();
	if (mp == NULL || mp->GetObjType() != objMAPOBJ)
		return;
	((TMapGPerson*)mp)->InspectStates = !((TMapGPerson*)mp)->InspectStates;
}

//---------------------------------------------------------------------------
void __fastcall TForm_m::DbgStopOnStateChangeClick(TObject * /* Sender */ )
{
	TMapPerson *mp = GetLastSelectedDragObj();
	if (mp == NULL || mp->GetObjType() != objMAPOBJ)
		return;
	((TMapGPerson*)mp)->StopOnStateChange = !((TMapGPerson*)mp)->StopOnStateChange;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActClearDbgMessagesExecute(TObject * /* Sender */ )
{
	ListBoxDebug->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActStopOnScriptExecute(TObject * /* Sender */ )
{
	ActStopOnScript->Checked = !ActStopOnScript->Checked;
	if (m_pGameField != NULL)
	{
		m_pGameField->stopOnStartScript = ActStopOnScript->Checked;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActResetTraceScriptsExecute(TObject *)
{
	if (m_pGameField != NULL)
	{
		__int32 i, len;
		TMapGPerson *mp;
		len = m_pObjManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
		for (i = 0; i < len; i++)
		{
			mp = (TMapGPerson*)m_pObjManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, i);
			mp->TraceTriggerActions = false;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CreateDebugPopupScriptList()
{
	TMapGPerson *mp = (TMapGPerson*)GetLastSelectedDragObj();

	if (mp == NULL || mp->GetObjType() != objMAPOBJ)
		return;

	TMenuItem *mi;
	UnicodeString name;
	__int32 i, len;

	len = mp->getScriptsCount();
	FreeDebugPopupScriptList();
	NPopupStartScript->AutoHotkeys = maManual;
	if (len == 0)
	{
		mi = new TMenuItem(NPopupStartScript);
		mi->Caption = Localization::Text(_T("mEmpty"));
		mi->Enabled = false;
		NPopupStartScript->Add(mi);
		return;
	}
	for (i = 0; i < ::Actions::sptCOUNT; i++)
	{
		mi = new TMenuItem(NPopupStartScript);
		name = mp->getScriptName(::Actions::Names[i]);
		if (!name.IsEmpty())
		{
			mi->AutoHotkeys = maManual;
			mi->Caption = name;
			mi->Hint = name;
			mi->OnClick = MDStartScriptClick;
			NPopupStartScript->Add(mi);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::FreeDebugPopupScriptList()
{
	__int32 i;
	for (i = 0; i < NPopupStartScript->Count; i++)
		delete NPopupStartScript->Items[i];
	NPopupStartScript->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::MDStartScriptClick(TObject *Sender)
{
	if (m_pGameField != NULL)
	{
		TMapGPerson *mp = (TMapGPerson*)GetLastSelectedDragObj();
		if (mp == NULL || mp->GetObjType() != objMAPOBJ)
			return;
		m_pGameField->StartScript(static_cast<TMenuItem*>(Sender)->Caption, Localization::Text(_T("mForceStartScript")) + _T(": ") + ((TMenuItem*)Sender)->Caption, mp, NULL, false, true);
		if (!mAnimationEnable)
		{
			DrawMap(true);
    }
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::MDScriptsClick(TObject *)
{
	DropObjOnDrag();
	if (ScriptsWndAlreadyExist)
	{
		return;
    }
	ScriptsWndAlreadyExist = true;
	Application->CreateForm(__classid(TFScriptList), &FScriptList);
	FScriptList->FormStyle = fsStayOnTop;
	FScriptList->SetEmulatorMode();
	FScriptList->Caption = MDScripts->Caption;
	FScriptList->SetScripts(m_pObjManager);
	FScriptList->Show();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::OnScriptsWndHide()
{
	if (!ScriptsWndAlreadyExist)
		return;
	FScriptList->Free();
	FScriptList = NULL;
	ScriptsWndAlreadyExist = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::OnScriptStartClick(UnicodeString name)
{
	if (m_pGameField != NULL)
	{
		m_pGameField->StartScript(name, Localization::Text(_T("mForceStartScript")) + _T(": ") + name, NULL, NULL, false, true);
		if (!mAnimationEnable)
		{
			DrawMap(true);
    }
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ListBoxDebugDblClick(TObject * /* Sender */ )
{
	unsigned __int32 idx = (unsigned int)ListBoxDebug->Items->Objects[ListBoxDebug->ItemIndex];
	if (idx > 1000)
	{
		double x, y;
		TMapGPerson *mp;
		mp = (TMapGPerson*)ListBoxDebug->Items->Objects[ListBoxDebug->ItemIndex];
		if (NULL != mp)
		{
			mp->getCoordinates(&x, &y);
			FindAndShowObject(mp);
			AddMarker(mp, TObjMarker::TObjMarkerType::Attention);
		}
	}
	else if (idx > 0)
	{
  /*
						  ActPause->Execute();
						  if(Form_m->m_pObjManager->MessagesFile.IsEmpty())
						  {
						     Application->MessageBox(Файл с текстовыми диалогами-сообщениями в проекте не определен",Ошибка",MB_OK|MB_ICONERROR);
						     return;
						  }
						  Application->CreateForm(__classid(TFMessages),&FMessages);
						  FMessages->Caption = Form_m->ActMessages->Caption;
						  if(FMessages->Init())
						  {
						    FMessages->LoadFile(Form_m->PathMessages+\\"+Form_m->m_pObjManager->MessagesFile);
						    if(!FMessages->ViewOnly(idx-1))
						    {
							  Application->MessageBox(
						      Текущий файл с текстовыми диалогами-сообщениями не имеет такого идентификатора",
						      Ошибка",MB_OK|MB_ICONERROR);
						    }
						    else FMessages->ShowModal();
						  }
						  FMessages->Free();
						  FMessages = NULL;
		 */
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::FindAndShowObject(TMapPerson *obj)
{
	if (!map_exist)
	{
		return;
    }
	if (!is_Visible(obj))
	{
		double dx, dy;
		double x, y;
		double pos = (double)HorzScrollBar->Position;
		obj->getCoordinates(&dx, &dy);
		x = dx;
		y = dy;
		if (pos + (double)HorzScrollBar->Width < x * gsGlobalScale)
		{
			pos = x * gsGlobalScale + (double)obj->Width - (double)HorzScrollBar->Width;
		}
		else if (pos > x * gsGlobalScale)
		{
			pos = x * gsGlobalScale;
		}
		__int32 ipos = (__int32)pos;
		VertScrollBarScroll(HorzScrollBar, scTrack, ipos);
		HorzScrollBar->Position = (__int32)pos;

		pos = (double)VertScrollBar->Position;
		if (pos + VertScrollBar->Height < y * gsGlobalScale)
		{
			pos = y * gsGlobalScale + (double)obj->Height - (double)VertScrollBar->Height;
		}
		else if (pos > y * gsGlobalScale)
		{
			pos = y * gsGlobalScale;
		}
		ipos = (__int32)pos;
		VertScrollBarScroll(VertScrollBar, scTrack, ipos);
		VertScrollBar->Position = (__int32)pos;
	}
}
//---------------------------------------------------------------------------

void *__fastcall TForm_m::FindAndShowObjectByName(UnicodeString name, bool switchToObj)
{
	TMapGPerson *obj;

	if (!map_exist)
		return NULL;

	if ((obj = (TMapGPerson*)FindObjectPointer(name)) != NULL)
	{
		if (znCurZone != obj->GetZone() && obj->GetZone() >= 0)
		{
			znCurZone = obj->GetZone();
			ListBoxZone->ItemIndex = znCurZone;
			SetZoneVisibility();
		}
		else
		{
			DrawMap(false);
		}
		FindAndShowObject(obj);
		AddMarker(obj, TObjMarker::TObjMarkerType::Attention);
		if (switchToObj)
		{
			DragObjListClear();
			DragObjListAdd(obj);
			if (FObjOrder->Visible)
			{
				FObjOrder->RebuildSelectionList();
			}
		}
		if (IsAnyPrpWindowVisible())
		{
			ShowObjPrpToolWindow(obj, obj->GetObjType());
		}
	}
	return obj;
}
//---------------------------------------------------------------------------

TMapPerson* __fastcall TForm_m::FindObjectPointer(const UnicodeString& name)
{
	if (!map_exist)
		return NULL;
	return m_pObjManager->FindObjectPointer(name);
}

//---------------------------------------------------------------------------

void __fastcall TForm_m::AddMarker(TMapPerson *obj, TObjMarker::TObjMarkerType type)
{
	TObjMarker *m = new TObjMarker(type, MARKER_DURATION, obj);
	markerList->Add(m);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DelMarker(__int32 idx)
{
	delete (TObjMarker*)markerList->Items[idx];
	markerList->Delete(idx);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ClearMarkers()
{
	for (__int32 i = 0; i < markerList->Count; i++)
	{
		delete (TObjMarker*)markerList->Items[i];
    }
	markerList->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::MDResetRunOnceScriptsClick(TObject *)
{
	/*
	__int32 i, len;
	TGScript *st;
	len = m_pObjManager->GetScriptsCount();
	for (i = 0; i < len; i++)
	{
		st = m_pObjManager->GetScriptByIndex(i);
		if (!st->Enabled)
		{
			st->Enabled = true;
        }
	}
    */
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::MDResetAllClick(TObject * /* Sender */ )
{
	mDoNotDraw = true;
	mNeedRebuildBack = false;

	TGameData data;
	ActPause->Execute();
	DropObjOnDrag();

	DragObjListClear();
	HideObjPrpToolWindow();
	if (FObjOrder->Visible)
		FObjOrder->RebuildSelectionList();

	RestoreAllFromMemory(false);
	SaveAllToMemory();

	memset(&data, 0, sizeof(data));

	data.StartZone = m_pObjManager->startZone;
	data.p_w = p_w;
	data.p_h = p_h;
	data.m_pObjManager = m_pObjManager;

	data.m_pCommonResObjManager = mainCommonRes;
	m_pGameField->SetData(&data);

	SetDrMode(drOBJECTS);
	MainMenu1Change(NULL, NULL, true);
	changeSizes();

	AddDebugMessage(Localization::Text(_T("mStartEvent")) + _T(":"), NULL);
	AddDebugMessage(_T(" - ") + Localization::Text(_T("mActiveZone")) + _T(": ") + IntToStr(m_pObjManager->startZone), NULL);
	AddDebugMessage(_T(" - ") + Localization::Text(_T("mScript")) + _T(": ") + m_pObjManager->StartScriptName, NULL);
	znCurZone = m_pObjManager->startZone;
	SetZoneVisibility();
	m_pGameField->StartScript(m_pObjManager->StartScriptName, _T(""), NULL, NULL, false, true);

	mDoNotDraw = false;
	mNeedRebuildBack = true;
}
//---------------------------------------------------------------------------

//CategoryButtons CategoryButtons CategoryButtons
void __fastcall TForm_m::CategoryButtons_sCategoryCollapase(TObject *Sender, const TButtonCategory *Category)
{
	CBCategoryCollapase(Sender, Category);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CategoryButtons_sMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, __int32 X, __int32 Y)
{
	if (dr_mode != ((TCategoryButtons*)Sender)->Tag)
	{
		SetDrMode(((TCategoryButtons*)Sender)->Tag);
	}
	CBMouseDown(Sender, Button, Shift, X, Y);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CategoryButtons_sMouseEnter(TObject * /* Sender */ )
{
	CBMouseEnter = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CategoryButtons_sMouseLeave(TObject * /* Sender */ )
{
	CBMouseEnter = false;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CategoryButtons_sDrawIcon(TObject *Sender, const TButtonItem *Button, TCanvas *Canvas, TRect &Rect, TButtonDrawState State, __int32 &TextOffset)
{
	CBDrawIcon(Sender, Button, Canvas, Rect, State, TextOffset);
}
//---------------------------------------------------------------------------

//Drag n drop game objects

void __fastcall TForm_m::ListView_uStartDrag(TObject * /* Sender */ , TDragObject *& /* DragObject */ )
{
	TMapPerson *DragGameObj;
	TListItem *li;

	if (!map_exist)
	{
		return;
	}

	li = ListView_u->Selected;
	if (li == NULL)
	{
		return;
	}

	if (showObjects)
	{
		GPerson *gp = (GPerson*)mainCommonRes->GParentObj->Items[(int)li->Data];
		__int32 type = objMAPOBJ;
		if (gp->GetName().Compare(UnicodeString(TRIGGER_NAME)) == 0)
		{
			type = objMAPTRIGGER;
		}
		else if (gp->GetName().Compare(UnicodeString(DIRECTOR_NAME)) == 0)
		{
			type = objMAPDIRECTOR;
		}
		else if (gp->GetName().Compare(UnicodeString(TEXTBOX_NAME)) == 0)
		{
			type = objMAPTEXTBOX;
		}
		else if (gp->GetName().Compare(UnicodeString(SOUNDSCHEME_NAME)) == 0)
		{
			type = objMAPSOUNDSCHEME;
		}
		else if (gp->GetName().Compare(UnicodeString(OBJPOOL_NAME)) == 0)
		{
			type = objMAPOBJPOOL;
		}

		TPoint pt;
		pt = PaintBox1->ScreenToClient(Mouse->CursorPos);
		LMBtn_main.snap_x = LMBtn_main.snap_y = false;
		int scrX = (__int32)((double)(pt.X + HorzScrollBar_Position) / gsGlobalScale);
		int scrY = (__int32)((double)(pt.Y + VertScrollBar_Position) / gsGlobalScale);

		DragGameObj = newGObject(m_pObjManager, type, gp->GetName(), scrX, scrY,
				ListBoxZone->ItemIndex == m_pObjManager->znCount ? ZONES_INDEPENDENT_IDX : ListBoxZone->ItemIndex, _T(""),
				ObjCreateModeNormal);
		if (DragGameObj != nullptr)
		{
			DragObjListClear();
			DragGameObj->SetVisible(true);
			ListView_u->DragCursor = crDefault;
			DragObjListAdd(DragGameObj);
		}
		else
		{
			DragObjListClear();
			ListView_u->DragCursor = crNo;
		}
	}
	else
	{
		ListView_u->DragCursor = crNo;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::PaintBox1DragOver(TObject * /* Sender */ , TObject *Source, __int32 scrX, __int32 scrY, TDragState /* State */ , bool &Accept)
{
	if (disabled_controlls)
	{
		return;
	}

	if (Source == ListView_u && GetLastSelectedDragObj() != nullptr)
	{
		Accept = true;
		TShiftState shift = TShiftState();
		if(LMBtn_main.pressed == false)
		{
            LMBtn_main.locked = false;
			UpdateMapCursorCoordinates(scrX, scrY, false, false);
			Shape_crMouseDown(ListView_u, TMouseButton::mbLeft, shift, scrX, scrY);
			LMBtn_main.prev.x = 0;
			LMBtn_main.prev.y = 0;
			GetLastSelectedDragObj()->setCoordinates(HorzScrollBar_Position / gsGlobalScale, VertScrollBar_Position / gsGlobalScale, 0, 0);
			PaintBox1MouseMove(ListView_u, shift, scrX, scrY);
		}
		else if(LMBtn_main.pressed || LMBtn_main.moved)
		{
			PaintBox1MouseMove(ListView_u, shift, scrX, scrY);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ListView_uEndDrag(TObject * /* Sender */ , TObject *Target, __int32 scrX, __int32 scrY)
{
	LMBtn_main.pressed = false;

	if (!map_exist)
	{
		return;
	}

	if (GetLastSelectedDragObj() == NULL)
	{
		return;
	}

	if (Target != NULL)
	{
		is_save = true;
		DrawMap(false);
		FObjOrder->RebuildList();
	}
	else
	{
		ActMapObjDel->Execute();
	}

	//if(LMBtn_main.pressed || LMBtn_main.moved)
	//PaintBox1MouseUp(ListView_u, shift, scrX, scrY);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DropObjOnDrag()
{
	CancelCollideLine();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::FormKeyUp(TObject*/*Sender*/, WORD &Key, TShiftState /*Shift*/)
{
	if(Key == VK_SHIFT)
	{
        LMBtn_main.snap_x = LMBtn_main.snap_y = false;
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::FormKeyDown(TObject * /* Sender */ , WORD &Key, TShiftState Shift)
{
	if (map_exist == false || this->Active == false)
	{
		return;
	}

	switch(dr_mode)
	{
	case drCOLLIDELINES:

		if (Key == VK_ESCAPE)
		{
			Key = 0;
			CancelCollideLine();
			break;
		}

	case drOBJECTS:

		if (Shift.Contains(ssCtrl))
		{
			if(Key == VK_DELETE)
			{
                Key = 0;
				ActMapObjDel->Execute();
            }
			else
			if (Key == L'p' || Key == L'P')
			{
				Key = 0;

				if (dr_mode != drCOLLIDELINES)
				{
					DropObjOnDrag();
				}

				if (ActStartEmul->Checked)
				{
					ActPause->Execute();
				}

				if ((dr_mode == drOBJECTS || dr_mode == drCOLLIDELINES) && GetLastSelectedDragObj() != NULL)
				{
					ActMapObjEdit->Execute();
				}
			}
			else if (Key == VK_UP || Key == VK_DOWN || Key == VK_LEFT || Key == VK_RIGHT)
			{
				int i;
				double speed = 1.0f;

				if (dr_mode != drCOLLIDELINES)
				{
					DropObjOnDrag();
				}

				for (i = 0; i < DragObjListCount(); i++)
				{
					double y, x;
					TMapPerson *mp = GetDragObjFromList(i);
					if (mp->GetObjType() == objMAPOBJ ||
							mp->GetObjType() == objMAPTEXTBOX ||
							mp->GetObjType() == objMAPSOUNDSCHEME ||
							mp->GetObjType() == objMAPOBJPOOL)
					{
						if (!((TMapGPerson*)mp)->ParentObj.IsEmpty() && DragObjListCount() > 1)
						{
							continue;
						}
						if (ActCmplxEnableDragChilds->Checked && !((TMapGPerson*)mp)->ParentObj.IsEmpty())
						{
							continue;
						}
						if (ActLockDecor->Checked && ((TMapGPerson*)mp)->GetDecorType() != decorNONE)
						{
							continue;
                        }
						if (ActStartEmul->Checked && !Shift.Contains(ssCtrl))
						{
							if (!((TMapGPerson*)mp)->GetVarValue(&speed, PredefProperties::PropertyNames[PredefProperties::PRP_SPEED]))
							{
								speed = 1.0f;
							}
						}
					}

					mp->getCoordinates(&x, &y);

					if (Key == VK_UP)
					{
						y -= speed;
					}
					else if (Key == VK_DOWN)
					{
						y += speed;
					}
					else if (Key == VK_LEFT)
					{
						x -= speed;
					}
					else if (Key == VK_RIGHT)
					{
						x += speed;
					}

					if (mp->GetObjType() == objMAPCOLLIDENODE)
					{
						TMapCollideLineNode* cn = static_cast<TMapCollideLineNode*>(mp);
						cn->setCoordinates(x, y, 0, 0);
						m_pObjManager->MarkCrosslines(cn);
					}
					else
					{
						mp->setCoordinates(x, y, 0, 0);
					}

					//перерисовать карту для уничтожения подложки под объектом карты
					DrawMap(false);
				}

				if (dr_mode == drCOLLIDELINES)
				{
					ApplyDraggedCollideNodes();
				}

				Key = 0;
			}
		}
		break;

	case drSTATIC:
		if (Shift.Contains(ssCtrl))
		{
			if (Key == 'p' || Key == 'P')
			{
				if (crVisible)
				{
					Key = 0;
					GMapBGObj *BGobj;
					if (m_px < 0 || m_py < 0 || m_px >= m_w || m_py >= m_h)
					{
						return;
					}
					if (gUsingOpenGL)
					{
						ReleaseRenderGL();
					}
					const __int32 idx = *(m_pObjManager->map + m_pObjManager->mp_w * m_py + m_px);
					Application->CreateForm(__classid(TFBOProperty), &FBOProperty);
					if (NONE_P != idx)
					{
						FBOProperty->Caption = FBOProperty->Caption + _T(" [") + Localization::Text(_T("mIndex")) + _T(" = ") + IntToStr(idx) + _T("]");
					}
					BGobj = new GMapBGObj(lrCount);
					if (idx < static_cast<__int32>(mainCommonRes->BackObj.size()))
					{
						mainCommonRes->BackObj[idx].CopyTo(BGobj);
					}
					FBOProperty->assignBGobj(BGobj);
					FBOProperty->setReadOnly(true);
					FBOProperty->ShowModal();
					FBOProperty->Free();
					delete BGobj;
					FBOProperty = NULL;
					if (gUsingOpenGL)
					{
						InitRenderGL();
					}
				}
			}
			else if (Key == 'c' || Key == 'C')
			{
				ActBGCopy->Execute();
				Key = 0;
			}
			else if (Key == 'v' || Key == 'V')
			{
				ActBGPaste->Execute();
				Key = 0;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActFindNextExecute(TObject *)
{
	DropObjOnDrag();
	if (!map_exist)
	{
		return;
	}

	__int32 i, j;
	TMapPerson *mo;
	TObjManager::TObjType tlist[5] =
	{
		TObjManager::BACK_DECORATION,
		TObjManager::CHARACTER_OBJECT,
		TObjManager::FORE_DECORATION,
		TObjManager::TRIGGER,
		TObjManager::DIRECTOR
	};
	if (showObjects)
	{
		for(j = 0; j < 5; j++)
		{
			__int32 len = m_pObjManager->GetObjTypeCount(tlist[j]);
			if (GetLastSelectedDragObj() == NULL)
			{
				if (len > 0)
				{
					DragObjListClear();
					DragObjListAdd((TMapPerson*)m_pObjManager->GetObjByIndex(tlist[j], 0));
					if (znCurZone != GetLastSelectedDragObj()->GetZone() && !ActStartEmul->Checked && GetLastSelectedDragObj()->GetZone() >= 0)
					{
						znCurZone = GetLastSelectedDragObj()->GetZone();
						ListBoxZone->ItemIndex = znCurZone;
						SetZoneVisibility();
					}
					else
					{
						DrawMap(false);
					}
					FindAndShowObject(GetLastSelectedDragObj());
					AddMarker(GetLastSelectedDragObj(), TObjMarker::TObjMarkerType::Attention);
					break;
				}
			}
			else
			{
				for (i = 0; i < len; i++)
				{
					if ((void*)GetLastSelectedDragObj() == m_pObjManager->GetObjByIndex(tlist[j], i))
					{
						if (i + 1 < len)
						{
							DragObjListClear();
							DragObjListAdd((TMapPerson*)m_pObjManager->GetObjByIndex(tlist[j], i + 1));
						}
						else
						{
							__int32 k1 = j;
							do
							{
								k1++;
								if (k1 > 4)
								{
									k1 = 0;
								}
								if (m_pObjManager->GetObjTypeCount(tlist[k1]) > 0)
								{
									DragObjListClear();
									DragObjListAdd((TMapPerson*)m_pObjManager->GetObjByIndex(tlist[k1], 0));
								}
							}
							while (GetLastSelectedDragObj() == m_pObjManager->GetObjByIndex(tlist[j], i) && k1 != j);
						}
						if (znCurZone != GetLastSelectedDragObj()->GetZone() && !ActStartEmul->Checked && GetLastSelectedDragObj()->GetZone() >= 0)
						{
							znCurZone = GetLastSelectedDragObj()->GetZone();
							ListBoxZone->ItemIndex = znCurZone;
							SetZoneVisibility();
						}
						else
						{
							DrawMap(false);
						}
						FindAndShowObject(GetLastSelectedDragObj());
						AddMarker(GetLastSelectedDragObj(), TObjMarker::TObjMarkerType::Attention);
						if (FObjOrder->Visible)
						{
							FObjOrder->RebuildSelectionList();
						}
						j = 5;
						break;
					}
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActShowFocusedExecute(TObject * /* Sender */ )
{
	if (GetLastSelectedDragObj() != NULL && showObjects)
	{
		AddMarker(GetLastSelectedDragObj(), TObjMarker::TObjMarkerType::Attention);
		FindAndShowObject(GetLastSelectedDragObj());
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActFindObjExecute(TObject *)
{
	DropObjOnDrag();
	FormFindObj->Show();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::SetScale(double iscale)
{
	if (gsGlobalScale != iscale)
	{
		gsGlobalScale = iscale;

		TIniFile *ini;
		ini = new TIniFile(PathIni + _T("editor.ini"));
		ini->WriteFloat(_T("Options"), _T("Scale"), gsGlobalScale);
		delete ini;

		if (map_exist)
		{
			DropObjOnDrag();

			if(dr_mode == drSTATIC)
			{
				m_CopyRectSet = false;
			}
			else
			{
				ClearCopySelection();
			}
			LMBtn_main.moved = false;
			LMBtn_main.pressed = false;
			m_px = m_py = -1;

			rebuildGameObjects(); //пересобираем все объекты игры на карте
			HorzScrollBar->PageSize = 0;
			VertScrollBar->PageSize = 0;
			HorzScrollBar->SetParams(0, 0, (__int32)((double)m_w * gsGlobalScale));
			VertScrollBar->SetParams(0, 0, (__int32)((double)m_h * gsGlobalScale));
			HorzScrollBar->SmallChange = (short)((double)p_w * gsGlobalScale);
			VertScrollBar->SmallChange = (short)((double)p_h * gsGlobalScale);
			HorzScrollBar->LargeChange = (short)((double)p_w * 3.0f * gsGlobalScale);
			VertScrollBar->LargeChange = (short)((double)p_h * 3.0f * gsGlobalScale);
			changeSizes();
		}
	}
	UnicodeString str = _T("x") + FloatToStr(gsGlobalScale);
	StatusBar1->Panels->Items[3]->Text = str;
	StatusBar1->Panels->Items[3]->Width = StatusBar1->Canvas->TextWidth(str) + GetSystemMetrics(SM_CXFRAME) * 4;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::StatusBar1MouseDown(TObject * /* Sender */ , TMouseButton /* Button */ , TShiftState /* Shift */ , __int32 X, __int32 Y)
{
	if (ActStartEmul->Checked)
		return;

	__int32 pl = StatusBar1->Panels->Items[0]->Width + StatusBar1->Panels->Items[1]->Width + StatusBar1->Panels->Items[2]->Width;
	if (X >= pl && X <= pl + StatusBar1->Panels->Items[3]->Width && Y >= 0 && Y <= StatusBar1->Height)
	{
		Application->CreateForm(__classid(TFormScale), &FormScale);
		FormScale->ComboBox->ItemIndex = GetCBIndexByScale(gsGlobalScale);
		if (FormScale->ShowModal() == mrOk)
		{
			double scale = GetScaleByCBIndex(FormScale->ComboBox->ItemIndex);
			if (gsGlobalScale != scale)
			{
				SetScale(scale);
            }
		}
		FormScale->Free();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActCmplxEnableDragChildsExecute(TObject * /* Sender */ )
{
	ActCmplxEnableDragChilds->Checked = !ActCmplxEnableDragChilds->Checked;
}
//---------------------------------------------------------------------------

void *__fastcall TForm_m::DbgGetFocusedObj()
{
	return(void*)GetLastSelectedDragObj();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::AddNewLevelToProj(TObjManager *objManager)
{
	m_pLevelList->Add(objManager);
	if (m_pLevelList->Count > 1)
		TabLevels->Tabs->Add(IntToStr(m_pLevelList->Count - 1));
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::TabLevelsChange(TObject *)
{
	DropObjOnDrag();
	if (ignoreTabLevelsChange || !map_exist)
	{
		return;
	}

	mDoNotDraw = true;
	mNeedRebuildBack = false;

	if(gUsingOpenGL)
	{
		TRenderGL::ReleaseBG();
	}

	ClearMarkers();
	HideObjPrpToolWindow();
	DragObjListClear();
	ClearCopySelection();
	if (FObjOrder->Visible)
	{
		FObjOrder->RebuildSelectionList();
	}

	Application->ProcessMessages();

	if(m_pObjManager != NULL)
	{
		m_pObjManager->ClearCashe();
	}
	m_pObjManager = (TObjManager*)m_pLevelList->Items[TabLevels->TabIndex];
	m_pObjManager->ClearCashe();

	FObjOrder->Caption = ActObjOrder->Caption;
	FObjOrder->SetObjManager(m_pObjManager);
	FObjOrder->RebuildList();

	TObjManager *mo = m_pObjManager;
	if (mo != NULL)
	{
		TObjManager::TObjType tlist[5] =
		{
			TObjManager::BACK_DECORATION,
			TObjManager::CHARACTER_OBJECT,
			TObjManager::FORE_DECORATION,
			TObjManager::TRIGGER,
			TObjManager::DIRECTOR
		};
		__int32 tlist_count = 5;
		__int32 i, j, len;
		for(j = 0; j < tlist_count; j++)
		{
			len = m_pObjManager->GetObjTypeCount(tlist[j]);
			for (i = 0; i < len; i++)
			{
				((TMapPerson*)m_pObjManager->GetObjByIndex(tlist[j], i))->SetVisible(false);
			}
		}
	}
	znCurZone = 0;
	SetDrMode(drNONE);
	map_exist = m_pObjManager != NULL;
	m_w = m_pObjManager->mp_w * p_w;
	m_h = m_pObjManager->mp_h * p_h;
	CreateMap(false); //создаем все вокруг карты (сама карта уже создана)
	MakeMapBackup();
	RelinkBGobjProperties(false); //завязываем свойства фоновых объектов
	RelinkGameObjStates(); //завязываем состояния
	rebuildGameObjects(); //пересобираем все объекты игры на карте
	ReorderPanelComponents();
	CreateZoneList();

	InitPrpWindows(mainCommonRes);

	mDoNotDraw = false;
	mNeedRebuildBack = true;

	if(gUsingOpenGL)
	{
		TRenderGL::InitBG();
	}

	if(Form_opt != NULL)
	{
		CancelOpt();
		ShowOpt(mEdit);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::SetActiveProjLevel(__int32 idx)
{
	DropObjOnDrag();
	if (idx < 0 || idx >= m_pLevelList->Count)
	{
		return;
    }
	TabLevels->TabIndex = idx;
	TabLevelsChange(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActNewMapExecute(TObject * /* Sender */ )
{
	int prpwCount = sizeof(gsPrpWindows) / sizeof(TForm*);
	for(int i = 0; i < prpwCount; i++)
	{
		gsPrpWindows[i]->Hide();
	}
	DropObjOnDrag();
	ShowOpt(mAdd);
	MainMenu1Change(NULL, NULL, false);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActMapDelExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	if (!map_exist)
	{
		return;
    }
	__int32 res = Application->MessageBox(Localization::Text(_T("mAskDeleteMap")).c_str(),
										Localization::Text(_T("mDelete")).c_str(),
										MB_YESNOCANCEL | MB_ICONQUESTION);
	if (res == ID_YES)
	{
		__int32 i, idx;
		TObjManager *mo;
		for (i = 0; i < m_pLevelList->Count; i++)
			if (m_pObjManager == m_pLevelList->Items[i])
			{
				idx = i;
				break;
			}
		if (idx < 0)
		{
			return;
        }
		ignoreTabLevelsChange = true;
		m_pLevelList->Delete(idx);
		TabLevels->Tabs->Clear();
		TabLevels->Tabs->Add(_T("0"));
		m_pObjManager->ClearAll();
		for (i = 1; i < m_pLevelList->Count; i++)
			TabLevels->Tabs->Add(IntToStr(i));
		mo = m_pObjManager;
		if (m_pLevelList->Count == 0)
		{
			map_exist = false;
			disabled_controlls = true;
			m_pObjManager = NULL;
			idx = m_w = m_h = 0;
			CreateMap(false);
		}
		else
		{
			if (idx >= m_pLevelList->Count)
				idx = m_pLevelList->Count - 1;
			m_pObjManager = (TObjManager*)m_pLevelList->Items[idx];
			m_pObjManager->ClearCashe();
		}
		delete mo;
		TabLevels->TabIndex = idx;
		ignoreTabLevelsChange = false;
		SetActiveProjLevel(idx);
		is_save = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActMapChangeIndexExecute(TObject * /* Sender */ )
{
	DropObjOnDrag();
	if (!map_exist)
		return;
	__int32 i, idx, int_res;
	UnicodeString res;
	idx = -1;
	for (i = 0; i < m_pLevelList->Count; i++)
		if (m_pObjManager == m_pLevelList->Items[i])
		{
			idx = i;
			break;
		}
	if (idx < 0)
		return;
	UnicodeString val = IntToStr(idx);
	res = InputBox(ActMapChangeIndex->Caption, Localization::Text(_T("mAskMapNumber")) + _T(":"), val);
	try
	{
		int_res = StrToInt(res);
	}
	catch(...)
	{
		return;
	}

	if (val == res || res.IsEmpty() || int_res < 0 || int_res >= m_pLevelList->Count)
		return;
	m_pLevelList->Move(idx, int_res);
	TabLevels->TabIndex = int_res;
	is_save = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActMapOptionsExecute(TObject * /* Sender */ )
{
	ShowOpt(mEdit);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActTileOptionsExecute(TObject *)
{
	__int32 tp_w, tp_h, td_w, td_h, tm_w, tm_h, i;
	TObjManager *tobjManager;
	bool needRebuildBackObjListView = false;
	DropObjOnDrag();

	if (!proj_exist)
	{
		return;
	}

	Application->CreateForm(__classid(TFTileSizes), &FTileSizes);
	FTileSizes->Caption = ActTileOptions->Caption;
	FTileSizes->CSpinEditPW->Value = p_w;
	FTileSizes->CSpinEditPH->Value = p_h;
	FTileSizes->CSpinEditLayers->Value = lrCount;
	tp_w = p_w;
	tp_h = p_h;
	if (map_exist)
	{
		td_w = d_w / p_w;
		td_h = d_h / p_h;
		tm_w = m_w / d_w;
		tm_h = m_h / d_h;
	}
	if (FTileSizes->ShowModal() == mrOk)
	{
		p_w = (short)FTileSizes->CSpinEditPW->Value;
		p_h = (short)FTileSizes->CSpinEditPH->Value;
		if (map_exist)
		{
			bool remainMapSize = false;
			if(FTileSizes->CheckBoxRemainMapSize->Enabled &&
				FTileSizes->CheckBoxRemainMapSize->Checked)
			{
				remainMapSize = true;
			}
			else
			{
				d_w = td_w * p_w;
				d_h = td_h * p_h;
				m_w = tm_w * d_w;
				m_h = tm_h * d_h;
				if(m_w > MAX_MAP_SIZE_PX)
				{
					d_w = MAX_MAP_SIZE_PX / p_w;
					m_w = d_w * p_w;
				}
				if(m_h > MAX_MAP_SIZE_PX)
				{
					d_h = MAX_MAP_SIZE_PX / p_h;
					m_h = d_h * p_h;
				}
			}

			tobjManager = m_pObjManager;
			for (i = 0; i < m_pLevelList->Count; i++)
			{
				m_pObjManager = (TObjManager*)m_pLevelList->Items[i];
				if ((tp_h > 0 && tp_w > 0) && (tp_h != p_h || tp_w != p_w))
				{
					m_pObjManager->ScalePosAll(p_w, p_h, tp_w, tp_h, remainMapSize);
					needRebuildBackObjListView = true;
				}
				if(remainMapSize)
				{
					m_pObjManager->old_mp_w = m_pObjManager->mp_w;
					m_pObjManager->old_mp_h = m_pObjManager->mp_h;
					m_pObjManager->mp_w = m_w / p_w;
					m_pObjManager->mp_h = m_h / p_h;
				}
				CreateMap(true);
			}
			m_pObjManager = tobjManager;
		}

		if (lrCount != FTileSizes->CSpinEditLayers->Value)
		{
			lrCount = FTileSizes->CSpinEditLayers->Value;
			const __int32 ct = static_cast<__int32>(mainCommonRes->BackObj.size());
			GMapBGObj *nmo = new GMapBGObj();
			for (i = 0; i < ct; i++)
			{
				GMapBGObj &mo = mainCommonRes->BackObj[i];
				if(mo.IsEmpty() == false)
				{
					mo.CopyTo(nmo, lrCount);
					nmo->CopyTo(&mo);
				}
			}
			delete nmo;
			needRebuildBackObjListView = true;
		}

		DrawMap(true);
		is_save = true;
	}

	FTileSizes->Free();
	FTileSizes = NULL;

	if (needRebuildBackObjListView)
	{
		rebuildBackObjListView();
	}

	MainMenu1Change(nullptr, nullptr, false);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActUndoExecute(TObject * /* Sender */ )
{
	if (!map_exist)
		return;
	if (dr_mode != drSTATIC)
		return;

	DropObjOnDrag();

	memcpy(m_pObjManager->map, m_pObjManager->map_undo, m_pObjManager->mp_w * m_pObjManager->mp_h*sizeof(unsigned short));
	DrawMap(true);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActBGCopyExecute(TObject * /* Sender */ )
{
	__int32 x, y;

	if (IsAnyPrpWindowVisible())
		return;

	switch(dr_mode)
	{
	case drOBJECTS:
    case drCOLLIDELINES:
		CopyGameObject();
		break;

	case drSTATIC:

		if (m_CopyRectCX < 0)
			return;
        //if(dr_mode != drSTATIC)return;

		DropObjOnDrag();

		DeleteCopyBGBuffer();
		m_CopyRectH = m_CopyRect.Height();
		m_CopyRectW = m_CopyRect.Width();
		m_CopyBGBuffer = new unsigned short*[m_CopyRectH];
		for (x = 0; x < m_CopyRectH; x++)
			m_CopyBGBuffer[x] = new unsigned short[m_CopyRectW];

		x = m_pObjManager->mp_w * m_CopyRect.Top + m_CopyRect.Left;
		for (y = 0; y < m_CopyRectH; y++)
		{
			memcpy(m_CopyBGBuffer[y], (m_pObjManager->map + x), m_CopyRectW*sizeof(short));
			x += m_pObjManager->mp_w;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CopyFromBGObjWindow(TGridRect iCopyRect)
{
	__int32 x, y, t, l;

	if (dr_mode != drSTATIC)
		return;

	DropObjOnDrag();

	DeleteCopyBGBuffer();
	m_CopyRectH = iCopyRect.Bottom - iCopyRect.Top + 1;
	m_CopyRectW = iCopyRect.Right - iCopyRect.Left + 1;
	m_CopyBGBuffer = new unsigned short*[m_CopyRectH];
	for (x = 0; x < m_CopyRectH; x++)
		m_CopyBGBuffer[x] = new unsigned short[m_CopyRectW];
	x = y = 0;
	for (t = iCopyRect.Top; t <= iCopyRect.Bottom; t++)
	{
		for (l = iCopyRect.Left; l <= iCopyRect.Right; l++)
		{
			if (!FBGObjWindow->GetBGObj(l, t)->IsEmpty())
			{
				m_CopyBGBuffer[y][x] = (unsigned short)(FBGObjWindow->GetRowCount() * l + t);
			}
			else
			{
				m_CopyBGBuffer[y][x] = NONE_P;
			}
			x++;
		}
		y++;
		x = 0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActGoToBGReferenceExecute(TObject * /* Sender */ )
{
	if (dr_mode != drSTATIC)
		return;

	if (m_px < 0 || m_py < 0 || m_px >= m_w || m_py >= m_h)
	{
		return;
	}
	const __int32 idx = *(m_pObjManager->map + m_pObjManager->mp_w * m_py + m_px);
	FBGObjWindow->FindBGObj(idx);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActBGPasteExecute(TObject * /* Sender */ )
{
	__int32 x, y, off, xx, yy;

	if (IsAnyPrpWindowVisible())
		return;

	switch(dr_mode)
	{
	case drOBJECTS:
    case drCOLLIDELINES:
		PasteGameObject();
		break;

	case drSTATIC:

		if (m_CopyBGBuffer == NULL)
			return;
       //if(dr_mode != drSTATIC)return;

		if (m_CopyRectCX >= 0)
		{
			yy = m_CopyRect.Top;
			xx = m_CopyRect.Left;
		}
		else
		{
			if (!crVisible)
				return;
			yy = m_py;
			xx = m_px;
		}
		off = m_pObjManager->mp_w * yy + xx;

		DropObjOnDrag();
		MakeMapBackup();

		for (y = 0; y < m_CopyRectH; y++)
		{
			for (x = 0; x < m_CopyRectW; x++)
			{
				if (xx + x >= m_pObjManager->mp_w)
					break;
				if (off + x >= m_pObjManager->mp_w * m_pObjManager->mp_h)
				{
					break;
				}
				*(m_pObjManager->map + off + x) = m_CopyBGBuffer[y][x];
			}
			if (yy + y + 1 >= m_pObjManager->mp_h)
			{
				break;
			}
			off += m_pObjManager->mp_w;
		}
		is_save = true;
		DrawMap(true);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActBGPasteMultiExecute(TObject * /* Sender */ )
{
	if (IsAnyPrpWindowVisible())
		return;

	__int32 x, y, off, xx, yy;
	if (m_CopyBGBuffer == NULL)
		return;
	if (dr_mode != drSTATIC)
		return;
	if (m_CopyRectCX < 0)
		return;

	DropObjOnDrag();
	MakeMapBackup();

	yy = 0;
	off = m_pObjManager->mp_w * m_CopyRect.Top + m_CopyRect.Left;
	for (y = 0; y < m_CopyRect.Height(); y++)
	{
		xx = 0;
		for (x = 0; x < m_CopyRect.Width(); x++)
		{
			if (xx >= m_CopyRectW)
				xx = 0;
			*(m_pObjManager->map + off + x) = m_CopyBGBuffer[yy][xx];
			xx++;
		}
		if (++yy >= m_CopyRectH)
			yy = 0;
		off += m_pObjManager->mp_w;
	}
	is_save = true;
	DrawMap(true);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActDelSelectionExecute(TObject * /* Sender */ )
{
	if (dr_mode == drSTATIC)
	{
		if (IsAnyPrpWindowVisible())
			return;

		if (!map_exist)
			return;
		DropObjOnDrag();
		ClearCopySelection();
		DrawMap(false);
	}
	else if (dr_mode == drOBJECTS)
	{
		if (!map_exist)
			return;
		DropObjOnDrag();
		DragObjListClear();
		ClearCopySelection();
		DrawMap(false);
		if (FObjOrder->Visible)
			FObjOrder->RebuildSelectionList();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActSelectAllExecute(TObject * /* Sender */ )
{
	if (dr_mode == drSTATIC)
	{
		if (IsAnyPrpWindowVisible())
		{
			return;
		}

		if (!map_exist)
		{
			return;
		}
		DropObjOnDrag();
		m_CopyRectSet = false;
		m_CopyRectCX = 0;
		m_CopyRectCY = 0;
		m_CopyRect.Left = 0;
		m_CopyRect.Top = 0;
		m_CopyRect.Bottom = (__int32)(((double)VertScrollBar->Max / gsGlobalScale) / (double)p_h);
		m_CopyRect.Right = (__int32)(((double)HorzScrollBar->Max / gsGlobalScale) / (double)p_w);
		DrawMap(false);
	}
	else if (dr_mode == drOBJECTS || dr_mode == drCOLLIDELINES)
	{
		if (!map_exist)
		{
			return;
        }
		DropObjOnDrag();
		m_CopyRect.Left = -HorzScrollBar->Position;
		m_CopyRect.Top = -VertScrollBar->Position;
		m_CopyRect.Bottom = VertScrollBar->Max;
		m_CopyRect.Right = HorzScrollBar->Max;
		TMapPerson *pt = DragObjListAddMulti(m_CopyRect, false);
		if (pt && IsAnyPrpWindowVisible())
		{
			ShowObjPrpToolWindow(pt, pt->GetObjType());
		}
		ClearCopySelection();
		DrawMap(false);
		if (FObjOrder->Visible)
		{
			FObjOrder->RebuildSelectionList();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DragObjListClear()
{
	DragObjList->Clear();
	mpLastDragObj = NULL;
	if(FormFindObj)
	{
		FormFindObj->UpdateButtonCaption();
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DragObjListAdd(TMapPerson *pt)
{
	if (pt == NULL)
	{
		return;
	}

	if (DragObjListContains(pt))
	{
		return;
	}

	DragObjList->Add(pt);
	mpLastDragObj = pt;
	FormFindObj->UpdateButtonCaption();
}
//---------------------------------------------------------------------------

TMapPerson *__fastcall TForm_m::DragObjListAddMulti(const TRect &SelectionRect, bool UnselectDublicates)
{
	TMapPerson *first = NULL;
	if (UnselectDublicates)
	{
		__int32 i, pos;
		TList *tlist = new TList();
		m_pObjManager->FindObjectPointers(tlist, SelectionRect);
		for (i = 0; i < tlist->Count; i++)
		{
			if ((pos = DragObjList->IndexOf(tlist->Items[i])) < 0)
			{
				DragObjList->Add(tlist->Items[i]);
				if (first == NULL)
				{
					first = (TMapGPerson*)tlist->Items[i];
				}
			}
			else
			{
				DragObjList->Delete(pos);
			}
		}
		delete tlist;
	}
	else
	{
		first = (TMapPerson*)m_pObjManager->FindObjectPointers(DragObjList, SelectionRect);
	}
	if (first != NULL)
	{
		mpLastDragObj = first;
	}
	FormFindObj->UpdateButtonCaption();
	return mpLastDragObj;
}
//---------------------------------------------------------------------------

bool __fastcall TForm_m::DragObjListContains(TMapPerson *pt)
{
	if (pt == nullptr)
	{
		return false;
	}
	return DragObjList->IndexOf(pt) >= 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::RemoveDragObjFromList(const UnicodeString Name)
{
	for (int i = 0; i < DragObjList->Count; i++)
	{
		if (Name.Compare(((TMapGPerson*)DragObjList->Items[i])->Name) == 0)
		{
			DragObjList->Delete(i);
			return;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::RemoveDragObjFromList(const TMapPerson *pt)
{
	for (int i = 0; i < DragObjList->Count; i++)
	{
		if (DragObjList->Items[i] == pt)
		{
			DragObjList->Delete(i);
			return;
		}
	}
}

//---------------------------------------------------------------------------
__int32 __fastcall TForm_m::DragObjListCount()
{
	return DragObjList->Count;
}
//---------------------------------------------------------------------------

TMapPerson *__fastcall TForm_m::GetDragObjFromList(__int32 idx)
{
	assert(idx >= 0 || idx < DragObjList->Count);
	return(TMapGPerson*)DragObjList->Items[idx];
}
//---------------------------------------------------------------------------

TMapPerson *__fastcall TForm_m::GetLastSelectedDragObj() const
{
	return mpLastDragObj;
}
//---------------------------------------------------------------------------

void *__fastcall TForm_m::FindObjectPointer(__int32 srcX, __int32 srcY, __int32 &dx, __int32 &dy, bool useParentIntersection)
{
	if (!map_exist)
	{
		return NULL;
	}
	return m_pObjManager->FindObjectPointer(srcX, srcY, dx, dy, useParentIntersection);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ShowObjHint(bool autoHint)
{
	if (MMVHideHints->Checked && autoHint)
	{
		mpHintWnd->ReleaseHandle();
		return;
	}

	TPoint pt;
	__int32 dx, dy;
	bool res = false;
	pt = PaintBox1->ScreenToClient(Mouse->CursorPos);
	TMapGPerson *obj = (TMapGPerson*)FindObjectPointer(pt.x, pt.y, dx, dy, false);
	if (obj != NULL)
	{
		if (obj->Visible)
		{
			if (!obj->Hint.IsEmpty())
			{
				mpHintWnd->ReleaseHandle();
				TRect rect = mpHintWnd->CalcHintRect(this->Width, obj->Hint, NULL);
				rect.Left = rect.Left + Mouse->CursorPos.x;
				rect.Right = rect.Right + Mouse->CursorPos.x;
				rect.Top = rect.Top + Mouse->CursorPos.y + GetSystemMetrics(SM_CYCURSOR) / 2 + GetSystemMetrics(SM_CYCURSOR) / 6;
				rect.Bottom = rect.Bottom + Mouse->CursorPos.y + GetSystemMetrics(SM_CYCURSOR) / 2 + GetSystemMetrics(SM_CYCURSOR) / 6;
				mpHintWnd->ActivateHint(rect, obj->Hint);
				res = true;
			}
		}
	}
	if (!res)
	{
		mpHintWnd->ReleaseHandle();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActShowHintExecute(TObject * /* Sender */ )
{
	ShowObjHint(false);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CopyGameObject()
{
	if (DragObjListCount() == 0 || CF_ME == 0 || !(dr_mode == drOBJECTS || dr_mode == drCOLLIDELINES))
	{
		return;
	}

	if (!map_exist)
		return;

	CancelCollideLine();
	DropObjOnDrag();

	__int32 i, j, w_val;
	BYTE ch[3];
	HGLOBAL aDataHandle;
	void *aDataPtr;
	TMapPerson *gp;
	TMapPerson *gp_copy;
	TList *collidelines = new TList();
	TClipboard *cb = new TClipboard();
	UnicodeString varname;

	mpCopyBuffer->Clear();

	TSaveLoadStreamData::TSaveLoadStreamDataInit initdata;
	initdata.type = TSaveLoadStreamData::TSLDataType::CopyClipboard;
	initdata.memoryStream = mpCopyBuffer;

	TSaveLoadStreamData *iodata = new TSaveLoadStreamData(initdata, false);
    iodata->Init();

	TSaveLoadStreamHandle rootHandle;
	iodata->writeFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftNewSection, "copygameobject", rootHandle);

	ch[0] = CLIPBOARD_VER;
	ch[1] = CLIPBOARD_TYPE_GAMEOBJ;

	iodata->writeFn(ch, TSaveLoadStreamData::TSLFieldType::ftU16, "header", rootHandle);

	for (i = 0; i < DragObjListCount(); i++)
	{
		gp = GetDragObjFromList(i);

		if (gp->GetObjType() == objMAPCOLLIDENODE)
		{
			const UnicodeString pname = ((TMapCollideLineNode*)gp)->getGParentName();
			gp = m_pObjManager->FindObjectPointer(pname);
			assert(gp);
			if (collidelines->IndexOf(gp) >= 0)
			{
				continue;
			}
			collidelines->Add(gp);
		}

		ch[0] = gp->GetObjType();
		iodata->writeFn(ch, TSaveLoadStreamData::TSLFieldType::ftChar, "type", rootHandle);

		SaveGameObjectTo(*iodata, rootHandle, gp);

		{ //значение переменных
			const std::list<PropertyItem>* vars = mainCommonRes->MainGameObjPattern->GetVars();
			w_val = vars->size();
			switch(gp->GetObjType())
			{
			case objMAPOBJ:
			case objMAPTRIGGER:
				iodata->writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftS32, "varcount", rootHandle);
				if (!vars->empty())
				{
					std::list<PropertyItem>::const_iterator iv;
					for (iv = vars->begin(); iv != vars->end(); ++iv)
					{
						bool val = true;
						double f_val = 0.0f;

						WriteAnsiStringData(*iodata, rootHandle, "varname", iv->name);

						((TMapGPerson*)gp)->GetVarValue(&f_val, iv->name);

						//mpCopyBuffer->Write(&f_val, sizeof(double));
						iodata->writeFn(&f_val, TSaveLoadStreamData::TSLFieldType::ftDouble, "varvalue", rootHandle);

						if (gp->GetObjType() == objMAPTRIGGER)
						{ //для триггеров записать checked
							((TMapGPerson*)gp)->GetCheckedValue(&val, iv->name);
						}
						//mpCopyBuffer->Write(&val, 1);
						iodata->writeFn(&val, TSaveLoadStreamData::TSLFieldType::ftChar, "varchecked", rootHandle);
					}
				}
			break;
			default:
			break;
			}
		}
	}
	ch[0] = objNONE;
	//mpCopyBuffer->Write(ch, 1);
	iodata->writeFn(ch, TSaveLoadStreamData::TSLFieldType::ftChar, "type", rootHandle);

	delete collidelines;
	delete iodata;

	cb->Open();
	mpCopyBuffer->Seek(0, soFromBeginning);
	aDataHandle = GlobalAlloc(GMEM_DDESHARE | GMEM_MOVEABLE, (size_t)mpCopyBuffer->Size);
	try
	{
			//lock to get memory adrress from handle because we use GMEM_MOVEABLE when GlobalAlloc()
		aDataPtr = GlobalLock(aDataHandle);
		if (aDataPtr != NULL)
		{
			MoveMemory(aDataPtr, mpCopyBuffer->Memory, (size_t)mpCopyBuffer->Size);
			cb->Clear();
				//set data. after setAsHAndle, aDataHandle is owned by clipboard. we must not delete
			cb->SetAsHandle(CF_ME, (THandle)aDataHandle);
		}
		GlobalUnlock(aDataHandle);
	}
	catch(...)
	{
		GlobalFree(aDataHandle);
	}
	cb->Close();
	delete cb;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::PasteGameObject()
{
	if (CF_ME == 0 || !(dr_mode == drOBJECTS || dr_mode == drCOLLIDELINES))
	{
		return;
	}

	if (!map_exist)
		return;

	CancelCollideLine();
	DropObjOnDrag();

	TClipboard *cb = new TClipboard();
	__int32 var_count, i, w_val;
	bool hasFormat;
	HGLOBAL aDataHandle;
	void *aDataPtr;
	BYTE ch[256];
	wchar_t wname[1024];
	TList* cpxlst = new TList();

	hasFormat = cb->HasFormat(CF_ME);
	if (hasFormat)
	{
		cb->Open();
		aDataHandle = (HGLOBAL)cb->GetAsHandle(CF_ME);
		aDataPtr = GlobalLock(aDataHandle);
		if (aDataPtr != NULL)
		{
			mpCopyBuffer->Clear();
			mpCopyBuffer->Size = GlobalSize(aDataHandle);
			MoveMemory(mpCopyBuffer->Memory, aDataPtr, (size_t)mpCopyBuffer->Size);
		}
		GlobalUnlock(aDataHandle);
		cb->Close();
	}
	delete cb;

	if(!hasFormat)
	{
		return;
	}

	TSaveLoadStreamData::TSaveLoadStreamDataInit initdata;
	initdata.type = TSaveLoadStreamData::TSLDataType::PasteClipboard;
	initdata.memoryStream = mpCopyBuffer;

	TSaveLoadStreamData *iodata = new TSaveLoadStreamData(initdata, false);
    iodata->Init();

	TSaveLoadStreamHandle rootHandle;
	iodata->readFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftNewSection, "copygameobject", rootHandle);

	DragObjListClear();
	//mpCopyBuffer->Read(ch, 2);
	iodata->readFn(ch, TSaveLoadStreamData::TSLFieldType::ftU16, "header", rootHandle);
	if(CLIPBOARD_TYPE_GAMEOBJ != ch[1] || CLIPBOARD_VER != ch[0])
	{
		delete iodata;
		return;
	}

	//mpCopyBuffer->Read(ch, 1); //type
	iodata->readFn(ch, TSaveLoadStreamData::TSLFieldType::ftChar, "type", rootHandle);

	while (objNONE != ch[0])
	{
		TMapPerson *gp = LoadGameObjectFrom(*iodata, rootHandle, FILEFORMAT_VER, m_pObjManager);
		if (gp != NULL)
		{
			DragObjListAdd(gp);
			//значение переменных
			switch(gp->GetObjType())
			{
				case objMAPOBJ:
				if(((TMapGPerson*)gp)->IsChildExist() && ((TMapGPerson*)gp)->ParentObj.IsEmpty())
				{
					cpxlst->Add(gp);
				}
				case objMAPTRIGGER:
					iodata->readFn(&var_count, TSaveLoadStreamData::TSLFieldType::ftS32, "varcount", rootHandle);
					if (var_count > 0)
					{
						for (i = 0; i < var_count; i++)
						{
							bool val = true;
							UnicodeString name;
							double f_val = 0.0f;

							ReadAnsiStringData(*iodata, rootHandle, "varname", name, FILEFORMAT_VER);

							iodata->readFn(&f_val, TSaveLoadStreamData::TSLFieldType::ftDouble, "varvalue", rootHandle);

							if (gp != NULL)
							{
								((TMapGPerson*)gp)->SetVarValue(f_val, name);
							}

							iodata->readFn(&val, TSaveLoadStreamData::TSLFieldType::ftChar, "varchecked", rootHandle);

							if (gp != nullptr)
							{
								if (gp->GetObjType() == objMAPTRIGGER)
								{
									((TMapGPerson*)gp)->SetCheckedValue(val, name);
								}
							}
						}
					}
				break;
				default:
				break;
			}

			iodata->readFn(ch, TSaveLoadStreamData::TSLFieldType::ftChar, "type", rootHandle);
		}
		else
		{
			break;
		}
	}
	while(cpxlst->Count > 0)
	{
		m_pObjManager->OrderChildsBeforeMain((TMapGPerson *)cpxlst->Items[cpxlst->Count - 1]);
		cpxlst->Delete(cpxlst->Count - 1);
	}

	delete cpxlst;
	delete iodata;

	FObjOrder->RebuildList();
	FGOProperty->refreshCplxList();
	FGOProperty->refreshNodes();
	FDirectorProperty->refreshNodes();
	FDirectorProperty->refreshTriggers();
	FDirectorProperty->refreshEvents();
	FSpObjProperty->refreshNodes();

	DrawMap(false);
}
//---------------------------------------------------------------------------

bool __fastcall TForm_m::RefreshFonts()
{
    bool res = false;
	TImageFont *font;
	if (mainCommonRes != NULL)
	{
		UnicodeString fontName;
		UnicodeString path;
		TFileListBox *flb = new TFileListBox((TComponent*)NULL);
		flb->Visible = false;
		flb->Parent = this;

		mpTexts->ClearFonts();

		UnicodeString ext = _T("*.xxx");
		path = PathPngRes;

		flb->Mask = forceCorrectFontFileExtention(ext);
		try
		{
			flb->Directory = path;
			for (__int32 i = 0; i < flb->Count; i++)
			{
				font = new TImageFont();
				fontName = ChangeFileExt(flb->Items->Strings[i], _T(""));
				if (font->LoadFont(fontName))
				{
                    res = true;
					mpTexts->AddFont(font);
				}
				else
				{
					delete font;
				}
			}
		}
		catch(...)
		{
			res = false;
		}
		delete flb;
	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActionList1Execute(TBasicAction *Action, bool &Handled)
{
	if (Screen->ActiveForm != this)
	{
		Handled = true;
		if (Action == ActBGCopy)
		{
			if (Screen->ActiveForm->ActiveControl != NULL)
			{
				PostMessage(Screen->ActiveForm->ActiveControl->Handle, WM_COPY, 0, 0);
			}
		}
		if (Action == ActBGPaste)
		{
			if (Screen->ActiveForm->ActiveControl != NULL)
			{
				PostMessage(Screen->ActiveForm->ActiveControl->Handle, WM_PASTE, 0, 0);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::TabSetObjInit()
{
	mPrevGameObjListViewCursorPos = 0;
	mGameObjListViewCount = PARENT_TYPE_COUNT_DEFAULT;
	TabSetObj->Tabs->Clear();
	for (__int32 i = 0; i < PARENT_TYPE_COUNT_MAX; i++)
	{
		mGameObjListViewCursorPos[i] = -1;
		if(i == 0)
		{
			TabSetObj->Tabs->Add(Localization::Text(PARENT_TYPE_SYSTEM_NAME));
		}
		else if(i > 0 && i < mGameObjListViewCount)
		{
			TabSetObj->Tabs->Add(Localization::Text(PARENT_TYPE_DEFAULT_NAME_PREFIX) + _T(" ") + IntToStr(i));
		}
	}
	TabSetObj->TabIndex = 0;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::TabSetObjClick(TObject *)
{
	if (mPrevGameObjListViewCursorPos == TabSetObj->TabIndex)
	{
		return;
	}
	mGameObjListViewCursorPos[mPrevGameObjListViewCursorPos] = -1;
	if (ListView_u->Items->Count > 0 && ListView_u->Selected != NULL)
	{
		mGameObjListViewCursorPos[mPrevGameObjListViewCursorPos] = ListView_u->Selected->Index;
	}
	rebuildGameObjListView();
	mPrevGameObjListViewCursorPos = TabSetObj->TabIndex;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::PMPTAddTabClick(TObject *)
{
	if(mGameObjListViewCount < PARENT_TYPE_COUNT_MAX)
	{
		mGameObjListViewCount++;
		TabSetObj->Tabs->Add(Localization::Text(PARENT_TYPE_DEFAULT_NAME_PREFIX) + _T(" ") + IntToStr(mGameObjListViewCount - 1));
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::PMPTRenameTabClick(TObject *)
{
	UnicodeString name;
	bool res;
	__int32 pos, idx;
	res = true;
	if(TabSetObj->TabIndex <= 0)
	{
		return;
	}

	Application->CreateForm(__classid(TFOPEdit), &FOPEdit);
	FOPEdit->ButtonOk->ModalResult = mrOk;
	FOPEdit->SetMode(TFOPEdit::TFOPEMLanguages);
	FOPEdit->Caption = PMPTRenameTab->Caption;
	FOPEdit->Label1->Caption = Localization::Text(_T("mGroupName"));
	FOPEdit->Ed_name->CharCase = ecNormal;
	FOPEdit->Ed_name->OnChange = NULL;
	FOPEdit->Ed_name->MaxLength = 32;
	FOPEdit->Ed_name->Text = TabSetObj->Tabs->Strings[TabSetObj->TabIndex];
	do
	{
		if (FOPEdit->ShowModal() == mrOk)
		{
			name = FOPEdit->GetVarName();
            pos = TabSetObj->Tabs->IndexOf(name);
			if (!name.IsEmpty() && (pos < 0 || pos == TabSetObj->TabIndex))
			{
				TabSetObj->Tabs->Strings[TabSetObj->TabIndex] = name;
				res = false;
			}
			if (res && pos >= 0)
			{
				Application->MessageBox(Localization::Text(_T("mGroupNameExist")).c_str(),
                                Localization::Text(_T("mMessageBoxWarning")).c_str(),
                                MB_OK | MB_ICONERROR);
			}
		}
		else
		{
			res = false;
		}
	}
	while (res);
	FOPEdit->Free();
	FOPEdit = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CreateLanguageMenu()
{
	__int32 ct;
	TMenuItem *mi;
	TStringList *sl = new TStringList();
	mpTexts->GetLanguages(sl);
	FreeLanguageMenu();
	MVLanguages->AutoHotkeys = maManual;
    ct = 0;
	for (__int32 i = 0; i < sl->Count; i++)
	{
		if(!sl->Strings[i].IsEmpty())
		{
			ct++;
			mi = new TMenuItem(MVLanguages);
			mi->AutoHotkeys = maManual;
			mi->Caption = sl->Strings[i];
			mi->Hint = sl->Strings[i];
			if(GetCurrentLanguage().Compare(mi->Caption) == 0)
			{
				mi->Checked = true;
			}
			else
			{
            	mi->Checked = false;
			}
			mi->OnClick = MDLanguageItemClick;
			MVLanguages->Add(mi);
		}
	}

	if (ct == 0)
	{
		mi = new TMenuItem(MVLanguages);
		mi->Caption = Localization::Text(_T("mEmpty"));
		mi->Enabled = false;
		MVLanguages->Add(mi);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::FreeLanguageMenu()
{
	for (__int32 i = 0; i < MVLanguages->Count; i++)
		delete MVLanguages->Items[i];
	MVLanguages->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::MDLanguageItemClick(TObject *Sender)
{
	TMenuItem *m = static_cast<TMenuItem*>(Sender);
	for (__int32 i = 0; i < MVLanguages->Count; i++)
	{
		static_cast<TMenuItem*>(MVLanguages->Items[i])->Checked = false;
	}
	m->Checked = true;
	mCurrentLanguage = m->Caption;
	m_pObjManager->ClearCashe();
	DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::GetTileSize(short &w, short &h) const
{
	w = p_w;
	h = p_h;
}
//---------------------------------------------------------------------------

__int32 __fastcall TForm_m::GetLayersCount() const
{
	return lrCount;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::GetMapSize(__int32 &w, __int32 &h) const
{
	w = m_w;
	h = m_h;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::GetDisplaySize(__int32 &w, __int32 &h) const
{
	w = d_w;
	h = d_h;
}
//---------------------------------------------------------------------------

const TObjManager* __fastcall TForm_m::GetCurrentObjManager() const
{
	assert(m_pObjManager);
	return m_pObjManager;
}
//---------------------------------------------------------------------------

const TCommonResObjManager* __fastcall TForm_m::GetCurrentResObjManager() const
{
	assert(mainCommonRes);
	return mainCommonRes;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CheckMenuCollideLines()
{
	bool EmStart = ActStartEmul->Checked;
	bool a = map_exist && !EmStart && !disabled_controlls;
	bool b = proj_exist && !EmStart && !disabled_controlls;
	bool enabled = a && b && LMBtn_main.collidenode == false && dr_mode == drCOLLIDELINES;
	if(enabled == false)
	{
		ActNewCollideShape->Enabled = false;
		ActNewCollideNode->Enabled = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::MoveOrCreateCollideNode(MoveOrCreateCollideNodeMode mode)
{
	if(dr_mode == drCOLLIDELINES)
	{
		TMapCollideLineNode *pn = NULL;
		const TMapCollideLineNode *n1 = NULL;
		const TMapCollideLineNode *n2 = NULL;
		const TCollideChainShape *sh = NULL;
		if(LMBtn_main.collidenode == true)
		{
			assert(DragObjListCount() <= 1);
			pn = static_cast<TMapCollideLineNode *>(GetLastSelectedDragObj());
			assert(pn);
			assert(pn->GetObjType() == objMAPCOLLIDENODE);
		}
		double x = static_cast<double>(m_px);
		double y = static_cast<double>(m_py);
		const double findRadius = MoveOrCreateCollideNodeMode::mccnInsertNode == mode ? TMapCollideLineNode::gscCollideLineNodeDefSize : 0.5;
		const TObjManager::TCreateCollideNodeType ccnt = m_pObjManager->TryCreateCollideNode(x, y, findRadius, pn, &sh, &n1, &n2);
		if(LMBtn_main.collidenode == false)
		{
			switch(ccnt)
			{
				default:
				return;

				case TObjManager::CCNT_ADDNODETOSHAPE:
					assert(sh);
				case TObjManager::CCNT_NEWSHAPE:
					DragObjListClear();
					if(AddStaticToMap(false) == true)
					{
						LMBtn_main.collidenode = true;
						CheckMenuCollideLines();
					}
					else
					{
						CancelCollideLine();
					}
				return;

				case TObjManager::CCNT_INSERTNODETOSHAPE:
					assert(sh);
					assert(n1);
					DragObjListClear();
					mCurrentCollideChainShapeName = sh->Name;

					m_px = x;
					m_py = y;
					if(AddStaticToMap(false) == true)
					{
						const TMapCollideLineNode *n = static_cast<const TMapCollideLineNode *>(GetLastSelectedDragObj());
						assert(n->getGParentName().Compare(sh->Name) == 0);
						m_pObjManager->ChangeNodeIndexInCollideShape(sh->Name, n->Name, n2->Name);
						CheckMenuCollideLines();
					}
					else
					{
						CancelCollideLine();
					}
				return;
			}
		}
		else if(sh != NULL && ccnt == TObjManager::CCNT_NONE && pn->IsErrorMark() == false)
		{
			DragObjListClear();
			if(sh->IsLastNode(pn) == true || sh->IsFirstNode(pn) == true)
			{
				if(AddStaticToMap(false) == true)
				{
					pn->ApplyCoordinates();
				}
				else
				{
					CancelCollideLine();
				}
			}
			else
			{
				pn->ApplyCoordinates();
				CancelCollideLine();
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActNewCollideShapeExecute(TObject *Sender)
{
	(void)Sender;
	if(ActNewCollideShape->Enabled)
	{
		MoveOrCreateCollideNode(MoveOrCreateCollideNodeMode::mccnNewShape);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ActNewCollideNodeExecute(TObject *Sender)
{
	(void)Sender;
	if(ActNewCollideNode->Enabled)
	{
		MoveOrCreateCollideNode(MoveOrCreateCollideNodeMode::mccnInsertNode);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ApplyDraggedCollideNodes()
{
	__int32 i;
	TMapCollideLineNode *cn = NULL;
	for(i = 0; i < DragObjListCount(); i++)
	{
		TMapPerson *dgo = GetDragObjFromList(i);
		if(dgo->GetObjType() == objMAPCOLLIDENODE)
		{
			cn = static_cast<TMapCollideLineNode*>(dgo);
			if(cn->IsErrorMark() == true)
			{
				CancelCollideLine();
				break;
			}
		}
	}
	const __int32 ct = DragObjListCount();
	for(i = 0; i < ct; i++)
	{
		TMapPerson *dgo = GetDragObjFromList(i);
		if (dgo->GetObjType() == objMAPCOLLIDENODE)
		{
			cn = static_cast<TMapCollideLineNode*>(dgo);
			cn->ApplyCoordinates();
		}
	}
	if(cn != NULL)
	{
		m_pObjManager->ResetCrosslinesErrorMarkers();
	}
}
//---------------------------------------------------------------------------

