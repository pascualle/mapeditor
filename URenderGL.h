//---------------------------------------------------------------------------
#ifndef UrenderGL_H
#define UrenderGL_H

#include "windows.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <Classes.hpp>
#include "UTypes.h"
#include <vector>
#include "system.h"

//---------------------------------------------------------------------------

#define RGL_MAX_GL_WINDOWS (2)

extern bool gUsingOpenGL;

class TCommonResObjManager;
class BMPImage;

typedef int TRGLWinId;

class TRenderGL
{
 public:

	enum RGLLineStyle
	{
		RGLLSSolid = 0xFFFF,
		RGLLSDot = 0xAAAA,
		RGLLSDash = 0xCCCC
	};

	enum
	{
		TEXTURE_SP_DIRECT = 0,
		TEXTURE_SP_TRANSPARENCY_PATTERN,
		TEXTURE_SP_COUNT
	};

	static bool __fastcall Init();
	static TRGLWinId __fastcall InitWindow(HWND hWnd, TRect viewport, TRect window, bool windowMode);
	static void __fastcall SetActiveWindow(TRGLWinId WinID);
	static void __fastcall ReleaseWindows();
	static void __fastcall Enable(bool val);
	static bool __fastcall IsEnabled();
	static bool __fastcall IsInited();

	static void __fastcall SetActiveCommonResObjManager(const TCommonResObjManager* ipCM);
	static int __fastcall CreateTexture();
	static void __fastcall UpdateTextureData(int idx);
	static void __fastcall DeleteTexture(int idx);
	static void __fastcall InitBG();
	static void __fastcall ReleaseBG();

	static void __fastcall Resize(TRect viewport, TRect window);
	static void __fastcall DrawScene(HDC hdc = NULL);
	static void __fastcall ClearRenderList();

	static void __fastcall SetBackdropImage(const BMPImage* pImage, TColor altColor);
	static void __fastcall SetColor(TColor color);
	static void __fastcall SetLineStyle(RGLLineStyle iStyle);
	static void __fastcall SetColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
	static void __fastcall FillRect(double iX, double iY, double iWidth, double iHeight);
	static void __fastcall RenderRect(double iX, double iY, double iWidth, double iHeight);
	static void __fastcall RenderLine(double iX1, double iY1, double iX2, double iY2);
	static void __fastcall RenderCircle(double iX, double iY, double iR);
	static void __fastcall FlushRenderNonResManagerImage();
	static void __fastcall RenderNonResManagerImage(const BMPImage* pImage,
							DrawImageType type,
                            double iSrcX, double iSrcY,
                            double iSrcWidth, double iSrcHeight,
                            double iDestX, double iDestY,
                            double iDestWidth, double iDestHeight,
							double iTransparency);
 	static void __fastcall RenderImage(int idx,
                            DrawImageType type,
                            double iSrcX, double iSrcY,
                            double iSrcWidth, double iSrcHeight,
                            double iDestX, double iDestY,
                            double iDestWidth, double iDestHeight,
							double iTransparency);
 private:

    enum RGLRenderTypes
    {
        RGL_PIXEL = 0,
        RGL_LINE,
        RGL_FILLRECT,
        RGL_RECT,
        RGL_CIRCLE,
        RGL_IMAGE,
    };

    struct TRGLColor
    {
    	double r;
        double g;
        double b;
        double a;
    };

    struct TRGLRenderItem
    {
		char mType;
		int mResIdx;
        double mX;
        double mY;
        double mX1;
        double mY1;
        double mZ;
        double mWidth;
        double mHeight;
		double mDestWidth;
		double mDestHeight;
        double mTransparency;
        TRGLColor mColor;
        GLushort mLinePattern;
        const BMPImage* mpData;
        __int32 mNext;
    };

	static bool __fastcall SetupPixelFormat(HDC hdc, bool windowMode);
	static void __fastcall BindDirectTexture(const BMPImage* pImage);
	static void DrawBackdrop(s32 *currentTextureID);

	static HWND  mhWnd[RGL_MAX_GL_WINDOWS];
	static HDC   mghDC[RGL_MAX_GL_WINDOWS];
	static HGLRC mghRC;
    static TRect mViewport;
	static bool	 mEnabled;
	static int mTextureCount;
	static unsigned int mSPTexture[TEXTURE_SP_COUNT];
	static bool mSPTextureInit;
	static TRGLWinId mCurrentWindowId;
	static bool mPatternImage;
	static std::vector<DrawImageFunctionData> mDirectBindData;
	static const void* mpDirectBindImage;
	static const TCommonResObjManager* mpCommonResObjManager;
	static float mCr,mCg,mCb,mCa;
};

//---------------------------------------------------------------------------
#endif

