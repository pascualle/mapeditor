//---------------------------------------------------------------------------

#ifndef UObjOrderH
#define UObjOrderH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ActnList.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include <ImgList.hpp>
#include <System.Actions.hpp>
#include <System.ImageList.hpp>
#include <list>
//---------------------------------------------------------------------------

class TObjManager;
class TMapGPerson;
class TCollideChainShape;

class TFObjOrder : public TForm
{
__published:
	TComboBox *ComboBox;
	TBevel *Bevel1;
	TActionList *ActionList1;
	TAction *ActUp;
	TAction *ActDown;
	TBitBtn *Bt_trup;
	TBitBtn *Bt_trdown;
	TStatusBar *StatusBar1;
	TBitBtn *Bt_Options;
	TBitBtn *Bt_Delete;
	TAction *ActOptions;
	TAction *ActDelete;
	TTreeView *TreeView;
	TImageList *ImageList_objmanager;
	TBitBtn *BtVisibility;
	TBitBtn *BtVisibleAll;
	TBitBtn *BtHideAll;
	TPopupMenu *PopupMenu1;
	TMenuItem *N1;
	TBitBtn *BtLockAll;
	TBitBtn *BtUnlockAll;
	TBitBtn *BtLock;
	TAction *ActionLock;
	TAction *ActionLockAll;
	TAction *ActionUnlockAll;
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall ComboBoxChange(TObject *Sender);
	void __fastcall ActUpExecute(TObject *Sender);
	void __fastcall ActDownExecute(TObject *Sender);
	void __fastcall FormKeyPress(TObject *Sender, char &Key);
	void __fastcall ListBoxKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall Bt_OptionsClick(TObject *Sender);
	void __fastcall Bt_DeleteClick(TObject *Sender);
	void __fastcall ActOptionsExecute(TObject *Sender);
	void __fastcall ActDeleteExecute(TObject *Sender);
	void __fastcall TreeViewChange(TObject *Sender, TTreeNode *Node);
	void __fastcall TreeViewKeyPress(TObject *Sender, char &Key);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormDeactivate(TObject *Sender);
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall TreeViewMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
	void __fastcall BtVisibilityClick(TObject *Sender);
	void __fastcall BtHideAllClick(TObject *Sender);
	void __fastcall BtVisibleAllClick(TObject *Sender);
	void __fastcall TreeViewDblClick(TObject *Sender);
	void __fastcall ActionLockExecute(TObject *Sender);
	void __fastcall ActionLockAllExecute(TObject *Sender);
	void __fastcall ActionUnlockAllExecute(TObject *Sender);

private:

	enum TOrderMode
	{
		MODE_OBJ = 0,
		MODE_DECOR_BACK,
		MODE_DECOR_FORE,
		MODE_TRIGGER,
		MODE_DIRECTOR,
		MODE_COLLIDESHAPE
	};

	struct TListNodeItem
	{
		UnicodeString name;
		TTreeNode *node;
	};

	void __fastcall RebuildCurrentPointerList();
	void __fastcall AddNodeBranch(const TMapGPerson *mp, TTreeNode *root);
	void __fastcall AddNodeBranch(const TCollideChainShape *sh, TTreeNode *root);
	void __fastcall UpdateButtonsState();

	void __fastcall CreateList();
	const TListNodeItem* __fastcall FindInBucket(const TTreeNode *node) const;
	TTreeNode* __fastcall FindInBucket(const UnicodeString name) const;
	const int& __fastcall GetImgStatusIdx(const TMapGPerson *mp) const;


	TObjManager *m_pObjManager;
	TList *mpCurrentLst;
	std::list<TListNodeItem> mBucket;
	bool mCreateList;
	bool mRefreshSelection;
	TOrderMode mMode;

public:

	void __fastcall RebuildList();
	void __fastcall RebuildSelectionList();
	void __fastcall Localize();
	void __fastcall SetObjManager(TObjManager *om);
	void __fastcall LockAll(bool val);

	__fastcall TFObjOrder(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFObjOrder *FObjOrder;
//---------------------------------------------------------------------------
#endif
