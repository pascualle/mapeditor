//---------------------------------------------------------------------------

#ifndef USndObjPropertyH
#define USndObjPropertyH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ToolWin.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Samples.Spin.hpp>

#include "UObjCode.h"
#include <System.Actions.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>

//---------------------------------------------------------------------------

class TCommonResObjManager;

class TFSndObjProperty : public TPrpFunctions
{
__published:
	TGroupBox *GroupBoxPos;
	TLabel *Label12;
	TLabel *Label14;
	TListView *ListView;
	TToolBar *ToolBar1;
	TToolButton *ToolButton5;
	TToolButton *ToolButton2;
	TToolButton *ToolButton1;
	TToolButton *ToolButton3;
	TActionList *ActionList1;
	TAction *ActNewItem;
	TAction *ActDelItem;
	TAction *ActEditItem;
	TImage *ImageLock;
	void __fastcall ActNewItemExecute(TObject *Sender);
	void __fastcall ActDelItemExecute(TObject *Sender);
	void __fastcall ActEditItemExecute(TObject *Sender);
	void __fastcall ListViewDblClick(TObject *Sender);
	void __fastcall ListViewExit(TObject *Sender);
	void __fastcall ListViewEnter(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall ListViewClick(TObject *Sender);

private:

	virtual void __fastcall SetRemarks(UnicodeString str) override {};
	virtual void __fastcall SetVariables(const GPersonProperties *prp) override {};
	virtual void __fastcall SetScript(UnicodeString KeyName, UnicodeString ScriptName) override {};
	virtual void __fastcall SetNode(UnicodeString NodeName) override {};
	virtual void __fastcall SetZone(__int32 zone) override {};
	virtual void __fastcall SetState(UnicodeString name) override {};
	virtual void __fastcall SetVarValue(double val, UnicodeString var_name) override {};
	virtual void __fastcall SetSizes(__int32 w, __int32 h) override {};

	virtual void __fastcall refreshNodes() override {};
	virtual void __fastcall refreshCplxList() override {};
	virtual void __fastcall refresStates() override {};
	virtual void __fastcall refreshVars() override {};
	virtual void __fastcall refreshScripts() override {};

	virtual void __fastcall SetNodeU(UnicodeString NodeName) override {};;
	virtual void __fastcall SetNodeD(UnicodeString NodeName) override {};
	virtual void __fastcall SetNodeL(UnicodeString NodeName) override {};
	virtual void __fastcall SetNodeR(UnicodeString NodeName) override {};
	virtual void __fastcall SetTrigger(UnicodeString triggerName) override {};
	virtual void __fastcall refreshTriggers() override {};

	void __fastcall SaveAllValues(TWinControl *iParent);
	void __fastcall FakeEnterActiveControl();

	void __fastcall CSEd_xChange(TObject *Sender);
	void __fastcall CSEd_xEnter(TObject *Sender);
	void __fastcall CSEd_xExit(TObject *Sender);

	void __fastcall AddEditDel(__int32 idx, __int32 mode);
	void __fastcall CheckActionsStatus();
	void __fastcall CreateList();
	void __fastcall FillListItem(TListItem *it, SoundSchemePropertyItem *i);

	TSpinEdit *CSEd_x;
	TSpinEdit *CSEd_y;
	UnicodeString mOldCSEd_x;
	bool disableChanges;
	TMapSoundScheme *t_MapGP;
	TCommonResObjManager *mpCommonResObjManager;

public:
	__fastcall TFSndObjProperty(TComponent* Owner) override;

	virtual void __fastcall Init(TCommonResObjManager *commonResObjManager) override;
	virtual void __fastcall Localize() override;
	virtual void __fastcall SetTransform(const TTransformProperties& transform) override;
	virtual void __fastcall Reset() override;
	virtual void __fastcall EmulatorMode(bool mode)  override;
	virtual void __fastcall Lock(bool value) override;

	void __fastcall AssignSoundSchemeObj(TMapSoundScheme *s);
	TMapSoundScheme* __fastcall GetAssignedObj(){return t_MapGP;}
};
//---------------------------------------------------------------------------
extern PACKAGE TFSndObjProperty *FSndObjProperty;
//---------------------------------------------------------------------------
#endif
