//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UObjPoolProperty.h"
#include "UObjPoolPropertyEdit.h"
#include "UObjCode.h"
#include "MainUnit.h"
#include "UObjManager.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFObjPoolProperty *FObjPoolProperty;
//---------------------------------------------------------------------------
__fastcall TFObjPoolProperty::TFObjPoolProperty(TComponent* Owner)
	: TPrpFunctions(Owner)
{
	CSEd_x = new TSpinEdit(GroupBoxPos);
	CSEd_x->Parent = GroupBoxPos;
	CSEd_x->Left = 16;
	CSEd_x->Top = 20;
	CSEd_x->Width = 55;
	CSEd_x->Height = 22;
	CSEd_x->TabOrder = 0;
	CSEd_x->OnChange = CSEd_xChange;
	CSEd_x->OnEnter = CSEd_xEnter;
	CSEd_x->OnExit = CSEd_xExit;
    CSEd_x->Visible = true;
	CSEd_y = new TSpinEdit(GroupBoxPos);
	CSEd_y->Parent = GroupBoxPos;
	CSEd_y->Left = 84;
	CSEd_y->Top = 20;
	CSEd_y->Width = 53;
	CSEd_y->Height = 22;
	CSEd_y->TabOrder = 1;
	CSEd_y->OnChange = CSEd_xChange;
	CSEd_y->OnEnter = CSEd_xEnter;
	CSEd_y->OnExit = CSEd_xExit;
	CSEd_y->Visible = true;

	mOldCSEd_x = _T("");
	t_MapGP = NULL;
	mpCommonResObjManager = NULL;
	disableChanges = true;
	NameObj = _T("");
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::Localize()
{
	Localization::Localize(this);
	Localization::Localize(ActionList1);
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::SaveAllValues(TWinControl *iParent)
{
  if(iParent == NULL)
	return;

  TControl *c;
  for(int i = 0; i < iParent->ControlCount; i++)
  {
	c = iParent->Controls[i];
	if(c == NULL)continue;

	if( c->ClassNameIs(_T("TPanel")) ||
		c->ClassNameIs(_T("TGroupBox")) ||
		c->ClassNameIs(_T("TForm")))
	{
		SaveAllValues((TWinControl*)c);
	}
	else
	{
		if(c->ClassNameIs(_T("TSpinEdit")))
		{
			if(((TSpinEdit*)c)->OnExit != NULL)
				((TSpinEdit*)c)->OnExit(c);
		}
		if(c->ClassNameIs(_T("TListView")))
		{
			if(((TListView*)c)->OnExit != NULL)
				((TListView*)c)->OnExit(c);
        }
	}
  }
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::FormHide(TObject *)
{
	mOldCSEd_x = _T("");
	SaveAllValues(this);
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::FormShow(TObject *)
{
	AssignObjPoolObj(t_MapGP);
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::FakeEnterActiveControl()
{
	TWinControl* c = ActiveControl;
	if(c == NULL)
	{
		return;
    }
	if(c->ClassNameIs(_T("TSpinEdit")))
	{
		((TSpinEdit*)c)->OnEnter(c);
		return;
	}
	if(c->ClassNameIs(_T("TListView")))
	{
		((TListView*)c)->OnEnter(c);
		return;
	}
	if (c->ClassNameIs(_T("TEdit")))
	{
		if (((TEdit*)c)->OnEnter != NULL)
		{
			((TEdit*)c)->OnEnter(c);
		}
		return;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::Init(TCommonResObjManager *commonResObjManager)
{
	mpCommonResObjManager = commonResObjManager;
	assert(mpCommonResObjManager);

	if(t_MapGP != NULL)
	{
		AssignObjPoolObj(t_MapGP);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::AssignObjPoolObj(TMapObjPool *sgp)
{
	if(t_MapGP == sgp)
	{
		return;
	}

	if(t_MapGP != NULL)
	{
		if(Visible)
		{
			mOldCSEd_x = _T("");
			SaveAllValues(this);
		}
		t_MapGP->UnregisterPrpWnd();
	}

	t_MapGP = sgp;
	if(t_MapGP != NULL)
	{
		t_MapGP->RegisterPrpWnd(this);
	}
	else
	{
		return;
	}

	Caption = Localization::Text(_T("mGameObjectProperties")) + _T(" [") + t_MapGP->getGParentName() + _T("]");

	disableChanges = true;

	Edit_name->Text = t_MapGP->Name;
	NameObj = t_MapGP->Name;
	mOldName = t_MapGP->Name;

    double x, y;
	sgp->getCoordinates(&x, &y);
	CSEd_x->Value = (__int32)x;
	CSEd_y->Value = (__int32)y;

    CreateList();

	disableChanges = false;

	CheckActionsStatus();

	Lock(t_MapGP->IsLocked());

	if(Visible)
	{
		FakeEnterActiveControl();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::CSEd_xChange(TObject *Sender)
{
  if(((TSpinEdit *)Sender)->Text.IsEmpty())return;
  try
  {
	StrToInt(((TSpinEdit *)Sender)->Text);
  }
  catch(...)
  {
	return;
  }

  if(!disableChanges)
	if(t_MapGP != NULL)
	  Form_m->OnChangeObjPosition(t_MapGP, CSEd_x->Value, CSEd_y->Value);
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::CSEd_xEnter(TObject *Sender)
{
	mOldCSEd_x = ((TSpinEdit *)Sender)->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::SetTransform(const TTransformProperties& transform)
{
	if(!Visible)
	{
		return;
	}
	double x, y;
	transform.GetPosition(x, y);
	CSEd_x->Value = static_cast<int>(x);
	CSEd_y->Value = static_cast<int>(y);
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::CSEd_xExit(TObject *Sender)
{
  UnicodeString str = ((TSpinEdit *)Sender)->Text.Trim();

  if(mOldCSEd_x.IsEmpty())
	return;

  if(mOldCSEd_x.Compare(str) != 0)
	if(t_MapGP != NULL && !disableChanges)
	  Form_m->OnChangeObjPosition(t_MapGP, CSEd_x->Value, CSEd_y->Value);
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::CheckActionsStatus()
{
	TListItem* si = ListView->Selected;
	ActEditItem->Enabled = si != NULL;
	ActDelItem->Enabled = si != NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::ActNewItemExecute(TObject *)
{
	AddEditDel(-1, mAdd);
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::ActDelItemExecute(TObject *)
{
	TListItem* si = ListView->Selected;
	if(si != NULL)
	{
		AddEditDel(si->Index, mDel);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::ActEditItemExecute(TObject *)
{
	TListItem* si = ListView->Selected;
	if(si != NULL)
	{
		AddEditDel(si->Index, mEdit);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::ListViewDblClick(TObject *)
{
	ActEditItem->Execute();
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::FormKeyDown(TObject *, WORD &, TShiftState)
{
	//
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::ListViewClick(TObject *)
{
	CheckActionsStatus();
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::CreateList()
{
    ListView->Items->Clear();
	if(t_MapGP == NULL)
	{
		return;
	}
	__int32 idx, ct;
	ct = t_MapGP->GetItemsCount();
	for(idx = 0; idx < ct; idx++)
	{
		ObjPoolPropertyItem i;
		t_MapGP->GetItemByIndex(idx, i);
		TListItem *it = ListView->Items->Add();
		FillListItem(it, &i);
	}
	if(ct > 0)
	{
		ListView->Items->Item[0]->Selected = true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::FillListItem(TListItem *it, ObjPoolPropertyItem *i)
{
    it->SubItems->Clear();
	it->Caption = i->mName;
	it->SubItems->Add(IntToStr((__int32)i->mCount));
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::AddEditDel(__int32 idx, __int32 mode)
{
	if(t_MapGP == NULL)
	{
		return;
	}
	bool res;
	UnicodeString oldName;
	__int32 oldCount = 0;
	switch(mode)
	{
		case mDel:
			t_MapGP->DeleteItem(ListView->Items->Item[idx]->Caption);
			ListView->Items->Delete(idx);
			ListViewExit(NULL);
		break;
		case mAdd:
		case mEdit:
			Application->CreateForm(__classid(TFOPPEdit), &FOPPEdit);
			FOPPEdit->Label_name->Caption = ListView->Columns->Items[0]->Caption;
			FOPPEdit->Label_count->Caption = ListView->Columns->Items[1]->Caption;
			if(mode == mEdit)
			{
				ObjPoolPropertyItem item;
				if(!t_MapGP->GetItemByIndex(idx, item))
				{
					FOPPEdit->Free();
					FOPPEdit = NULL;
					return;
				}
				oldName = item.mName;
				oldCount = (__int32)item.mCount;
				FOPPEdit->SetPoolName(item.mName);
				FOPPEdit->SetPoolCount(item.mCount);
			}
			do
			{
				res = true;
				if (FOPPEdit->ShowModal() == mrOk)
				{
					ObjPoolPropertyItem newitem;
					newitem.mName = FOPPEdit->GetPoolName();
					newitem.mCount = FOPPEdit->GetPoolCount();
					if (FOPPEdit->GetPoolName().IsEmpty())
					{
						FOPPEdit->ActiveControl = FOPPEdit->Ed_name;
						Application->MessageBox(Localization::Text(_T("mObjGroupTypeNameError")).c_str(),
												Localization::Text(_T("mError")).c_str(),
												MB_OK | MB_ICONERROR);
						res = false;
					}
					else
					{
						bool valid;
						__int32 count = Form_m->GetCurrentObjManager()->GetTotalMapObjectsCount();
						count -= oldCount;
						count += newitem.mCount;
						valid = count < MAX_CHARACTER_ON_MAP;
						if(!valid)
						{
							FOPPEdit->ActiveControl = FOPPEdit->Edit_count;
							const UnicodeString foerrmess = Localization::Text(_T("mMaximumObjOnMapExceeded"));
							Application->MessageBox(foerrmess.c_str(), Localization::Text(_T("mError")).c_str(), MB_OK | MB_ICONERROR);
							res = false;
                        }
					}
					if (res)
					{
						if (mode == mAdd)
						{
							TListItem *it = ListView->Items->Add();
							FillListItem(it, &newitem);
							t_MapGP->AddItem(newitem);
							ListViewExit(NULL);
						}
						else
						{
							TListItem *it = ListView->Items->Item[idx];
							FillListItem(it, &newitem);
							t_MapGP->ChangeItem(oldName, newitem);
							ListViewExit(NULL);
						}
					}
				}
				else
				{
					res = true;
				}
			}
			while(!res);
			FOPPEdit->Free();
			FOPPEdit = NULL;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::ListViewExit(TObject *)
{
	if (t_MapGP == NULL)
	{
		return;
	}
	if (!disableChanges && Visible)
	{
		Form_m->CreateZoneList();
    }
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::ListViewEnter(TObject *)
{
	//nope
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::BitBtn_find_objClick(TObject *)
{
	Form_m->FindAndShowObjectByName(NameObj, false);
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::Edit_nameChange(TObject *)
{
	Edit_nameChangeMain(Edit_name);
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::Edit_nameEnter(TObject *)
{
	mOldName = Edit_name->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::Edit_nameExit(TObject *)
{
	if (t_MapGP == NULL)
	{
		return;
    }

	UnicodeString str = Edit_name->Text.Trim();
	if(str.Compare(mOldName) == 0)
	{
		return;
	}


	if (str.IsEmpty())
	{
		Application->MessageBox(Localization::Text(_T("mObjNameError")).c_str(),
								Localization::Text(_T("mError")).c_str(),
								MB_OK | MB_ICONERROR);
		Edit_name->Text = mOldName;
		return;
	}
	if (Form_m->GetCurrentObjManager()->CheckIfMGONameExists(str, t_MapGP) == true)
	{
		Application->MessageBox(Localization::Text(_T("mObjNameExist")).c_str(),
								Localization::Text(_T("mError")).c_str(),
								MB_OK | MB_ICONERROR);
		Edit_name->Text = mOldName;
		return;
	}
	if (!disableChanges && mOldName.Compare(str) != 0)
	{
		t_MapGP->Name = str;
		NameObj = str;
		Form_m->OnChangeObjName(t_MapGP, mOldName);
		t_MapGP->changeHint();
		mOldName = str;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::Edit_nameKeyPress(TObject *, System::WideChar &Key)

{
	if (VK_RETURN == Key)
	{
		Key = 0;
		Edit_nameExit(NULL);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::ListViewKeyDown(TObject *, WORD &Key, TShiftState)
{
	if(Key == VK_RETURN)
	{
		ActEditItem->Execute();
		Key = 0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::ActionUpExecute(TObject* obj)
{
	TListItem* si = ListView->Selected;
	if(si != NULL)
	{
		__int32 idx = si->Index;
		idx = t_MapGP->MoveItem(idx, idx + (obj == ActionDown ? +1 : -1));
		if(idx >= 0)
		{
			__int32 ct;
			ListView->Enabled = false;
			ct = t_MapGP->GetItemsCount();
			for(int i = 0; i < ct; i++)
			{
				ObjPoolPropertyItem obj;
				TListItem *it = ListView->Items->Item[i];
				t_MapGP->GetItemByIndex(i, obj);
				FillListItem(it, &obj);
			}
			ListView->Items->Item[idx]->Selected = true;
			ListView->Enabled = true;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::EmulatorMode(bool mode)
{
	(void)mode;
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::Reset()
{
	AssignObjPoolObj(nullptr);
}
//---------------------------------------------------------------------------

void __fastcall TFObjPoolProperty::Lock(bool value)
{
	ImageLock->Visible = value;
}
//---------------------------------------------------------------------------

