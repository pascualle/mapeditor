object FBOProperty: TFBOProperty
  Left = 329
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'mTileObjectPropertiesCaption'
  ClientHeight = 357
  ClientWidth = 342
  Color = clBtnFace
  Constraints.MinHeight = 391
  Constraints.MinWidth = 350
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Pitch = fpFixed
  Font.Style = []
  KeyPreview = True
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  DesignSize = (
    342
    357)
  TextHeight = 13
  object GroupBox_type: TGroupBox
    Left = 6
    Top = 124
    Width = 326
    Height = 58
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'mSetTileObjectType'
    TabOrder = 1
    ExplicitTop = 123
    ExplicitWidth = 322
    DesignSize = (
      326
      58)
    object Bevel3: TBevel
      Left = 291
      Top = 21
      Width = 27
      Height = 21
      Anchors = [akTop, akRight]
      ExplicitLeft = 295
    end
    object CB_Type: TComboBox
      Left = 12
      Top = 21
      Width = 280
      Height = 21
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      OnExit = CB_TypeExit
      ExplicitWidth = 276
    end
    object Bt_3dot: TButton
      Left = 292
      Top = 22
      Width = 25
      Height = 19
      Anchors = [akTop, akRight]
      Caption = '?'
      TabOrder = 1
      OnClick = Bt_3dotClick
      ExplicitLeft = 288
    end
  end
  object ListView: TListView
    Left = 8
    Top = 31
    Width = 326
    Height = 87
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <>
    HideSelection = False
    IconOptions.AutoArrange = True
    LargeImages = ImageList
    ReadOnly = True
    ParentShowHint = False
    PopupMenu = PopupMenu
    ShowHint = False
    TabOrder = 0
    OnClick = ListViewClick
    OnDblClick = ListViewDblClick
    OnEnter = ListViewEnter
    OnExit = ListViewExit
    OnKeyDown = ListViewKeyDown
    ExplicitWidth = 322
    ExplicitHeight = 86
  end
  object ButtonOk: TButton
    Left = 167
    Top = 309
    Width = 86
    Height = 25
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'mApply'
    ModalResult = 1
    TabOrder = 3
    ExplicitTop = 308
    ExplicitWidth = 82
  end
  object Button2: TButton
    Left = 85
    Top = 309
    Width = 76
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 4
    ExplicitTop = 308
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 338
    Width = 342
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    ExplicitTop = 337
    ExplicitWidth = 338
  end
  object GroupBox: TGroupBox
    Left = 8
    Top = 185
    Width = 326
    Height = 119
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'mMemo'
    TabOrder = 2
    ExplicitTop = 184
    ExplicitWidth = 322
    DesignSize = (
      326
      119)
    object Memo: TMemo
      Left = 10
      Top = 16
      Width = 306
      Height = 91
      Anchors = [akLeft, akTop, akRight]
      MaxLength = 200
      ScrollBars = ssVertical
      TabOrder = 0
      WordWrap = False
      OnExit = MemoExit
      ExplicitWidth = 302
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 342
    Height = 29
    Caption = 'ToolBar1'
    Images = Form_m.ImageList1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 6
    ExplicitWidth = 338
    object ToolButton3: TToolButton
      Left = 0
      Top = 0
      Action = ActEdit
    end
    object ToolButton2: TToolButton
      Left = 23
      Top = 0
      Action = ActShiftRight
    end
    object ToolButton1: TToolButton
      Left = 46
      Top = 0
      Action = ActShiftLeft
    end
  end
  object PopupMenu: TPopupMenu
    Images = Form_m.ImageList1
    Left = 24
    Top = 56
    object NEdit: TMenuItem
      Action = ActEdit
      Caption = 'mChange'
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object NSRight: TMenuItem
      Action = ActShiftRight
    end
    object NSLeft: TMenuItem
      Action = ActShiftLeft
    end
  end
  object ActionList: TActionList
    Images = Form_m.ImageList1
    Left = 72
    Top = 56
    object ActEdit: TAction
      Caption = 'mEdit'
      Hint = 'mEdit'
      ImageIndex = 7
      ShortCut = 16453
      OnExecute = ActEditExecute
    end
    object ActShiftRight: TAction
      Caption = 'mShiftRight'
      Hint = 'mShiftRight'
      ImageIndex = 31
      OnExecute = ActShiftRightExecute
    end
    object ActShiftLeft: TAction
      Caption = 'mShiftLeft'
      Hint = 'mShiftLeft'
      ImageIndex = 30
      OnExecute = ActShiftLeftExecute
    end
  end
  object ImageList: TImageList
    ColorDepth = cd32Bit
    Height = 48
    Masked = False
    Width = 48
    Left = 120
    Top = 56
  end
end
