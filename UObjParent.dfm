object FObjParent: TFObjParent
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'mGameObject'
  ClientHeight = 594
  ClientWidth = 698
  Color = clBtnFace
  Constraints.MinHeight = 360
  Constraints.MinWidth = 520
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnMouseDown = FormMouseDown
  OnMouseMove = FormMouseMove
  OnMouseUp = FormMouseUp
  OnResize = FormResize
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 517
    Top = 54
    Height = 371
    Align = alRight
    AutoSnap = False
    Beveled = True
    MinSize = 100
    ResizeStyle = rsUpdate
    OnCanResize = Splitter1CanResize
    ExplicitLeft = 519
    ExplicitHeight = 350
  end
  object PanelAnimation: TPanel
    Left = 0
    Top = 425
    Width = 698
    Height = 120
    Align = alBottom
    BevelOuter = bvNone
    DoubleBuffered = True
    FullRepaint = False
    ParentDoubleBuffered = False
    TabOrder = 4
    ExplicitTop = 426
    ExplicitWidth = 702
    DesignSize = (
      698
      120)
    object CBAutorepeat: TCheckBox
      Left = 8
      Top = 102
      Width = 100
      Height = 21
      Caption = 'mAutorepeat'
      Checked = True
      State = cbChecked
      TabOrder = 9
    end
    object BtPlay: TBitBtn
      Left = 92
      Top = 73
      Width = 30
      Height = 30
      Hint = 'mStart'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        0800000000000001000000000000000000000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FD0000FDFDFD
        FDFDFDFDFDFDFDFDFDFDFD00000000FDFDFDFDFDFDFDFDFDFDFDFD0000000000
        00FDFDFDFDFDFDFDFDFDFD0000000000000000FDFDFDFDFDFDFDFD0000000000
        0000000000FDFDFDFDFDFD000000000000000000000000FDFDFDFD0000000000
        000000000000000000FDFD000000000000000000000000000000FD0000000000
        00000000000000000000FD0000000000000000000000000000FDFD0000000000
        00000000000000FDFDFDFD00000000000000000000FDFDFDFDFDFD0000000000
        000000FDFDFDFDFDFDFDFD000000000000FDFDFDFDFDFDFDFDFDFD00000000FD
        FDFDFDFDFDFDFDFDFDFDFD0000FDFDFDFDFDFDFDFDFDFDFDFDFD}
      Layout = blGlyphBottom
      ParentShowHint = False
      ShowHint = True
      Spacing = 1
      TabOrder = 7
      OnClick = BtPlayClick
    end
    object BtFrPrev: TBitBtn
      Left = 8
      Top = 76
      Width = 24
      Height = 24
      Hint = 'mPrevFrameButton'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        0800000000000001000000000000000000000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FDFDFDFDFDFD
        FDFDFDFDFDFDFDFDFDFDFDFDFDA4A4A4A4A4A4A4A4A4FD0000FDFDFDFDA407A4
        070707A407A4000000FDFDFDFDA400A4070707A400A4000000FDFDFDFDA407A4
        070707A407A4000000FDFDFDFDA400A4070707A400A4000000FDFDFDFDA407A4
        070707A407A4000000FDFD0000A4A4A4A4A4A4A4A4A4000000FDFD0000000000
        000000000000000000FDFDFDFD000000000000000000000000FDFDFDFDFDFD00
        000000000000000000FDFDFDFDFDFDFDFD0000000000000000FDFDFDFDFDFDFD
        FDFDFD000000000000FDFDFDFDFDFDFDFDFDFDFDFD00000000FDFDFDFDFDFDFD
        FDFDFDFDFDFDFD0000FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD}
      Layout = blGlyphBottom
      ParentShowHint = False
      ShowHint = True
      Spacing = 1
      TabOrder = 4
      OnClick = BtFrPrevClick
    end
    object BtBack: TBitBtn
      Left = 32
      Top = 73
      Width = 30
      Height = 30
      Hint = 'mStartRW'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        0800000000000001000000000000000000000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FDFDFDFDFDFD
        FDFDFDFDFDFDFD0000FDFDFDFDFDFDFDFDFDFDFDFD00000000FDFDFDFDFDFDFD
        FDFDFD000000000000FDFDFDFDFDFDFDFD0000000000000000FDFDFDFDFDFD00
        000000000000000000FDFDFDFD000000000000000000000000FDFD0000000000
        000000000000000000FD000000000000000000000000000000FD000000000000
        000000000000000000FDFD0000000000000000000000000000FDFDFDFD000000
        000000000000000000FDFDFDFDFDFD00000000000000000000FDFDFDFDFDFDFD
        FD0000000000000000FDFDFDFDFDFDFDFDFDFD000000000000FDFDFDFDFDFDFD
        FDFDFDFDFD00000000FDFDFDFDFDFDFDFDFDFDFDFDFDFD0000FD}
      Layout = blGlyphBottom
      ParentShowHint = False
      ShowHint = True
      Spacing = 1
      TabOrder = 5
      OnClick = BtBackClick
    end
    object BtFrNext: TBitBtn
      Left = 122
      Top = 76
      Width = 24
      Height = 24
      Hint = 'mNextFrameButton'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        0800000000000001000000000000000000000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FDFDFDFDFDFD
        FDFDFDFDFDFDFDFDFDFDFD0000FDA4A4A4A4A4A4A4A4A4FDFDFDFD000000A407
        A4070707A407A4FDFDFDFD000000A400A4070707A400A4FDFDFDFD000000A407
        A4070707A407A4FDFDFDFD000000A400A4070707A400A4FDFDFDFD000000A407
        A4070707A407A4FDFDFDFD000000A4A4A4A4A4A4A4A4A40000FDFD0000000000
        000000000000000000FDFD000000000000000000000000FDFDFDFD0000000000
        0000000000FDFDFDFDFDFD0000000000000000FDFDFDFDFDFDFDFD0000000000
        00FDFDFDFDFDFDFDFDFDFD00000000FDFDFDFDFDFDFDFDFDFDFDFD0000FDFDFD
        FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD}
      Layout = blGlyphBottom
      ParentShowHint = False
      ShowHint = True
      Spacing = 1
      TabOrder = 8
      OnClick = BtFrNextClick
    end
    object BtStop: TBitBtn
      Left = 62
      Top = 73
      Width = 30
      Height = 30
      Hint = 'mStop'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        0800000000000001000000000000000000000001000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FDFDFDFDFDFD
        FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD
        FDFDFDFDFDFDFDFDFDFDFDFDFD00000000000000000000FDFDFDFDFDFD000000
        00000000000000FDFDFDFDFDFD00000000000000000000FDFDFDFDFDFD000000
        00000000000000FDFDFDFDFDFD00000000000000000000FDFDFDFDFDFD000000
        00000000000000FDFDFDFDFDFD00000000000000000000FDFDFDFDFDFD000000
        00000000000000FDFDFDFDFDFD00000000000000000000FDFDFDFDFDFD000000
        00000000000000FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD
        FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD}
      Layout = blGlyphBottom
      ParentShowHint = False
      ShowHint = True
      Spacing = 1
      TabOrder = 6
      OnClick = BtStopClick
    end
    object ComboBoxScale: TComboBox
      Left = 242
      Top = 97
      Width = 71
      Height = 21
      Hint = 'mScale'
      Style = csDropDownList
      ItemIndex = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 11
      Text = 'x1'
      OnChange = ComboBoxScaleChange
      Items.Strings = (
        'x0.125'
        'x0.25'
        'x0.5'
        'x1'
        'x2'
        'x3'
        'x4'
        'x5'
        'x6'
        'x7'
        'x8')
    end
    object CB_AniType: TComboBox
      Left = 135
      Top = 3
      Width = 351
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      CharCase = ecUpperCase
      TabOrder = 1
      OnChange = CB_AniTypeChange
      OnEnter = CB_AniTypeEnter
      OnExit = CB_AniTypeExit
      OnKeyPress = CB_AniTypeKeyPress
    end
    object FrameGrid: TDrawGrid
      Left = 8
      Top = 28
      Width = 502
      Height = 42
      Anchors = [akLeft, akTop, akRight]
      Color = clBtnFace
      ColCount = 1
      DefaultColWidth = 10
      DefaultRowHeight = 20
      DrawingStyle = gdsClassic
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected]
      PopupMenu = PopupMenuGrid
      ScrollBars = ssHorizontal
      TabOrder = 3
      OnDrawCell = FrameGridDrawCell
      OnKeyUp = FrameGridKeyUp
      OnMouseDown = FrameGridMouseDown
      OnMouseMove = FrameGridMouseMove
      OnMouseUp = FrameGridMouseUp
      OnSelectCell = FrameGridSelectCell
      ExplicitWidth = 510
      ColWidths = (
        10)
      RowHeights = (
        20)
    end
    object CBViewMode: TComboBox
      Left = 242
      Top = 74
      Width = 215
      Height = 21
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      ItemIndex = 0
      TabOrder = 10
      Text = 'mCurrentFrame'
      OnChange = CBViewModeChange
      Items.Strings = (
        'mCurrentFrame'
        'mShowSelectedFragmentFromAllFrames'
        'mShowAllFragmentsFromAllFrames')
    end
    object StaticText2: TStaticText
      Left = 186
      Top = 78
      Width = 50
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'mView'
      TabOrder = 12
    end
    object LabelCount: TStaticText
      Left = 488
      Top = 70
      Width = 22
      Height = 17
      Anchors = [akTop, akRight]
      AutoSize = False
      Caption = '999'
      TabOrder = 13
      ExplicitLeft = 496
    end
    object GroupBox1: TGroupBox
      Left = 514
      Top = 0
      Width = 176
      Height = 120
      Anchors = [akTop, akRight]
      Caption = 'mCurrentFrame'
      TabOrder = 14
      ExplicitLeft = 522
      DesignSize = (
        176
        120)
      object StaticText5: TStaticText
        Left = 15
        Top = 35
        Width = 87
        Height = 17
        Caption = 'mFragmentCount'
        TabOrder = 0
      end
      object LabelFrafments: TStaticText
        Left = 132
        Top = 33
        Width = 22
        Height = 17
        Caption = '999'
        TabOrder = 1
      end
      object StaticText3: TStaticText
        Left = 15
        Top = 18
        Width = 49
        Height = 17
        Caption = 'mNumber'
        TabOrder = 2
      end
      object LabelCurrent: TStaticText
        Left = 132
        Top = 16
        Width = 22
        Height = 17
        Caption = '999'
        TabOrder = 3
      end
      object Edit_duration: TEdit
        Left = 129
        Top = 49
        Width = 40
        Height = 21
        MaxLength = 4
        NumbersOnly = True
        TabOrder = 4
        Text = '9999'
        OnExit = Edit_durationExit
        OnKeyPress = Edit_durationKeyPress
      end
      object StaticText1: TStaticText
        Left = 15
        Top = 53
        Width = 64
        Height = 17
        Caption = 'mFrameTime'
        TabOrder = 5
      end
      object StaticText7: TStaticText
        Left = 15
        Top = 73
        Width = 40
        Height = 17
        Caption = 'mEvent'
        TabOrder = 6
      end
      object ComboBox_Event: TComboBox
        Left = 8
        Top = 89
        Width = 161
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        CharCase = ecUpperCase
        TabOrder = 7
        OnChange = ComboBox_EventChange
        OnEnter = CB_AniTypeEnter
        OnExit = CB_AniTypeExit
        OnKeyPress = CB_AniTypeKeyPress
      end
    end
    object EditFilter: TEdit
      Left = 8
      Top = 3
      Width = 121
      Height = 21
      TabOrder = 0
      TextHint = 'mFilter'
      OnChange = EditFilterChange
    end
    object StaticText4: TStaticText
      Left = 186
      Top = 100
      Width = 50
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'mScale'
      TabOrder = 15
    end
    object BtAddNewAnimationType: TBitBtn
      Left = 486
      Top = 1
      Width = 24
      Height = 24
      Action = ActCreateNewAnimType
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      ExplicitLeft = 494
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 575
    Width = 698
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    ParentFont = True
    UseSystemFont = False
    ExplicitTop = 576
    ExplicitWidth = 702
  end
  object TabSet: TTabSet
    Left = 0
    Top = 0
    Width = 698
    Height = 28
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    SoftTop = True
    Style = tsModernTabs
    TabHeight = 24
    Tabs.Strings = (
      'mGlobalCommon'
      'mAnimationEditor')
    TabIndex = 0
    TabPosition = tpTop
    OnClick = TabSetClick
    ExplicitWidth = 702
  end
  object PanelMain: TPanel
    Left = 4
    Top = 54
    Width = 515
    Height = 340
    BevelOuter = bvNone
    DoubleBuffered = False
    FullRepaint = False
    ParentDoubleBuffered = False
    ShowCaption = False
    TabOrder = 1
    DesignSize = (
      515
      340)
    object Image: TImage
      Left = 365
      Top = 164
      Width = 11
      Height = 10
      Hint = 'mImageStateHint'
      ParentShowHint = False
      ShowHint = True
      Transparent = True
    end
    object Label4: TLabel
      Left = 16
      Top = 161
      Width = 81
      Height = 13
      Caption = 'mStateByDefault'
    end
    object LabelGrOptHint: TLabel
      Left = 32
      Top = 214
      Width = 454
      Height = 43
      AutoSize = False
      Caption = 'mAllowGraphicsOptimizationOnHint'
      WordWrap = True
    end
    object GroupBox_name: TGroupBox
      Left = 8
      Top = 8
      Width = 281
      Height = 49
      Anchors = [akLeft, akTop, akRight]
      Caption = 'mNamelatinOnly'
      TabOrder = 0
      DesignSize = (
        281
        49)
      object Edit_name: TEdit
        Left = 8
        Top = 17
        Width = 264
        Height = 21
        Anchors = [akLeft, akTop, akRight, akBottom]
        MaxLength = 64
        TabOrder = 0
        OnChange = Edit_nameChange
      end
    end
    object GroupBox_Gr: TGroupBox
      Left = 378
      Top = 1
      Width = 117
      Height = 123
      Anchors = [akTop, akRight]
      Caption = 'mIcon'
      TabOrder = 6
      object Image1: TImage
        Left = 8
        Top = 15
        Width = 100
        Height = 100
      end
    end
    object CheckBox_decoration: TCheckBox
      Left = 16
      Top = 61
      Width = 313
      Height = 18
      Caption = 'mUseThisObjTypeAsDecoration'
      TabOrder = 1
      OnClick = CheckBox_decorationClick
    end
    object Bt_propdef: TButton
      Left = 16
      Top = 114
      Width = 356
      Height = 25
      Caption = 'mEditTemplatePrpOfThisType'
      TabOrder = 3
      OnClick = Bt_propdefClick
    end
    object CheckBox_graphics: TCheckBox
      Left = 16
      Top = 81
      Width = 313
      Height = 18
      Hint = 'mDecorTypeCaption'
      Caption = 'mDontUseGraphicsForThisObjType'
      TabOrder = 2
      OnClick = CheckBox_graphicsClick
    end
    object CheckBox_res: TCheckBox
      Left = 16
      Top = 198
      Width = 356
      Height = 16
      Caption = 'mAllowGraphicsOptimization'
      Checked = True
      State = cbChecked
      TabOrder = 5
      OnClick = CheckBox_resClick
    end
    object CBDefState: TComboBox
      Left = 158
      Top = 158
      Width = 198
      Height = 21
      Style = csDropDownList
      TabOrder = 4
    end
  end
  object PanelLeft: TPanel
    Left = 520
    Top = 54
    Width = 178
    Height = 371
    Align = alRight
    BevelEdges = [beLeft, beBottom]
    BevelKind = bkFlat
    BevelOuter = bvNone
    DoubleBuffered = False
    FullRepaint = False
    ParentDoubleBuffered = False
    ShowCaption = False
    TabOrder = 2
    ExplicitLeft = 528
    ExplicitHeight = 372
    object CategoryPanelGroup: TCategoryPanelGroup
      Left = 4
      Top = 24
      Width = 72
      Height = 342
      VertScrollBar.Tracking = True
      Align = alNone
      BevelInner = bvNone
      BevelOuter = bvNone
      DoubleBuffered = False
      Ctl3D = False
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWindowText
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = []
      HeaderHeight = 16
      ParentCtl3D = False
      ParentDoubleBuffered = False
      TabOrder = 2
      object CP3: TCategoryPanel
        Top = 206
        Height = 69
        Hint = 'mNodeHint'
        FullRepaint = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        object RBNode1: TCheckBox
          Left = 0
          Top = 0
          Width = 68
          Height = 17
          Align = alTop
          Caption = 'mNode'
          TabOrder = 0
          OnClick = RBCollideRectClick
        end
        object VLENode1: TValueListEditor
          Left = 0
          Top = 17
          Width = 68
          Height = 34
          Align = alClient
          BorderStyle = bsNone
          DefaultRowHeight = 16
          DisplayOptions = [doAutoColResize, doKeyColFixed]
          DrawingStyle = gdsClassic
          FixedCols = 1
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goEditing]
          ScrollBars = ssNone
          Strings.Strings = (
            'X=0'
            'Y=0')
          TabOrder = 1
          OnDrawCell = VLECollideDrawCell
          OnKeyPress = VLECollideKeyPress
          OnValidate = VLECollideValidate
          ColWidths = (
            29
            37)
          RowHeights = (
            16
            16)
        end
      end
      object CP2: TCategoryPanel
        Top = 103
        Height = 103
        Hint = 'mViewRect'
        FullRepaint = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        object RBViewRect: TCheckBox
          Left = 0
          Top = 0
          Width = 68
          Height = 17
          Align = alTop
          Caption = 'mViewRect'
          TabOrder = 0
          OnClick = RBCollideRectClick
        end
        object VLEView: TValueListEditor
          Left = 0
          Top = 17
          Width = 68
          Height = 68
          Align = alClient
          BorderStyle = bsNone
          DefaultRowHeight = 16
          DisplayOptions = [doAutoColResize, doKeyColFixed]
          DrawingStyle = gdsClassic
          FixedCols = 1
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goEditing]
          ScrollBars = ssNone
          Strings.Strings = (
            'X=0'
            'Y=0'
            'W=0'
            'H=0')
          TabOrder = 1
          OnDrawCell = VLECollideDrawCell
          OnKeyPress = VLECollideKeyPress
          OnValidate = VLECollideValidate
          ColWidths = (
            29
            37)
          RowHeights = (
            16
            16
            16
            16)
        end
      end
      object CP1: TCategoryPanel
        Top = 0
        Height = 103
        Hint = 'mCollideRect'
        Color = clHighlight
        FullRepaint = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        object RBCollideRect: TCheckBox
          Left = 0
          Top = 0
          Width = 68
          Height = 17
          Align = alTop
          Caption = 'mCollideRect'
          TabOrder = 0
          OnClick = RBCollideRectClick
        end
        object VLECollide: TValueListEditor
          Left = 0
          Top = 17
          Width = 68
          Height = 68
          Align = alClient
          BorderStyle = bsNone
          DefaultRowHeight = 16
          DisplayOptions = [doAutoColResize, doKeyColFixed]
          DrawingStyle = gdsClassic
          FixedCols = 1
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goEditing]
          ScrollBars = ssNone
          Strings.Strings = (
            'X=0'
            'Y=0'
            'W=0'
            'H=0')
          TabOrder = 1
          OnDrawCell = VLECollideDrawCell
          OnKeyPress = VLECollideKeyPress
          OnValidate = VLECollideValidate
          ColWidths = (
            28
            38)
          RowHeights = (
            16
            16
            16
            16)
        end
      end
    end
    object CategoryButtons_s: TCategoryButtons
      Left = 40
      Top = 27
      Width = 67
      Height = 342
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      ButtonFlow = cbfVertical
      ButtonHeight = 48
      ButtonWidth = 48
      ButtonOptions = [boShowCaptions, boCaptionOnlyBorder]
      Categories = <>
      DoubleBuffered = False
      HotButtonColor = 14150380
      Images = ImageList
      ParentDoubleBuffered = False
      PopupMenu = PopupMenu
      RegularButtonColor = 15660791
      SelectedButtonColor = 15717318
      ShowHint = True
      TabOrder = 0
      OnDrawIcon = CategoryButtons_sDrawIcon
      OnEnter = CategoryButtons_sEnter
      OnExit = CategoryButtons_sExit
      OnSelectedItemChange = CategoryButtons_sSelectedItemChange
      OnSelectedCategoryChange = CategoryButtons_sSelectedCategoryChange
    end
    object TabSetGraphicsMode: TTabSet
      Left = 0
      Top = 21
      Width = 176
      Height = 21
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      SoftTop = True
      Style = tsModernTabs
      Tabs.Strings = (
        'mAnimTypeRegular'
        'mAnimTypeStreaming')
      TabIndex = 0
      TabPosition = tpTop
      OnClick = TabSetGraphicsModeClick
    end
    object Panel_strm: TPanel
      Left = 70
      Top = 48
      Width = 80
      Height = 262
      BevelOuter = bvNone
      Ctl3D = True
      ParentColor = True
      ParentCtl3D = False
      ShowCaption = False
      TabOrder = 3
      object Panel1: TPanel
        Left = 0
        Top = 90
        Width = 80
        Height = 139
        Align = alClient
        BevelOuter = bvNone
        ShowCaption = False
        TabOrder = 1
        DesignSize = (
          80
          139)
        object Label5: TLabel
          Left = 6
          Top = 0
          Width = 24
          Height = 13
          Caption = 'mFile'
        end
        object FilesList: TListView
          Left = 6
          Top = 14
          Width = 70
          Height = 123
          Anchors = [akLeft, akTop, akRight, akBottom]
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Columns = <
            item
            end>
          ColumnClick = False
          HideSelection = False
          IconOptions.WrapText = False
          MultiSelect = True
          ReadOnly = True
          RowSelect = True
          ShowColumnHeaders = False
          SmallImages = Form_m.ImageList1
          TabOrder = 0
          ViewStyle = vsReport
          OnClick = FoldersListClick
          OnResize = FoldersListResize
        end
      end
      object Panel0: TPanel
        Left = 0
        Top = 0
        Width = 80
        Height = 90
        Align = alTop
        BevelOuter = bvNone
        ShowCaption = False
        TabOrder = 0
        DesignSize = (
          80
          90)
        object Label1: TLabel
          Left = 6
          Top = 1
          Width = 38
          Height = 13
          Caption = 'mFolder'
        end
        object FoldersList: TListView
          Left = 6
          Top = 15
          Width = 70
          Height = 75
          Anchors = [akLeft, akTop, akRight]
          BevelInner = bvNone
          BevelOuter = bvNone
          BorderStyle = bsNone
          Columns = <
            item
            end>
          ColumnClick = False
          HideSelection = False
          IconOptions.WrapText = False
          ReadOnly = True
          RowSelect = True
          ShowColumnHeaders = False
          SmallImages = Form_m.ImageList1
          SortType = stText
          TabOrder = 0
          ViewStyle = vsReport
          OnClick = FoldersListClick
          OnResize = FoldersListResize
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 229
        Width = 80
        Height = 33
        Align = alBottom
        BevelOuter = bvNone
        FullRepaint = False
        ShowCaption = False
        TabOrder = 2
        DesignSize = (
          80
          33)
        object Button_AutoCreateStrmAnim: TButton
          Left = 3
          Top = 2
          Width = 74
          Height = 25
          Anchors = [akLeft, akTop, akRight]
          Caption = 'mAutocreateAnimation'
          TabOrder = 0
          OnClick = Button_AutoCreateStrmAnimClick
        end
      end
    end
    object VLEObj: TValueListEditor
      Left = 0
      Top = 280
      Width = 176
      Height = 89
      Align = alBottom
      Ctl3D = True
      DefaultColWidth = 78
      DefaultRowHeight = 16
      DisplayOptions = [doAutoColResize, doKeyColFixed]
      DrawingStyle = gdsClassic
      FixedCols = 1
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goEditing]
      ParentCtl3D = False
      PopupMenu = PopupMenuXYWH
      ScrollBars = ssNone
      Strings.Strings = (
        'X=0'
        'Y=0'
        'W=0'
        'H=0'
        'mTransparencyText=255')
      TabOrder = 5
      OnDrawCell = VLECollideDrawCell
      OnKeyPress = VLECollideKeyPress
      OnValidate = VLECollideValidate
      ExplicitTop = 281
      ColWidths = (
        78
        92)
      RowHeights = (
        16
        16
        16
        16
        16)
    end
    object TabSetAniMode: TTabSet
      Left = 0
      Top = 0
      Width = 176
      Height = 21
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      SoftTop = True
      Style = tsModernTabs
      Tabs.Strings = (
        'mGraphics'
        'mLogic')
      TabIndex = 0
      TabPosition = tpTop
      OnClick = TabSetGraphicsModeClick
    end
  end
  object FakeControl: TEdit
    Left = 8
    Top = 377
    Width = 17
    Height = 21
    TabOrder = 0
  end
  object ToolBar: TToolBar
    Left = 0
    Top = 28
    Width = 698
    Height = 26
    EdgeBorders = [ebBottom]
    Images = Form_m.ImageList1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 6
    Transparent = False
    ExplicitWidth = 702
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Action = ActObjAdd
    end
    object ToolButton3: TToolButton
      Left = 23
      Top = 0
      Action = ActObjEdit
    end
    object ToolButton2: TToolButton
      Left = 46
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 54
      Top = 0
      Action = ActObjFindParent
    end
    object ToolButton6: TToolButton
      Left = 77
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 85
      Top = 0
      Action = ActObjLevelUp
    end
    object ToolButton7: TToolButton
      Left = 108
      Top = 0
      Action = ActObjLevelDown
    end
    object ToolButton9: TToolButton
      Left = 131
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 31
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 139
      Top = 0
      Action = ActSelectAll
    end
    object ToolButton10: TToolButton
      Left = 162
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 26
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 170
      Top = 0
      Action = ActObjCopy
    end
    object ToolButton12: TToolButton
      Left = 193
      Top = 0
      Action = ActObjPaste
    end
    object ToolButton13: TToolButton
      Left = 216
      Top = 0
      Width = 8
      Caption = 'ToolButton13'
      ImageIndex = 34
      Style = tbsSeparator
    end
    object ToolButton16: TToolButton
      Left = 224
      Top = 0
      Action = ActShowRulers
    end
    object ToolButton17: TToolButton
      Left = 247
      Top = 0
      Action = ActAniAndLogic
    end
    object ToolButton15: TToolButton
      Left = 270
      Top = 0
      Action = ActBGColor
      OnClick = ToolButton15Click
    end
    object ToolButton19: TToolButton
      Left = 293
      Top = 0
      Width = 8
      Caption = 'ToolButton19'
      ImageIndex = 27
      Style = tbsSeparator
    end
    object ToolButton14: TToolButton
      Left = 301
      Top = 0
      Action = ActCreateIcon
    end
  end
  object PanelBt: TPanel
    Left = 0
    Top = 545
    Width = 698
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    DoubleBuffered = True
    FullRepaint = False
    ParentDoubleBuffered = False
    TabOrder = 7
    ExplicitTop = 546
    ExplicitWidth = 702
    DesignSize = (
      698
      30)
    object Button1: TButton
      Left = 224
      Top = 2
      Width = 329
      Height = 25
      Anchors = [akLeft, akRight, akBottom]
      Caption = 'mApply'
      ModalResult = 1
      TabOrder = 0
      ExplicitWidth = 337
    end
    object Button2: TButton
      Left = 142
      Top = 2
      Width = 76
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'mCancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object PaintBoxGL: TPanel
    Left = 0
    Top = 53
    Width = 88
    Height = 49
    ParentCustomHint = False
    BevelOuter = bvNone
    BiDiMode = bdLeftToRight
    Ctl3D = False
    UseDockManager = False
    DoubleBuffered = False
    FullRepaint = False
    ParentBiDiMode = False
    ParentBackground = False
    ParentCtl3D = False
    ParentDoubleBuffered = False
    ParentShowHint = False
    ShowCaption = False
    ShowHint = False
    TabOrder = 8
    OnMouseDown = FormMouseDown
    OnMouseMove = FormMouseMove
    OnMouseUp = FormMouseUp
    object ErrorGLText: TStaticText
      Left = 16
      Top = 8
      Width = 62
      Height = 17
      ParentCustomHint = False
      BevelInner = bvNone
      BevelOuter = bvNone
      BiDiMode = bdLeftToRight
      Caption = 'ErrorGLText'
      Color = clWhite
      DoubleBuffered = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBiDiMode = False
      ParentColor = False
      ParentDoubleBuffered = False
      ParentFont = False
      ParentShowHint = False
      ShowAccelChar = False
      ShowHint = False
      TabOrder = 0
      Transparent = False
    end
  end
  object ImageList: TImageList
    ColorDepth = cd32Bit
    Height = 48
    Masked = False
    Width = 48
    Left = 160
    Top = 328
  end
  object ActionList1: TActionList
    Images = Form_m.ImageList1
    Left = 88
    Top = 280
    object ActSelectAll: TAction
      Category = 'edit'
      Caption = 'mSelectAll'
      Hint = 'mSelectAll'
      ImageIndex = 25
      ShortCut = 16449
      OnExecute = ActSelectAllExecute
    end
    object ActDeselectAll: TAction
      Category = 'edit'
      Caption = 'mDeselect'
      Hint = 'mDeselect'
      ShortCut = 16452
      OnExecute = ActDeselectAllExecute
    end
    object ActObjCopy: TAction
      Category = 'edit'
      Caption = 'mCopy'
      Hint = 'mCopy'
      ImageIndex = 34
      OnExecute = ActObjCopyExecute
    end
    object ActObjPaste: TAction
      Category = 'edit'
      Caption = 'mPaste'
      Hint = 'mPaste'
      ImageIndex = 33
      OnExecute = ActObjPasteExecute
    end
    object ActEdit: TAction
      Category = 'obj'
      Caption = 'mEdit'
      Hint = 'mEdit'
      ShortCut = 16453
      OnExecute = ActEditExecute
    end
    object ActAddEmptyFrame: TAction
      Category = 'grid'
      Caption = 'mAddEmptyFrame'
      Hint = 'mAddEmptyFrame'
      ImageIndex = 0
      OnExecute = ActAddEmptyFrameExecute
    end
    object ActDelFrame: TAction
      Category = 'grid'
      Caption = 'mDeleteFrame'
      Hint = 'mDeleteFrame'
      ImageIndex = 6
      OnExecute = ActDelFrameExecute
    end
    object ActAddCopyFrame: TAction
      Category = 'grid'
      Caption = 'mCloneFrame'
      Hint = 'mCloneFrame'
      ImageIndex = 34
      OnExecute = ActAddCopyFrameExecute
    end
    object ActClearFrame: TAction
      Category = 'grid'
      Caption = 'mClearFrame'
      Hint = 'mClearFrame'
      ImageIndex = 0
      OnExecute = ActClearFrameExecute
    end
    object ActObjAdd: TAction
      Category = 'edit'
      Caption = 'mAddFragmentToCanvas'
      Hint = 'mAddFragmentToCanvas'
      ImageIndex = 5
      ShortCut = 16429
      OnExecute = ActObjAddExecute
    end
    object ActObjDelete: TAction
      Category = 'edit'
      Caption = 'mDoDelete'
      Hint = 'mDoDelete'
      ImageIndex = 6
      ShortCut = 16430
      OnExecute = ActObjDeleteExecute
    end
    object ActObjEdit: TAction
      Category = 'edit'
      Caption = 'mEdit'
      Hint = 'mEdit'
      ImageIndex = 7
      ShortCut = 49221
      OnExecute = ActObjEditExecute
    end
    object ActObjLevelUp: TAction
      Category = 'edit'
      Caption = 'mMoveToLayerUp'
      Hint = 'mMoveToLayerUp'
      ImageIndex = 32
      ShortCut = 16417
      OnExecute = ActObjLevelUpExecute
    end
    object ActObjLevelDown: TAction
      Category = 'edit'
      Caption = 'mMoveToLayerDown'
      Hint = 'mMoveToLayerDown'
      ImageIndex = 29
      ShortCut = 16418
      OnExecute = ActObjLevelDownExecute
    end
    object ActObjFindParent: TAction
      Category = 'edit'
      Caption = 'mFindParent'
      Hint = 'mFindParent'
      ImageIndex = 24
      ShortCut = 16466
      OnExecute = ActObjFindParentExecute
    end
    object ActCreateIcon: TAction
      Category = 'grid'
      Caption = 'mCreateIcon'
      Hint = 'mCreateIcon'
      ImageIndex = 10
      ShortCut = 49225
      OnExecute = ActCreateIconExecute
    end
    object ActTabNext: TAction
      Category = 'edit'
      Caption = 'mGotoNext'
      Hint = 'mGotoNext'
      ShortCut = 16393
      OnExecute = ActTabNextExecute
    end
    object ActShowRulers: TAction
      Category = 'grid'
      Caption = 'mGrid'
      Checked = True
      Hint = 'mGrid'
      ImageIndex = 26
      OnExecute = ActShowRulersExecute
    end
    object ActAniAndLogic: TAction
      Category = 'grid'
      Caption = 'mGraphicsAndLogicsMode'
      Hint = 'mGraphicsAndLogicsMode'
      ImageIndex = 22
      OnExecute = ActAniAndLogicExecute
    end
    object ActBGColor: TAction
      Category = 'grid'
      Caption = 'mChangeBackdrop'
      Hint = 'mChangeBackdrop'
      ImageIndex = 23
    end
    object ActCreateNewAnimType: TAction
      Category = 'edit'
      Hint = 'mCreateNewAnimationType'
      ImageIndex = 5
    end
    object ActImportFromXML: TAction
      Category = 'obj'
      Caption = 'mActImportFromXML'
      Enabled = False
      Hint = 'mActImportFromXML'
      ImageIndex = 28
      OnExecute = ActImportFromXMLExecute
    end
    object ActionToCenter: TAction
      Category = 'edit'
      Caption = 'mAlignToCenter'
      Hint = 'mAlignToCenter'
      ImageIndex = 25
      OnExecute = ActionToCenterExecute
    end
    object ActionToDefaultSizes: TAction
      Category = 'edit'
      Caption = 'mDefaultSizes'
      Hint = 'mDefaultSizes'
      ImageIndex = 25
      OnExecute = ActionToDefaultSizesExecute
    end
  end
  object PopupMenu: TPopupMenu
    Images = Form_m.ImageList1
    Left = 160
    Top = 280
    object NEdit: TMenuItem
      Action = ActEdit
    end
    object N21: TMenuItem
      Caption = '-'
    end
    object mActImportFromXML1: TMenuItem
      Action = ActImportFromXML
    end
  end
  object PopupMenuGrid: TPopupMenu
    OnPopup = PopupMenuGridPopup
    Left = 240
    Top = 280
    object N1: TMenuItem
      Action = ActAddEmptyFrame
    end
    object N2: TMenuItem
      Action = ActAddCopyFrame
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Action = ActClearFrame
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object ActDelFrame1: TMenuItem
      Action = ActDelFrame
    end
    object N17: TMenuItem
      Caption = '-'
    end
    object N18: TMenuItem
      Action = ActCreateIcon
    end
  end
  object PopupMenuObj: TPopupMenu
    Images = Form_m.ImageList1
    OnPopup = PopupMenuObjPopup
    Left = 328
    Top = 280
    object N6: TMenuItem
      Caption = 'mAddFragmentToCanvas'
      Hint = 'mAddFragmentToCanvas'
      ShortCut = 16429
      OnClick = N6Click
    end
    object N8: TMenuItem
      Action = ActObjEdit
    end
    object N20: TMenuItem
      Caption = '-'
    end
    object N10: TMenuItem
      Action = ActObjFindParent
    end
    object N19: TMenuItem
      Action = ActTabNext
    end
    object N14: TMenuItem
      Caption = '-'
    end
    object N15: TMenuItem
      Action = ActObjCopy
    end
    object N16: TMenuItem
      Action = ActObjPaste
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object N7: TMenuItem
      Action = ActObjDelete
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object N12: TMenuItem
      Action = ActObjLevelUp
    end
    object N13: TMenuItem
      Action = ActObjLevelDown
    end
    object N23: TMenuItem
      Caption = '-'
    end
    object mMoreOptions1: TMenuItem
      Caption = 'mMoreOptions'
      object mAlignToCenter2: TMenuItem
        Action = ActionToCenter
      end
      object N24: TMenuItem
        Caption = '-'
      end
      object mDefaultSizes2: TMenuItem
        Action = ActionToDefaultSizes
      end
    end
  end
  object ColorDialog: TColorDialog
    Left = 224
    Top = 328
  end
  object PopupMenuXYWH: TPopupMenu
    Images = Form_m.ImageList1
    OnPopup = PopupMenuXYWHPopup
    Left = 324
    Top = 326
    object mAlignToCenter1: TMenuItem
      Action = ActionToCenter
    end
    object N22: TMenuItem
      Caption = '-'
    end
    object mDefaultSizes1: TMenuItem
      Action = ActionToDefaultSizes
    end
  end
end
