//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UObjPattern.h"
#include "UEditObjPattern.h"
#include "MainUnit.h"
#include <FileCtrl.hpp>
#include "UObjCode.h"
#include "inifiles.hpp"
#include "ULocalization.h"
#include "USaveLoadStreamData.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFObjPattern *FObjPattern;
//---------------------------------------------------------------------------
__fastcall TFObjPattern::TFObjPattern(TComponent* Owner)
        : TForm(Owner)
{
	t_pattern = new TObjPattern();
	clearAll();
	Bt_add->Tag = NativeInt(mAdd);
	Bt_edit->Tag = NativeInt(mEdit);
	Bt_del->Tag = NativeInt(mDel);
	change = false;
	prewiewMode = false;

	OpenDialog->Filter = Localization::Text(_T("mObjPrpPetternFileTyprName")) + _T(" (*.opf)|*.opf");

	Localization::Localize(this);

	sDefaultExt = _T(".opf");
	sPtDir = _T("Patterns");
	sFilter = Localization::Text(_T("mObjPrpPetternFileTyprName")) + _T(" (*.opf)|*.opf");
}
//---------------------------------------------------------------------------
void __fastcall TFObjPattern::FormDestroy(TObject */*Sender*/)
{
	delete t_pattern;
}
//---------------------------------------------------------------------------
void __fastcall TFObjPattern::clearAll()
{
	t_pattern->Clear();
	refreshListBox();
}
//---------------------------------------------------------------------------
void __fastcall TFObjPattern::clearListItems(TList* /*list*/)
{
	t_pattern->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TFObjPattern::refreshListBox()
{
	const std::list<PropertyItem>* items = t_pattern->GetVars();
	if(!items->empty())
	{
		int it_idx = ListBox->ItemIndex;
		ListBox->Clear();
		std::list<PropertyItem>::const_iterator iv;
		for (iv = items->begin(); iv != items->end(); ++iv)
		{
			UnicodeString val;
			val=iv->name;
			val=val + _T(" (") + _T("4 ") + Localization::Text(_T("mBytes"));
			val=val + _T("; ") + Localization::Text(_T("mByDefault")) + _T("=") + FloatToStr(iv->GetDefaultValue()) + ")";
			ListBox->Items->Add(val);
		}
		if(it_idx>ListBox->Items->Count - 1)
		{
			ListBox->ItemIndex = ListBox->Items->Count - 1;
		}
		else if(it_idx<0&&ListBox->Items->Count>0)
		{
			ListBox->ItemIndex = 0;
		}
		else
		{
			ListBox->ItemIndex = it_idx;
		}
	}
	else ListBox->Clear();
}
//---------------------------------------------------------------------------
bool __fastcall TFObjPattern::checkUniqName(UnicodeString name)
{
	if(name.IsEmpty())
	{
		Application->MessageBox(Localization::Text(_T("mVarNameError")).c_str(),
								Localization::Text(_T("mError")).c_str(),
								MB_OK | MB_ICONERROR);
		return true;
	}
	if(getItemIndex(name) > -1)
	{
		Application->MessageBox(Localization::Text(_T("mVarNameExist")).c_str(),
								Localization::Text(_T("mError")).c_str(),
								MB_OK | MB_ICONERROR);
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------
int __fastcall TFObjPattern::getItemIndex(UnicodeString name)
{
	int i;
	const std::list<PropertyItem>* items = t_pattern->GetVars();
	std::list<PropertyItem>::const_iterator iv;
	i = 0;
	for (iv = items->begin(); iv != items->end(); ++iv)
	{
		if(iv->name.Compare(name) == 0)
		{
			return i;
		}
		i++;
	}
	return -1;
}
//---------------------------------------------------------------------------

void __fastcall TFObjPattern::editItem(int mode)
{
	int it_idx;

	if(prewiewMode)
	{
		return;
	}

	it_idx = ListBox->ItemIndex;
	if((mode == mEdit || mode == mDel) && it_idx < 0)
	{
		return;
	}

	std::list<PropertyItem> items;
	t_pattern->GetVars(&items);

	if(mode != mDel)
	{
		UnicodeString oldName = _T("");
		if(mode == mEdit)
		{
			std::list<PropertyItem>::const_iterator iv = items.begin();
			std::advance(iv, it_idx);
			oldName = iv->name;
		}
		UnicodeString cur_name;
		Application->CreateForm(__classid(TFOPEdit), &FOPEdit);
		do
		{
			if(mode == mEdit)
			{
				std::list<PropertyItem>::const_iterator iv = items.begin();
				std::advance(iv, it_idx);
				FOPEdit->SetVarName(iv->name);
				FOPEdit->Edit_def->Text = FloatToStr(iv->GetDefaultValue());
				FOPEdit->Caption = Localization::Text(_T("mEditing"));
			}
			else
			{
				FOPEdit->Caption = Localization::Text(_T("mAdd"));
			}
			if(FOPEdit->ShowModal() != mrOk)
			{
				FOPEdit->Free();
				return;
			}
			cur_name = FOPEdit->GetVarName();
			if(oldName == cur_name && mode == mEdit && !cur_name.IsEmpty())
			{
				break; //���� �� ������ ����� ��� ��������������
			}
		}
		while(checkUniqName(cur_name));
	}

	std::list<PropertyItem>::iterator iiv = items.begin();
	switch(mode)
	{
		case mAdd:
		case mEdit:
		{
			PropertyItem iv;
			iv.name = FOPEdit->GetVarName();
			iv.SetDefaultValue((double)StrToFloat(FOPEdit->Edit_def->Text));
			if(mode == mAdd)
			{
				items.push_back(iv);
			}
			else
			{
				std::advance(iiv, it_idx);
				*iiv = iv;
			}
			change = true;
        }
		break;

		case mDel:
		{
			std::advance(iiv, it_idx);
			items.erase(iiv);
			change = true;
		}
	}

	if(mode != mDel)
	{
		FOPEdit->Free();
		FOPEdit = NULL;
	}

	t_pattern->SetVars(&items);
	refreshListBox();

	if(mode == mAdd)
	{
		ListBox->ItemIndex = ListBox->Items->Count - 1; //������ �� ����������� �������
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjPattern::Bt_addClick(TObject *Sender)
{
	editItem(((TButton*)Sender)->Tag);
}
//---------------------------------------------------------------------------

void __fastcall TFObjPattern::UpDownClick(TObject *,TUDBtnType Button)
{
	int it_idx = ListBox->ItemIndex;
	if(ListBox->Items->Count > 1)
	{
		PropertyItem tv;
		std::list<PropertyItem> items;
		t_pattern->GetVars(&items);
		std::list<PropertyItem>::iterator iiv1 = items.begin();
		std::list<PropertyItem>::iterator iiv2 = items.begin();
		std::advance(iiv1, it_idx);
		std::advance(iiv2, it_idx);
		if(Button == Comctrls::btNext && it_idx > 0)
		{
			iiv2--;
			tv = *iiv1;
			*iiv1 = *iiv2;
			*iiv2 = tv;
			t_pattern->SetVars(&items);
			refreshListBox();
			ListBox->ItemIndex = it_idx - 1;
			change = true;
		}
		else if(Button == Comctrls::btPrev && it_idx < ListBox->Items->Count - 1)
		{
			iiv2++;
			tv = *iiv1;
			*iiv1 = *iiv2;
			*iiv2 = tv;
			t_pattern->SetVars(&items);
			refreshListBox();
			ListBox->ItemIndex = it_idx + 1;
			change = true;
		}
	}
}
//---------------------------------------------------------------------------
void __fastcall TFObjPattern::Bt_upClick(TObject */*Sender*/)
{
	UpDownClick(NULL,Comctrls::btNext);
}
//---------------------------------------------------------------------------
void __fastcall TFObjPattern::Bt_downClick(TObject */*Sender*/)
{
	UpDownClick(NULL,Comctrls::btPrev);
}
//---------------------------------------------------------------------------
int __fastcall TFObjPattern::askToSave()
{
	return Application->MessageBox(Localization::Text(_T("mAskSaveChanges")).c_str(),
									Localization::Text(_T("mAskSaveChangesHeader")).c_str(),
									MB_YESNOCANCEL|MB_ICONQUESTION);
}
//---------------------------------------------------------------------------
bool __fastcall TFObjPattern::checkToSave()
{
 if(change){
  switch(askToSave()){
   case ID_YES: N6->Click(); //save
   break;
   case ID_NO:
   break;
   case ID_CANCEL:
   return false;
  }
 }
 return true;
}
//---------------------------------------------------------------------------
void __fastcall TFObjPattern::CSEd_grChange(TObject */*Sender*/)
{
 change=true;
}
//---------------------------------------------------------------------------
//MENU

//clear all
void __fastcall TFObjPattern::N3Click(TObject */*Sender*/)
{
	change = true;
	clearAll();
}
//---------------------------------------------------------------------------
//new
void __fastcall TFObjPattern::N4Click(TObject */*Sender*/)
{
	if(!checkToSave())return;
	clearAll();
	change=false;
}
//---------------------------------------------------------------------------
//save
void __fastcall TFObjPattern::N6Click(TObject */*Sender*/)
{
	UnicodeString Dir=Form_m->PathPattern;
	SaveDialog->DefaultExt = sDefaultExt;
	SaveDialog->Filter = sFilter;

	if(SaveDialog->Execute())
	{
		Dir=SaveDialog->FileName;
		ChangeFileExt(Dir, _T(".opf"));
		if(!saveToFile(Dir))return;
		change=false;

		Form_m->PathPattern = ExtractFilePath(Dir);
		TIniFile *ini=new TIniFile(Form_m->PathIni + _T("editor.ini"));
		ini->WriteString(_T("Path"), _T("PathPattern"), Form_m->PathPattern);
		delete ini;
	}
}
//---------------------------------------------------------------------------

bool __fastcall TFObjPattern::saveToFile(UnicodeString file_name)
{
	TSaveLoadStreamData::TSaveLoadStreamDataInit initdata;
	initdata.type = TSaveLoadStreamData::TSLDataType::ExportToXML;
	initdata.fileName = file_name;

	TSaveLoadStreamData *iodata = new TSaveLoadStreamData(initdata, false);
	if(iodata->Init() == false)
	{
		delete iodata;
        return false;
    }

	TSaveLoadStreamHandle rootHandle;
	iodata->writeFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftNewSection, "ObjPatternData", rootHandle);

	t_pattern->saveToFile(*iodata, rootHandle);

	bool res = iodata->Save();

	delete iodata;

	return res;
}
//---------------------------------------------------------------------------
//load
void __fastcall TFObjPattern::N7Click(TObject */*Sender*/)
{
	UnicodeString Dir=Form_m->PathPattern;
	OpenDialog->DefaultExt = sDefaultExt;
	OpenDialog->Filter = sFilter;

	if(!checkToSave())
	{
		return;
	}

	if(OpenDialog->Execute())
	{
		UnicodeString mesErr;
		Dir=OpenDialog->FileName;
		ChangeFileExt(Dir, _T(".opf"));
		if(!t_pattern->loadPattern(Dir, (UnicodeString*)&mesErr))
		{
			Application->MessageBox(mesErr.c_str(), Localization::Text(_T("mError")).c_str(),
									MB_OK|MB_ICONERROR);
		}
		refreshListBox();
		change = false;

		Form_m->PathPattern = ExtractFilePath(Dir);
		TIniFile *ini = new TIniFile(Form_m->PathIni + _T("editor.ini"));
		ini->WriteString(_T("Path"), _T("PathPattern"), Form_m->PathPattern);
		delete ini;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjPattern::ListBoxDblClick(TObject */*Sender*/)
{
	editItem(mEdit);
}
//---------------------------------------------------------------------------

void __fastcall TFObjPattern::ListBoxKeyDown(TObject */*Sender*/, WORD &Key,
	  TShiftState Shift)
{
	if(Shift.Contains(ssCtrl))
	{
		if     (Key==VK_UP){Key=NULL;Bt_up->Click();}
		else if(Key==VK_DOWN){Key=NULL;Bt_down->Click();}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjPattern::SetPattern(TObjPattern *p)
{
	p->CopyTo(t_pattern);
	refreshListBox();
}
//---------------------------------------------------------------------------

void __fastcall TFObjPattern::GetPattern(TObjPattern *p)
{
	t_pattern->CopyTo(p);
}
//---------------------------------------------------------------------------
