//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UDefProp.h"
#include "MainUnit.h"
#include "UObjCode.h"
#include "UObjManager.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TFDefProperties *FDefProperties;

//---------------------------------------------------------------------------

__fastcall TFDefProperties::TFDefProperties(TComponent* Owner)
	: TForm(Owner)
{
	mpPropertiesControl = new TPropertiesControl(ScrollBox);
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFDefProperties::SetProperties(GPersonProperties* prp)
{
	const std::list<PropertyItem>* lvars = Form_m->mainCommonRes->GetVariablesMainGameObjPattern();
	ClearScrollBox();
	mpPropertiesControl->Init(lvars, TPropertiesControl::PCM_NORMAL);
	std::list<PropertyItem>::const_iterator iv;
	for (iv = lvars->begin(); iv != lvars->end(); ++iv)
	{
		double val = 0.0f;
		if(!prp->getVarValue(&val, iv->name))
		{
			val = 0.0f;
		}
		mpPropertiesControl->SetValue(val, iv->name);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFDefProperties::FormDestroy(TObject */*Sender*/)
{
	ClearScrollBox();
	delete mpPropertiesControl;
}
//---------------------------------------------------------------------------

void __fastcall TFDefProperties::GetProperties(GPersonProperties* prp)
{
	const std::list<PropertyItem>* lvars = Form_m->mainCommonRes->GetVariablesMainGameObjPattern();
	std::list<PropertyItem>::const_iterator iv;
	for (iv = lvars->begin(); iv != lvars->end(); ++iv)
	{
		double var = 0.0f;
		bool find = false;
		for(int j = 0; j < ScrollBox->ControlCount; j++)
		{
		 TControl* c = (TControl*)ScrollBox->Controls[j];
		 if(c->ClassNameIs("TEdit"))
		 {
		  TEdit *ed = (TEdit *)c;
		  if(iv->name.Compare(ed->Hint) == 0)
		  {
			try
			{
				var = (double)StrToFloat(ed->Text);
			}
			catch(...)
			{
				var = 0.0f;
			}
			find = true;
			break;
		  }
		 }
		}
		if(find)
		{
			prp->setVarValue(var, iv->name);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFDefProperties::ClearScrollBox()
{
	mpPropertiesControl->Clear();
}
//---------------------------------------------------------------------------

