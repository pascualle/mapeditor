#include "URenderThread.h"
#include "UWait.h"

#pragma hdrstop

#include "USave.h"
#include "MainUnit.h"
#include "UScript.h"
#include "UObjCode.h"
#include "inifiles.hpp"
#include "UObjManager.h"
#include "UGameField.h"
#include "FileCtrl.hpp"
#include "ULocalization.h"
#include "UMapCollideLines.h"
#include <map>

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

enum SaveFormat
{
	SAVE_PROJ = 0, SAVE_XML = 1
};

static TSaveLoadStreamData* gspIOStreamData = nullptr;
static TStringList* gsAutoNameSL = nullptr;

//---------------------------------------------------------------------------

void __fastcall TForm_m::GenerateNameListForAutoNamePoolObj(TObjManager *m)
{
	if(gsAutoNameSL == NULL)
	{
		gsAutoNameSL = new TStringList();
	}
	gsAutoNameSL->Clear();

	assert(m);

	__int32 i, j;
	TMapPerson *mo;
	TObjManager::TObjType tlist[TObjManager::OBJ_TYPE_COUNT] =
	{
		TObjManager::CHARACTER_OBJECT,
		TObjManager::BACK_DECORATION,
		TObjManager::FORE_DECORATION,
		TObjManager::TRIGGER,
		TObjManager::DIRECTOR,
		TObjManager::COLLIDESHAPE
	};
	for(j = 0; j < TObjManager::OBJ_TYPE_COUNT; j++)
	{
		__int32 len = m->GetObjTypeCount(tlist[j]);
		for (i = 0; i < len; i++)
		{
			mo = (TMapPerson*)m->GetObjByIndex(tlist[j], i);
			gsAutoNameSL->Add(mo->Name);
		}
	}
	TList* cn = m->GetCollideNodesList();
	for(j = 0; j < cn->Count; j++)
	{
		mo = (TMapPerson*)cn->Items[j];
		gsAutoNameSL->Add(mo->Name);
	}
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TForm_m::AutoNamePoolObj(UnicodeString parentName)
{
	__int32 pos, idx;
	UnicodeString str, prefix;
	prefix = _T("POOL_\0") + parentName + _T("_\0");
	idx = gsAutoNameSL->Count + 1;
	do
	{
		str = prefix + IntToStr(idx);
		pos = gsAutoNameSL->IndexOf(str);
		idx++;
	}
	while (pos >= 0);
	gsAutoNameSL->Add(str);
	return str;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ClearNameListForAutoNamePoolObj()
{
	if(gsAutoNameSL != NULL)
	{
		delete gsAutoNameSL;
		gsAutoNameSL = NULL;
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ProjSave(bool askPath)
{
	if (!proj_exist)
	{
		return;
	}

	if(askPath == false && proj_file_name.IsEmpty())
	{
		askPath = true;
    }

	if(askPath)
	{
		SaveDialog->Title = ActProjSaveAs->Caption;
		SaveDialog->InitialDir = ExtractFilePath(proj_file_name);

		if(mSaveProgDataXMLFormat)
		{
			SaveDialog->Filter = _T("Map editor project (*.meproj)|*.meproj\0");
		}
		else
		{
			SaveDialog->Filter = _T("Map editor project (*.mpf)|*.mpf\0");
		}

		if (!SaveDialog->Execute())
		{
			return;
		}

		if(mSaveProgDataXMLFormat)
		{
			proj_file_name = ChangeFileExt(SaveDialog->FileName, _T(".meproj"));
		}
		else
		{
			proj_file_name = ChangeFileExt(SaveDialog->FileName, _T(".mpf"));
		}
	}
	else
	{
		if(mSaveProgDataXMLFormat)
		{
			proj_file_name = ChangeFileExt(proj_file_name, _T(".meproj"));
		}
		else
		{
			proj_file_name = ChangeFileExt(proj_file_name, _T(".mpf"));
		}
	}

	TSaveLoadStreamData::TSaveLoadStreamDataInit initdata;

	initdata.type = mSaveProgDataXMLFormat ? TSaveLoadStreamData::TSLDataType::SaveProject : TSaveLoadStreamData::TSLDataType::SaveAsBinary;
	initdata.fileName = proj_file_name;

	gspIOStreamData = new TSaveLoadStreamData(initdata, false);
	if(gspIOStreamData->Init() == false)
	{
		delete gspIOStreamData;
		return;
	}

	Application->CreateForm(__classid(TFWait), &FWait);
	FWait->Left = Form_m->Left + (Form_m->Width - FWait->Width) / 2;
	FWait->Top = Form_m->Top + (Form_m->Height - FWait->Height) / 2;
	FWait->Enabled = true;

	new TRenderThread(&DoThreadProcessProjSave); //will free on finish automatically

	if (FWait->Enabled)
	{
		FWait->ShowModal();
	}

	FWait->Free();
	FWait = nullptr;

	//DoThreadProcessProjSave();

	if(!gspIOStreamData->Save())
	{
		Application->MessageBox((Localization::Text(_T("mSaveError")) + _T(" ") + proj_file_name).c_str(),
								(Localization::Text(_T("mError"))).c_str(),
								MB_OK | MB_ICONERROR);


		delete gspIOStreamData;
		gspIOStreamData = nullptr;
		is_save = false;
	}
	else
	{
		delete gspIOStreamData;
		gspIOStreamData = nullptr;

		file_name = proj_file_name;
		RefreshFileName();
		is_save = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::SaveXML()
{
	/*
	if (!proj_exist)
	{
		return;
	}

	SaveDialog->Title = ActXMLSave->Caption;
	SaveDialog->InitialDir = ExtractFilePath(xml_proj_file_name);
	SaveDialog->Filter = _T("Map editor xml format (*.xml)|*.xml");
	if (!SaveDialog->Execute())
	{
		return;
	}
	xml_proj_file_name = ChangeFileExt(SaveDialog->FileName, _T(".xml"));

	XMLDocument->Active = true;
	if (!XMLDocument->IsEmptyDoc())
	{
		XMLDocument->DocumentElement->GetChildNodes()->Clear();
	}
	else
	{
		XMLDocument->AddChild(_T("MapEditorData"));
	}
	_di_IXMLNode nmain = XMLDocument->DocumentElement;
	nmain->SetAttribute(_T("version"), FILEFORMAT_VER);

	TSaveLoadStreamData::TSaveLoadStreamDataInit initdata;
	initdata.type = TSaveLoadStreamData::TSLDataType::ExportToXML;
	initdata.stream.node = nmain;

	gspIOStreamData = new TSaveLoadStreamData(initdata, false);

	Application->CreateForm(__classid(TFWait), &FWait);
	FWait->Left = Form_m->Left + (Form_m->Width - FWait->Width) / 2;
	FWait->Top = Form_m->Top + (Form_m->Height - FWait->Height) / 2;
	FWait->Enabled = true;

	new TRenderThread(&DoThreadProcessProjSave); // will free on finish automatically

	if(FWait->Enabled)
	{
		FWait->ShowModal();
	}
	FWait->Free();
	FWait = nullptr;

	//DoThreadProcessProjSave();

	XMLDocument->SaveToFile(xml_proj_file_name);
	XMLDocument->DocumentElement->GetChildNodes()->Clear();
	XMLDocument->Active = false;

    delete gspIOStreamData;
	gspIOStreamData = nullptr;
    */
}
//---------------------------------------------------------------------------

void WriteAnsiStringData(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h,
								UnicodeString fieldName, const UnicodeString& str)
{
	/*
	UnicodeString tstr = str;
	size_t size = static_cast<size_t>(tstr.Length());
	f.writeFn(&size, TSaveLoadStreamData::TSLFieldType::ftWord, fieldName + _T("sz"), h);
	if(size > 0)
	{
		TSaveLoadStreamCharArray a = { size, AnsiString(tstr).c_str()};
		f.writeFn(&a, TSaveLoadStreamData::TSLFieldType::ftAnsiStringArray, fieldName, h);
	}
	*/
	WriteWideStringData(f, h, TSaveLoadStreamData::TSLFieldType::ftU16, fieldName, str);
}
//---------------------------------------------------------------------------

void WriteWideStringData(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h,
								const TSaveLoadStreamData::TSLFieldType& ftype, UnicodeString fieldName, const UnicodeString& wstr)
{
	size_t size = wstr.Length();
	if(f.type == TSaveLoadStreamData::TSLDataType::SaveAsBinary || f.type == TSaveLoadStreamData::TSLDataType::CopyClipboard)
	{
		f.writeFn(&size, ftype, fieldName + _T("sz"), h);
	}
	if(size > 0)
	{
		TSaveLoadStreamCharArray a = {size * sizeof(wchar_t), wstr.c_str()};
		f.writeFn(&a, TSaveLoadStreamData::TSLFieldType::ftWideStringArray, fieldName, h);
	}
}

//---------------------------------------------------------------------------

void __fastcall TForm_m::DoThreadProcessProjSave()
{
	TSaveLoadStreamHandle h;

	FWait->mText = Localization::Text(_T("mProcessingCommonData"));

	gspIOStreamData->writeFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("MapEditorProjectData"), h);
	TSaveLoadStreamData& f = *gspIOStreamData;

	if (f.type != TSaveLoadStreamData::TSLDataType::ExportToXML)
	{
		{
			unsigned char ch[4];

			//ch_val[0] = 'm';
			//ch_val[1] = 'p';
			//f.writeFn(ch_val, 2, 1, gsIOStreamData);
			//ch_val[0] = FILEFORMAT_VER;
			//gsIOStreamData.writeFn(ch_val, 1, 1, gsIOStreamData);

			ch[0] = 'm';
			ch[1] = 'p';
			ch[2] = FILEFORMAT_VER;

			f.writeFn(&ch[0], TSaveLoadStreamData::TSLFieldType::ftChar, _T("h1"), h);
			f.writeFn(&ch[1], TSaveLoadStreamData::TSLFieldType::ftChar, _T("h2"), h);
			f.writeFn(&ch[2], TSaveLoadStreamData::TSLFieldType::ftChar, _T("h3"), h);
		}

		WriteAnsiStringData(f, h, _T("rawrespath"), PathPngRes0);

		//____________settings:
		{
			unsigned char ch_val[4];
			TSaveLoadStreamHandle h1;

			f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("settings"), h1);

			f.writeFn(&lvWHSizeIcon, TSaveLoadStreamData::TSLFieldType::ftU16, _T("whiconsize"), h1);

			ch_val[0] = (unsigned char)(showRectGameObj ? 1 : 0);
			f.writeFn(&ch_val, TSaveLoadStreamData::TSLFieldType::ftChar, _T("objectborders"), h1);

			ch_val[0] = (unsigned char)(showGrid ? 1 : 0);
			f.writeFn(&ch_val, TSaveLoadStreamData::TSLFieldType::ftChar, _T("grid"), h1);

			f.writeFn(&tickDuration, TSaveLoadStreamData::TSLFieldType::ftU16, _T("animdelay"), h1);

			f.writeFn(&lrCount, TSaveLoadStreamData::TSLFieldType::ftU16, _T("lrcount"), h1);

			f.writeFn(&p_w, TSaveLoadStreamData::TSLFieldType::ftU16, _T("pw"), h1);

			f.writeFn(&p_h, TSaveLoadStreamData::TSLFieldType::ftU16, _T("ph"), h1);
		}
	}
	else
	{
		//____________settings:
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("settings"), h1);

		f.writeFn(&tickDuration, TSaveLoadStreamData::TSLFieldType::ftU16, _T("animdelay"), h1);
		f.writeFn(&lrCount, TSaveLoadStreamData::TSLFieldType::ftU16, _T("lrcount"), h1);
		f.writeFn(&p_w, TSaveLoadStreamData::TSLFieldType::ftU16, _T("pw"), h1);
		f.writeFn(&p_h, TSaveLoadStreamData::TSLFieldType::ftU16, _T("ph"), h1);
	}

	// ____________ grres:
	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("rawres"), h1);

		SaveResArray(f, h1, nullptr);
	}

	// ____________ bgobjects:
	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("bgobjects"), h1);

		unsigned short w_val = static_cast<unsigned short>(mainCommonRes->bgotypes->Count);
		f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("bgobjproperties"), h1);
		for(unsigned short i = 0; i < w_val; i++)
		{
			TSaveLoadStreamHandle h2;
			f.writeFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("bgobjproperty_") + IntToStr(i), h2);

			static_cast<TBGobjProperties*>(mainCommonRes->bgotypes->Items[i])->SaveToFile(f, h2);
		}
	}

	// ____________ mapbgobjs:
	if (f.type != TSaveLoadStreamData::TSLDataType::ExportToXML)
	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("mapbgobjs"), h1);

		ExportBackObjPalette(f, h1, nullptr);
	}

	// ____________ states:
	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("states"), h1);

		unsigned short w_val = static_cast<unsigned short>(mainCommonRes->states->Count);
		f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("statescount"), h1);
		for(unsigned short i = 0; i < w_val; i++)
		{
			TSaveLoadStreamHandle h2;
			f.writeFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("state_") + IntToStr(i), h2);

			static_cast<TState*>(mainCommonRes->states->Items[i])->SaveToFile(f, h2);
		}
	}

	// _________ stateslookuptable:
	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("stateslookuptable"), h1);

		unsigned short w_val = static_cast<unsigned short>(mainCommonRes->stateOppositeTable->Count);
		f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("stateslookuptablecount"), h1);
		for(unsigned short i = 0; i < w_val; i++)
		{
			TSaveLoadStreamHandle h2;
			f.writeFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("stateslookup_") + IntToStr(i), h2);

			WriteAnsiStringData(f, h2, _T("name"), mainCommonRes->stateOppositeTable->Strings[i]);
		}
	}

	// _________ animations:
	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("animations"), h1);

		unsigned short w_val = static_cast<unsigned short>(mainCommonRes->aniNameTypes->Count);
		f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("animationscount"), h1);

		for(unsigned short i = 0; i < w_val; i++)
		{
			TSaveLoadStreamHandle h2;
			f.writeFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("animation_") + IntToStr(i), h2);

			WriteAnsiStringData(f, h2, _T("name"), mainCommonRes->aniNameTypes->Strings[i]);
		}
	}

	// _________ events:
	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("events"), h1);

		unsigned short w_val = static_cast<unsigned short>(mainCommonRes->eventNameTypes->Count);
		f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("eventscount"), h1);

		for(unsigned short i = 0; i < w_val; i++)
		{
			TSaveLoadStreamHandle h2;
			f.writeFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("event_") + IntToStr(i), h2);

			WriteAnsiStringData(f, h2, _T("name"), mainCommonRes->eventNameTypes->Strings[i]);
		}
	}

	// _________ propertiestemplate:
	if(f.type != TSaveLoadStreamData::TSLDataType::ExportToXML)
	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("propertiestemplate"), h1);

		mainCommonRes->MainGameObjPattern->saveToFile(f, h1);
	}

	// ____________ frameobjects
	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("frameobjects"), h1);

		SaveFrameObjInf(f, h1);
	}

	// _________ gameobjpalette:
	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("gameobjpalette"), h1);

		ExportGameObjPalette(f, h1);
	}

	// _________ levels:
	{
		unsigned char ch[4];
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("levels"), h1);

		ch[0] = static_cast<unsigned char>(m_pLevelList->Count);
		f.writeFn(ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("levelscount"), h1);

		for (int m = 0; m < m_pLevelList->Count; m++)
		{
			unsigned short w_val;
			TObjManager *objManager;
			TSaveLoadStreamHandle h2;

			f.writeFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("level_") + IntToStr(m), h2);

			FWait->mText = Localization::Text(_T("mProcessingMap")) + _T(" �") + IntToStr(m) + _T("...");

			objManager = static_cast<TObjManager*>(m_pLevelList->Items[m]);

			GenerateNameListForAutoNamePoolObj(objManager);

			{
				UnicodeString nfile = _T(" ");
				WriteAnsiStringData(f, h2, _T("textspath"), nfile);
			}

			{
				ch[0] = 0;
				ch[1] = 0;
				ch[2] = 0;
				f.writeFn(&ch[0], TSaveLoadStreamData::TSLFieldType::ftChar, _T("r"), h);
				f.writeFn(&ch[1], TSaveLoadStreamData::TSLFieldType::ftChar, _T("g"), h);
				f.writeFn(&ch[2], TSaveLoadStreamData::TSLFieldType::ftChar, _T("b"), h);
			}

			w_val = static_cast<unsigned short>(d_w / p_w);
			f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("dw"), h2);

			w_val = static_cast<unsigned short>(d_h / p_h);
			f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("dh"), h2);

			f.writeFn(&objManager->znCount, TSaveLoadStreamData::TSLFieldType::ftU16, _T("zones"), h2);

			f.writeFn(&objManager->startZone, TSaveLoadStreamData::TSLFieldType::ftS16, _T("activezone"), h2);

			f.writeFn(&objManager->tileCameraPosOnStart, TSaveLoadStreamData::TSLFieldType::ftU16, _T("cameratileidx"), h2);

			w_val = static_cast<unsigned short>(objManager->ignoreUnoptimizedResoures);
			f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("ignoreunoptimizedresoures"), h2);

			f.writeFn(&objManager->mp_w, TSaveLoadStreamData::TSLFieldType::ftS32, _T("mpw"), h2);

			f.writeFn(&objManager->mp_h, TSaveLoadStreamData::TSLFieldType::ftS32, _T("mph"), h2);

			if(f.type == TSaveLoadStreamData::TSLDataType::ExportToXML)
			{
				size_t chdata_sz;
				wchar_t *chdata;
				TList *opt_tiletypes_list = new TList();

				CreateOptimizedBackObjListAndMap(opt_tiletypes_list, nullptr, objManager, &chdata, &chdata_sz);

				const int tile_count = opt_tiletypes_list->Count;
				if(tile_count > 0)
				{
					ExportBackObjPalette(f, h2, opt_tiletypes_list);

					delete opt_tiletypes_list;
					//BSTR bstr = SysAllocString(chdata);
					//delete[] chdata;

					TSaveLoadStreamHandle h3;
					f.writeFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("tiledata"), h3);

					TSaveLoadStreamCharArray a = {chdata_sz, chdata};
					f.writeFn(&a, TSaveLoadStreamData::TSLFieldType::ftDataArray, _T("hex"), h3);

                    delete[] chdata;
					//SysFreeString(bstr);
				}
				else
				{
					delete opt_tiletypes_list;
					delete[] chdata;
				}
			}
			else
			{
				TSaveLoadStreamHandle h3;
				f.writeFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("tiledata"), h3);

				TSaveLoadStreamCharArray a = {objManager->mp_w * objManager->mp_h * sizeof(unsigned short), objManager->map};
				f.writeFn(&a, TSaveLoadStreamData::TSLFieldType::ftDataArray, _T("hex"), h3);
			}

			int ld;
			for(ld = 0; ld < 3; ld++)
			{
				TSaveLoadStreamHandle h3;

				TObjManager::TObjType tlist[3] =
				{
					TObjManager::BACK_DECORATION,
					TObjManager::CHARACTER_OBJECT,
					TObjManager::FORE_DECORATION
				};

				switch(tlist[ld])
				{
					case TObjManager::BACK_DECORATION:
						f.writeFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("bgdecor"), h3);
					break;
					case TObjManager::CHARACTER_OBJECT:
						f.writeFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("mapobjects"), h3);
					break;
					case TObjManager::FORE_DECORATION:
						f.writeFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("fgdecor"), h3);
					break;
					default:
					break;
				}

				w_val = static_cast<unsigned short>(objManager->GetObjTypeCount(tlist[ld]));
				if(tlist[ld] == TObjManager::CHARACTER_OBJECT)
				{
					if(objManager->FindObjectPointer(UnicodeString(SOUNDSCHEME_NAME)))
					{
						w_val--;
					}
				}

				f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("mapobjectscount"), h3);

                int idx = 0;
				for(int i = 0; i < objManager->GetObjTypeCount(tlist[ld]); i++)
				{
					TMapGPerson *gp = (TMapGPerson*)objManager->GetObjByIndex(tlist[ld], i);
					if(gp->GetObjType() == objMAPSOUNDSCHEME)
					{
						continue;
					}

					TSaveLoadStreamHandle h4;
					f.writeFn(&h3, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("obj_") + IntToStr(idx), h4);

					SaveGameObjectTo(f, h4, gp);

					f.writeFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftDebugCheckSum, _T("checksum"), h4);

                    idx++;
				}
			}

			for(ld = 0; ld < 2; ld++)
			{
				TSaveLoadStreamHandle h3;

				TObjManager::TObjType tlist[2] =
				{
					TObjManager::TRIGGER, TObjManager::DIRECTOR
				};

				switch(tlist[ld])
				{
					case TObjManager::TRIGGER:
						f.writeFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("triggers"), h3);
					break;
					case TObjManager::DIRECTOR:
						f.writeFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("directors"), h3);
					break;
					default:
					break;
				}

				w_val = static_cast<unsigned short>(objManager->GetObjTypeCount(tlist[ld]));
				f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("mapobjectscount"), h3);

				for(int i = 0; i < objManager->GetObjTypeCount(tlist[ld]); i++)
				{
					TSaveLoadStreamHandle h4;
					f.writeFn(&h3, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("obj_") + IntToStr(i), h4);

					TMapGPerson *gp = (TMapGPerson*)objManager->GetObjByIndex(tlist[ld], i);
					SaveGameObjectTo(f, h4, gp);

					f.writeFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftDebugCheckSum, _T("checksum"), h4);
				}
			}

			// ____________collideshapes:
			{
				TSaveLoadStreamHandle h3;
				f.writeFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("collideshapes"), h3);

				w_val = static_cast<unsigned short>(objManager->GetObjTypeCount(TObjManager::COLLIDESHAPE));
				f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("collideshapescount"), h3);

				for(int i = 0; i < objManager->GetObjTypeCount(TObjManager::COLLIDESHAPE); i++)
				{
					TSaveLoadStreamHandle h4;
					f.writeFn(&h3, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("obj_") + IntToStr(i), h4);

					TMapGPerson *gp = (TMapGPerson*)objManager->GetObjByIndex(TObjManager::COLLIDESHAPE, i);
					SaveGameObjectTo(f, h4, gp);

					f.writeFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftDebugCheckSum, _T("checksum"), h4);
				}
			}

			//sounds
			{
				TSaveLoadStreamHandle h3;
				f.writeFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("soundscheme"), h3);

				TMapSoundScheme* mss = static_cast<TMapSoundScheme*>(objManager->FindObjectPointer(UnicodeString(SOUNDSCHEME_NAME)));

				if(mss != nullptr)
				{
					__int32 mss_x, mss_y;
					double dmss_x, dmss_y;
					mss->getCoordinates(&dmss_x, &dmss_y);
					mss_x = static_cast<__int32>(dmss_x);
					mss_y = static_cast<__int32>(dmss_y);

					ch[0] = mss->IsLocked() ? 1 : 0;
					f.writeFn(ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("locked"), h3);

					ch[0] = mss->CustomVisible ? 1 : 0;
					f.writeFn(ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("customvisible"), h3);

					w_val = static_cast<unsigned short>(mss->GetItemsCount());
					f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("soundscount"), h3);

					f.writeFn(&mss_x, TSaveLoadStreamData::TSLFieldType::ftS32, _T("x"), h3);

					f.writeFn(&mss_y, TSaveLoadStreamData::TSLFieldType::ftS32, _T("y"), h3);
				}
				else
				{
					__int32 t = 0;

					ch[0] = 0;
					f.writeFn(ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("locked"), h3);
					f.writeFn(ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("customvisible"), h3);
					w_val = 0;
					f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("soundscount"), h3);
					f.writeFn(&t, TSaveLoadStreamData::TSLFieldType::ftS32, _T("x"), h3);
					f.writeFn(&t, TSaveLoadStreamData::TSLFieldType::ftS32, _T("y"), h3);
				}

				if(w_val > 0)
				{
					for (int i = 0; i < mss->GetItemsCount(); i++)
					{
						SoundSchemePropertyItem si;
						TSaveLoadStreamHandle h4;

						f.writeFn(&h3, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("sound_") + IntToStr(i), h4);

						mss->GetItemByIndex(i, si);

						WriteAnsiStringData(f, h4, _T("name"), si.mName);

						WriteWideStringData(f, h4, TSaveLoadStreamData::TSLFieldType::ftU16, _T("path"), si.mFileName);

						w_val = static_cast<short>(si.mType);
						f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("type"), h4);
					}
				}

				f.writeFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftDebugCheckSum, _T("checksum"), h3);
			}

			//scripts
			{
				TSaveLoadStreamHandle h3;
				f.writeFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("scripts"), h3);

                w_val = 0;
				f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("scriptscount"), h3);
			}

			WriteAnsiStringData(f, h2, _T("startscript"), objManager->StartScriptName);

			{
				TSaveLoadStreamHandle h3;
				f.writeFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("mapobjvarscount"), h3);

				SaveObjVarValues(f, h3, mapObj, objManager);

				f.writeFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftDebugCheckSum, _T("checksum"), h3);
			}

			{
				TSaveLoadStreamHandle h3;
				f.writeFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("trigvarscount"), h3);

				SaveObjVarValues(f, h3, triggerObj, objManager);

                f.writeFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftDebugCheckSum, _T("checksum"), h3);
			}

			ClearNameListForAutoNamePoolObj();
		}
	}

	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("gpersonvarscount"), h1);

		SaveObjVarValues(f, h1, parentObj, nullptr);
	}

	FWait->mText = Localization::Text(_T("mProcessingTexts"));

	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("textsdata"), h1);

		SaveProjTexts(f, h1);
	}

	if (f.type != TSaveLoadStreamData::TSLDataType::ExportToXML)
	{
		unsigned short w_val;
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("objectstabs"), h1);

		w_val = static_cast<unsigned short>(TabSetObj->Tabs->Count);
		f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("objectstabscount"), h1);

		for(int i = 0; i < TabSetObj->Tabs->Count; i++)
		{
			TSaveLoadStreamHandle h2;
			f.writeFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("objectstab_") + IntToStr(i), h2);

			WriteWideStringData(f, h2, TSaveLoadStreamData::TSLFieldType::ftU16, _T("name"), TabSetObj->Tabs->Strings[i]);
		}
	}

	FWait->Enabled = false;
	FWait->Close();
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::SaveResArray(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, TObjManager *objManager)
{
	unsigned short w_val;

	std::list<TResourceItem> *str_lst = createImgListForSave(f.type != TSaveLoadStreamData::TSLDataType::ExportToXML ? lstProj : lstAllMaps, nullptr);
	std::list<TResourceItem>::iterator it;

	assert(h.IsValid());

	w_val = (short)str_lst->size();
	f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("rawrescount"), h);

	int i = 0;
	for (it = str_lst->begin(); it != str_lst->end(); ++it)
	{
        TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("resitem_") + IntToStr(i), h1);

		WriteAnsiStringData(f, h1, _T("name"), it->mName);
		f.writeFn(&it->mWidth, TSaveLoadStreamData::TSLFieldType::ftU16, _T("w"), h1);
		f.writeFn(&it->mHeight, TSaveLoadStreamData::TSLFieldType::ftU16, _T("h"), h1);
		if (f.type == TSaveLoadStreamData::TSLDataType::ExportToXML)
		{
			switch (it->mType)
			{
				case RESOURCE_TYPE_IMAGE:
				{
					const UnicodeString image_name_attr = _T("image\0");
					WriteAnsiStringData(f, h1, _T("type"), image_name_attr);
				}
				break;
				case RESOURCE_TYPE_FONTDATA:
				{
					const UnicodeString fontdata_name_attr = _T("fontdata\0");
					WriteAnsiStringData(f, h1, _T("type"), fontdata_name_attr);
				}
				break;
				case RESOURCE_TYPE_SOUNDDATA:
				{
					const UnicodeString sounddata_name_attr = _T("sounddata\0");
					WriteAnsiStringData(f, h1, _T("type"), sounddata_name_attr);
				}
			}
		}
		else
		{
			w_val = static_cast<unsigned short>(it->mType);
			f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("type"), h1);
		}
        i++;
	}

	str_lst->clear();
	delete str_lst;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ExportBackObjPalette(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, TList* altBackObjList)
{
	int obj_count;

	if(altBackObjList == nullptr)
	{
		obj_count = static_cast<int>(mainCommonRes->BackObj.size());
	}
	else
	{
		obj_count = altBackObjList->Count;
	}
	f.writeFn(&obj_count, TSaveLoadStreamData::TSLFieldType::ftU16, _T("mapbgobjcount"), h);

	for (int i = 0; i < obj_count; i++)
	{
		GMapBGObj *mo;
		if(altBackObjList == nullptr)
		{
			mo = &mainCommonRes->BackObj[i];
		}
		else
		{
			mo = static_cast<GMapBGObj*>(altBackObjList->Items[i]);
		}

		if(f.type != TSaveLoadStreamData::TSLDataType::SaveAsBinary)
		{
			if(mo->GetFrameObjCount() == 0)
			{
                continue;
			}

			bool found = false;
			for (int k = 0; k < mo->GetFrameObjCount(); k++)
			{
				TFrameObj *bo = static_cast<TFrameObj*>(mo->GetFrameObj(k));
				if(bo != nullptr)
				{
					int check_bmp;
					UnicodeString sname;
					bo->getSourceRes(nullptr, nullptr, &check_bmp, &sname);
					if(check_bmp >= 0)
					{
						found = true;
						break;
					}
				}
			}
			if(!found)
			{
				continue;
            }
		}

		unsigned short w_val;
		TSaveLoadStreamHandle h1;

		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("bgo_") + IntToStr(i), h1);

		w_val = static_cast<unsigned short>(mo->GetFrameObjCount());
		f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("srccount"), h1);

		for (int k = 0; k < mo->GetFrameObjCount(); k++)
		{
			unsigned char ch[2];
			UnicodeString sname;
			TSaveLoadStreamHandle h2;
			f.writeFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("src_") + IntToStr(k), h2);

			TFrameObj *bo = static_cast<TFrameObj*>(mo->GetFrameObj(k));
			if (bo != nullptr)
			{
				int check_bmp;

				bo->getSourceRes(nullptr, nullptr, &check_bmp, &sname);
				if (check_bmp < 0)
				{
					sname = _T("");
				}

				WriteAnsiStringData(f, h2, _T("name"), sname);

				f.writeFn(&bo->x, TSaveLoadStreamData::TSLFieldType::ftU16, _T("x"), h2);

				f.writeFn(&bo->y, TSaveLoadStreamData::TSLFieldType::ftU16, _T("y"), h2);

				ch[0] = (BYTE)bo->w;
				f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("w"), h2);

				ch[0] = (BYTE)bo->h;
				f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("h"), h2);

				WriteAnsiStringData(f, h2, _T("prp"), bo->bgPropertyName);
			}
			else
			{
				sname = _T("");
				w_val = 0;
				ch[0] = 0;

				WriteAnsiStringData(f, h2, _T("name"), sname);
				f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("x"), h2);
				f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("y"), h2);
				f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("w"), h2);
				f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("h"), h2);
				WriteAnsiStringData(f, h2, _T("prp"), sname);
			}
		}

		WriteAnsiStringData(f, h1, _T("memo"), mo->Memo);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ExportGameObjPalette(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h)
{
	unsigned short w_val;

	w_val = static_cast<unsigned short>(mainCommonRes->GParentObj->Count);
	if(f.type == TSaveLoadStreamData::TSLDataType::ExportToXML)
	{
		w_val -= 2; // SOUNDSCHEME, OBJPOOL
	}

	f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("gpersoncount"), h);

	for (int i = 0; i < mainCommonRes->GParentObj->Count; i++)
	{
		unsigned char ch;
		TSaveLoadStreamHandle h1;

		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("gperson_") + IntToStr(i), h1);

		GPerson *gp = static_cast<GPerson*>(mainCommonRes->GParentObj->Items[i]);
		UnicodeString gpid = GPerson::NameToId(gp->GetName());

		if((gpid.Compare(UnicodeString(SOUNDSCHEME_ID)) == 0 ||
			gpid.Compare(UnicodeString(OBJPOOL_ID)) == 0) && f.type == TSaveLoadStreamData::TSLDataType::ExportToXML)
		{
			continue;
		}

		WriteAnsiStringData(f, h1, _T("name"), gpid);

		ch = gp->ResOptimization ? 1 : 0;
		f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("resoptimization"), h1);

		ch = gp->UseGraphics ? 1 : 0;
		f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("usegraphics"), h1);

		{
			int acr;
			int animCount = 0;
			for (acr = 0; acr < mainCommonRes->aniNameTypes->Count; acr++)
			{
				if (gp->GetAnimation(mainCommonRes->aniNameTypes->Strings[acr]) != nullptr)
				{
					animCount++;
				}
			}

			f.writeFn(&animCount, TSaveLoadStreamData::TSLFieldType::ftU16, _T("animationscount"), h1);

            int animation_idx = 0;
			for (acr = 0; acr < mainCommonRes->aniNameTypes->Count; acr++)
			{
				TAnimation *anm = gp->GetAnimation(mainCommonRes->aniNameTypes->Strings[acr]);
				if (anm == nullptr)
				{
					continue;
				}

				TSaveLoadStreamHandle h2;

				f.writeFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("animation_") + IntToStr(animation_idx), h2);

                animation_idx++;

				UnicodeString aniname = anm->GetName();

				w_val = static_cast<unsigned short>(aniname.Length());

				WriteAnsiStringData(f, h2, _T("name"), aniname);

				w_val = static_cast<unsigned short>(anm->GetLooped());
				f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("loop"), h2);

				w_val = static_cast<unsigned short>(Form_m->mpEdPrObjWndParams->joinNodesCount);
				f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("joinnodescount"), h2);

				int frameCount = anm->GetFrameCount();
				f.writeFn(&frameCount, TSaveLoadStreamData::TSLFieldType::ftU16, _T("framescount"), h2);

				for (int fr = 0; fr < frameCount; fr++)
				{
					TSaveLoadStreamHandle h3;
					std::list<TAnimationFrameData> flst;
					const TAnimationFrame *aframe = anm->GetFrame(fr);

					aframe->CopyFrameDataTo(flst);

					f.writeFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("frame_") + IntToStr(fr), h3);

					int framePartsCount = flst.size();
					f.writeFn(&framePartsCount, TSaveLoadStreamData::TSLFieldType::ftU16, _T("framepartscount"), h3);

					unsigned short dur = static_cast<unsigned short>(aframe->GetDuration());
					f.writeFn(&dur, TSaveLoadStreamData::TSLFieldType::ftU16, _T("duration"), h3);

                    const UnicodeString eventname = aframe->GetEventId();
					WriteAnsiStringData(f, h3, _T("event"), eventname);

					int ct = 0;
					std::list<TAnimationFrameData>::const_iterator afd;
					for (afd = flst.begin(); afd != flst.end(); ++afd)
					{
						TSaveLoadStreamHandle h4;

						f.writeFn(&h3, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("framepart_") + IntToStr(ct), h4);

						hash_t fh = afd->frameId;
						assert(sizeof(hash_t) == sizeof(__int32));
						f.writeFn(&fh, TSaveLoadStreamData::TSLFieldType::ftS32, _T("hashid"), h4);

						if(afd->frameId == 0 && afd->strm_filename.IsEmpty())
						{
                            assert(0);
						}

						__int32 ival = static_cast<__int32>(afd->posX);
						f.writeFn(&ival, TSaveLoadStreamData::TSLFieldType::ftS16, _T("offx"), h4);

						ival = static_cast<__int32>(afd->posY);
						f.writeFn(&ival, TSaveLoadStreamData::TSLFieldType::ftS16, _T("offy"), h4);

						w_val = afd->rw;
						f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("rw"), h4);

						w_val = afd->rh;
						f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("rh"), h4);

						w_val = afd->a;
						f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("alpha"), h4);

						unsigned short dataval = afd->getData();
						f.writeFn(&dataval, TSaveLoadStreamData::TSLFieldType::ftU16, _T("transformdata"), h4);

						WriteAnsiStringData(f, h4, _T("streamfile"), afd->strm_filename);

						ct++;
					}

					{
						w_val = static_cast<unsigned short>(aframe->GetCollideRect().Width());
						f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("colliderectw"), h3);

						w_val = static_cast<unsigned short>(aframe->GetCollideRect().Height());
						f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("colliderecth"), h3);

						short val = static_cast<short>(aframe->GetCollideRect().Left);
						f.writeFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("colliderectoffx"), h3);

						val = static_cast<short>(aframe->GetCollideRect().Top);
						f.writeFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("colliderectoffy"), h3);

						TPoint pt;
						for(int n = 0; n < Form_m->mpEdPrObjWndParams->joinNodesCount; n++)
						{
							TSaveLoadStreamHandle h4;

							f.writeFn(&h3, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("joinnode_") + IntToStr(n), h4);

							aframe->GetJoinNodeCoordinate(n, pt);
							f.writeFn(&pt.x, TSaveLoadStreamData::TSLFieldType::ftS16, _T("offx"), h4);
							f.writeFn(&pt.y, TSaveLoadStreamData::TSLFieldType::ftS16, _T("offy"), h4);
						}
					}
				}

				{
					w_val = static_cast<unsigned short>(anm->GetViewRect().Width());
					f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("viewrectw"), h2);

					w_val = static_cast<unsigned short>(anm->GetViewRect().Height());
					f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("viewrecth"), h2);

					short val = static_cast<short>(anm->GetViewRect().Left);
					f.writeFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("viewrectoffx"), h2);

					val = static_cast<short>(anm->GetViewRect().Top);
					f.writeFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("viewrectoffy"), h2);
				}
			}
		}

		{
			ch = gp->IsDecoration ? 1 : 0;
			f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("isdecoration"), h1);

			ch = gp->DecorType;
			f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("decorationtype"), h1);

			WriteAnsiStringData(f, h1, _T("defaultstate"), gp->DefaultStateName);

			f.writeFn(&gp->mGroupId, TSaveLoadStreamData::TSLFieldType::ftChar, _T("grouptabindex"), h1);

			if (gp->GetIconData())
			{
				std::list<TAnimationFrameData> lst;
				gp->GetIconData()->CopyFrameDataTo(lst);

				w_val = static_cast<unsigned short>(lst.size());
				f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("iconfragmentscount"), h1);

				if (w_val > 0)
				{
                    int frp = 0;
					std::list<TAnimationFrameData>::const_iterator afd;
					for (afd = lst.begin(); afd != lst.end(); ++afd)
					{
						TSaveLoadStreamHandle h2;

						f.writeFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("iconfragment_") + IntToStr(frp), h2);

						hash_t fh = afd->frameId;
						assert(sizeof(hash_t) == sizeof(__int32));
						f.writeFn(&fh, TSaveLoadStreamData::TSLFieldType::ftS32, _T("hashid"), h2);

						__int32 ival = static_cast<__int32>(afd->posX);
						f.writeFn(&ival, TSaveLoadStreamData::TSLFieldType::ftS16, _T("offx"), h2);

						ival = static_cast<__int32>(afd->posY);
						f.writeFn(&ival, TSaveLoadStreamData::TSLFieldType::ftS16, _T("offy"), h2);

						w_val = afd->rw;
						f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("rw"), h2);

						w_val = afd->rh;
						f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("rh"), h2);

                        frp++;
					}
				}
			}
			else
			{
				w_val = 0;
				f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("iconfragmentscount"), h1);
			}
		}
	}
}
//---------------------------------------------------------------------------

//��������� ���������� � ����������� ���������� ��������
void __fastcall TForm_m::SaveFrameObjInf(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h)
{
	unsigned short w_val;

	int fcount = mainCommonRes->GetBitmapFrameCount();
	f.writeFn(&fcount, TSaveLoadStreamData::TSLFieldType::ftU16, _T("frameobjectscount"), h);

	for (int j = 0; j < fcount; j++)
	{
		int check_bmp;
		UnicodeString sname;
		TSaveLoadStreamHandle h1;

		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("fo_") + IntToStr(j), h1);

		const TFrameObj *bo = mainCommonRes->GetBitmapFrameByIdx(j);

		if (bo != nullptr)
		{
			bo->getSourceRes(nullptr, nullptr, &check_bmp, &sname);
		}
		else
		{
			sname = _T("");
		}

		if (check_bmp < 0)
		{
			sname = _T("");
		}

		hash_t fh = bo->getHash();
		assert(sizeof(hash_t) == sizeof(__int32));
		f.writeFn(&fh, TSaveLoadStreamData::TSLFieldType::ftS32, _T("hashid"), h1);

		WriteAnsiStringData(f, h1, _T("src"), sname);

		w_val = static_cast<unsigned short>((bo != nullptr) ? bo->x : 0);
		f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("x"), h1);

		w_val = static_cast<unsigned short>((bo != nullptr) ? bo->y : 0);
		f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("y"), h1);

		w_val = static_cast<unsigned short>((bo != nullptr) ? bo->w : 0);
		f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("w"), h1);

		w_val = static_cast<unsigned short>((bo != nullptr) ? bo->h : 0);
		f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("h"), h1);

		if(bo != nullptr)
		{
			WriteAnsiStringData(f, h1, _T("group"), bo->groupId);
		}
		else
		{
			sname = _T("");
			WriteAnsiStringData(f, h1, _T("group"), sname);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::SaveObjVarValues(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, int obj_type, TObjManager *objManager)
{
	int objCount;
	unsigned short w_val;

	switch(obj_type)
	{
		case mapObj:
			objCount = objManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
			break;
		case parentObj:
			objCount = mainCommonRes->GParentObj->Count;
			break;
		case triggerObj:
			objCount = objManager->GetObjTypeCount(TObjManager::TRIGGER);
			break;
		default:
			return;
	}

	const std::list<PropertyItem>* lvars = mainCommonRes->MainGameObjPattern->GetVars();

	w_val = static_cast<unsigned short>(lvars->size());
	f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("varscount"), h);

	for (int i = 0; i < objCount; i++)
	{
		UnicodeString name;
		GPersonProperties *prp;

		TSaveLoadStreamHandle h1;

		switch(obj_type)
		{
			case mapObj:
				{
					TMapGPerson* mp = static_cast<TMapGPerson*>(objManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, i));
					if (mp->GetObjType() == objMAPSOUNDSCHEME)
					{
						continue;
					}
					name = mp->Name;
					prp = &mp->GetProperties();
				}
				break;
			case parentObj:
				{
					GPerson* obj = static_cast<GPerson*>(mainCommonRes->GParentObj->Items[i]);
					name = GPerson::NameToId(obj->GetName());
					prp = &obj->GetBaseProperties();
				}
				break;
			case triggerObj:
				{
					TMapSpGPerson* obj = static_cast<TMapSpGPerson*>(objManager->GetObjByIndex(TObjManager::TRIGGER, i));
					name = obj->Name;
					prp = &obj->GetProperties();
				}
				break;
			default:
				prp = nullptr;
				name = _T("");
				break;
		}

		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, name, h1);

		std::list<PropertyItem>::const_iterator iv;

		for (iv = lvars->begin(); iv != lvars->end(); ++iv)
		{
			TSaveLoadStreamHandle h2;

			f.writeFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, iv->name, h2);

			double f_val = 0.0f;
			prp->getVarValue(&f_val, iv->name);
			f.writeFn(&f_val, TSaveLoadStreamData::TSLFieldType::ftDouble, _T("value"), h2);

			if (obj_type == triggerObj)
			{
				char ch;
				bool val;
				prp->getChecked(&val, iv->name);
				ch = val ? 1 : 0;
				f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("checked"), h2);
			}
		}
	}
}
//---------------------------------------------------------------------------

static void SaveGameObjectEditorData(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, const TMapPerson *gp)
{
	unsigned char ch;
	ch = gp->IsLocked() ? 1 : 0;
	f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("locked"), h);
	ch = gp->CustomVisible ? 1 : 0;
	f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("customvisible"), h);
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::SaveGameObjectTo(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, const TMapPerson *gp)
{
	__int32 dw_val;
	unsigned short w_val;

	int objType = gp->GetObjType();

	switch(objType)
	{
		case objMAPCOLLIDESHAPE:
		{
			const TCollideChainShape *sh = static_cast<const TCollideChainShape*>(gp);

			f.writeFn(&objType, TSaveLoadStreamData::TSLFieldType::ftU16, _T("objtype"), h);

			SaveGameObjectEditorData(f, h, gp);

			WriteAnsiStringData(f, h, _T("name"), sh->Name);

			dw_val = sh->GetNodesCount();
			f.writeFn(&dw_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("nodescount"), h);

            int n_idx = 0;
			const TCollideChainShape::TCollideNodeList& nlist = sh->GetNodeList();
			for(TCollideChainShape::TCollideNodeList::const_iterator it = nlist.begin(); it != nlist.end(); ++it)
			{
				TSaveLoadStreamHandle h1;
				f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("node_") + IntToStr(n_idx), h1);

				const TMapCollideLineNode& cn = *it;

				SaveGameObjectEditorData(f, h, &cn);

                double xx, yy;
				cn.getCoordinates(&xx, &yy);
				int x = static_cast<__int32>(xx);
				int y = static_cast<__int32>(yy);
				f.writeFn(&x, TSaveLoadStreamData::TSLFieldType::ftS32, _T("x"), h1);
				f.writeFn(&y, TSaveLoadStreamData::TSLFieldType::ftS32, _T("y"), h1);

				n_idx++;
			}
		}
		break;

		case objMAPTEXTBOX:
		{
			const TMapTextBox *tgp = static_cast<const TMapTextBox*>(gp);

			f.writeFn(&objType, TSaveLoadStreamData::TSLFieldType::ftU16, _T("objtype"), h);

			SaveGameObjectEditorData(f, h, gp);

			WriteAnsiStringData(f, h, _T("parentname"), tgp->getGParentName());

			WriteAnsiStringData(f, h, _T("name"), tgp->Name);

			short zone = static_cast<short>(tgp->GetZone());
			f.writeFn(&zone, TSaveLoadStreamData::TSLFieldType::ftS16, _T("zone"), h);

			tgp->GetTransform().Save(f, h);

			WriteAnsiStringData(f, h, _T("memo"), tgp->getRemarks());

			w_val = static_cast<unsigned short>(tgp->GetTextType());
			f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("txttype"), h);

			WriteAnsiStringData(f, h, _T("txtid"), tgp->GetTextId());

			WriteAnsiStringData(f, h, _T("font"), tgp->GetFontName());

			dw_val = static_cast<__int32>(tgp->GetFillColor());
			f.writeFn(&dw_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("rbgabackdrop"), h);

			dw_val = static_cast<__int32>(tgp->GetAlignment());
			f.writeFn(&dw_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("alignment"), h);

			w_val = static_cast<unsigned short>(tgp->GetTextWidth());
			f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("wcanvas"), h);

			w_val = static_cast<unsigned short>(tgp->GetTextHeight());
			f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("hcanvas"), h);

			__int32 sc, cc;
			tgp->GetDynamicTextSizes(sc, cc);
			f.writeFn(&sc, TSaveLoadStreamData::TSLFieldType::ftU16, _T("dynamictextstringscount"), h);
			f.writeFn(&cc, TSaveLoadStreamData::TSLFieldType::ftU16, _T("dynamictextcharsperstring"), h);

			WriteAnsiStringData(f, h, _T("objlink"), tgp->ParentObj);
		}
		break;

		case objMAPSOUNDSCHEME:
		{
			const TMapSoundScheme *sgp = static_cast<const TMapSoundScheme*>(gp);

			f.writeFn(&objType, TSaveLoadStreamData::TSLFieldType::ftU16, _T("objtype"), h);

			SaveGameObjectEditorData(f, h, gp);

			sgp->GetTransform().Save(f, h);

			__int32 sndresct = sgp->GetItemsCount();
			f.writeFn(&sndresct, TSaveLoadStreamData::TSLFieldType::ftU16, _T("soundscount"), h);

			for (int j = 0; j < sndresct; j++)
			{
				SoundSchemePropertyItem pi;
				TSaveLoadStreamHandle h1;

				f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("sound_") + IntToStr(j), h1);

				sgp->GetItemByIndex(j, pi);

				WriteAnsiStringData(f, h1, _T("name"), pi.mName);

				WriteWideStringData(f, h1, TSaveLoadStreamData::TSLFieldType::ftU16, _T("path"), pi.mFileName);

				w_val = static_cast<unsigned short>(pi.mType);
				f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("type"), h1);
			}
		}
		break;

		case objMAPOBJPOOL:
		{
			if (f.type == TSaveLoadStreamData::TSLDataType::ExportToXML)
			{
				double x, y;
				__int32 pos_x ,pos_y;
				TMapObjPool *mop = (TMapObjPool*)gp;
				ObjPoolPropertyItem it;
				__int32 count = mop->GetItemsCount();
				TObjManager *towner = new TObjManager(mainCommonRes);
				mop->getCoordinates(&x, &y);
				pos_x = pos_y = 0;
				for(__int32 i = 0; i < count; i++)
				{
					mop->GetItemByIndex(i, it);
					GPerson *gmp = (GPerson*)mainCommonRes->GetParentObjByName(it.mName);
					assert(gmp);
					for(unsigned __int32 j = 0; j < it.mCount; j++)
					{
						double enabled_val;
						TMapGPerson *mp = towner->CreateMapGPerson(x, y, &pos_x, &pos_y, gmp->GetName());
						mp->SetProperties(gmp->GetBaseProperties());
						if(mp->GetVarValue(&enabled_val, UnicodeString(PredefProperties::PropertyNames[PredefProperties::PRP_ENABLE])))
						{
							enabled_val = 0.0f;
							mp->SetVarValue(enabled_val, UnicodeString(PredefProperties::PropertyNames[PredefProperties::PRP_ENABLE]));
						}
						if (!gmp->IsDecoration)
						{
							mp->SetDecorType(decorNONE);
						}
						else
						{
							mp->SetDecorType(gmp->DecorType);
						}
						mp->State_Name = gmp->DefaultStateName;
						mp->SetZone(mop->GetZone());
						mp->Name = AutoNamePoolObj(mp->getGParentName());
						SaveGameObjectTo(f, h, mp);
					}
					towner->ClearAll();
				}
				delete towner;
			}
			else
			{
				const TMapObjPool *mop = static_cast<const TMapObjPool*>(gp);

				f.writeFn(&objType, TSaveLoadStreamData::TSLFieldType::ftU16, _T("objtype"), h);

				SaveGameObjectEditorData(f, h, gp);

				WriteAnsiStringData(f, h, _T("name"), mop->Name);

				short zone = mop->GetZone();
				f.writeFn(&zone, TSaveLoadStreamData::TSLFieldType::ftS16, _T("zone"), h);

				mop->GetTransform().Save(f, h);

				__int32 poct = mop->GetItemsCount();
				f.writeFn(&poct, TSaveLoadStreamData::TSLFieldType::ftU16, _T("poolcount"), h);

				for (int j = 0; j < poct; j++)
				{
					ObjPoolPropertyItem pi;
					TSaveLoadStreamHandle h1;

					f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("pool_") + IntToStr(j), h1);

					mop->GetItemByIndex(j, pi);

					WriteAnsiStringData(f, h1, _T("name"), pi.mName);

					w_val = static_cast<unsigned short>(pi.mCount);
					f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("objcount"), h1);
				}
            }
		}
		break;

		case objMAPOBJ:
		{
            unsigned char ch[2];
			const TMapGPerson *mgp = static_cast<const TMapGPerson*>(gp);

			f.writeFn(&objType, TSaveLoadStreamData::TSLFieldType::ftU16, _T("objtype"), h);

			SaveGameObjectEditorData(f, h, mgp);

			WriteAnsiStringData(f, h, _T("parentname"), mgp->getGParentName());
			WriteAnsiStringData(f, h, _T("name"), mgp->Name);

			short zone = mgp->GetZone();
			f.writeFn(&zone, TSaveLoadStreamData::TSLFieldType::ftS16, _T("zone"), h);

			mgp->GetTransform().Save(f, h);

			ch[0] = 0; //mgp->isAssignLinks();
			f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("activelink"), h);

			WriteAnsiStringData(f, h, _T("director"), mgp->NextLinkedDirector);

			WriteAnsiStringData(f, h, _T("directorfield"), _T(""));

			WriteAnsiStringData(f, h, _T("memo"), mgp->getRemarks());

			WriteAnsiStringData(f, h, _T("state"), mgp->State_Name);

			w_val = static_cast<unsigned short>(mgp->getDirStatesCount());
			f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("statescount"), h);

			dw_val = w_val;
			for (int j = 0; j < dw_val; j++)
			{
				TSaveLoadStreamHandle h1;
				f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("state_") + IntToStr(j), h1);
				WriteAnsiStringData(f, h1, _T("name"), mgp->getDirStateItem(j));
			}

			w_val = 0;// static_cast<unsigned short>(mgp->getScriptsCount());
			f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("scriptscount"), h);

			/*
			dw_val = ::Actions::sptCOUNT;
			for (int j = 0; j < dw_val; j++)
			{
				_di_IXMLNode stnode;
				name = gp->getScriptName(UnicodeString(::Actions::Names[j]));
				if (&SaveToFileXMLFn == iWriteFn)
				{
					stnode = snode->AddChild(_T("action\0"), -1);
					stnode->SetAttribute(_T("name\0"), UnicodeString(::Actions::Names[j]));
					stnode->SetAttribute(_T("script\0"), name);
				}
				if (!name.IsEmpty())
				{
					//short		���������� �������� � �������� �������
					w_val = (short)strlen(::Actions::Names[j]);
					iWriteFn(&w_val, sizeof(short), 1, f);
					//BYTE[]	�������� �������
					iWriteFn(&::Actions::Names[j], w_val, 1, f);
					//short		���������� �������� � ����� �������
					w_val = (short)name.Length();
					iWriteFn(&w_val, sizeof(short), 1, f);
					//BYTE[]	��� �������
					iWriteFn(AnsiString(name).c_str(), w_val, 1, f);
				}
			}
            */

			{
				WriteAnsiStringData(f, h, _T("objlink"), mgp->ParentObj);

				//WORD      ���������� join nodes
				if (mgp->IsChildExist())
				{
					dw_val = w_val = static_cast<unsigned short>(MAX_ANIMATON_JOIN_NODES);
				}
				else
				{
					dw_val = w_val = 0;
				}

				f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("objlinkcount"), h);

				for (int j = 0; j < dw_val; j++)
				{
					TSaveLoadStreamHandle h1;
					f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("objlink_") + IntToStr(j), h1);
					WriteAnsiStringData(f, h1, _T("name"), mgp->GetChild(j));
				}
			}
		}
		break;

		case objMAPTRIGGER:
		case objMAPDIRECTOR:
		{
			unsigned char ch[2];
			const TMapSpGPerson *sgp = static_cast<const TMapSpGPerson*>(gp);

			f.writeFn(&objType, TSaveLoadStreamData::TSLFieldType::ftU16, _T("objtype"), h);

			SaveGameObjectEditorData(f, h, sgp);

			WriteAnsiStringData(f, h, _T("name"), sgp->Name);

			short zone = sgp->GetZone();
			f.writeFn(&zone, TSaveLoadStreamData::TSLFieldType::ftS16, _T("zone"), h);

			sgp->GetTransform().Save(f, h);

			WriteAnsiStringData(f, h, _T("memo"), sgp->getRemarks());

			WriteAnsiStringData(f, h, _T("targetobj"), sgp->objName);

			ch[0] = static_cast<unsigned char>(sgp->mapDirection);
			f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("tilemapchangedirection"), h);

			w_val = static_cast<unsigned short>(sgp->mapIndex);
			f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("tilemapindexormathop"), h);

			w_val = static_cast<unsigned short>(sgp->mapCount);
			f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("tilemapchangecount"), h);

			ch[0] = static_cast<unsigned char>(sgp->Type);
			f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("triggertype"), h);

			if (objMAPTRIGGER == objType)
			{
				__int32 int_val;
				const TMapTrigger *trig = static_cast<const TMapTrigger*>(sgp);

				int_val = trig->stateList->Count;
				f.writeFn(&int_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("changestatescount"), h);
				for (int k = 0; k < trig->stateList->Count; k++)
				{
					TSaveLoadStreamHandle h1;
					f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("state_") + IntToStr(k), h1);
					WriteAnsiStringData(f, h1, _T("name"), trig->stateList->Strings[k]);
				}

				int_val = 0;//::Actions::sptCOUNT;
				f.writeFn(&int_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("actionscount"), h);
				/*for (k = 0; k < ::Actions::sptCOUNT; k++)
				{
					TSaveLoadStreamHandle h1;
					f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("action_") + IntToStr(k), h1);
					WriteAnsiStringData(f, h1, _T("action"), ::Actions::Names[k]);
					WriteAnsiStringData(f, h1, _T("script"), trig->getScriptName(UnicodeString(::Actions::Names[k]));
				}*/

				WriteAnsiStringData(f, h, _T("teleportobj"), trig->objTeleport);

				int_val = trig->tileTeleport;
				f.writeFn(&int_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("teleportmaptileidx"), h);
			}
			else if (objMAPDIRECTOR == objType)
			{
				__int32 int_val;
				const TMapDirector *dir = static_cast<const TMapDirector*>(sgp);

				for (int k = 0; k < 4; k++)
				{
					TSaveLoadStreamHandle h1;
					f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("joinnode_") + IntToStr(k), h1);
					WriteAnsiStringData(f, h1, _T("name"), dir->nearNodes[k]);
				}

				WriteAnsiStringData(f, h, _T("nodetrigger"), dir->nodeTrigger);

				WriteAnsiStringData(f, h, _T("event"), dir->objTeleport);
			}
		}
	}

	/*
	if (&SaveToFileXMLFn == iWriteFn && (objType == objMAPOBJ || objType == objMAPTRIGGER))
	{
		if(objType == objMAPTRIGGER)
		{
			if(static_cast<TMapSpGPerson*>(iObj)->Type == trMAP)
			{
				return;
			}
		}
		if(objType == objMAPOBJ)
		{
			if(static_cast<TMapGPerson*>(iObj)->GetDecorType() != decorNONE)
			{
				return;
			}
		}
		__int32 ct;
		GPersonProperties *prp;
		_di_IXMLNode vnode;
		const std::list<PropertyItem>* lvars = mainCommonRes->MainGameObjPattern->GetVars();
		assert(node);
		vnode = node->AddChild(_T("properties\0"), -1);
		prp = NULL;
		switch(objType)
		{
			case objMAPOBJ:
				prp = &static_cast<TMapGPerson*>(iObj)->GetProperties();
				break;
			case objMAPTRIGGER:
				prp = &static_cast<TMapSpGPerson*>(iObj)->GetProperties();
		}
		ct = 0;
		std::list<PropertyItem>::const_iterator iv;
		for (iv = lvars->begin(); iv != lvars->end(); ++iv)
		{
			_di_IXMLNode vinode;
			double f_val = 0.0f;
			if (objMAPTRIGGER == objType)
			{
				bool val = false;
				prp->getChecked(&val, iv->name);
				if(val)
				{
					vinode = vnode->AddChild(_T("property\0"), -1);
					prp->getVarValue(&f_val, iv->name);
					vinode->SetAttribute(_T("name\0"), iv->name);
					vinode->SetAttribute(_T("value\0"), f_val);
					ct++;
				}
			}
			else
			{
				vinode = vnode->AddChild(_T("property\0"), -1);
				prp->getVarValue(&f_val, iv->name);
				vinode->SetAttribute(_T("name\0"), iv->name);
				vinode->SetAttribute(_T("value\0"), f_val);
				ct++;
			}
		}
		vnode->SetAttribute(_T("count\0"), ct);
	}
    */
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::SaveProjTexts(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h)
{
	int j;
	int ct = MAX_LANGUAGES;

	f.writeFn(&ct, TSaveLoadStreamData::TSLFieldType::ftChar, _T("langcount"), h);

	if (ct == 0)
	{
		return;
	}

	TStringList *list = new TStringList();
	mpTexts->GetNames(list);
	ct = list->Count;

	f.writeFn(&ct, TSaveLoadStreamData::TSLFieldType::ftU16, _T("stringscount"), h);

	if (ct == 0)
	{
		delete list;
		return;
	}

	for (j = 0; j < ct; j++)
	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("string_") + IntToStr(j), h1);
		WriteWideStringData(f, h1, TSaveLoadStreamData::TSLFieldType::ftChar, _T("id"), list->Strings[j]);
	}

	TStringList *langs = new TStringList();
	mpTexts->GetLanguages(langs);

	for (int i = 0; i < MAX_LANGUAGES; i++)
	{
		TSaveLoadStreamHandle h1;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("lang_") + IntToStr(i), h1);
		WriteWideStringData(f, h1, TSaveLoadStreamData::TSLFieldType::ftChar, _T("langname"), langs->Strings[i]);

		if (!langs->Strings[i].IsEmpty())
		{
			for (j = 0; j < ct; j++)
			{
				TSaveLoadStreamHandle h2;

				const TUnicodeText *text = mpTexts->GetText(list->Strings[j], langs->Strings[i]);
				assert(text);

				f.writeFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, list->Strings[j], h2);

				//BYTE margin
				//DWORD back color
				//DWORD alignment
				//WORD width
				//WORD height
				//WORD font name char count
				//BYTES[] font name
				//WORD strings count
				//{
				  //WORD string char count
				  //BYTES[] string (unicode)
				//}
				text->Save(f, h2);
			}
		}
	}

	delete langs;
	delete list;
}
//---------------------------------------------------------------------------

