object FormScale: TFormScale
  Left = 402
  Top = 346
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  ClientHeight = 134
  ClientWidth = 256
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 55
    Top = 21
    Width = 33
    Height = 13
    Caption = 'mScale'
  end
  object ButtonCancel: TButton
    Left = 38
    Top = 100
    Width = 72
    Height = 24
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 1
  end
  object ButtonOk: TButton
    Left = 116
    Top = 100
    Width = 100
    Height = 24
    Caption = 'mApply'
    ModalResult = 1
    TabOrder = 2
  end
  object ComboBox: TComboBox
    Left = 55
    Top = 40
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemIndex = 3
    TabOrder = 0
    Text = 'x1'
    Items.Strings = (
      'x0.125'
      'x0.25'
      'x0.5'
      'x1'
      'x2'
      'x4')
  end
end
