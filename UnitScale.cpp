//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UnitScale.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormScale *FormScale;

//---------------------------------------------------------------------------

__fastcall TFormScale::TFormScale(TComponent* Owner)
		: TForm(Owner)
{
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFormScale::FormKeyPress(TObject */*Sender*/, char &Key)
{
	if(Key == VK_ESCAPE)Close();
}
//---------------------------------------------------------------------------
