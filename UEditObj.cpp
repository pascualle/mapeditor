#include <vcl.h>
#pragma hdrstop

#include "UEditObj.h"
#include "MainUnit.h"
#include "UObjManager.h"
#include "UObjCode.h"
#include "UEditObjPattern.h"
#include "URenderGL.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"

TFEditGrObj *FEditGrObj;

static const int BORDER = 10;
static const int SCALE_PANEL_WIDTH = 42;
static const int MAX_SCALE = 5000;

static const int SCALE_DELAY_MS = 400;
static const int CREATE_PREVIEW_MS = 100;
static const int REFRESH_TIME_MS = 200;

static const int RENDER_DELAY_MS = 60;

static const int SCROLL_STEP = 8;

static const int MAX_PREWIEV_SIZE = 42;
static const int TFEditGrObjMAINMENUDISABLETAG = 38;

static TRGLWinId mWindGlID[2];

//---------------------------------------------------------------------------
__fastcall TFEditGrObj::TFEditGrObj(TComponent *Owner) : TForm(Owner)
{
	CSEd_x = new TSpinEdit(GroupBox_p);
	CSEd_x->Parent = GroupBox_p;
	CSEd_x->Left = 48;
	CSEd_x->Top = 18;
	CSEd_x->Width = 82;
	CSEd_x->Height = 22;
	CSEd_x->TabOrder = 0;
	CSEd_x->OnChange = CSEd_xChange;
	CSEd_x->OnKeyPress = CSEd_xKeyPress;
	CSEd_x->Visible = true;
	CSEd_y = new TSpinEdit(GroupBox_p);
	CSEd_y->Parent = GroupBox_p;
	CSEd_y->Left = 48;
	CSEd_y->Top = 42;
	CSEd_y->Width = 82;
	CSEd_y->Height = 22;
	CSEd_y->TabOrder = 1;
	CSEd_y->OnChange = CSEd_xChange;
	CSEd_y->OnKeyPress = CSEd_xKeyPress;
	CSEd_y->Visible = true;
	CSEd_w = new TSpinEdit(GroupBox_p);
	CSEd_w->Parent = GroupBox_p;
	CSEd_w->Left = 48;
	CSEd_w->Top = 71;
	CSEd_w->Width = 82;
	CSEd_w->Height = 22;
	CSEd_w->TabOrder = 2;
	CSEd_w->OnChange = CSEd_xChange;
	CSEd_w->OnKeyPress = CSEd_xKeyPress;
	CSEd_w->Visible = true;
	CSEd_h = new TSpinEdit(GroupBox_p);
	CSEd_h->Parent = GroupBox_p;
	CSEd_h->Left = 48;
	CSEd_h->Top = 96;
	CSEd_h->Width = 82;
	CSEd_h->Height = 22;
	CSEd_h->TabOrder = 3;
	CSEd_h->OnChange = CSEd_xChange;
	CSEd_h->OnKeyPress = CSEd_xKeyPress;
	CSEd_h->Visible = true;

	mpResObjManager = NULL;

	scale[tgBottom] = scale[tgTop] = 1;
	currentFileName = _T("");
	MainMenu1->Tag = NativeInt(0);
	mpImageListBG = new TList();

	CurrentBGColor = clWhite;
	ColorBox->Selected = CurrentBGColor;

	mpSizeTrackBar = NULL;
	mSizeTrackBarCounter = -1;
	mCreatePreviewCounter = -1;

	Image1->Visible = false;
	Image1->Top = 0;
	Image1->Left = 0;
	Image1->Height = 1;
	Image1->Width = 1;

	CSEd_y->Tag = NativeInt(sTop);
	CSEd_h->Tag = NativeInt(sBottom);
	CSEd_x->Tag = NativeInt(sLeft);
	CSEd_w->Tag = NativeInt(sRight);

	TrackBar1->Tag = NativeInt(tgTop);
	TrackBar2->Tag = NativeInt(tgBottom);

	ZeroMemory(&mSelectRect, sizeof(TSelectionObject));
	ZeroMemory(&copyRect, sizeof(TRect));

	FakeControl->Top = 0;
	FakeControl->Left = 0;
	FakeControl->Width = 0;
	FakeControl->Height = 0;

	ImageList->Width = MAX_PREWIEV_SIZE;
	ImageList->Height = MAX_PREWIEV_SIZE;
	CategoryButtons_s->ButtonWidth = ImageList->Width + 6;
	CategoryButtons_s->ButtonHeight = ImageList->Height + 6;

	PaintBox1->Tag = NativeInt(-1);
	PaintBox1->Top = 0;
	PaintBox1->Left = ScrollBox1->Left;

	mpActiveItem = NULL;
	mUpdateTimer = -1;

	prev_bmp = new BMPImage(new Graphics::TBitmap());
	mpGameObjGM = NULL;
	temp_res = new TList();

	mOldPaintBox1WndProc = PaintBox1->WindowProc;
	PaintBox1->WindowProc = CustomPaintBox1WndMethod;
	mRenderDelay = RENDER_DELAY_MS;

	Localization::Localize(ActionList1);
	Localization::Localize(PopupMenu);
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

__fastcall TFEditGrObj::~TFEditGrObj()
{
	ReleaseRenderGL();
	delete prev_bmp;
	prev_bmp = NULL;
	ClearTempRes();
	FreePopupMoveTo();
	delete temp_res;
	delete CSEd_y;
	delete CSEd_x;
	delete CSEd_w;
	delete CSEd_h;
	ClearImageListBG();
	delete mpImageListBG;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::FormDestroy(TObject *)
{
	ReleaseRenderGL();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::Init(TCommonResObjManager *commonResObjManager)
{
	assert(!TRenderGL::IsInited());
	mpResObjManager = commonResObjManager;
	assert(mpResObjManager);
	InitRenderGL();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::InitRenderGL()
{
	if (gUsingOpenGL)
	{
		TRect v;
		v.Top = 0;
		v.Left = 0;
		v.Bottom = PaintBox1->Height;
		v.Right = PaintBox1->Width;
		if(v.Width() > 0 && v.Height() > 0)
		{
			ReleaseRenderGL();
			mWindGlID[1] = TRenderGL::InitWindow(PaintBox1->Handle, v, v, true);
            TRenderGL::SetActiveCommonResObjManager(mpResObjManager);
			TRenderGL::SetBackdropImage(NULL, ColorBox->Selected);
			v.Bottom = Image1->Height;
			v.Right = Image1->Width;
			mWindGlID[0] = TRenderGL::InitWindow(Image1->Handle, v, v, true);
		}
	}
	Form_m->SetChildRenderGLForm(this);
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ReleaseRenderGL()
{
	Form_m->SetChildRenderGLForm(NULL);
	TRenderGL::ReleaseWindows();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::InitImageListBG(int ct)
{
	ClearImageListBG();
	for (int i = 0; i < ct; i++)
	{
		mpImageListBG->Add(reinterpret_cast<void *>(0));
		mpImageListBG->Add(NULL);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ClearImageListBG()
{
	for (int i = 0; i < mpImageListBG->Count; i += 2)
	{
		if(reinterpret_cast<NativeInt>(mpImageListBG->Items[i]) != 0)
		{
			delete static_cast<BMPImage*>(mpImageListBG->Items[i + 1]);
		}
	}
	mpImageListBG->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ClearImageListBGItem(int idx)
{
	if(reinterpret_cast<NativeInt>(mpImageListBG->Items[idx * 2]) != 0)
	{
		delete static_cast<BMPImage*>(mpImageListBG->Items[idx * 2 + 1]);
	}
	mpImageListBG->Items[idx * 2] = reinterpret_cast<void *>(0);
	mpImageListBG->Items[idx * 2 + 1] = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::AssignToImageListBG(int idx, const BMPImage *img, bool deleteOnRelease)
{
	ClearImageListBGItem(idx);
	mpImageListBG->Items[idx * 2] = reinterpret_cast<void *>(deleteOnRelease ? 1 : 0);
	mpImageListBG->Items[idx * 2 + 1] = static_cast<void*>(const_cast<BMPImage *>(img));
}
//---------------------------------------------------------------------------

const BMPImage* __fastcall TFEditGrObj::GetFromImageListBG(int idx)
{
	return static_cast<BMPImage*>(mpImageListBG->Items[idx * 2 + 1]);
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ClearTempRes()
{
	for (int i = 0; i < temp_res->Count; i++)
	{
		TTempBGobj *o = (TTempBGobj*)temp_res->Items[i];
		delete o;
	}
	temp_res->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::clearAll()
{
	StatusBar->Panels->Items[0]->Text = _T("");
	resetRect();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::resetRect()
{
	mSelectRect.mLeft = 0.0;
	mSelectRect.mTop = 0.0;
	if(mSelectRect.mLockHW == false)
	{
		mSelectRect.mRight = 0.0;
		mSelectRect.mBottom = 0.0;
		Image1->Visible = false;
	}
	else
	{
		mSelectRect.mRight = (double)mSelectRect.mFixedWidth;
		mSelectRect.mBottom = (double)mSelectRect.mFixedHeight;
	}
	CheckBounds_LTRB(mSelectRect.mLeft, mSelectRect.mTop, mSelectRect.mRight, mSelectRect.mBottom);
	checkCrVisible();
	PaintBox1->Tag = NativeInt(-1);
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::checkCrVisible()
{
	onSelRectResize();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::onSelRectResize()
{
	CSEd_x->Value = (__int32)mSelectRect.mLeft;
	CSEd_y->Value = (__int32)mSelectRect.mTop;
	CSEd_w->Value = mSelectRect.getWidth();
	CSEd_h->Value = mSelectRect.getHeight();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CSEd_xChange(TObject *Sender)
{
	bool res = true;
	int selSt;
	int len;

	TSpinEdit *se = (TSpinEdit*)Sender;

	if (se->Text.IsEmpty())
		return;

	if (mpActiveItem == NULL)
	{
		se->Value = 0;
	}
	selSt = se->SelStart;
	try
	{
		if (StrToInt(se->Text) < 0)
			res = false;
	}
	catch(...)
	{
		if (!se->Text.IsEmpty())
		{ //������� ������ �������
			UnicodeString str = _T("");
			UnicodeString strNum = _T("0123456789");
			for (len = 1; len <= se->Text.Length(); len++)
				if (strNum.Pos(se->Text[len]))
					str += se->Text[len];
				else
				{
					if (selSt > 0)
						selSt--;
				}
			try
			{
				if (StrToInt(str) < 0)
					res = false;
			}
			catch(...)
			{
				res = false;
			};
			if (res)
				se->Text = str;
		}
	}
	if (!res)
		se->Text = _T("");
	len = se->Text.Length();
	if (selSt == 0 && key_press)
		se->SelStart = 0;
	else
	{
		if (selSt < len && selSt > 0)
			se->SelStart = selSt;
		else
			se->SelStart = len;
	}
	se->SelLength = 0;
	key_press = false;

	if (se->Text.IsEmpty())
		return;

	res = false;
	switch(se->Tag)
	{
	case sTop:
		if (se->Value != (__int32)mSelectRect.mTop)
			res = true;
		break;
	case sBottom:
		if (se->Value != mSelectRect.getHeight())
			res = true;
		break;
	case sLeft:
		if (se->Value != (__int32)mSelectRect.mLeft)
			res = true;
		break;
	case sRight:
		if (se->Value != mSelectRect.getWidth())
			res = true;
		break;
	}
	if (res)
	{
		setRectSel_XYWH((double)CSEd_x->Value, (double)CSEd_y->Value, CSEd_w->Value, CSEd_h->Value);
		checkCrVisible();
	}

	FakeControlEnter(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CSEd_xKeyPress(TObject * /* Sender */ , System::WideChar& /* Key */ )
{
	key_press = true;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::FileListBoxClick(TObject*)
{
	if (FileListBox->ItemIndex > -1)
	{
		UnicodeString filename = FileListBox->FileName;
		if (currentFileName != filename)
		{
			delete prev_bmp;
			prev_bmp = new BMPImage(new Graphics::TBitmap());
			if (gUsingOpenGL)
			{
				TRenderGL::FlushRenderNonResManagerImage();
			}
			Graphics::TBitmap *t_prev_bmp = new Graphics::TBitmap();
			if (read_png(filename.c_str(), t_prev_bmp))
			{
				prev_bmp->Assign(t_prev_bmp);
				delete t_prev_bmp;
				currentFileName = filename;
				StatusBar->Panels->Items[0]->Text = Localization::Text(_T("mFile")) + _T(": ") +
													currentFileName + _T(" (W=") + prev_bmp->Width +
													_T(";H=") + prev_bmp->Height + _T(")");
				resetRect();
				if(!mSelectRect.mLockHW)
				{
					Image1->Visible = false;
				}
				setScale(scale[tgBottom], tgBottom);
			}
			else
			{
				delete t_prev_bmp;
				clearAll();
				const UnicodeString ws = Localization::Text(_T("mOpenFileError")) + _T(": ") + filename;
				Application->MessageBox(ws.c_str(), Localization::Text(_T("mError")).c_str(), MB_OK | MB_ICONERROR);
				return;
			}
		}
		else
		{
			//���������� � OnActivate ����� ��������� ��������� ���������� ��� ������������ ���������
			//setScale(scale[tgTop], tgTop);
			//setScale(scale[tgBottom], tgBottom);
		}
	}
	else
	{
		clearAll();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ScrollBox1Resize(TObject*)
{
	if (Image1->Height < ScrollBox1->Height)
	{
		Image1->Top = (ScrollBox1->Height - ScrollBox1->VertScrollBar->Position - Image1->Height) >> 1;
	}
	else
	{
		Image1->Top = -ScrollBox1->VertScrollBar->Position;
	}
	if (Image1->Width < ScrollBox1->Width)
	{
		Image1->Left = (ScrollBox1->Width - ScrollBox1->HorzScrollBar->Position - Image1->Width) >> 1;
	}
	else
	{
		Image1->Left = -ScrollBox1->HorzScrollBar->Position;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::PaintBox1MouseEnter(TObject *)
{
	CheckCursorView();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::PaintBox1MouseLeave(TObject *)
{
	CheckCursorView();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::Sh_tMouseDown(TObject *, TMouseButton Button, TShiftState shstate, int X, int Y)
{
	if (mpActiveItem == NULL)
	{
		return;
	}

	CheckCursorView();

	if (Button == mbRight)
	{
		mLMBtn.moved = false;
		mLMBtn.pressed = false;
		mSelectRect.mSet = false;
	}
	else
	if (Button == mbLeft)
	{
		const __int32 cpx = X + HorzScrollBar->Position - BORDER;
		const __int32 cpy = Y + VertScrollBar->Position - BORDER;
		if (cpx < HorzScrollBar->Max && cpx >= 0 && cpy < VertScrollBar->Max && cpy >= 0)
		{
			mLMBtn.pos.x = mLMBtn.prev.x = X;
			mLMBtn.pos.y = mLMBtn.prev.y = Y;
			mLMBtn.moved = false;
			mLMBtn.pressed = true;
			mSelectRect.mSet = false;
			if(shstate.Contains(ssCtrl) == false)
			{
				mLMBtn.moved = false;
				mLMBtn.pressed = true;
				mSelectRect.mSet = false;
				if(mSelectRect.mLockHW)
				{
					SafePasteBGRect((double)cpx / (double)scale[tgBottom],
										(double)cpy / (double)scale[tgBottom]);
				}
				else
				if(PaintBox1->Tag == NativeInt(-1))
				{
					mSelectRect.mRight = mSelectRect.mLeft = (double)cpx / (double)scale[tgBottom];
					mSelectRect.mBottom = mSelectRect.mTop = (double)cpy / (double)scale[tgBottom];
					mSelectRect.mSet = true;
					mSelectRect.mPos0.x = X;
					mSelectRect.mPos0.y = Y;
				}
			}
		}
	}

	if(FakeControl->Focused() == false)
	{
		FakeControl->SetFocus();
    }
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::Sh_tMouseUp(TObject *Sender, TMouseButton, TShiftState Shift, int X, int Y)
{
	if (mpActiveItem != NULL)
	{
		PaintBox1MouseMove(Sender, Shift, X, Y);
	}
	mLMBtn.moved = false;
	mLMBtn.pressed = false;
	mSelectRect.mSet = false;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::PaintBox1MouseMove(TObject *, TShiftState shstate, int X, int Y)
{
	if (mpActiveItem == NULL)
	{
		return;
	}

	if(!mLMBtn.pressed)
	{
		CheckCursorView();
	}

	const __int32 cpx = X + HorzScrollBar->Position - BORDER;
	const __int32 cpy = Y + VertScrollBar->Position - BORDER;
	if (mSelectRect.mSet && !mSelectRect.mLockHW)
	{
		TRect r;
		if (X > mSelectRect.mPos0.x)
		{
			r.Left = mSelectRect.mPos0.x;
			r.Right = X;
		}
		else
		{
			r.Right = mSelectRect.mPos0.x;
			r.Left = X;
		}
		if (Y > mSelectRect.mPos0.y)
		{
			r.Top = mSelectRect.mPos0.y;
			r.Bottom = Y;
		}
		else
		{
			r.Bottom = mSelectRect.mPos0.y;
			r.Top = Y;
		}
		setRectSel_LTRB((double)(r.Left + HorzScrollBar->Position - BORDER) / (double)scale[tgBottom],
				   (double)(r.Top + VertScrollBar->Position - BORDER) / (double)scale[tgBottom],
				   (double)(r.Right + HorzScrollBar->Position - BORDER) / (double)scale[tgBottom],
				   (double)(r.Bottom + VertScrollBar->Position - BORDER) / (double)scale[tgBottom]);
	}
	else
	if(mLMBtn.pressed)
	{
		if(shstate.Contains(ssCtrl))
		{
			const double cpx_dx = mSelectRect.mLeft + (double)(X - mLMBtn.pos.x) / (double)scale[tgBottom];
			const double cpy_dx = mSelectRect.mTop + (double)(Y - mLMBtn.pos.y) / (double)scale[tgBottom];
			SafePasteBGRect(cpx_dx, cpy_dx);
		}
		else
		{
			const double nx = (double)cpx / (double)scale[tgBottom];
			const double ny = (double)cpy / (double)scale[tgBottom];
			if (mSelectRect.mLockHW)
			{
				if(PaintBox1->Tag != NativeInt(-1))
				{
					setRectSel_XYWH(nx, ny, mSelectRect.getWidth(), mSelectRect.getHeight());
				}
			}
			else
			{
				switch(PaintBox1->Tag)
				{
					case sTop:
						setRectSel_LTRB(mSelectRect.mLeft, ny, mSelectRect.mRight, mSelectRect.mBottom);
						break;
					case sBottom:
						if(mSelectRect.mTop <= ny)
						{
							setRectSel_LTRB(mSelectRect.mLeft, mSelectRect.mTop, mSelectRect.mRight, ny);
						}
						break;
					case sLeft:
						setRectSel_LTRB(nx, mSelectRect.mTop, mSelectRect.mRight, mSelectRect.mBottom);
						break;
					case sRight:
						if(mSelectRect.mLeft <= nx)
						{
							setRectSel_LTRB(mSelectRect.mLeft, mSelectRect.mTop, nx, mSelectRect.mBottom);
						}
						break;
				}
			}
		}
	}
	mLMBtn.pos.x = X;
	mLMBtn.pos.y = Y;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CheckCursorView()
{
	if(mSelectRect.getWidth() != 0 && mSelectRect.getHeight() != 0)
	{
		TPoint p;
		TRect r;
		p = PaintBox1->ScreenToClient(Mouse->CursorPos);
		p.x += HorzScrollBar->Position;
		p.y += VertScrollBar->Position;
		r.Left = (__int32)mSelectRect.mLeft * scale[tgBottom] + BORDER;
		r.Top = (__int32)mSelectRect.mTop * scale[tgBottom] + BORDER;
		r.Right = r.Left + mSelectRect.getWidth() * scale[tgBottom];
		r.Bottom = r.Top + mSelectRect.getHeight() * scale[tgBottom];
		if(p.y >= r.Top - 1 && p.y <= r.Bottom + 1)
		{
			if(p.x >= r.Left - 1 && p.x <= r.Left + 1)
			{
				PaintBox1->Tag = NativeInt(sLeft);
				PaintBox1->Cursor = mSelectRect.mLockHW ? crSizeAll : crSizeWE;
				return;
			}
			else
			if(p.x >= r.Right - 1 && p.x <= r.Right + 1)
			{
				PaintBox1->Tag = NativeInt(sRight);
				PaintBox1->Cursor = mSelectRect.mLockHW ? crSizeAll : crSizeWE;
				return;
			}
		}
		if(p.x >= r.Left - 1 && p.x <= r.Right + 1)
		{
			if(p.y >= r.Top - 1 && p.y <= r.Top + 1)
			{
				PaintBox1->Tag = NativeInt(sTop);
				PaintBox1->Cursor = mSelectRect.mLockHW ? crSizeAll : crSizeNS;
				return;
			}
			else
			if(p.y >= r.Bottom - 1 && p.y <= r.Bottom + 1)
			{
				PaintBox1->Tag = NativeInt(sBottom);
				PaintBox1->Cursor = mSelectRect.mLockHW ? crSizeAll : crSizeNS;
				return;
			}
		}
    }
	PaintBox1->Cursor = crDefault;
	PaintBox1->Tag = NativeInt(-1);
}
//---------------------------------------------------------------------------

bool __fastcall TFEditGrObj::CheckBounds_LTRB(double &l, double &t, double &r, double &b)
{
	bool res = true;
	const __int32 maxR = prev_bmp->Width;
	const __int32 maxB = prev_bmp->Height;
	if (l < 0.0)
	{
		l = 0.0;
		if(mSelectRect.mLockHW == true)
		{
			r = l + (double)mSelectRect.mFixedWidth;
		}
		res = false;
	}
	if (r > (double)maxR)
	{
		r = (double)maxR;
		if(mSelectRect.mLockHW == true)
		{
			l = r - (double)mSelectRect.mFixedWidth;
		}
		res = false;
	}
	if (l > r)
	{
		l = r;
		if(mSelectRect.mLockHW == true)
		{
			l = r - (double)mSelectRect.mFixedWidth;
		}
		res = false;
	}

	if (t < 0.0)
	{
		t = 0.0;
		if(mSelectRect.mLockHW == true)
		{
			b = t + (double)mSelectRect.mFixedHeight;
		}
		res = false;
	}
	if (b > (double)maxB)
	{
		b = (double)maxB;
		if(mSelectRect.mLockHW == true)
		{
			t = b - (double)mSelectRect.mFixedHeight;
		}
		res = false;
	}
	if (t > b)
	{
		t = b;
		if(mSelectRect.mLockHW == true)
		{
			t = b - (double)mSelectRect.mFixedHeight;
		}
		res = false;
	}

	if(mSelectRect.mLockHW == true)
	{
		if(mSelectRect.mFixedHeight >= prev_bmp->Height)
		{
			t = 0.0;
			b = maxB;
		}
		if(mSelectRect.mFixedWidth >= prev_bmp->Width)
		{
			l = 0.0;
			r = maxR;
		}
	}

	return res;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::setRectSel_LTRB(double left, double top, double right, double bottom)
{
	CheckBounds_LTRB(left, top, right, bottom);

	mSelectRect.mLeft = left;
	mSelectRect.mTop = top;
	mSelectRect.mRight = right;
	mSelectRect.mBottom = bottom;

	checkCrVisible();
	mUpdateTimer = REFRESH_TIME_MS;
	CreatePreview();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::setRectSel_XYWH(double x, double y, __int32 w, __int32 h)
{
	setRectSel_LTRB(x, y, (double)((__int32)x + w), (double)((__int32)y + h));
}
//---------------------------------------------------------------------------

bool __fastcall TFEditGrObj::SafePasteBGRect(double x, double y)
{
	const __int32 w0 = mSelectRect.getWidth();
	const __int32 h0 = mSelectRect.getHeight();
	if (x < 0.0)
	{
		x = 0.0;
	}
	if (x + (double)w0 > (double)prev_bmp->Width)
	{
		x = (double)(prev_bmp->Width - w0);
	}
	if (x < 0.0)
	{
		return false;
	}
	if (y < 0.0)
	{
		y = 0.0;
	}
	if (y + (double)h0 > (double)prev_bmp->Height)
	{
		y = (double)(prev_bmp->Height - h0);
	}
	if (y < 0.0)
	{
		return false;
	}
	setRectSel_XYWH(x, y, w0, h0);
	return true;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::Bt_upClick(TObject * /* Sender */ )
{
	const __int32 val = (__int32)mSelectRect.mTop - mSelectRect.getHeight();
	if(val >= 0)
	{
		SafePasteBGRect((double)((__int32)mSelectRect.mLeft), (double)val);
		FindSelection();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::Bt_downClick(TObject * /* Sender */ )
{
	if((__int32)mSelectRect.mBottom + mSelectRect.getHeight() <= prev_bmp->Height)
	{
		SafePasteBGRect((double)((__int32)mSelectRect.mLeft),
						(double)((__int32)mSelectRect.mTop + mSelectRect.getHeight()));
		FindSelection();
    }
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::Bt_leftClick(TObject * /* Sender */ )
{
	const __int32 val = (__int32)mSelectRect.mLeft - mSelectRect.getWidth();
	if(val >= 0)
	{
		SafePasteBGRect((double)val, (double)((__int32)mSelectRect.mTop));
		FindSelection();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::Bt_rightClick(TObject * /* Sender */ )
{
	if((__int32)mSelectRect.mRight + mSelectRect.getWidth() <= prev_bmp->Width)
	{
		SafePasteBGRect((double)((__int32)mSelectRect.mLeft + mSelectRect.getWidth()),
						(double)((__int32)mSelectRect.mTop));
		FindSelection();
    }
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActionFindSelectionExecute(TObject *)
{
	FindSelection();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::setScale(int iscale, int tagWnd)
{
	TTrackBar *tb;
	__int32 width;
	__int32 height;

	switch(tagWnd)
	{
		case tgTop:
			width = mSelectRect.getWidth();
			height = mSelectRect.getHeight();
			tb = TrackBar1;
			break;
		case tgBottom:
			width = prev_bmp->Width;
			height = prev_bmp->Height;
			tb = TrackBar2;
			break;
		default:
			assert(0);
	}

	tb->Position = iscale - 1;

	if (prev_bmp->IsEmpty())
	{
		return;
	}

	if (width * iscale > MAX_SCALE || height * iscale > MAX_SCALE)
	{
		int i = iscale;
		for (; i > 0; i--)
		{
			if (width * i <= MAX_SCALE && height * i <= MAX_SCALE)
			{
				break;
			}
		}
		if (i == 0)
		{
			i = 1;
		}
		iscale = i;
		tb->Position = iscale - 1;
	}

	if (mpActiveItem || MainMenu1->Tag == NativeInt(TFEditGrObjMAINMENUDISABLETAG))
	{
		switch(tagWnd)
		{
			case tgTop:
				if(mSelectRect.mLockHW)
				{
					Image1->Height = mSelectRect.getHeight() * iscale;
					Image1->Width = mSelectRect.getWidth() * iscale;
				}
				else
				{
					Image1->Height = height * iscale;
					Image1->Width = width * iscale;
                }
				ScrollBox1Resize(NULL);
			break;
			case tgBottom:
				//mSelectRect.mRect.Left = iscale > scale[tgBottom] ? (mSelectRect.mRect.Left * iscale) / scale[tgBottom] : (mSelectRect.mRect.Left / scale[tgBottom]) * iscale;
				//mSelectRect.mRect.Top = iscale > scale[tgBottom] ? (mSelectRect.mRect.Top * iscale) / scale[tgBottom] : (mSelectRect.mRect.Top / scale[tgBottom]) * iscale;
				//mSelectRect.mRect.Right = iscale > scale[tgBottom] ? (mSelectRect.mRect.Right * iscale) / scale[tgBottom] : (mSelectRect.mRect.Right / scale[tgBottom]) * iscale;
				//mSelectRect.mRect.Bottom = iscale > scale[tgBottom] ? (mSelectRect.mRect.Bottom * iscale) / scale[tgBottom] : (mSelectRect.mRect.Bottom / scale[tgBottom]) * iscale;
			break;
		}
	}

	scale[tagWnd] = iscale;
	onSelRectResize();

	if(tagWnd == tgBottom)
	{
		HorzScrollBar->PageSize = 0;
		VertScrollBar->PageSize = 0;
		HorzScrollBar->SetParams(0, 0, prev_bmp->Width * iscale + BORDER * 2);
		VertScrollBar->SetParams(0, 0, prev_bmp->Height * iscale + BORDER * 2);
		HorzScrollBar->SmallChange = (unsigned short)(SCROLL_STEP * iscale);
		VertScrollBar->SmallChange = (unsigned short)(SCROLL_STEP * iscale);
		HorzScrollBar->LargeChange = (unsigned short)(SCROLL_STEP * 3 * iscale);
		VertScrollBar->LargeChange = (unsigned short)(SCROLL_STEP * 3 * iscale);
		ChangeSizes();
		FindSelection();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ColorBoxChange(TObject *)
{
	if (CurrentBGColor != ColorBox->Selected)
	{
		CurrentBGColor = ColorBox->Selected;
		if (gUsingOpenGL)
		{
			TRenderGL::SetBackdropImage(NULL, CurrentBGColor);
		}
		CreatePreview();
	}
}
//---------------------------------------------------------------------------

bool __fastcall TFEditGrObj::setSelection(const UnicodeString *file_name, const TRect *sel_rect, TColor BGColor)
{
	int idx;
	if (file_name == NULL)
	{
		return false;
	}
	UnicodeString fname = ExtractFileName(*file_name);
	if (fname.IsEmpty())
	{
		return false;
	}
	fname = forceCorrectResFileExtention(fname);
	idx = FileListBox->Items->IndexOf(fname);
	if (idx < 0)
	{
		return false;
	}
	FileListBox->ItemIndex = idx;
	CurrentBGColor = BGColor;
	FileListBoxClick(NULL);
	if (sel_rect != NULL)
	{
		setRectSel_XYWH((double)(sel_rect->Left), (double)(sel_rect->Top), sel_rect->Width(), sel_rect->Height());
	}
	return true;
}
//---------------------------------------------------------------------------

//�������� ������� ���������� ����������� + ��� �����, ������ ��� � ����������
bool __fastcall TFEditGrObj::getSelection(UnicodeString *file_name, TRect *sel_rect)
{
	if (file_name == NULL || sel_rect == NULL)
	{
		return false;
	}
	if (currentFileName.IsEmpty())
	{
		return false;
	}
	if (mSelectRect.getWidth() == 0 || mSelectRect.getHeight() == 0)
	{
		return false;
    }
	*file_name = currentFileName;
	sel_rect->Top = (__int32)mSelectRect.mLeft;
	sel_rect->Left = (__int32)mSelectRect.mTop;
	sel_rect->Right = (__int32)mSelectRect.mRight;
	sel_rect->Bottom = (__int32)mSelectRect.mBottom;
	return true;
}
//---------------------------------------------------------------------------

//�������� ���������� �����������
bool __fastcall TFEditGrObj::getSelBitmap(BMPImage *out_bmp, const TEDitObjSelectionParams *params)
{
	if (out_bmp == NULL)
	{
		return false;
	}
	TRect rect;
	bool forceLoadImage, res;
	Graphics::TBitmap *t_bmp;

	forceLoadImage = false;

	if (params != NULL)
	{
		if (params->selectionRect.Width() == 0 || params->selectionRect.Height() == 0)
		{
			return false;
		}
		rect.Top = params->selectionRect.Top;
		rect.Left = params->selectionRect.Left;
		rect.Right = params->selectionRect.Right;
		rect.Bottom = params->selectionRect.Bottom;
		forceLoadImage = true;
	}
	else
	{
		if (mSelectRect.getWidth() == 0 || mSelectRect.getHeight() == 0)
		{
			return false;
		}
		rect.Top = (__int32)mSelectRect.mTop;
		rect.Left = (__int32)mSelectRect.mLeft;
		rect.Right = (__int32)mSelectRect.mRight;
		rect.Bottom = (__int32)mSelectRect.mBottom;
	}

	res = true;

	if (forceLoadImage)
	{
		t_bmp = new Graphics::TBitmap();
	}
	else
	{
		t_bmp = const_cast<Graphics::TBitmap*>(prev_bmp->GetBitmapData());
	}

	if ((forceLoadImage) ? read_png(params->fileName.c_str(), t_bmp) : true)
	{
		BLENDFUNCTION bf;
		bf.BlendOp = AC_SRC_OVER;
		bf.BlendFlags = 0;
		bf.SourceConstantAlpha = 255;
		bf.AlphaFormat = AC_SRC_ALPHA;
		out_bmp->SetSize(rect.Width(), rect.Height());
		out_bmp->Clear(TColor(0x00000000));
		::AlphaBlend(out_bmp->Canvas->Handle, 0, 0, rect.Width(), rect.Height(),
						t_bmp->Canvas->Handle, rect.Left, rect.Top, rect.Width(), rect.Height(), bf);
	}
	else
	{
		res = false;
	}

	if (forceLoadImage)
	{
		delete t_bmp;
	}

	return res;
}
//---------------------------------------------------------------------------

//�������� ������� ���������
void __fastcall TFEditGrObj::getSourceImageSize(int *siw, int *sih)
{
	*siw = prev_bmp->Width;
	*sih = prev_bmp->Height;
}
//---------------------------------------------------------------------------

//��� ����, ����� �� ����������� ��������� �������������� � ��������� ������� ����
void __fastcall TFEditGrObj::GetWndParams(TEGrWndParams *params)
{
    UnicodeString name;
	if (params == NULL)
	{
		return;
	}
	params->Left = Left;
	params->Top = Top;
	params->Width = Width;
	params->Height = Height;
	params->scale = TrackBar2->Position + 1;
	params->TrColor = CurrentBGColor;
	name = ExtractFileName(FileListBox->FileName);
	wcscpy(params->LastFileName, name.c_str());
	params->LastSelection.Top = (__int32)mSelectRect.mTop;
	params->LastSelection.Left = (__int32)mSelectRect.mLeft;
	params->LastSelection.Right = (__int32)mSelectRect.mRight;
	params->LastSelection.Bottom = (__int32)mSelectRect.mBottom;
	params->splitterLV = CategoryButtons_s->Width;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::SetWndParams(const TEGrWndParams *params)
{
	if (params == NULL)
	{
		return;
	}
	Left = params->Left;
	Top = params->Top;
	Width = params->Width;
	Height = params->Height;
	setScale(params->scale, tgBottom);
	setScale(1, tgTop);
	ColorBox->Selected = params->TrColor;
	ColorBoxChange(NULL);
	if (wcslen(params->LastFileName) > 0)
	{
		UnicodeString fname = UnicodeString(params->LastFileName);
		setSelection(&fname, &params->LastSelection, params->TrColor);
	}
	CategoryButtons_s->Width = params->splitterLV;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::FormKeyPress(TObject * /* Sender */ , char &Key)
{
	if (Key == VK_ESCAPE)
		Close();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::N3Click(TObject * /* Sender */ )
{
	Close();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActSelectAllExecute(TObject *)
{
	if (!prev_bmp->IsEmpty())
	{
		setRectSel_XYWH(0.0, 0.0, prev_bmp->Width, prev_bmp->Height);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActDeselectAllExecute(TObject * /* Sender */ )
{
	mUpdateTimer = REFRESH_TIME_MS;
	resetRect();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActCopyExecute(TObject * /* Sender */ )
{
	copyRect.Top = (__int32)mSelectRect.mTop;
	copyRect.Left = (__int32)mSelectRect.mLeft;
	copyRect.Right = (__int32)mSelectRect.mRight;
	copyRect.Bottom = (__int32)mSelectRect.mBottom;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActPasteExecute(TObject * /* Sender */ )
{
	if (copyRect.Width() == 0 || copyRect.Height() == 0)
	{
		return;
    }
	if (mSelectRect.mLockHW)
	{
		SafePasteBGRect((double)copyRect.left, (double)copyRect.Top);
	}
	else
	{
		setRectSel_XYWH((double)copyRect.left, (double)copyRect.Top, copyRect.Width(), copyRect.Height());
    }
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::TrackBar1Change(TObject *Sender)
{
	if (mpSizeTrackBar == NULL)
	{
		mSizeTrackBarCounter = SCALE_DELAY_MS;
		mpSizeTrackBar = (TTrackBar*)Sender;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::Panel_mainCanResize(TObject * /* Sender */ , int & /* NewWidth */ , int & /* NewHeight */ , bool & /* Resize */ )
{
	if (Panel2->Height > Panel_main->Height - Splitter1->MinSize)
	{
		int h = Panel_main->Height - Splitter1->MinSize - Splitter1->Height;
		Panel2->Height = h;
		Splitter1->Top = Panel2->Height + Panel2->Top;
		Panel_1->Top = Splitter1->Top + Splitter1->Height;
	}
	ScrollBox1Resize(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActShiftRightExecute(TObject * /* Sender */ )
{
	BitBt_right->Click();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActShiftLeftExecute(TObject * /* Sender */ )
{
	BitBt_left->Click();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActShiftUpExecute(TObject * /* Sender */ )
{
	BitBt_up->Click();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActShiftDownExecute(TObject * /* Sender */ )
{
	BitBt_down->Click();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::FakeControlEnter(TObject *)
{
	FakeControl->Top = (__int32)mSelectRect.mTop + PaintBox1->Top + BORDER + mSelectRect.getHeight() / 2;
	FakeControl->Left = (__int32)mSelectRect.mLeft + PaintBox1->Left + BORDER + mSelectRect.getWidth() / 2;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::FakeControlKeyDown(TObject * /* Sender */ , WORD &Key, TShiftState Shift)
{
	if (mSelectRect.mSet)
	{
		return;
	}

	if (Shift.Contains(ssShift))
	{
		if (Key == VK_UP)
		{
			ActShiftUp->Execute();
			Key = 0;
		}
		else if (Key == VK_DOWN)
		{
			ActShiftDown->Execute();
			Key = 0;
		}
		else if (Key == VK_LEFT)
		{
			ActShiftLeft->Execute();
			Key = 0;
		}
		else if (Key == VK_RIGHT)
		{
			ActShiftRight->Execute();
			Key = 0;
		}
		return;
	}

	if (Shift.Contains(ssCtrl))
	{
		__int32 y = CSEd_h->Value + CSEd_y->Value;
		__int32 x = CSEd_w->Value + CSEd_x->Value;
		if (Key == VK_UP)
		{
			if (y > CSEd_y->Value)
				CSEd_h->Value = CSEd_h->Value - 1;
			Key = 0;
		}
		else if (Key == VK_DOWN)
		{
			if (y < prev_bmp->Height)
				CSEd_h->Value = CSEd_h->Value + 1;
			Key = 0;
		}
		else if (Key == VK_LEFT)
		{
			if (x > CSEd_x->Value)
				CSEd_w->Value = CSEd_w->Value - 1;
			Key = 0;
		}
		else if (Key == VK_RIGHT)
		{
			if (x < prev_bmp->Width)
				CSEd_w->Value = CSEd_w->Value + 1;
			Key = 0;
		}
		return;
	}

	if (Key == VK_UP)
	{
		if (CSEd_y->Value > 0)
			CSEd_y->Value = CSEd_y->Value - 1;
		Key = 0;
	}
	else if (Key == VK_DOWN)
	{
		if (CSEd_y->Value + CSEd_h->Value < prev_bmp->Height)
			CSEd_y->Value = CSEd_y->Value + 1;
		Key = 0;
	}
	else if (Key == VK_LEFT)
	{
		if (CSEd_x->Value > 0)
			CSEd_x->Value = CSEd_x->Value - 1;
		Key = 0;
	}
	else if (Key == VK_RIGHT)
	{
		if (CSEd_x->Value + CSEd_w->Value < prev_bmp->Width)
			CSEd_x->Value = CSEd_x->Value + 1;
		Key = 0;
	}

	Key = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::FakeControlKeyPress(TObject * /* Sender */ , char &Key)
{
	Key = 0;
	FakeControl->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ScrollBox_oldMouseDown(TObject * /* Sender */ , TMouseButton /* Button */ , TShiftState /* Shift */ , int /* X */ , int /* Y */ )
{
	FakeControl->SetFocus();
}
//---------------------------------------------------------------------------

BMPImage *__fastcall TFEditGrObj::GetBitmapForIcon(const TFrameObj *o)
{
	TEDitObjSelectionParams p;
	BMPImage *bmp;
	o->getSourceRes(NULL, NULL, NULL, &p.fileName);
	p.selectionRect.Left = o->x;
	p.selectionRect.Top = o->y;
	p.selectionRect.Right = o->x + o->w;
	p.selectionRect.Bottom = o->y + o->h;
	if(o->getResIdx() >= 0)
	{
		BLENDFUNCTION bf;
		bf.BlendOp = AC_SRC_OVER;
		bf.BlendFlags = 0;
		bf.SourceConstantAlpha = 255;
		bf.AlphaFormat = AC_SRC_ALPHA;
		bmp = new BMPImage(new Graphics::TBitmap());
		bmp->SetSize(o->w, o->h);
		bmp->Clear(TColor(0x00000000));
		Graphics::TBitmap *t_bmp = const_cast<Graphics::TBitmap *>(mpResObjManager->GetGraphicResource(o->getResIdx())->GetBitmapData());
		assert(t_bmp);
		::AlphaBlend(bmp->Canvas->Handle, 0, 0, o->w, o->h, t_bmp->Canvas->Handle, o->x, o->y, o->w, o->h, bf);
	}
	else
	{
        bmp = new BMPImage(new Graphics::TBitmap());
		if (!getSelBitmap(bmp, &p))
		{
			bmp->Assign(static_cast<Graphics::TBitmap*>(NULL));
		}
	}
	return bmp;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::SetImagePreviewMode()
{
	MainMenu1->Tag = NativeInt(TFEditGrObjMAINMENUDISABLETAG);
	MainMenu1->Items->Clear();
	CategoryButtons_s->Enabled = false;
	CategoryButtons_s->Visible = false;
	Panel2->Visible = false;
	SplitterLV->Visible = false;
	Splitter1->Visible = false;
	GroupBox_p->Visible = false;
	BitBt_down->Visible = false;
	BitBt_up->Visible = false;
	BitBt_left->Visible = false;
	BitBt_right->Visible = false;
    BitBt_findselection->Visible = false;
	Caption = Localization::Text(_T("mChooseGraphicsResource"));
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::SetBGObj(GMapBGObj *obj, int activeIdx, int w, int h)
{
	TTempBGobj *o;
	int ct, i;

	assert(obj);

	mpGameObjGM = obj;
	ct = obj->GetFrameObjCount();
	ClearTempRes();
	for (i = 0; i < ct; i++)
	{
		o = new TTempBGobj();
		//o->o = new TFrameObj();
		o->bi = NULL;
		if(obj->GetFrameObj(i) != NULL)
		{
			obj->GetFrameObj(i)->copyTo(o->o);
        }
		temp_res->Add(o);
	}

	CSEd_w->Enabled = false;
	CSEd_h->Enabled = false;
	CSEd_w->Value = w;
	CSEd_h->Value = h;
	CSEd_xChange(CSEd_h);
	ClearImageListBG();
	mSelectRect.mLockHW = true;
	mSelectRect.mRight = (double)w;
	mSelectRect.mBottom = (double)h;
	mSelectRect.mFixedWidth = w;
	mSelectRect.mFixedHeight = h;
	Image1->Visible = true;
	ActSelectAll->Enabled = false;
	ActDeselectAll->Enabled = false;
	ImageList->Width = w > MAX_PREWIEV_SIZE ? MAX_PREWIEV_SIZE : w;
	ImageList->Height = h > MAX_PREWIEV_SIZE ? MAX_PREWIEV_SIZE : h;
	CategoryButtons_s->ButtonWidth = ImageList->Width + 6;
	CategoryButtons_s->ButtonHeight = ImageList->Height + 6;
	ActDel->Visible = false;
	ActAdd->Visible = false;
	ActDel->Enabled = false;
	ActAdd->Enabled = false;
	ActBGObjShiftLeft->Enabled = true;
	ActBGObjShiftRight->Enabled = true;
	ActBGObjShiftLeft->Visible = true;
	ActBGObjShiftRight->Visible = true;
	ActGroupCreate->Enabled = false;
	ActGroupDelete->Enabled = false;
	ActGroupCreate->Visible = false;
	ActGroupDelete->Visible = false;

	CreateListView(CategoryButtons_s, ImageList, temp_res);
	if (activeIdx >= 0)
	{
		SetActiveBGIndex(activeIdx);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ApplyChangesToBGObj()
{
	if (mpGameObjGM == NULL)
	{
		return;
	}

	TTempBGobj *o;
	int ct, i, g_idx, w, h;
	hash_t hashBmp;
	UnicodeString fname;

	if (mpGameObjGM != NULL)
	{
		mpGameObjGM->DelAllFrameObj();
		ct = temp_res->Count;
		for (i = 0; i < ct; i++)
		{
			o = (TTempBGobj*)temp_res->Items[i];
			o->o.getSourceRes(&w, &h, NULL, &fname);
			if (!fname.IsEmpty())
			{
				assert(mpResObjManager);
				if ((g_idx = mpResObjManager->GetGraphicResourceIndex(fname)) < 0)
				{
					Graphics::TBitmap *bmp = new Graphics::TBitmap();
					UnicodeString path = Form_m->PathPngRes + fname;
					read_png(path.c_str(), bmp);
					BMPImage *i2d = new BMPImage(bmp);
					g_idx = mpResObjManager->AddGraphicResource(i2d, fname);
					o->o.setSourceRes(bmp->Width, bmp->Height, g_idx, fname);
					delete i2d;
				}
				else
				{
					o->o.setSourceRes(w, h, g_idx, fname);
				}
			}
			TFrameObj *fo = new TFrameObj();
			o->o.copyTo(*fo);
			mpGameObjGM->AssignFrameObj(fo, i);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::SetObjList(hash_t activeObjId, const TList *iobjList)
{
	int ct, i;
	TTempBGobj *o, *fo;
	TList *hiobjList = const_cast<TList*>(iobjList);

	ActBGObjShiftLeft->Enabled = false;
	ActBGObjShiftRight->Enabled = false;
	ActBGObjShiftLeft->Visible = false;
	ActBGObjShiftRight->Visible = false;
	ActGroupCreate->Enabled = true;
	ActGroupDelete->Enabled = true;
	ActGroupCreate->Visible = true;
	ActGroupDelete->Visible = true;
	ClearImageListBG();
	mSelectRect.mLockHW = false;
	mpGameObjGM = NULL;

	ct = iobjList->Count;
	ClearTempRes();
	for (i = 0; i < ct; i++)
	{
		fo = static_cast<TTempBGobj*>(hiobjList->Items[i]);
		if(fo->o.w > 0 && fo->o.h > 0)
		{
			int w, h;
			UnicodeString fname;
			o = new TTempBGobj();
			o->oldHash = fo->oldHash;
            o->oldGroup = fo->oldGroup;
			o->bi = NULL;
			fo->o.copyTo(o->o);
			o->o.getSourceRes(&w, &h, NULL, &fname);
			o->o.setSourceRes(w, h, -1, fname);
			temp_res->Add(o);
		}
	}

	CreateListView(CategoryButtons_s, ImageList, temp_res);
	ct = temp_res->Count;
	for (i = 0; i < ct; i++)
	{
		o = static_cast<TTempBGobj*>(temp_res->Items[i]);
		if (o->oldHash == activeObjId)
		{
			OnActivateItem(o->bi);
			if(o->bi != NULL)
			{
				CategoryButtons_s->SelectedItem = o->bi;
				o->bi->Category->Collapsed = false;
				CategoryButtons_s->SelectedItem->ScrollIntoView();
            }
			break;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::GetObjListChanges(TList *oobjList)
{
	if (mpGameObjGM == NULL)
	{
		TTempBGobj *fo, *o;
		assert(oobjList->Count == 0);
		int ct = temp_res->Count;
		for (int i = 0; i < ct; i++)
		{
			fo = static_cast<TTempBGobj*>(temp_res->Items[i]);
			if(fo->o.w == 0 || fo->o.h == 0)
			{
				fo->o.w = fo->o.h = 0;
			}
			if(fo->oldHash == 0 && fo->o.w == 0)
			{
				continue;
			}
			if(fo->oldHash == 0 || fo->oldHash != fo->o.getHash() || fo->oldGroup != fo->o.groupId)
			{
				bool res = true;
				if(fo->o.w > 0 && fo->o.h > 0)
				{
					hash_t h = fo->o.getHash();
					for(int j = 0; j < oobjList->Count; j++)
					{
						o = static_cast<TTempBGobj*>(oobjList->Items[j]);
						if(h == o->o.getHash())
						{
                            res = false;
							break;
						}
					}
				}
				if(res == true)
				{
					int w, h, g_idx;
					UnicodeString fname;
					o = new TTempBGobj();
					//o->o = new TFrameObj();
					o->oldHash = fo->oldHash;
					o->oldGroup = fo->oldGroup;
					o->bi = NULL;
					fo->o.copyTo(o->o);
					oobjList->Add(o);
					o->o.getSourceRes(&w, &h, NULL, &fname);
					if (!fname.IsEmpty())
					{
						assert(mpResObjManager);
						if ((g_idx = mpResObjManager->GetGraphicResourceIndex(fname)) < 0)
						{
							Graphics::TBitmap *bmp = new Graphics::TBitmap();
							UnicodeString path = Form_m->PathPngRes + fname;
							read_png(path.c_str(), bmp);
							BMPImage *i2d = new BMPImage(bmp);
							g_idx = mpResObjManager->AddGraphicResource(i2d, fname);
							o->o.setSourceRes(bmp->Width, bmp->Height, g_idx, fname);
							delete i2d;
						}
						else
						{
							o->o.setSourceRes(w, h, g_idx, fname);
						}
					}
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::SetActiveBGIndex(int idx)
{
	assert(CategoryButtons_s->Categories->Count != 0);
	TButtonCollection *bc = CategoryButtons_s->Categories->Items[0]->Items;
	if (bc != NULL)
	{
		if (idx >= 0 && idx < CategoryButtons_s->Categories->Items[0]->Items->Count)
		{
			CategoryButtons_s->SelectedItem = bc->Items[idx];
			OnActivateItem(bc->Items[idx]);
			return;
		}
	}
	CategoryButtons_s->SelectedItem = NULL;
	CategoryButtons_s->FocusedItem = NULL;
	OnActivateItem(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::OnActivateItem(const TButtonItem *bi)
{
	if (CategoryButtons_s->Visible == false)
	{
		return;
	}
	if (mpActiveItem != NULL && mUpdateTimer > 0)
	{
		mUpdateTimer = 1;
		ProcessTimer(1);
	}
	if (bi == NULL)
	{
		mpActiveItem = NULL;
		clearAll();
		return;
	}
	UnicodeString img_source_name;
	void *ptr = mpActiveItem;
	mpActiveItem = (TTempBGobj*)bi->Data;
	TTempBGobj *o = mpActiveItem;
	o->o.getSourceRes(NULL, NULL, NULL, &img_source_name);
	if (!img_source_name.IsEmpty())
	{
		TRect rect;
		rect.Left = o->o.x;
		rect.Top = o->o.y;
		rect.Right = o->o.x + o->o.w;
		rect.Bottom = o->o.y + o->o.h;
		setSelection(&img_source_name, &rect, ColorBox->Selected);

		if(ptr == NULL && mpActiveItem)
		{
			setScale(scale[tgBottom], tgBottom);
			setScale(scale[tgTop], tgTop);
		}
		else
		{
			FindSelection();
        }
	}
	else
	{
		if(mSelectRect.mLockHW == false)
		{
			clearAll();
			Image1->Visible = false;
			setScale(scale[tgBottom], tgBottom);
		}
    }
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::FindSelection()
{
	if(mSelectRect.getHeight() == 0 || mSelectRect.getWidth() == 0)
	{
		return;
    }

	__int32 pos = HorzScrollBar->Position;
	if (pos + HorzScrollBar->Width < (__int32)mSelectRect.mRight * scale[tgBottom])
	{
		pos = (__int32)mSelectRect.mRight * scale[tgBottom] - HorzScrollBar->Width + BORDER * 2;
	}
	else if (pos > (__int32)mSelectRect.mLeft * scale[tgBottom])
	{
		pos = (__int32)mSelectRect.mLeft * scale[tgBottom];
	}
	if(pos != HorzScrollBar->Position)
	{
		HorzScrollBarScroll(HorzScrollBar, scTrack, pos);
		HorzScrollBar->Position = pos;
    }

	pos = VertScrollBar->Position;
	if (pos + VertScrollBar->Height < (__int32)mSelectRect.mBottom * scale[tgBottom])
	{
		pos = (__int32)mSelectRect.mBottom * scale[tgBottom] - VertScrollBar->Height + BORDER * 2;
	}
	else if (pos > (__int32)mSelectRect.mTop * scale[tgBottom])
	{
		pos = (__int32)mSelectRect.mTop * scale[tgBottom];
	}
	if(pos != VertScrollBar->Position)
	{
		HorzScrollBarScroll(VertScrollBar, scTrack, pos);
		VertScrollBar->Position = pos;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CreateListView(TCategoryButtons *cd, TImageList *il, TList *res)
{
	int i, ct, pos;
	TTempBGobj *o;
	TListItem *li;
	UnicodeString caption;

	il->Clear();
	ct = res->Count;
	il->AllocBy = ct;
	if(il == ImageList && mSelectRect.mLockHW)
	{
		ClearImageListBG();
		InitImageListBG(ct);
	}

	cd->Visible = false;
	cd->SelectedItem = NULL;
	cd->FocusedItem = NULL;
	while ((pos = cd->Categories->Count - 1) >= 0)
	{
		cd->Categories->Items[pos]->Items->Clear();
		cd->Categories->Delete(pos);
	}
	cd->Categories->Add();
	if (mSelectRect.mLockHW)
	{
		cd->Categories->Items[0]->Caption = Localization::Text(_T("mLayers"));
	}
	else
	{
		cd->Categories->Items[0]->Caption = Localization::Text(_T("mGlobalCommon"));
	}

	for (i = 0; i < ct; i++)
	{
		o = static_cast<TTempBGobj*>(res->Items[i]);
		if(mSelectRect.mLockHW || (o->o.w > 0 && o->o.h > 0))
		{
			o->o.getSourceRes(NULL, NULL, NULL, &caption);
			if (mSelectRect.mLockHW || (!caption.IsEmpty() && caption[1] != L'?'))//? - hack. define sp obj.
			{
				EditItem(o, NULL, mAdd, cd, il);
			}
		}
	}
	cd->Visible = true;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::EditItem(TTempBGobj *item, BMPImage *bmp, int mode, TCategoryButtons *cd, TImageList *il)
{
	if (mode == mDel)
	{
		mpActiveItem = NULL;
		il->Replace(item->bi->ImageIndex, NULL, NULL);
		item->bi->Data = NULL;
		cd->Categories->Items[item->bi->Category->Index]->Items->Delete(item->bi->Index);
		if(mSelectRect.mLockHW)
		{
			ClearImageListBGItem(item->bi->ImageIndex);
		}
		item->bi = NULL;
		item->o.w = item->o.h = 0;
		return;
	}

	int pos = 0;
	if (!item->o.groupId.IsEmpty())
	{
		pos = cd->Categories->IndexOf(item->o.groupId);
		if (pos < 0)
		{
			TButtonCategory *bc = cd->Categories->Add();
			pos = bc->Index;
			bc->Caption = item->o.groupId;
		}
	}
	if (mode == mAdd)
	{
		item->bi = cd->Categories->Items[pos]->Items->Add();
		if(cd->Categories->Items[0] == item->bi->Category)
		{
			item->o.groupId = _T("");
		}
		else
		{
			item->o.groupId = item->bi->Category->Caption;
		}
	}

	int p_w = il->Width;
	int p_h = il->Height;
	bool delBmp = false;
	if (bmp == NULL)
	{
		bmp = GetBitmapForIcon(&item->o);
		delBmp = true;
	}
	if (bmp->IsEmpty())
	{
		Form_m->makeEmptyPreview(bmp, _T("x"), p_w, p_h);
		item->bi->Hint = _T("");
		if(!mSelectRect.mLockHW)
		{
			Image1->Visible = false;
        }
	}
	else
	{
		if (item->o.w != p_w || item->o.h != p_h)
		{
			Form_m->resizeBmpForListView(bmp, p_w, p_h, 1.0f);
		}
		item->bi->Hint = IntToStr(item->o.w) + _T("x") + IntToStr(item->o.h);
	}

	item->bi->Data = item;
	int bgidx = -1;
	switch(mode)
	{
		case mAdd:
			item->bi->ImageIndex = il->Add(const_cast<Graphics::TBitmap*>(bmp->GetBitmapData()), NULL);
			bgidx = il->Count - 1;
		break;
		case mEdit:
			il->Replace(item->bi->ImageIndex, const_cast<Graphics::TBitmap*>(bmp->GetBitmapData()), NULL);
			bgidx = item->bi->ImageIndex;
	}

	if(il == ImageList && mSelectRect.mLockHW)
	{
		assert(bgidx != -1);
		const BMPImage* res_img = mpResObjManager->GetGraphicResource(item->o.getResIdx());
		if(res_img != NULL)
		{
			AssignToImageListBG(bgidx, res_img, false);
		}
		else
		{
			UnicodeString fname;
			item->o.getSourceRes(NULL, NULL, NULL, &fname);
			if(!fname.IsEmpty())
			{
				Graphics::TBitmap *t_prev_bmp = new Graphics::TBitmap();
				UnicodeString path = Form_m->PathPngRes + fname;
				if(read_png(path.c_str(), t_prev_bmp))
				{
					res_img = new BMPImage(t_prev_bmp);
					AssignToImageListBG(bgidx, res_img, true);
				}
				else
				{
					delete t_prev_bmp;
					ClearImageListBGItem(bgidx);
				}
			}
			else
			{
				ClearImageListBGItem(bgidx);
			}
		}
	}

	if (delBmp)
	{
		delete bmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CreatePreview()
{
	if(mCreatePreviewCounter < 0)
	{
		mCreatePreviewCounter = CREATE_PREVIEW_MS;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CategoryButtons_sDrawIcon(TObject *Sender, const TButtonItem *Button, TCanvas *Canvas, TRect &Rect, TButtonDrawState State, int &TextOffset)
{
	CBDrawIcon(Sender, Button, Canvas, Rect, State, TextOffset);
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CategoryButtons_sEnter(TObject *)
{
	CategoryButtons_s->Color = clWindow;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CategoryButtons_sExit(TObject *)
{
	CategoryButtons_s->Color = clBtnFace;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CategoryButtons_sSelectedItemChange(TObject *, const TButtonItem *Button)
{
	OnActivateItem(Button);
	CreatePopupMoveTo();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CategoryButtons_sSelectedCategoryChange(TObject*, const TButtonCategory*)
{
	OnActivateItem(NULL);
	CreatePopupMoveTo();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActBGObjShiftLeftExecute(TObject*)
{
	TButtonCategory *cc = CategoryButtons_s->CurrentCategory;
	if (cc != NULL && cc == CategoryButtons_s->Categories->Items[0])
	{
		TBaseItem *li = CategoryButtons_s->SelectedItem;
		if (li != NULL)
		{
			int idx = li->Index;
			if (idx > 0 && temp_res->Count > 0)
			{
				temp_res->Move(idx, idx - 1);
				li->Index = idx - 1;
				CreatePreview();
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActBGObjShiftRightExecute(TObject*)
{
	TButtonCategory *cc = CategoryButtons_s->CurrentCategory;
	if (cc != NULL && cc == CategoryButtons_s->Categories->Items[0])
	{
		TBaseItem *li = CategoryButtons_s->SelectedItem;
		if (li != NULL)
		{
			int idx = li->Index;
			if (idx < temp_res->Count - 1 && temp_res->Count > 0)
			{
				temp_res->Move(idx, idx + 1);
				li->Index = idx + 1;
				CreatePreview();
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActAddExecute(TObject*)
{
	if (CategoryButtons_s->Visible == false || CategoryButtons_s->CurrentCategory == NULL)
	{
		return;
	}
	TTempBGobj *o = new TTempBGobj();
	o->oldHash = 0;
	o->o.groupId = CategoryButtons_s->CurrentCategory->Caption;
	o->oldGroup = o->o.groupId;
	temp_res->Add(o);
	EditItem(o, NULL, mAdd, CategoryButtons_s, ImageList);
	OnActivateItem(o->bi);
	CategoryButtons_s->SelectedItem = o->bi;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActDelExecute(TObject*)
{
	TBaseItem *li = CategoryButtons_s->SelectedItem;
	if(li != NULL && li->ClassNameIs(_T("TButtonItem")))
	{
		TButtonItem *bi = static_cast<TButtonItem*>(li);
		int idx = li->Index;
		TButtonCategory *bc = bi->Category;
		EditItem(static_cast<TTempBGobj*>(bi->Data), NULL, mDel, CategoryButtons_s, ImageList);
		if(bc->Items->Count > idx)
		{
			bi = bc->Items->Items[idx];
			CategoryButtons_s->SelectedItem = bi;
			OnActivateItem(bi);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActClearExecute(TObject*)
{
	TBaseItem *li;
	UnicodeString t_bgPropertyName;
	li = CategoryButtons_s->SelectedItem;
	if (li == NULL)
	{
		return;
	}
	int idx = li->Index;
	TTempBGobj *o = (TTempBGobj*)temp_res->Items[idx];
	t_bgPropertyName = o->o.bgPropertyName;
    o->o.Clear();
	o->o.bgPropertyName = t_bgPropertyName;
	EditItem((TTempBGobj*)temp_res->Items[idx], NULL, mEdit, CategoryButtons_s, ImageList);
	clearAll();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CategoryButtons_sKeyDown(TObject*, WORD &Key, TShiftState Shift)
{
	if (Shift.Contains(ssShift) && mSelectRect.mLockHW)
	{
		if (Key == VK_LEFT)
		{
			Key = 0;
			ActBGObjShiftLeft->Execute();
		}
		else if (Key == VK_RIGHT)
		{
			Key = 0;
			ActBGObjShiftRight->Execute();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CategoryButtons_sAfterDrawButton(TObject *, const TButtonItem *Button, TCanvas *Canvas, TRect &Rect, TButtonDrawState)
{
	if (mpActiveItem != NULL)
	{
		if (Button == mpActiveItem->bi)
		{
			Canvas->DrawFocusRect(Rect);
		}
	}
}
//---------------------------------------------------------------------------

//Groups

//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActGroupCreateExecute(TObject *)
{
	UnicodeString name;
	bool res;
	int pos;
	res = true;
	Application->CreateForm(__classid(TFOPEdit), &FOPEdit);
	FOPEdit->ButtonOk->ModalResult = mrOk;
	FOPEdit->SetMode(TFOPEdit::TFOPEMLanguages);
	FOPEdit->Caption = ActGroupCreate->Caption;
	FOPEdit->Label1->Caption = Localization::Text(_T("mGroupName"));
	do
	{
		if (FOPEdit->ShowModal() == mrOk)
		{
			pos = -1;
			name = FOPEdit->GetVarName();
			if (!name.IsEmpty() && (pos = CategoryButtons_s->Categories->IndexOf(name)) < 0)
			{
				CategoryButtons_s->Visible = false;
				CategoryButtons_s->Categories->Add()->Caption = name;
                CategoryButtons_s->Visible = true;
				res = false;
			}
			if (res && pos >= 0)
			{
				Application->MessageBox(Localization::Text(_T("mGroupNameExist")).c_str(),
										Localization::Text(_T("mMessageBoxWarning")).c_str(), MB_OK | MB_ICONERROR);
			}
		}
		else
		{
			res = false;
		}
	}
	while (res);
	FOPEdit->Free();
	FOPEdit = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ActGroupDeleteExecute(TObject *)
{
	TButtonCategory *cc = CategoryButtons_s->CurrentCategory;
	if (cc == NULL || (CategoryButtons_s->Categories->Count > 0 &&
		cc == CategoryButtons_s->Categories->Items[0]))
	{
		return;
	}
	if (cc->Items->Count > 0)
	{
		if (Application->MessageBox(Localization::Text(_T("mGraphicFtagmentsGroupDeleteWarning")).c_str(),
									Localization::Text(_T("mDelete")).c_str(), MB_YESNO | MB_ICONQUESTION) == ID_YES)
		{
			while(cc->Items->Count > 0)
			{
				EditItem(static_cast<TTempBGobj*>(cc->Items->Items[cc->Items->Count - 1]->Data), NULL, mDel, CategoryButtons_s, ImageList);
			}
		}
		else
		{
			return;
		}
	}
	assert(cc->Items->Count == 0);
	CategoryButtons_s->Visible = false;
	CategoryButtons_s->Categories->Delete(cc->Index);
	CategoryButtons_s->Visible = true;
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::MainMenu1Change(TObject*, TMenuItem*, bool)
{
	PopupMenuPopup(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::PopupMenuPopup(TObject *)
{
	if(MainMenu1->Tag == NativeInt(TFEditGrObjMAINMENUDISABLETAG))
	{
        return;
    }

	if (mSelectRect.mLockHW)
	{
		NEGOMovetoSeparator->Visible = false;
		NEGOMoveto->Visible = false;
		NEGOMMMovetoSeparator->Visible = false;
		NEGOMMMoveto->Visible = false;
	}
	else
	{
		TBaseItem *li = CategoryButtons_s->SelectedItem;
		NEGOMovetoSeparator->Visible = true;
		NEGOMMMovetoSeparator->Visible = true;
		NEGOMoveto->Enabled = mpActiveItem != NULL && CategoryButtons_s->Categories->Count > 1 && li != NULL;
		NEGOMMMoveto->Enabled = NEGOMoveto->Enabled;
		ActGroupDelete->Enabled = CategoryButtons_s->CurrentCategory != NULL && (CategoryButtons_s->Categories->Count > 0 && CategoryButtons_s->CurrentCategory != CategoryButtons_s->Categories->Items[0]);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CreatePopupMoveTo()
{
	if(MainMenu1->Tag == NativeInt(TFEditGrObjMAINMENUDISABLETAG))
	{
        return;
	}

	TMenuItem *mi;
	UnicodeString name;
	int i, len;
	len = CategoryButtons_s->Categories->Count;
	FreePopupMoveTo();
	NEGOMoveto->AutoHotkeys = maManual;
	if (len <= 1)
	{
		return;
	}
	for (i = 0; i < len; i++)
	{
		if (CategoryButtons_s->Categories->Items[i] == CategoryButtons_s->CurrentCategory)
		{
			continue;
		}
		name = CategoryButtons_s->Categories->Items[i]->Caption;
		mi = new TMenuItem(NEGOMoveto);
		mi->AutoHotkeys = maManual;
		mi->Caption = name;
		mi->Hint = name;
		mi->OnClick = NPopupMoveToClick;
		mi->Tag = NativeInt(i);
		NEGOMoveto->Add(mi);

		mi = new TMenuItem(NEGOMMMoveto);
		mi->AutoHotkeys = maManual;
		mi->Caption = name;
		mi->Hint = name;
		mi->OnClick = NPopupMoveToClick;
		mi->Tag = NativeInt(i);
		NEGOMMMoveto->Add(mi);
	}
	PopupMenuPopup(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::FreePopupMoveTo()
{
	if(MainMenu1->Tag == NativeInt(TFEditGrObjMAINMENUDISABLETAG))
	{
        return;
	}

	for (int i = 0; i < NEGOMoveto->Count; i++)
	{
		delete NEGOMoveto->Items[i];
		delete NEGOMMMoveto->Items[i];
	}
	NEGOMoveto->Clear();
	NEGOMMMoveto->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::NPopupMoveToClick(TObject *Sender)
{
	if(MainMenu1->Tag == NativeInt(TFEditGrObjMAINMENUDISABLETAG))
	{
        return;
	}

	int idx = static_cast<TMenuItem*>(Sender)->Tag;
	TButtonCategory *cc = CategoryButtons_s->Categories->Items[idx];
	if(cc != CategoryButtons_s->CurrentCategory && CategoryButtons_s->SelectedItem != NULL)
	{
		if(CategoryButtons_s->SelectedItem->ClassNameIs(_T("TButtonItem")))
		{
			TButtonItem *bi = static_cast<TButtonItem*>(CategoryButtons_s->SelectedItem);
			UnicodeString grId = _T("");
			if(idx != 0)
			{
				grId = cc->Caption;
			}
			idx = cc->Items->Count - 1;
			if(idx < 0)
			{
				idx = 0;
			}
			cc->Items->AddItem(bi, idx);
			static_cast<TTempBGobj*>(bi->Data)->o.groupId = grId;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::DoTick(unsigned int ms)
{
	ProcessTimer(ms);
	if(gUsingOpenGL)
	{
		DrawFrame();
	}
	else
	{
		mRenderDelay -= ms;
		if(mRenderDelay <= 0)
		{
			mRenderDelay = RENDER_DELAY_MS;
			DrawFrame();
			//Application->ProcessMessages();
		}
    }
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ProcessTimer(unsigned int ms)
{
	if(mCreatePreviewCounter >= 0)
	{
		mCreatePreviewCounter -= ms;
		if(mCreatePreviewCounter <= 0)
		{
            mCreatePreviewCounter = -1;
			DoCreatePreview();
        }
	}

	if (mpSizeTrackBar != NULL && mSizeTrackBarCounter >= 0)
	{
		mSizeTrackBarCounter -= ms;
		if(mSizeTrackBarCounter <= 0)
		{
            mSizeTrackBarCounter = -1;
			setScale(mpSizeTrackBar->Position + 1, mpSizeTrackBar->Tag);
			mpSizeTrackBar = NULL;
		}
	}

	if (mpActiveItem != NULL && mUpdateTimer >= 0)
	{
		mUpdateTimer -= ms;
		if (mUpdateTimer <= 0)
		{
			int p_w, p_h;
			BMPImage *ibmp = new BMPImage(new Graphics::TBitmap());
			TTempBGobj *o;
			UnicodeString caption;
			o = mpActiveItem;
			if (getSelBitmap(ibmp, NULL))
			{
				if (!ibmp->IsEmpty())
				{
					int siw, sih;
					getSourceImageSize(&siw, &sih);
					o->o.setSourceRes(siw, sih, -1, ExtractFileName(currentFileName));
				}
				else
				{
					o->o.setSourceRes(0, 0, -1, _T(""));
				}
			}
			o->o.x = (short)(mSelectRect.mLeft);
			o->o.y = (short)(mSelectRect.mTop);
			o->o.w = (unsigned short)(mSelectRect.getWidth());
			o->o.h = (unsigned short)(mSelectRect.getHeight());
			EditItem(o, ibmp, mEdit, CategoryButtons_s, ImageList);
			delete ibmp;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::DoCreatePreview()
{
	__int32 w = mSelectRect.getWidth();
	__int32 h = mSelectRect.getHeight();
	if(w == 0 || h == 0)
	{
		Image1->Visible = false;
		return;
	}
	if (w * scale[tgTop] > MAX_SCALE || h * scale[tgTop] > MAX_SCALE)
	{
		int i = scale[tgTop];
		for (; i > 0; i--)
			if (w * i <= MAX_SCALE && h * i <= MAX_SCALE)
				break;
		if (i == 0)
		{
			i = 1;
		}
		scale[tgTop] = i;
		TrackBar1->Position = i - 1;
	}
	w *= scale[tgTop];
	h *= scale[tgTop];
	if (w != Image1->Width)
	{
		Image1->Width = w;
	}
	if (h != Image1->Height)
	{
		Image1->Height = h;
	}
	if (!Image1->Visible)
	{
		Image1->Visible = true;
	}
	ScrollBox1Resize(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::DrawFrame()
{
	if(prev_bmp && !prev_bmp->IsEmpty())
	{
		BLENDFUNCTION bf;
		bf.BlendOp = AC_SRC_OVER;
		bf.BlendFlags = 0;
		bf.SourceConstantAlpha = 255;
		bf.AlphaFormat = AC_SRC_ALPHA;

		if (gUsingOpenGL)
		{
			TRenderGL::SetActiveWindow(mWindGlID[0]);
        }
		const __int32 gl_sw = mSelectRect.getWidth();
		const __int32 gl_sh = mSelectRect.getHeight();
		if(mSelectRect.mLockHW)
		{
			if(Image1->Visible && Image1->Width > 0 && Image1->Height > 0)
			{
				const BMPImage *abmp;
				if (gUsingOpenGL)
				{
					TRenderGL::Resize(Image1->ClientRect, Image1->ClientRect);
				}
				else
				{
                    TCanvas *c = ((TMyPanel*)Image1)->GetCanvas();
					c->Brush->Color = CurrentBGColor;
					c->FillRect(Image1->ClientRect);
                }
				for (__int32 i = 0; i < temp_res->Count; i++)
				{
					TTempBGobj *o = (TTempBGobj*)temp_res->Items[i];
					if(o == mpActiveItem && gl_sw > 0 && gl_sh > 0)
					{
						const __int32 gl_sy = (__int32)mSelectRect.mTop;
						const __int32 gl_sx = (__int32)mSelectRect.mLeft;
						if (gUsingOpenGL)
						{
							TRenderGL::RenderNonResManagerImage(prev_bmp, DIT_Obj,
											gl_sx,
											gl_sy,
											Image1->ClientWidth,
											Image1->ClientHeight,
											0,
											0,
											Image1->ClientWidth * scale[tgTop],
											Image1->ClientHeight * scale[tgTop], 1.0f);
						}
						else
						{
							::AlphaBlend(((TMyPanel*)Image1)->GetCanvas()->Handle,
											0,
											0,
											Image1->ClientWidth * scale[tgTop],
											Image1->ClientHeight * scale[tgTop],
											prev_bmp->Canvas->Handle,
											gl_sx,
											gl_sy,
											Image1->ClientWidth,
											Image1->ClientHeight,
											bf);
                        }
					}
					else
					if(o->o.w > 0 && o->o.h > 0)
					{
						abmp = GetFromImageListBG(o->bi->ImageIndex);
						if(abmp != NULL)
						{
							if (gUsingOpenGL)
							{
								TRenderGL::RenderNonResManagerImage(abmp, DIT_Obj,
											o->o.x,
											o->o.y,
											o->o.w,
											o->o.h,
											0,
											0,
											o->o.w * scale[tgTop],
											o->o.h * scale[tgTop], 1.0f);
							}
							else
							{
								::AlphaBlend(((TMyPanel*)Image1)->GetCanvas()->Handle,
											0,
											0,
											o->o.w * scale[tgTop],
											o->o.h * scale[tgTop],
											const_cast<BMPImage *>(abmp)->Canvas->Handle,
											o->o.x,
											o->o.y,
											o->o.w,
											o->o.h,
											bf);
                            }
						}
					}
				}
			}
		}
		else
		if (gl_sw > 0 && gl_sh > 0)
		{
			const __int32 gl_sy = (__int32)mSelectRect.mTop;
			const __int32 gl_sx = (__int32)mSelectRect.mLeft;
			if (gUsingOpenGL)
			{
				TRenderGL::Resize(Image1->ClientRect, Image1->ClientRect);
				TRenderGL::RenderNonResManagerImage(prev_bmp, DIT_Obj,
								gl_sx,
								gl_sy,
								gl_sw,
								gl_sh,
								0,
								0,
								Image1->ClientWidth ,   // scale[tgTop]
								Image1->ClientHeight, 1.0f); // scale[tgTop]
			}
			else
			{
				TCanvas *c = ((TMyPanel*)Image1)->GetCanvas();
				c->Brush->Color = CurrentBGColor;
				c->FillRect(Image1->ClientRect);
				::AlphaBlend(c->Handle,
								0,
								0,
								Image1->ClientWidth,
								Image1->ClientHeight,
								prev_bmp->Canvas->Handle,
								gl_sx,
								gl_sy,
								gl_sw,
								gl_sh,
								bf);
			}
		}

		if (gUsingOpenGL)
		{
			TRenderGL::DrawScene();

			TRenderGL::SetActiveWindow(mWindGlID[1]);
			TRenderGL::Resize(PaintBox1->ClientRect, PaintBox1->ClientRect);
			TRenderGL::RenderNonResManagerImage(prev_bmp, DIT_Obj,
							0,
							0,
							prev_bmp->Width,
							prev_bmp->Height,
							-HorzScrollBar->Position + BORDER,
							-VertScrollBar->Position + BORDER,
							prev_bmp->Width * scale[tgBottom],
							prev_bmp->Height * scale[tgBottom], 1.0f);
		}
		else
		{
			TCanvas *c = ((TMyPanel*)PaintBox1)->GetCanvas();
			c->Brush->Color = CurrentBGColor;
			c->FillRect(PaintBox1->ClientRect);
			::AlphaBlend(c->Handle,
							-HorzScrollBar->Position + BORDER,
							-VertScrollBar->Position + BORDER,
							prev_bmp->Width * scale[tgBottom],
							prev_bmp->Height * scale[tgBottom],
							prev_bmp->Canvas->Handle,
							0,
							0,
							prev_bmp->Width,
							prev_bmp->Height,
							bf);
		}

		if(CheckBox_border->Checked)
		{
			unsigned char r, g, b;
			int cl = static_cast<int>(CurrentBGColor);
			r = static_cast<unsigned char>(CurrentBGColor);
			g = static_cast<unsigned char>(cl >> 8);
			b = static_cast<unsigned char>(cl >> 16);
			TColor bordercolor = (r > 128 || g > 128 || b  > 128) ? clBlack : clWhite;
			TRect rl;
			rl.Left = -HorzScrollBar->Position + BORDER - 1;
			rl.Top = -VertScrollBar->Position + BORDER - 2;
			rl.Right = -HorzScrollBar->Position + prev_bmp->Width * scale[tgBottom] + BORDER + 2;
			rl.Bottom = -VertScrollBar->Position + prev_bmp->Height * scale[tgBottom] + BORDER + 1;
			if(gUsingOpenGL)
			{
				TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
				TRenderGL::SetColor(bordercolor);
				TRenderGL::RenderRect(rl.Left, rl.Top, rl.Width(), rl.Height());
			}
			else
			{
				TCanvas *c = ((TMyPanel*)PaintBox1)->GetCanvas();
				c->Brush->Color = bordercolor;
				c->FrameRect(rl);
			}
		}

		DrawSelection();

        if (gUsingOpenGL)
		{
			TRenderGL::DrawScene();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::DrawSelection()
{
	TCanvas *c = ((TMyPanel*)PaintBox1)->GetCanvas();
	if (!gUsingOpenGL)
	{
		c->Brush->Style = bsSolid;
		c->Pen->Mode = pmNop;
		c->Pen->Style = psSolid;
	}
	if (mSelectRect.getWidth() != 0 && mSelectRect.getHeight() != 0)
	{
		TRect r1, r2;
		r1.Left = (__int32)mSelectRect.mLeft * scale[tgBottom] - HorzScrollBar->Position + BORDER;
		r1.Top = (__int32)mSelectRect.mTop * scale[tgBottom] - VertScrollBar->Position + BORDER;
		r1.Right = r1.Left + mSelectRect.getWidth() * scale[tgBottom];
		r1.Bottom = r1.Top + mSelectRect.getHeight() * scale[tgBottom];
		r2 = r1;
		r1.Left--;
		r1.Top--;
		r1.Right++;
		r1.Bottom++;
		if (gUsingOpenGL)
		{
			TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
			TRenderGL::SetColor(clBlack);
			TRenderGL::RenderRect(r2.Left, r2.Top, r2.Width(), r2.Height());
			TRenderGL::RenderRect(r1.Left, r1.Top, r1.Width(), r1.Height());
			TRenderGL::SetLineStyle(TRenderGL::RGLLSDot);
			TRenderGL::SetColor(clWhite);
			TRenderGL::RenderRect(r2.Left, r2.Top, r2.Width(), r2.Height());
			TRenderGL::RenderRect(r1.Left, r1.Top, r1.Width(), r1.Height());
		}
		else
		{
			c->Brush->Color = clWhite;
			c->DrawFocusRect(r2);
			c->DrawFocusRect(r1);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::ChangeSizes()
{
	int ov, oh, t;
	ov = VertScrollBar->Position;
	oh = HorzScrollBar->Position;
	VertScrollBar->Position = 0;
	HorzScrollBar->Position = 0;
	HorzScrollBar->Left = SCALE_PANEL_WIDTH;
	VertScrollBar->Top = 0;
	HorzScrollBar->Width = Panel_1->Width - SCALE_PANEL_WIDTH - VertScrollBar->Width;
	VertScrollBar->Height = Panel_1->Height - HorzScrollBar->Height;
	HorzScrollBar->Top = VertScrollBar->Height + VertScrollBar->Top;
	VertScrollBar->Left = HorzScrollBar->Width + SCALE_PANEL_WIDTH;
	if (HorzScrollBar->Max < HorzScrollBar->Width)
	{
		HorzScrollBar->PageSize = 0;
		HorzScrollBar->Position = 0;
		HorzScrollBar->Enabled = true;
		HorzScrollBar->Enabled = false;
	}
	else
	{
		HorzScrollBar->Enabled = true;
		HorzScrollBar->PageSize = HorzScrollBar->Width;
	}

	if (VertScrollBar->Max < VertScrollBar->Height)
	{
		VertScrollBar->PageSize = 0;
		VertScrollBar->Position = 0;
		VertScrollBar->Enabled = true;
		VertScrollBar->Enabled = false;
	}
	else
	{
		VertScrollBar->Enabled = true;
		VertScrollBar->PageSize = VertScrollBar->Height;
	}

	if (PaintBox1->Height != VertScrollBar->Height)
	{
		PaintBox1->Height = VertScrollBar->Height;
	}
	if (PaintBox1->Width != HorzScrollBar->Width)
	{
		PaintBox1->Width = HorzScrollBar->Width;
	}

	if (VertScrollBar->Enabled)
	{
		if (ov + VertScrollBar->PageSize > VertScrollBar->Max)
			ov = VertScrollBar->Max - VertScrollBar->PageSize;
		VertScrollBar->Position = ov;
	}
	if (HorzScrollBar->Enabled)
	{
		if (oh + HorzScrollBar->PageSize > HorzScrollBar->Max)
			oh = HorzScrollBar->Max - HorzScrollBar->PageSize;
		HorzScrollBar->Position = oh;
	}

	if (gUsingOpenGL)
	{
		DrawFrame();
	}
	else
	{
        mRenderDelay = RENDER_DELAY_MS;
		DrawFrame();
		//Application->ProcessMessages();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::HorzScrollBarScroll(TObject *Sender, TScrollCode ScrollCode, int &ScrollPos)
{
	int oScrollPos;
	TScrollBar *sb = (TScrollBar*)Sender;
	oScrollPos = sb->Position;
	switch(ScrollCode)
	{
		case scTrack:
		case scPosition:
		case scLineUp:
		case scPageUp:
			if (ScrollPos <= sb->Min)
				ScrollPos = sb->Min;
		case scLineDown:
		case scPageDown:
			if (ScrollPos + sb->PageSize >= sb->Max)
				ScrollPos = sb->Max - sb->PageSize;
			break;
		default:
		break;
	}
	if (oScrollPos == ScrollPos)
	{
		return;
	}

	if (gUsingOpenGL)
	{
		DrawFrame();
	}
	else
	{
		mRenderDelay = RENDER_DELAY_MS;
		DrawFrame();
		//Application->ProcessMessages();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::SplitterLVMoved(TObject *)
{
	ChangeSizes();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::Splitter1Moved(TObject *)
{
	ChangeSizes();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::FormResize(TObject *)
{
	ChangeSizes();
}
//---------------------------------------------------------------------------

void __fastcall TFEditGrObj::CustomPaintBox1WndMethod(Winapi::Messages::TMessage &Message)
{
	if(Message.Msg == WM_PAINT)
	{
		mRenderDelay = RENDER_DELAY_MS;
	}
	mOldPaintBox1WndProc(Message);
}
//---------------------------------------------------------------------------
