//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UnitFindObj.h"
#include "MainUnit.h"
#include "UObjManager.h"
#include "UObjCode.h"
#include "UObjOrder.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormFindObj *FormFindObj;

static const _TCHAR FIND_NAME[] = _T("mFind");
static const _TCHAR FIND_NAME_NEXT[] = _T("mFindNextObject");

//---------------------------------------------------------------------------
__fastcall TFormFindObj::TFormFindObj(TComponent* Owner)
	: TForm(Owner)
{
	Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFormFindObj::Localize()
{
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFormFindObj::Init()
{
	ComboBoxGroup->ItemIndex = -1;
	ComboBoxProperty->ItemIndex = -1;
	ComboBoxGroupReplace->ItemIndex = -1;
	ComboBoxPropertyReplace->ItemIndex = -1;

	ComboBoxGroup->Items->Clear();
	ComboBoxGroupReplace->Items->Clear();
	ComboBoxProperty->Items->Clear();
	ComboBoxPropertyReplace->Items->Clear();

	assert(Form_m->mainCommonRes);
	int ct = Form_m->mainCommonRes->GParentObj->Count;
	if(ct > 0)
	{
		ComboBoxGroup->Items->Add(_T(""));
		ComboBoxGroupReplace->Items->Add(_T(""));
		for(int i = 0; i < ct; i++)
		{
			GPerson* gp = (GPerson*)Form_m->mainCommonRes->GParentObj->Items[i];
			ComboBoxGroup->Items->Add(gp->GetName());
			ComboBoxGroupReplace->Items->Add(gp->GetName());
		}
		ComboBoxGroup->Sorted = false;
		ComboBoxGroup->Sorted = true;
		ComboBoxGroupReplace->Sorted = false;
		ComboBoxGroupReplace->Sorted = true;
	}

	TObjPattern *pt = Form_m->mainCommonRes->MainGameObjPattern;
	if(pt != NULL)
	{
		const std::list<PropertyItem>* lvars = pt->GetVars();
		if(!lvars->empty())
		{
			ComboBoxProperty->Items->Add(_T(""));
			ComboBoxPropertyReplace->Items->Add(_T(""));
			std::list<PropertyItem>::const_iterator iv;
			for (iv = lvars->begin(); iv != lvars->end(); ++iv)
			{
				ComboBoxProperty->Items->Add(iv->name);
				ComboBoxPropertyReplace->Items->Add(iv->name);
			}
        }
    }
	UpdateButtonCaption();
	ComboBoxPropertyReplaceChange(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFormFindObj::Clear()
{
	EditName->Text = _T("");
	ComboBoxGroup->ItemIndex = -1;
	ComboBoxProperty->ItemIndex = -1;
	EditVal->Text = _T("");
	EditVal1->Text = _T("");
	EditVal2->Text = _T("");
	CheckRadioButtons();
	UpdateButtonCaption();
}
//---------------------------------------------------------------------------

void __fastcall TFormFindObj::UpdateButtonCaption()
{
	if(Form_m->GetLastSelectedDragObj() == NULL)
	{
		Button1->Caption = UnicodeString(Localization::Text(FIND_NAME));
	}
	else
	{
		Button1->Caption = UnicodeString(Localization::Text(FIND_NAME_NEXT));
    }
}
//---------------------------------------------------------------------------

void __fastcall TFormFindObj::CheckRadioButtons()
{
	if(ComboBoxProperty->ItemIndex >= 1)
	{
		RadioButton1->Enabled = true;
		RadioButton2->Enabled = true;
		if(RadioButton1->Checked)
		{
			RadioButton2->Checked = false;
			EditVal->Color = clWindow;
			EditVal->Enabled = true;
			EditVal1->Color = clBtnFace;
			EditVal2->Color = clBtnFace;
			EditVal1->Enabled = false;
			EditVal2->Enabled = false;
		}
		else
		{
			RadioButton1->Checked = false;
			EditVal->Color = clBtnFace;
			EditVal->Enabled = false;
			EditVal1->Color = clWindow;
			EditVal2->Color = clWindow;
			EditVal1->Enabled = true;
			EditVal2->Enabled = true;
		}
	}
	else
	{
		RadioButton1->Enabled = false;
		RadioButton2->Enabled = false;
		EditVal->Color = clBtnFace;
		EditVal->Enabled = false;
		EditVal1->Color = clBtnFace;
		EditVal2->Color = clBtnFace;
		EditVal1->Enabled = false;
		EditVal2->Enabled = false;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFormFindObj::ComboBoxPropertyChange(TObject *)
{
	CheckRadioButtons();
}
//---------------------------------------------------------------------------

void __fastcall TFormFindObj::RadioButton1Click(TObject *)
{
	CheckRadioButtons();
}
//---------------------------------------------------------------------------

void __fastcall TFormFindObj::EditValChange(TObject *Sender)
{
	onChangeSpinEdit((TEdit*)Sender, true);
}
//---------------------------------------------------------------------------

void __fastcall TFormFindObj::EditNameChange(TObject *)
{
	Edit_nameChangeMain(EditName);
}
//---------------------------------------------------------------------------

void __fastcall TFormFindObj::Button1Click(TObject *)
{
	if(EditName->Text.IsEmpty() && ComboBoxProperty->ItemIndex < 1 && ComboBoxGroup->ItemIndex < 1)
	{
		return;
	}
	int i, j, jj;
	double val1, val2;
	TMapPerson *mo;
	TMapGPerson *gmo;
	bool forceExit;
	TObjManager::TObjType tlist[5] =
	{
		TObjManager::BACK_DECORATION,
		TObjManager::CHARACTER_OBJECT,
		TObjManager::FORE_DECORATION,
		TObjManager::TRIGGER,
		TObjManager::DIRECTOR
	};

	UnicodeString var = _T("");
	if(ComboBoxProperty->ItemIndex >= 1)
	{
		var = ComboBoxProperty->Text;
		if(RadioButton1->Checked)
		{
			try
			{
				val1 = (double)StrToFloat(EditVal->Text);
			}
			catch(...)
			{
				val1 = 0.0f;
			}
		}
		if(RadioButton2->Checked)
		{
			try
			{
				val1 = (double)StrToFloat(EditVal1->Text);
				val2 = (double)StrToFloat(EditVal2->Text);
			}
			catch(...)
			{
				val1 = 0.0f;
				val2 = 0.0f;
			}
		}
	}
	jj = -1;
	j = 0;
	forceExit = false;
	for(;;)
	{
		int len = Form_m->m_pObjManager->GetObjTypeCount(tlist[j]);
		for (i = 0; i < len; i++)
		{
			bool resultName;
			bool resultGroup;
			bool resultVar = false;
			mo = static_cast<TMapPerson*>(Form_m->m_pObjManager->GetObjByIndex(tlist[j], i));
			if(Form_m->GetLastSelectedDragObj() != NULL)
			{
				if(jj < 0)
				{
					if(Form_m->GetLastSelectedDragObj() == mo)
					{
						jj = j;
					}
					continue;
				}
				else
				{
					if(Form_m->GetLastSelectedDragObj() == mo)
					{
						forceExit = true;
					}
				}
			}

			if(ComboBoxProperty->ItemIndex >= 1)
			{
				double val;
				gmo = static_cast<TMapGPerson*>(mo);
				if(tlist[j] == TObjManager::CHARACTER_OBJECT &&
					gmo->GetDecorType() == decorNONE &&
						gmo->GetVarValue(&val, var))
				{
					if(RadioButton1->Checked)
					{
						resultVar = val == val1;
					}
					else
					{
						resultVar = val >= val1 && val <= val2;
					}
				}
			}
			else
			{
				resultVar = true;
			}

			if(ComboBoxGroup->ItemIndex >= 1)
			{
				resultGroup = mo->getGParentName().Compare(ComboBoxGroup->Text) == 0;
			}
			else
			{
				resultGroup = true;
			}

			if(!EditName->Text.IsEmpty())
			{
				resultName = mo->Name.Pos(EditName->Text) > 0;
			}
			else
			{
				resultName = true;
			}

			if(resultName && resultGroup && resultVar)
			{
				Form_m->DragObjListClear();
				Form_m->DragObjListAdd(mo);
				if(Form_m->showObjects)
				{
					if(Form_m->znCurZone != Form_m->GetLastSelectedDragObj()->GetZone() &&
						!Form_m->ActStartEmul->Checked && Form_m->GetLastSelectedDragObj()->GetZone() >= 0)
					{
						Form_m->znCurZone = Form_m->GetLastSelectedDragObj()->GetZone();
						Form_m->ListBoxZone->ItemIndex = Form_m->znCurZone;
						Form_m->SetZoneVisibility();
					}
					else
					{
						Form_m->DrawMap(false);
					}
					Form_m->FindAndShowObject(Form_m->GetLastSelectedDragObj());
					Form_m->AddMarker(Form_m->GetLastSelectedDragObj(), TObjMarker::TObjMarkerType::Attention);
				}
				if (FObjOrder->Visible)
				{
					FObjOrder->RebuildSelectionList();
				}
				return;
			}

			if(forceExit)
			{
				Form_m->DragObjListClear();
				return;
			}
		}
		j++;
		if(j == 5)
		{
			j = 0;
			if(jj < 0)
			{
				return;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormFindObj::ButtonReplaceClick(TObject *)
{
	UnicodeString var;
	int j, i, chct = 0;
	double val1, val2;
	try
	{
		val1 = (double)StrToFloat(EditValReplace->Text);
	}
	catch(...)
	{
		val1 = 0.0f;
	}
	try
	{
		val2 = (double)StrToFloat(EditReplaceVal->Text);
	}
	catch(...)
	{
		val2 = 0.0f;
	}
	var = ComboBoxPropertyReplace->Text;
	for (i = 0; i < Form_m->m_pLevelList->Count; i++)
	{
		TObjManager *mgo = (TObjManager*)Form_m->m_pLevelList->Items[i];
		if(!ApplyToAllMaps->Checked)
		{
			mgo = Form_m->m_pObjManager;
			i = Form_m->m_pLevelList->Count;
		}
		int len = mgo->GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
		for (j = 0; j < len; j++)
		{
			bool resultName;
			bool resultGroup;
			bool resultVar = false;
			TMapPerson* mo = static_cast<TMapPerson*>(mgo->GetObjByIndex(TObjManager::CHARACTER_OBJECT, j));
			if(ComboBoxPropertyReplace->ItemIndex >= 1)
			{
				double val;
				TMapGPerson *gmo = static_cast<TMapGPerson*>(mo);
				if(gmo->GetObjType() == objMAPOBJ &&
					gmo->GetDecorType() == decorNONE &&
						gmo->GetVarValue(&val, var))
				{
					resultVar = val == val1;
				}
			}

			if(ComboBoxGroupReplace->ItemIndex >= 1)
			{
				resultGroup = mo->getGParentName().Compare(ComboBoxGroupReplace->Text) == 0;
			}
			else
			{
				resultGroup = true;
			}

			if(!EditNameReplace->Text.IsEmpty())
			{
				resultName = mo->Name.Pos(EditNameReplace->Text) > 0;
			}
			else
			{
				resultName = true;
			}

			if(resultName && resultGroup && resultVar)
			{
				TMapGPerson *gmo = static_cast<TMapGPerson*>(mo);
				gmo->SetVarValue(val2, var);
				chct++;
			}
		}
	}
	var = Localization::Text(_T("mReplaceCount")) + _T(": ") + IntToStr(chct);
	Application->MessageBox(var.c_str(), Localization::Text(_T("mResult")).c_str(), MB_OK | MB_ICONINFORMATION);
}
//---------------------------------------------------------------------------

void __fastcall TFormFindObj::ComboBoxPropertyReplaceChange(TObject *)
{
	ButtonReplace->Enabled = !ComboBoxPropertyReplace->Text.IsEmpty();
}
//---------------------------------------------------------------------------

