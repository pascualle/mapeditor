//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UBGEditWindow.h"
#include "MainUnit.h"
#include "UObjCode.h"
#include "UObjManager.h"
#include "UFragmentProperty.h"
#include "UEditObj.h"
#include "URenderGL.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFBGObjWindow *FBGObjWindow;

static const int BG_WINDOW_DEF_SCALE = 1;
static const int THRESHOLD_TO_DRAG = 5;

//---------------------------------------------------------------------------
__fastcall TFBGObjWindow::TFBGObjWindow(TComponent* Owner)
	: TForm(Owner)
{
	int i, j;

	mDrawGrid = false;
    mOtherLayerVisible = true;
	mScale = -1.0f;
	DrawGrid->ColCount = MAX_BG_WND_OBJECTS_W;
	DrawGrid->RowCount = MAX_BG_WND_OBJECTS_H;

	mppCells = new GMapBGObj**[MAX_BG_WND_OBJECTS_H];
	for(i = 0; i < MAX_BG_WND_OBJECTS_H; i++)
	{
		mppCells[i] = new GMapBGObj*[MAX_BG_WND_OBJECTS_W];
	}
	for(i = 0; i < MAX_BG_WND_OBJECTS_H; i++)
	{
		for(j = 0; j < MAX_BG_WND_OBJECTS_W; j++)
		{
			mppCells[i][j] = NULL;
		}
	}
	mColWidth = DrawGrid->DefaultColWidth;
	mRowHeight = DrawGrid->DefaultRowHeight;
	ActAutoCreate->Enabled = false;
	SetScale(BG_WINDOW_DEF_SCALE);
	ActStayOnTop->Checked = true;
	ActGrid->Checked = true;
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::Localize()
{
	Localization::Localize(this);
	Localization::Localize(ActionList1);
  	Localization::Localize(PopupMenu);
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::FormDestroy(TObject */*Sender*/)
{
    int i;
    mDrawGrid = false;
    for(i = 0; i < MAX_BG_WND_OBJECTS_H; i++)
    {
    	delete[] mppCells[i];
    }
	delete[] mppCells;

	std::list<SelItem>::iterator lit;
	for (lit = mSelList.begin(); lit != mSelList.end(); ++lit)
	{
		delete lit->gobj;
	}
	mSelList.clear();
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::DrawGridDrawCell(TObject *Sender, System::LongInt ACol,
		  System::LongInt ARow, TRect &Rect, TGridDrawState State)
{
	const int BG_MAX_ALPHA = 255;
	const int BG_TRANS_ALPHA = 64;

	Graphics::TBitmap* pbmp;
	TFrameObj* bgo;
    TRect rs;
	DrawGrid->Canvas->FillRect(Rect);
	if(mDrawGrid)
	{
		BLENDFUNCTION bf;
		bf.BlendOp = AC_SRC_OVER;
		bf.BlendFlags = 0;
		bf.SourceConstantAlpha = BG_MAX_ALPHA;
		bf.AlphaFormat = AC_SRC_ALPHA;

		if(mppCells[ARow][ACol] != nullptr)
		{
			const bool allLayers = CB_Layer->ItemIndex == CB_Layer->Items->Count - 1;
			const int lastLayer = allLayers ? CB_Layer->Items->Count - 1 : CB_Layer->ItemIndex;
			int currentLayer = allLayers || mOtherLayerVisible ? 0 : CB_Layer->ItemIndex;
			while(currentLayer <= lastLayer)
			{
				bgo = mppCells[ARow][ACol]->GetFrameObj(currentLayer);

				if(!allLayers && mOtherLayerVisible)
				{
					bf.SourceConstantAlpha = CB_Layer->ItemIndex == currentLayer ? BG_MAX_ALPHA : BG_TRANS_ALPHA;
				}

				if(bgo != NULL && bgo->getResIdx() >= 0)
				{
					::AlphaBlend(DrawGrid->Canvas->Handle,        // handle to destination DC
									Rect.Left,   // x-coord of destination upper-left corner
									Rect.Top,   // y-coord of destination upper-left corner
									Rect.Width(),     // width of destination rectangle
									Rect.Height(),    // height of destination rectangle
									Form_m->mainCommonRes->GetGraphicResource(bgo->getResIdx())->Canvas->Handle,
									bgo->x,    // x-coord of source upper-left corner
									bgo->y,    // y-coord of source upper-left corner
									bgo->w,      // width of source rectangle
									bgo->h,     // height of source rectangle
									bf);
				}
				currentLayer++;
			}
		}

        if(State.Contains(gdSelected))
        {
			DrawGrid->Canvas->DrawFocusRect(Rect);
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::ActGridExecute(TObject */*Sender*/)
{
	if(ActGrid->Checked)
    {
		DrawGrid->GridLineWidth = 1;
    }
    else
    {
		DrawGrid->GridLineWidth = 0;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::SetScale(float scale)
{
	if(mScale != scale || scale < 0)
    {
		if(scale >= 0.01f)
		{
			mScale = scale;
		}
		DrawGrid->DefaultColWidth = (int)((float)mColWidth * mScale);
		DrawGrid->DefaultRowHeight = (int)((float)mRowHeight * mScale);
		DrawGrid->Repaint();
    }
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::ProcessChanges()
{
	mDrawGrid = false;
	CB_Layer->Clear();
    for(int i = 0; i < Form_m->lrCount; i++)
    {
		CB_Layer->Items->Add(IntToStr(i));
    }
	CB_Layer->Items->Add(Localization::Text(_T("mAll")));
    CB_Layer->ItemIndex = CB_Layer->Items->Count - 1;
    mDrawGrid = true;
    DrawGrid->Repaint();
}
//---------------------------------------------------------------------------
/*
void __fastcall TFBGObjWindow::CreateEmptyBGObjects()
{
    int i, x, y;
    x = y = i = 0;
    GMapBGObj *bo;
    TList *lst = Form_m->mainCommonRes->BackObj;

    mDrawGrid = false;

	ClearSelectionList();

	for(; i < lst->Count; i++)
    {
        delete lst->Items[i];
    }
    lst->Clear();

	for(i = 0; i < MAX_BG_WND_OBJECTS_W * MAX_BG_WND_OBJECTS_H; i++)
    {
		bo = new GMapBGObj(Form_m->lrCount);
        lst->Add(bo);
		mppCells[y][x] = bo;
        x++;
        if(x >= MAX_BG_WND_OBJECTS_W)
        {
            x = 0;
            y++;
        }
    }
    mDrawGrid = true;
}
//---------------------------------------------------------------------------
*/
void __fastcall TFBGObjWindow::ArrangePositions()
{
    __int32 i, j, idx;
    for(i = 0; i < MAX_BG_WND_OBJECTS_H; i++)
	for(j = 0; j < MAX_BG_WND_OBJECTS_W; j++)
    {
		mppCells[i][j] = NULL;
	}

	ClearSelectionList();
	ProcessChanges();

    mDrawGrid = false;

	i = j = 0;
	for(idx = 0; idx < MAX_BG_WND_OBJECTS_W * MAX_BG_WND_OBJECTS_H; idx++)
	{
		mppCells[i][j] = &Form_m->mainCommonRes->BackObj[idx];
		i++;
		if(i >= MAX_BG_WND_OBJECTS_H)
		{
			i = 0;
			j++;
		}
	}
    mDrawGrid = true;
    DrawGrid->Repaint();
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::SetGridSize(int iW, int iH)
{
    mColWidth = iW;
	mRowHeight = iH;
	SetScale(-1.0f);
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::ActAutoCreateExecute(TObject*)
{
	if(CB_Layer->ItemIndex == CB_Layer->Items->Count - 1 || DrawGrid->Row < 0 || DrawGrid->Col < 0 || !mDrawGrid)
	{
		return;
	}
	if (gUsingOpenGL)
	{
		Form_m->ReleaseRenderGL();
	}
	 TFEditGrObj* ed = Form_m->getSelectionBlock(Form_m->mainCommonRes, false);
	 if(ed != NULL)
	 {
		ed->SetImagePreviewMode();
        if(ed->ShowModal() == mrOk)
        {
            BMPImage			*i2d;
            Graphics::TBitmap	*t_bmp;
			TFrameObj			*bo;
            int					ww, hh, c, r, gr_idx;
            UnicodeString		fname;
            ed->getSourceImageSize(&ww, &hh);
            ed->Hide();
			fname = ExtractFileName(ed->FileListBox->FileName);
            c = DrawGrid->Col;
            r = DrawGrid->Row;
			t_bmp = new Graphics::TBitmap();
			UnicodeString flbname = ed->FileListBox->FileName;
			read_png(flbname.c_str(), t_bmp);
			i2d = new BMPImage(t_bmp);
			gr_idx = Form_m->mainCommonRes->GetGraphicResourceIndex(fname);
			if(gr_idx < 0)
			{
				gr_idx = Form_m->mainCommonRes->AddGraphicResource(i2d, fname);
			}
            for(int y = 0; y <= hh - mRowHeight; y += mRowHeight)
            {
                for(int x = 0; x <= ww - mColWidth; x += mColWidth)
                {
                    if(c < MAX_BG_WND_OBJECTS_W && r < MAX_BG_WND_OBJECTS_H)
                    {
						bo = mppCells[r][c]->GetFrameObj(CB_Layer->ItemIndex);
						if(bo == NULL)
						{
							bo = new TFrameObj();
							mppCells[r][c]->AssignFrameObj(bo, CB_Layer->ItemIndex);
                        }
						bo->setSourceRes(ww, hh, gr_idx, fname);
                        bo->x = (short)x;
                        bo->y = (short)y;
						bo->h = (short)mRowHeight;
						bo->w = (short)mColWidth;
                    }
                    c++;
                }
            	r++;
                c = DrawGrid->Col;
            }
            delete i2d;
            DrawGrid->Repaint();
        }
     	ed->Free();
	 }
	if (gUsingOpenGL)
	{
		Form_m->InitRenderGL();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::DrawGridSelectCell(TObject *Sender, System::LongInt ACol,
		  System::LongInt ARow, bool &CanSelect)
{
  StatusBar->SimpleText = _T("#") + IntToStr(MAX_BG_WND_OBJECTS_H * ACol + ARow);
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::SetColor(TColor iColor)
{
	DrawGrid->Canvas->Brush->Color = iColor;
}
//---------------------------------------------------------------------------

int __fastcall TFBGObjWindow::GetMaximumCapacity()
{
	return MAX_BG_WND_OBJECTS_W * MAX_BG_WND_OBJECTS_H;
}
//---------------------------------------------------------------------------

int __fastcall TFBGObjWindow::GetColCount()
{
	return MAX_BG_WND_OBJECTS_W;
}
//---------------------------------------------------------------------------

int __fastcall TFBGObjWindow::GetRowCount()
{
	return MAX_BG_WND_OBJECTS_H;
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::DrawGridKeyPress(TObject */*Sender*/, char &Key)
{
	if(Key == VK_RETURN)
		AddEditDel(mEdit);
    else
    if(Key == VK_DELETE)
		AddEditDel(mDel);
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::ActDelExecute(TObject */*Sender*/)
{
	AddEditDel(mDel);
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::ActEditExecute(TObject */*Sender*/)
{
	AddEditDel(mEdit);
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::DrawGridDblClick(TObject */*Sender*/)
{
	ActEdit->Execute();
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::AddEditDel(int mode)
{
	if(!mDrawGrid || !Form_m->IsProjExist())
	{
		return;
    }

 if(mode == mDel)
 {
 	UnicodeString mes;
	if(DrawGrid->Selection.Bottom - DrawGrid->Selection.Top == 0 &&
		DrawGrid->Selection.Right - DrawGrid->Selection.Left == 0)
	{
		mes = Localization::Text(_T("mAskDeleteTileObject"));
	}
	else
	{
		mes = Localization::Text(_T("mAskDeleteTileObjectGroup"));
	}

	if(Application->MessageBox(mes.c_str(),
		Localization::Text(_T("mDelete")).c_str(),
		MB_YESNO|MB_ICONQUESTION) == ID_YES)
	{
		int len, t ,l, j, idx, i;
		for(t = DrawGrid->Selection.Top; t <= DrawGrid->Selection.Bottom; t++)
		for(l = DrawGrid->Selection.Left; l <= DrawGrid->Selection.Right; l++)
		{
			if(CB_Layer->ItemIndex == CB_Layer->Items->Count - 1)
			{
				mppCells[t][l]->DelAllFrameObj();
			}
			else
			{
				mppCells[t][l]->AssignFrameObj(NULL, CB_Layer->ItemIndex);
			}
		}

		//перерисовать карты
		if(Form_m->IsMapExist())
		{
			bool need_backup_rebuild = false;
			for(j = 0; j < Form_m->m_pLevelList->Count; j++)
			{
				TObjManager *mo = (TObjManager *)Form_m->m_pLevelList->Items[j];
				if(mo->map != NULL)
				{
					len = mo->mp_w * mo->mp_h;
					for(i = 0; i < len; i++)
					{
						idx = mo->map[i];
						if(idx != NONE_P)
						{
							GMapBGObj &BGobj = Form_m->mainCommonRes->BackObj[idx];
							if(BGobj.IsEmpty())
							{
								mo->map[i] = NONE_P;
								need_backup_rebuild = Form_m->m_pObjManager == mo;
							}
						}
					}
				}
			}
			if(need_backup_rebuild)
			{
				Form_m->MakeMapBackup();
			}
		}
	}
	Form_m->SetSaveFlag();
	Form_m->DrawMap(true);
	DrawGrid->Repaint();
	return;
 }

 if(DrawGrid->Col >= 0 && DrawGrid->Row >= 0)
 {
	if (gUsingOpenGL)
	{
		Form_m->ReleaseRenderGL();
	}
	GMapBGObj *bgobj = new GMapBGObj(Form_m->lrCount);
	mppCells[DrawGrid->Row][DrawGrid->Col]->CopyTo(bgobj, Form_m->lrCount);
	Application->CreateForm(__classid(TFBOProperty), &FBOProperty);
	FBOProperty->Caption = FBOProperty->Caption +
		_T(" [") + Localization::Text(_T("mIndex")) + _T("= ") + IntToStr(GetSelectedIndex()) + _T("]");

	FBOProperty->assignBGobj(bgobj);
    if(FBOProperty->ShowModal() == mrOk)
    {
    	FBOProperty->applyChangesToBGobj();
		bgobj->CopyTo(mppCells[DrawGrid->Row][DrawGrid->Col], Form_m->lrCount);
     	Form_m->SetSaveFlag();
        Form_m->DrawMap(true);
        DrawGrid->Repaint();
    }
	delete bgobj;
    FBOProperty->Free();
	FBOProperty = NULL;
	if (gUsingOpenGL)
	{
		Form_m->InitRenderGL();
	}
 }
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::DrawGridMouseDown(TObject */*Sender*/,
      TMouseButton Button, TShiftState /*Shift*/, int X, int Y)
{
    int r, c;
    r = c = -1;
	DrawGrid->MouseToCell(X, Y, c, r);

    if(r >= 0 && c >= 0)
	{
        if(Button == mbRight)
        {
            if(DrawGrid->Selection.Left > c || DrawGrid->Selection.Right < c ||
               DrawGrid->Selection.Top > r || DrawGrid->Selection.Bottom < r)
            {
                WORD x, y;
				x = (WORD)X;
				y = (WORD)Y;
				LPARAM l = (x & 0xffff) + ((y & 0xffff)<<16); //MAKELPARAM(x, y);
				PostMessage(DrawGrid->Handle, WM_LBUTTONDOWN, 0, l);
				PostMessage(DrawGrid->Handle, WM_LBUTTONUP, 0, l);
				Application->ProcessMessages();
			}
            PopupMenu->Popup(Mouse->CursorPos.x, Mouse->CursorPos.y);
		}
    }
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::ClearSelectionList()
{
	std::list<SelItem>::iterator lit;
	for (lit = mSelList.begin(); lit != mSelList.end(); ++lit)
	{
		delete lit->gobj;
	}
	mSelList.clear();
    if(DrawGrid->Selection.Left != DrawGrid->Selection.Right ||
       DrawGrid->Selection.Top != DrawGrid->Selection.Bottom)
    {
        DrawGrid->Col = 0;
        DrawGrid->Row = 0;
		TRect r = DrawGrid->CellRect(0, 0);
		LPARAM l = (r.Left & 0xffff) + ((r.Top & 0xffff)<<16); //(MAKELPARAM((r.Left), (r.Top)))
		PostMessage(DrawGrid->Handle, WM_LBUTTONDOWN, 0, l);
		PostMessage(DrawGrid->Handle, WM_LBUTTONUP, 0, l);
        Application->ProcessMessages();
    }
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::GetSelectionList(std::list<SelItem>& lst)
{
	std::list<SelItem>::iterator lit;
	for (lit = lst.begin(); lit != lst.end(); ++lit)
	{
		delete lit->gobj;
	}
	lst.clear();
    for(int t = DrawGrid->Selection.Top; t <= DrawGrid->Selection.Bottom; t++)
    for(int l = DrawGrid->Selection.Left; l <= DrawGrid->Selection.Right; l++)
	{
	   SelItem sit;
       GMapBGObj* o = new GMapBGObj();
	   sit.Left = l;
	   sit.Top = t;
       sit.ItemIndex = CB_Layer->ItemIndex;
	   mppCells[t][l]->CopyTo(o);
       sit.gobj = o;
	   lst.push_back(sit);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::DrawGridDragDrop(TObject */*Sender*/,
      TObject */*Source*/, int /*X*/, int /*Y*/)
{
	/*int r, c;
    r = c = -1;
    DrawGrid->MouseToCell(X, Y, c, r);
	if(r >= 0 && c >= 0)
    {
    	Paste(c, r);
    }*/
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::DrawGridDragOver(TObject */*Sender*/,
      TObject */*Source*/, int /*X*/, int /*Y*/, TDragState /*State*/, bool &/*Accept*/)
{
	/*int r, c;
    r = c = -1;
    DrawGrid->MouseToCell(X, Y, c, r);
	Accept = Source == Sender && r >= 0 && c >= 0;*/
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::DrawGridStartDrag(TObject */*Sender*/,
      TDragObject *&/*DragObject*/)
{
	//GetSelectionList();
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::Paste(int col, int row)
{
	if(mSelList.size() == 0 || !mDrawGrid || !Form_m->IsProjExist())
	{
		return;
	}

	GMapBGObj* o;
	int o_c, o_r, c, r, pr_r, ls, ld;
	c = col;
	r = row;
	pr_r = mSelList.front().Top;
    ld = CB_Layer->ItemIndex;
    if(ld == CB_Layer->Items->Count - 1)
    {
        ld = -1;
    }
	std::list<SelItem>::iterator lit;
	for (lit = mSelList.begin(); lit != mSelList.end(); ++lit)
	{
		const SelItem& sit = *lit;
		o_c = sit.Left;
		o_r = sit.Top;
		ls = sit.ItemIndex;
        o = sit.gobj;
		if(o_r != pr_r)
        {
            c = col;
            r++;
        }
		pr_r = o_r;
        if(c < MAX_BG_WND_OBJECTS_W && r < MAX_BG_WND_OBJECTS_H && (o_r != r || o_c != c))
        {
            if(ls == CB_Layer->Items->Count - 1)
            {
                for(int j = 0; j < Form_m->lrCount; j++)
                {
					if(o != NULL && o->GetFrameObj(j) != NULL)
					{
						TFrameObj* to = new TFrameObj();
						o->GetFrameObj(j)->copyTo(*to);
						mppCells[r][c]->AssignFrameObj(to, j);
					}
                    else
                    {
						if(mppCells[r][c]->GetFrameObj(j) != NULL)
						{
							mppCells[r][c]->AssignFrameObj(NULL, j);
						}
                    }
                }
            }
            else
            {
				if(ld < 0)
				{
					ld = ls;
                }
				if(o != NULL && o->GetFrameObj(ls) != NULL)
                {
					TFrameObj* to = new TFrameObj();
					o->GetFrameObj(ls)->copyTo(*to);
					mppCells[r][c]->AssignFrameObj(to, ld);
            	}
                else
                {
					if(mppCells[r][c]->GetFrameObj(ld) != NULL)
                    {
						mppCells[r][c]->AssignFrameObj(NULL, ld);
                	}
                }
            }
        }
        c++;
    }
    DrawGrid->Repaint();
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::ActCopyExecute(TObject */*Sender*/)
{
	if(!mDrawGrid || !Form_m->IsProjExist())
	{
		return;
	}

	GetSelectionList(mSelList);

    Form_m->CopyFromBGObjWindow(DrawGrid->Selection);
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::ActPasteExecute(TObject */*Sender*/)
{
	if(DrawGrid->Row >= 0 && DrawGrid->Col >= 0)
    {
    	Paste(DrawGrid->Col, DrawGrid->Row);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::CB_LayerChange(TObject */*Sender*/)
{
    ActAutoCreate->Enabled = CB_Layer->ItemIndex != CB_Layer->Items->Count - 1;
    DrawGrid->Repaint();
}
//---------------------------------------------------------------------------

int __fastcall TFBGObjWindow::GetSelectedIndex()
{
	return MAX_BG_WND_OBJECTS_H * DrawGrid->Col + DrawGrid->Row;
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::ActStayOnTopExecute(TObject */*Sender*/)
{
    if(ActStayOnTop->Checked)
    {
		this->FormStyle = fsNormal;
        ActStayOnTop->Checked = false;
    }
    else
    {
		this->FormStyle = fsStayOnTop;
		ActStayOnTop->Checked = true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::CB_ZoomChange(TObject */*Sender*/)
{
    switch(CB_Zoom->ItemIndex)
	{
        case 0:
			SetScale(0.125f);
		break;
		case 1:
			SetScale(0.25f);
		break;
		case 2:
			SetScale(0.5f);
		break;
		case 3:
			SetScale(1.0f);
		break;
		case 4:
			SetScale(2.0f);
		break;
		case 5:
			SetScale(4.0f);
		break;
		case 6:
			SetScale(8.0f);
    }
}
//---------------------------------------------------------------------------

GMapBGObj* __fastcall TFBGObjWindow::GetBGObj(int iCol, int iRow)
{
	assert(iCol < MAX_BG_WND_OBJECTS_W);
    assert(iRow < MAX_BG_WND_OBJECTS_H);
	return mppCells[iRow][iCol];
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::FindBGObj(unsigned int iIdx)
{
	if(iIdx < (unsigned int)GetMaximumCapacity())
    {
    	DrawGrid->Col = iIdx / MAX_BG_WND_OBJECTS_H;
		DrawGrid->Row = iIdx - DrawGrid->Col * MAX_BG_WND_OBJECTS_H;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFBGObjWindow::ActBGLayerVisibilityExecute(TObject *Sender)
{
	(void)Sender;
	ActBGLayerVisibility->Checked = !ActBGLayerVisibility->Checked;
	mOtherLayerVisible = ActBGLayerVisibility->Checked;
	DrawGrid->Repaint();
}
//---------------------------------------------------------------------------

