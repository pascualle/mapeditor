//---------------------------------------------------------------------------

#ifndef USpObjPropertyH
#define USpObjPropertyH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ValEdit.hpp>
#include <Buttons.hpp>
#include <Menus.hpp>
#include <Vcl.Samples.Spin.hpp>

#include "UObjCode.h"
#include <Vcl.Graphics.hpp>

//---------------------------------------------------------------------------

class TCommonResObjManager;
class TPropertiesControl;

class TFSpObjProperty : public TPrpFunctions
{
__published:
	TPopupMenu *PopupMenu;
	TMenuItem *N1;
	TMenuItem *N2;
	TPageControl *PageControl1;
	TTabSheet *TabSheet2;
	TLabel *Label11;
	TGroupBox *GroupBoxPos;
	TLabel *Label12;
	TLabel *Label14;
	TBitBtn *Bt_find_self;
	TTabSheet *TS_Memo;
	TMemo *Memo;
	TLabel *Label1;
	TComboBox *ComboBox_zone;
	TPageControl *PageControl;
	TTabSheet *TS_main;
	TLabel *Label13;
	TComboBox *CB_obj;
	TPageControl *PageControlProperty;
	TTabSheet *TSP_props;
	TLabel *Label4;
	TScrollBox *ScrollBox;
	TComboBox *CB_ParamTransmitOpt;
	TTabSheet *TSP_State;
	TImage *Image1;
	TLabel *Label6;
	TComboBox *CB_states;
	TCheckBox *CheckBox_newState;
	TListBox *ListBox;
	TBitBtn *Bt_st_add;
	TBitBtn *Bt_st_del;
	TPanel *Panel1;
	TTabSheet *TSP_Actions;
	TScrollBox *ScrollBoxActions;
	TTabSheet *TabSheet1;
	TComboBox *CB_objPos;
	TRadioButton *RB_ChPosObjCoord;
	TRadioButton *RB_ChPosTileCoord;
	TRadioButton *RB_ChPosNoChange;
	TBitBtn *Bt_find_node;
	TBitBtn *Bt_find_obj;
	TTabSheet *TS_map;
	TBevel *Bevel;
	TLabel *Label7;
	TLabel *Label8;
	TLabel *Label9;
	TRadioButton *RBDown;
	TRadioButton *RBLeft;
	TRadioButton *RBUp;
	TRadioButton *RBRight;
	TEdit *Edit_name;
	TImage *ImageLock;
	void __fastcall Edit_nameChange(TObject *Sender);
	void __fastcall ComboBox_zoneChange(TObject *Sender);
	void __fastcall ScrollBoxResize(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall CheckBox_newStateClick(TObject *Sender);
	void __fastcall ScrollBoxActionsResize(TObject *Sender);
    void __fastcall RB_ChPosNoChangeClick(TObject *Sender);
	void __fastcall Bt_find_nodeClick(TObject *Sender);
	void __fastcall Bt_find_objClick(TObject *Sender);
	void __fastcall CB_statesEnter(TObject *Sender);
	void __fastcall ListBoxExit(TObject *Sender);
	void __fastcall ListBoxClick(TObject *Sender);
	void __fastcall CB_statesChange(TObject *Sender);
	void __fastcall Image1Click(TObject *Sender);
	void __fastcall Bt_st_addClick(TObject *Sender);
	void __fastcall Bt_st_delClick(TObject *Sender);
	void __fastcall N1Click(TObject *Sender);
	void __fastcall N2Click(TObject *Sender);
	void __fastcall Edit_nameKeyPress(TObject *Sender, char &Key);
	void __fastcall Edit_nameEnter(TObject *Sender);
	void __fastcall Edit_nameExit(TObject *Sender);
	void __fastcall CSEd_xChange(TObject *Sender);
	void __fastcall CSEd_xEnter(TObject *Sender);
	void __fastcall CSEd_xExit(TObject *Sender);
	void __fastcall CB_ParamTransmitOptChange(TObject *Sender);
	void __fastcall CE_TileNUmChange(TObject *Sender);
	void __fastcall Bt_find_selfClick(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall MemoExit(TObject *Sender);
	void __fastcall MemoEnter(TObject *Sender);
	void __fastcall PageControlChange(TObject *Sender);
	void __fastcall RBLeftClick(TObject *Sender);
	void __fastcall CE_map_countChange(TObject *Sender);
	void __fastcall CE_map_idxChange(TObject *Sender);
	void __fastcall CE_TileNUmExit(TObject *Sender);
	void __fastcall CE_map_countExit(TObject *Sender);
	void __fastcall CE_map_idxExit(TObject *Sender);
	void __fastcall CB_ParamTransmitOptExit(TObject *Sender);
	void __fastcall CB_objExit(TObject *Sender);
	void __fastcall ComboBox_zoneExit(TObject *Sender);
	void __fastcall CB_objPosExit(TObject *Sender);
	void __fastcall CB_objKeyPress(TObject *Sender, char &Key);
	void __fastcall EmptyOnEnter(TObject *Sender);
	
private:

	UnicodeString      		mOldName, mOldCSEd_x, mOldMemo, mOldOnVar;
	UnicodeString      		NameObj;
	TMapSpGPerson 			*t_MapGP;
	TCommonResObjManager	*mpCommonResObjManager;
	TPropertiesControl		*mpPropertiesControl;
	const std::list<PropertyItem>* vars;
	bool            		icon_show_list;
	bool					disableChanges;
	bool            		inited;
	TSpinEdit *CE_map_count;
	TSpinEdit *CE_map_idx;
	TSpinEdit *CE_TileNUm;

	void __fastcall FakeEnterActiveControl();

	void __fastcall ChangeZone(int zone);
	void __fastcall ClearScrollBox();
	void __fastcall ClearScrollBoxActions();
	void __fastcall CheckBoxActionsClick(TObject *Sender);
	void __fastcall refreshActions();
	void __fastcall ShowStatePreview(UnicodeString name);
	void __fastcall EditScript(int mode);
	void __fastcall RefreshProperties();

	void __fastcall OnScriptChange(TObject *Sender);
	void __fastcall OnPropertiesControlOnCheck(const UnicodeString name, bool val);
	void __fastcall OnPropertiesControlValueValidate(const UnicodeString name, double val);

	virtual void __fastcall SetNodeU(UnicodeString NodeName) override {};
	virtual void __fastcall SetNodeD(UnicodeString NodeName) override {};
	virtual void __fastcall SetNodeL(UnicodeString NodeName) override {};
	virtual void __fastcall SetNodeR(UnicodeString NodeName) override {};
	virtual void __fastcall SetTrigger(UnicodeString triggerName) override {};
	virtual void __fastcall refreshTriggers() override {};
	virtual void __fastcall refreshCplxList() override {};
	virtual void __fastcall SetScript(UnicodeString KeyName, UnicodeString ScriptName) override {};
	virtual void __fastcall SetNode(UnicodeString NodeName) override {};
	virtual void __fastcall SetZone(int zone) override {};
	virtual void __fastcall SetState(UnicodeString name) override {};
	virtual void __fastcall SetVarValue(double val, UnicodeString var_name) override {};
	virtual void __fastcall SetRemarks(UnicodeString str) override {};
	virtual void __fastcall SetVariables(const GPersonProperties *prp) override {};
	virtual void __fastcall refreshVars() override {};
	virtual void __fastcall SetSizes(int , int ) override {};

	void __fastcall SaveAllValues(TWinControl *iParent);

public:

	TSpinEdit *CSEd_x;
	TSpinEdit *CSEd_y;

	__fastcall TFSpObjProperty(TComponent* Owner) override;

	virtual void __fastcall Init(TCommonResObjManager *commonResObjManager) override;
	virtual void __fastcall Localize() override;
	virtual void __fastcall SetTransform(const TTransformProperties& transform) override;
	virtual void __fastcall refreshNodes() override;
	virtual void __fastcall refresStates() override;
	virtual void __fastcall refreshScripts() override;
	virtual void __fastcall Reset() override;
	virtual void __fastcall EmulatorMode(bool mode)  override;
	virtual void __fastcall Lock(bool value) override;

	void __fastcall AssignSpMapGobj(TMapSpGPerson *sgp);
	TMapSpGPerson* __fastcall GetAssignedObj(){return t_MapGP;}
};
//---------------------------------------------------------------------------
extern PACKAGE TFSpObjProperty *FSpObjProperty;
//---------------------------------------------------------------------------
#endif
