//---------------------------------------------------------------------------

#ifndef UEditObjPatternH
#define UEditObjPatternH

//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

//---------------------------------------------------------------------------

class TFOPEdit : public TForm
{
__published:
	TEdit *Ed_name;
	TButton *ButtonOk;
	TLabel *Label1;
	TLabel *Label_def;
	TEdit *Edit_def;
	TLabel *Labelinfo;
	TEdit *EditPrefix;
	void __fastcall Ed_nameChange(TObject *Sender);
	void __fastcall Edit_defChange(TObject *Sender);
	void __fastcall ButtonOkClick(TObject *Sender);

public:
		enum TFOPEditMode
		{
			TFOPEMProperties = 0,
			TFOPEMAnimations,
			TFOPEMEvents,
			TFOPEMLanguages
		};

		__fastcall TFOPEdit(TComponent* Owner);
		void __fastcall SetMode(TFOPEditMode mode);
		void __fastcall SetVarName(UnicodeString txt);
		UnicodeString __fastcall GetVarName();

private:
		bool showWarning;
		bool isError;
		TFOPEditMode mMode;
};
//---------------------------------------------------------------------------
extern PACKAGE TFOPEdit *FOPEdit;
//---------------------------------------------------------------------------
#endif
