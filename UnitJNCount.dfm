object FJNCount: TFJNCount
  Left = 366
  Top = 269
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  ClientHeight = 271
  ClientWidth = 376
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ButtonOk: TButton
    Left = 177
    Top = 237
    Width = 100
    Height = 24
    Caption = 'mApply'
    ModalResult = 1
    TabOrder = 3
  end
  object ButtonCancel: TButton
    Left = 99
    Top = 237
    Width = 72
    Height = 24
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 2
  end
  object GroupBox_f: TGroupBox
    Left = 8
    Top = 7
    Width = 361
    Height = 111
    TabOrder = 0
    object Label2: TLabel
      Left = 11
      Top = 51
      Width = 37
      Height = 13
      Caption = 'mCount'
    end
  end
  object GroupBoxLayers: TGroupBox
    Left = 8
    Top = 124
    Width = 361
    Height = 103
    TabOrder = 1
    object Label14: TLabel
      Left = 11
      Top = 16
      Width = 339
      Height = 73
      AutoSize = False
      Caption = 'mJNCountDesctiption'
      WordWrap = True
    end
  end
end
