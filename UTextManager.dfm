object FormTextManager: TFormTextManager
  Left = 0
  Top = 0
  ActiveControl = TntStringGrid
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'mTexts'
  ClientHeight = 384
  ClientWidth = 709
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 300
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 709
    Height = 26
    Caption = 'ToolBar1'
    EdgeBorders = [ebTop, ebBottom]
    Images = Form_m.ImageList1
    TabOrder = 0
    object ToolButton5: TToolButton
      Left = 0
      Top = 0
      Action = ActNewMessage
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton2: TToolButton
      Left = 23
      Top = 0
      Action = ActEdit
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton1: TToolButton
      Left = 46
      Top = 0
      Action = ActDelMessage
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton3: TToolButton
      Left = 69
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object EditFilter: TEdit
      Left = 77
      Top = 0
      Width = 121
      Height = 22
      TabOrder = 0
      TextHint = 'mFilter'
      OnChange = EditFilterChange
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 365
    Width = 709
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object TntStringGrid: TStringGrid
    Left = 0
    Top = 26
    Width = 709
    Height = 276
    Align = alClient
    BorderStyle = bsNone
    ColCount = 2
    Ctl3D = True
    DefaultRowHeight = 22
    DoubleBuffered = False
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowSelect]
    ParentCtl3D = False
    ParentDoubleBuffered = False
    TabOrder = 1
    OnDblClick = TntStringGridDblClick
    OnKeyPress = TntStringGridKeyPress
    ColWidths = (
      210
      497)
  end
  object TabSet: TTabSet
    Left = 0
    Top = 302
    Width = 709
    Height = 22
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    SoftTop = True
    Style = tsSoftTabs
    Tabs.Strings = (
      '  DEFAULT  ')
    TabIndex = 0
    OnClick = TabSetClick
  end
  object PanelButtons: TPanel
    Left = 0
    Top = 324
    Width = 709
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    FullRepaint = False
    ShowCaption = False
    TabOrder = 3
    DesignSize = (
      709
      41)
    object Button1: TButton
      Left = 539
      Top = 10
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'mApply'
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 620
      Top = 10
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'mClose'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object MainMenu: TMainMenu
    AutoHotkeys = maManual
    Images = Form_m.ImageList1
    Left = 80
    Top = 232
    object N1: TMenuItem
      Caption = 'mFile'
      object N3: TMenuItem
        Action = ActLoad
      end
      object N4: TMenuItem
        Action = ActSave
      end
    end
    object N2: TMenuItem
      Caption = 'mEdit'
      object N5: TMenuItem
        Action = ActNewMessage
      end
      object N7: TMenuItem
        Action = ActEdit
      end
      object N6: TMenuItem
        Action = ActDelMessage
      end
    end
  end
  object ActionList1: TActionList
    Images = Form_m.ImageList1
    Left = 16
    Top = 232
    object ActSave: TAction
      Caption = 'mExport'
      ImageIndex = 8
    end
    object ActLoad: TAction
      Caption = 'mImport'
      ImageIndex = 9
    end
    object ActNewMessage: TAction
      Caption = 'mAddItem'
      ImageIndex = 0
      OnExecute = ActNewMessageExecute
    end
    object ActDelMessage: TAction
      Caption = 'mDeleteItem'
      ImageIndex = 6
      OnExecute = ActDelMessageExecute
    end
    object ActEdit: TAction
      Caption = 'mEditItem'
      ImageIndex = 7
      OnExecute = ActEditExecute
    end
  end
end
