//---------------------------------------------------------------------------

#ifndef UStateListH
#define UStateListH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------

class TFStateList : public TForm
{
__published:	// IDE-managed Components
	TButton *ButtonOk;
	TButton *ButtonCancel;
	TPageControl *PageControl;
	TTabSheet *TabStates;
	TLabel *LabelInfo;
	TTabSheet *TabList;
	TListBox *ListBoxView;
	TPanel *Panel1;
	TBitBtn *Bt_del;
	TBitBtn *Bt_edit;
	TBitBtn *Bt_add;
	TBevel *Bevel3;
	TStatusBar *StatusBar;
	TLabel *Label1;
	TEdit *EdFind;
	TBitBtn *BtCrearFilter;
	TBitBtn *Bt_trup;
	TBitBtn *Bt_trdown;
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall Bt_addClick(TObject *Sender);
	void __fastcall Bt_editClick(TObject *Sender);
	void __fastcall Bt_delClick(TObject *Sender);
	void __fastcall ListBoxViewDblClick(TObject *Sender);
	void __fastcall ListBoxViewKeyPress(TObject *Sender, char &Key);
	void __fastcall ListBoxViewClick(TObject *Sender);
	void __fastcall EdFindChange(TObject *Sender);
	void __fastcall BtCrearFilterClick(TObject *Sender);
	void __fastcall Bt_trupClick(TObject *Sender);
	void __fastcall Bt_trdownClick(TObject *Sender);

private:

	TList           *list;
	int             mode;
	TStringList     *changeList;
	TStringList		*listBox;
	void __fastcall Add();
	void __fastcall Edit(int idx);
	void __fastcall Delete(int idx);
	void __fastcall CreateList(UnicodeString selected);
	void __fastcall SetList(TList *states, int mode);
	void __fastcall GetList(TList *newlist, TStringList *changelist, int mode);
	void __fastcall CreateChangeList(TStringList *sl);
	void __fastcall WriteDeleteToChangeList(UnicodeString value);
	void __fastcall WriteAddToChangeList(UnicodeString value);
	void __fastcall WriteRenameToChangeList(UnicodeString oldName, UnicodeString newName);
	void __fastcall EditState(int idx);
	void __fastcall EditBackProperties(int idx);
	void __fastcall EditOppositeStates(int idx);
	void __fastcall EditAnimTypes(int idx);
	void __fastcall EditEventTypes(int idx);
	void __fastcall EditLangTypes(int idx);
	int __fastcall GetPos();

public:

	void __fastcall SetStates(TList *states);
	void __fastcall GetStates(TList *states, TStringList *changelist);

	void __fastcall SetBackProperties(TList *bgproperties);
	void __fastcall GetBackProperties(TList *bgproperties, TStringList *changelist);

	void __fastcall SetOppositeStates(TStringList *sl);
	void __fastcall GetOppositeStates(TStringList *sl, TStringList *changelist);

	void __fastcall SetAnimTypes(TStringList *sl);
	void __fastcall GetAnimTypes(TStringList *sl, TStringList *changelist);

	void __fastcall SetEventTypes(TStringList *sl);
	void __fastcall GetEventTypes(TStringList *sl, TStringList *changelist);

	void __fastcall SetLanguageTypes(TStringList *sl);
	void __fastcall GetLanguageTypes(TStringList *sl, TStringList *changelist);

	void __fastcall SetInfo(UnicodeString info);
	__fastcall TFStateList(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFStateList *FStateList;
//---------------------------------------------------------------------------
#endif

