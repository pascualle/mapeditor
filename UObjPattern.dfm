object FObjPattern: TFObjPattern
  Left = 400
  Top = 327
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  ClientHeight = 340
  ClientWidth = 321
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Pitch = fpFixed
  Font.Style = []
  Menu = MainMenu1
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  DesignSize = (
    321
    340)
  TextHeight = 13
  object ButtonCancel: TButton
    Left = 57
    Top = 305
    Width = 76
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 2
  end
  object ButtonOk: TButton
    Left = 139
    Top = 305
    Width = 121
    Height = 25
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'mApply'
    ModalResult = 1
    TabOrder = 1
    ExplicitWidth = 125
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 321
    Height = 297
    ActivePage = TabSheet1
    Align = alTop
    TabOrder = 0
    ExplicitWidth = 317
    object TabSheet1: TTabSheet
      Caption = 'mProperties'
      object ListBox: TListBox
        Left = 13
        Top = 13
        Width = 261
        Height = 240
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnDblClick = ListBoxDblClick
        OnKeyDown = ListBoxKeyDown
      end
      object Bt_add: TBitBtn
        Left = 274
        Top = 15
        Width = 24
        Height = 24
        Hint = 'mAdd'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD000DDDDDDDDDDDD02227DD
          DDDDDDDDD02227DDDDDDDDD000222777DDDDDD02222222227DDDDD0222222222
          7DDDDD02222222227DDDDDD000222777DDDDDDDDD02227DDDDDDDDDDD02227DD
          DDDDDDDDDD777DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = Bt_addClick
      end
      object Bt_edit: TBitBtn
        Left = 274
        Top = 38
        Width = 24
        Height = 24
        Hint = 'mEdit'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDD777777777DDDDDD09999999997DDDD00999999
          9997DDD0209999999997DDD022000000000DD000222777DDDDDD02222222227D
          DDDD02222222227DDDDD02222222227DDDDDD000222777DDDDDDDDD02227DDDD
          DDDDDDD02227DDDDDDDDDDDD777DDDDDDDDDDDDDDDDDDDDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = Bt_addClick
      end
      object Bt_del: TBitBtn
        Left = 274
        Top = 61
        Width = 24
        Height = 24
        Hint = 'mDoDelete'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDD777777777DDDDDD09999999997DDDDD0999999999
          7DDDDD09999999997DDDDDD000000000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = Bt_addClick
      end
      object Panel1: TPanel
        Left = 274
        Top = 84
        Width = 24
        Height = 118
        TabOrder = 4
      end
      object Bt_up: TBitBtn
        Left = 274
        Top = 202
        Width = 24
        Height = 24
        Hint = 'mMoveUp'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDD000DDDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DD
          DDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DD
          DDDDDDDDD0CCC7DDDDDDDD0000CCC7777DDDDD0CCCCCCCCC7DDDDDD0CCCCCCC7
          DDDDDDDD0CCCCC7DDDDDDDDDD0CCC7DDDDDDDDDDDD777DDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = Bt_upClick
      end
      object Bt_down: TBitBtn
        Left = 274
        Top = 225
        Width = 24
        Height = 24
        Hint = 'mMoveDown'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD777DDD
          DDDDDDDDD0CCC7DDDDDDDDDD0CCCCC7DDDDDDDD0CCCCCCC7DDDDDD0CCCCCCCCC
          7DDDDD0000CCC7777DDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DD
          DDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DD
          DDDDDDDDD0CCC7DDDDDDDDDDDD000DDDDDDDDDDDDDDDDDDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = Bt_downClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'mMessageBoxInfo'
      ImageIndex = 1
      object Label1: TLabel
        Left = 13
        Top = 8
        Width = 136
        Height = 13
        Caption = 'mPredefinedVarsInfoHeader'
      end
      object Label2: TLabel
        Left = 13
        Top = 46
        Width = 292
        Height = 47
        AutoSize = False
        Caption = 'mPredefinedVarsInfoPrpEnable'
        WordWrap = True
      end
      object Label3: TLabel
        Left = 13
        Top = 115
        Width = 292
        Height = 62
        AutoSize = False
        Caption = 'mPredefinedVarsInfoPrpSpeed'
        WordWrap = True
      end
      object StaticText1: TStaticText
        Left = 13
        Top = 32
        Width = 66
        Height = 17
        Caption = 'PRP_ENABLE'
        TabOrder = 0
      end
      object StaticText2: TStaticText
        Left = 13
        Top = 100
        Width = 60
        Height = 17
        Caption = 'PRP_SPEED'
        TabOrder = 1
      end
    end
  end
  object MainMenu1: TMainMenu
    Images = Form_m.ImageList1
    Left = 8
    Top = 251
    object N1: TMenuItem
      Caption = 'mFile'
      object N4: TMenuItem
        Caption = 'mNew'
        ImageIndex = 0
        OnClick = N4Click
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object N7: TMenuItem
        Caption = 'mOpen'
        ImageIndex = 1
        OnClick = N7Click
      end
      object N6: TMenuItem
        Caption = 'mSave'
        ImageIndex = 2
        OnClick = N6Click
      end
    end
    object N2: TMenuItem
      Caption = 'mEditing'
      object N3: TMenuItem
        Caption = 'mClearAll'
        OnClick = N3Click
      end
    end
  end
  object OpenDialog: TOpenDialog
    Left = 48
    Top = 251
  end
  object SaveDialog: TSaveDialog
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 88
    Top = 251
  end
end
