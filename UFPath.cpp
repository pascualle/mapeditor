#include <vcl.h>
#pragma hdrstop

#include "UFPath.h"
#include "FileCtrl.hpp"
#include "ULocalization.h"
#include "MainUnit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFProjPath *FProjPath;

//---------------------------------------------------------------------------
__fastcall TFProjPath::TFProjPath(TComponent* Owner)
        : TForm(Owner)
{
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFProjPath::BtG_3dotClick(TObject */*Sender*/)
{
	TSelectDirExtOpts opt;
	System::UnicodeString Directory = EdPngPath->Text;
	opt<<sdShowShares<<sdNewUI<<sdValidateDir;
	if(SelectDirectory(Label2->Caption, UnicodeString(_T("")), Directory, opt, this))
	{
		EdPngPath->Text = ExtractRelativePath(Form_m->PathIni, Directory + _T("\\"));
	}
}
//---------------------------------------------------------------------------
