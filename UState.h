//---------------------------------------------------------------------------

#ifndef UStateH
#define UStateH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include "UObjCode.h"
//---------------------------------------------------------------------------
class TFEditState : public TForm
{
__published:
	TGroupBox *GroupBoxMovement;
	TImage *Image;
	TComboBox *CB_dir;
	TEdit *Ed_name;
	TLabel *Label5;
	TGroupBox *GroupBoxAnimation;
	TComboBox *CB_AniType;
	TButton *Button;
	TButton *ButtonCancel;
	TEdit *EditPrefix;
	TLabel *Label1;
	TEdit *EditFilter;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall Ed_nameChange(TObject *Sender);
	void __fastcall CB_dirChange(TObject *Sender);
	void __fastcall EditScriptChange(TObject *Sender);
	void __fastcall EditFilterChange(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall CB_AniTypeEnter(TObject *Sender);
	void __fastcall CB_AniTypeExit(TObject *Sender);
private:

	void __fastcall CheckBoxAlienChanged();
	void __fastcall SetVarName(UnicodeString txt);
	UnicodeString __fastcall GetVarName();

	TStringList *mpTempFilterList;
	UnicodeString mOldCBValue;

public:

	__fastcall TFEditState(TComponent* Owner);
	void __fastcall SetState(TState *state);
	void __fastcall GetState(TState *state);
};
//---------------------------------------------------------------------------
extern PACKAGE TFEditState *FEditState;
//---------------------------------------------------------------------------
#endif

