MapEditor3

Levels, tilemaps, resources, animations, properties, and states editor.
It also provides final data for tengine.

## Gallery
![mainscreen](https://lh5.googleusercontent.com/-Dua8I7TZXjg/VBQ2kc2S14I/AAAAAAAAFCA/24HaHAHJYE0/w800-h600-no/tengine_img.png)