#ifndef TEXTS_H
#define TEXTS_H

#include "platform.h"
#include <stdio.h>
#include <Classes.hpp>

class BMPImage;
class TImageFont;
class TSaveLoadStreamData;
class TSaveLoadStreamHandle;

static const s16 DEFAULT_TEXT_MARGIN = 1;

typedef enum TextAlignment
{
	TEXT_ALIGNMENT_LEFT		= 0x00011000,
	TEXT_ALIGNMENT_RIGHT	= 0x00110000,
	TEXT_ALIGNMENT_TOP		= 0x00000011,
	TEXT_ALIGNMENT_BOTTOM	= 0x00000110,
	TEXT_ALIGNMENT_VCENTER	= 0x00000111,
	TEXT_ALIGNMENT_HCENTER	= 0x00111000
}TextAlignment;
//----------------------------------------------------------------------------------------------------------------

typedef struct AlphabetTextureOffsetMap
{
	s16 mX;
	s16 mY;
	s16 mW;
	s16 mH;
	s16 mX_off;
	s16 mY_off;
}AlphabetTextureOffsetMap;
//----------------------------------------------------------------------------------------------------------------

typedef struct TextAlphabet
{
	u16	mFixedWidth;	///< Value of fixed width, this parameter has no effect when mpAlphabetTextureOffsetMap != NULL 
	u16	mFixedHeight;	///< Value of fixed height, this parameter has no effect when mpAlphabetTextureOffsetMap != NULL
	u16	mAlphabetSize;	///< Alphabet size (in characters)
	wchar_t	*mpAlphabet;	///< pointer to alphabet string. Can be NULL, in this case Text object will be use global alphabet values
	AlphabetTextureOffsetMap *mpAlphabetTextureOffsetMap;	///< pointer to sequence of Rect that contain X, Y offset on 2d texture and width and height of each char. Can be NULL, in this case it is expected, that 2d texture contains chars with fixed width and height
}TextAlphabet;
//----------------------------------------------------------------------------------------------------------------

typedef struct TextAttributes
{
	s16 mMargin;
	s32	mAlignment;
	TColor mCanvasColor;
}TextAttributes;
//----------------------------------------------------------------------------------------------------------------

typedef struct Font
{
	//u8* mpAbcBmpData;
	BMPImage *mpAbcImage;
	TextAlphabet mAlphabet;
}Font;
//----------------------------------------------------------------------------------------------------------------

typedef struct Text
{
	s16 mStringsCount;
	s16 mStringsCountMax;
	wchar_t** mppStrings;
	const Font* mpFont;
    BMPImage *mpCanvas;
	TextAttributes mAttributes;
}Text;
//----------------------------------------------------------------------------------------------------------------

void InitFont(u8* iData, Font *oFont);

void ReleaseFont(Font *oFont);

void InitText(::Text *oText);

void ReleaseText(::Text *oText);

void SetTextCanvas(u16 canvasWidth, u16 canvasHeight, ::Text *opText);

void SetText(const wchar_t* ipText, ::Text *opText);

//----------------------------------------------------------------------------------------------------------------

class TUnicodeText
{
 public:
	__fastcall TUnicodeText();
	__fastcall TUnicodeText(const TUnicodeText &t);
	__fastcall virtual ~TUnicodeText();

	const UnicodeString __fastcall GetFontName() const;
	void __fastcall SetFontName(UnicodeString fontName);

	void __fastcall SetText(const wchar_t* txt);
	void __fastcall SetCanvasSize(int w, int h);
	void __fastcall GetCanvasSize(int &w, int &h) const;

	void __fastcall SetAttributes(const ::TextAttributes attr);
	const ::TextAttributes __fastcall GetAttributes() const ;

	void __fastcall Save(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h) const;
	void __fastcall Load(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, unsigned char ver);

	const ::Text* __fastcall GetTextData() const;
	int __fastcall GetCanvasWidth() const;
	int __fastcall GetCanvasHeight() const;

	bool __fastcall IsEmpty() const;
	void __fastcall CopyTo(TUnicodeText *pt) const;
	void __fastcall Clear();

 private:

	void __fastcall FillIfEmpty();

	::Text				mText;
	UnicodeString		mLastFontName;
	int					mWidth;
	int					mHeight;
};
//----------------------------------------------------------------------------------------------------------------

#endif //TEXTS_H