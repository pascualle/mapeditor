object FEditBGOType: TFEditBGOType
  Left = 329
  Top = 307
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'mTileObjectPropertiesCaption'
  ClientHeight = 181
  ClientWidth = 341
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label: TLabel
    Left = 73
    Top = 77
    Width = 153
    Height = 13
    Caption = 'mTileObjectAdditionalParameter'
  end
  object Label5: TLabel
    Left = 18
    Top = 17
    Width = 35
    Height = 13
    Caption = 'mName'
  end
  object ButtonCancel: TButton
    Left = 67
    Top = 146
    Width = 76
    Height = 25
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 2
  end
  object ButtonOk: TButton
    Left = 149
    Top = 146
    Width = 125
    Height = 25
    Caption = 'mApply'
    ModalResult = 1
    TabOrder = 4
  end
  object CheckBoxSolid: TCheckBox
    Left = 18
    Top = 43
    Width = 309
    Height = 17
    Caption = 'mTileObjectSolid'
    TabOrder = 1
  end
  object CheckBoxDirector: TCheckBox
    Left = 18
    Top = 113
    Width = 309
    Height = 17
    Caption = 'mTileObjectAsDirector'
    Enabled = False
    TabOrder = 3
  end
  object Edit_name: TEdit
    Left = 67
    Top = 13
    Width = 262
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 64
    TabOrder = 0
    OnChange = Edit_nameChange
  end
end
