//---------------------------------------------------------------------------

#ifndef UCollideNodePropertyH
#define UCollideNodePropertyH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "UObjCode.h"
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>

class TCommonResObjManager;
class TMapCollideLineNode;

//---------------------------------------------------------------------------
class TFCollideNodeProperty : public TPrpFunctions
{
__published:
	TPageControl *PageControl;
	TTabSheet *TS_main;
	TLabel *Label5;
	TEdit *Edit_name;
	TGroupBox *GroupBox_pos;
	TLabel *Label2;
	TLabel *Label3;
	TEdit *CSEd_x;
	TEdit *CSEd_y;
	TBitBtn *BitBtn_find_obj;
	TTabSheet *TS_Memo;
	TMemo *Memo;
	TStatusBar *StatusBar1;
	void __fastcall BitBtn_find_objClick(TObject *Sender);
	void __fastcall Edit_nameEnter(TObject *Sender);
	void __fastcall Edit_nameExit(TObject *Sender);
	void __fastcall Edit_nameKeyPress(TObject *Sender, System::WideChar &Key);
	void __fastcall CSEd_xEnter(TObject *Sender);
	void __fastcall CSEd_xExit(TObject *Sender);
	void __fastcall CSEd_xKeyPress(TObject *Sender, System::WideChar &Key);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall StatusBar1DrawPanel(TStatusBar *StatusBar, TStatusPanel *Panel, const TRect &Rect);
private:

	TMapCollideLineNode *mpCollideLineNodeObj;
	UnicodeString  mOldName;
	UnicodeString  mOldVal;
	UnicodeString  mNameObj;
	void *mpEditField;
	bool mEmulatorMode;
	bool mDisableChanges;

	void __fastcall SaveAllValues(TWinControl *iParent);
	void __fastcall FakeEnterActiveControl();

	virtual void __fastcall SetNodeU(UnicodeString NodeName) override {}
	virtual void __fastcall SetNodeD(UnicodeString NodeName) override {}
	virtual void __fastcall SetNodeL(UnicodeString NodeName) override {}
	virtual void __fastcall SetNodeR(UnicodeString NodeName) override {}
	virtual void __fastcall SetTrigger(UnicodeString triggerName) override {}
	virtual void __fastcall refreshTriggers() override {}
	virtual void __fastcall SetState(UnicodeString name) override {}
	virtual void __fastcall SetVarValue(double val, UnicodeString var_name) override {}
	virtual void __fastcall SetVariables(const GPersonProperties *prp) override {}
	virtual void __fastcall SetScript(UnicodeString KeyName, UnicodeString ScriptName) override {}
	virtual void __fastcall SetNode(UnicodeString NodeName) override {}
	virtual void __fastcall refreshNodes() override {}
	virtual void __fastcall refreshCplxList() override {}
	virtual void __fastcall refresStates() override {}
	virtual void __fastcall refreshVars() override {}
	virtual void __fastcall refreshScripts() override {}
	virtual void __fastcall SetZone(int zone) override {};
	virtual void __fastcall SetSizes(int w, int h) override {};

public:
	__fastcall TFCollideNodeProperty(TComponent* Owner) override;

	virtual void __fastcall Init(TCommonResObjManager *resManager) override;
	virtual void __fastcall EmulatorMode(bool mode) override;
	virtual void __fastcall Localize() override;
	virtual void __fastcall SetRemarks(UnicodeString str) override;
	virtual void __fastcall SetTransform(const TTransformProperties& transform) override;
	virtual void __fastcall Reset() override;
	virtual void __fastcall Lock(bool value) override;

	void __fastcall AssignCollideLineNodeObj(TMapCollideLineNode *obj);
	TMapCollideLineNode* __fastcall GetAssignedCollideLineNode(){return mpCollideLineNodeObj;}

};
//---------------------------------------------------------------------------
extern PACKAGE TFCollideNodeProperty *FCollideNodeProperty;
//---------------------------------------------------------------------------
#endif
