//---------------------------------------------------------------------------

#ifndef UTransformPropertiesH
#define UTransformPropertiesH

class TSaveLoadStreamData;
class TSaveLoadStreamHandle;

class TTransformProperties
{
 private:

	double mX;
	double mY;
	double mScaleX;
	double mScaleY;
	double mAngle;

 public:

	__fastcall TTransformProperties();

	void __fastcall SetRotation(const double& angleDegree);
	const double& __fastcall GetRotation() const;

	void __fastcall SetScale(const double& xScale, const double& yScale);
	void __fastcall GetScale(double& xScale, double& yScale) const;

	void __fastcall SetPosition(const double& x, const double& y);
	void __fastcall GetPosition(double& x, double& y) const;

	void __fastcall Save(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h) const;
	void __fastcall Load(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, unsigned char ver);
};
//---------------------------------------------------------------------------
#endif
