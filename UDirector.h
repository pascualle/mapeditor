//---------------------------------------------------------------------------

#ifndef UDirectorH
#define UDirectorH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Vcl.Samples.Spin.hpp>

#include "UObjCode.h"
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>
//---------------------------------------------------------------------------

class TFDirectorProperty : public TPrpFunctions
{
__published:
	TPageControl *PageControl;
	TTabSheet *TS_main;
	TTabSheet *TS_Memo;
	TGroupBox *GroupBoxPos;
	TLabel *Label2;
	TLabel *Label3;
	TGroupBox *GroupBoxDirection;
	TLabel *Label1;
	TLabel *Label4;
	TLabel *Label6;
	TLabel *Label7;
	TComboBox *CB_Down;
	TComboBox *CB_Left;
	TComboBox *CB_Up;
	TComboBox *CB_Right;
	TBitBtn *Bt_find2;
	TBitBtn *Bt_find0;
	TBitBtn *Bt_find1;
	TBitBtn *Bt_find3;
	TGroupBox *GroupBoxTrigger;
	TComboBox *CB_Trigger;
	TBitBtn *Bt_find4;
	TEdit *Edit_name;
	TMemo *Memo;
	TBitBtn *Bt_find_obj;
	TLabel *Label8;
	TGroupBox *GroupBox_event;
	TComboBox *ComboBox_Event;
	TImage *ImageLock;
	void __fastcall Bt_find3Click(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall Edit_nameChange(TObject *Sender);
	void __fastcall Edit_nameEnter(TObject *Sender);
	void __fastcall Edit_nameExit(TObject *Sender);
	void __fastcall CSEd_xChange(TObject *Sender);
	void __fastcall CSEd_xEnter(TObject *Sender);
	void __fastcall CSEd_xExit(TObject *Sender);
	void __fastcall MemoEnter(TObject *Sender);
	void __fastcall MemoExit(TObject *Sender);
	void __fastcall CB_TriggerExit(TObject *Sender);
	void __fastcall CB_UpExit(TObject *Sender);
	void __fastcall CB_UpKeyPress(TObject *Sender, char &Key);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall ComboBox_EventExit(TObject *Sender);

private:

		UnicodeString    mOldName, mOldValue;
		UnicodeString    mNameObj;
		TMapDirector* t_MapGP;
		bool          disableChanges;

		void __fastcall FakeEnterActiveControl();
		void __fastcall SaveAllValues(TWinControl *iParent);

		virtual void __fastcall SetVariables(const GPersonProperties *prp) override {};
		virtual void __fastcall SetScript(UnicodeString KeyName, UnicodeString ScriptName) override {};
		virtual void __fastcall SetNode(UnicodeString NodeName) override {};
		virtual void __fastcall SetZone(int zone) override {};
		virtual void __fastcall SetState(UnicodeString name) override {};
		virtual void __fastcall SetVarValue(double val, UnicodeString var_name) override {};
		virtual void __fastcall refreshCplxList() override {};
		virtual void __fastcall refresStates() override {};
		virtual void __fastcall refreshVars() override {};
		virtual void __fastcall refreshScripts() override {};

public:
		TSpinEdit *CSEd_y;
		TSpinEdit *CSEd_x;

		__fastcall TFDirectorProperty(TComponent* Owner) override;

		virtual void __fastcall Init(TCommonResObjManager *commonResObjManager) override;
		virtual void __fastcall Localize() override;

		TMapDirector* __fastcall GetAssignedObj(){return t_MapGP;}
		void __fastcall AssignDirector(TMapDirector* in);

		virtual void __fastcall SetRemarks(UnicodeString str) override;
		virtual void __fastcall SetTransform(const TTransformProperties& transform) override;
		virtual void __fastcall SetNodeU(UnicodeString NodeName) override;
		virtual void __fastcall SetNodeD(UnicodeString NodeName) override;
		virtual void __fastcall SetNodeL(UnicodeString NodeName) override;
		virtual void __fastcall SetNodeR(UnicodeString NodeName) override;
		virtual void __fastcall SetTrigger(UnicodeString triggerName) override;
		virtual void __fastcall EmulatorMode(bool mode)  override;
		virtual void __fastcall Lock(bool value) override;
		virtual void __fastcall refreshNodes() override;
		virtual void __fastcall refreshTriggers() override;

		virtual void __fastcall Reset() override;

		void __fastcall refreshEvents();
};
//---------------------------------------------------------------------------
extern PACKAGE TFDirectorProperty *FDirectorProperty;
//---------------------------------------------------------------------------
#endif
