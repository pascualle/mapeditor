//---------------------------------------------------------------------------

#pragma hdrstop

#include "UTransformProperties.h"
#include "USaveLoadStreamData.h"
#include "UObjCode.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)


__fastcall TTransformProperties::TTransformProperties()
: mX(0.0)
, mY(0.0)
, mScaleX(1.0)
, mScaleY(1.0)
, mAngle(0.0)
{
}
//---------------------------------------------------------------------------

void __fastcall TTransformProperties::SetRotation(const double& angleDegree)
{
	mAngle = angleDegree;
}
//---------------------------------------------------------------------------

const double& __fastcall TTransformProperties::GetRotation() const
{
	return mAngle;
}
//---------------------------------------------------------------------------

void __fastcall TTransformProperties::SetScale(const double& xScale, const double& yScale)
{
	mScaleX = xScale;
	mScaleY = yScale;
}
//---------------------------------------------------------------------------
void __fastcall TTransformProperties::GetScale(double& xScale, double& yScale) const
{
	xScale = mScaleX;
	yScale = mScaleY;
}
//---------------------------------------------------------------------------

void __fastcall TTransformProperties::SetPosition(const double& x, const double& y)
{
	mX = x;
	mY = y;
}
//---------------------------------------------------------------------------

void __fastcall TTransformProperties::GetPosition(double& x, double& y) const
{
	x = mX;
	y = mY;
}
//---------------------------------------------------------------------------

void __fastcall TTransformProperties::Load(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, unsigned char ver)
{
    __int32 x, y;
	io.readFn(&x, TSaveLoadStreamData::TSLFieldType::ftS32, "x", h);
	io.readFn(&y, TSaveLoadStreamData::TSLFieldType::ftS32, "y", h);
	mX = static_cast<double>(x);
	mY = static_cast<double>(y);
	if(ver >= 35)
	{
		io.readFn(&mScaleX, TSaveLoadStreamData::TSLFieldType::ftDouble, "scalex", h);
		io.readFn(&mScaleY, TSaveLoadStreamData::TSLFieldType::ftDouble, "scaley", h);
		io.readFn(&mAngle, TSaveLoadStreamData::TSLFieldType::ftDouble, "angle", h);
	}
}
//---------------------------------------------------------------------------

void __fastcall TTransformProperties::Save(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h) const
{
	__int32 x, y;
	x = static_cast<__int32>(mX);
	y = static_cast<__int32>(mY);
	io.writeFn(&x, TSaveLoadStreamData::TSLFieldType::ftS32, "x", h);
	io.writeFn(&y, TSaveLoadStreamData::TSLFieldType::ftS32, "y", h);

	double f_val;
	f_val = mScaleX;
	io.writeFn(&f_val, TSaveLoadStreamData::TSLFieldType::ftDouble, "scalex", h);

	f_val = mScaleY;
	io.writeFn(&f_val, TSaveLoadStreamData::TSLFieldType::ftDouble, "scaley", h);

	f_val = mAngle;
	io.writeFn(&f_val, TSaveLoadStreamData::TSLFieldType::ftDouble, "angle", h);
}
//---------------------------------------------------------------------------

