object FBGObjWindow: TFBGObjWindow
  Left = 0
  Top = 0
  BorderStyle = bsSizeToolWin
  Caption = 'mTileObjectProperties'
  ClientHeight = 556
  ClientWidth = 522
  Color = clBtnFace
  Constraints.MinHeight = 182
  Constraints.MinWidth = 282
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  ScreenSnap = True
  OnDestroy = FormDestroy
  TextHeight = 13
  object DrawGrid: TDrawGrid
    Left = 0
    Top = 25
    Width = 522
    Height = 486
    Align = alClient
    ColCount = 256
    DefaultColWidth = 8
    DefaultRowHeight = 8
    DefaultDrawing = False
    FixedCols = 0
    RowCount = 256
    FixedRows = 0
    Options = [goVertLine, goHorzLine, goRangeSelect, goThumbTracking]
    TabOrder = 0
    OnDblClick = DrawGridDblClick
    OnDragDrop = DrawGridDragDrop
    OnDragOver = DrawGridDragOver
    OnDrawCell = DrawGridDrawCell
    OnKeyPress = DrawGridKeyPress
    OnMouseDown = DrawGridMouseDown
    OnSelectCell = DrawGridSelectCell
    OnStartDrag = DrawGridStartDrag
    ExplicitTop = 26
    ExplicitHeight = 485
    ColWidths = (
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8)
    RowHeights = (
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8
      8)
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 522
    Height = 25
    ButtonHeight = 24
    ButtonWidth = 24
    Caption = 'ToolBar1'
    Images = Form_m.ImageList1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    object TBGrid: TToolButton
      Left = 0
      Top = 0
      Action = ActGrid
    end
    object ToolButtonStayOnTop: TToolButton
      Left = 24
      Top = 0
      Action = ActStayOnTop
    end
    object ToolButton2: TToolButton
      Left = 48
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 28
      Style = tbsSeparator
    end
    object ToolButton1: TToolButton
      Left = 56
      Top = 0
      Action = ActEdit
    end
    object Panel2: TPanel
      Left = 80
      Top = 0
      Width = 157
      Height = 24
      BevelOuter = bvNone
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 0
      object CB_Layer: TComboBox
        Left = 91
        Top = 0
        Width = 62
        Height = 21
        Style = csDropDownList
        TabOrder = 0
        OnChange = CB_LayerChange
      end
      object StaticText: TStaticText
        Left = 9
        Top = 4
        Width = 69
        Height = 17
        Caption = 'mActiveLayer'
        TabOrder = 1
      end
    end
    object ToolButton5: TToolButton
      Left = 237
      Top = 0
      Action = ActBGLayerVisibility
    end
    object ToolButton3: TToolButton
      Left = 261
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 29
      Style = tbsSeparator
    end
    object ToolButtonAutoCreate: TToolButton
      Left = 269
      Top = 0
      Action = ActAutoCreate
    end
    object ToolButton4: TToolButton
      Left = 293
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 29
      Style = tbsSeparator
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 537
    Width = 522
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 511
    Width = 522
    Height = 26
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Label2: TLabel
      Left = 8
      Top = 6
      Width = 33
      Height = 13
      Caption = 'mScale'
    end
    object CB_Zoom: TComboBox
      Left = 60
      Top = 2
      Width = 77
      Height = 21
      Style = csDropDownList
      ItemIndex = 3
      TabOrder = 0
      Text = 'x1'
      OnChange = CB_ZoomChange
      Items.Strings = (
        'x0.125'
        'x0.25'
        'x0.5'
        'x1'
        'x2'
        'x4'
        'x8')
    end
  end
  object ActionList1: TActionList
    Images = Form_m.ImageList1
    Left = 24
    Top = 32
    object ActGrid: TAction
      AutoCheck = True
      Caption = 'mGrid'
      Hint = 'mGrid'
      ImageIndex = 26
      ShortCut = 49223
      OnExecute = ActGridExecute
    end
    object ActDel: TAction
      Caption = 'mDoDelete'
      Hint = 'mDoDelete'
      ImageIndex = 6
      OnExecute = ActDelExecute
    end
    object ActEdit: TAction
      Caption = 'mChange'
      Hint = 'mChange'
      ImageIndex = 7
      OnExecute = ActEditExecute
    end
    object ActCopy: TAction
      Caption = 'mCopy'
      Hint = 'mCopy'
      ShortCut = 16451
      OnExecute = ActCopyExecute
    end
    object ActPaste: TAction
      Caption = 'mPaste'
      Hint = 'mPaste'
      ShortCut = 16470
      OnExecute = ActPasteExecute
    end
    object ActStayOnTop: TAction
      Caption = 'mStayOnTopWindow'
      Hint = 'mStayOnTopWindow'
      ImageIndex = 27
      OnExecute = ActStayOnTopExecute
    end
    object ActAutoCreate: TAction
      Caption = 'mAutoCreateTiles'
      Hint = 'mAutoCreateTiles'
      ImageIndex = 28
      OnExecute = ActAutoCreateExecute
    end
    object ActBGLayerVisibility: TAction
      Caption = 'mBGLayerVisibility'
      Checked = True
      ImageIndex = 15
      OnExecute = ActBGLayerVisibilityExecute
    end
  end
  object PopupMenu: TPopupMenu
    Images = Form_m.ImageList1
    Left = 96
    Top = 32
    object N1: TMenuItem
      Action = ActEdit
    end
    object N2: TMenuItem
      Action = ActDel
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Action = ActCopy
    end
    object N5: TMenuItem
      Action = ActPaste
    end
  end
end
