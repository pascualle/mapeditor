//---------------------------------------------------------------------------

#ifndef UOppositeStateH
#define UOppositeStateH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFOppState : public TForm
{
__published:
        TButton *ButtonCancel;
        TButton *ButtonOk;
        TComboBox *ComboBoxState;
        TComboBox *ComboBoxInvState;
        TLabel *Label1;
        TLabel *Label2;
        void __fastcall ButtonOkClick(TObject *Sender);
private:
public:
		void __fastcall SetStateStrigCondition(UnicodeString str);
        void __fastcall GetStateStrigCondition(UnicodeString *str);
        __fastcall TFOppState(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFOppState *FOppState;
//---------------------------------------------------------------------------
#endif
