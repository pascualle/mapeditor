#pragma hdrstop

#include "USaveMap.h"
#include "URenderThread.h"
#include "UWait.h"
#include "MainUnit.h"
#include "UScript.h"
#include "UObjCode.h"
#include "UObjManager.h"
#include "UGameField.h"
#include "FileCtrl.hpp"
#include "ULocalization.h"
#include "UMapCollideLines.h"
#include "inifiles.hpp"
#include <map>

#pragma package(smart_init)

//---------------------------------------------------------------------------

#define SIZEOF_PTR 8
#define CS_MAGIC_NUMBER 23456

#define MATH_ROUNDUP(x, base) (((x) + ((base)-1)) & ~((base)-1))
#define MEM_ALIGNMENT SIZEOF_PTR

class fx32
{
 public:

	fx32()
	{
		data.fx = 0;
		data.fl = 0;
	}

	void operator = (const int& b)
	{
		_set<int>(b);
	}

	void operator = (const double& b)
	{
		_set<double>(b);
	}

	const size_t getSize() const
	{
		switch(mFx32size.getType())
		{
			case TFx32size::FX32T_DOUBLE:
				return sizeof(double);
			case TFx32size::FX32T_FIXED:
			case TFx32size::FX32T_NINTENDODS:
				return sizeof(__int32);
			default:
				assert(0);
		}
		return 0;
    }

	fx32 operator +(const fx32& b)
	{
		switch(mFx32size.getType())
		{
			case TFx32size::FX32T_DOUBLE:
				data.fl += b.data.fl;
				break;
			case TFx32size::FX32T_FIXED:
			case TFx32size::FX32T_NINTENDODS:
				data.fx += b.data.fx;
				break;
			default:
				assert(0);
		}
		return *this;
	}

  	union
	{
		__int32 fx;
		double fl;
	}data;

  private:

	template<typename T>
	void _set(const T& val)
	{
		switch(mFx32size.getType())
		{
			case TFx32size::FX32T_DOUBLE:
				data.fl = static_cast<double>(val);
				break;
			case TFx32size::FX32T_FIXED:
			case TFx32size::FX32T_NINTENDODS:
				data.fx = static_cast<__int32>(val);
				break;
			default:
				assert(0);
		}
	}
};
//---------------------------------------------------------------------------

#define GX_RGBA_R_SHIFT_DS           (0)
#define GX_RGBA_G_SHIFT_DS           (5)
#define GX_RGBA_B_SHIFT_DS           (10)
#define GX_RGBA_A_SHIFT_DS           (15)

#define GX_RGBA_B_SHIFT           (1)
#define GX_RGBA_G_SHIFT           (6)
#define GX_RGBA_R_SHIFT           (11)
#define GX_RGBA_A_SHIFT           (0)
#define GX_RGBA(r, g, b, a)       ((TColor)(((r) << ((mFx32size.isNintendo()) ? GX_RGBA_R_SHIFT_DS : GX_RGBA_R_SHIFT)) | \
										((g) << ((mFx32size.isNintendo()) ? GX_RGBA_G_SHIFT_DS : GX_RGBA_G_SHIFT)) | \
										((b) << ((mFx32size.isNintendo()) ? GX_RGBA_B_SHIFT_DS : GX_RGBA_B_SHIFT)) | \
										((a) << ((mFx32size.isNintendo()) ? GX_RGBA_A_SHIFT_DS : GX_RGBA_A_SHIFT))))
#define COLOR888TO1555(r,g,b) (GX_RGBA((r * 31) >> 8, (g * 31) >> 8, (b * 31) >> 8, 1))

//---------------------------------------------------------------------------

typedef void(*writeFxFunction)(const fx32* fxVal, FILE *f);

static FILE *f = NULL;
static writeFxFunction writeFx = NULL;

static void __fastcall saveAnimationFrames(__int32 frameCount, const TAnimation *anm,
											const GPerson *gp, TStringList *strmfilefist,
											const TCommonResObjManager *mainCommonRes);

static void __fastcall saveAnimationFrames(__int32 frameCount, const TAnimation *anm, const GPerson *gp,
											TStringList *strmfilefist, const TCommonResObjManager *mainCommonRes);

static void __fastcall createStreamDataForSave(TStringList *anim_files, TStringList *pprobj_list,
												unsigned short *obj_count, TObjManager *objManager);

static __int32 __fastcall FindAddAndSortResList(std::list<TSaveResItem> &resList, UnicodeString sname,
												ResourceTypes type, std::list<TSaveResItem> &srlist);

static void __fastcall DoThreadProcessMapSave();

static void __fastcall SaveTriggersForMap(FILE *f, TObjManager *objManager, const TCommonResObjManager* mainCommonRes,
											TList* opt_tiletypes_list,
											const std::list<PropertyItem>* lvars, bool save_global);

static void __fastcall SaveScriptsForMap(FILE *f, TObjManager *objManager, const TCommonTextManager* commonTextManager,
											const std::list<PropertyItem>* lvars, bool save_global);

//---------------------------------------------------------------------------

template<typename T>
const fx32 FX32(const T& val)
{
	//((fx32)((v) * ((double)(1 << ((mFx32size == 4096) ? 12 : 16)))))
	fx32 res;
	switch(mFx32size.getType())
	{
		case TFx32size::FX32T_DOUBLE:
			res.data.fl = static_cast<double>(val);
			break;
		case TFx32size::FX32T_FIXED:
			res.data.fx = static_cast<__int32>(val * (1 << 16));
			break;
		case TFx32size::FX32T_NINTENDODS:
			res.data.fx = static_cast<__int32>(val * (1 << 12));
			break;
		default:
			assert(0);
	}
	return res;
}
//---------------------------------------------------------------------------

static void writeFx_Double(const fx32* fxVal, FILE *f)
{
	fwrite(&fxVal->data.fl, sizeof(double), 1, f);
}
//---------------------------------------------------------------------------

static void writeFx_Int(const fx32* fxVal, FILE *f)
{
	fwrite(&fxVal->data.fx, sizeof(__int32), 1, f);
}
//---------------------------------------------------------------------------

static void inline WriteRoundup32Offset(FILE* f)
{
	fpos_t fpos;
	short w_val;
	char ch_val[256];
	fgetpos(f, &fpos);
	fpos++;
	w_val = ch_val[0] = (char)(MATH_ROUNDUP(fpos, MEM_ALIGNMENT) - fpos);
	fwrite(ch_val, 1, 1, f);
	//�������� ������ ������ ������������
	memset(ch_val, 0, w_val);
	fwrite(ch_val, 1, w_val, f);
}
//---------------------------------------------------------------------------

static void inline WriteRoundup32ReserveForPointers(FILE* f, const size_t data_size)
{
	//BYTE[] ������ ������ ��� ������ ������
	const size_t j = data_size * SIZEOF_PTR;
	unsigned char* achptr = new unsigned char[j];
	memset(achptr, 0, j);
	fwrite(achptr, 1, j, f);
	delete[] achptr;
}
//---------------------------------------------------------------------------

//��������� �����
void __fastcall TForm_m::MapSave(const UnicodeString *opath, bool commandline, UnicodeString *errmes)
{
#ifndef LITE_VER
	UnicodeString nfile;
	if (!proj_exist)
	{
		return;
	}

	switch(mFx32size.getType())
	{
		case TFx32size::FX32T_DOUBLE:
			writeFx = writeFx_Double;
			break;
		case TFx32size::FX32T_FIXED:
		case TFx32size::FX32T_NINTENDODS:
			writeFx = writeFx_Int;
			break;
		default:
			assert(0);
	}

	if(opath == NULL)
	{
		TSelectDirExtOpts opt;
		UnicodeString Directory;
		if(map_file_name.IsEmpty())
		{
			Directory = PathIni;
		}
		else
		{
			Directory = map_file_name;
		}
		opt << sdShowShares << sdNewUI << sdValidateDir;
		if (!SelectDirectory(ActMapSave->Caption, UnicodeString(_T("\0")), Directory, opt, this))
		{
			return;
		}
		map_file_name = Directory;
		nfile = map_file_name + _T("\\o");
	}
	else
	{
		map_file_name = *opath;
		nfile = *opath + _T("\\o");
	}

	if ((f = _wfopen(nfile.c_str(), _T("wb\0"))) == NULL)
	{
		const UnicodeString estr = Localization::Text(_T("mSaveError")) + _T(" ") + nfile;
		if(commandline)
		{
			if(errmes != NULL)
			{
				*errmes = *errmes + _T("ERROR: ") + estr + _T("\n");
            }
		}
		else
		{
			Application->MessageBox(estr.c_str(), Localization::Text(_T("mError")).c_str(), MB_OK | MB_ICONERROR);
        }
		return;
	}

	Application->CreateForm(__classid(TFWait), &FWait);
	FWait->Left = Form_m->Left + (Form_m->Width - FWait->Width) / 2;
	FWait->Top = Form_m->Top + (Form_m->Height - FWait->Height) / 2;
	FWait->Enabled = true;

		new TRenderThread(&DoThreadProcessMapSave); //will free on finish automatically

		if (FWait->Enabled)
		{
			FWait->ShowModal();
		}
		FWait->Free();
		FWait = NULL;

	//DoThreadProcessMapSave();

	if (f == NULL)
	{
		const UnicodeString estr = Localization::Text(_T("mSaveMapsError"));
		if(commandline)
		{
			if(errmes != NULL)
			{
				*errmes = *errmes + _T("ERROR: ") + estr + _T("\n");
			}
		}
		else
		{
			Application->MessageBox(estr.c_str(), Localization::Text(_T("mError")).c_str(), MB_OK | MB_ICONERROR);
		}
	}
	else
	{
		fclose(f);
		f = NULL;
	}

	if(commandline == false)
	{
		TIniFile *ini;
		ini = new TIniFile(PathIni + _T("editor.ini"));
		ini->WriteString(_T("Path"), _T("LastMapFilesPath"), ExtractRelativePath(PathIni, map_file_name));
		delete ini;
	}
#endif
}
//---------------------------------------------------------------------------

__int32 __fastcall FindAddAndSortResList(std::list<TSaveResItem> &resList, UnicodeString sname, ResourceTypes,
											std::list<TSaveResItem> &srlist)
{
	__int32 j = 0;
	std::list<TSaveResItem>::const_iterator it;
	for (it = resList.begin(); it != resList.end(); ++it)
	{
		//assert(it->mType == type);
		if(it->mName.Compare(sname) == 0)
		{
			{
				std::list<TSaveResItem>::iterator srlit;
				for(srlit = srlist.begin(); srlit != srlist.end(); ++srlit)
				{
					if(srlit->mIdx == j)
					{
						return j;
					}
				}
			}
			bool inserted = false;
			std::list<TSaveResItem>::reverse_iterator rsrlit;
			for(rsrlit = srlist.rbegin(); rsrlit != srlist.rend(); ++rsrlit)
			{
				__int32 cidx = rsrlit->mIdx;
				if(j > cidx)
				{
					/*if(j1 + 1 >= srlist->Count)
					{
						srlist->Add(reinterpret_cast<void*>(j));
					}
					else
					{
						srlist->Insert(j1 + 1, reinterpret_cast<void*>(j));
					}*/
					TSaveResItem sri = *it;
					sri.mIdx = j;
					srlist.insert(rsrlit.base(), sri);
					inserted = true;
					break;
				}
			}
			if(!inserted && rsrlit == srlist.rend())
			{
				/*if(srlist->Count == 0)
				{
					srlist->Add(reinterpret_cast<void*>(j));
				}
				else
				{
					srlist->Insert(0, reinterpret_cast<void*>(j));
				}*/
				TSaveResItem sri = *it;
				sri.mIdx = j;
				srlist.push_front(sri);
			}
			return j;
		}
		j++;
	}
	return -1;
}
//---------------------------------------------------------------------------

void __fastcall createStreamDataForSave(TStringList *anim_files, TStringList *pprobj_list,
											unsigned short *obj_count, TObjManager *objManager)
{
	__int32 j, k, i;
	TObjManager::TObjType otype;
	*obj_count = 0;
	anim_files->Clear();
	pprobj_list->Clear();
	pprobj_list->NameValueSeparator = L'=';
	for(k = 0; k < 3; k++)
	{
		switch(k)
		{
			case 0:
				otype = TObjManager::BACK_DECORATION;
				break;
			case 1:
				otype = TObjManager::CHARACTER_OBJECT;
				break;
			case 2:
				otype = TObjManager::FORE_DECORATION;
		}
		__int32 olen = objManager->GetObjTypeCount(otype);
		for(i = 0; i < olen; i++)
		{
			bool find = false;
			TMapGPerson *mgp = (TMapGPerson*)objManager->GetObjByIndex(otype, i);
			GPerson *gp = objManager->commonRes->GetParentObjByName(mgp->getGParentName());
			if(gp)
			{
				for(j = 0; j < objManager->commonRes->aniNameTypes->Count; j++)
				{
					TAnimation *anm = gp->GetAnimation(objManager->commonRes->aniNameTypes->Strings[j]);
					if(anm && anm->HasStreamObject())
					{
						find = true;
						UnicodeString str_key = gp->GetName() + _T("=\0") + anm->GetName();
						if(pprobj_list->IndexOf(str_key) < 0)
						{
							__int32 l;
							__int32 fct = anm->GetFrameCount();
                            pprobj_list->Add(str_key);
							for(l = 0; l < fct; l++)
							{
								__int32 a;
								std::list<TAnimationFrameData> lst;
								const TAnimationFrame *af = anm->GetFrame(l);
								af->CopyFrameDataTo(lst);
								std::list<TAnimationFrameData>::iterator it;
								for(it = lst.begin(); it != lst.end(); ++it)
								{
									str_key = it->strm_filename;
									if(!it->strm_filename.IsEmpty())
									{
										__int32 pos = str_key.LastDelimiter(_T(".\0"));
										if(pos > 0)
										{
											str_key = str_key.SubString(1, pos - 1);
										}
										while((pos = str_key.LastDelimiter(_T("\\"))) > 0)
										{
											str_key[pos] = L'/';
										}
										if(anim_files->IndexOf(str_key) < 0)
										{
											anim_files->Add(str_key);
										}
									}
								}
							}
						}

					}
				}
			}
			if(find)
			{
				(*obj_count)++;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall saveAnimationFrames(__int32 frameCount, const TAnimation *anm, const GPerson *gp,
										TStringList *strmfilefist, const TCommonResObjManager *mainCommonRes)
{
	__int32 i;

	if(frameCount > 0)
	{
		short w_val;

		//BYTE ������������ 32b
		WriteRoundup32Offset(f);
		//BYTE[] ������ ������ ��� ������ ���������� ��� ������ ������� (���������� ��������-��������� * sizeof(void*))
		WriteRoundup32ReserveForPointers(f, frameCount);

		//BYTE ������������ 32b
		WriteRoundup32Offset(f);
		//BYTE[] ������ ������ ��� ������ ���������� ��� ������ ���������� ������� (���������� ��������-��������� * sizeof(void*))
		WriteRoundup32ReserveForPointers(f, frameCount);
	}

	for (__int32 fr = 0; fr < frameCount; fr++)
	{
		fx32 dw_val;
		std::list<TAnimationFrameData> flst;
		const TAnimationFrame *aframe = anm->GetFrame(fr);
		aframe->CopyFrameDataTo(flst);

		//BYTE ������������ 32b
		WriteRoundup32Offset(f);

		//DWORD �������� �� ������� ����� collide rect offx ������ �������� ���� �������������� (-512..+512)
		dw_val = FX32(aframe->GetCollideRect().Left);
		writeFx(&dw_val, f);
		//DWORD �������� �� ������� ����� collide rect offy ������ �������� ���� ��������������
		dw_val = FX32(aframe->GetCollideRect().Top);
		writeFx(&dw_val, f);
		//DWORD �������� �� ������� ����� collide rect offx ������� ������� ���� �������������� (-512..+512)
		dw_val = FX32(aframe->GetCollideRect().Width() + aframe->GetCollideRect().Left);
		writeFx(&dw_val, f);
		//DWORD �������� �� ������� ����� collide rect offy ������� ������� ���� ��������������
		dw_val = FX32(aframe->GetCollideRect().Height() + aframe->GetCollideRect().Top);
		writeFx(&dw_val, f);
		//DWORD ���� �������� ����� ������������ ������-�����
		dw_val = 0;
		writeFx(&dw_val, f);
		//DWORD ������������� left �����
		dw_val = FX32(aframe->GetLeftOffset());
		writeFx(&dw_val, f);
		//DWORD ������������� top �����
		dw_val = FX32(aframe->GetTopOffset());
		writeFx(&dw_val, f);
		//DWORD width �����
		dw_val = FX32(aframe->GetWidth());
		writeFx(&dw_val, f);
		//DWORD height �����
		dw_val = FX32(aframe->GetHeight());
		writeFx(&dw_val, f);
		//DWORD time duration
		dw_val = FX32(aframe->GetDuration());
		writeFx(&dw_val, f);
		//DWORD event id
		dw_val = FX32(mainCommonRes->GetEventID(aframe->GetEventId()));
		writeFx(&dw_val, f);
		//DWORD	���������� �������� �� ������ ��������
		__int32 framePartsCount = flst.size();
		if(!gp->UseGraphics)
		{
			framePartsCount = 0;
		}
		dw_val = FX32(framePartsCount);
		writeFx(&dw_val, f);
		TPoint pt;
		for(__int32 n = 0; n < Form_m->mpEdPrObjWndParams->joinNodesCount; n++)
		{
			aframe->GetJoinNodeCoordinate(n, pt);
			//DWORD join node �������� �� ������� ����� offx (-512..+512)
			dw_val = FX32(pt.x);
			writeFx(&dw_val, f);
			//DWORD join node �������� �� ������� ����� offy
			dw_val = FX32(pt.y);
			writeFx(&dw_val, f);
		}
		if(framePartsCount > 0)
		{
			short w_val;

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ���������� �� ������ �� �������� (���������� �������� * sizeof(void*))
			WriteRoundup32ReserveForPointers(f, framePartsCount);

			std::list<TAnimationFrameData>::const_iterator afd;
			for (afd = flst.begin(); afd != flst.end(); ++afd)
			{
				//BYTE ������������ 32b
				WriteRoundup32Offset(f);

				if(strmfilefist && anm->HasStreamObject() && afd->isStreamFrame())
				{
					UnicodeString str_key = afd->strm_filename;
					__int32 pos = str_key.LastDelimiter(_T("."));
					if(pos > 0)
					{
						str_key = str_key.SubString(1, pos - 1);
					}
					while((pos = str_key.LastDelimiter(_T("\\"))) > 0)
					{
						str_key[pos] = L'/';
                    }
					w_val = static_cast<short>(strmfilefist->IndexOf(str_key));
				}
				else
				{
					w_val = static_cast<short>(mainCommonRes->GetIndexByHash(afd->frameId));
				}
				//WORD frame id
				fwrite(&w_val, sizeof(short), 1, f);
				//WORD offx
				short t = (short)afd->posX;
				fwrite(&t, sizeof(short), 1, f);
				//WORD offy
				t = (short)afd->posY;
				fwrite(&t, sizeof(short), 1, f);
				//WORD alpha
				w_val = (afd->a * 31) / 255;
				fwrite(&w_val, sizeof(short), 1, f);
				//WORD data
				unsigned short dataval = afd->getData();
				fwrite(&dataval, sizeof(unsigned short), 1, f);
				//WORD width
				dataval = (unsigned short)afd->rw;
				fwrite(&dataval, sizeof(unsigned short), 1, f);
				//WORD height
				dataval = (unsigned short)afd->rh;
				fwrite(&dataval, sizeof(unsigned short), 1, f);
			}
		}
	}
}
//---------------------------------------------------------------------------

//��������� ���� �����
void __fastcall TForm_m::CreateOptimizedBackObjListAndMap(TList *list, unsigned short *tmap, TObjManager *objManager, wchar_t **chdata, size_t *chdata_sz)
{
	__int32 i, j, map_len, tct;
	GMapBGObj *mo;
	map_len = objManager->mp_w * objManager->mp_h;
	list->Clear();
	{
		std::map<unsigned short, unsigned short>arrmap;
		std::map<unsigned short, unsigned short>::iterator it;
		size_t chdata_size = map_len * (5 + 1) + 1; //short = "65535," 5 + 1
		size_t pos = 0;
		if(chdata_sz != nullptr)
		{
			*chdata_sz = chdata_size;
		}
		if(chdata != nullptr)
		{
			*chdata = new wchar_t[chdata_size];
		}
		for (i = 0; i < map_len; i++)
		{
			unsigned short val;
			unsigned short idx = objManager->map[i];
			if (idx == NONE_P)
			{
				val = NONE_P;
			}
			else
			{
				it = arrmap.find(idx);
				if (it == arrmap.end())
				{
					list->Add(&mainCommonRes->BackObj[idx]);
					unsigned short pos_idx = (unsigned short)(list->Count - 1);
					arrmap[idx] = pos_idx;
					val = pos_idx;
				}
				else
				{
					val = it->second;
				}
			}
			if(tmap)
			{
				tmap[i] = val;
			}
			if(chdata)
			{
				__int32 step = 1;
				if(val >= 10000)
				{
					step = 5;
				}
				else
				if(val >= 1000)
				{
					step = 4;
				}
				else
				if(val >= 100)
				{
					step = 3;
				}
				else
				if(val >= 10)
				{
					step = 2;
				}
				_itow(val, (*chdata) + pos, 10);
				pos += step;
				if(i != map_len - 1)
				{
					(*chdata)[pos] =  L',';
				}
				pos++;
				assert(pos < chdata_size);
			}
		}
		if(chdata != nullptr)
		{
			(*chdata)[pos] = L'\0';
		}
	}

	tct = objManager->GetObjTypeCount(TObjManager::TRIGGER);
	const __int32 backObjSize = static_cast<__int32>(mainCommonRes->BackObj.size());
	for (i = 0; i < tct; i++)
	{
		TMapTrigger *tr = (TMapTrigger*)objManager->GetObjByIndex(TObjManager::TRIGGER, i);
		if (trMAP == tr->Type && tr->mapIndex >= 0 && backObjSize > tr->mapIndex)
		{
			bool k = false;
			for (j = 0; j < list->Count; j++)
			{
				if ((&mainCommonRes->BackObj[tr->mapIndex]) == list->Items[j])
				{
					k = true;
					break;
				}
			}
			if (k == false)
			{
				list->Add(&mainCommonRes->BackObj[tr->mapIndex]);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CreateOptimizedGameObjList(TList *list, TObjManager *objManager)
{
	__int32 i, j, ct, k, map_len;
	GPerson *mo;
	ct = mainCommonRes->GParentObj->Count;
	for (j = 0; j < ct; j++)
	{
		mo = (GPerson*)mainCommonRes->GParentObj->Items[j];
		if (!objManager->ignoreUnoptimizedResoures && !mo->ResOptimization)
		{
			list->Add(mo);
		}
		else
		{
			TObjManager::TObjType tlist[3] =
			{
				TObjManager::CHARACTER_OBJECT, TObjManager::BACK_DECORATION, TObjManager::FORE_DECORATION
			};
			bool found = false;
			for (k = 0; k < 3 && !found; k++)
			{
				for (i = 0; i < objManager->GetObjTypeCount(tlist[k]) && !found; i++)
				{
					TMapGPerson *p = static_cast<TMapGPerson*>(objManager->GetObjByIndex(tlist[k], i));
					if(p->GetObjType() == objMAPOBJPOOL)
					{
						TMapObjPool* op = static_cast<TMapObjPool*>(p);
						__int32 opitct = op->GetItemsCount();
						for(__int32 opit = 0; opit < opitct; opit++)
						{
							ObjPoolPropertyItem it;
							op->GetItemByIndex(opit, it);
							if(it.mName.Compare(mo->GetName()) == 0)
							{
								list->Add(mo);
								found = true;
								break;
							}
						}
					}
					else
					if (p->getGParentName().Compare(mo->GetName()) == 0)
					{
						if(p->GetObjType() == objMAPSOUNDSCHEME)
						{
							continue;
						}
						list->Add(mo);
						found = true;
						break;
					}
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::SaveObjForMapSave(const TMapGPerson *gp,
											const TObjManager *objManager,
											const std::list<PropertyItem>* lvars,
											const std::list<TSaveResItem> &font_res)
{
#ifndef LITE_VER
	fx32 fx_val;
	__int32 int_val, i, j, len, len2, m, check_bmp;
	double x, y, k;
	fpos_t fpos;
	short w_val;
	char ch_val[256];
	std::list<PropertyItem>::const_iterator iv;

	//BYTE ������������ 32b
	WriteRoundup32Offset(f);

	gp->getCoordinates(&x, &y);
	//(fx32)         X (FX) ��������� �� ����� (������������ align)
	fx_val = FX32(x);
	writeFx(&fx_val, f);
	//(fx32)         Y (FY)
	fx_val = FX32(y);
	writeFx(&fx_val, f);
	fx_val = 0;
	//(fx32)         (FLEFT)
	writeFx(&fx_val, f);
	//(fx32)         (FTOP)
	writeFx(&fx_val, f);
	//(fx32)         (FCURRWIDTH)
	writeFx(&fx_val, f);
	//(fx32)         (FCURRHEIGHT)
	writeFx(&fx_val, f);

	//BYTE ������������ 32b
	WriteRoundup32Offset(f);

	//DWORD         (IPARENT_IDX) ������ ��������-������� (������)
	int_val = objManager->GetParentObjID(gp->getGParentName());
	fwrite(&int_val, sizeof(__int32), 1, f);
	//DWORD         (IZONE) ����
	int_val = gp->GetZone();
	fwrite(&int_val, sizeof(__int32), 1, f);
	//DWORD         (ISTATE) id ��������������� ���������
	int_val = objManager->GetStateID(gp->State_Name);
	fwrite(&int_val, sizeof(__int32), 1, f);
	int_val = 0;
	//DWORD         (ITEMPSTATE)
	fwrite(&int_val, sizeof(__int32), 1, f);
	//DWORD         (IANIFRIDX)
	fwrite(&int_val, sizeof(__int32), 1, f);
	//DWORD         (IANIENDFLAG)
	fwrite(&int_val, sizeof(__int32), 1, f);
	//DWORD         (ITIMER) ������
	fwrite(&int_val, sizeof(__int32), 1, f);
	//WORD id �������, � �������� ����������� (-1 -- �� �����������)
	w_val = (short)objManager->GetGameObjID(gp->ParentObj);
	fwrite(&w_val, sizeof(short), 1, f);
	//WORD id ����� ����
	w_val = (short)objManager->GetDirectorID(gp->NextLinkedDirector);
	// join nodes
	fwrite(&w_val, sizeof(short), 1, f);
	for(__int32 n = 0; n < Form_m->mpEdPrObjWndParams->joinNodesCount; n++)
	{
		//WORD id �������, ������� ����������� � join node (-1 -- �� �����������)
		w_val = (short)objManager->GetGameObjID(gp->GetChild(n));
		fwrite(&w_val, sizeof(short), 1, f);
	}

	//BYTE ������������ 32b
	WriteRoundup32Offset(f);

	//WORD         id ������� sptONCOLLIDE
	w_val = (short)objManager->GetScriptID(gp->getScriptName((UnicodeString)::Actions::Names[::Actions::sptONCOLLIDE]));
	fwrite(&w_val, sizeof(short), 1, f);
	//WORD         id ������� sptONTILECOLLIDE
	w_val = (short)objManager->GetScriptID(gp->getScriptName((UnicodeString)::Actions::Names[::Actions::sptONTILECOLLIDE]));
	fwrite(&w_val, sizeof(short), 1, f);
	//WORD         id ������� sptONCHANGENODE
	w_val = (short)objManager->GetScriptID(gp->getScriptName((UnicodeString)::Actions::Names[::Actions::sptONCHANGENODE]));
	fwrite(&w_val, sizeof(short), 1, f);
	//WORD         id ������� sptONCHANGE1
	w_val = (short)objManager->GetScriptID(gp->getScriptName((UnicodeString)::Actions::Names[::Actions::sptONCHANGE1]));
	fwrite(&w_val, sizeof(short), 1, f);
	//WORD         id ������� sptONCHANGE2
	w_val = (short)objManager->GetScriptID(gp->getScriptName((UnicodeString)::Actions::Names[::Actions::sptONCHANGE2]));
	fwrite(&w_val, sizeof(short), 1, f);
	//WORD         id ������� sptONCHANGE3
	w_val = (short)objManager->GetScriptID(gp->getScriptName((UnicodeString)::Actions::Names[::Actions::sptONCHANGE3]));
	fwrite(&w_val, sizeof(short), 1, f);
	//WORD         id ������� sptONENDANIM
	w_val = (short)objManager->GetScriptID(gp->getScriptName((UnicodeString)::Actions::Names[::Actions::sptONENDANIM]));
	fwrite(&w_val, sizeof(short), 1, f);

	//WORD         ���������� ����, � ������� �������� ���������� � ��������� �������� �������
	len2 = lvars->size();
	w_val = (short)(len2 * fx_val.getSize());
	fwrite(&w_val, sizeof(short), 1, f);
	//BYTE[]    �������� �������, ������������������ ����
	//������������������ ����=(���������� �������� �� �����*��������� ����� ������� ������� �����)
	//������� � ����� �����, ��� ����, ���� ����� ������ �� ������ �����������, ���-�� ���� ���-�� ���������
	//������������������ ��������� � ������������������� �������� �� �����

	//BYTE ������������ 32b
	WriteRoundup32Offset(f);

	for (iv = lvars->begin(); iv != lvars->end(); ++iv)
	{
		gp->GetProperties().getVarValue(&k, iv->name);
		fx_val = FX32(k);
		writeFx(&fx_val, f);
	}

	// texts
	if(gp->GetObjType() == objMAPTEXTBOX)
	{
		__int32 sc, cc;
		const TMapTextBox *txb = static_cast<const TMapTextBox*>(gp);
		txb->GetDynamicTextSizes(sc, cc);

		//BYTE ������������ 32b
		WriteRoundup32Offset(f);

		// WORD			StringsCount (�� ��������� ����� �������� ��� ������������ ��������)
		if(txb->GetTextType() == TMapTextBox::MTBT_Dynamic)
		{
			w_val = static_cast<short>(sc);
		}
		else
		{
			w_val = -1;
		}
		fwrite(&w_val, sizeof(short), 1, f);
		// WORD			MaxStringCharCount (��� ������������ ��������)
		if(txb->GetTextType() == TMapTextBox::MTBT_Dynamic)
		{
			w_val = static_cast<short>(cc);
		}
		else
		{
			w_val = -1;
		}
		fwrite(&w_val, sizeof(short), 1, f);
		// WORD			CanvasWidth
		w_val = static_cast<short>(txb->GetTextWidth());
		fwrite(&w_val, sizeof(short), 1, f);
		// WORD			CanvasHeight
		w_val = static_cast<short>(txb->GetTextHeight());
		fwrite(&w_val, sizeof(short), 1, f);
		// WORD			Margin
		w_val = static_cast<short>(txb->GetMargin());
		fwrite(&w_val, sizeof(short), 1, f);
		// WORD			CanvasColor
		if(clTRANSPARENT == txb->GetFillColor())
		{
			w_val = 0;
		}
		else
		{
			unsigned char txtcr = static_cast<unsigned char>(txb->GetFillColor());
			unsigned char txtcg = static_cast<unsigned char>(txb->GetFillColor() >> 8);
			unsigned char txtcb = static_cast<unsigned char>(txb->GetFillColor() >> 16);
			w_val = static_cast<short>(COLOR888TO1555(txtcr, txtcg, txtcb));
		}
		fwrite(&w_val, sizeof(short), 1, f);
		// WORD ������ font-������� (�������-��������� � ������ �����)
		w_val = -1;
		if(!txb->GetFontName().IsEmpty())
		{
			j = 0;
			std::list<TSaveResItem>::const_iterator sresit;
			for (sresit = font_res.begin(); sresit != font_res.end(); ++sresit)
			{
				if(sresit->mName.Compare(forceCorrectFontFileExtention(txb->GetFontName())) == 0)
				{
					w_val = static_cast<short>(j);
					break;
				}
				j++;
			}
		}
		fwrite(&w_val, sizeof(short), 1, f);
		// DWORD		Alignment
		int_val = static_cast<__int32>(txb->GetAlignment());
		fwrite(&int_val, sizeof(__int32), 1, f);

		//BYTE ������������ 32b
		WriteRoundup32Offset(f);

		if(txb->GetTextType() == TMapTextBox::MTBT_Dynamic)
		{
			if(sc > 0)
			{
				// BYTE[]		������ ������ ��� ������ ���������� �� ��������� ������ (StringsCount * sizeof(void*))
				WriteRoundup32ReserveForPointers(f, sc);

				for(__int32 sci = 0; sci < sc; sci++)
				{
					//BYTE ������������ 32b
					WriteRoundup32Offset(f);

					// BYTE[]	������ ������ ��� ������ ��������� ������ (MaxStringCharCount *  sizeof(wchar_t) + 1 + 1) 2 ������ ����� ��������������� ������� ��� ������ ������ + 1 ��� 0-�����������
					const unsigned __int32 wcharsize = mWcharsizeExternal == 0 ? mWcharsize : mWcharsizeExternal;
					j = (cc + 1 + 1) * wcharsize;
					unsigned char* achptr = new unsigned char[j];
					memset(achptr, 0, j);
					fwrite(achptr, 1, j, f);
					delete[] achptr;
				}
			}
		}
		else
		{
			//	DWORD		������ ���������� �������
			int_val = mpTexts->GetTextID(txb->GetTextId());
			fwrite(&int_val, sizeof(__int32), 1, f);
		}
	 }
#endif
}
//---------------------------------------------------------------------------

//�� �����������, ����� �� �������� ������, ��� ���������� ������� ������ ����
//�������� �� ��������� ���������. �� ����� ���� ������, ����� � ������� �����
//������������� ��� ����������
void __fastcall SaveScriptsForMap(FILE *f, TObjManager *objManager, const TCommonTextManager* commonTextManager,
									const std::list<PropertyItem>* lvars, bool save_global)
{
	unsigned short w_val = 0;
	fwrite(&w_val, sizeof(unsigned short), 1, f);

/*
#ifndef LITE_VER
	__int32 i, j, len, len2, k, scriptsCount;
	WORD w_val;
	__int32 dw_val;
	char ch_val[128];
	fpos_t fpos;
	std::list<PropertyItem>::const_iterator iv;

	len = 0;
	scriptsCount = objManager->GetScriptsCount();
	for (i = 0; i < scriptsCount; i++)
	{
		TGScript *spt = objManager->GetScriptByIndex(i);
		if ((save_global && LEVELS_INDEPENDENT_IDX == spt->Zone) || (!save_global && LEVELS_INDEPENDENT_IDX != spt->Zone))
		{
			len++;
		}
	}

	//____________�������:
	//!!! ������ ������� ������ ��� ���������� ����� � ����� 0�-0, 1�-1, 2�-2...
	//WORD          ���������� ��������
	w_val = (unsigned short)len;
	fwrite(&w_val, sizeof(unsigned short), 1, f);
	for (i = 0; i < scriptsCount; i++)
	{
		UnicodeString str;
		TGScript *spt = objManager->GetScriptByIndex(i);
		if ((save_global && LEVELS_INDEPENDENT_IDX != spt->Zone) || (!save_global && LEVELS_INDEPENDENT_IDX == spt->Zone))
		{
			continue;
		}

		// //sptONCOLLIDE
		//BYTE         ���������� ������ GroupNameList
		ch_val[0] = (char)spt->CldGroupNameList->Count;
		fwrite(ch_val, 1, 1, f);
		len2 = (unsigned char)ch_val[0];

		//BYTE ������������ 32b
		WriteRoundup32Offset(f);

		for (j = 0; j < len2; j++)
		{
			//BYTE        id ������
			ch_val[0] = (char)objManager->GetParentObjID(spt->CldGroupNameList->Strings[j]);
			fwrite(ch_val, 1, 1, f);
		}

		//BYTE         ���������� ������� ObjNameList
		ch_val[0] = (char)spt->CldObjNameList->Count;
		fwrite(ch_val, 1, 1, f);
		len2 = (unsigned char)ch_val[0];

		//BYTE ������������ 32b
		WriteRoundup32Offset(f);

		for (j = 0; j < len2; j++)
		{
			//WORD        id �������
			w_val = (unsigned short)objManager->GetGameObjID(spt->CldObjNameList->Strings[j]);
			fwrite(&w_val, sizeof(unsigned short), 1, f);
		}

		//BYTE ������������ 32b
		WriteRoundup32Offset(f);

		//WORD         ����������� ��� ������������ ������
		w_val = (unsigned short)spt->Multiple;
		fwrite(&w_val, sizeof(unsigned short), 1, f);
		//WORD         id �������, �� �������� ����������� ����������
		w_val = (unsigned short)objManager->GetGameObjID(spt->ChangeCtrlTo);
		fwrite(&w_val, sizeof(unsigned short), 1, f);
		//WORD         ������� �� ����
		w_val = (unsigned short)spt->GoToZone;
		fwrite(&w_val, sizeof(unsigned short), 1, f);
		//WORD         ��������� �������
		w_val = (unsigned short)spt->EndLevel;
		fwrite(&w_val, sizeof(unsigned short), 1, f);
		//WORD         id ���������
		w_val = (unsigned short)commonTextManager->GetTextID(spt->Message);
		fwrite(&w_val, sizeof(unsigned short), 1, f);

		//WORD           sptONCOLLIDE,sptONTILECOLLIDE,sptONCHANGENODE: id �������� ����������; sptONCHANGE: id ���������
		w_val = NONE_P;
		switch(spt->Mode)
		{
		case ::Actions::
			sptONCOLLIDE :
		case ::Actions::
			sptONTILECOLLIDE :
		case ::Actions::
			sptONCHANGENODE : str = spt->CldPropNameInitiator;
			break;
		case ::Actions::
			sptONCHANGE1 : str = spt->ChgPropName;
		}
		j = 0;
		for (iv = lvars->begin(); iv != lvars->end(); ++iv)
		{
			if (iv->name.Compare(str) == 0)
			{
				w_val = (unsigned short)j;
				break;
			}
			j++;
		}
		fwrite(&w_val, sizeof(unsigned short), 1, f);

		//WORD           sptONCOLLIDE,sptONTILECOLLIDE,sptONCHANGENODE: �������� �������� ����������; sptONCHANGE: �������� ONCHANGE
		w_val = NONE_P;
		switch(spt->Mode)
		{
		case ::Actions::
			sptONCOLLIDE :
		case ::Actions::
			sptONTILECOLLIDE :
		case ::Actions::
			sptONCHANGENODE : w_val = (unsigned short)spt->CldParamValueInitiator;
			break;
		case ::Actions::
			sptONCHANGE1 : w_val = (unsigned short)spt->ChgValue;
		}
		fwrite(&w_val, sizeof(unsigned short), 1, f);

		//WORD           sptONCOLLIDE,sptONTILECOLLIDE,sptONCHANGENODE: �������������� �������� ������� ��������; sptONCHANGE: �������������� ��������
		w_val = NONE_P;
		switch(spt->Mode)
		{
		case ::Actions::
			sptONCOLLIDE :
		case ::Actions::
			sptONTILECOLLIDE :
		case ::Actions::
			sptONCHANGENODE : w_val = (unsigned short)spt->CldMathOperPropInitiator;
			break;
		case ::Actions::
			sptONCHANGE1 : w_val = (unsigned short)spt->ChgMathOper;
		}
		fwrite(&w_val, sizeof(unsigned short), 1, f);

		//WORD           sptONCOLLIDE,sptONTILECOLLIDE,sptONCHANGENODE: id ��������� ����������
		w_val = NONE_P;
		switch(spt->Mode)
		{
		case ::Actions::
			sptONCOLLIDE :
		case ::Actions::
			sptONTILECOLLIDE :
		case ::Actions::
			sptONCHANGENODE : w_val = (unsigned short)objManager->GetStateID(spt->CldStateNameInitiator);
		}
		fwrite(&w_val, sizeof(unsigned short), 1, f);

		//BYTE           sptONCOLLIDE,sptONTILECOLLIDE,sptONCHANGENODE: �������������� �������� ������� ���������
		w_val = NONE_P;
		switch(spt->Mode)
		{
		case ::Actions::sptONCOLLIDE :
		case ::Actions::sptONTILECOLLIDE :
		case ::Actions::sptONCHANGENODE :
			w_val = (unsigned short)spt->CldMathOperStateInitiator;
		}
		fwrite(&w_val, sizeof(unsigned short), 1, f);

		//WORD           sptONCOLLIDE: id �������� ������� ���������; sptONCHANGENODE: ��� ������� �����
		w_val = NONE_P;
		switch(spt->Mode)
		{
		case ::Actions::sptONCOLLIDE :
			j = 0;
			for (iv = lvars->begin(); iv != lvars->end(); ++iv)
			{
				if (iv->name.Compare(spt->CldPropNameSecond) == 0)
				{
					w_val = (unsigned short)j;
					break;
				}
				j++;
			}
			break;
		case ::Actions::sptONCHANGENODE :
			w_val = (unsigned short)objManager->GetDirectorID(spt->CldPropNameSecond);
		}
		fwrite(&w_val, sizeof(unsigned short), 1, f);

		//WORD           sptONCOLLIDE: �������� �������� ������� ���������; sptONTILECOLLIDE: id ����� (1..62)
		w_val = NONE_P;
		switch(spt->Mode)
		{
		case ::Actions::sptONCOLLIDE :
		case ::Actions::sptONTILECOLLIDE :
			w_val = (unsigned short)spt->CldParamValueSecond;
		}
		fwrite(&w_val, sizeof(unsigned short), 1, f);

		//WORD           sptONCOLLIDE: �������������� �������� ������� �������� ������� ���������
		w_val = NONE_P;
		switch(spt->Mode)
		{
		case ::Actions::sptONCOLLIDE :
			w_val = (unsigned short)spt->CldMathOperPropSecond;
		}
		fwrite(&w_val, sizeof(unsigned short), 1, f);

		//WORD           sptONCOLLIDE: id ��������� ������� ���������
		w_val = NONE_P;
		switch(spt->Mode)
		{
		case ::Actions::
			sptONCOLLIDE : w_val = (unsigned short)objManager->GetStateID(spt->CldStateNameSecond);
		}
		fwrite(&w_val, sizeof(unsigned short), 1, f);

		//WORD           sptONCOLLIDE: �������������� �������� ������� ��������� ������� ���������
		w_val = NONE_P;
		switch(spt->Mode)
		{
		case ::Actions::
			sptONCOLLIDE : w_val = (unsigned short)spt->CldMathOperStateSecond;
		}
		fwrite(&w_val, sizeof(unsigned short), 1, f);

		//WORD ��� ������� (SPTTYPE_COLLIDE, SPTTYPE_DIRECTOR, SPTTYPE_PROPERTY, SPTTYPE_NONE)
		switch(spt->Mode)
		{
			case ::Actions::sptONCOLLIDE:
				w_val = (unsigned short)::Actions::SPTTYPE_COLLIDE;
			break;
			case ::Actions::sptONCHANGENODE:
				w_val = (unsigned short)::Actions::SPTTYPE_DIRECTOR;
			break;
			case ::Actions::sptONCHANGE1:
			case ::Actions::sptONCHANGE2:
			case ::Actions::sptONCHANGE3:
				w_val = (unsigned short)::Actions::SPTTYPE_PROPERTY;
			break;
			default:
				w_val = (unsigned short)::Actions::SPTTYPE_NONE;
		}
		fwrite(&w_val, sizeof(unsigned short), 1, f);

  //BYTE         ���������� ������� ��������� TrigNameList
		ch_val[0] = (char)spt->TrigNameList->Count;
		len2 = (unsigned char)ch_val[0];
		fwrite(ch_val, 1, 1, f);

		//BYTE ������������ 32b
		WriteRoundup32Offset(f);

		for (j = 0; j < len2; j++)
		{
			//WORD        id ��������
			w_val = (unsigned short)objManager->GetTriggerID(spt->TrigNameList->Strings[j]);
			fwrite(&w_val, sizeof(unsigned short), 1, f);
		}
	}
#else
	#pragma argsused
#endif
*/
}
//---------------------------------------------------------------------------

//�� �����������, ����� �� �������� ������, ��� ���������� ������� ������ ����
//�������� �� ��������� ���������. �� ����� ���� ������, ����� � ������� �����
//������������� ��� ����������
void __fastcall SaveTriggersForMap(FILE *f, TObjManager *objManager, const TCommonResObjManager* mainCommonRes,
									TList *opt_tiletypes_list, const std::list<PropertyItem>* lvars, bool save_global)
{
#ifndef LITE_VER
	double x, y, k;
	__int32 i, len, len2, triggersCount;
	WORD w_val;
	short s_val;
	__int32 dw_val;
	char ch_val[128];
	fpos_t fpos;

	len = 0;
	triggersCount = objManager->GetObjTypeCount(TObjManager::TRIGGER);
	for (i = 0; i < triggersCount; i++)
	{
		TMapTrigger *tr = (TMapTrigger*)objManager->GetObjByIndex(TObjManager::TRIGGER, i);
		if ((save_global && LEVELS_INDEPENDENT_IDX == tr->GetZone()) || (!save_global && LEVELS_INDEPENDENT_IDX != tr->GetZone()))
		{
			len++;
		}
	}

	//BYTE         ���������� ���������
	w_val = (unsigned short)len;
	fwrite(&w_val, sizeof(unsigned short), 1, f);

	for (i = 0; i < triggersCount; i++)
	{
		TMapTrigger *tr = (TMapTrigger*)objManager->GetObjByIndex(TObjManager::TRIGGER, i);

		if ((save_global && LEVELS_INDEPENDENT_IDX != tr->GetZone()) || (!save_global && LEVELS_INDEPENDENT_IDX == tr->GetZone()))
		{
			continue;
		}

		//BYTE ������������ 32b
		WriteRoundup32Offset(f);

		if (trMAP == tr->Type) //���� ��� �������� trMAP, ����������� ������ ���� ���� ������
		{
            fx32 fx_val;

			if (save_global)
			{
				assert(0);
			}

			tr->getCoordinates(&x, &y);
			//(fx32)         X       ��������� �� ����� (������ ������� �����)
			fx_val = FX32(x);
			writeFx(&fx_val, f);
			//(fx32)         Y
			fx_val = FX32(y);
			writeFx(&fx_val, f);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//DWORD         ITRIG_TYPE, ��� �������� (trMAP, trPROPERTIES)
			dw_val = (__int32)tr->Type;
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD		ITRIG_TARGET, �� ������������
			dw_val = 0;
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD          ITRIG_MDIR_PTILE, ����������� ��������� �����
			dw_val = (__int32)tr->mapDirection;
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD		ITRIG_MIDX_POPER, ������� ������ ������
			dw_val = (__int32)tr->mapCount;
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD      ITRIG_MCOUNT_POBJ,  �� ����� ������ ������
			{
				dw_val = NONE_P;
				const __int32 backObjSize = static_cast<__int32>(mainCommonRes->BackObj.size());
				if (tr->mapIndex >= 0 && backObjSize > tr->mapIndex)
				{
					for (__int32 elem = 0; elem < opt_tiletypes_list->Count; elem++)
					{
						const GMapBGObj* bgobj = mainCommonRes->GetMapBGObj(tr->mapIndex);
						if (bgobj == opt_tiletypes_list->Items[elem])
						{
							dw_val = (__int32)elem;
							break;
						}
					}
				}
				fwrite(&w_val, sizeof(__int32), 1, f);
			}
		}
		else //�����, ���� ��� �������� trPROPERTIES, ����������� ������ ���� ���� ������
		{
			fx32 fx_val;
			//(fx32)		�� ������������
			writeFx(&fx_val, f);
			//(fx32)		�� ������������
			writeFx(&fx_val, f);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//DWORD         ITRIG_TYPE, ��� �������� (trMAP, trPROPERTIES)
			dw_val = (__int32)tr->Type;
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD         ITRIG_TARGET, id �������, �������� ���������� ��������
			dw_val = (__int32)objManager->GetGameObjID(tr->objName);
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD          ITRIG_MDIR_PTILE, ���� > 0, ����� ����� ���� �����������������
			dw_val = (__int32)tr->tileTeleport;
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD           ITRIG_MIDX_POPER, ��� �������� ������ ������� (add, sub, mov)
			dw_val = (__int32)tr->mapIndex;
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD           ITRIG_MCOUNT_POBJ, id ������� �� ���������� �������� �����������������
			dw_val = (__int32)objManager->GetDirectorID(tr->objTeleport);
			fwrite(&dw_val, sizeof(__int32), 1, f);
		}

		if (trMAP == tr->Type)
		{
			//BYTE         ���������� ������� � ������
			ch_val[0] = 0;
			fwrite(ch_val, 1, 1, f);
		}
		else
		{
			//BYTE         ���������� ������� � ������
			ch_val[0] = (char)tr->stateList->Count;
			fwrite(ch_val, 1, 1, f);
			if (ch_val[0] > 0)
			{
				//BYTE ������������ 32b
				WriteRoundup32Offset(f);

				for (__int32 j = 0; j < tr->stateList->Count; j++)
				{
					//BYTE        id ���������
					ch_val[0] = (char)objManager->GetStateID(tr->stateList->Strings[j]);
					fwrite(ch_val, 1, 1, f);
				}
			}
		}

		//BYTE		���������� ����, � ������� �������� ���������� �� id �������� ������� ��������� (��������������)
		len2 = 0;
		s_val = (short)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONCOLLIDE]));
		if (s_val >= 0)
			len2 += 2;
		s_val = (char)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONTILECOLLIDE]));
		if (s_val >= 0)
			len2 += 2;
		s_val = (char)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONCHANGENODE]));
		if (s_val >= 0)
			len2 += 2;
		s_val = (char)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONCHANGE1]));
		if (s_val >= 0)
			len2 += 2;
		s_val = (char)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONCHANGE2]));
		if (s_val >= 0)
			len2 += 2;
		s_val = (char)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONCHANGE3]));
		if (s_val >= 0)
			len2 += 2;
		s_val = (char)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONENDANIM]));
		if (s_val >= 0)
			len2 += 2;
		if (trMAP == tr->Type)
		{
			len2 = 0;
			fwrite(&len2, 1, 1, f);
		}
		else
		{
			fwrite(&len2, 1, 1, f);
		}

		//���� ���� ��� ����������, ������������ ���� ����� id
		if (len2 > 0)
		{
			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

		//WORD         id ������� ������� ��������� sptONCOLLIDE  (-1 -- �� ���������)
			w_val = (unsigned short)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONCOLLIDE]));
			fwrite(&w_val, sizeof(unsigned short), 1, f);
        //WORD         id ������� ������� ��������� sptONTILECOLLIDE (-1 -- �� ���������)
			w_val = (unsigned short)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONTILECOLLIDE]));
			fwrite(&w_val, sizeof(unsigned short), 1, f);
        //WORD         id ������� ������� ��������� sptsptONCHANGENODE (-1 -- �� ���������)
			w_val = (unsigned short)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONCHANGENODE]));
			fwrite(&w_val, sizeof(unsigned short), 1, f);
		//WORD         id ������� ������� ��������� sptONCHANGE1  (-1 -- �� ���������)
			w_val = (unsigned short)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONCHANGE1]));
			fwrite(&w_val, sizeof(unsigned short), 1, f);
        //WORD         id ������� ������� ��������� sptONCHANGE2  (-1 -- �� ���������)
			w_val = (unsigned short)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONCHANGE2]));
			fwrite(&w_val, sizeof(unsigned short), 1, f);
        //WORD         id ������� ������� ��������� sptONCHANGE3  (-1 -- �� ���������)
			w_val = (unsigned short)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONCHANGE3]));
			fwrite(&w_val, sizeof(unsigned short), 1, f);
        //WORD         id ������� ������� ��������� sptONENDANIM (-1 -- �� ���������)
			w_val = (unsigned short)objManager->GetScriptID(tr->getScriptName(::Actions::Names[::Actions::sptONENDANIM]));
			fwrite(&w_val, sizeof(unsigned short), 1, f);
		}

    //WORD         ���������� ����, � ������� �������� ���������� �� ��������� ������� ������� (��������������)
		if (trMAP == tr->Type)
		{
			w_val = 0;
			fwrite(&w_val, sizeof(short), 1, f);
		}
		else
		{
			fx32 fx_idx;
			w_val = 0;
			std::list<PropertyItem>::const_iterator iv;
			for (iv = lvars->begin(); iv != lvars->end(); ++iv)
			{
				bool checked;
				tr->GetProperties().getChecked(&checked, iv->name);
				if (checked)
				{
					w_val += (short)fx_idx.getSize();
                }
			}
			fwrite(&w_val, sizeof(short), 1, f);

			if (w_val > 0)
			{
				//BYTE ������������ 32b
				WriteRoundup32Offset(f);

            	//BYTE[]       ������������������ ����

				for (iv = lvars->begin(); iv != lvars->end(); ++iv)
				{
					bool checked;
					fx32 fx32val;
					tr->GetProperties().getChecked(&checked, iv->name);
					if (checked)
					{
						tr->GetProperties().getVarValue(&k, iv->name);
						fx32val = FX32(k);
						writeFx(&fx_idx, f);
						writeFx(&fx32val, f);
					}
					fx_idx = fx_idx + FX32(1);
				}
			}
		}
	}
#else
	#pragma argsused
#endif
}
//---------------------------------------------------------------------------

//��������� �����
void __fastcall TForm_m::DoThreadProcessMapSave()
{
#ifndef LITE_VER
	__int32 i, j, len, len2, m, check_bmp;
	__int32 dw_val;
	double x, y, k;
	fpos_t fpos;
	short w_val;
	char ch_val[256];
	UnicodeString nfile;
	TList *list, *opt_tiletypes_list;
	unsigned short *tmap;
	const unsigned __int32 wcharsize = mWcharsizeExternal == 0 ? mWcharsize : mWcharsizeExternal;

	//----------------------
	//���� � ������ �������:
	//----------------------

	const std::list<PropertyItem>* lvars = mainCommonRes->MainGameObjPattern->GetVars();

	//WORD			������ wchart_t
	w_val = (short)wcharsize;
	fwrite(&w_val, sizeof(short), 1, f);
	//WORD			��� fx32
	w_val = (short)mFx32size.getType();
	fwrite(&w_val, sizeof(short), 1, f);
	//WORD			������ sizeof fx32
	{
		const fx32 fx32_val;
		w_val = (short)fx32_val.getSize();
		fwrite(&w_val, sizeof(short), 1, f);
	}
	//WORD			����� ����� ������� (tick delay)
	w_val = (short)tickDuration;
	fwrite(&w_val, sizeof(short), 1, f);
	//WORD	������ PRP_ENABLE ��������
	w_val = 0;
	i = 0;
	std::list<PropertyItem>::const_iterator iv;
	for (iv = lvars->begin(); iv != lvars->end(); ++iv)
	{
		if(iv->name.Compare(UnicodeString(PredefProperties::PropertyNames[PredefProperties::PRP_ENABLE])) == 0)
		{
			fwrite(&w_val, sizeof(short), 1, f);
			i = 1;
			break;
		}
		w_val++;
	}
	if(i == 0)
	{
		w_val = -1;
		fwrite(&w_val, sizeof(short), 1, f);
	}
	//WORD	������ PRP_SPEED ��������
	w_val = 0;
	i = 0;
	for (iv = lvars->begin(); iv != lvars->end(); ++iv)
	{
		if(iv->name.Compare(UnicodeString(PredefProperties::PropertyNames[PredefProperties::PRP_SPEED])) == 0)
		{
			fwrite(&w_val, sizeof(short), 1, f);
			i = 1;
			break;
		}
		w_val++;
	}
	if(i == 0)
	{
		w_val = -1;
		fwrite(&w_val, sizeof(short), 1, f);
	}
	//WORD          ������ ��������� (W)
	w_val = p_w;
	fwrite(&w_val, sizeof(short), 1, f);
	//WORD          ������ ��������� (H)
	w_val = p_h;
	fwrite(&w_val, sizeof(short), 1, f);
	//BYTE          ����
	ch_val[0] = (char)lrCount;
	fwrite(ch_val, 1, 1, f);

	//�������� �������� ��������-���������� �� ����������� (w*h) � List
	//createImgListForSave(res_names, lstAllMaps, NULL);
	std::list<TResourceItem> *all_res_lst = createImgListForSave(lstAllMaps, NULL);
	std::list<TSaveResItem> graphics_res;
	std::list<TSaveResItem> font_res;
	std::list<TSaveResItem> sound_res;
	std::list<TResourceItem>::iterator it;
	std::list<TSaveResItem>::iterator sresit;
	{
		__int32 graphics_res_count = 0;
		__int32 font_res_count = 0;
		__int32 sound_res_count = 0;
		for (it = all_res_lst->begin(); it != all_res_lst->end(); ++it)
		{
			switch(it->mType)
			{
				case ResourceTypes::RESOURCE_TYPE_IMAGE:
					graphics_res_count++;
				break;
				case ResourceTypes::RESOURCE_TYPE_FONTDATA:
					font_res_count++;
				break;
				case ResourceTypes::RESOURCE_TYPE_SOUNDDATA:
					sound_res_count++;
			}
		}

		//WORD			���������� ����������� ��������
		fwrite(&graphics_res_count, sizeof(short), 1, f);
		if(graphics_res_count > 0) // ���� ���� ����������� �������
		{
			j = graphics_res_count * SIZEOF_PTR;
			unsigned char* achptr = new unsigned char[j];
			memset(achptr, 0, j);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//BYTE[] ������ ������ ��� ������ ���������� �� ������
			fwrite(achptr, 1, j, f);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//BYTE[] ������ ������ ��� ������ ���������� �� ����������� ������� (BMPImage, res_img)
			fwrite(achptr, 1, j, f);
			delete[] achptr;

			for (it = all_res_lst->begin(); it != all_res_lst->end(); ++it)
			{
				if(it->mType == ResourceTypes::RESOURCE_TYPE_IMAGE)
				{
					TSaveResItem ri;
					ri.mType = it->mType;
					ri.mName = it->mName;
					ri.mIdx = -1;
					graphics_res.push_back(ri);
					UnicodeString sname = it->mName;
					__int32 pos = sname.LastDelimiter(_T("."));
					if (pos > 0)
					{
						sname = sname.SubString(1, pos - 1);
					}
					//BYTE         ���������� �������� � ����� �����
					pos = sname.Length();
					ch_val[0] = static_cast<char>(pos + 1);
					fwrite(ch_val, 1, 1, f);
					if(pos > 0)
					{
						//BYTE ������������ 32b
						WriteRoundup32Offset(f);

						//BYTE[] �������� �����-��������� (������ ��� �����, ��� ����������)
						fwrite(AnsiString(sname).c_str(), 1, pos + 1, f);
					}
				}
			}
		}

		//WORD			���������� ������-��������
		fwrite(&font_res_count, sizeof(short), 1, f);
		if(font_res_count > 0) // ���� ���� �����
		{
			j = font_res_count * SIZEOF_PTR;
			unsigned char* achptr = new unsigned char[j];
			memset(achptr, 0, j);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//BYTE[] ������ ������ ��� ������ ���������� �� ������
			fwrite(achptr, 1, j, f);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//BYTE[] ������ ������ ��� ������ ���������� �� �����-������� (TMFont, res_font)
			fwrite(achptr, 1, j, f);
			delete[] achptr;

			for (it = all_res_lst->begin(); it != all_res_lst->end(); ++it)
			{
				if(it->mType == ResourceTypes::RESOURCE_TYPE_FONTDATA)
				{
					TSaveResItem ri;
					ri.mType = it->mType;
					ri.mName = it->mName;
					ri.mIdx = -1;
					font_res.push_back(ri);
					UnicodeString sname = it->mName;
					__int32 pos = sname.LastDelimiter(_T(".\0"));
					if (pos > 0)
					{
						sname = sname.SubString(1, pos - 1);
					}
					//BYTE         ���������� �������� � ����� �����
					pos = sname.Length();
					ch_val[0] = static_cast<char>(pos + 1);
					fwrite(ch_val, 1, 1, f);
					if(pos > 0)
					{
						//BYTE ������������ 32b
						WriteRoundup32Offset(f);

						//BYTE[] �������� �����-��������� (������ ��� �����, ��� ����������)
						fwrite(AnsiString(sname).c_str(), 1, pos + 1, f);
					}
				}
			}
		}

		//WORD			���������� �������� ��������
		fwrite(&sound_res_count, sizeof(short), 1, f);
		if(sound_res_count > 0) //���� ���� �������� �������
		{
			j = sound_res_count * SIZEOF_PTR;
			unsigned char* achptr = new unsigned char[j];
			memset(achptr, 0, j);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//BYTE[] ������ ������ ��� ������ ���������� �� ������
			fwrite(achptr, 1, j, f);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//BYTE[] ������ ������ ��� ������ ���������� �� �������� �������
			fwrite(achptr, 1, j, f);
			delete[] achptr;

			for (it = all_res_lst->begin(); it != all_res_lst->end(); ++it)
			{
				if(it->mType == ResourceTypes::RESOURCE_TYPE_SOUNDDATA)
				{
					TSaveResItem ri;
					ri.mType = it->mType;
					ri.mName = it->mName;
					ri.mIdx = -1;
					sound_res.push_back(ri);
					UnicodeString sname = it->mName;
					__int32 pos = sname.LastDelimiter(_T(".\0"));
					if (pos > 0)
					{
						sname = sname.SubString(1, pos - 1);
					}
					//BYTE         ���������� �������� � ����� �����
					pos = sname.Length();
					ch_val[0] = static_cast<char>(pos + 1);
					fwrite(ch_val, 1, 1, f);
					if(pos > 0)
					{
						//BYTE ������������ 32b
						WriteRoundup32Offset(f);

						//BYTE[] �������� �����-��������� (������ ��� �����, ��� ����������)
						fwrite(AnsiString(sname).c_str(), 1, pos + 1, f);
					}
				}
			}
		}
	}
	all_res_lst->clear();
	delete all_res_lst;

	//____________���� ��������

	//BYTE          ���������� ����� ��������
	ch_val[0] = (char)mainCommonRes->aniNameTypes->Count;
	fwrite(ch_val, 1, 1, f);
	//BYTE			���������� join node
	ch_val[0] = (char)Form_m->mpEdPrObjWndParams->joinNodesCount;
	fwrite(ch_val, 1, 1, f);

	//____________���������:

	//BYTE          ���������� ���������
	ch_val[0] = (char)mainCommonRes->states->Count;
	len = (unsigned char)ch_val[0];
	char *opp_map_arr = new char[len];
	setmem(opp_map_arr, len, -1);
	fwrite(ch_val, 1, 1, f);

	//BYTE ������������ 32b
	WriteRoundup32Offset(f);
	//BYTE[] ������ ������ ��� ������ ���������� �� ���������
	WriteRoundup32ReserveForPointers(f, len);

	for (i = 0; i < len; i++)
	{
		//BYTE ������������ 32b
		WriteRoundup32Offset(f);

		TState *st = (TState*)mainCommonRes->states->Items[i];
		//BYTE         id ��������
		ch_val[0] = (char)mainCommonRes->GetAnimationID(st->animation);
		fwrite(ch_val, 1, 1, f);
		//BYTE         �����������
		ch_val[0] = (char)st->direction;
		fwrite(ch_val, 1, 1, f);
		opp_map_arr[i] = (char)mainCommonRes->GetStateID(mainCommonRes->stateOppositeTable->Values[st->name]);
	}
	//���������� ������������ ��������� = ���������� ���������
	//{
	//BYTE   id ������������ ������������ ����������� ������ (-1 -- ������ �� �������������)
	//}...    ���������� ������

	//BYTE ������������ 32b
	WriteRoundup32Offset(f);

	fwrite(opp_map_arr, 1, len, f);
	delete[] opp_map_arr;

	//____________�������:

	//WORD ���������� ���������� (v22)
	len = mainCommonRes->GetBitmapFrameCount();
	fwrite(&len, sizeof(short), 1, f);
	if(len > 0)
	{
		//BYTE ������������ 32b
		WriteRoundup32Offset(f);
		//BYTE[] ������ ������ ��� ������ ���������� �� ������
		WriteRoundup32ReserveForPointers(f, len);

		for (j = 0; j < len; j++)
		{
			UnicodeString sname = _T("\0");
			const TFrameObj* bo = mainCommonRes->GetBitmapFrameByIdx(j);
			if (bo != NULL)
			{
				bo->getSourceRes(NULL, NULL, &check_bmp, &sname);
			}

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//WORD ������ �������
			w_val = -1;
			if(bo != NULL && !sname.IsEmpty())
			{
				i = 0;
				for (sresit = graphics_res.begin(); sresit != graphics_res.end(); ++sresit)
				{
					if(sresit->mName.Compare(sname) == 0)
					{
						w_val = static_cast<short>(i);
						break;
					}
					i++;
				}
			}
			fwrite(&w_val, sizeof(short), 1, f);
			//WORD	X ��������� �� ���������
			w_val = (short)((bo != NULL) ? bo->x : 0);
			fwrite(&w_val, sizeof(short), 1, f);
			//WORD	Y
			w_val = (short)((bo != NULL) ? bo->y : 0);
			fwrite(&w_val, sizeof(short), 1, f);
			//WORD	W
			w_val = (short)((bo != NULL) ? bo->w : 0);
			fwrite(&w_val, sizeof(short), 1, f);
			//WORD	H
			w_val = (short)((bo != NULL) ? bo->h : 0);
			fwrite(&w_val, sizeof(short), 1, f);
		}
	}
	//WORD ���������� ��������-��������� (������ ������� ������ ��� ���������� ����� � ����� 0�-0, 1�-1, 2�-2...) (v22)
	w_val = static_cast<short>(mainCommonRes->GParentObj->Count);
	len = w_val;
	fwrite(&w_val, sizeof(short), 1, f);
	if(len > 0)
	{
		//BYTE ������������ 32b
		WriteRoundup32Offset(f);

		//BYTE[] ������ ������ ��� ������ ������ � ���������� (���������� ��������-��������� * sizeof(WORD))
		j = len * sizeof(short);
		unsigned char* achptr = new unsigned char[j];
		memset(achptr, 0, j);
		fwrite(achptr, 1, j, f);
		delete[] achptr;

		//BYTE ������������ 32b
		WriteRoundup32Offset(f);
		//BYTE[] ������ ������ ��� ������ ���������� ��� ������ �������� (���������� ��������-��������� * sizeof(void*))
		WriteRoundup32ReserveForPointers(f, len);

		//BYTE ������������ 32b
		WriteRoundup32Offset(f);
		//BYTE[] ������ ������ ��� ������ ���������� ��� ������ ������� (���������� ��������-��������� * sizeof(void*))
		WriteRoundup32ReserveForPointers(f, len);

		//BYTE ������������ 32b
		WriteRoundup32Offset(f);
		//BYTE[] ������ ������ ��� ������ ���������� ��� ������ ���������� ������� (���������� ��������-��������� * sizeof(void*))
		WriteRoundup32ReserveForPointers(f, len);

		for (i = 0; i < len; i++)
		{
			GPerson *gp = (GPerson*)mainCommonRes->GParentObj->Items[i];
			//WORD	���� ���������, �� ������ ����
			w_val = gp->DecorType;
			fwrite(&w_val, sizeof(short), 1, f);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ���������� ��� ������ �������� (���������� ��������-��������� * sizeof(void*))
			WriteRoundup32ReserveForPointers(f, mainCommonRes->aniNameTypes->Count);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ���������� ��� ������ ������� (���������� ��������-��������� * sizeof(void*))
			WriteRoundup32ReserveForPointers(f, mainCommonRes->aniNameTypes->Count);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ���������� ��� ������ ���������� ������� (���������� ��������-��������� * sizeof(void*))
			WriteRoundup32ReserveForPointers(f, mainCommonRes->aniNameTypes->Count);

			for (__int32 acr = 0; acr < mainCommonRes->aniNameTypes->Count; acr++)
			{
				TAnimation *anm = gp->GetAnimation(mainCommonRes->aniNameTypes->Strings[acr]);
				//WORD 1 - �������� ����������, ����� -- �� ���������� (��� ������� ����������)
				w_val = ((anm == NULL) ? 0 : 1);
				fwrite(&w_val, sizeof(short), 1, f);
				if (anm == NULL)
				{
					continue;
				}

				//BYTE ������������ 32b
				WriteRoundup32Offset(f);

				//WORD �������� �� ������� ����� view rect offx ������ �������� ���� �������������� (-512..+512)
				w_val = (short)anm->GetViewRect().Left;
				fwrite(&w_val, sizeof(short), 1, f);
				//WORD �������� �� ������� ����� view rect offy ������ �������� ���� ��������������
				w_val = (short)anm->GetViewRect().Top;
				fwrite(&w_val, sizeof(short), 1, f);
				//WORD �������� �� ������� ����� view rect offx ������� ������� ���� �������������� (-512..+512)
				w_val = (short)(anm->GetViewRect().Width() + anm->GetViewRect().Left);
				fwrite(&w_val, sizeof(short), 1, f);
				//WORD �������� �� ������� ����� view rect offy ������� ������� ���� ��������������
				w_val = (short)(anm->GetViewRect().Height() + anm->GetViewRect().Top);
				fwrite(&w_val, sizeof(short), 1, f);
				//WORD ����������� ��������
				w_val = (short)anm->GetLooped();
				fwrite(&w_val, sizeof(short), 1, f);
				//WORD	���������� ������� ��������
				if(anm->HasStreamObject())
				{
                    __int32 frameCount = 0;
					fwrite(&frameCount, sizeof(short), 1, f);
				}
				else
				{
					__int32 frameCount = anm->GetFrameCount();
					fwrite(&frameCount, sizeof(short), 1, f);
					saveAnimationFrames(frameCount, anm, gp, NULL, mainCommonRes);
				}
			}
		}
	}

	{ //���������� ���������� ��������
		TObjManager *objManager;
		for (i = 0; i < m_pLevelList->Count; i++)
		{
			objManager = (TObjManager*)m_pLevelList->Items[i];
			if (objManager != NULL)
			{
				break;
            }
		}
		SaveTriggersForMap(f, objManager, mainCommonRes, NULL, lvars, true);
		SaveScriptsForMap(f, objManager, this->mpTexts, lvars, true);
	}

	//WORD ����������� �����, ��������� ������
	w_val = CS_MAGIC_NUMBER;
	fwrite(&w_val, sizeof(short), 1, f);

	fclose(f);
	f = NULL;

	//-------------
	//����� ����:
	//-------------

	for (m = 0; m < m_pLevelList->Count; m++)
	{
		TObjManager *objManager = (TObjManager*)m_pLevelList->Items[m];
		nfile = map_file_name + _T("\\l\0") + IntToStr(m);

		if(f != NULL)
		{
			fclose(f);
			f = NULL;
		}
		if ((f = _wfopen(nfile.c_str(), _T("wb\0"))) == NULL)
		{
			break;
		}

		//____________������� �����:

		//WORD          ������ ������ � ���������� (W)
		w_val = (short)(d_w / p_w);
		fwrite(&w_val, sizeof(short), 1, f);
		//WORD          ������ ������ � ���������� (H)
		w_val = (short)(d_h / p_h);
		fwrite(&w_val, sizeof(short), 1, f);
		//BYTE          ����
		ch_val[0] = (char)objManager->znCount;
		fwrite(ch_val, 1, 1, f);
		//WORD			������������ ���������� ����������� �������� �� �����
		{
			std::list<TResourceItem> *str_lst_bg;
			str_lst_bg = createImgListForSave(lstBackObjOnly, objManager);
			w_val = (short)str_lst_bg->size();
			fwrite(&w_val, sizeof(short), 1, f);
			str_lst_bg->clear();
			delete str_lst_bg;
		}
		opt_tiletypes_list = new TList();
		tmap = new unsigned short[objManager->mp_w * objManager->mp_h];
		CreateOptimizedBackObjListAndMap(opt_tiletypes_list, tmap, objManager, nullptr, nullptr);
		std::list<TSaveResItem> srlist;
		std::list<TSaveResItem> srlist_fnt;
		std::list<TSaveResItem> srlist_snd;
		//WORD ���������� ���������� �����
		w_val = static_cast<short>(opt_tiletypes_list->Count);
		fwrite(&w_val, sizeof(short), 1, f);
		len = opt_tiletypes_list->Count;
		if(len > 0)
		{
			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ������
			WriteRoundup32ReserveForPointers(f, len);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ������
			WriteRoundup32ReserveForPointers(f, len);

			for (i = 0; i < len; i++)
			{
				char type;
				GMapBGObj *mo;
				UnicodeString sname;
				mo = (GMapBGObj*)opt_tiletypes_list->Items[i];

				//BYTE ������������ 32b
				WriteRoundup32Offset(f);

				//WORD ������� ��������� (��������-���������� (-/+), �������� �� ���������� (abs>62) � ���������� ������ (1..62))
				{
					TFrameObj *bo = mo->GetFrameObj(0);
					type = 0;
					if(bo != NULL)
					{
						const TBGobjProperties* bgprp = mainCommonRes->GetBGobjProperties(bo->bgPropertyName);
						if(bgprp != NULL)
						{
							type = bgprp->type;
						}
					}
					w_val = type;
					fwrite(&w_val, sizeof(short), 1, f);
				}
				//WORD ���������� �����
				w_val = static_cast<short>(lrCount);
				fwrite(&w_val, sizeof(short), 1, f);
				if(lrCount > 0)
				{
					//BYTE ������������ 32b
					WriteRoundup32Offset(f);
					//BYTE[] ������ ������ ��� ������ ������
					WriteRoundup32ReserveForPointers(f, lrCount);
				}
				for (__int32 ki = 0; ki < lrCount; ki++)
				{
					//BYTE ������������ 32b
					WriteRoundup32Offset(f);

					TFrameObj *bo = mo->GetFrameObj(ki);
					//WORD	������ �����-��������� (������ �������� ����, � ����� ����� 0�-0, 1�-1, 2�-2...), �� �������� ������� ���� ������
					check_bmp = -1;
					w_val = -1;
					if(bo != NULL)
					{
						bo->getSourceRes(NULL, NULL, &check_bmp, &sname);
						if(check_bmp >= 0 && !sname.IsEmpty())
						{
							w_val = static_cast<short>(FindAddAndSortResList(graphics_res, sname, RESOURCE_TYPE_IMAGE, srlist));
						}
					}
					fwrite(&w_val, sizeof(short), 1, f);
					//WORD X ��������� �� ���������
					w_val = (short)(check_bmp < 0 ? 0 : bo->x);
					fwrite(&w_val, sizeof(short), 1, f);
					//WORD Y
					w_val = (short)(check_bmp < 0 ? 0 : bo->y);
					fwrite(&w_val, sizeof(short), 1, f);
				}
			}
		}
		//WORD ������ �����
		w_val = (short)objManager->mp_w;
		fwrite(&w_val, sizeof(short), 1, f);
		//WORD ������ �����
		w_val = (short)objManager->mp_h;
		fwrite(&w_val, sizeof(short), 1, f);
		if(len > 0)
		{
			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//BYTE[] �����
			fwrite(tmap, 1, objManager->mp_w * objManager->mp_h * sizeof(unsigned short), f);
		}
		delete[] tmap;

		//____________�������:
		list = new TList();
		CreateOptimizedGameObjList(list, objManager);
		{
			TList *frlist = new TList();
			for (i = 0; i < list->Count; i++)
			{
				GPerson *gp = static_cast<GPerson*>(list->Items[i]);
				if(gp->UseGraphics)
				{
					gp->GetBitmapFrameList(frlist);
					for (j = 0; j < frlist->Count; j++)
					{
						UnicodeString sname = _T("");
						const TFrameObj* bo = mainCommonRes->GetBitmapFrame(reinterpret_cast<hash_t>(frlist->Items[j]));
						if (bo != NULL)
						{
							bo->getSourceRes(NULL, NULL, &check_bmp, &sname);
						}
						if(bo != NULL && !sname.IsEmpty())
						{
							FindAddAndSortResList(graphics_res, sname, RESOURCE_TYPE_IMAGE, srlist);
						}
					}
				}
			}
			delete frlist;

			UnicodeString ext = _T(".xxx");
			forceCorrectFontFileExtention(ext);
			for (i = 0; i < objManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT); i++)
			{
				TMapGPerson *obj = static_cast<TMapGPerson*>(objManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, i));
				if (obj->GetObjType() == objMAPTEXTBOX)
				{
					TMapTextBox *tb = static_cast<TMapTextBox*>(obj);
					UnicodeString fn = tb->GetFontName();
					if(!fn.IsEmpty())
					{
						const TImageFont* pfnt = mpTexts->GetFont(fn);
						if(pfnt != NULL)
						{
							if(!pfnt->GetImageResourceName().IsEmpty())
							{
								FindAddAndSortResList(graphics_res, pfnt->GetImageResourceName(), RESOURCE_TYPE_IMAGE, srlist);
								FindAddAndSortResList(font_res, forceCorrectFontFileExtention(pfnt->GetName()), RESOURCE_TYPE_FONTDATA, srlist_fnt);
							}
						}
					}
				}
				else
				if (obj->GetObjType() == objMAPSOUNDSCHEME)
				{
					TMapSoundScheme *mss = static_cast<TMapSoundScheme*>(obj);
					__int32 mssitct = mss->GetItemsCount();
					for(__int32 mssit = 0; mssit < mssitct; mssit++)
					{
						SoundSchemePropertyItem sit;
						mss->GetItemByIndex(mssit, sit);
						if(!sit.mFileName.IsEmpty())
						{
							FindAddAndSortResList(sound_res, sit.mFileName, RESOURCE_TYPE_SOUNDDATA, srlist_snd);
						}
					}
                }
			}
		}
		// ����������� �������, ������� ������������ �� �����
		len = srlist.size();
		if(len > 0)
		{
			//WORD ���������� ����������� ��������-����������, ������� ������������ �� ����� (������������� �� ����������� w*h)
			fwrite(&len, sizeof(unsigned short), 1, f);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//WORD[] ������ ��������
			for (sresit = srlist.begin(); sresit != srlist.end(); ++sresit)
			{
				w_val = static_cast<short>(sresit->mIdx);
				fwrite(&w_val, sizeof(short), 1, f);
			}
		}
		else
		{
			//WORD ���������� �������� == 0
			fwrite(&len, sizeof(unsigned short), 1, f); //���������� ����������� ��������-����������
		}
		// �����, ������� ������������ �� �����
		len = srlist_fnt.size();
		if(len > 0)
		{
			//WORD ���������� ������, ������� ������������ �� �����
			fwrite(&len, sizeof(unsigned short), 1, f);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//WORD[] ������ ��������
			for (sresit = srlist_fnt.begin(); sresit != srlist_fnt.end(); ++sresit)
			{
				w_val = static_cast<short>(sresit->mIdx);
				fwrite(&w_val, sizeof(short), 1, f);
			}
		}
		else
		{
			//WORD ���������� �������� == 0
			fwrite(&len, sizeof(unsigned short), 1, f); //���������� ������
		}
		// �������� �������, ������� ������������ �� �����
		len = srlist_snd.size();
		if(len > 0)
		{
			__int32 sfx_ct, bgm_ct;
			SoundSchemePropertyItem sspi;
			TMapSoundScheme *ssh = static_cast<TMapSoundScheme*>(objManager->FindObjectPointer(UnicodeString(SOUNDSCHEME_NAME)));
			sfx_ct = 0;
			bgm_ct = 0;
			for(i = 0; i < ssh->GetItemsCount(); i++)
			{
				ssh->GetItemByIndex(i, sspi);
				for (sresit = srlist_snd.begin(); sresit != srlist_snd.end(); ++sresit)
				{
					if(sresit->mName.Compare(sspi.mFileName) == 0)
					{
						sresit->mSoundType = sspi.mType;
						switch(sspi.mType)
						{
							case SoundSchemePropertyItem::SSRT_SFX:
								sfx_ct++;
							break;
							case SoundSchemePropertyItem::SSRT_BG:
								bgm_ct++;
							break;
							default:
							break;
						}
                    }
				}
			}
			//WORD			���������� sfx ��������
			fwrite(&sfx_ct, sizeof(unsigned short), 1, f);
			if(sfx_ct > 0)
			{
				//BYTE ������������ 32b
				WriteRoundup32Offset(f);

				for(i = 0; i < ssh->GetItemsCount(); i++)
				{
					ssh->GetItemByIndex(i, sspi);
					for (sresit = srlist_snd.begin(); sresit != srlist_snd.end(); ++sresit)
					{
						if(sresit->mName.Compare(sspi.mFileName) == 0)
						{
							if(sspi.mType == SoundSchemePropertyItem::SSRT_SFX)
							{
								w_val = static_cast<short>(sresit->mIdx);
								fwrite(&w_val, sizeof(short), 1, f);
							}
						}
					}
				}
			}
			//WORD			���������� bgm ��������
			fwrite(&bgm_ct, sizeof(unsigned short), 1, f);
			if(bgm_ct > 0)
			{
				//BYTE ������������ 32b
				WriteRoundup32Offset(f);

				for(i = 0; i < ssh->GetItemsCount(); i++)
				{
					ssh->GetItemByIndex(i, sspi);
					for (sresit = srlist_snd.begin(); sresit != srlist_snd.end(); ++sresit)
					{
						if(sresit->mName.Compare(sspi.mFileName) == 0)
						{
							if(sspi.mType == SoundSchemePropertyItem::SSRT_BG)
							{
								w_val = static_cast<short>(sresit->mIdx);
								fwrite(&w_val, sizeof(short), 1, f);
							}
						}
					}
				}
			}
		}
		else
		{
			//WORD ���������� sfx �������� == 0
			fwrite(&len, sizeof(unsigned short), 1, f);
			//WORD ���������� bgm �������� == 0
			fwrite(&len, sizeof(unsigned short), 1, f);
		}
		// ����������� �������
		{
			TStringList *anim_files = new TStringList();
			TStringList *pprobj_list = new TStringList();
			unsigned short obj_count, ani_count;
			createStreamDataForSave(anim_files, pprobj_list, &obj_count, objManager);
			//WORD ���������� ��������� ������ ��� ��������� �������� (�����)
			ani_count = static_cast<unsigned short>(anim_files->Count);
			fwrite(&ani_count, sizeof(unsigned short), 1, f);
			if(ani_count != 0)	//���� �� ����
			{
				unsigned short af;

				//BYTE ������������ 32b
				WriteRoundup32Offset(f);
				//BYTE[] ������ ������ ��� ������ ���������� �� ������
				WriteRoundup32ReserveForPointers(f, ani_count);

				for(af = 0; af < ani_count; af++)
				{
					UnicodeString tfname = anim_files->Strings[af];
					//BYTE ���������� �������� � �����
					j = ch_val[0] = static_cast<char>(tfname.Length());
					ch_val[0]++;
					fwrite(ch_val, 1, 1, f);

					//BYTE ������������ 32b
					WriteRoundup32Offset(f);

					//BYTE[]		�������� �����
					fwrite(AnsiString(tfname).c_str(), 1, j, f);
					ch_val[0] = 0;
					fwrite(ch_val, 1, 1, f);
				}
				//WORD ���������� ��������� �������� �� �����
				fwrite(&obj_count, sizeof(unsigned short), 1, f);
				//WORD ���������� ����� ��������� �������� �� �����
				obj_count = static_cast<unsigned short>(pprobj_list->Count);
				fwrite(&obj_count, sizeof(unsigned short), 1, f);

				//BYTE ������������ 32b
				WriteRoundup32Offset(f);

				//WORD[] ������, ������� ��������-��������� ��������� (������) � �������� (��������)
				for(af = 0; af < obj_count; af++)
				{
					ani_count = static_cast<unsigned short>(mainCommonRes->GetParentObjID(pprobj_list->Names[af]));
					fwrite(&ani_count, sizeof(unsigned short), 1, f);
					ani_count = static_cast<unsigned short>(mainCommonRes->GetAnimationID(pprobj_list->ValueFromIndex[af]));
					fwrite(&ani_count, sizeof(unsigned short), 1, f);
				}
				//����� ������, ������� ������ �������� � ������� �������� �������� � ��������-���������
				//BYTE[] ������ "������ ��������" ����, � ����� ���������� ����� ������
				for(af = 0; af < obj_count; af++)
				{
					const GPerson *gp = mainCommonRes->GetParentObjByName(pprobj_list->Names[af]);
					const TAnimation *anm = gp->GetAnimation(pprobj_list->ValueFromIndex[af]);
					__int32 frameCount = anm->GetFrameCount();
					ani_count = static_cast<unsigned short>(frameCount);
					fwrite(&ani_count, sizeof(unsigned short), 1, f);
					saveAnimationFrames(frameCount, anm, gp, anim_files, mainCommonRes);
				}
			}
			delete anim_files;
			delete pprobj_list;
		}

		{
			short pr_idata_ct = 0;
			short all_pr_count = 0;

			//WORD BACK_DECORATION ���������� ��������� ������� ����� ��� ���
			w_val = 0;
			len = objManager->GetObjTypeCount(TObjManager::BACK_DECORATION);
			for (j = 0; j < len; j++)
			{
				if (((TMapGPerson*)objManager->GetObjByIndex(TObjManager::BACK_DECORATION, j))->GetZone() == -1)
				{
					w_val++;
					pr_idata_ct++;
				}
			}
			fwrite(&w_val, sizeof(unsigned short), 1, f);

			//WORD CHARACTER_OBJECT ��� ���
			len = objManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
			w_val = 0;
			for (j = 0; j < len; j++)
			{
				TMapGPerson *gp = (TMapGPerson*)objManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, j);
				if(gp->GetObjType() == objMAPSOUNDSCHEME)
				{
					continue;
				}
				if (gp->GetZone() == -1)
				{
					if(gp->GetObjType() == objMAPOBJPOOL)
					{
						TMapObjPool *opgp = (TMapObjPool*)gp;
						const short totalobjct = (short)opgp->GetTolalObjCount();
						w_val += totalobjct;
						pr_idata_ct += totalobjct;
						all_pr_count += totalobjct;
					}
					else
					{
						w_val++;
						pr_idata_ct++;
						all_pr_count++;
					}
				}
			}
			fwrite(&w_val, sizeof(unsigned short), 1, f);

			//WORD FORE_DECORATION ��� ���
			len = objManager->GetObjTypeCount(TObjManager::FORE_DECORATION);
			w_val = 0;
			for (j = 0; j < len; j++)
			{
				if (((TMapGPerson*)objManager->GetObjByIndex(TObjManager::FORE_DECORATION, j))->GetZone() == -1)
				{
					w_val++;
					pr_idata_ct++;
				}
			}
			fwrite(&w_val, sizeof(unsigned short), 1, f);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//WORD[]      ������, ���������� ��������� ������� ����� ������ �� ���
			len = objManager->GetObjTypeCount(TObjManager::BACK_DECORATION);
			for (i = 0; i < objManager->znCount; i++)
			{
				w_val = 0;
				for (j = 0; j < len; j++)
				{
					if (((TMapGPerson*)objManager->GetObjByIndex(TObjManager::BACK_DECORATION, j))->GetZone() == i)
					{
						w_val++;
						pr_idata_ct++;
					}
				}
				fwrite(&w_val, sizeof(unsigned short), 1, f);
			}

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//WORD[]      ������, ���������� �������� �� ����� ������ �� ���
			len = objManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
			for (i = 0; i < objManager->znCount; i++)
			{
				w_val = 0;
				for (j = 0; j < len; j++)
				{
					TMapGPerson *gp = (TMapGPerson*)objManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, j);
					if(gp->GetObjType() == objMAPSOUNDSCHEME)
					{
						continue;
					}
					if (gp->GetZone() == i)
					{
						if(gp->GetObjType() == objMAPOBJPOOL)
						{
							TMapObjPool *opgp = (TMapObjPool*)gp;
							const short totalobjct = (short)opgp->GetTolalObjCount();
							w_val += totalobjct;
							pr_idata_ct += totalobjct;
							all_pr_count += totalobjct;
						}
						else
						{
							w_val++;
							pr_idata_ct++;
							all_pr_count++;
						}
					}
				}
				fwrite(&w_val, sizeof(unsigned short), 1, f);
			}

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//WORD[]      ������, ���������� ��������� ��������� ����� ������ �� ���
			len = objManager->GetObjTypeCount(TObjManager::FORE_DECORATION);
			for (i = 0; i < objManager->znCount; i++)
			{
				w_val = 0;
				for (j = 0; j < len; j++)
				{
					if (((TMapGPerson*)objManager->GetObjByIndex(TObjManager::FORE_DECORATION, j))->GetZone() == i)
					{
						w_val++;
						pr_idata_ct++;
					}
				}
				fwrite(&w_val, sizeof(unsigned short), 1, f);
			}

			//WORD ����������� �����, ��������� ������
			fwrite(&pr_idata_ct, sizeof(short), 1, f);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ������ pr_fdata (fx32**)
			WriteRoundup32ReserveForPointers(f, pr_idata_ct);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ������ pr_idata (s32**)
			WriteRoundup32ReserveForPointers(f, pr_idata_ct);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ������ pr_cfdata (fx32**)
			WriteRoundup32ReserveForPointers(f, all_pr_count);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ������ pr_cidata (s32**)
			WriteRoundup32ReserveForPointers(f, all_pr_count);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ������ pr_prp (fx32**)
			WriteRoundup32ReserveForPointers(f, all_pr_count);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ������ pr_spt (u16**)
			WriteRoundup32ReserveForPointers(f, all_pr_count);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ������ pr_textdata (u16**)
			WriteRoundup32ReserveForPointers(f, all_pr_count);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ������ pr_textdataalign (u32*)
			WriteRoundup32ReserveForPointers(f, all_pr_count);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ������ pr_textdatatext (wchar***)
			WriteRoundup32ReserveForPointers(f, all_pr_count);
		}

		delete list;

		//��������� ������� �����
		//!!! ������ ������� ������ ��� ���������� ����� � ����� 0�-0, 1�-1, 2�-2...
		len = objManager->GetObjTypeCount(TObjManager::BACK_DECORATION);
		for (i = 0; i < len; i++)
		{
			fx32 fx_val;

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			TMapGPerson *db = (TMapGPerson*)objManager->GetObjByIndex(TObjManager::BACK_DECORATION, i);
			db->getCoordinates(&x, &y);
			//(fx32)         X ��������� �� ����� (������������ align)
			fx_val = FX32(x);
			writeFx(&fx_val, f);
			//(fx32)         Y
			fx_val = FX32(y);
			writeFx(&fx_val, f);
			fx_val = 0;
			//(fx32)         (FLEFT)
			writeFx(&fx_val, f);
			//(fx32)         (FTOP)
			writeFx(&fx_val, f);
			//(fx32)         (FCURRWIDTH)
			writeFx(&fx_val, f);
			//(fx32)         (FCURRHEIGHT)
			writeFx(&fx_val, f);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//DWORD         ������ ��������-������� (������)
			dw_val = objManager->GetParentObjID(db->getGParentName());
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD         ����
			dw_val = db->GetZone();
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD         id ��������������� ���������
			dw_val = objManager->GetStateID(db->State_Name);
			fwrite(&dw_val, sizeof(__int32), 1, f);
			dw_val = 0;
			//DWORD         (ITEMPSTATE)
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD         (IANIFRIDX)
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD         (IANIENDFLAG)
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD         (ITIMER) ������
			fwrite(&dw_val, sizeof(__int32), 1, f);
		}

		//������� �� �����

		//��� ��� � ����� ������ ���� ������� 32
		//������������ �������� ������ �������, �� ����� ����� ���������� ������ ���������
		len = objManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
		for (i = 0; i < len; i++)
		{
			TMapGPerson *gp = (TMapGPerson*)objManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, i);
			if(gp->GetObjType() == objMAPSOUNDSCHEME)
			{
				continue;
			}
			if(gp->GetObjType() == objMAPOBJPOOL)
			{
				__int32 pos_x, pos_y;
				TMapObjPool *mop = (TMapObjPool*)gp;
				ObjPoolPropertyItem it;
				__int32 it_count = mop->GetItemsCount();
				TObjManager *towner = new TObjManager(mainCommonRes);
				mop->getCoordinates(&x, &y);
				pos_x = pos_y = 0;
				for(__int32 i_it = 0; i_it < it_count; i_it++)
				{
					mop->GetItemByIndex(i_it, it);
					const GPerson *gmp = (GPerson*)mainCommonRes->GetParentObjByName(it.mName);
					assert(gmp);
					for(unsigned __int32 i_curr_it = 0; i_curr_it < it.mCount; i_curr_it++)
					{
						double enabled_val;
						TMapGPerson *mp = towner->CreateMapGPerson(x, y, &pos_x, &pos_y, gmp->GetName());
						mp->SetProperties(gmp->GetBaseProperties());
						if(mp->GetVarValue(&enabled_val, UnicodeString(PredefProperties::PropertyNames[PredefProperties::PRP_ENABLE])))
						{
							enabled_val = 0.0f;
							mp->SetVarValue(enabled_val, UnicodeString(PredefProperties::PropertyNames[PredefProperties::PRP_ENABLE]));
						}
						if (!gmp->IsDecoration)
						{
							mp->SetDecorType(decorNONE);
						}
						else
						{
							mp->SetDecorType(gmp->DecorType);
						}
						mp->State_Name = gmp->DefaultStateName;
						mp->SetZone(mop->GetZone());
						SaveObjForMapSave(mp, objManager, lvars, font_res);
					}
					towner->ClearAll();
				}
				delete towner;
			}
			else
			{
				SaveObjForMapSave(gp, objManager, lvars, font_res);
			}
		}

		//��������� ��������� �����
		//!!! ������ ������� ������ ��� ���������� ����� � ����� 0�-0, 1�-1, 2�-2...
		len = objManager->GetObjTypeCount(TObjManager::FORE_DECORATION);
		for (i = 0; i < len; i++)
		{
			fx32 fx_val;

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			TMapGPerson *df = (TMapGPerson*)objManager->GetObjByIndex(TObjManager::FORE_DECORATION, i);
			df->getCoordinates(&x, &y);
			//(fx32)         X ��������� �� ����� (������������ align)
			fx_val = FX32(x);
			writeFx(&fx_val, f);
			//(fx32)         Y
			fx_val = FX32(y);
			writeFx(&fx_val, f);
			fx_val = 0;
			//(fx32)         (FLEFT)
			writeFx(&fx_val, f);
			//(fx32)         (FTOP)
			writeFx(&fx_val, f);
			//(fx32)         (FCURRWIDTH)
			writeFx(&fx_val, f);
			//(fx32)         (FCURRHEIGHT)
			writeFx(&fx_val, f);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);

			//DWORD         ������ ��������-������� (������)
			dw_val = objManager->GetParentObjID(df->getGParentName());
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD         ����
			dw_val = df->GetZone();
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD         id ��������������� ���������
			dw_val = objManager->GetStateID(df->State_Name);
			fwrite(&dw_val, sizeof(__int32), 1, f);
			dw_val = 0;
			//DWORD         (ITEMPSTATE)
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD         (IANIFRIDX)
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD         (IANIENDFLAG)
			fwrite(&dw_val, sizeof(__int32), 1, f);
			//DWORD         (ITIMER) ������
			fwrite(&dw_val, sizeof(__int32), 1, f);
		}

		SaveTriggersForMap(f, objManager, mainCommonRes, opt_tiletypes_list, lvars, false);

		delete opt_tiletypes_list;

		//____________������� �����:
		//WORD          ���������� ����� �� �����
		w_val = (short)objManager->GetObjTypeCount(TObjManager::DIRECTOR);
		len = w_val;
		fwrite(&w_val, sizeof(short), 1, f);
		if(len > 0)
		{
			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ������
			WriteRoundup32ReserveForPointers(f, len);

			//BYTE ������������ 32b
			WriteRoundup32Offset(f);
			//BYTE[] ������ ������ ��� ������ ������
			WriteRoundup32ReserveForPointers(f, len);

			for (i = 0; i < len; i++)
			{
                fx32 fx_val;

				//BYTE ������������ 32b
				WriteRoundup32Offset(f);

				TMapDirector *dir = (TMapDirector*)objManager->GetObjByIndex(TObjManager::DIRECTOR, i);
				dir->getCoordinates(&x, &y);
				//DWORD         X       ��������� �� ����� (������ ������� �����)
				fx_val = FX32(x);
				writeFx(&fx_val, f);
				//DWORD         Y
				fx_val = FX32(y);
				writeFx(&fx_val, f);

				//BYTE ������������ 32b
				WriteRoundup32Offset(f);

				//�����
				for (j = 0; j < 4; j++)
				{
					w_val = (short)objManager->GetDirectorID(dir->nearNodes[j]);
					fwrite(&w_val, sizeof(short), 1, f);
				}
				//WORD	NODE_RECALCULATE_DIST_FLAG
				w_val = 0;
				fwrite(&w_val, sizeof(short), 1, f);
				//WORD	NODE_ID_TRIGGER, id ��������
				w_val = (short)objManager->GetTriggerID(dir->nodeTrigger);
				fwrite(&w_val, sizeof(short), 1, f);
				//WORD	NODE_ID_EVENT, id �������
				w_val = (short)objManager->GetEventID(dir->objTeleport);
				fwrite(&w_val, sizeof(short), 1, f);
			}
		}

		//____________�������:

		SaveScriptsForMap(f, objManager, this->mpTexts, lvars, false);

		//____________����������� �����
		{
			//BYTE			���������� ����������� ������
			const unsigned char sclct = (unsigned char)objManager->GetObjTypeCount(TObjManager::COLLIDESHAPE);
			fwrite(&sclct, 1, 1, f);
			//���� �� ����� ����
			if(sclct > 0)
			{
				//BYTE ������������ 32b
				WriteRoundup32Offset(f);
				//BYTE[] ������ ������ ��� ������ ������
				WriteRoundup32ReserveForPointers(f, sclct);

				//BYTE ������������ 32b
				WriteRoundup32Offset(f);

				//BYTE[] ������ ���������� ��� � ������ ����� (���������� �������� * sizeof(WORD))
				for (i = 0; i < (int)sclct; i++)
				{
				   TCollideChainShape *sh = (TCollideChainShape*)objManager->GetObjByIndex(TObjManager::COLLIDESHAPE, i);
				   w_val = (short)sh->GetNodesCount();
				   fwrite(&w_val, 1, sizeof(short), f);
				}

				//BYTE ������������ 32b
				WriteRoundup32Offset(f);

				// ������ ���� ��� ���� ������ ��� ������������� �������, �� �������
				for (i = 0; i < (int)sclct; i++)
				{
					TCollideChainShape *sh = (TCollideChainShape*)objManager->GetObjByIndex(TObjManager::COLLIDESHAPE, i);
					const TCollideChainShape::TCollideNodeList& nlist = sh->GetNodeList();
					for(TCollideChainShape::TCollideNodeList::const_iterator it = nlist.begin(); it != nlist.end(); ++it)
					{
						fx32 fx_val;
						double cnx, cny;
						it->getCoordinates(&cnx, &cny);
						//DWORD			x ���� (��������� �� �����)
						fx_val = FX32(cnx);
						writeFx(&fx_val, f);
						//DWORD			y ����
						fx_val = FX32(cny);
						writeFx(&fx_val, f);
					}
				}
			}
		}
		//BYTE          �������� ����
		ch_val[0] = (char)objManager->startZone;
		fwrite(ch_val, 1, 1, f);
		//WORD          id ���������� ������� (������������ � ������ ������)
		w_val = (unsigned short)objManager->GetScriptID(objManager->StartScriptName);
		fwrite(&w_val, sizeof(unsigned short), 1, f);
		//WORD          ���� >=0 ��������� ������ �� ���������� ����� (��� �������� � �������)
		w_val = (short)objManager->tileCameraPosOnStart;
		fwrite(&w_val, sizeof(short), 1, f);

		//WORD ����������� �����, ��������� ������
		w_val = CS_MAGIC_NUMBER;
		fwrite(&w_val, sizeof(short), 1, f);
	}

	fclose(f);
	f = NULL;

	//-------------
	//����� � ��������:
	//-------------
	{
		TStringList *langs = new TStringList();
		TStringList *txtnames = new TStringList();
		mpTexts->GetLanguages(langs);
		mpTexts->GetNames(txtnames);
		for (m = 0; m < langs->Count; m++)
		{
			if(!langs->Strings[m].IsEmpty())
			{
				nfile = map_file_name + _T("\\t") + IntToStr(m);
				if(f != NULL)
				{
					fclose(f);
					f = NULL;
				}
				if ((f = _wfopen(nfile.c_str(), _T("wb"))) == NULL)
				{
					break;
				}
				//WORD			���������� ��������� ��������
				i = w_val = (short)txtnames->Count;
				fwrite(&w_val, sizeof(short), 1, f);
				if(i > 0)
				{
					//BYTE ������������ 32b
					WriteRoundup32Offset(f);
					// BYTE[]			������ ������ ��� ������ ���������� �� ��������� ������� (���������� ��������� �������� * sizeof(void*))
					WriteRoundup32ReserveForPointers(f, i);

					//BYTE ������������ 32b
					WriteRoundup32Offset(f);
					//BYTE[]			������ ������ ��� ������ ���������� ����� (���������� ��������� �������� * sizeof(wchar))
					j = i * sizeof(short);
					unsigned char* achptr = new unsigned char[j];
					memset(achptr, 0, j);
					fwrite(achptr, 1, j, f);
					delete[] achptr;

					for(__int32 sci = 0; sci < i; sci++)
					{
						const TUnicodeText* ptxt = mpTexts->GetText(txtnames->Strings[sci], langs->Strings[m]);
						assert(ptxt);
						assert(ptxt->GetTextData());
						//WORD			���������� �����
						w_val = (short)ptxt->GetTextData()->mStringsCount;
						fwrite(&w_val, sizeof(short), 1, f);

						//BYTE ������������ 32b
						WriteRoundup32Offset(f);

						//BYTE[]		������ ������ ��� ������ ���������� �� ��������� ������ (���������� ����� * sizeof(void*))
						j = ptxt->GetTextData()->mStringsCount * SIZEOF_PTR;
						achptr = new unsigned char[j];
						memset(achptr, 0, j);
						fwrite(achptr, 1, j, f);
						delete[] achptr;
						for(__int32 tsi = 0; tsi < ptxt->GetTextData()->mStringsCount; tsi++)
						{
							//BYTE ������������ 32b
							WriteRoundup32Offset(f);

							//BYTE[]		������ ��������� ������ + 2 ������ ����� ��������������� ������� ��� ������ ������ + 1 ��� 0-�����������
							if(wcharsize == sizeof(wchar_t))
							{
								if(ptxt->GetTextData()->mppStrings[tsi] == NULL)
								{
									dw_val = 0;
								}
								else
								{
									dw_val = static_cast<short>(wcslen(ptxt->GetTextData()->mppStrings[tsi]));
								}
								fwrite(&dw_val, sizeof(short), 1, f);
							}
							else
							{
								if(ptxt->GetTextData()->mppStrings[tsi] == NULL)
								{
									dw_val = 0;
								}
								else
								{
									dw_val = static_cast<unsigned __int32>(wcslen(ptxt->GetTextData()->mppStrings[tsi]));
								}
								fwrite(&dw_val, sizeof(unsigned __int32), 1, f);
							}
							if(dw_val > 0)
							{
								if(wcharsize == sizeof(wchar_t))
								{
									fwrite(ptxt->GetTextData()->mppStrings[tsi], sizeof(wchar_t), dw_val + 1, f);
								}
								else
								{
									unsigned __int32 *t4ptr = new unsigned __int32[wcharsize * (dw_val + 1)];
									const wchar_t *t2ptr = ptxt->GetTextData()->mppStrings[tsi];
									unsigned __int32 *t_t4ptr = t4ptr;
									while(*t2ptr != L'\0')
									{
										*t4ptr = *t2ptr;
										++t4ptr;
										++t2ptr;
									}
									*t4ptr = L'\0';
									fwrite(t_t4ptr, wcharsize, dw_val + 1, f);
									delete[] t_t4ptr;
								}
							}
						}
					}
				}
			}
		}
		delete langs;
		delete txtnames;
	}

	FWait->Enabled = false;
	FWait->Close();

#endif
}
//---------------------------------------------------------------------------
