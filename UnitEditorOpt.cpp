//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UnitEditorOpt.h"
#include "inifiles.hpp"
#include "UObjCode.h"
#include "MainUnit.h"
#include "UStateList.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"

TFEditorOptions *FEditorOptions;

//---------------------------------------------------------------------------

GraphicsTypes GlobalGraphicsType = GRAPHICS_TYPE_BMP;
SoundTypes GlobalSoundType = SOUND_TYPE_WAV;
FontTypes GlobalFontType = FONT_TYPE_FNT;
static const __int32  TEXT_CANVAS_DEF_W = 256;
static const __int32  TEXT_CANVAS_DEF_H = 192;
const TColor TFEditorOptions::DEF_PATTERN_COLOR = clWhite;
const TColor TFEditorOptions::DEF_GRID_COLOR = clSilver;

//---------------------------------------------------------------------------
__fastcall TFEditorOptions::TFEditorOptions(TComponent* Owner)
        : TForm(Owner)
{
	CEd_sz = new TSpinEdit(GroupBoxView);
	CEd_sz->Parent = GroupBoxView;
	CEd_sz->Left = 277;
	CEd_sz->Top = 24;
	CEd_sz->Width = 68;
	CEd_sz->Height = 22;
	CEd_sz->MaxValue = 128;
	CEd_sz->MinValue = 24;
	CEd_sz->TabOrder = 0;
	CEd_sz->Visible = true;
	CEd_anim_duration = new TSpinEdit(GroupBoxAnim);
	CEd_anim_duration->Parent = GroupBoxAnim;
	CEd_anim_duration->Left = 277;
	CEd_anim_duration->Top = 24;
	CEd_anim_duration->Width = 68;
	CEd_anim_duration->Height = 22;
	CEd_anim_duration->MaxValue = 10000;
	CEd_anim_duration->MinValue = 16;
	CEd_anim_duration->TabOrder = 0;
	CEd_anim_duration->Visible = true;
	CSE_dispW = new TSpinEdit(GroupBox1);
	CSE_dispW->Parent = GroupBox1;
	CSE_dispW->Left = 91;
	CSE_dispW->Top = 21;
	CSE_dispW->Width = 68;
	CSE_dispW->Height = 22;
	CSE_dispW->MaxValue = 65535;
	CSE_dispW->MinValue = 1;
	CSE_dispW->TabOrder = 0;
	CSE_dispW->Value = 256;
	CSE_dispW->Visible = true;
	CSE_dispH = new TSpinEdit(GroupBox1);
	CSE_dispH->Parent = GroupBox1;
	CSE_dispH->Left = 91;
	CSE_dispH->Top = 49;
	CSE_dispH->Width = 68;
	CSE_dispH->Height = 22;
	CSE_dispH->MaxValue = 65535;
	CSE_dispH->MinValue = 1;
	CSE_dispH->TabOrder = 1;
	CSE_dispH->Value = 192;
	CSE_dispH->Visible = true;
	mCurrentPatternColor = DEF_PATTERN_COLOR;
	PatternPreviewRepaint();

	Localization::Localize(this);
}
//---------------------------------------------------------------------------

bool __fastcall TFEditorOptions::SaveOptions()
{
	TIniFile *ini;
	TMemoryStream *s;
	TFx32size fx32size;
	ini = new TIniFile(Form_m->PathIni + _T("editor.ini"));
	ini->WriteString(_T("Options"), _T("Language"), Form_m->Lang);
	ini->WriteInteger(_T("Options"), _T("lvWHSizeIcon"), CEd_sz->Value);
	ini->WriteInteger(_T("Options"), _T("aniDuration"), CEd_anim_duration->Value);
	fx32size.setType(static_cast<TFx32size::TFx32Type>(ComboBoxFx32->ItemIndex));
	ini->WriteString(_T("Options"), _T("fx32"), fx32size.getTypeName());
	ini->WriteInteger(_T("Options"), _T("wchar_t"), ((ComboBoxWchar->ItemIndex == 0) ? 4 : 2));
	ini->WriteInteger(_T("Options"), _T("DispH"), CSE_dispH->Value);
	ini->WriteInteger(_T("Options"), _T("BgColor"), (unsigned __int32 )mCurrentPatternColor);
	ini->WriteInteger(_T("Options"), _T("gridColor"), static_cast<int>(mCurrentGridColor));
	ini->WriteString(_T("Options"), _T("GraphicsType"), ((GlobalGraphicsType == GRAPHICS_TYPE_BMP) ? _T("BMP") : _T("PNG")));
	ini->WriteString(_T("Options"), _T("SoundType"), ((GlobalSoundType == SOUND_TYPE_WAV) ? _T("WAV") : _T("WAV")));
	ini->WriteInteger(_T("TxtEditor"), _T("DispW"), CSE_dispW->Value);
	ini->WriteInteger(_T("TxtEditor"), _T("DispH"), CSE_dispH->Value);
	delete ini;
	return true;
}
//---------------------------------------------------------------------------

void __fastcall TFEditorOptions::GetLanguages(TStringList* list)
{
	UnicodeString str;
	list->Clear();
	for(__int32  i = 0; i < MAX_LANGUAGES; i++)
	{
		if(ListBoxLang->Checked[i])
		{
			str = ListBoxLang->Items->Strings[i];
		}
		else
		{
			str = _T("");
		}
		list->Add(str);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditorOptions::AssignLanguages(TStringList* list)
{
	UnicodeString str;
	ListBoxLang->Items->Clear();
	for(__int32  i = 0; i < MAX_LANGUAGES; i++)
	{
		if(list->Count > i)
		{
			str = list->Strings[i];
		}
		else
		{
            str = _T("");
        }
		if(!str.IsEmpty())
		{
			ListBoxLang->Items->Add(str);
			ListBoxLang->Checked[i] = true;
		}
		else
		{
			if(i == 0)
			{
				ListBoxLang->Items->Add(UnicodeString(TEXT_NAME_DEFAULT));
				ListBoxLang->Checked[0] = true;
			}
			else
			{
				ListBoxLang->Items->Add(UnicodeString(TEXT_NAME_LANG) + IntToStr(i));
				ListBoxLang->Checked[i] = false;
			}
		}
	}
}
//---------------------------------------------------------------------------

bool __fastcall TFEditorOptions::LoadOptions()
{
	TIniFile *ini;
	TMemoryStream *s;
	wchar_t *buf;

	ini = new TIniFile(Form_m->PathIni + _T("editor.ini"));
	CEd_sz->Value = ini->ReadInteger(_T("Options"), _T("lvWHSizeIcon"), 32);
	CEd_anim_duration->Value = ini->ReadInteger(_T("Options"), _T("aniDuration"), DEFAULT_TICK_DURATION);
	{
		 __int32  gr_type_idx = 0;
		 UnicodeString gr_type = _T("PNG");
		 gr_type = ini->ReadString(_T("Options"), _T("GraphicsType"), _T("PNG")).Trim();
		 if(gr_type.Compare(_T("BMP")) == 0)
		 {
			gr_type_idx = 0;
			GlobalGraphicsType = GRAPHICS_TYPE_BMP;
		 }
		 if(gr_type.Compare(_T("PNG")) == 0)
		 {
			gr_type_idx = 1;
			GlobalGraphicsType = GRAPHICS_TYPE_PNG;
		 }
		 ComboBox_GrType->ItemIndex = gr_type_idx;
	}
	{
		 __int32  snd_type_idx = 0;
		 UnicodeString snd_type = _T("WAV");
		 snd_type = ini->ReadString(_T("Options"), _T("SoundType"), _T("WAV")).Trim();
		 if(snd_type.Compare(_T("WAV")) == 0)
		 {
			snd_type_idx = 0;
			GlobalSoundType = SOUND_TYPE_WAV;
		 }
		 ComboBox_SndType->ItemIndex = snd_type_idx;
	}
	{
		TFx32size fx32size;
		fx32size.setTypeByName(ini->ReadString(_T("Options"), _T("fx32"), ""));
		ComboBoxFx32->ItemIndex = static_cast<int>(fx32size.getType());
	}
	const __int32 wcharsize = ini->ReadInteger(_T("Options"), _T("wchar_t"), 4);
	if(wcharsize == 4)
	{
		ComboBoxWchar->ItemIndex = 0;
	}
	else
	{
		ComboBoxWchar->ItemIndex = 1;
	}
	mCurrentPatternColor = (TColor)ini->ReadInteger(_T("Options"), _T("BgColor"), (unsigned __int32 )DEF_PATTERN_COLOR);
	mCurrentGridColor = static_cast<TColor>(ini->ReadInteger(_T("Options"), _T("gridColor"), static_cast<int>(DEF_GRID_COLOR)));
	CSE_dispW->Value = ini->ReadInteger(_T("TxtEditor"), _T("DispW"), TEXT_CANVAS_DEF_W);
	CSE_dispH->Value = ini->ReadInteger(_T("TxtEditor"), _T("DispH"), TEXT_CANVAS_DEF_H);
	delete ini;
	{
        ComboBoxLang->Clear();
		TStringList* llist = new TStringList();
		Localization::GetLangAliasList(llist);
		__int32 activeIdx;
		for(__int32 i = 0; i < llist->Count; i++)
		{
			ComboBoxLang->AddItem(Localization::GetLangName(llist->Strings[i]), (TObject*)new UnicodeString(llist->Strings[i]));
		}
		activeIdx = llist->IndexOf(Form_m->Lang);
		if(activeIdx == -1)
		{
			Form_m->Lang = INTERFACE_LANG_DEFAULT;
			activeIdx = llist->IndexOf(Form_m->Lang);
		}
		ComboBoxLang->ItemIndex = activeIdx;
		delete llist;
	}
	PatternPreviewRepaint();
	return true;
}
//---------------------------------------------------------------------------

void __fastcall TFEditorOptions::ComboBox_GrTypeChange(TObject */*Sender*/)
{
	if(ComboBox_GrType->Text.Compare(_T("BMP")) == 0)
	{
		GlobalGraphicsType = GRAPHICS_TYPE_BMP;
	}
	else
	if(ComboBox_GrType->Text.Compare(_T("PNG")) == 0)
	{
		GlobalGraphicsType = GRAPHICS_TYPE_PNG;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditorOptions::ComboBox_SndTypeChange(TObject *)
{
	if(ComboBox_SndType->Text.Compare(_T("WAV")) == 0)
	{
		GlobalSoundType = SOUND_TYPE_WAV;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditorOptions::Button1Click(TObject */*Sender*/)
{
	__int32  i;
	TStringList* str = new TStringList();
	TList* ch = new TList();
	Application->CreateForm(__classid(TFStateList),&FStateList);
	FStateList->Caption = Localization::Text(_T("mLocalizations"));
	FStateList->SetInfo(Localization::Text(_T("mLocalizationDescription")));
	for(i = 0; i < ListBoxLang->Items->Count; i++)
	{
		ch->Add((void*)ListBoxLang->Checked[i]);
	}
	str->Assign(ListBoxLang->Items);
	FStateList->SetLanguageTypes(str);
	FStateList->Bt_add->Enabled = false;
	FStateList->Bt_del->Enabled = false;
	if(FStateList->ShowModal() == mrOk)
	{
		FStateList->GetLanguageTypes(str, NULL);
		ListBoxLang->Items->Assign(str);
	}
	FStateList->Free();
	FStateList = NULL;
	delete str;
	for(i = 0; i < ListBoxLang->Items->Count; i++)
	{
		ListBoxLang->Checked[i] = ch->Items[i];
	}
	delete ch;
}
//---------------------------------------------------------------------------

void __fastcall TFEditorOptions::ListBoxLangClickCheck(TObject */*Sender*/)
{
	if(ListBoxLang->Items->Count > 0)
	{
		if(ListBoxLang->Checked[0] == false)
		{
			ListBoxLang->Checked[0] = true;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditorOptions::FormDestroy(TObject *)
{
	delete CEd_sz;
	delete CEd_anim_duration;
	delete CSE_dispW;
	delete CSE_dispH;
	delete mpBackPatternBmp;
	for(__int32 i = 0; i < ComboBoxLang->Items->Count; i++)
	{
		delete (UnicodeString*)ComboBoxLang->Items->Objects[i];
    }
}
//---------------------------------------------------------------------------

void __fastcall TFEditorOptions::ButtonBTCClick(TObject *button)
{
	if (ColorDialog->Execute())
	{
		if(button == ButtonGC)
		{
			mCurrentGridColor = ColorDialog->Color;
			PatternPreviewRepaint();
		}
		else if(button == ButtonBTC)
		{
			mCurrentPatternColor = ColorDialog->Color;
			PatternPreviewRepaint();
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditorOptions::PatternPreviewRepaint()
{
	TCanvas *c = Image->Canvas;

	c->Brush->Bitmap = NULL;
	delete mpBackPatternBmp;
	mpBackPatternBmp = Form_m->CreatePatternImage(mCurrentPatternColor);
	c->Brush->Bitmap = const_cast<Graphics::TBitmap*>(mpBackPatternBmp->GetBitmapData());
	c->Rectangle(0, 0, Image->Width, Image->Height);

	c->Pen->Color = mCurrentGridColor;
	c->Pen->Mode = pmCopy; /*pmNotCopy;*/
	c->Pen->Style = psSolid;

	int d_w = Image->Width / 3;
	int d_h = d_w;
	int beg = -Image->Width % d_w;
	int step = d_w;
	for (int i = beg; i <= Image->Width; i += step)
	{
		c->MoveTo(i, 0);
		c->LineTo(i, Image->Height);
	}
	beg = -Image->Height % d_h;
	step = d_h;
	for (int i = beg; i <= Image->Height; i += step)
	{
		c->MoveTo(0, i);
		c->LineTo(Image->Width, i);
	}
}
//---------------------------------------------------------------------------

TColor __fastcall TFEditorOptions::GetPatternColor()
{
	return mCurrentPatternColor;
}
//---------------------------------------------------------------------------

TColor __fastcall TFEditorOptions::GetGridColor()
{
	return mCurrentGridColor;
}
//---------------------------------------------------------------------------

