
#pragma hdrstop

#include "pugiconfig.hpp"
#include "pugixml.hpp"
#include "USaveLoadStreamData.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

static int gsIODebugChecksum = 29346520;

//---------------------------------------------------------------------------

class TAbstractXMLNode
{
friend class TAbstractXMLDocument;

public:

	__fastcall TAbstractXMLNode()
	{
	}

	bool __fastcall FindNode(const UnicodeString& fieldName, TAbstractXMLNode& n)
	{
		n.node = node.child(to_utf8String(fieldName));
		return !n.node.empty();
	}

	void __fastcall AddChild(const UnicodeString& fieldName, TAbstractXMLNode &n)
	{
		pugi::xml_node t = node;
		n.node = t.append_child(to_utf8String(fieldName));
	}

	template <typename T>
	void __fastcall SetAttribute(const UnicodeString& fieldName, T v)
	{
		node.append_attribute(to_utf8String(fieldName)).set_value(v);
	}

	void __fastcall SetAttribute(const UnicodeString& fieldName, UnicodeString v)
	{
		node.append_attribute(to_utf8String(fieldName)).set_value(to_utf8String(v));
	}

	template <typename T>
	bool __fastcall HasAttribute(const UnicodeString& fieldName, T& v)
	{
		pugi::xml_attribute attr = node.attribute(to_utf8String(fieldName));
		if(!attr.empty())
		{
			if(std::is_integral<T>::value)
			{
				v = static_cast<T>(attr.as_int());
				return true;
			}
			else if(std::is_floating_point<T>::value)
			{
				v = static_cast<T>(attr.as_double());
				return true;
			}
		}
		return false;
	}

	bool __fastcall HasAttribute(const UnicodeString& fieldName, UnicodeString& v)
	{
		pugi::xml_attribute attr = node.attribute(to_utf8String(fieldName));
		if(!attr.empty())
		{
			const char* utf8String = attr.value();
			v = UnicodeString(pugi::as_wide(utf8String).c_str());
			return true;
		}
        return false;
	}

private:

	const char* to_utf8String(const UnicodeString& str)
	{
		utf8String = pugi::as_utf8(str.c_str());
		return utf8String.c_str();
	}

	std::string utf8String;
	pugi::xml_node node;
};
//---------------------------------------------------------------------------

class TAbstractXMLDocument
{
public:

	__fastcall TAbstractXMLDocument(const UnicodeString& createRootNode = _T(""))
	{
		if(!createRootNode.IsEmpty())
		{
			doc.append_child(to_utf8String(createRootNode));
		}
	}

	bool __fastcall GetRootNode(TAbstractXMLNode &n)
	{
		n.node = doc.first_child();
		return !n.node.empty();
	}

	bool __fastcall Save(const UnicodeString& fileName)
	{
		if(!fileName.IsEmpty())
		{
			return doc.save_file(fileName.c_str(), PUGIXML_TEXT("\t"), pugi::format_default, pugi::xml_encoding::encoding_utf8);
		}
		return false;
	}

	bool __fastcall Load(const UnicodeString& fileName)
	{
		if(!fileName.IsEmpty())
		{
			pugi::xml_parse_result result = doc.load_file(fileName.c_str());
			return result.status == pugi::status_ok;
		}
        return false;
	}

private:

	const char* to_utf8String(const UnicodeString& str)
	{
		utf8String = pugi::as_utf8(str.c_str());
		return utf8String.c_str();
	}

	std::string utf8String;
	pugi::xml_document doc;
};

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


__fastcall TSaveLoadStreamHandle::TSaveLoadStreamHandle()
: nodeInstance(nullptr)
{
	stream.f = nullptr;
}
//---------------------------------------------------------------------------

__fastcall TSaveLoadStreamHandle::~TSaveLoadStreamHandle()
{
	if(nodeInstance != nullptr)
	{
		delete nodeInstance;
		nodeInstance = nullptr;
	}
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


__fastcall TSaveLoadStreamData::TSaveLoadStreamData(const TSaveLoadStreamDataInit& initData, bool debugmode)
: _fileStream(nullptr)
, _inited(false)
{
	_debugmode = debugmode;
	_d = initData;
	_xmlDoc = nullptr;
   switch(_d.type)
   {
	   case TSLDataType::SaveAsBinary:
			_writeFn = SaveToFileFn;
			_readFn = nullptr;
	   break;
	   case TSLDataType::LoadAsBinary:
			_writeFn = nullptr;
			_readFn = LoadFromFileFn;
	   break;
	   case TSLDataType::ExportToXML:
			_xmlDoc = new TAbstractXMLDocument(_T("MapEditorProject"));
			_writeFn = SaveToFileXMLFn;
			_readFn = nullptr;
	   break;
	   case TSLDataType::SaveProject:
			_xmlDoc = new TAbstractXMLDocument(_T("MapEditorProject"));
			_writeFn = SaveToFileXMLFn;
			_readFn = nullptr;
	   break;
	   case TSLDataType::LoadProject:
			_xmlDoc = new TAbstractXMLDocument(_T("MapEditorProject"));
			_writeFn = nullptr;
			_readFn = LoadFromFileXMLFn;
	   break;
	   case TSLDataType::CopyClipboard:
			_writeFn = SaveStreamFn;
			_readFn = nullptr;
	   break;
	   case TSLDataType::PasteClipboard:
			_writeFn = nullptr;
			_readFn = LoadStreamFn;
	   break;
		   default:
		   assert(0);
	   break;
   }
}
//---------------------------------------------------------------------------

__fastcall TSaveLoadStreamData::~TSaveLoadStreamData()
{
	Release();
}
//---------------------------------------------------------------------------

bool __fastcall TSaveLoadStreamData::Init()
{
	if(!_inited)
	{
	   switch(_d.type)
	   {
			case TSLDataType::SaveProject:
			case TSLDataType::ExportToXML:
				_inited = !_d.fileName.IsEmpty();
				return _inited;

			case TSLDataType::LoadProject:
				_inited = _xmlDoc->Load(_d.fileName);
				return _inited;

			case TSLDataType::SaveAsBinary:
				_fileStream = _wfopen(_d.fileName.c_str(), _T("wb"));
				_inited = _fileStream != nullptr;
				return _inited;

			case TSLDataType::LoadAsBinary:
				_fileStream = _wfopen(_d.fileName.c_str(), _T("rb"));
				_inited = _fileStream != nullptr;
				return _inited;

			case TSLDataType::CopyClipboard:
			case TSLDataType::PasteClipboard:
				_inited = _d.memoryStream != nullptr;
				return _inited;

			default:
				assert(0);
				break;
	   }
	}
    return false;
}
//---------------------------------------------------------------------------

bool __fastcall TSaveLoadStreamData::Save()
{
	bool res = false;
	if(_inited && (type == TSaveLoadStreamData::TSLDataType::SaveProject || type == TSaveLoadStreamData::TSLDataType::ExportToXML))
	{
		res = _xmlDoc->Save(_d.fileName);
	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall TSaveLoadStreamData::Release()
{
	if(_fileStream != nullptr)
	{
		fclose(_fileStream);
        _fileStream = nullptr;
	}
	if(_xmlDoc != nullptr)
	{
		delete _xmlDoc;
		_xmlDoc = nullptr;
    }
    _inited = false;
}
//---------------------------------------------------------------------------

unsigned int __fastcall TSaveLoadStreamData::SaveToFileFn(void *ptr, const TSLFieldType& ftype, UnicodeString fieldName, TSaveLoadStreamHandle& h)
{
	assert(!fieldName.IsEmpty());
	switch(ftype)
	{
		case TSLFieldType::ftNewSection:
			if(ptr == nullptr)
			{
                h.type = type;
				h.stream.f = _fileStream;
			}
			else
			{
				TSaveLoadStreamHandle* n = static_cast<TSaveLoadStreamHandle*>(ptr);
				assert(n != nullptr);
				h.type = type;
				h.stream.f = _fileStream;
			}
			return 0;

		case TSLFieldType::ftDebugCheckSum:
			if(_debugmode)
			{
				int checksum;
				assert(h.type == TSaveLoadStreamData::TSLDataType::SaveAsBinary && h.stream.f != nullptr);
				fwrite(&gsIODebugChecksum, sizeof(int), 1, h.stream.f);
			}
			return 0;

		case TSLFieldType::ftChar:
			assert(h.type == TSaveLoadStreamData::TSLDataType::SaveAsBinary && h.stream.f != nullptr);
			return fwrite(ptr, sizeof(char), 1, h.stream.f);

		case TSLFieldType::ftS16:
			assert(h.type == TSaveLoadStreamData::TSLDataType::SaveAsBinary && h.stream.f != nullptr);
			return fwrite(ptr, sizeof(short), 1, h.stream.f);

		case TSLFieldType::ftU16:
			assert(h.type == TSaveLoadStreamData::TSLDataType::SaveAsBinary && h.stream.f != nullptr);
			return fwrite(ptr, sizeof(unsigned short), 1, h.stream.f);

		case TSLFieldType::ftS32:
			assert(h.type == TSaveLoadStreamData::TSLDataType::SaveAsBinary && h.stream.f != nullptr);
			return fwrite(ptr, sizeof(__int32), 1, h.stream.f);

		case TSLFieldType::ftDouble:
			assert(h.type == TSaveLoadStreamData::TSLDataType::SaveAsBinary && h.stream.f != nullptr);
			return fwrite(ptr, sizeof(double), 1, h.stream.f);

		case TSLFieldType::ftAnsiStringArray:
		case TSLFieldType::ftWideStringArray:
		case TSLFieldType::ftDataArray:
			{
				assert(h.type == TSaveLoadStreamData::TSLDataType::SaveAsBinary && h.stream.f != nullptr);
				TSaveLoadStreamCharArray* a = static_cast<TSaveLoadStreamCharArray*>(ptr);
				assert(a->ptr);
				return fwrite(a->ptr, 1, a->size, h.stream.f);
			}
		default:
			assert(0);
			break;
	}
    return 0;
}
//---------------------------------------------------------------------------

unsigned int __fastcall TSaveLoadStreamData::LoadFromFileFn(void *ptr, const TSLFieldType& ftype, UnicodeString fieldName, TSaveLoadStreamHandle& h)
{
	assert(!fieldName.IsEmpty());
	switch(ftype)
	{
		case TSLFieldType::ftNewSection:
			if(ptr == nullptr)
			{
                h.type = type;
				h.stream.f = _fileStream;
			}
			else
			{
				TSaveLoadStreamHandle* n = static_cast<TSaveLoadStreamHandle*>(ptr);
				assert(n != nullptr);
				h.type = type;
				h.stream.f = _fileStream;
			}
			return 0;

		case TSLFieldType::ftDebugCheckSum:
			if(_debugmode)
			{
				int checksum;
				assert(h.type == TSaveLoadStreamData::TSLDataType::LoadAsBinary && h.stream.f != nullptr);
				fread(&checksum, sizeof(int), 1, h.stream.f);
				if(gsIODebugChecksum != checksum)
				{
					assert(gsIODebugChecksum == checksum);
				}
			}
			return 0;

		case TSLFieldType::ftChar:
			assert(h.type == TSaveLoadStreamData::TSLDataType::LoadAsBinary && h.stream.f != nullptr);
			return fread(ptr, sizeof(char), 1, h.stream.f);

		case TSLFieldType::ftS16:
			assert(h.type == TSaveLoadStreamData::TSLDataType::LoadAsBinary && h.stream.f != nullptr);
			return fread(ptr, sizeof(short), 1, h.stream.f);

		case TSLFieldType::ftU16:
			assert(h.type == TSaveLoadStreamData::TSLDataType::LoadAsBinary && h.stream.f != nullptr);
			return fread(ptr, sizeof(unsigned short), 1, h.stream.f);

		case TSLFieldType::ftS32:
			assert(h.type == TSaveLoadStreamData::TSLDataType::LoadAsBinary && h.stream.f != nullptr);
			return fread(ptr, sizeof(__int32), 1, h.stream.f);

		case TSLFieldType::ftDouble:
			assert(h.type == TSaveLoadStreamData::TSLDataType::LoadAsBinary && h.stream.f != nullptr);
			return fread(ptr, sizeof(double), 1, h.stream.f);

		case TSLFieldType::ftAnsiStringArray:
		case TSLFieldType::ftWideStringArray:
		case TSLFieldType::ftDataArray:
			{
				assert(h.type == TSaveLoadStreamData::TSLDataType::LoadAsBinary && h.stream.f != nullptr);
				TSaveLoadStreamCharArray* a = static_cast<TSaveLoadStreamCharArray*>(ptr);
				assert(a->ptr);
				return fread(a->ptr, 1, a->size, h.stream.f);
			}
		default:
			assert(0);
			break;
	}
    return 0;
}
//---------------------------------------------------------------------------

static void CharToHexString(unsigned char w, wchar_t *ochar)
{
	static const wchar_t digits[] = L"0123456789ABCDEF";
	size_t hex_len = sizeof(unsigned char) << 1;
	size_t i = 0;
	size_t j;
	for (j = (hex_len - 1) * 4 ; i < hex_len; ++i , j -= 4)
	{
		ochar[i] = digits[(w>>j) & 0x0f];
	}
	ochar[i] = L'\0';
}
//---------------------------------------------------------------------------

unsigned int __fastcall TSaveLoadStreamData::SaveToFileXMLFn(void *ptr, const TSLFieldType& ftype, UnicodeString fieldName, TSaveLoadStreamHandle& h)
{
	assert(!fieldName.IsEmpty());
	switch(ftype)
	{
		case TSLFieldType::ftNewSection:
			if(h.nodeInstance == nullptr)
			{
				h.nodeInstance = new TAbstractXMLNode();
				h.stream.node = h.nodeInstance;
			}
			h.type = type;
			if(ptr == nullptr)
			{
				if(!_xmlDoc->GetRootNode(*h.nodeInstance))
				{
					h.stream.node = nullptr;
				}
				else
				{
					h.stream.node->AddChild(fieldName, *h.stream.node);
                }
			}
			else
			{
				TSaveLoadStreamHandle* n = static_cast<TSaveLoadStreamHandle*>(ptr);
				assert(n != nullptr);
				n->stream.node->AddChild(fieldName, *h.stream.node);
			}
			return 0;

		case TSLFieldType::ftDebugCheckSum:
			return 0;

		case TSLFieldType::ftS16:
			assert((h.type == TSaveLoadStreamData::TSLDataType::ExportToXML ||
					 h.type == TSaveLoadStreamData::TSLDataType::SaveProject) && h.stream.node != nullptr);
			assert(ptr != nullptr);
			s16_val = *((short*)ptr);
			h.stream.node->SetAttribute(fieldName, s16_val);
			return sizeof(short);

		case TSLFieldType::ftU16:
			assert((h.type == TSaveLoadStreamData::TSLDataType::ExportToXML ||
					 h.type == TSaveLoadStreamData::TSLDataType::SaveProject) && h.stream.node != nullptr);
			assert(ptr != nullptr);
			u16_val = *((unsigned short*)ptr);
			h.stream.node->SetAttribute(fieldName, u16_val);
			return sizeof(unsigned short);

		case TSLFieldType::ftS32:
			assert((h.type == TSaveLoadStreamData::TSLDataType::ExportToXML ||
					 h.type == TSaveLoadStreamData::TSLDataType::SaveProject) && h.stream.node != nullptr);
			assert(ptr != nullptr);
			s32_val = *((__int32*)ptr);
			h.stream.node->SetAttribute(fieldName, s32_val);
			return sizeof(__int32);

		case TSLFieldType::ftChar:
			assert((h.type == TSaveLoadStreamData::TSLDataType::ExportToXML ||
					 h.type == TSaveLoadStreamData::TSLDataType::SaveProject) && h.stream.node != nullptr);
			assert(ptr != nullptr);
			ch_val = *((unsigned char*)ptr);
			h.stream.node->SetAttribute(fieldName, ch_val);
			return sizeof(unsigned char);

		case TSLFieldType::ftDouble:
			assert((h.type == TSaveLoadStreamData::TSLDataType::ExportToXML ||
					 h.type == TSaveLoadStreamData::TSLDataType::SaveProject) && h.stream.node != nullptr);
			assert(ptr != nullptr);
			f_val = *((double*)ptr);
			h.stream.node->SetAttribute(fieldName, f_val);
			return sizeof(double);

		case TSLFieldType::ftAnsiStringArray:
			/*{
				assert(ptr != nullptr);
				assert((h.type == TSaveLoadStreamData::TSLDataType::ExportToXML ||
						h.type == TSaveLoadStreamData::TSLDataType::SaveProject) && h.stream.node != nullptr);
				TSaveLoadStreamCharArray* a = static_cast<TSaveLoadStreamCharArray*>(ptr);
				assert(a->ptr);

				char* ch = (char*)a->ptr;
				wchar_t* t = new wchar_t[a->size + 1];
				for(int i = 0; i < a->size; i++)
				{
				  t[i] = ch[i];
				}
				t[a->size] = L'\0';

				h.stream.node->SetAttribute(fieldName, UnicodeString(t));

				delete[] t;

				return a->size;
			}*/
			assert(0);
			break;

		case TSLFieldType::ftWideStringArray:
			{
				assert(ptr != nullptr);
				assert((h.type == TSaveLoadStreamData::TSLDataType::ExportToXML ||
						h.type == TSaveLoadStreamData::TSLDataType::SaveProject) && h.stream.node != nullptr);
				TSaveLoadStreamCharArray* a = static_cast<TSaveLoadStreamCharArray*>(ptr);
				assert(a->ptr);
				h.stream.node->SetAttribute(fieldName, UnicodeString(static_cast<wchar_t*>(a->ptr)));
				return a->size;
			}

		case TSLFieldType::ftDataArray:
			{
				assert(ptr != nullptr);
				assert((h.type == TSaveLoadStreamData::TSLDataType::ExportToXML ||
						h.type == TSaveLoadStreamData::TSLDataType::SaveProject) && h.stream.node != nullptr);
				TSaveLoadStreamCharArray* a = static_cast<TSaveLoadStreamCharArray*>(ptr);
				assert(a->ptr);

				unsigned char* ch = static_cast<unsigned char*>(a->ptr);
				wchar_t* buf = new wchar_t[a->size * 3 + 1];
				size_t hex_c = 0;
				for(size_t i = 0; i < a->size; i++)
				{
					CharToHexString(ch[i], &buf[hex_c]);
					buf[hex_c + 2] = L' ';
					hex_c += 3;
				}
				buf[a->size * 3 - 1] = L'\0';

				h.stream.node->SetAttribute(fieldName, UnicodeString(buf));

				delete[] buf;

				/*
				int bounds[2];
				bounds[0] = 0;
				bounds[1] = a->size - 1;

				Variant varArray = VarArrayCreate(EXISTINGARRAY(bounds), varByte);
				void *varArrayData = VarArrayLock(varArray);

				memcpy(varArrayData, a->ptr, a->size);

				VarArrayUnlock(varArray);

				h.stream.node->SetAttribute(fieldName, varArray);

				VarClear(varArray);
				*/
				return a->size;
			}

		default:
			assert(0);
			break;
	}

	return 0;
}
//---------------------------------------------------------------------------

unsigned int __fastcall TSaveLoadStreamData::LoadFromFileXMLFn(void *ptr, const TSLFieldType& ftype, UnicodeString fieldName, TSaveLoadStreamHandle& h)
{
	assert(!fieldName.IsEmpty());
	switch(ftype)
	{
		case TSLFieldType::ftNewSection:
			if(h.nodeInstance == nullptr)
			{
				h.nodeInstance = new TAbstractXMLNode();
				h.stream.node = h.nodeInstance;
			}
			h.type = type;
			if(ptr == nullptr)
			{
				if(!_xmlDoc->GetRootNode(*h.nodeInstance))
				{
					h.stream.node = nullptr;
				}
				else
				{
					if(!h.stream.node->FindNode(fieldName, *h.stream.node))
					{
					   h.stream.node = nullptr;
					}
                }
			}
            else
			{
				TSaveLoadStreamHandle* n = static_cast<TSaveLoadStreamHandle*>(ptr);
				assert(n != nullptr);
				if(!n->stream.node->FindNode(fieldName, *h.stream.node))
				{
					h.stream.node = nullptr;
                }
			}
			return 0;

		case TSLFieldType::ftDebugCheckSum:
			return 0;

		case TSLFieldType::ftS16:
			assert(h.type == TSaveLoadStreamData::TSLDataType::LoadProject && h.stream.node != nullptr);
			assert(ptr != nullptr);
			if (h.stream.node->HasAttribute(fieldName, s16_val))
			{
				*((short*)ptr) = s16_val;
				return sizeof(short);
			}
			return 0;

		case TSLFieldType::ftU16:
			assert(h.type == TSaveLoadStreamData::TSLDataType::LoadProject && h.stream.node != nullptr);
			assert(ptr != nullptr);
			if (h.stream.node->HasAttribute(fieldName, u16_val))
			{
				*((unsigned short*)ptr) = u16_val;
				return sizeof(unsigned short);
			}
			return 0;

		case TSLFieldType::ftS32:
			assert(h.type == TSaveLoadStreamData::TSLDataType::LoadProject && h.stream.node != nullptr);
			assert(ptr != nullptr);
			if (h.stream.node->HasAttribute(fieldName, s32_val))
			{
				*((__int32*)ptr) = s32_val;
				return sizeof(__int32);
			}
			return 0;

		case TSLFieldType::ftChar:
			assert(h.type == TSaveLoadStreamData::TSLDataType::LoadProject && h.stream.node != nullptr);
			assert(ptr != nullptr);
			if (h.stream.node->HasAttribute(fieldName, ch_val))
			{
				*((unsigned char*)ptr) = ch_val;
				return sizeof(unsigned char);
			}
            return 0;

		case TSLFieldType::ftDouble:
			assert(h.type == TSaveLoadStreamData::TSLDataType::LoadProject && h.stream.node != nullptr);
			assert(ptr != nullptr);
			if (h.stream.node->HasAttribute(fieldName, f_val))
			{
				*((double*)ptr) = f_val;
				return sizeof(double);
			}
			return 0;

		case TSLFieldType::ftAnsiStringArray:
			assert(0);
			break;

		case TSLFieldType::ftWideStringArray:
			{
				assert(ptr != nullptr);
				assert(h.type == TSaveLoadStreamData::TSLDataType::LoadProject && h.stream.node != nullptr);
				UnicodeString s;
				if (h.stream.node->HasAttribute(fieldName, s))
				{
					TSaveLoadStreamCharArray* a = static_cast<TSaveLoadStreamCharArray*>(ptr);
					assert(a->ptr);
					unsigned int sz = s.Length() * sizeof(wchar_t);
					if(sz > a->size)
					{
						sz = a->size;
					}
					memcpy(a->ptr, s.c_str(), sz);
					return sz;
				}
				return 0;
			}

		case TSLFieldType::ftDataArray:
			{
				assert(ptr != nullptr);
				assert(h.type == TSaveLoadStreamData::TSLDataType::LoadProject && h.stream.node != nullptr);
				UnicodeString s;
				if (h.stream.node->HasAttribute(fieldName, s))
				{
					TSaveLoadStreamCharArray* a = static_cast<TSaveLoadStreamCharArray*>(ptr);
					assert(a->ptr);
					wchar_t ch[3];
					const size_t sz = s.Length();
					const wchar_t* data = s.c_str();
					unsigned char* buf = static_cast<unsigned char*>(a->ptr);
                    size_t i = 0;
					size_t pt_i = 0;
					ch[2] = '\0';
					while(i < sz && pt_i < a->size)
					{
						ch[0] = data[i]; i++;
						ch[1] = data[i]; i++;
						buf[pt_i] = static_cast<unsigned char>(wcstol(ch, nullptr, 16));
						pt_i++;
						if(data[i] != ' ')
						{
							break;
						}
						i++;
					}
					return a->size;
				}
				return 0;
			}

		default:
			assert(0);
			break;
	}

	return 0;
}
//---------------------------------------------------------------------------

unsigned int __fastcall TSaveLoadStreamData::SaveStreamFn(void *ptr, const TSLFieldType& ftype, UnicodeString fieldName, TSaveLoadStreamHandle& h)
{
	switch(ftype)
	{
		case TSLFieldType::ftNewSection:
			if(ptr == nullptr)
			{
				h.type = type;
				h.stream.memoryStream = _d.memoryStream;
			}
			else
			{
				TSaveLoadStreamHandle* n = static_cast<TSaveLoadStreamHandle*>(ptr);
				assert(n != nullptr);
				h.type = type;
				h.stream.memoryStream = _d.memoryStream;
			}
			return 0;

		case TSLFieldType::ftChar:
			assert(ptr != nullptr);
			assert(h.type == TSaveLoadStreamData::TSLDataType::CopyClipboard && h.stream.memoryStream != nullptr);
			return h.stream.memoryStream->Write(ptr, static_cast<System::LongInt>(sizeof(char)));

		case TSLFieldType::ftS16:
			assert(ptr != nullptr);
			assert(h.type == TSaveLoadStreamData::TSLDataType::CopyClipboard && h.stream.memoryStream != nullptr);
			return h.stream.memoryStream->Write(ptr, static_cast<System::LongInt>(sizeof(short)));

		case TSLFieldType::ftU16:
			assert(ptr != nullptr);
			assert(h.type == TSaveLoadStreamData::TSLDataType::CopyClipboard && h.stream.memoryStream != nullptr);
			return h.stream.memoryStream->Write(ptr, static_cast<System::LongInt>(sizeof(unsigned short)));

		case TSLFieldType::ftS32:
			assert(ptr != nullptr);
			assert(h.type == TSaveLoadStreamData::TSLDataType::CopyClipboard && h.stream.memoryStream != nullptr);
			return h.stream.memoryStream->Write(ptr, static_cast<System::LongInt>(sizeof(__int32)));

		case TSLFieldType::ftDouble:
			assert(ptr != nullptr);
			assert(h.type == TSaveLoadStreamData::TSLDataType::CopyClipboard && h.stream.memoryStream != nullptr);
			return h.stream.memoryStream->Write(ptr, static_cast<System::LongInt>(sizeof(double)));

		case TSLFieldType::ftAnsiStringArray:
		case TSLFieldType::ftWideStringArray:
		case TSLFieldType::ftDataArray:
			{
				assert(ptr != nullptr);
				assert(h.type == TSaveLoadStreamData::TSLDataType::CopyClipboard && h.stream.memoryStream != nullptr);
				TSaveLoadStreamCharArray* a = static_cast<TSaveLoadStreamCharArray*>(ptr);
				assert(a->ptr);
				return h.stream.memoryStream->Write(a->ptr, static_cast<System::LongInt>(a->size));
			}
		default:
			assert(0);
			break;
	}
	return 0;
}
//---------------------------------------------------------------------------

unsigned int __fastcall TSaveLoadStreamData::LoadStreamFn(void *ptr, const TSLFieldType& ftype, UnicodeString fieldName, TSaveLoadStreamHandle& h)
{
	switch(ftype)
	{
		case TSLFieldType::ftNewSection:
			if(ptr == nullptr)
			{
				h.type = type;
				h.stream.memoryStream = _d.memoryStream;
			}
			else
			{
				TSaveLoadStreamHandle* n = static_cast<TSaveLoadStreamHandle*>(ptr);
				assert(n != nullptr);
				h.type = type;
				h.stream.memoryStream = _d.memoryStream;
			}
			return 0;

		case TSLFieldType::ftChar:
			assert(ptr != nullptr);
			assert(h.type == TSaveLoadStreamData::TSLDataType::PasteClipboard && h.stream.memoryStream != nullptr);
			return h.stream.memoryStream->Read(ptr, static_cast<System::LongInt>(sizeof(char)));

		case TSLFieldType::ftS16:
			assert(ptr != nullptr);
			assert(h.type == TSaveLoadStreamData::TSLDataType::PasteClipboard && h.stream.memoryStream != nullptr);
			return h.stream.memoryStream->Read(ptr, static_cast<System::LongInt>(sizeof(short)));

		case TSLFieldType::ftU16:
			assert(ptr != nullptr);
			assert(h.type == TSaveLoadStreamData::TSLDataType::PasteClipboard && h.stream.memoryStream != nullptr);
			return h.stream.memoryStream->Read(ptr, static_cast<System::LongInt>(sizeof(unsigned short)));

		case TSLFieldType::ftS32:
			assert(ptr != nullptr);
			assert(h.type == TSaveLoadStreamData::TSLDataType::PasteClipboard && h.stream.memoryStream != nullptr);
			return h.stream.memoryStream->Read(ptr, static_cast<System::LongInt>(sizeof(__int32)));

		case TSLFieldType::ftDouble:
			assert(ptr != nullptr);
			assert(h.type == TSaveLoadStreamData::TSLDataType::PasteClipboard && h.stream.memoryStream != nullptr);
			return h.stream.memoryStream->Read(ptr, static_cast<System::LongInt>(sizeof(double)));

		case TSLFieldType::ftAnsiStringArray:
		case TSLFieldType::ftWideStringArray:
		case TSLFieldType::ftDataArray:
			{
				assert(ptr != nullptr);
				assert(h.type == TSaveLoadStreamData::TSLDataType::PasteClipboard && h.stream.memoryStream != nullptr);
				TSaveLoadStreamCharArray* a = static_cast<TSaveLoadStreamCharArray*>(ptr);
				assert(a->ptr);
				return h.stream.memoryStream->Read(a->ptr, static_cast<System::LongInt>(a->size));
			}
		default:
			assert(0);
			break;
	}
	return 0;
}
//---------------------------------------------------------------------------


