//---------------------------------------------------------------------------

#ifndef UDefPropH
#define UDefPropH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class GPersonProperties;
class TPropertiesControl;

class TFDefProperties : public TForm
{
__published:

	TStatusBar *StatusBar1;
	TScrollBox *ScrollBox;
	TButton *Button2;
	TButton *Button1;
	void __fastcall FormDestroy(TObject *Sender);

private:

	TPropertiesControl		*mpPropertiesControl;

	void __fastcall ClearScrollBox();
	
public:

	void __fastcall SetProperties(GPersonProperties* prp);
	void __fastcall GetProperties(GPersonProperties* prp);

	__fastcall TFDefProperties(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFDefProperties *FDefProperties;
//---------------------------------------------------------------------------
#endif
