//---------------------------------------------------------------------------

#ifndef UMapCollideLinesH
#define UMapCollideLinesH

#include <list>
#include "UTypes.h"
#include "UObjCode.h"

//---------------------------------------------------------------------------

class TCollideChainShape;

class TMapCollideLineNode : public TMapGPerson
{
	friend class TCollideChainShape;

 public:

	static const __int32 gscCollideLineNodeDefSize;

	// do not call it manually
	__fastcall TMapCollideLineNode(const TMapCollideLineNode& c);
	virtual __fastcall ~TMapCollideLineNode() override;

	void __fastcall CopyTo(TMapCollideLineNode *c) const;

	virtual void __fastcall setCoordinates(double x, double y, __int32 dx, __int32 dy) override;
	void __fastcall ApplyCoordinates();
	bool __fastcall GetPrevCoordinates(double& x, double& y);

	void __fastcall SetErrorMark();
	bool __fastcall IsErrorMark() const;

	const TCollideChainShape* __fastcall GetParent() const;

	virtual void __fastcall OnScrollBarChange() override;

 protected:

	__fastcall TMapCollideLineNode(double x, double y,
									__int32 *pos_x, __int32 *pos_y,
									UnicodeString ParentName,
									TObjManager* Owner);

	virtual void __fastcall inner_realign() override;
	virtual void __fastcall OnGlobalScale() override;
	virtual void __fastcall SetZone(__int32 /*zone*/) override {}
	virtual void __fastcall ShowRect(bool /*val*/) override {}
	virtual void __fastcall ClearSizes() override {};

    TTransformProperties mPrevTransform;
	bool mPrevPosExist;
	TDateTime mErrorMark;
	const TCollideChainShape* mpShapeParent;
};
//---------------------------------------------------------------------------

class TCollideChainShape : public TMapPerson
{
	friend class TObjManager;
	friend class TFObjOrder;
	friend class TForm_m;

public:

	typedef std::list<TMapCollideLineNode> TCollideNodeList;

	__fastcall TCollideChainShape();
	__fastcall TCollideChainShape(const TCollideChainShape& c);
	virtual __fastcall ~TCollideChainShape() override;

	void __fastcall CopyTo(TCollideChainShape *cns) const;

	TMapCollideLineNode& __fastcall CreateNode(double x, double y, __int32 *pos_x, __int32 *pos_y);
	bool __fastcall DeleteNode(const TMapCollideLineNode* n);
	__int32 __fastcall GetNodesCount() const;

	void __fastcall SetName(const UnicodeString& name);

	void __fastcall TrimNodes(const TRect& r);
	bool __fastcall IsPointOnNode(const double& x, const double& y,
									const TMapCollideLineNode* dragNode,
									const TMapCollideLineNode** on) const;
	bool __fastcall IsPointOnLine(double& x, double& y, const double& findRadius,
									const TMapCollideLineNode* dragNode,
									const TMapCollideLineNode** on1, const TMapCollideLineNode** on2) const;
	bool __fastcall GetPreMidNextNodes(const TMapCollideLineNode* orig_n,
										const TMapCollideLineNode** pre_n,
										const TMapCollideLineNode** mid_n,
										const TMapCollideLineNode** nex_n) const;
	bool __fastcall MarkIfCrossline(const TMapCollideLineNode* pre_n,
										const TMapCollideLineNode* mid_n,
										const TMapCollideLineNode* nex_n);
	bool __fastcall IsFirstNode(const TMapCollideLineNode* n) const;
	bool __fastcall IsLastNode(const TMapCollideLineNode* n) const;
	bool __fastcall MoveNode(const UnicodeString& nodeName, const UnicodeString& toNodeName);

	virtual void __fastcall OnScrollBarChange() override;
	virtual void __fastcall SetVisible(bool val) override;
	virtual void __fastcall SetCustomVisible(bool val) override;

private:

	__fastcall TCollideChainShape(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString GPName, TObjManager* Owner);
	__fastcall TCollideChainShape(TObjManager* Owner);
	bool __fastcall Crossline(const TMapCollideLineNode* dragNode,
									const TMapCollideLineNode* fixedPrevNode,
									const TMapCollideLineNode* fixedNextNode) const;
	const TCollideNodeList& __fastcall GetNodeList() const;
	void __fastcall CopyNodeList(TList* list) const;
	TMapCollideLineNode* __fastcall FindNode(const UnicodeString& nodeName);

	virtual void __fastcall OnGlobalScale() override;

	virtual void __fastcall setCoordinates(double , double , __int32 , __int32 ) override {
	assert(0);
	}
	virtual void __fastcall inner_realign() override {}
	virtual void __fastcall setRemarks(UnicodeString ) override {}
	virtual void __fastcall ShowRect(bool ) override {}
	virtual void __fastcall SetZone(__int32 ) override {}
	virtual void __fastcall ClearSizes() override {};

	TCollideNodeList mNodes;
};

//---------------------------------------------------------------------------

#endif