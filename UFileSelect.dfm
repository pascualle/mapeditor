object FFileSelect: TFFileSelect
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  ClientHeight = 386
  ClientWidth = 244
  Color = clBtnFace
  Constraints.MinHeight = 380
  Constraints.MinWidth = 240
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object FileListBox: TFileListBox
    Left = 0
    Top = 0
    Width = 244
    Height = 337
    Align = alClient
    ItemHeight = 13
    TabOrder = 0
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 367
    Width = 244
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    ParentFont = True
    UseSystemFont = False
  end
  object PanelBt: TPanel
    Left = 0
    Top = 337
    Width = 244
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    DoubleBuffered = True
    FullRepaint = False
    ParentDoubleBuffered = False
    TabOrder = 2
    DesignSize = (
      244
      30)
    object Button1: TButton
      Left = 118
      Top = 2
      Width = 98
      Height = 25
      Anchors = [akLeft, akRight, akBottom]
      Caption = 'mApply'
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 28
      Top = 2
      Width = 76
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'mCancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
end
