//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainUnit.h"
#include "UStateList.h"
#include "UState.h"
#include "UBGOType.h"
#include "UEditObjPattern.h"
#include "UOppositeState.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TFStateList *FStateList;

enum StateListMode
{
  lmNONE                = 0,
  lmSTATE               = 1,
  lmBACKPART            = 2,
  lmOPPOSITE_TABLE      = 3,
  lmANIM_TYPES          = 4,
  lmEVENT_TYPES			= 5,
  lmLANGUAGE_TYPES		= 6
};

static const int addElement    = -10;

//---------------------------------------------------------------------------

__fastcall TFStateList::TFStateList(TComponent* Owner)
: TForm(Owner)
{
	list = new TList();
	changeList = new TStringList();
	listBox = new TStringList();
	mode = lmNONE;

	Localization::Localize(this);

	EdFindChange(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::SetList(TList *newlist, int mode)
{
	__int32 i;
	void *s,*d;
	UnicodeString str;

	if(newlist == NULL)
	{
		return;
	}
	if(mode == lmNONE)
	{
		return;
	}
	for(i = 0; i < list->Count; i++)
	{
		TListObject *obj = static_cast<TListObject*>(list->Items[i]);
		assert(obj->GetType() != LISTOBJ_Undef);
		delete obj;
	}
	list->Clear();
	changeList->Clear();
	for(i = 0; i < newlist->Count; i++)
	{
	  s = newlist->Items[i];
	  switch(mode)
	  {
			case lmSTATE:
					d = new TState();
					((TState*)s)->CopyTo((TState*)d);
					str = ((TState*)s)->name + _T("=") + ((TState*)s)->name;
					changeList->Add(str);
			break;

			case lmBACKPART:
					d = new TBGobjProperties();
					((TBGobjProperties*)s)->CopyTo(*((TBGobjProperties*)d));
					str = ((TBGobjProperties*)s)->name + _T("=") + ((TBGobjProperties*)s)->name;
					changeList->Add(str);
	  }
	  list->Add(d);
	}
	CreateList(_T(""));
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::GetList(TList *newlist, TStringList *changelist, int mode)
{
	int i;
	void *s,*d;
	if(newlist == NULL)
	{
		return;
	}
	if(mode == lmNONE)
	{
		return;
	}
	for(i = 0; i < newlist->Count; i++)
	{
		TListObject *obj = static_cast<TListObject*>(newlist->Items[i]);
		assert(obj->GetType() != LISTOBJ_Undef);
		delete obj;
	}
	newlist->Clear();
	for(i = 0; i < list->Count; i++)
	{
		s = list->Items[i];
		switch(mode)
		{
		 case lmSTATE:
		  d = new TState();
		  ((TState*)s)->CopyTo((TState*)d);
		 break;

		 case lmBACKPART:
		  d = new TBGobjProperties();
		  ((TBGobjProperties*)s)->CopyTo(*((TBGobjProperties*)d));
		 break;
		}
		newlist->Add(d);
	}
	if(changelist != NULL)
	{
		changelist->Assign(changeList);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::SetStates(TList *states)
{
	mode = lmSTATE;
	SetList(states, mode);
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::GetStates(TList *states, TStringList *changelist)
{
	GetList(states, changelist, mode);
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::CreateChangeList(TStringList *sl)
{
	assert(mode != lmNONE && mode != lmSTATE && mode != lmBACKPART);
	changeList->Clear();
	for(int i = 0; i < sl->Count; i++)
	{
		UnicodeString str = sl->Strings[i] + _T("=") + sl->Strings[i];
		changeList->Add(str);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::WriteDeleteToChangeList(UnicodeString value)
{
	for(int i = 0; i < changeList->Count; i++)
	{
		if(changeList->ValueFromIndex[i].Compare(value) == 0)
		{
		  changeList->Strings[i] = changeList->Names[i] + _T("=");
		  break;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::WriteAddToChangeList(UnicodeString value)
{
	if(changeList->IndexOfName(value) >= 0)
	{
		changeList->Values[value] = value;
	}
	else
	{
		changeList->Add(value + _T("=") + value);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::WriteRenameToChangeList(UnicodeString oldName, UnicodeString newName)
{
	for(int i = 0; i < changeList->Count; i++)
	{
		if(changeList->ValueFromIndex[i].Compare(oldName) == 0)
		{
			changeList->ValueFromIndex[i] = newName;
			break;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::SetOppositeStates(TStringList *sl)
{
	mode = lmOPPOSITE_TABLE;
	listBox->Assign(sl);
	CreateChangeList(sl);
	CreateList(_T(""));
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::GetOppositeStates(TStringList *sl, TStringList *changelist)
{
	sl->Assign(listBox);
	if(changelist != NULL)
	{
		changelist->Assign(changeList);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::SetAnimTypes(TStringList *sl)
{
	mode = lmANIM_TYPES;
	listBox->Assign(sl);
	CreateChangeList(sl);
	CreateList(_T(""));
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::GetAnimTypes(TStringList *sl, TStringList *changelist)
{
	sl->Assign(listBox);
	if(changelist != NULL)
	{
		changelist->Assign(changeList);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::SetEventTypes(TStringList *sl)
{
	mode = lmEVENT_TYPES;
	listBox->Assign(sl);
	CreateChangeList(sl);
	CreateList(_T(""));
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::GetEventTypes(TStringList *sl, TStringList *changelist)
{
	sl->Assign(listBox);
	if(changelist != NULL)
	{
		changelist->Assign(changeList);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::SetBackProperties(TList *bgproperties)
{
	mode = lmBACKPART;
	SetList(bgproperties, mode);
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::GetBackProperties(TList *bgproperties, TStringList *changelist)
{
	GetList(bgproperties, changelist, mode);
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::SetLanguageTypes(TStringList *sl)
{
	mode = lmLANGUAGE_TYPES;
	listBox->Assign(sl);
	CreateChangeList(sl);
	CreateList(_T(""));
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::GetLanguageTypes(TStringList *sl, TStringList *changelist)
{
	sl->Assign(listBox);
	if(changelist != NULL)
	{
		changelist->Assign(changeList);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::SetInfo(UnicodeString info)
{
	LabelInfo->Caption = info;
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::Add()
{
	Edit(addElement);
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::Edit(int idx)
{
  switch(mode)
  {
   case lmSTATE:
    EditState(idx);
   break;

   case lmBACKPART:
    EditBackProperties(idx);
   break;

   case lmOPPOSITE_TABLE:
    EditOppositeStates(idx);
   break;

   case lmANIM_TYPES:
    EditAnimTypes(idx);
   break;

   case lmEVENT_TYPES:
    EditEventTypes(idx);
   break;

   case lmLANGUAGE_TYPES:
	EditLangTypes(idx);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::EditState(int idx)
{
	int            pos;
	TState         *state;
	bool           res;
	UnicodeString     oldName;
	state = new TState();
	if(idx != addElement)
	{
		if(idx < 0)
		{
			return;
		}
		((TState*)list->Items[idx])->CopyTo(state);
	}
	else if(list->Count >= MAX_STATES)
	{
		Application->MessageBox(Localization::Text(_T("mMaximumTextsExceeded")).c_str(),
							Localization::Text(_T("mError")).c_str(),
							MB_OK | MB_ICONERROR);
		return;
	}
	oldName = state->name;
	Application->CreateForm(__classid(TFEditState), &FEditState);
	FEditState->SetState(state);//������������ � ������� ��������
	do
	{
		res = true;
		if(FEditState->ShowModal() == mrOk)
		{
		   FEditState->GetState(state);//�������� ���������� ������
		   if(state->name.IsEmpty())
		   {
			Application->MessageBox(Localization::Text(_T("mStateNameError")).c_str(),
								Localization::Text(_T("mError")).c_str(),
								MB_OK | MB_ICONERROR);
			res = false;
		   }
		   else
		   {
			for(pos=0; pos < list->Count; pos++)
			 if( ((TState*)list->Items[pos])->name == state->name && pos!=idx)
			 {
				Application->MessageBox(Localization::Text(_T("mItemNameExist")).c_str(),
									Localization::Text(_T("mError")).c_str(),
									MB_OK | MB_ICONERROR);
			  res = false;
			  break;
			 }
		   }

		   if(res)//���� ��� ���������
		   {
				if(idx == addElement)
				{
					list->Add(state);
					//������ ��� � changeList
					WriteAddToChangeList(state->name);
				}
				else
				{
					state->CopyTo(((TState*)list->Items[idx]));
					//������ ��� � changeList
					WriteRenameToChangeList(oldName, state->name);
				}
				CreateList(state->name);
		   }
		}
		else res = true;
	}
	while(!res);
	FEditState->Free();
	FEditState = NULL;
	if(state != NULL && idx != addElement)
	{
		delete state;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::EditBackProperties(int idx)
{
 int                    pos;
 TBGobjProperties       *prp;
 bool                   res;
 UnicodeString             oldName;

 prp = new TBGobjProperties();
 if(idx != addElement)
 {
   if(idx < 0)
   {
		return;
   }
   ((TBGobjProperties*)list->Items[idx])->CopyTo(*prp);
 }
 else if(list->Count >= DIRECTOR_TYPE)  //62 -- max 62*2 = 124, -125 - ��� �������� 
 {
	Application->MessageBox(Localization::Text(_T("mMaximumTextsExceeded")).c_str(),
						Localization::Text(_T("mError")).c_str(),
						MB_OK | MB_ICONERROR);
   return;
 }
 oldName = prp->name;
 Application->CreateForm(__classid(TFEditBGOType), &FEditBGOType);
 FEditBGOType->SetBackProperty(prp);//������������ � ������� ��������
 do{
	 res = true;
	 if(FEditBGOType->ShowModal() == mrOk)
	 {
	  FEditBGOType->GetBackProperty(prp);//�������� ���������� ������
	  if(prp->name.IsEmpty())
	  {
		Application->MessageBox(Localization::Text(_T("mBgObjectNameError")).c_str(),
							Localization::Text(_T("mError")).c_str(),
							MB_OK | MB_ICONERROR);
		res = false;
	  }
	  else
	  {
		for(pos = 0; pos<list->Count; pos++)
		{
		  TBGobjProperties* item = (TBGobjProperties*)list->Items[pos];
		  if( item->name.Compare( prp->name ) == 0 && pos != idx)
		  {
			Application->MessageBox(Localization::Text(_T("mItemNameExist")).c_str(),
								Localization::Text(_T("mError")).c_str(),
								MB_OK | MB_ICONERROR);
			res = false;
			break;
		  }
		  else
		  if(item->type == prp->type && pos!=idx)
		  {
			const UnicodeString str = Localization::Text(_T("mBgObjectWithThisParametersExist")) + _T(" ") + item->name;
			Application->MessageBox(str.c_str(),
								Localization::Text(_T("mError")).c_str(),
								MB_OK | MB_ICONERROR);
			res = false;
			break;
		  }
		}
      }

      if(res) //���� ��� ���������
      {
		if(idx == addElement)
		{
			list->Add(prp);
			WriteAddToChangeList(prp->name);
		}
		else
		{
			prp->CopyTo( *(((TBGobjProperties*)list->Items[idx])) );
			WriteRenameToChangeList(oldName, prp->name);
		}
		CreateList(prp->name);
      }
     }
     else res=true;
 }while(!res);
 FEditBGOType->Free();
 FEditBGOType = NULL;
 if(prp != NULL && idx != addElement)
	delete prp;
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::EditOppositeStates(int idx)
{
 if(idx != addElement)
 {
   if(idx < 0)return;
 }
 else if(listBox->Count >= MAX_STATES)
 {
	Application->MessageBox(Localization::Text(_T("mMaximumTextsExceeded")).c_str(),
						Localization::Text(_T("mError")).c_str(),
						MB_OK | MB_ICONERROR);
   return;
 }
 UnicodeString oldName = _T("\0");
 Application->CreateForm(__classid(TFOppState), &FOppState);
 if(idx != addElement)
 {
	oldName = listBox->Strings[idx];
	FOppState->SetStateStrigCondition(oldName);
 }
 if(FOppState->ShowModal() == mrOk)
 {
   UnicodeString s;
   FOppState->GetStateStrigCondition(&s);
   if(idx == addElement)
   {
	listBox->Add(s);
	WriteAddToChangeList(s);
   }
   else
   {
	listBox->Strings[idx] = s;
	WriteRenameToChangeList(oldName, s);
   }
   CreateList(s);
 }
 FOppState->Free();
 FOppState = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::EditAnimTypes(int idx)
{
 UnicodeString name;
 bool res;
 int  pos;

 if(idx != addElement)
 {
   if(idx < 0)return;
 }
 else if(listBox->Count >= MAX_ANIMTYPES)
 {
	Application->MessageBox(Localization::Text(_T("mMaximumTextsExceeded")).c_str(),
						Localization::Text(_T("mError")).c_str(),
						MB_OK | MB_ICONERROR);
   return;
 }

 res = true;

 UnicodeString oldName = _T("\0");
 Application->CreateForm(__classid(TFOPEdit),&FOPEdit);
 FOPEdit->ButtonOk->ModalResult = mrOk;
 FOPEdit->SetMode(TFOPEdit::TFOPEMAnimations);
 FOPEdit->Caption = Caption;
 if(idx != addElement)
 {
	oldName = listBox->Strings[idx];
	FOPEdit->SetVarName(oldName);
 }
 do
 {
   if(FOPEdit->ShowModal() == mrOk)
   {
     pos = -1;
     name = FOPEdit->GetVarName();
     if(!name.IsEmpty())
	 if((pos = listBox->IndexOf(name)) < 0)
	 {
	   if(idx == addElement)
	   {
		listBox->Add(name);
		WriteAddToChangeList(name);
	   }
	   else
	   {
		listBox->Strings[idx] = name;
		WriteRenameToChangeList(oldName, name);
	   }
	   CreateList(name);
	   res = false;
     }
     if(res && pos>=0)
	 {
		Application->MessageBox(Localization::Text(_T("mAnimationTypeNameExist")).c_str(),
							Localization::Text(_T("mError")).c_str(),
							MB_OK | MB_ICONERROR);
	 }
   }else res=false;
 }while(res);
 FOPEdit->Free();
 FOPEdit = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::EditEventTypes(int idx)
{
 UnicodeString name;
 bool res;
 int  pos;

 if(idx != addElement)
 {
   if(idx < 0)return;
 }
 else if(listBox->Count >= MAX_EVENTTYPES)
 {
	Application->MessageBox(Localization::Text(_T("mMaximumTextsExceeded")).c_str(),
						Localization::Text(_T("mError")).c_str(),
						MB_OK | MB_ICONERROR);
   return;
 }

 res = true;

 UnicodeString oldName = _T("\0");
 Application->CreateForm(__classid(TFOPEdit),&FOPEdit);
 FOPEdit->ButtonOk->ModalResult = mrOk;
 FOPEdit->SetMode(TFOPEdit::TFOPEMEvents);
 FOPEdit->Caption = Caption;
 if(idx != addElement)
 {
	oldName = listBox->Strings[idx];
	FOPEdit->SetVarName(listBox->Strings[idx]);
 }
 do
 {
   if(FOPEdit->ShowModal() == mrOk)
   {
     pos = -1;
     name = FOPEdit->GetVarName();
     if(!name.IsEmpty())
	 if((pos = listBox->IndexOf(name)) < 0)
	 {
	   if(idx == addElement)
	   {
		listBox->Add(name);
		WriteAddToChangeList(name);
	   }
	   else
	   {
		listBox->Strings[idx] = name;
		WriteRenameToChangeList(oldName, name);
	   }
	   CreateList(name);
	   res = false;
     }
     if(res && pos>=0)
	 {
		Application->MessageBox(Localization::Text(_T("mEventTypeNameExist")).c_str(),
							Localization::Text(_T("mError")).c_str(),
							MB_OK | MB_ICONERROR);
     }
   }else res=false;
 }while(res);
 FOPEdit->Free();
 FOPEdit = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::EditLangTypes(int idx)
{
 UnicodeString name;
 bool res;
 int  pos;
 if(idx != addElement)
 {
   if(idx < 0)return;
 }
 else if(listBox->Count >= MAX_LANGUAGES)
 {
	Application->MessageBox(Localization::Text(_T("mMaximumTextsExceeded")).c_str(),
						Localization::Text(_T("mError")).c_str(),
						MB_OK | MB_ICONERROR);
   return;
 }
 res = true;
 UnicodeString oldName = _T("\0");
 Application->CreateForm(__classid(TFOPEdit),&FOPEdit);
 FOPEdit->ButtonOk->ModalResult = mrOk;
 FOPEdit->SetMode(TFOPEdit::TFOPEMLanguages);
 FOPEdit->Caption = Caption;
 if(idx != addElement)
 {
	oldName = listBox->Strings[idx];
	FOPEdit->SetVarName(oldName);
 }
 do
 {
   if(FOPEdit->ShowModal() == mrOk)
   {
     pos = -1;
     name = FOPEdit->GetVarName();
     if(!name.IsEmpty())
	 if((pos = listBox->IndexOf(name)) < 0)
     {
	   if(idx == addElement)
	   {
		listBox->Add(name);
		WriteAddToChangeList(name);
	   }
	   else
	   {
		listBox->Strings[idx] = name;
		WriteRenameToChangeList(oldName, name);
	   }
	   CreateList(name);
	   res = false;
	 }
	 if(res && pos>=0)
	 {
		Application->MessageBox(Localization::Text(_T("mLanguageTypeNameExist")).c_str(),
							Localization::Text(_T("mError")).c_str(),
							MB_OK | MB_ICONERROR);
     }
   }
   else res=false;
 }
 while(res);
 FOPEdit->Free();
 FOPEdit = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::Delete(int idx)
{
	int i;
	UnicodeString	name;
	if(idx < 0)
	{
		return;
	}
	switch(mode)
	{
		case lmSTATE:
			name = ((TState*)list->Items[idx])->name;
		break;

		case lmBACKPART:
			name = ((TBGobjProperties*)list->Items[idx])->name;
		break;

		case lmOPPOSITE_TABLE:
		case lmANIM_TYPES:
		case lmEVENT_TYPES:
		case lmLANGUAGE_TYPES:
			WriteDeleteToChangeList(listBox->Strings[idx]);
			listBox->Delete(idx);
			name = _T("");
			if(listBox->Count <= idx)
			{
			   idx--;
			}
			if(idx >= 0)
			{
				name = listBox->Strings[idx];
			}
			CreateList(name);
		return;

		case lmNONE:
		return;
	}
	WriteDeleteToChangeList(name);
	TListObject *del_obj = static_cast<TListObject*>(list->Items[idx]);
	assert(del_obj->GetType() != LISTOBJ_Undef);
	delete del_obj;
	list->Delete(idx);
	name = _T("");
	if(list->Count <= idx)
	{
	   idx--;
	}
	if(idx >= 0)
	{
		 switch(mode)
		 {
		   case lmSTATE:
			name = ((TState*)list->Items[idx])->name;
		   break;

		   case lmBACKPART:
			name = ((TBGobjProperties*)list->Items[idx])->name;
		 }
	}
	CreateList(name);
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::CreateList(UnicodeString selected)
{
	int i, sel_idx, len;
	UnicodeString name;
	if(mode == lmNONE)
	{
		return;
	}
	switch(mode)
	{
		case lmSTATE:
		case lmBACKPART:
		len = list->Count;
		break;

		case lmOPPOSITE_TABLE:
		case lmANIM_TYPES:
		case lmEVENT_TYPES:
		case lmLANGUAGE_TYPES:
		len = listBox->Count;
	}
	ListBoxView->Items->Clear();
	for(i = 0; i < len; i++)
	{
		switch(mode)
		{
			case lmSTATE:
			name = ((TState *)list->Items[i])->name;
			break;

			case lmBACKPART:
			name = ((TBGobjProperties *)list->Items[i])->name;
			break;

			case lmOPPOSITE_TABLE:
			case lmANIM_TYPES:
			case lmEVENT_TYPES:
			case lmLANGUAGE_TYPES:
			name = listBox->Strings[i];
		}
		if(EdFind->Text.IsEmpty())
		{
			ListBoxView->Items->Add(name);
			if(!selected.IsEmpty() && selected.Compare(name) == 0)
			{
				ListBoxView->ItemIndex = ListBoxView->Items->Count - 1;
			}
		}
		else
		if(name.Pos(EdFind->Text.UpperCase()) != 0)
		{
			ListBoxView->Items->Add(name);
			if(!selected.IsEmpty() && selected.Compare(name) == 0)
			{
				ListBoxView->ItemIndex = ListBoxView->Items->Count - 1;
			}
		}
	}
	ListBoxViewClick(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::FormDestroy(TObject */*Sender*/)
{
	int  i;
	for(i = 0; i < list->Count; i++)
	{
		TListObject *del_obj = static_cast<TListObject*>(list->Items[i]);
		assert(del_obj->GetType() != LISTOBJ_Undef);
		delete del_obj;
	}
	delete list;
	delete changeList;
	delete listBox;
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::Bt_addClick(TObject *)
{
	Add();
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::Bt_editClick(TObject *)
{
	Edit(GetPos());
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::Bt_delClick(TObject *)
{
	Delete(GetPos());
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::ListBoxViewDblClick(TObject */*Sender*/)
{
	Edit(GetPos());
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::ListBoxViewKeyPress(TObject */*Sender*/, char &Key)
{
	if(Key == VK_RETURN)
	{
		Edit(GetPos());
	}
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::ListBoxViewClick(TObject *)
{
    int pos;
	StatusBar->SimpleText = _T("");
	if((pos = GetPos()) >= 0)
	{
    	StatusBar->SimpleText = Localization::Text(_T("mIndex")) +_T("= ") + IntToStr(pos);
    }	
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::EdFindChange(TObject *)
{
	UnicodeString str = _T("");
	if(GetPos() >= 0)
	{
		str = ListBoxView->Items->Strings[ListBoxView->ItemIndex];
	}
	CreateList(str);
	bool e = EdFind->Text.IsEmpty();
	Bt_trup->Enabled = e;
	Bt_trdown->Enabled = e;
}
//---------------------------------------------------------------------------

int __fastcall TFStateList::GetPos()
{
	int pos = -1;
	if(ListBoxView->ItemIndex >= 0)
	{
		switch(mode)
		{
			case lmSTATE:
				for(int i = 0; i < list->Count; i++)
				{
					if(((TState *)list->Items[i])->name.Compare(ListBoxView->Items->Strings[ListBoxView->ItemIndex]) == 0)
					{
						pos = i;
						break;
					}
				}
			break;
			case lmBACKPART:
				for(int i = 0; i < list->Count; i++)
				{
					if(((TBGobjProperties *)list->Items[i])->name.Compare(ListBoxView->Items->Strings[ListBoxView->ItemIndex]) == 0)
					{
						pos = i;
						break;
					}
				}
			break;
			case lmOPPOSITE_TABLE:
			case lmANIM_TYPES:
			case lmEVENT_TYPES:
			case lmLANGUAGE_TYPES:
				pos = listBox->IndexOf(ListBoxView->Items->Strings[ListBoxView->ItemIndex]);
		}
	}
	return pos;
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::BtCrearFilterClick(TObject *)
{
	EdFind->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::Bt_trupClick(TObject *)
{
	if(!EdFind->Text.IsEmpty() || ListBoxView->ItemIndex <= 0)
	{
        return;
    }
	switch(mode)
	{
		case lmSTATE:
		case lmBACKPART:
		 list->Move(ListBoxView->ItemIndex, ListBoxView->ItemIndex - 1);
		break;
		case lmOPPOSITE_TABLE:
		case lmANIM_TYPES:
		case lmEVENT_TYPES:
		case lmLANGUAGE_TYPES:
		 listBox->Move(ListBoxView->ItemIndex, ListBoxView->ItemIndex - 1);
	}
	CreateList(ListBoxView->Items->Strings[ListBoxView->ItemIndex]);
}
//---------------------------------------------------------------------------

void __fastcall TFStateList::Bt_trdownClick(TObject *)
{
	if(!EdFind->Text.IsEmpty() || ListBoxView->ItemIndex < 0 || ListBoxView->ItemIndex == ListBoxView->Items->Count - 1)
	{
        return;
    }
	switch(mode)
	{
		case lmSTATE:
		case lmBACKPART:
		 list->Move(ListBoxView->ItemIndex, ListBoxView->ItemIndex + 1);
		break;
		case lmOPPOSITE_TABLE:
		case lmANIM_TYPES:
		case lmEVENT_TYPES:
		case lmLANGUAGE_TYPES:
		 listBox->Move(ListBoxView->ItemIndex, ListBoxView->ItemIndex + 1);
	}
	CreateList(ListBoxView->Items->Strings[ListBoxView->ItemIndex]);
}
//---------------------------------------------------------------------------

