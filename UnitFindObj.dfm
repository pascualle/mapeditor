object FormFindObj: TFormFindObj
  Left = 0
  Top = 0
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderStyle = bsToolWindow
  Caption = 'mFind'
  ClientHeight = 240
  ClientWidth = 453
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  Position = poDesigned
  TextHeight = 13
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 453
    Height = 240
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 381
    ExplicitHeight = 228
    object TabSheet1: TTabSheet
      Caption = 'mFindGameObject'
      object Bevel1: TBevel
        Left = 10
        Top = 176
        Width = 425
        Height = 7
        Shape = bsTopLine
      end
      object Label3: TLabel
        Left = 8
        Top = 72
        Width = 77
        Height = 13
        Caption = 'mPropertyName'
      end
      object Label2: TLabel
        Left = 8
        Top = 45
        Width = 37
        Height = 13
        Caption = 'mGroup'
      end
      object Label1: TLabel
        Left = 8
        Top = 18
        Width = 98
        Height = 13
        Caption = 'mCharactersInName'
      end
      object Button1: TButton
        Left = 170
        Top = 184
        Width = 105
        Height = 25
        Caption = 'mFindNextObject'
        TabOrder = 8
        OnClick = Button1Click
      end
      object EditVal2: TEdit
        Left = 360
        Top = 123
        Width = 65
        Height = 21
        TabOrder = 7
        Text = '0'
        OnChange = EditValChange
      end
      object EditVal1: TEdit
        Left = 289
        Top = 123
        Width = 65
        Height = 21
        TabOrder = 6
        Text = '0'
        OnChange = EditValChange
      end
      object RadioButton2: TRadioButton
        Left = 155
        Top = 125
        Width = 128
        Height = 17
        Caption = 'mValueWithin'
        TabOrder = 5
        OnClick = RadioButton1Click
      end
      object EditVal: TEdit
        Left = 289
        Top = 96
        Width = 65
        Height = 21
        TabOrder = 4
        Text = '0'
        OnChange = EditValChange
      end
      object RadioButton1: TRadioButton
        Left = 155
        Top = 98
        Width = 104
        Height = 17
        Caption = 'mValue'
        Checked = True
        TabOrder = 3
        TabStop = True
        OnClick = RadioButton1Click
      end
      object ComboBoxProperty: TComboBox
        Left = 155
        Top = 69
        Width = 270
        Height = 21
        Style = csDropDownList
        TabOrder = 2
        OnChange = ComboBoxPropertyChange
      end
      object ComboBoxGroup: TComboBox
        Left = 155
        Top = 42
        Width = 270
        Height = 21
        Style = csDropDownList
        TabOrder = 1
      end
      object EditName: TEdit
        Left = 155
        Top = 15
        Width = 270
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 0
        OnChange = EditNameChange
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'mChangePropertiesValue'
      ImageIndex = 1
      object Label4: TLabel
        Left = 8
        Top = 18
        Width = 98
        Height = 13
        Caption = 'mCharactersInName'
      end
      object Label5: TLabel
        Left = 8
        Top = 45
        Width = 37
        Height = 13
        Caption = 'mGroup'
      end
      object Label6: TLabel
        Left = 8
        Top = 72
        Width = 77
        Height = 13
        Caption = 'mPropertyName'
      end
      object Bevel2: TBevel
        Left = 10
        Top = 176
        Width = 425
        Height = 7
        Shape = bsTopLine
      end
      object Label7: TLabel
        Left = 8
        Top = 99
        Width = 76
        Height = 13
        Caption = 'mPropertyValue'
      end
      object Label8: TLabel
        Left = 8
        Top = 126
        Width = 84
        Height = 13
        Caption = 'mReplaceToValue'
      end
      object EditNameReplace: TEdit
        Left = 155
        Top = 15
        Width = 270
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 0
        OnChange = EditNameChange
      end
      object ComboBoxGroupReplace: TComboBox
        Left = 155
        Top = 42
        Width = 270
        Height = 21
        Style = csDropDownList
        TabOrder = 1
      end
      object ComboBoxPropertyReplace: TComboBox
        Left = 155
        Top = 69
        Width = 270
        Height = 21
        Style = csDropDownList
        TabOrder = 2
        OnChange = ComboBoxPropertyReplaceChange
      end
      object EditValReplace: TEdit
        Left = 360
        Top = 96
        Width = 65
        Height = 21
        TabOrder = 3
        Text = '0'
        OnChange = EditValChange
      end
      object EditReplaceVal: TEdit
        Left = 360
        Top = 123
        Width = 65
        Height = 21
        TabOrder = 4
        Text = '0'
        OnChange = EditValChange
      end
      object ButtonReplace: TButton
        Left = 170
        Top = 184
        Width = 105
        Height = 25
        Caption = 'mReplace'
        TabOrder = 6
        OnClick = ButtonReplaceClick
      end
      object ApplyToAllMaps: TCheckBox
        Left = 8
        Top = 150
        Width = 161
        Height = 17
        Caption = 'mApplyToAllMaps'
        TabOrder = 5
      end
    end
  end
end
