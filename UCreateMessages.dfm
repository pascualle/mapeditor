object FMessages: TFMessages
  Left = 329
  Top = 269
  ActiveControl = CB_List
  BorderIcons = [biSystemMenu, biMaximize]
  ClientHeight = 563
  ClientWidth = 465
  Color = clBtnFace
  Constraints.MinHeight = 422
  Constraints.MinWidth = 320
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Pitch = fpFixed
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu
  Position = poOwnerFormCenter
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 0
    Top = 413
    Width = 465
    Height = 4
    Cursor = crVSplit
    Align = alBottom
    AutoSnap = False
    ResizeStyle = rsUpdate
    ExplicitTop = 453
    ExplicitWidth = 468
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 544
    Width = 465
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    ExplicitTop = 543
    ExplicitWidth = 461
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 465
    Height = 26
    Caption = 'ToolBar1'
    EdgeBorders = [ebTop, ebBottom]
    Images = Form_m.ImageList1
    TabOrder = 2
    ExplicitWidth = 461
    object ToolButton8: TToolButton
      Left = 0
      Top = 0
      Action = ActSizes
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton13: TToolButton
      Left = 23
      Top = 0
      Action = ActColor
    end
  end
  object Panel_m: TPanel
    Left = 0
    Top = 26
    Width = 465
    Height = 387
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 461
    ExplicitHeight = 386
    object Label2: TLabel
      Left = 222
      Top = 4
      Width = 30
      Height = 13
      Caption = 'mFont'
    end
    object Panel: TPanel
      Left = 0
      Top = 23
      Width = 465
      Height = 364
      Align = alBottom
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      FullRepaint = False
      TabOrder = 0
      ExplicitWidth = 461
      ExplicitHeight = 363
      object PanelG: TPanel
        Left = 0
        Top = 0
        Width = 465
        Height = 364
        Align = alClient
        BevelOuter = bvNone
        FullRepaint = False
        TabOrder = 0
        ExplicitWidth = 461
        ExplicitHeight = 363
        object Splitter1: TSplitter
          Left = 0
          Top = 214
          Width = 465
          Height = 4
          Cursor = crVSplit
          Align = alBottom
          AutoSnap = False
          ResizeStyle = rsUpdate
          ExplicitTop = 200
        end
        object ScrollBox: TScrollBox
          Left = 0
          Top = 0
          Width = 465
          Height = 214
          HorzScrollBar.Tracking = True
          VertScrollBar.Tracking = True
          Align = alClient
          BevelEdges = []
          TabOrder = 0
          ExplicitWidth = 461
          ExplicitHeight = 213
          object Image: TImage
            Left = 0
            Top = 0
            Width = 153
            Height = 145
          end
        end
        object Memo: TMemo
          Left = 0
          Top = 218
          Width = 465
          Height = 146
          Align = alBottom
          TabOrder = 1
          OnChange = MemoChange
          ExplicitTop = 217
          ExplicitWidth = 461
        end
      end
    end
    object CB_Font: TComboBox
      Left = 268
      Top = 1
      Width = 196
      Height = 21
      Style = csDropDownList
      MaxLength = 2
      TabOrder = 1
      OnChange = CB_FontChange
    end
  end
  object Alphabet: TCategoryButtons
    Left = 0
    Top = 417
    Width = 465
    Height = 127
    Align = alBottom
    BackgroundGradientDirection = gdVertical
    ButtonFlow = cbfVertical
    ButtonHeight = 22
    ButtonWidth = 22
    ButtonOptions = [boShowCaptions, boVerticalCategoryCaptions, boCaptionOnlyBorder]
    Categories = <
      item
        Color = clBtnFace
        Collapsed = False
        GradientColor = clWindow
        Items = <>
      end>
    HotButtonColor = clBtnFace
    Images = ImageList
    RegularButtonColor = clBtnFace
    SelectedButtonColor = 15717318
    ShowHint = True
    TabOrder = 4
    OnCategoryCollapase = AlphabetCategoryCollapase
    OnDrawIcon = AlphabetDrawIcon
    OnKeyPress = ListViewKeyPress
    OnMouseDown = AlphabetMouseDown
    ExplicitTop = 416
    ExplicitWidth = 461
  end
  object CB_List: TEdit
    Left = 4
    Top = 27
    Width = 212
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 64
    TabOrder = 1
  end
  object MainMenu: TMainMenu
    Images = Form_m.ImageList1
    Left = 48
    Top = 64
    object N2: TMenuItem
      Caption = 'mEditing'
      object N15: TMenuItem
        Action = ActSizes
      end
      object N10: TMenuItem
        Action = ActColor
      end
    end
    object N12: TMenuItem
      Caption = 'mAlignment'
      object NALeft: TMenuItem
        AutoCheck = True
        Caption = 'mAlignLeft'
        Checked = True
        GroupIndex = 1
        RadioItem = True
        OnClick = NALeftClick
      end
      object NARight: TMenuItem
        AutoCheck = True
        Caption = 'mAlignRight'
        GroupIndex = 1
        RadioItem = True
        OnClick = NALeftClick
      end
      object NAHCenter: TMenuItem
        AutoCheck = True
        Caption = 'mAlignHCenter'
        GroupIndex = 1
        RadioItem = True
        OnClick = NALeftClick
      end
      object N13: TMenuItem
        Caption = '-'
        GroupIndex = 1
      end
      object NATop: TMenuItem
        AutoCheck = True
        Caption = 'mAlignTop'
        Checked = True
        GroupIndex = 2
        RadioItem = True
        OnClick = NALeftClick
      end
      object NABottom: TMenuItem
        AutoCheck = True
        Caption = 'mAlignBottom'
        GroupIndex = 2
        RadioItem = True
        OnClick = NALeftClick
      end
      object NAVCenter: TMenuItem
        AutoCheck = True
        Caption = 'mAlignVCenter'
        GroupIndex = 2
        RadioItem = True
        OnClick = NALeftClick
      end
    end
  end
  object ActionList1: TActionList
    Images = Form_m.ImageList1
    Left = 48
    Top = 112
    object ActSave: TAction
      Caption = 'mSave'
      Hint = 'mSave'
      ImageIndex = 2
    end
    object ActLoad: TAction
      Caption = 'mOpen'
      Hint = 'mOpen'
      ImageIndex = 1
      OnExecute = ActLoadExecute
    end
    object ActSizes: TAction
      Caption = 'mCanvasSize'
      Hint = 'mCanvasSize'
      ImageIndex = 25
      OnExecute = ActSizesExecute
    end
    object ActNewMessage: TAction
      Caption = 'mAddText'
      Hint = 'mAddText'
      ImageIndex = 0
    end
    object ActDelMessage: TAction
      Caption = 'mDeleteText'
      Hint = 'mDeleteText'
      ImageIndex = 6
    end
    object ActColor: TAction
      Caption = 'mBackdrop'
      Hint = 'mBackdrop'
      ImageIndex = 23
      OnExecute = ActColorExecute
    end
  end
  object ImageList: TImageList
    Left = 16
    Top = 248
  end
  object ApplicationEvents1: TApplicationEvents
    OnMessage = ApplicationEvents1Message
    Left = 96
    Top = 248
  end
  object ColorDialog: TColorDialog
    Left = 176
    Top = 248
  end
end
