//---------------------------------------------------------------------------

#ifndef UFPathH
#define UFPathH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFProjPath : public TForm
{
__published:
		TButton *Button1;
		TEdit *EdPngPath;
		TButton *BtG_3dot;
		TLabel *Label2;
		TButton *ButtonCancel;
		TBevel *Bevel1;

		void __fastcall BtG_3dotClick(TObject *Sender);
private:

public:
        __fastcall TFProjPath(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFProjPath *FProjPath;
//---------------------------------------------------------------------------
#endif
