//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UObjPoolPropertyEdit.h"
#include "MainUnit.h"
#include "UObjCode.h"
#include "UObjManager.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFOPPEdit *FOPPEdit;
//---------------------------------------------------------------------------
__fastcall TFOPPEdit::TFOPPEdit(TComponent* Owner)
	: TForm(Owner)
{
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFOPPEdit::FormCreate(TObject *)
{
	__int32 ct = Form_m->GetCurrentResObjManager()->GParentObj->Count;
	if(ct > 0)
	{
		Ed_name->Items->Add("");
		for(int i = 0; i < ct; i++)
		{
			GPerson* gp = (GPerson*)Form_m->GetCurrentResObjManager()->GParentObj->Items[i];
			if(gp->DecorType == decorNONE &&
				gp->GetName().Compare(UnicodeString(TRIGGER_NAME)) != 0 &&
				gp->GetName().Compare(UnicodeString(DIRECTOR_NAME)) != 0 &&
				gp->GetName().Compare(UnicodeString(TEXTBOX_NAME)) != 0 &&
				gp->GetName().Compare(UnicodeString(SOUNDSCHEME_NAME)) != 0 &&
				gp->GetName().Compare(UnicodeString(OBJPOOL_NAME)) != 0)
			{
				Ed_name->Items->Add(gp->GetName());
            }
		}
		Ed_name->Sorted = false;
		Ed_name->Sorted = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFOPPEdit::SetPoolName(UnicodeString txt)
{
	Ed_name->ItemIndex = Ed_name->Items->IndexOf(txt);
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TFOPPEdit::GetPoolName() const
{
	return Ed_name->Text.Trim();
}
//---------------------------------------------------------------------------

void __fastcall TFOPPEdit::SetPoolCount(unsigned __int32 count)
{
	Edit_count->Text = IntToStr((__int32)count);
}
//---------------------------------------------------------------------------

unsigned __int32 __fastcall TFOPPEdit::GetPoolCount() const
{
	unsigned __int32 count;
	try
	{
		count = (unsigned __int32)StrToInt(Edit_count->Text.Trim());
	}
	catch(...)
	{
    	count = 0;
    }
	return count;
}
//---------------------------------------------------------------------------

void __fastcall TFOPPEdit::ButtonOkClick(TObject *)
{
	if(Ed_name->Text.IsEmpty())
	{
		Ed_name->SetFocus();
		return;
    }
	if(Edit_count->Text.IsEmpty() || GetPoolCount() < 1)
	{
		Edit_count->SetFocus();
		return;
	}
	ModalResult = mrOk;
}
//---------------------------------------------------------------------------

