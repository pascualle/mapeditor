//---------------------------------------------------------------------------

#ifndef UMETypesH
#define UMETypesH

//---------------------------------------------------------------------------

#include <System.hpp>
#include <XMLIntf.hpp>

//���������� ����
#define clTRANSPARENT   TColor(0xFFFF00FF)

//��� TBGobjProperties
#define DIRECTOR_TYPE   62

//"������" ������ �����
#define NONE_P          65535
//#define NONE_P_BYTE     255

//��������, ��� ��� �������� ���� �� ����������
#define NONE_BGTYPE     -125

#define MEWM_REPAINT WM_USER + 1
#define MEWM_WNDOPT WM_USER + 2
#define MEWM_MAINFORMRESIZE WM_USER + 3

typedef unsigned long hash_t;

extern double gsGlobalScale;

static const __int32 IX = 0;
static const __int32 IY = 1;

static const __int32 MAX_BG_WND_OBJECTS_W	= 250;
static const __int32 MAX_BG_WND_OBJECTS_H	= 250;
#define INTERFACE_LANG_DEFAULT _T("eng")
#define TEXT_NAME_DEFAULT _T("DEFAULT")
#define TEXT_NAME_LANG _T("LNG_")
#define TEXT_STR_NONE (Localization::Text(_T("mNONE")))

static const __int32 DEFAULT_TICK_DURATION = 30; //(30fps)

static const __int32 MAX_TRIGGER_ON_MAP		= 1024;
static const __int32 MAX_DIRECTOR_ON_MAP	= 32000;
static const __int32 MAX_CHARACTER_ON_MAP	= 5000;
static const __int32 MAX_OBJ_PARENTS 		= 4000;
static const __int32 MAX_OBJ_COLLIDENODES	= 254;
static const __int32 MAX_BG_TILETYPES		= MAX_BG_WND_OBJECTS_W * MAX_BG_WND_OBJECTS_H;
static const __int32 MAX_STATES				= 126;
static const __int32 MAX_SCRIPTS			= 127;
static const __int32 MAX_ANIMTYPES			= 127;
static const __int32 MAX_EVENTTYPES			= 255;
static const __int32 MAX_LANGUAGES			= 8;
static const __int32 MAX_TXT_STRINGS		= 65535;

static const __int32 MAX_MAP_SIZE_PX		= 64000;
static const __int32 MIN_TILE_SIZE			= 8;
static const __int32 MAX_TILE_SIZE			= 1024;
static const __int32 DEF_TILE_SIZE			= 32;

static const __int32 MAX_TILE_BG_LAYERS = 5;

static const __int32 ERROR_IMAGE_WIDTH = 32;
static const __int32 ERROR_IMAGE_HEIGHT = 32;
static const unsigned char GAMEOBJ_INNACTIVE_TRANSPARENCY_GDI = 80;
static const unsigned char SYSTEMOBJ_TRANSPARENCY_GDI = 170;
#define GAMEOBJ_INNACTIVE_TRANSPARENCY_GL (0.31f)
#define SYSTEMOBJ_TRANSPARENCY_GL (0.7f)
static const __int32 MIN_BOUND = -4096;
static const __int32 MAX_BOUND = 4096;

static const __int32 PARENT_TYPE_COUNT_MAX = 32;
static const __int32 PARENT_TYPE_SYSTEM = 0;
static const __int32 PARENT_TYPE_COUNT_DEFAULT = 6;

#define F_EPS ((double)1e-8)

#define ZONES_INDEPENDENT_IDX (-1)
#define LEVELS_INDEPENDENT_IDX (-2)

#define TRIGGER_NAME (_T("TRIGGER\0"))
#define DIRECTOR_NAME (_T("DIRECTOR\0"))
#define TEXTBOX_NAME (_T("TEXTBOX\0"))
#define SOUNDSCHEME_NAME (_T("SOUND SCHEME\0"))
#define OBJPOOL_NAME (_T("OBJ POOL\0"))

#define TRIGGER_ID (_T("TRIGGER_TYPE\0"))
#define DIRECTOR_ID (_T("DIRECTOR_TYPE\0"))
#define TEXTBOX_ID (_T("TEXTBOX_TYPE\0"))
#define SOUNDSCHEME_ID (_T("SOUND_SCHEME_TYPE\0"))
#define OBJPOOL_ID (_T("OBJ_POOL_TYPE\0"))

#define ANIM_TRIGGER_R_NAME (_T("?ANI_R_TRIGGER\0"))
#define ANIM_TRIGGER_G_NAME (_T("?ANI_G_TRIGGER\0"))
#define ANIM_DIRECTOR_NAME (_T("?ANI_DIRECTOR\0"))
#define ANIM_TEXTBOX_ICON_NAME (_T("?ANI_I_TEXTBOX\0"))
#define ANIM_TEXTBOX_TEXT_NAME (_T("?ANI_T_TEXTBOX\0"))
#define ANIM_SOUNDSCHEME_NAME (_T("?ANI_SOUNDSCH\0"))
#define ANIM_OBJPOOL_NAME (_T("?ANI_OBJPOOL\0"))

//---------------------------------------------------------------------------

enum TListObjectType
{
	LISTOBJ_Undef = 0,
	LISTOBJ_TState,
	LISTOBJ_TBGobjProperties
};

//---------------------------------------------------------------------------

enum ClipboardTypes
{
	CLIPBOARD_TYPE_GAMEOBJ = 0,
	CLIPBOARD_TYPE_FRAME
};

enum ResourceTypes
{
	RESOURCE_TYPE_IMAGE,
	RESOURCE_TYPE_FONTDATA,
	RESOURCE_TYPE_SOUNDDATA
};

typedef struct
{
	ResourceTypes	mType;
	UnicodeString	mName;
	unsigned short	mWidth;
	unsigned short	mHeight;
	unsigned __int32	mSize;
}TResourceItem;

//---------------------------------------------------------------------------

enum GraphicsTypes
{
	GRAPHICS_TYPE_BMP,
	GRAPHICS_TYPE_PNG
};

enum SoundTypes
{
	SOUND_TYPE_WAV
};

enum FontTypes
{
	FONT_TYPE_FNT
};

extern GraphicsTypes GlobalGraphicsType;
extern SoundTypes GlobalSoundType;
extern FontTypes GlobalFontType;

namespace Movement
{
  typedef enum
  {
	dirIDLE                 = 0,
	dirUP                   = 2,
	dirLEFT                 = 4,
	dirRIGHT                = 6,
	dirDOWN                 = 8,
	dirCOUNT                = 5
  }Direction;

  extern UnicodeString Name[dirCOUNT];
  extern const char XMLName[dirCOUNT][32];
}

//---------------------------------------------------------------------------

namespace Actions
{
	// ���� �������� � tengine
	enum SptTypes
	{
		SPTTYPE_NONE = 0,
		SPTTYPE_COLLIDE = 1,
		SPTTYPE_DIRECTOR = 2,
		SPTTYPE_PROPERTY = 3
	};

	enum Modes
	{
		sptONCOLLIDE    = 0,
		sptONTILECOLLIDE,   //�� �������� ����, � ����������� ������
		sptONCHANGENODE,
		sptONCHANGE1,
		sptONCHANGE2,
		sptONCHANGE3,
		sptONENDANIM,
		sptCOUNT        = 7
	};

	extern const char Names[sptCOUNT][32];
	extern UnicodeString Caption[sptCOUNT];

	inline __int32 GetScriptType(__int32 idx)
	{
		__int32 type = -1;
		switch(idx)
		{
			case sptONCOLLIDE:
			type=sptONCOLLIDE;
			break;

			case sptONTILECOLLIDE:
			type=sptONTILECOLLIDE;
			break;

			case sptONCHANGENODE:
			type=sptONCHANGENODE;
			break;

			case sptONCHANGE1:
			case sptONCHANGE2:
			case sptONCHANGE3:
			type=sptONCHANGE1;
			break;

			case sptONENDANIM:
			type=sptONENDANIM;
		}
		return type;
	}
};

//---------------------------------------------------------------------------
//�������, ����������� ����������� �������� �������� �������
namespace SpecialObjConstants
{
  enum
  {
   trOBJ_CHANGE_PRP_INITIATOR     = -1,
   trOBJ_CHANGE_PRP_SECOND        = -2,
   trOBJ_CHANGE_PRP_OPPOSITESTATE = 127,
   trOBJ_CHANGE_PRP_BLANKACTION   = 127
  };

  extern const char VarObjNameInitiator[32];
  extern const char VarObjNameSecond[32];
  extern const char VarStateName[32];
  extern const char VarBlankActionName[32];

  enum
  {
   trOBJ_CHANGE_PRP_TRANSMIT = 0, // �������� ��������� ��������
   trOBJ_CHANGE_PRP_DECREASE = 1, // ��������� �� ��������� ��������
   trOBJ_CHANGE_PRP_INCREASE = 2  // ��������� �� ��������� ��������
  };
};
//---------------------------------------------------------------------------

//������ �������������� ������� ������� �����
enum{modeParentObj, modeCurrentObj};

enum EditMode
{
	mAdd,
	mEdit,
	mDel
};

enum Alignment
{
	alUP = 0,
	alLEFT,
	alCENTER,
	alRIGHT,
	alDOWN,
	alINIT   //������������ ��� �������������� ������������� ��������
};
enum TMapObjDecorType
{
	decorNONE = 0, decorBACK, decorFORE
};

enum{ALIGN_LEFT = 100, ALIGN_CENTER, ALIGN_RIGHT};

enum TMapObjType
{
	objGPOBJ = 0,
	objMAPOBJ = 1,
	objGBACKOBJ = 2,
	objMAPTRIGGER = 4,
	objMAPDIRECTOR = 6,
	objMAPCOLLIDENODE = 7,
	objMAPCOLLIDELINE_OBSOLETE = 8,
	objMAPCOLLIDESHAPE = 15,
	objMAPTEXTBOX = 10,
	objMAPSOUNDSCHEME = 11,
	objMAPOBJPOOL = 12,
	objRESBMPFRAME = 13,
	objRESLOGICFRAME = 14,
	objNONE = 255,
	objSPCOUNT = 5 // objMAPTRIGGER, objMAPDIRECTOR, objMAPTEXTBOX, objMAPSOUNDSCHEME, objMAPOBJPOOL
};
enum{trNONE=0, trLEFTUP, trUP, trRIGHTUP, trLEFT, trFIRE, trRIGHT, trLEFTDOWN, trDOWN, trRIGHTDOWN};

enum{trMAP = 0,trPROPERTIES};  //����
enum{transformNONE = 0,transformVMIRROR = 1,transformHMIRROR = 2};  //���� ������������ ��� ��������� ������������

enum
{
	drawOnlyFirstFrame = 0,
	drawAnimation
};

enum LogicObjType
{
	UOP_LOGIC_OBJ_BMP = 0,
	UOP_LOGIC_OBJ_COLLIDE,
	UOP_LOGIC_OBJ_VIEW,
	UOP_LOGIC_OBJ_NODE1,
	UOP_LOGIC_OBJ_NODE2,
	UOP_LOGIC_OBJ_NODE3,
	UOP_LOGIC_OBJ_NODE4,
	UOP_LOGIC_OBJ_NODE5,
	UOP_LOGIC_OBJ_NODE6,
	UOP_LOGIC_OBJ_NODE7,
	UOP_LOGIC_OBJ_NODE8,
	UOP_LOGIC_OBJ_NODE9,
	UOP_LOGIC_OBJ_NODE10,
	UOP_LOGIC_OBJ_NODE11,
	UOP_LOGIC_OBJ_NODE12,
	UOP_LOGIC_OBJ_NODE13,
	UOP_LOGIC_OBJ_NODE14,
	UOP_LOGIC_OBJ_NODE15,
	UOP_LOGIC_OBJ_NODE16,
	UOP_MAX_LOGIC_DRAW_OBJ,
	MAX_ANIMATON_JOIN_NODES = 16,
	DEF_ANIMATON_JOIN_NODES = 1
};
//---------------------------------------------------------------------------

class TLMBtn
{
	public:
		__fastcall TLMBtn()
		: pos()
		, prev()
		, pressed(false)
		, moved(false)
		, collidenode(false)
		, snap_x(false)
		, snap_y(false)
		, locked(false)
		{
		}

	POINT pos;
	POINT prev;
	bool pressed;
	bool moved;
	bool collidenode;
	bool snap_x;
	bool snap_y;
	bool locked;
};
//---------------------------------------------------------------------------

//�������� ���������� �������� ������� �����
class PropertyItem
{
 private :

	 double	def;    //�������� �� ��������� (def)
	 double	value;  //������� ��������

 public:

	UnicodeString	name;	//��� ���������� (name)
	bool		checked;//�������� ��� ���������

	__fastcall PropertyItem();
	virtual __fastcall ~PropertyItem(){}

	void SetValue(double val);
	double GetValue() const;
	void SetDefaultValue(double val);
	double GetDefaultValue() const;
};
//---------------------------------------------------------------------------

class TRenderGLForm : public IUnknown
{
 public:

	virtual void __fastcall DoTick(unsigned __int32 ms) = 0;

 protected:

	virtual void __fastcall InitRenderGL() = 0;
	virtual void __fastcall ReleaseRenderGL() = 0;
};
//---------------------------------------------------------------------------

struct TSaveResItem
{
	ResourceTypes	mType;
	UnicodeString	mName;
	__int32			mIdx;
	char			mSoundType;
};
//---------------------------------------------------------------------------

class TFx32size
{
 public:

	enum TFx32Type
	{
		FX32T_DOUBLE = 0,
		FX32T_FIXED,
		FX32T_NINTENDODS
	};

	TFx32size()
		: mType(TFx32Type::FX32T_DOUBLE)
	{
		mName[FX32T_DOUBLE] = "double";
		mName[FX32T_FIXED] = "fixed";
		mName[FX32T_NINTENDODS] = "nintendo_ds";
	}

	~TFx32size(){}

	bool setType(const TFx32Type& type)
	{
		switch(type)
		{
			case FX32T_DOUBLE:
			case FX32T_FIXED:
			case FX32T_NINTENDODS:
				mType = type;
			return true;
			default:
				mType = FX32T_DOUBLE;
			return false;
		}
	}

	bool setTypeByName(const UnicodeString& name)
	{
		for(int i = FX32T_DOUBLE; i < FX32T_NINTENDODS + 1; ++i)
		if((mName[i].Compare(name) == 0))
		{
			mType = static_cast<TFx32Type>(i);
			return true;
		}
		mType = FX32T_DOUBLE;
		return false;
	}

	const inline TFx32Type& getType() const {return mType;}
	const inline UnicodeString& getTypeName() const {return mName[mType];}
	const inline bool isNintendo() const {return mType == TFx32Type::FX32T_NINTENDODS;}

 private:

	TFx32Type mType;
	UnicodeString mName[3];
};

#endif