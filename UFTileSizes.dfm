object FTileSizes: TFTileSizes
  Left = 366
  Top = 269
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  ClientHeight = 271
  ClientWidth = 376
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox_f: TGroupBox
    Left = 8
    Top = 8
    Width = 361
    Height = 111
    Caption = 'mTileObjectSizes'
    TabOrder = 0
    object Label1: TLabel
      Left = 10
      Top = 52
      Width = 36
      Height = 13
      Caption = 'mWidth'
    end
    object Label2: TLabel
      Left = 11
      Top = 27
      Width = 39
      Height = 13
      Caption = 'mHeight'
    end
    object CheckBoxRemainMapSize: TCheckBox
      Left = 11
      Top = 74
      Width = 334
      Height = 17
      Caption = 'mSaveMapSizeAndGameObjPositions'
      TabOrder = 0
    end
  end
  object ButtonCancel: TButton
    Left = 99
    Top = 237
    Width = 72
    Height = 24
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 2
  end
  object ButtonOk: TButton
    Left = 177
    Top = 237
    Width = 100
    Height = 24
    Caption = 'mApply'
    ModalResult = 1
    TabOrder = 3
  end
  object GroupBoxLayers: TGroupBox
    Left = 8
    Top = 124
    Width = 361
    Height = 103
    Caption = 'mLayers'
    TabOrder = 1
    object Label13: TLabel
      Left = 11
      Top = 18
      Width = 85
      Height = 13
      Caption = 'mTileLayersCount'
    end
    object Label14: TLabel
      Left = 11
      Top = 50
      Width = 339
      Height = 47
      AutoSize = False
      Caption = 'mTileLayersRenderRuleDesctiption'
      WordWrap = True
    end
    object Label15: TLabel
      Left = 11
      Top = 36
      Width = 221
      Height = 13
      Caption = 'mTileLayersAscendingRenderOrderDesctiption'
    end
  end
end
