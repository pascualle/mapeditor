//---------------------------------------------------------------------------

#ifndef UnitFindObjH
#define UnitFindObjH

//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Vcl.ComCtrls.hpp>

//---------------------------------------------------------------------------

class TMapPerson;

class TFormFindObj : public TForm
{
__published:
	TPageControl *PageControl;
	TTabSheet *TabSheet1;
	TTabSheet *TabSheet2;
	TButton *Button1;
	TBevel *Bevel1;
	TEdit *EditVal2;
	TEdit *EditVal1;
	TRadioButton *RadioButton2;
	TEdit *EditVal;
	TRadioButton *RadioButton1;
	TComboBox *ComboBoxProperty;
	TLabel *Label3;
	TLabel *Label2;
	TComboBox *ComboBoxGroup;
	TEdit *EditName;
	TLabel *Label1;
	TLabel *Label4;
	TEdit *EditNameReplace;
	TLabel *Label5;
	TComboBox *ComboBoxGroupReplace;
	TLabel *Label6;
	TComboBox *ComboBoxPropertyReplace;
	TEdit *EditValReplace;
	TEdit *EditReplaceVal;
	TButton *ButtonReplace;
	TBevel *Bevel2;
	TLabel *Label7;
	TLabel *Label8;
	TCheckBox *ApplyToAllMaps;
	void __fastcall ComboBoxPropertyChange(TObject *Sender);
	void __fastcall RadioButton1Click(TObject *Sender);
	void __fastcall EditValChange(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall EditNameChange(TObject *Sender);
	void __fastcall ButtonReplaceClick(TObject *Sender);
	void __fastcall ComboBoxPropertyReplaceChange(TObject *Sender);
private:

	void __fastcall CheckRadioButtons();

public:

	void __fastcall Init();
	void __fastcall Localize();
	void __fastcall Clear();
	void __fastcall UpdateButtonCaption();

	__fastcall TFormFindObj(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormFindObj *FormFindObj;
//---------------------------------------------------------------------------
#endif
