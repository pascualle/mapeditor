object FStateList: TFStateList
  Left = 366
  Top = 269
  BorderIcons = [biSystemMenu]
  ClientHeight = 446
  ClientWidth = 330
  Color = clBtnFace
  Constraints.MinHeight = 228
  Constraints.MinWidth = 300
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  DesignSize = (
    330
    446)
  TextHeight = 13
  object ButtonOk: TButton
    Left = 135
    Top = 398
    Width = 144
    Height = 26
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'mApply'
    ModalResult = 1
    TabOrder = 0
    ExplicitTop = 397
    ExplicitWidth = 140
  end
  object ButtonCancel: TButton
    Left = 53
    Top = 398
    Width = 76
    Height = 26
    Anchors = [akLeft, akBottom]
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 1
    ExplicitTop = 397
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 330
    Height = 395
    ActivePage = TabList
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    ExplicitWidth = 326
    ExplicitHeight = 394
    object TabList: TTabSheet
      Caption = 'mEditing'
      DesignSize = (
        322
        367)
      object Bevel3: TBevel
        Left = 275
        Top = 40
        Width = 28
        Height = 317
        Anchors = [akTop, akRight]
      end
      object Label1: TLabel
        Left = 16
        Top = 13
        Width = 32
        Height = 13
        Caption = 'mFilter'
      end
      object ListBoxView: TListBox
        Left = 16
        Top = 40
        Width = 262
        Height = 317
        Anchors = [akLeft, akTop, akRight, akBottom]
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = False
        TabOrder = 2
        OnClick = ListBoxViewClick
        OnDblClick = ListBoxViewDblClick
        OnKeyPress = ListBoxViewKeyPress
        ExplicitWidth = 258
        ExplicitHeight = 316
      end
      object Panel1: TPanel
        Left = 278
        Top = 160
        Width = 24
        Height = 197
        Anchors = [akTop, akRight, akBottom]
        TabOrder = 3
      end
      object Bt_del: TBitBtn
        Left = 278
        Top = 88
        Width = 24
        Height = 24
        Hint = 'mDoDelete'
        Anchors = [akTop, akRight]
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDD777777777DDDDDD09999999997DDDDD0999999999
          7DDDDD09999999997DDDDDD000000000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = Bt_delClick
        ExplicitLeft = 274
      end
      object Bt_edit: TBitBtn
        Left = 278
        Top = 64
        Width = 24
        Height = 24
        Hint = 'mEdit'
        Anchors = [akTop, akRight]
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDD777777777DDDDDD09999999997DDDD00999999
          9997DDD0209999999997DDD022000000000DD000222777DDDDDD02222222227D
          DDDD02222222227DDDDD02222222227DDDDDD000222777DDDDDDDDD02227DDDD
          DDDDDDD02227DDDDDDDDDDDD777DDDDDDDDDDDDDDDDDDDDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = Bt_editClick
        ExplicitLeft = 274
      end
      object Bt_add: TBitBtn
        Left = 278
        Top = 40
        Width = 24
        Height = 24
        Hint = 'mAdd'
        Anchors = [akTop, akRight]
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD000DDDDDDDDDDDD02227DD
          DDDDDDDDD02227DDDDDDDDD000222777DDDDDD02222222227DDDDD0222222222
          7DDDDD02222222227DDDDDD000222777DDDDDDDDD02227DDDDDDDDDDD02227DD
          DDDDDDDDDD777DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = Bt_addClick
        ExplicitLeft = 274
      end
      object EdFind: TEdit
        Left = 56
        Top = 10
        Width = 222
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        OnChange = EdFindChange
        ExplicitWidth = 218
      end
      object BtCrearFilter: TBitBtn
        Left = 278
        Top = 8
        Width = 24
        Height = 24
        Anchors = [akTop, akRight]
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDD777777777DDDDDD09999999997DDDDD0999999999
          7DDDDD09999999997DDDDDD000000000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtCrearFilterClick
        ExplicitLeft = 274
      end
      object Bt_trup: TBitBtn
        Left = 278
        Top = 112
        Width = 24
        Height = 24
        Hint = 'mMoveUp'
        Anchors = [akTop, akRight]
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDD000DDDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DD
          DDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DD
          DDDDDDDDD0CCC7DDDDDDDD0000CCC7777DDDDD0CCCCCCCCC7DDDDDD0CCCCCCC7
          DDDDDDDD0CCCCC7DDDDDDDDDD0CCC7DDDDDDDDDDDD777DDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        OnClick = Bt_trupClick
        ExplicitLeft = 274
      end
      object Bt_trdown: TBitBtn
        Left = 278
        Top = 136
        Width = 24
        Height = 24
        Hint = 'mMoveDown'
        Anchors = [akTop, akRight]
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD777DDD
          DDDDDDDDD0CCC7DDDDDDDDDD0CCCCC7DDDDDDDD0CCCCCCC7DDDDDD0CCCCCCCCC
          7DDDDD0000CCC7777DDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DD
          DDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DD
          DDDDDDDDD0CCC7DDDDDDDDDDDD000DDDDDDDDDDDDDDDDDDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        OnClick = Bt_trdownClick
        ExplicitLeft = 274
      end
    end
    object TabStates: TTabSheet
      Caption = 'mMessageBoxInfo'
      ImageIndex = 1
      object LabelInfo: TLabel
        Left = 18
        Top = 20
        Width = 277
        Height = 184
        AutoSize = False
        WordWrap = True
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 427
    Width = 330
    Height = 19
    Panels = <>
    SimplePanel = True
    ExplicitTop = 426
    ExplicitWidth = 326
  end
end
