#ifndef RENDER2dGL1_H_INCLUDED
#define RENDER2dGL1_H_INCLUDED

#include "system.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef NITRO_SDK
#ifdef USE_OPENGL_1_RENDER

#define RENDERFN(name) glRender_##name

class BMPImage;
typedef float fx32;

enum RGLLineStyle
{
	RGLLSSolid = 0xFFFF,
	RGLLSDot = 0xAAAA,
	RGLLSDash = 0xCCCC
};

enum RGLRenderTypes
{
	RGL_PIXEL = 0,
	RGL_LINE,
	RGL_FILLRECT,
	RGL_IMAGE,
};

typedef void (*BackdropFn)(s32 *currentTextureID);

u32 glRender_CreateTexture();
void glRender_LoadTextureToVRAM(const BMPImage *pImage, u32 resId);
void glRender_DeleteTexture(u32 resId);

// data from mapeditor
void glRender_SetupBGLayersData(enum BGSelect iBgType, s32 iMaxLayers, s32 iMaxTextures, s32 iMaxElements);
void glRender_ReleaseBGLayersData(enum BGSelect iBgType);

BOOL glRender_IsGraphicsInit(void);

void glRender_PlaneInit(const struct RenderPlaneInitParams* ipParams);
BOOL glRender_IsRenderPlaneInit(enum BGSelect iType);
void glRender_PlaneResize(enum BGSelect iType, const struct RenderPlaneSizeParams* ipParams);
void glRender_PlanePosition(enum BGSelect iType, s32 offX, s32 offY);
void glRender_PlaneRelease(enum BGSelect iType);

void glRender_SetBackdropFn(BackdropFn fn);

void glRender_SetActiveBGForGraphics(enum BGSelect iActiveBG);
enum BGSelect glRender_GetActiveBGForGraphics(void);

s32 glRender_GetViewWidth(enum BGSelect iBGType);
s32 glRender_GetViewHeight(enum BGSelect iBGType);
s32 glRender_GetViewLeft(enum BGSelect iBGType);
s32 glRender_GetViewTop(enum BGSelect iBGType);

s32 glRender_GetFrameBufferWidth(enum BGSelect iBGType);
s32 glRender_GetFrameBufferHeight(enum BGSelect iBGType);

void glRender_SetColor(u8 r, u8 g, u8 b, u8 a);

void glRender_SetLineStyle(u16 iStyle);
void glRender_DrawLine(fx32 iX0, fx32 iY0, fx32 iX1, fx32 iY1);

void glRender_ColorRect(fx32 iX, fx32 iY, fx32 iWidth, fx32 iHeight);

void glRender_DrawImage(const struct DrawImageFunctionData* data);

void glRender_Resize(s32 w, s32 h);

void glRender_SetRenderPlaneScale(fx32 val, enum BGSelect iBG);
fx32 glRender_GetRenderPlaneScale(enum BGSelect iBG);

s32 glRender_DrawFrame(void);

void glRender_ClearFrameBuffer(enum BGSelect iBG);

void glRender_Init(void);
void glRender_Release(void);

void glRender_LostDevice(void);
void glRender_RestoreDevice(void);

BOOL glRender_IsTexturesEnabled(void);
void glRender_ForceBindTexture(u32 idx);
void glRender_DummyDraw(void);

#endif //USE_OPENGL_1_RENDER
#endif //NITRO_SDK

#ifdef __cplusplus
}
#endif

#endif //RENDER2dGL1_H_INCLUDED
