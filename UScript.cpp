//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UScript.h"
#include "MainUnit.h"
#include "UObjCode.h"
#include "UCreateMessages.h"
#include "UObjManager.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TFScript *FScript;
//---------------------------------------------------------------------------

__fastcall TGScript::TGScript()
{
/*  TrigNameList = new TStringList;
  CldGroupNameList = new TStringList;
  CldObjNameList = new TStringList;
  Name = _T("");
  ChangeCtrlTo = _T("");
  Message = _T("");
  Zone = GoToZone = -1;
  EndLevel = -1;
  Multiple = false;
  Mode = ::Actions::sptONCOLLIDE;
  CldPropNameInitiator = _T("");
  CldPropNameSecond = _T("");
  CldParamValueInitiator = 0.0f;
  CldParamValueSecond = 0.0f;
  CldStateNameInitiator = _T("");
  CldStateNameSecond = _T("");
  CldMathOperPropInitiator = 1;
  CldMathOperPropSecond = 1;
  CldMathOperStateInitiator = 0;
  CldMathOperStateSecond = 0;
  ChgPropName = _T("");
  ChgMathOper = 0;
  ChgValue = 0.0f;
  Enabled = true;
*/
}
//---------------------------------------------------------------------------

__fastcall TGScript::~TGScript()
{
/*  delete TrigNameList;
  delete CldGroupNameList;
  delete CldObjNameList;
*/
 }
//---------------------------------------------------------------------------

void __fastcall TGScript::CopyTo(TGScript *spt) const
{
	if(spt==NULL)return;
	/*
	spt->Name = Name;
	spt->ChangeCtrlTo=ChangeCtrlTo;
	spt->TrigNameList->Assign(TrigNameList);
	spt->Zone = Zone;
	spt->GoToZone = GoToZone;
	spt->EndLevel = EndLevel;
	spt->Message = Message;
	spt->Multiple = Multiple;
	spt->Mode = Mode;
	//sptONCOLLIDE
	spt->CldGroupNameList->Assign(CldGroupNameList);
	spt->CldObjNameList->Assign(CldObjNameList);
	spt->CldPropNameInitiator = CldPropNameInitiator;
	spt->CldPropNameSecond = CldPropNameSecond;
	spt->CldParamValueInitiator = CldParamValueInitiator;
	spt->CldParamValueSecond = CldParamValueSecond;
	spt->CldStateNameInitiator = CldStateNameInitiator;
	spt->CldStateNameSecond = CldStateNameSecond;
	spt->CldMathOperPropInitiator = CldMathOperPropInitiator;
	spt->CldMathOperPropSecond = CldMathOperPropSecond;
	spt->CldMathOperStateInitiator = CldMathOperStateInitiator;
	spt->CldMathOperStateSecond = CldMathOperStateSecond;

	spt->ChgPropName = ChgPropName;
	spt->ChgMathOper = ChgMathOper;
	spt->ChgValue = ChgValue;

	spt->Enabled = Enabled;
    */
}
//---------------------------------------------------------------------------



//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
__fastcall TFScript::TFScript(TComponent* Owner)
	: TForm(Owner)
{
	CE_act_valInitiator = new TSpinEdit(GroupBoxInitiator);
	CE_act_valInitiator->Parent = GroupBoxInitiator;
	CE_act_valInitiator->Left = 241;
	CE_act_valInitiator->Top = 42;
	CE_act_valInitiator->Width = 59;
	CE_act_valInitiator->Height = 22;
	CE_act_valInitiator->TabOrder = 5;
    CE_act_valInitiator->Visible = true;
	CE_act_valSecond = new TSpinEdit(GroupBox2);
	CE_act_valSecond->Parent = GroupBox2;
	CE_act_valSecond->Left = 241;
	CE_act_valSecond->Top = 42;
	CE_act_valSecond->Width = 59;
	CE_act_valSecond->Height = 22;
	CE_act_valSecond->TabOrder = 5;
	CE_act_valSecond->Visible = true;
	CE_act_valInitiatorNode = new TSpinEdit(GroupBox5);
	CE_act_valInitiatorNode->Parent = GroupBox5;
	CE_act_valInitiatorNode->Left = 241;
	CE_act_valInitiatorNode->Top = 42;
	CE_act_valInitiatorNode->Width = 59;
	CE_act_valInitiatorNode->Height = 22;
	CE_act_valInitiatorNode->TabOrder = 5;
	CE_act_valInitiatorNode->Visible = true;
	CE_val = new TSpinEdit(TabChange);
	CE_val->Parent = TabChange;
	CE_val->Left = 305;
	CE_val->Top = 22;
	CE_val->Width = 72;
	CE_val->Height = 22;
	CE_val->TabOrder = 2;
    CE_val->Visible = true;
	CE_zone = new TSpinEdit(GroupBox1);
	CE_zone->Parent = GroupBox1;
	CE_zone->Left = 193;
	CE_zone->Top = 57;
	CE_zone->Width = 43;
	CE_zone->Height = 22;
	CE_zone->MaxValue = 255;
	CE_zone->TabOrder = 2;
    CE_zone->Visible = true;
	CE_end = new TSpinEdit(GroupBox1);
	CE_end->Parent = GroupBox1;
	CE_end->Left = 193;
	CE_end->Top = 130;
	CE_end->Width = 43;
	CE_end->Height = 22;
	CE_end->MaxValue = 255;
	CE_end->TabOrder = 7;
    CE_end->Visible = true;
	script=new TGScript();
	inited = false;
	//CE_TileUniqIdx->MaxValue = DIRECTOR_TYPE;
	//CE_TileUniqIdx->MinValue = 0;
	LastFocus = NULL;
	mOnlyZone = -10000;
	mOnlyMode = -10000;
	mpZonesNames = new TStringList();

	Localization::Localize(this);
}
//---------------------------------------------------------------------------
void __fastcall TFScript::FormDestroy(TObject *)
{
	delete CE_act_valInitiator;
	delete CE_act_valSecond;
	delete CE_act_valInitiatorNode;
	delete CE_val;
	delete CE_zone;
	delete CE_end;

	delete script;
	delete mpZonesNames;
}
//---------------------------------------------------------------------------

void __fastcall TFScript::ActiveOnlyZone(int zone)
{
   mOnlyZone = zone;
   CreateCBList();
}
//---------------------------------------------------------------------------

void __fastcall TFScript::ActiveOnlyMode(int mode)
{
   mOnlyMode = mode;

   TabCollide->TabVisible = false;
   TabChange->TabVisible = false;
   TabChangeDirector->TabVisible = false;

   switch(mOnlyMode)
   {
	case ::Actions::sptONCOLLIDE:
		TabCollide->TabVisible = true;
		PageControl->ActivePage=TabCollide;
	break;

	case ::Actions::sptONCHANGE1:
	case ::Actions::sptONCHANGE2:
	case ::Actions::sptONCHANGE3:
		TabChange->TabVisible = true;
		PageControl->ActivePage=TabChange;
	break;

	case ::Actions::sptONCHANGENODE:
		TabChangeDirector->TabVisible = true;
		PageControl->ActivePage=TabChangeDirector;
	break;
  }
  TabEndAnim->TabVisible = true;
}
//---------------------------------------------------------------------------

void __fastcall TFScript::CreateCBList()
{
   if(mpZonesNames->Count == 0)
	 return;

  ComboBox_zone->Items->Clear();
  if(mOnlyZone >= 0)
  {
	 ComboBox_zone->Items->Add(mpZonesNames->Strings[mOnlyZone]);
  }
  /*else
  {
	switch(mOnlyZone)
	{
		case ZONES_INDEPENDENT_IDX: ComboBox_zone->Items->Add(mpZonesNames->Strings[mpZonesNames->Count - 2]);
		break;
		case LEVELS_INDEPENDENT_IDX: ComboBox_zone->Items->Add(mpZonesNames->Strings[mpZonesNames->Count - 1]);
	}
  }*/
  ComboBox_zone->Items->Add(mpZonesNames->Strings[mpZonesNames->Count - 2]);
  ComboBox_zone->Items->Add(mpZonesNames->Strings[mpZonesNames->Count - 1]);
  ComboBox_zone->ItemIndex = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFScript::Init()
{
 int        i;
 GPerson    *gp;
 TState     *st;
 TMapDirector *dir;

 if(inited)
 {
	return;
 }

 ComboBox_zone->Items->Assign(Form_m->ListBoxZone->Items);
 mpZonesNames->Assign(Form_m->ListBoxZone->Items);
 ComboBox_zone->Items->Add(Localization::Text(GLOBAL_OJB_NAME));
 mpZonesNames->Add(Localization::Text(GLOBAL_OJB_NAME));

 if(mOnlyZone >= LEVELS_INDEPENDENT_IDX)
 {
   CreateCBList();
 }

	const std::list<PropertyItem>* lvars  = Form_m->mainCommonRes->GetVariablesMainGameObjPattern();
	CB_act_ParamInitiator->Clear();
	CB_act_ParamInitiator->Items->Add(UnicodeString());
	std::list<PropertyItem>::const_iterator iv;
	for (iv = lvars->begin(); iv != lvars->end(); ++iv)
	{
		CB_act_ParamInitiator->Items->Add(iv->name);
	}

 CB_act_ParamSecond->Items->Assign(CB_act_ParamInitiator->Items);
 CB_param->Items->Assign(CB_act_ParamInitiator->Items);
 CB_act_ParamInitiatorTile->Items->Assign(CB_act_ParamInitiator->Items);
 CB_act_ParamInitiatorNode->Items->Assign(CB_act_ParamInitiator->Items);

 for(i = 0; i < Form_m->mainCommonRes->GParentObj->Count; i++)
 {
	gp = (GPerson*)Form_m->mainCommonRes->GParentObj->Items[i];
	if(gp->IsDecoration)
	{
		continue;
	}
	if(gp->GetName().Compare(UnicodeString(TRIGGER_NAME)) == 0 ||
		gp->GetName().Compare(UnicodeString(DIRECTOR_NAME)) == 0)
	{
			continue;
	}
	CB_col_gr->Items->Add(gp->GetName());
 }

 CE_zone->MaxValue = Form_m->m_pObjManager->znCount - 1;
 if(CE_zone->MaxValue <= CE_zone->Value)
 {
   CE_zone->Value = CE_zone->MaxValue;
 }

 if(CE_zone->MaxValue <= 0)
 {
   CE_zone->Enabled = false;
   CheckBox_zone->Enabled = false;
   CheckBox_zone->Checked = false;
 }

 CE_end->MaxValue = Form_m->m_pLevelList->Count - 1;
 if(CE_end->MaxValue <= CE_end->Value)
 {
   CE_end->Value = CE_end->MaxValue;
 }

 CB_StateNameInitiator->Clear();
 CB_StateNameSecond->Clear();
 CB_StateNameInitiatorNode->Clear();
 CB_StateNameInitiator->Items->Add(_T(""));
 for(i = 0; i < Form_m->mainCommonRes->states->Count; i++)
 {
   st=(TState*)Form_m->mainCommonRes->states->Items[i];
   CB_StateNameInitiator->Items->Add( st->name );
 }
 CB_StateNameSecond->Items->Assign(CB_StateNameInitiator->Items);
 CB_StateNameInitiatorTile->Items->Assign(CB_StateNameInitiator->Items);
 CB_StateNameInitiatorNode->Items->Assign(CB_StateNameInitiator->Items);
 CB_StateNameInitiator->ItemIndex = 0;
 CB_StateNameInitiatorTile->ItemIndex = 0;
 CB_StateNameSecond->ItemIndex = 0;
 CB_StateNameInitiatorNode->ItemIndex = 0;

 CB_node->Clear();
 CB_node->Items->Add(_T(""));
 for(i = 0; i < Form_m->m_pObjManager->GetObjTypeCount(TObjManager::DIRECTOR); i++)
 {
   dir = (TMapDirector*)Form_m->m_pObjManager->GetObjByIndex(TObjManager::DIRECTOR, i);
   CB_node->Items->Add( dir->Name );
 }
 CB_StateNameInitiatorNode->ItemIndex = 0;

 ShowCheckBox(CheckBox_zone, CE_zone);
 CheckBoxMessageIDClick(NULL);
 ShowCheckBox(CheckBox_end, CE_end);
 Bt_3dot->Enabled=CheckBoxMessageID->Checked;
 inited = true;
}
//---------------------------------------------------------------------------

void __fastcall TFScript::ShowCheckBox(TCheckBox *cb, TSpinEdit *ed)
{
	if(cb->Checked)
	{
		ed->Enabled = true;
		ed->Color = clWindow;
	}
	else
	{
		ed->Enabled = false;
		ed->Color = clBtnFace;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFScript::CheckBox_zoneClick(TObject */*Sender*/)
{
	ShowCheckBox(CheckBox_zone, CE_zone);
}
//---------------------------------------------------------------------------

void __fastcall TFScript::ComboBox_zoneChange(TObject */*Sender*/)
{
	int idx = GetSelectedZone();
	ChangeZone(idx);
	ComboBox_zone->Hint = ComboBox_zone->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFScript::CheckBox_endClick(TObject */*Sender*/)
{
	ShowCheckBox(CheckBox_end, CE_end);
}
//---------------------------------------------------------------------------

int __fastcall TFScript::GetSelectedZone()
{
  int idx, i;
  idx = 0x7fffffff; //maximum positive
  if(ComboBox_zone->ItemIndex >= 0)
  {
	for(i = 0; i < mpZonesNames->Count; i++)
	{
		if(ComboBox_zone->Items->Strings[ComboBox_zone->ItemIndex].Compare(mpZonesNames->Strings[i]) == 0)
		{
			idx = i;
			if(idx == mpZonesNames->Count - 1)idx = LEVELS_INDEPENDENT_IDX;
			else
			if(idx == mpZonesNames->Count - 2)idx = ZONES_INDEPENDENT_IDX;
			break;
		}
	}
  }
  return idx;
}
//---------------------------------------------------------------------------
/*
void __fastcall TFScript::SetScript(TGScript* spt)
{
	Init();
	if(spt == NULL)return;
	spt->CopyTo(script);
	FillForm();
}
//---------------------------------------------------------------------------

void __fastcall TFScript::GetScript(TGScript* spt)
{
 if(spt == NULL)return;
 SaveFormToStruct();
 script->CopyTo(spt);
}
//---------------------------------------------------------------------------
*/
void __fastcall TFScript::ChangeZone(int zone)
{
 int i;
 TMapGPerson *gp;

 //clear all
 CB_new_hero->ItemIndex = -1;
 ListBox_obj->Items->Clear();
 CB_col_obj->Clear();
 CB_trig->Clear();
 ListBox_trig->Clear();
 //assign new obj

 if(zone != LEVELS_INDEPENDENT_IDX)
 {
	for(i = 0; i < Form_m->m_pObjManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT); i++)
	{
		gp = (TMapGPerson*)Form_m->m_pObjManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, i);
		if(gp->GetDecorType() != decorNONE)continue;
		if(gp->GetZone() != zone && gp->GetZone()>=0)continue;
		CB_col_obj->Items->Add(gp->Name);
	}
 }
 CB_new_hero->Items->Assign(CB_col_obj->Items);
 //if(CB_new_hero->Items->Count > 0)
 //{
	CB_new_hero->Items->Insert(0, TEXT_STR_NONE);
 //}
 CB_col_obj->ItemIndex = -1;

 for(i = 0; i < Form_m->m_pObjManager->GetObjTypeCount(TObjManager::TRIGGER); i++)
 {
	gp = (TMapTrigger*)Form_m->m_pObjManager->GetObjByIndex(TObjManager::TRIGGER, i);
	if(gp->GetZone() != zone && gp->GetZone() >= 0)continue;
	if(gp->GetZone() != zone && zone == LEVELS_INDEPENDENT_IDX)continue;
	CB_trig->Items->Add(gp->Name);
 }
 CB_trig->ItemIndex = -1;
}
//---------------------------------------------------------------------------

void __fastcall TFScript::FillForm()
{
 /*
 int        i,j;
 bool       find;
 GPerson    *gp;

 Edit_name->Text = script->Name;

 if(mOnlyZone < LEVELS_INDEPENDENT_IDX)
 {
	 if(script->Zone < 0)
	 {
		switch(script->Zone)
		{
			case ZONES_INDEPENDENT_IDX: ComboBox_zone->ItemIndex = ComboBox_zone->Items->Count - 2;
			break;
			case LEVELS_INDEPENDENT_IDX: ComboBox_zone->ItemIndex = ComboBox_zone->Items->Count - 1;
		}
	 }
	 else
	 {
	   if(ComboBox_zone->Items->Count > script->Zone)
	   {
		  ComboBox_zone->ItemIndex = script->Zone;
	   }
	   else
	   {
		  ComboBox_zone->ItemIndex = ComboBox_zone->Items->Count - 1; //���� ��� ������������ ������ (����� ��� ������ ������)
	   }
	 }
 }
 ComboBox_zone->Hint = ComboBox_zone->Text;
 ChangeZone(GetSelectedZone());

 ListBox_trig->Items->Assign(script->TrigNameList);
 for(j=0; j<ListBox_trig->Items->Count; j++) //check to validate
 {
  find = false;
  for(i = 0; i < CB_trig->Items->Count; i++)
  {
   UnicodeString lbStr = ListBox_trig->Items->Strings[j];
   if(CB_trig->Items->Strings[i].Compare(lbStr) == 0)
   {
     find = true;
     break;
   }
  }
  if(!find)
  {
    ListBox_trig->Items->Delete(j);
    j--;
  }
 }

 if(script->GoToZone < 0)CheckBox_zone->Checked=false;
 else
 {
  CheckBox_zone->Checked=true;
  CE_zone->Value=script->GoToZone;
 }
 ShowCheckBox(CheckBox_zone, CE_zone);

 i = CB_new_hero->Items->IndexOf(script->ChangeCtrlTo);
 if(i < 0)i = 0;
 CB_new_hero->ItemIndex = i;
 if(script->EndLevel >= 0)
 {
	CheckBox_end->Checked = true;
	CE_end->Value = script->EndLevel;
 }
 else
 {
	CheckBox_end->Checked = false;
	CE_end->Value = 0;
 }
 CheckBox_multi->Checked = script->Multiple;
 switch(script->Mode)
 {
  case ::Actions::sptONCOLLIDE:
		PageControl->ActivePage=TabCollide;
  break;

  case ::Actions::sptONTILECOLLIDE:
	   PageControl->ActivePage=TabTileCollide;
  break;

  case ::Actions::sptONCHANGE1:
		PageControl->ActivePage=TabChange;
  break;

  case ::Actions::sptONENDANIM:
		PageControl->ActivePage=TabEndAnim;
  break;

  case ::Actions::sptONCHANGENODE:
        PageControl->ActivePage=TabChangeDirector;
 }

 if(PageControl->ActivePage==TabCollide)
 {
  //scriptONCOLLIDE
  ListBox_gr->Items->Assign(script->CldGroupNameList);
  for(j=0; j<ListBox_gr->Items->Count; j++) //check to validate
  {
   find=false;
   for(i=0; i<Form_m->mainCommonRes->GParentObj->Count; i++)
   {
    gp=(GPerson*)Form_m->mainCommonRes->GParentObj->Items[i];
	if(gp->IsDecoration)
	{
		continue;
	}
	if(gp->GetName().Compare(ListBox_gr->Items->Strings[j]) == 0)
    {
	   find=true;
       break;
    }
   }
   if(!find)
   {
    ListBox_gr->Items->Delete(j);
    j--;
   }
  }

  ListBox_obj->Items->Assign(script->CldObjNameList);
  for(j=0; j<ListBox_obj->Items->Count; j++) //check to validate
  {
   find=false;
   for(i=0; i<CB_col_obj->Items->Count; i++)
   {
	UnicodeString objStr = CB_col_obj->Items->Strings[i];
	if(objStr.Compare(ListBox_obj->Items->Strings[j]) == 0)
    {
       find=true;
       break;
    }
   }
   if(!find)
   {
    ListBox_obj->Items->Delete(j);
    j--;
   }
  }

  i = CB_act_ParamInitiator->Items->IndexOf( script->CldPropNameInitiator );
  CB_act_ParamInitiator->ItemIndex = i;
  CE_act_valInitiator->Value = (int)script->CldParamValueInitiator;
  if(!script->CldStateNameInitiator.IsEmpty())
  {
    if((i = CB_StateNameInitiator->Items->IndexOf( script->CldStateNameInitiator )) >= 0)
    {
      CB_StateNameInitiator->ItemIndex = i;
    }
  }
  switch ( script->CldMathOperStateInitiator )
  {
   case mtEQUAL: CB_Op_StateInitiator->ItemIndex = 0;
   break;
   case mtNOT:   CB_Op_StateInitiator->ItemIndex = 1;
  }
  CB_Op_ParamInitiator->ItemIndex = script->CldMathOperPropInitiator;

  i = CB_act_ParamSecond->Items->IndexOf( script->CldPropNameSecond );
  CB_act_ParamSecond->ItemIndex = i;
  CE_act_valSecond->Value = (int)script->CldParamValueSecond;
  if(!script->CldStateNameSecond.IsEmpty())
  {
    if((i=CB_StateNameSecond->Items->IndexOf( script->CldStateNameSecond )) >= 0)
    {
      CB_StateNameSecond->ItemIndex = i;
    }
  }
  switch ( script->CldMathOperStateSecond )
  {
   case mtEQUAL: CB_Op_StateSecond->ItemIndex = 0;
   break;
   case mtNOT:   CB_Op_StateSecond->ItemIndex = 1;
  }
  CB_Op_ParamSecond->ItemIndex = script->CldMathOperPropSecond;
 }
 else
 if(PageControl->ActivePage==TabTileCollide)
 {
  //sptONTILECOLLIDE
  i = CB_act_ParamInitiatorTile->Items->IndexOf( script->CldPropNameInitiator );
  CB_act_ParamInitiatorTile->ItemIndex = i;
  //CE_act_valInitiatorTile->Value = (int)script->CldParamValueInitiator;
  if(!script->CldStateNameInitiator.IsEmpty())
  {
    if((i=CB_StateNameInitiatorTile->Items->IndexOf( script->CldStateNameInitiator )) >= 0)
    {
      CB_StateNameInitiatorTile->ItemIndex = i;
    }
  }
  switch ( script->CldMathOperStateInitiator )
  {
   case mtEQUAL: CB_Op_StateInitiatorTile->ItemIndex = 0;
   break;
   case mtNOT:   CB_Op_StateInitiatorTile->ItemIndex = 1;
  }
  CB_Op_ParamInitiatorTile->ItemIndex = script->CldMathOperPropInitiator;
  //� ���� ������ ��� ��� ���������� ������ �����
  //CE_TileUniqIdx->Value = (int)script->CldParamValueSecond;
 }
 else
 if(PageControl->ActivePage==TabChangeDirector)
 {
  //sptONCHANGENODE
  i = CB_act_ParamInitiatorNode->Items->IndexOf( script->CldPropNameInitiator );
  CB_act_ParamInitiatorNode->ItemIndex = i;
  CE_act_valInitiatorNode->Value = (int)script->CldParamValueInitiator;
  if(!script->CldStateNameInitiator.IsEmpty())
  {
    if((i=CB_StateNameInitiatorNode->Items->IndexOf( script->CldStateNameInitiator )) >= 0)
    {
      CB_StateNameInitiatorNode->ItemIndex = i;
    }
  }
  switch ( script->CldMathOperStateInitiator )
  {
   case mtEQUAL: CB_Op_StateInitiatorNode->ItemIndex = 0;
   break;
   case mtNOT:   CB_Op_StateInitiatorNode->ItemIndex = 1;
  }
  CB_Op_ParamInitiatorNode->ItemIndex = script->CldMathOperPropInitiator;
  i = CB_node->Items->IndexOf( script->CldPropNameSecond );
  CB_node->ItemIndex = i;
 }

 //scriptONCHANGE
 i = CB_param->Items->IndexOf(script->ChgPropName);
 CB_param->ItemIndex = i;
 CB_Op->ItemIndex = script->ChgMathOper;
 CE_val->Value = (int)script->ChgValue;

 if(!script->Message.IsEmpty())
 {
	EditMessageName->Text = script->Message;
	TStringList* list = new TStringList;
	Form_m->mpTexts->GetNames(list);
	if(list->IndexOf(script->Message) >= 0)
	{
		CheckBoxMessageID->Checked = true;
	}
	else
	{
		CheckBoxMessageID->Checked = false;
	}
	delete list;
 }
 else
 {
	CheckBoxMessageID->Checked = false;
 }
 CheckBoxMessageIDClick(NULL);
*/
}
//---------------------------------------------------------------------------

void __fastcall TFScript::SaveFormToStruct()
{
 /*
 script->Name=Edit_name->Text.Trim();
 if(CB_new_hero->ItemIndex <= 0)
 {
	script->ChangeCtrlTo = _T("");
 }
 else
 {
	script->ChangeCtrlTo = CB_new_hero->Text;
 }
 script->TrigNameList->Assign(ListBox_trig->Items);
 script->Zone = GetSelectedZone();
 //if(script->Zone == mpZonesNames->Count - 1)script->Zone = -1;
 if(CheckBox_zone->Checked)
 {
	script->GoToZone = CE_zone->Value;
 }
 else
 {
	script->GoToZone = -1;
 }
 if(CheckBox_end->Checked)
 {
	script->EndLevel = CE_end->Value;
 }
 else
 {
    script->EndLevel = -1;
 }
 script->Multiple = CheckBox_multi->Checked;

 script->Message = _T("");
 if(CheckBoxMessageID->Checked)
 {
	UnicodeString str = EditMessageName->Text.Trim();
	TStringList* list = new TStringList;
	Form_m->mpTexts->GetNames(list);
	if(list->IndexOf(str) >= 0)
	{
		script->Message = str;
	}
	delete list;
 }

 if(PageControl->ActivePage==TabCollide)script->Mode=::Actions::sptONCOLLIDE;
 else
 if(PageControl->ActivePage==TabTileCollide)script->Mode=::Actions::sptONTILECOLLIDE;
 else
 if(PageControl->ActivePage==TabChange)script->Mode=::Actions::sptONCHANGE1;
 else
 if(PageControl->ActivePage==TabEndAnim)script->Mode=::Actions::sptONENDANIM;
 else
 if(PageControl->ActivePage==TabChangeDirector)script->Mode=::Actions::sptONCHANGENODE;

 if(PageControl->ActivePage==TabCollide)
 {
  //scriptONCOLLIDE
  script->CldGroupNameList->Assign(ListBox_gr->Items);
  script->CldObjNameList->Assign(ListBox_obj->Items);
  script->CldPropNameInitiator = CB_act_ParamInitiator->Text;
  script->CldPropNameSecond = CB_act_ParamSecond->Text;
  script->CldParamValueInitiator = CE_act_valInitiator->Value;
  script->CldParamValueSecond = CE_act_valSecond->Value;
  script->CldStateNameInitiator = CB_StateNameInitiator->Text;
  script->CldStateNameSecond = CB_StateNameSecond->Text;
  script->CldMathOperPropInitiator = (char)CB_Op_ParamInitiator->ItemIndex;
  script->CldMathOperPropSecond = (char)CB_Op_ParamSecond->ItemIndex;
  switch ( CB_Op_StateInitiator->ItemIndex )
  {
   case 0: script->CldMathOperStateInitiator = mtEQUAL;
   break;
   case 1: script->CldMathOperStateInitiator = mtNOT;
  }
  switch ( CB_Op_StateSecond->ItemIndex )
  {
   case 0: script->CldMathOperStateSecond = mtEQUAL;
   break;
   case 1: script->CldMathOperStateSecond = mtNOT;
  }
 }
 else
 if(PageControl->ActivePage==TabTileCollide)
 {
   script->CldPropNameInitiator = CB_act_ParamInitiatorTile->Text;
   //script->CldParamValueInitiator = CE_act_valInitiatorTile->Value;
   script->CldStateNameInitiator = CB_StateNameInitiatorTile->Text;
   script->CldMathOperPropInitiator = (char)CB_Op_ParamInitiatorTile->ItemIndex;
   //script->CldParamValueSecond = CE_TileUniqIdx->Value;
   switch ( CB_Op_StateInitiatorTile->ItemIndex )
   {
   case 0: script->CldMathOperStateInitiator = mtEQUAL;
   break;
   case 1: script->CldMathOperStateInitiator = mtNOT;
   }
 }
 else
 if(PageControl->ActivePage==TabChangeDirector)
 {
  script->CldPropNameInitiator = CB_act_ParamInitiatorNode->Text;
  script->CldParamValueInitiator = CE_act_valInitiatorNode->Value;
  script->CldStateNameInitiator = CB_StateNameInitiatorNode->Text;
  script->CldMathOperPropInitiator = (char)CB_Op_ParamInitiatorNode->ItemIndex;
  script->CldPropNameSecond = CB_node->Text;
  switch ( CB_Op_StateInitiatorNode->ItemIndex )
  {
   case 0: script->CldMathOperStateInitiator = mtEQUAL;
   break;
   case 1: script->CldMathOperStateInitiator = mtNOT;
  }
 }

 //scriptONCHANGE
 script->ChgPropName=CB_param->Text;
 script->ChgMathOper=(char)CB_Op->ItemIndex;
 script->ChgValue=CE_val->Value;
 */
}
//---------------------------------------------------------------------------

void __fastcall TGScript::SaveToFile(BYTE , TSaveLoadStreamData &f)
{
 /*
 short      w_val;
 char       ch_val[128];
 int        i,j;
 if(f == NULL)return;
 // BYTE ��� ������� (sptONCOLLIDE,sptONACTIVATE,sptONCHANGE)
 ch_val[0]=Mode;
 fwrite(ch_val,1,1,f);
 // WORD ������� ����
 w_val=(short)Zone;
 fwrite(&w_val,sizeof(short),1,f);
 // BYTE ����������� ��� ������������ ������
 ch_val[0]=Multiple;
 fwrite(ch_val,1,1,f);
 // WORD ���������� �������� � �����
 w_val=(short)Name.Length();
 fwrite(&w_val,sizeof(short),1,f);
 // BYTE[] ���
 fwrite(AnsiString(Name).c_str(),w_val,1,f);
 // WORD ���������� �������� � ����� �������, �� �������� ����������� ����������
 w_val=(short)ChangeCtrlTo.Length();
 fwrite(&w_val,sizeof(short),1,f);
 // BYTE[] ���
 fwrite(AnsiString(ChangeCtrlTo).c_str(),w_val,1,f);
 // WORD ������� �� ����
 w_val=(short)GoToZone;
 fwrite(&w_val,sizeof(short),1,f);
 // BYTE ��������� �������
 ch_val[0]=(char)EndLevel;
 fwrite(ch_val,1,1,f);
 // BYTE ���������� �������� � id ���������� ��������� (v18)
 w_val = (char)Message.Length();
 fwrite(&w_val, 1, 1, f);
 //  BYTES[] id ���������� ��������� (v18)
 if(w_val > 0)
 {
	fwrite(AnsiString(Message).c_str(), w_val, 1, f);
 }
 //sptONCOLLIDE
 // WORD ���������� ������ GroupNameList
 w_val=(short)CldGroupNameList->Count;
 fwrite(&w_val,sizeof(short),1,f);
 j=w_val;
 for(i=0;i<j;i++)
 {
  // WORD ���������� �������� � ����� ������
  w_val=(short)CldGroupNameList->Strings[i].Length();
  fwrite(&w_val,sizeof(short),1,f);
  // BYTE[] ��� ������
  fwrite(AnsiString(CldGroupNameList->Strings[i]).c_str(),w_val,1,f);
 }
 //WORD ���������� ������� ObjNameList
 w_val=(short)CldObjNameList->Count;
 fwrite(&w_val,sizeof(short),1,f);
 j=w_val;
 for(i=0;i<j;i++)
 {
  // WORD ���������� �������� � ����� �������
  w_val=(short)CldObjNameList->Strings[i].Length();
  fwrite(&w_val,sizeof(short),1,f);
  // BYTE[] ��� �������
  fwrite(AnsiString(CldObjNameList->Strings[i]).c_str(),w_val,1,f);
 }
 // WORD ���������� �������� � ����� �������� ����������
 w_val = (short)CldPropNameInitiator.Length();
 fwrite(&w_val, sizeof(short), 1, f);
 // BYTE[] ��� �������� ����������
 fwrite(AnsiString(CldPropNameInitiator).c_str(), w_val, 1, f);
 // WORD �������� ��������
 w_val = (short)CldParamValueInitiator;
 fwrite(&w_val, sizeof(short), 1, f);
 // BYTE �������������� �������� ������� ��������
 ch_val[0] = (char)CldMathOperPropInitiator;
 fwrite(ch_val, 1, 1, f);
 // WORD ���������� �������� � ����� ��������� ����������
 w_val = (short)CldStateNameInitiator.Length();
 fwrite(&w_val, sizeof(short), 1, f);
 // BYTE[] ��� ��������� ����������
 fwrite(AnsiString(CldStateNameInitiator).c_str(), w_val, 1, f);
 // BYTE �������������� �������� ������� ���������
 ch_val[0] = (char)CldMathOperStateInitiator;
 fwrite(ch_val, 1, 1, f);

 // WORD ���������� �������� � ����� �������� ������� ���������
 w_val = (short)CldPropNameSecond.Length();
 fwrite(&w_val, sizeof(short), 1, f);
 // BYTE[] ��� �������� ����������
 fwrite(AnsiString(CldPropNameSecond).c_str(), w_val, 1, f);
 // WORD �������� ��������
 w_val = (short)CldParamValueSecond;
 fwrite(&w_val, sizeof(short), 1, f);
 // BYTE �������������� �������� ������� ��������
 ch_val[0] = (char)CldMathOperPropSecond;
 fwrite(ch_val, 1, 1, f);
 // WORD ���������� �������� � ����� ��������� ������� ���������
 w_val = (short)CldStateNameSecond.Length();
 fwrite(&w_val, sizeof(short), 1, f);
 // BYTE[] ��� ��������� ����������
 fwrite(AnsiString(CldStateNameSecond).c_str(), w_val, 1, f);
 // BYTE �������������� �������� ������� ���������
 ch_val[0] = (char)CldMathOperStateSecond;
 fwrite(ch_val, 1, 1, f);

 //sptONCHANGE
 // WORD ���������� �������� � ����� ���������
 w_val=(short)ChgPropName.Length();
 fwrite(&w_val,sizeof(short),1,f);
 // BYTE[] ���
 fwrite(AnsiString(ChgPropName).c_str(),w_val,1,f);
 // BYTE �������������� �������� char
 ch_val[0]=ChgMathOper;
 fwrite(ch_val,1,1,f);
 // WORD �������� ONCHANGE
 w_val=(short)ChgValue;
 fwrite(&w_val,sizeof(short),1,f);

 //WORD ���������� ������� ��������� TrigNameList
 w_val=(short)TrigNameList->Count;
 fwrite(&w_val,sizeof(short),1,f);
 j=w_val;
 for(i=0;i<j;i++)
 {
  // WORD ���������� �������� � ����� ��������
  w_val=(short)TrigNameList->Strings[i].Length();
  fwrite(&w_val,sizeof(short),1,f);
  // BYTE[] ��� ��������
  fwrite(AnsiString(TrigNameList->Strings[i]).c_str(),w_val,1,f);
 }
 */
}
//---------------------------------------------------------------------------

void __fastcall TGScript::LoadFromFile(TSaveLoadStreamData &f, int ver)
{
 /*
 short      w_val;
 char       ch_val[MAX_PATH];
 int        i,j;
 if(f==NULL)return;
 // BYTE ��� ������� (sptONCOLLIDE,sptONACTIVATE,sptONCHANGE)
 fread(ch_val,1,1,f);
 Mode=ch_val[0];
 // WORD ������� ����
 fread(&w_val,sizeof(short),1,f);
 Zone=w_val;
 // BYTE ����������� ��� ������������ ������
 fread(ch_val,1,1,f);
 Multiple=ch_val[0];
 // WORD ���������� �������� � �����
 fread(&w_val,sizeof(short),1,f);
 // BYTE[] ���
 if(w_val > 0)fread(ch_val,w_val,1,f);
 ch_val[w_val]=0;
 Name=UnicodeString((char*)ch_val);
 // WORD ���������� �������� � ����� �������, �� �������� ����������� ����������
 fread(&w_val,sizeof(short),1,f);
 // BYTE[] ���
 if(w_val > 0)fread(ch_val,w_val,1,f);
 ch_val[w_val]=0;
 ChangeCtrlTo=UnicodeString((char*)ch_val);
 // WORD ������� �� ����
 fread(&w_val,sizeof(short),1,f);
 GoToZone=w_val;
 // BYTE ��������� �������
 fread(ch_val,1,1,f);
 EndLevel = ch_val[0];
 if(ver <= 12)
 {
	if(EndLevel == 0)EndLevel = -1;
 }
 if(ver < 18)
 {
	 //BYTE ID ���������
	 fread(ch_val, 1, 1, f);
	 //MessageId = ch_val[0];
 }
 else
 {
	// BYTE ���������� �������� � id ���������� ��������� (v18)
    w_val = 0;
	fread(&w_val, 1, 1, f);
	//  BYTES[] id ���������� ��������� (v18)
	if(w_val > 0)
	{
		fread(ch_val, w_val, 1, f);
    }
	ch_val[w_val] = 0;
	Message = UnicodeString(ch_val);
 }

 //sptONCOLLIDE
 // WORD ���������� ������ GroupNameList
 CldGroupNameList->Clear();
 fread(&w_val,sizeof(short),1,f);
 j = w_val;
 for(i=0; i < j; i++)
 {
  // WORD ���������� �������� � ����� ������
  fread(&w_val,sizeof(short),1,f);
  // BYTE[] ��� ������
  if(w_val > 0)fread(ch_val,w_val,1,f);
  ch_val[w_val]=0;
  CldGroupNameList->Add((char*)ch_val);
 }
 //WORD ���������� ������� ObjNameList
 CldObjNameList->Clear();
 fread(&w_val,sizeof(short),1,f);
 j = w_val;
 for(i = 0; i < j; i++)
 {
  // WORD ���������� �������� � ����� �������
  fread(&w_val,sizeof(short),1,f);
  // BYTE[] ��� �������
  if(w_val > 0)fread(ch_val,w_val,1,f);
  ch_val[w_val]=0;
  CldObjNameList->Add((char*)ch_val);
 }

 if(ver >= 7)
 {
  // WORD ���������� �������� � ����� �������� ����������
  fread(&w_val, sizeof(short), 1, f);
  // BYTE[] ���
  if(w_val > 0)fread(ch_val, w_val, 1, f);
  ch_val[w_val] = 0;
  CldPropNameInitiator = UnicodeString((char*)ch_val);
  // WORD �������� ��������
  fread(&w_val, sizeof(short), 1, f);
  CldParamValueInitiator = w_val;
  // BYTE �������������� �������� ������� ��������
  fread(ch_val, 1, 1, f);
  CldMathOperPropInitiator = ch_val[0];
  // WORD ���������� �������� � ����� ��������� ����������
  fread(&w_val, sizeof(short), 1, f);
  // BYTE[] ���
  if(w_val > 0)fread(ch_val, w_val, 1, f);
  ch_val[w_val] = 0;
  CldStateNameInitiator = UnicodeString((char*)ch_val);
  // BYTE �������������� �������� ������� ���������
  fread(ch_val, 1, 1, f);
  CldMathOperStateInitiator = ch_val[0];

  // WORD ���������� �������� � ����� �������� ����������
  fread(&w_val, sizeof(short), 1, f);
  // BYTE[] ���
  if(w_val > 0)fread(ch_val, w_val, 1, f);
  ch_val[w_val] = 0;
  CldPropNameSecond = UnicodeString((char*)ch_val);
  // WORD �������� ��������
  fread(&w_val, sizeof(short), 1, f);
  CldParamValueSecond = w_val;
  // BYTE �������������� �������� ������� ��������
  fread(ch_val, 1, 1, f);
  CldMathOperPropSecond = ch_val[0];
  // WORD ���������� �������� � ����� ��������� ����������
  fread(&w_val, sizeof(short), 1, f);
  // BYTE[] ���
  if(w_val > 0)fread(ch_val, w_val, 1, f);
  ch_val[w_val] = 0;
  CldStateNameSecond = UnicodeString((char*)ch_val);
  // BYTE �������������� �������� ������� ���������
  fread(ch_val, 1, 1, f);
  CldMathOperStateSecond = ch_val[0];
 }
 else
 {
  //sptONACTIVATE  --- ���������� ������
  // WORD ���������� �������� � ����� ��������� (����������)
  fread(&w_val,sizeof(short),1,f);
  // BYTE[] ���
  if(w_val > 0)fread(ch_val,w_val,1,f);
  ch_val[w_val]=0;
  //ActPropName=UnicodeString((char*)ch_val);
  // WORD �������� ONACTIVATE
  fread(&w_val,sizeof(short),1,f);
  //ActValue = w_val;
  // WORD ���������� �������� � ����� ���������
  fread(&w_val,sizeof(short),1,f);
  // BYTE[] ��� ���������
  if(w_val > 0)fread(ch_val,w_val,1,f);
  ch_val[w_val]=0;
  //StateName = UnicodeString((char*)ch_val);
 }

 //sptONCHANGE
 // WORD ���������� �������� � ����� ���������
 fread(&w_val,sizeof(short),1,f);
 // BYTE[] ���
 if(w_val > 0)fread(ch_val,w_val,1,f);
 ch_val[w_val]=0;
 ChgPropName=UnicodeString((char*)ch_val);
 // BYTE �������������� �������� char
 fread(ch_val,1,1,f);
 ChgMathOper=ch_val[0];
 // WORD �������� ONCHANGE
 fread(&w_val,sizeof(short),1,f);
 ChgValue=w_val;

 //WORD ���������� ������� ��������� TrigNameList
 fread(&w_val,sizeof(short),1,f);
 j=w_val;
 for(i=0;i<j;i++)
 {
  // WORD ���������� �������� � ����� ��������
  fread(&w_val,sizeof(short),1,f);
  // BYTE[] ��� ��������
  if(w_val > 0)fread(ch_val,w_val,1,f);
  ch_val[w_val]=0;
  TrigNameList->Add((char*)ch_val);
 }
 */
}

//---------------------------------------------------------------------------
void __fastcall TFScript::Edit_nameChange(TObject *Sender)
{
	Edit_nameChangeMain((TEdit*)Sender);
}
//---------------------------------------------------------------------------


//������� ���������
//---------------------------------------------------------------------------
void __fastcall TFScript::Bt_trupClick(TObject */*Sender*/)
{
 int idx;
 idx=ListBox_trig->ItemIndex;
 if(idx<=0)return;
 ListBox_trig->Items->Move(idx,idx-1);
}
//---------------------------------------------------------------------------
void __fastcall TFScript::Bt_trdownClick(TObject */*Sender*/)
{
 int idx;
 idx=ListBox_trig->ItemIndex;
 if(idx<0)return;
 if(idx>=ListBox_trig->Count-1)return;
 ListBox_trig->Items->Move(idx,idx+1);
}
//---------------------------------------------------------------------------
void __fastcall TFScript::Bt_traddClick(TObject */*Sender*/)
{
 if(CB_trig->Text.IsEmpty())return;
 if(ListBox_trig->Items->IndexOf(CB_trig->Text)>=0)return;
 ListBox_trig->Items->Add(CB_trig->Text);
 ListBox_trig->ItemIndex=ListBox_trig->Items->Count-1;
}
//---------------------------------------------------------------------------
void __fastcall TFScript::Bt_trdelClick(TObject */*Sender*/)
{
 int idx;
 idx=ListBox_trig->ItemIndex;
 if(idx<0)return;
 ListBox_trig->Items->Delete(idx);
 if(idx>=ListBox_trig->Items->Count)idx=ListBox_trig->Items->Count-1;
 ListBox_trig->ItemIndex=idx;
}
//---------------------------------------------------------------------------

//������
void __fastcall TFScript::Bt_graddClick(TObject */*Sender*/)
{
 if(CB_col_gr->Text.IsEmpty())return;
 if(ListBox_gr->Items->IndexOf(CB_col_gr->Text)>=0)return;
 ListBox_gr->Items->Add(CB_col_gr->Text);
 ListBox_gr->ItemIndex=ListBox_gr->Items->Count-1;
}
//---------------------------------------------------------------------------

void __fastcall TFScript::Bt_grdelClick(TObject */*Sender*/)
{
 int idx;
 idx=ListBox_gr->ItemIndex;
 if(idx<0)return;
 ListBox_gr->Items->Delete(idx);
 if(idx>=ListBox_gr->Items->Count)idx=ListBox_gr->Items->Count-1;
 ListBox_gr->ItemIndex=idx;
}
//---------------------------------------------------------------------------

//�������
void __fastcall TFScript::Bt_objaddClick(TObject */*Sender*/)
{
 if(CB_col_obj->Text.IsEmpty())return;
 if(ListBox_obj->Items->IndexOf(CB_col_obj->Text)>=0)return;
 ListBox_obj->Items->Add(CB_col_obj->Text);
 ListBox_obj->ItemIndex=ListBox_obj->Items->Count-1;
}
//---------------------------------------------------------------------------

void __fastcall TFScript::Bt_objdelClick(TObject */*Sender*/)
{
 int idx;
 idx=ListBox_obj->ItemIndex;
 if(idx<0)return;
 ListBox_obj->Items->Delete(idx);
 if(idx>=ListBox_obj->Items->Count)idx=ListBox_obj->Items->Count-1;
 ListBox_obj->ItemIndex=idx;
}
//---------------------------------------------------------------------------

void __fastcall TFScript::CheckBoxMessageIDClick(TObject */*Sender*/)
{
	if(CheckBoxMessageID->Checked)
	{
		EditMessageName->Enabled = true;
		EditMessageName->Color = clWindow;
	}
	else
	{
		EditMessageName->Enabled = false;
		EditMessageName->Color = clBtnFace;
	}
	Bt_3dot->Enabled = CheckBoxMessageID->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TFScript::Bt_3dotClick(TObject */*Sender*/)
{
	UnicodeString str = EditMessageName->Text.Trim();
	bool res = Form_m->TextMessagesManager(&str);
	if(res)
	{
		EditMessageName->Text = str;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFScript::Bt_find_trigClick(TObject */*Sender*/)
{
  int idx = -1;
  UnicodeString text;
  if(LastFocus == CB_trig && CB_trig->ItemIndex >= 0)
  {
    text = CB_trig->Text;
    idx = 1;
  }
  else
  if(LastFocus == ListBox_trig && ListBox_trig->ItemIndex >= 0)
  {
    text = ListBox_trig->Items->Strings[ListBox_trig->ItemIndex];
    idx = 1;
  }
  if(idx < 0)
  {
	return;
  }
  Form_m->FindAndShowObjectByName(text, false);
}
//---------------------------------------------------------------------------

void __fastcall TFScript::CB_trigExit(TObject */*Sender*/)
{
   LastFocus = CB_trig;
}
//---------------------------------------------------------------------------

void __fastcall TFScript::ListBox_trigExit(TObject */*Sender*/)
{
   LastFocus = ListBox_trig;
}
//---------------------------------------------------------------------------


