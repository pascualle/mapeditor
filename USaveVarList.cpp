//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "USaveVarList.h"
#include "ULocalization.h"
#include "MainUnit.h"
#include "inifiles.hpp"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TFSaveVarList *FSaveVarList;

//---------------------------------------------------------------------------
__fastcall TFSaveVarList::TFSaveVarList(TComponent* Owner)
		: TForm(Owner)
{
	Localization::Localize(this);
	SaveDialog->Title = Localization::Text(_T("mSaveGeneratedConstants"));
}
//---------------------------------------------------------------------------

void __fastcall TFSaveVarList::Bt_saveClick(TObject *)
{
	if(!Form_m->h_file_name.IsEmpty())
	{
		SaveDialog->InitialDir = ExtractFilePath(Form_m->h_file_name);
		SaveDialog->FileName = ExtractFileName(Form_m->h_file_name);
	}

	if(SaveDialog->Execute())
	{
		UnicodeString fname = ChangeFileExt(SaveDialog->FileName, _T(".h"));
		Memo->Lines->SaveToFile(fname);
		Form_m->h_file_name = fname;

		TIniFile *ini;
		ini = new TIniFile(Form_m->PathIni + _T("editor.ini"));
		ini->WriteString(_T("Path"), _T("LastConstantsPath"), ExtractRelativePath(Form_m->PathIni, Form_m->h_file_name));
		delete ini;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSaveVarList::FormShow(TObject *)
{
	Bt_save->Enabled = (Memo->Lines->Count != 0);
}
//---------------------------------------------------------------------------

void __fastcall TFSaveVarList::ActSelectAllExecute(TObject *)
{
	Memo->SelectAll();
}
//---------------------------------------------------------------------------

