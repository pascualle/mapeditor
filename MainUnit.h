//---------------------------------------------------------------------------

#ifndef MainUnitH
#define MainUnitH
//---------------------------------------------------------------------------
#include <stdio.h>
#include <ActnList.hpp>
#include <AppEvnts.hpp>
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <StdCtrls.hpp>
#include <Tabs.hpp>
#include <ToolWin.hpp>
#include <XPMan.hpp>
#include <System.Actions.hpp>
#include <ActnList.hpp>
#include <Classes.hpp>
#include <ComCtrls.hpp>
#include <Controls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <StdCtrls.hpp>
#include <Tabs.hpp>
#include <ToolWin.hpp>
#include <XPMan.hpp>
#include <AppEvnts.hpp>
#include <CategoryButtons.hpp>
#include <Graphics.hpp>
#include <Grids.hpp>
#include <CustomizeDlg.hpp>
#include "UTypes.h"
#include <System.ImageList.hpp>
#include <list>
#include "UObjMarker.h"

//---------------------------------------------------------------------------

//#define LITE_VER
//#define XML_VER

//last change 25.06.2024
#define FILEFORMAT_VER (char)35
#define CLIPBOARD_VER (char)3

struct  TLMBtn;
struct  TEGrWndParams;
struct  TEdPrObjWndParams;
class   TMapPerson;
class   TMapGPerson;
class   TMapSpGPerson;
class   GPerson;
class   GMapBGObj;
class   TObjPattern;
class   TFEditGrObj;
class   TPlatformGameField;
class   TState;
class   TObjManager;
class   TCommonResObjManager;
class   BMPImage;
class	TCommonTextManager;
class   TMapTextBox;
class	TFormFindObj;
class	TAnimationFrameData;
class 	TTransformProperties;
class	TSaveLoadStreamData;
class	TSaveLoadStreamHandle;

//---------------------------------------------------------------------------

extern const _TCHAR* GLOBAL_OJB_NAME;

extern TFx32size mFx32size;
extern unsigned __int32 mWcharsize;
extern unsigned __int32 mWcharsizeExternal;
extern TDateTime gCurrentTimeMark;

enum TObjCreateMode
{
	ObjCreateModeClipboard = 0,
	ObjCreateModeLoading,
	ObjCreateModeNormal
};

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

class TForm_m : public TForm
{
friend class TFGOProperty;
friend class TFMessages;
friend class TFScript;
friend class TFScriptList;
friend class TFSpObjProperty;
friend class TFormTxtObjProperty;
friend class TFEditState;
friend class TFOppState;
friend class TFDirectorProperty;
friend class TFDefProperties;
friend class TFBGObjWindow;
friend class TFBOProperty;
friend class TFGOCreate;
friend class TFormTextManager;
friend class TFObjParent;
friend class TFormFindObj;
friend class TFCollideNodeProperty;

__published:
	TStatusBar *StatusBar1;
	TActionList *ActionList1;
	TMainMenu *MainMenu1;
	TMenuItem *N1;
	TMenuItem *N2;
	TMenuItem *N3;
	TMenuItem *N5;
	TMenuItem *N6;
	TAction *ActNew;
	TAction *ActMapSave;
	TAction *ActExit;
	TSplitter *Splitter;
	TAction *ActColor;
	TAction *ActInfo;
	TMenuItem *N9;
	TMenuItem *N10;
	TTimer *Timer_cr;
	TAction *ActGrid;
	TAction *ActHideObjects;
	TOpenDialog *OpenDialog;
	TSaveDialog *SaveDialog;
	TMenuItem *N17;
	TImageList *ImageList1;
	TAction *ActPath;
	TMenuItem *N14;
	TMenuItem *N19;
	TMenuItem *N22;
	TMenuItem *N23;
	TMenuItem *N24;
	TMenuItem *N11;
	TMenuItem *N12;
	TAction *ActBackAdd;
	TAction *ActBackEdit;
	TAction *ActBackDel;
	TAction *ActObjAdd;
	TAction *ActObjDel;
	TAction *ActObjEdit;
	TMenuItem *N27;
	TMenuItem *N28;
	TMenuItem *N29;
	TMenuItem *N30;
	TAction *ActObjPattern;
	TMenuItem *N33;
	TMenuItem *N34;
	TAction *ActProjSaveAs;
	TAction *ActProjLoad;
	TMenuItem *N32;
	TMenuItem *N35;
	TPopupMenu *PopupMenu_u;
	TPopupMenu *PopupMenu_s;
	TMenuItem *N18;
	TMenuItem *N8;
	TMenuItem *N31;
	TMenuItem *N36;
	TMenuItem *N37;
	TMenuItem *N38;
	TMenuItem *N39;
	TMenuItem *N40;
	TScrollBar *HorzScrollBar;
	TScrollBar *VertScrollBar;
	TPopupMenu *PopupMenu_img;
	TMenuItem *N41;
	TMenuItem *N42;
	TMenuItem *N43;
	TAction *ActMapObjEdit;
	TAction *ActMapObjDel;
	TAction *ActShowRectObj;
	TMenuItem *N44;
	TAction *ActSetVatDef;
	TMenuItem *N26;
	TMenuItem *N45;
	TAction *ActVarList;
	TMenuItem *N46;
	TAction *ActClearMap;
	TMenuItem *N50;
	TMenuItem *N54;
	TMenuItem *N55;
	TAction *ActSetVarDefGroup;
	TAction *ActSetVarparentToDef;
	TMenuItem *N56;
	TToolBar *ToolBar;
	TToolButton *ToolButtonEdit;
	TAction *ActEditMode;
	TAction *ActCursorMode;
	TAction *ActAniDisableAll;
	TAction *ActAniEnableObj;
	TAction *ActAniDisableObj;
	TAction *ActAniEnableBG;
	TAction *ActAniDisableBG;
	TAction *ActEditorOpt;
	TMenuItem *N20;
	TMenuItem *N21;
	TPanel *Panel_e1;
	TPanel *Panel_layer;
	TTabSet *TabSet;
	TSplitter *SplitterObjPal_Zones;
	TListBox *ListBoxZone;
	TAction *ActObjOrder;
	TMenuItem *N59;
	TMenuItem *N60;
	TAction *ActLockDecor;
	TMenuItem *N61;
	TToolButton *ToolButton1;
	TToolButton *ToolButton2;
	TToolButton *ToolButton3;
	TToolButton *ToolButton4;
	TToolButton *ToolButton5;
	TToolButton *ToolButton6;
	TAction *ActMessages;
	TMenuItem *N62;
	TAction *ActScript;
	TMenuItem *N64;
	TImageList *ImageListDir;
	TImageList *ImageListSpIcons;
	TImageList *ImageListTrig;
	TAction *ActStates;
	TMenuItem *N65;
	TAction *ActTypes;
	TMenuItem *N67;
	TToolButton *ToolButtonStart;
	TMenuItem *N51;
	TAction *ActAnimation;
	TMenuItem *NVAnimation;
	TAction *ActStartEmul;
	TToolButton *ToolButtonSSeparator;
	TToolButton *ToolButtonSStart;
	TToolButton *ToolButtonSPause;
	TToolButton *ToolButtonSStep;
	TAction *ActStart;
	TAction *ActPause;
	TAction *ActStep;
	TAction *ActShowBGTransMap;
	TMenuItem *MDebug;
	TMenuItem *DbgSeparator;
	TMenuItem *DbgStateTrace;
	TMenuItem *DbgStopOnStateChange;
	TMenuItem *DbgResetDirList;
	TAction *ActClearDbgMessages;
	TMenuItem *MDClearDebugMessages;
	TAction *ActStopOnScript;
	TAction *ActResetTraceScripts;
	TAction *ActStartScript;
	TMenuItem *MDStopOnScript;
	TMenuItem *MDTraceScript;
	TMenuItem *MDScripts;
	TMenuItem *N70;
	TMenuItem *DbgSeparator1;
	TMenuItem *NPopupStartScript;
	TAction *ActOppositeStates;
	TMenuItem *N4;
	TMenuItem *MDResetRunOnceScripts;
	TToolButton *ToolButton8;
	TToolButton *TBObjects;
	TToolButton *TBBackground;
	TAction *ActObjMode;
	TAction *ActBackMode;
	TAction *ShowNonActiveZoneObj;
	TMenuItem *N48;
	TAction *ActFindNext;
	TMenuItem *N53;
	TMenuItem *N57;
	TAction *ActShowFocused;
	TMenuItem *N58;
	TAction *ActFindObj;
	TMenuItem *N68;
	TAction *ActDbgChangeZone;
	TMenuItem *N69;
	TMenuItem *N71;
	TMenuItem *N72;
	TAction *ActMapObjClone;
	TMenuItem *N74;
	TAction *ActCmplxEnableDragChilds;
	TMenuItem *DragDrop1;
	TAction *ActHidePath;
	TMenuItem *N75;
	TTabControl *TabLevels;
	TMenuItem *N76;
	TAction *ActNewMap;
	TAction *ActMapDel;
	TAction *ActMapChangeIndex;
	TMenuItem *N7;
	TMenuItem *N47;
	TMenuItem *N77;
	TAction *ActMapOptions;
	TMenuItem *N78;
	TAction *ActAnimTypes;
	TMenuItem *N73;
	TAction *ActTileOptions;
	TMenuItem *N79;
	TAction *ActBGCopy;
	TAction *ActBGPaste;
	TAction *ActBGPasteMulti;
	TAction *ActUndo;
	TMenuItem *N13;
	TMenuItem *N80;
	TMenuItem *N81;
	TMenuItem *N82;
	TMenuItem *N83;
	TAction *ActDelSelection;
	TMenuItem *N84;
	TMenuItem *N49;
	TMenuItem *N52;
	TMenuItem *MDResetAll;
	TMenuItem *N86;
	TMenuItem *DbgTrigActionsTrace;
	TMenuItem *N85;
	TMenuItem *N87;
	TAction *ActSelectAll;
	TMenuItem *N88;
	TMenuItem *N89;
	TMenuItem *N90;
	TAction *ActShowHint;
	TMenuItem *MMVHideHints;
	TToolButton *ToolButton7;
	TToolButton *ToolButtonObjManager;
	TToolButton *TBCollideNodes;
	TAction *ActCollideNodesMode;
	TButton *FakeControl;
	TMenuItem *CtrlC1;
	TMenuItem *N15;
	TMenuItem *N16;
	TAction *ActGoToBGReference;
	TMenuItem *N25;
	TPanel *Sh_sqr;
	TPanel *PaintBox1;
	TPanel *PanelObj;
	TListBox *ListBoxDebug;
	TListView *ListView_u;
	TTabSet *TabSetObj;
	TMenuItem *N66;
	TMenuItem *NPMUMoveto;
	TAction *ActXMLSave;
	TAction *ActCloneMap;
	TMenuItem *N91;
	TPopupMenu *PopupMenuParentTabs;
	TMenuItem *PMPTAddTab;
	TMenuItem *MenuItem5;
	TMenuItem *PMPTRenameTab;
	TMenuItem *MVLanguages;
	TMenuItem *N92;
	TAction *ActEventTypes;
	TMenuItem *N93;
	TAction *ActProjSave;
	TMenuItem *N94;
	TMenuItem *N95;
	TMenuItem *NPMI_NewShape;
	TMenuItem *NPMI_NewNode;
	TAction *ActNewCollideShape;
	TAction *ActNewCollideNode;
	TAction *ActMapSaveTo;
	TMenuItem *N3_1;
	TAction *ActAnimJNCount;
	TMenuItem *mAnimationJNCount1;
	TAction *ActLockGameObject;
	TMenuItem *NLockGameObject;
	TMenuItem *N63;

	void __fastcall ActExitExecute(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall ActNewExecute(TObject *Sender);
	void __fastcall ActGridExecute(TObject *Sender);
	void __fastcall Shape_crMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall PaintBox1MouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall ActInfoExecute(TObject *Sender);
	void __fastcall ListView_sEnter(TObject *Sender);
	void __fastcall ActHideObjectsExecute(TObject *Sender);
	void __fastcall ActMapSaveExecute(TObject *Sender);
	void __fastcall ActObjPatternExecute(TObject *Sender);
	void __fastcall ActBackAddExecute(TObject *Sender);
	void __fastcall ActPathExecute(TObject *Sender);
	void __fastcall ActBackEditExecute(TObject *Sender);
	void __fastcall ActBackDelExecute(TObject *Sender);
	void __fastcall ActObjAddExecute(TObject *Sender);
	void __fastcall ActObjDelExecute(TObject *Sender);
	void __fastcall ActObjEditExecute(TObject *Sender);
	void __fastcall Splitter2Moved(TObject *Sender);
	void __fastcall VertScrollBarScroll(TObject *Sender, TScrollCode ScrollCode, int &ScrollPos);
	void __fastcall ActMapObjEditExecute(TObject *Sender);
	void __fastcall ActMapObjDelExecute(TObject *Sender);
	void __fastcall ActShowRectObjExecute(TObject *Sender);
	void __fastcall MainMenu1Change(TObject *Sender, TMenuItem *Source, bool Rebuild);
	void __fastcall ActSetVatDefExecute(TObject *Sender);
	void __fastcall ActProjLoadExecute(TObject *Sender);
	void __fastcall ActProjSaveAsExecute(TObject *Sender);
	void __fastcall PopupMenu_uPopup(TObject *Sender);
	void __fastcall PopupMenu_sPopup(TObject *Sender);
	void __fastcall N45Click(TObject *Sender);
	void __fastcall ActVarListExecute(TObject *Sender);
	void __fastcall ActClearMapExecute(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall ActSetVarDefGroupExecute(TObject *Sender);
	void __fastcall ActSetVarparentToDefExecute(TObject *Sender);
	void __fastcall PaintBox1MouseLeave(TObject *Sender);
	void __fastcall ActEditModeExecute(TObject *Sender);
	void __fastcall ActAniEnableObjExecute(TObject *Sender);
	void __fastcall ActAniDisableObjExecute(TObject *Sender);
	void __fastcall ListBoxZoneClick(TObject *Sender);
	void __fastcall ListBoxZoneEnter(TObject *Sender);
	void __fastcall ActObjOrderExecute(TObject *Sender);
	void __fastcall ActLockDecorExecute(TObject *Sender);
	void __fastcall ActMessagesExecute(TObject *Sender);
	void __fastcall ActScriptExecute(TObject *Sender);
	void __fastcall ActStatesExecute(TObject *Sender);
	void __fastcall ActTypesExecute(TObject *Sender);
	void __fastcall ActAnimationExecute(TObject *Sender);
	void __fastcall ActStartEmulExecute(TObject *Sender);
	void __fastcall ActStartExecute(TObject *Sender);
	void __fastcall ActPauseExecute(TObject *Sender);
	void __fastcall ActStepExecute(TObject *Sender);
	void __fastcall ActShowBGTransMapExecute(TObject *Sender);
	void __fastcall DbgStateTraceClick(TObject *Sender);
	void __fastcall DbgStopOnStateChangeClick(TObject *Sender);
	void __fastcall ActClearDbgMessagesExecute(TObject *Sender);
	void __fastcall ActStopOnScriptExecute(TObject *Sender);
	void __fastcall ActResetTraceScriptsExecute(TObject *Sender);
	void __fastcall MDStartScriptClick(TObject *Sender);
	void __fastcall ListBoxDebugDblClick(TObject *Sender);
	void __fastcall ActOppositeStatesExecute(TObject *Sender);
	void __fastcall MDResetRunOnceScriptsClick(TObject *Sender);
	void __fastcall ListView_uDblClick(TObject *Sender);
	void __fastcall ListView_uKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall ActObjModeExecute(TObject *Sender);
	void __fastcall ActBackModeExecute(TObject *Sender);
	void __fastcall CategoryButtons_sCategoryCollapase(TObject *Sender, const TButtonCategory *Category);
	void __fastcall CategoryButtons_sMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall CategoryButtons_sDrawIcon(TObject *Sender, const TButtonItem *Button, TCanvas *Canvas, TRect &Rect, TButtonDrawState State, int &TextOffset);
	void __fastcall CategoryButtons_sEnter(TObject *Sender);
	void __fastcall CategoryButtons_sKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall PaintBox1DblClick(TObject *Sender);
	void __fastcall Image_dDblClick(TObject *Sender);
	void __fastcall ListView_uStartDrag(TObject *Sender, TDragObject *&DragObject);
	void __fastcall PaintBox1DragOver(TObject *Sender, TObject *Source, int X, int Y, TDragState State, bool &Accept);
	void __fastcall ListView_uEndDrag(TObject *Sender, TObject *Target, int X, int Y);
	void __fastcall ShowNonActiveZoneObjExecute(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall ActFindNextExecute(TObject *Sender);
	void __fastcall ActShowFocusedExecute(TObject *Sender);
	void __fastcall ActFindObjExecute(TObject *Sender);
	void __fastcall ActDbgChangeZoneExecute(TObject *Sender);
	void __fastcall StatusBar1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall CategoryButtons_sMouseEnter(TObject *Sender);
	void __fastcall CategoryButtons_sMouseLeave(TObject *Sender);
	void __fastcall ActMapObjCloneExecute(TObject *Sender);
	void __fastcall ActCmplxEnableDragChildsExecute(TObject *Sender);
	void __fastcall ActHidePathExecute(TObject *Sender);
	void __fastcall TabLevelsChange(TObject *Sender);
	void __fastcall ActNewMapExecute(TObject *Sender);
	void __fastcall ActMapDelExecute(TObject *Sender);
	void __fastcall ActMapChangeIndexExecute(TObject *Sender);
	void __fastcall ActMapOptionsExecute(TObject *Sender);
	void __fastcall ActAnimTypesExecute(TObject *Sender);
	void __fastcall ActTileOptionsExecute(TObject *Sender);
	void __fastcall ListView_uMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall ActUndoExecute(TObject *Sender);
	void __fastcall ActBGCopyExecute(TObject *Sender);
	void __fastcall ActBGPasteExecute(TObject *Sender);
	void __fastcall ActBGPasteMultiExecute(TObject *Sender);
	void __fastcall ActDelSelectionExecute(TObject *Sender);
	void __fastcall MDScriptsClick(TObject *Sender);
	void __fastcall MDResetAllClick(TObject *Sender);
	void __fastcall Timer_crTimer(TObject *Sender);
	void __fastcall DbgTrigActionsTraceClick(TObject *Sender);
	void __fastcall PaintBox1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall ActSelectAllExecute(TObject *Sender);
	void __fastcall ActShowHintExecute(TObject *Sender);
	void __fastcall ActCollideNodesModeExecute(TObject *Sender);
	void __fastcall ActGoToBGReferenceExecute(TObject *Sender);
	void __fastcall ActionList1Execute(TBasicAction *Action, bool &Handled);
	void __fastcall ActEditorOptExecute(TObject *Sender);
	void __fastcall TabSetObjClick(TObject *Sender);
	void __fastcall ActXMLSaveExecute(TObject *Sender);
	void __fastcall ActCloneMapExecute(TObject *Sender);
	void __fastcall PMPTAddTabClick(TObject *Sender);
	void __fastcall PMPTRenameTabClick(TObject *Sender);
	void __fastcall ActEventTypesExecute(TObject *Sender);
	void __fastcall ActProjSaveExecute(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall ActNewCollideShapeExecute(TObject *Sender);
	void __fastcall ActNewCollideNodeExecute(TObject *Sender);
	void __fastcall FormKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall ActMapSaveToExecute(TObject *Sender);
	void __fastcall ActAnimJNCountExecute(TObject *Sender);
	void __fastcall ActLockGameObjectExecute(TObject *Sender);

private:

		enum{mapObj, parentObj, triggerObj};
		enum{lstProj, lstMap, lstPalette, lstForReload, lstBackObjOnly, lstMapObjOnly, lstAllMaps};
		enum MoveOrCreateCollideNodeMode
		{
			mccnNewShape,
			mccnAddNode,
			mccnInsertNode
		};

		TPlatformGameField      *m_pGameField;

		bool            proj_exist, map_exist, ScriptsWndAlreadyExist, disabled_controlls;

        TObjManager     *m_pObjManager;
		TObjManager     *m_pTempObjManager;
        TList           *m_pLevelList;
		TCommonResObjManager *mainCommonRes;

		TCommonTextManager *mpTexts;
		UnicodeString mCurrentLanguage;
		UnicodeString mCurrentCollideChainShapeName;
		//{ ��� �����-----------------------------------------------------------
		bool            showCollideMap;
        TRect           crCursor;  //������
        bool            crVisible; //����� �� ������
        bool            objDrag;   //����� �� ������� ������� (TImage->Visible=false|true)
        
		__int32			znCurZone, oldZone; //����
		BMPImage		*mpBackPatternBmp;
        //} ��� �����-----------------------------------------------------------

        //{ ������� �������� ����-----------------------------------------------
        TCustomImageList        *images_u; //��� ����������� � ListBoxs
        bool                    showObjects; //���������� ������
        bool                    showRectGameObj; //���������� ������
        bool                    showOBJDirectors;
        //} ������� �������� ����-----------------------------------------------

        //{ �������� �������� ����-----------------------------------------------
		bool                    aniAniProcessed;
		TDateTime				mTickPreviousTime;
		unsigned int			mTickDuration;
		unsigned int			mRenderTickDuration;
		unsigned int			mFPSTimer;
		int						mFPSCounter;
		int						mFPSVal;
		bool					mShowFPS;
		bool					mExitApp;
		bool					mAnimationEnable;
		bool					mAnimationStateBeforeEmul;

		//} �������� �������� ����-----------------------------------------------
		short			p_w, p_h;  //������ � ������ ��������� �����
		__int32			lrCount; // ����
		__int32			m_w, m_h;   //������ � ������ ����� (� ��������)
		__int32			d_w, d_h;   //������ � ������ ������
		__int32			m_px, m_py; //������� ������� ������� �� �����

		__int32     drawObjMode;

        bool        mSaveProgDataXMLFormat;

		Graphics::TBitmap *mpBGBmp;
		Graphics::TBitmap *mpBackBufferBmp;
		TCanvas	*mpFinalRenderCanvas;
		TWndMethod mOldPaintBox1WndProc;

		__int32 mTileTmpBmp_idx;
		__int32 mObjGeneralErrorBmp_idx;

		TMapPerson			*mpLastDragObj;
		TList 				*DragObjList;

        bool            is_save;
		bool            LoadSaveProcess;
		bool            showGrid;
        TColor          gridColor;

		bool            drag_on_PaintBox;
		__int32			dr_mode, prev_dr_mode;

        bool            CBMouseEnter, PaintBox1MouseMove_process, DrawMapProcess;

		bool			mNeedRebuildBack;
		bool			mDoNotDraw;
		bool			mUpdateCursor;

        TList           *markerList;

        unsigned short  **m_CopyBGBuffer;
        __int32             m_CopyRectCX, m_CopyRectCY,
                        m_CopyRectW,  m_CopyRectH;
        TRect           m_CopyRect;
        bool            m_CopyRectSet;
		int				mHintTime;
		TPoint			mHintCoordinates;
		THintWindow 	*mpHintWnd;

        TMemoryStream	*mpCopyBuffer;

        TRenderGLForm	*mpCurrentGLForm;

		__int32			mDirAssetIndex;

		int				mGameObjListViewCursorPos[PARENT_TYPE_COUNT_MAX];
		int				mGameObjListViewCount;
		int				mPrevGameObjListViewCursorPos;

		int				mDelayCounterLoadTexturesToVRAMGL;

		void __fastcall IdleLoop(TObject*, bool& done);
		void __fastcall OnMessage(tagMSG &Msg, bool &Handled);
		void __fastcall AppMinimize(TObject*);
		void __fastcall AppRestore(TObject*);
		void __fastcall CustomPaintBox1WndMethod(Winapi::Messages::TMessage &Message);

		void __fastcall TabSetObjInit();

		void __fastcall DeleteCopyBGBuffer();
        void __fastcall ClearCopySelection();
		void __fastcall SelectAll();
		void __fastcall UnselectAll();
		void __fastcall ShowObjHint(bool autoHint);

		void __fastcall ShowOpt(const EditMode mode);
		void __fastcall ApplyOpt();
		void __fastcall CancelOpt();

		void __fastcall CreateMap(bool rebuid, TObjManager *mo = NULL);
		bool __fastcall AddStaticToMap(bool del);

		void __fastcall DoTick();

		double __fastcall GetScaleByCBIndex(__int32 cb_idx);
		__int32 __fastcall GetCBIndexByScale(double scale);
		__int32 __fastcall SnapCoordinate(const __int32& coordinate, const __int32& snapSize);
		void __fastcall DrawGrids(TCanvas *c,TRect *r);
        void __fastcall DrawBackMap(__int32 xx, __int32 yy, bool paint);
        void __fastcall DrawBackMap();
        void __fastcall PaintMap();
		void __fastcall DrawCursor(TCanvas *c);
		void __fastcall SetCursorPosition(__int32 l, __int32 t, __int32 xl, __int32 xt);
		void __fastcall fillGrayIfEmpty(TCanvas *c);
		bool __fastcall is_Visible(const TMapPerson* mp);

        bool __fastcall SetDrMode(__int32 mode);

		TMapPerson* __fastcall newGObject(TObjManager *objManager,
											__int32 type,
											UnicodeString iParentName,
											double x, double y,
											__int32 zone,
											UnicodeString iName,
											TObjCreateMode iMode);

        __int32 __fastcall  AskToSave();
        void __fastcall RefreshFileName();

		void __fastcall ReadIniData(bool langmode = false);
		UnicodeString __fastcall CheckPathWithDelimeter(UnicodeString path, bool useDefaultPathifEmpty);
		void __fastcall EditorOptions(bool readOnly);
        void __fastcall checkMenuBackObj();
		void __fastcall checkMenuGameObj();
		void __fastcall CheckMenuCollideLines();

		void __fastcall changeSizes(); //������������

        //�������� ��� ������� � ���, ��� ����� �������
        void __fastcall clearAllObjects();

		std::list<TResourceItem> *__fastcall createImgListForSave(__int32 typeCreate, const TObjManager *objManager);
		std::list<TResourceItem> *__fastcall createStreamObjBufferList(TObjManager *objManager);
		// ������� ������� �������� �����
        void __fastcall ExportGameObjPalette(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h);
        // ������ ������� �������� �����
		void __fastcall ImportGameObjPalette(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, int ver_file);
		void __fastcall CheckForWrongHash(TAnimationFrameData& afd);

        // ������� ������� ���� �����
		void __fastcall ExportBackObjPalette(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, TList* altBackObjList);
        // ������ ������� ���� �����
		void __fastcall ImportBackObjPalette(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, int ver_file);

		void __fastcall ProjSave(bool askPath);
		void __fastcall DoThreadProcessProjSave();
		void __fastcall SaveXML();
		void __fastcall ProjLoad(UnicodeString filename, bool commandline = false, UnicodeString *errmes = NULL);
		void __fastcall DoThreadProcessProjLoad();
		void __fastcall MapSave(const UnicodeString *path = NULL, bool commandline = false, UnicodeString *errmes = NULL);
		void __fastcall SaveObjForMapSave(const TMapGPerson *gp,
											const TObjManager *objManager,
											const std::list<PropertyItem>* lvars,
											const std::list<TSaveResItem> &font_res);
		void __fastcall DoThreadProcessMapSave();
        //��������� ��� �������, ������� ��������� � �������
		void __fastcall SaveResArray(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, TObjManager *objManager);
        // ������� ���� ���� ��������� png-������ � ������ ��������-��������, ����������� � ���
        std::list<TResourceItem> *__fastcall LoadResArray(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, int ver);
        //��������� ���������� �������� obj_type -> mapObj||parentObj
		void __fastcall SaveObjVarValues(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, int obj_type, TObjManager *objManager);
        //��������� ���������� �������� obj_type -> mapObj||parentObj, ver_file ������� �� ��������� �����
		bool __fastcall LoadObjVarValues(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, int obj_type, int ver, TObjManager *objManager);
		// ��������� ���������� � frame obj
		void __fastcall SaveFrameObjInf(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h);
		// ��������� ���������� � frame obj
		//int __fastcall LoadFrameObjInfOld(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, hash_t **harray, int ver_file);
		void __fastcall LoadFrameObjInf(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, int ver_file);

		// ������/multilanguage
		void __fastcall SaveProjTexts(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h);
		void __fastcall LoadProjTexts(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, int ver_file);

		void __fastcall LoadMainPattern(UnicodeString name, UnicodeString *mes);

		void __fastcall SaveGameObjectTo(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, const TMapPerson *gp);

		TMapPerson* __fastcall LoadGameObjectFrom(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, unsigned char ver, TObjManager* ipObjManager);
		void __fastcall GenerateNameListForAutoNamePoolObj(TObjManager *m);
		UnicodeString __fastcall AutoNamePoolObj(UnicodeString parentName);
		void __fastcall ClearNameListForAutoNamePoolObj();

        void __fastcall CreateOptimizedBackObjListAndMap(TList *list, unsigned short *tmap, TObjManager *objManager, wchar_t **outtextmap, size_t *chdata_size);
		void __fastcall CreateOptimizedGameObjList(TList *list, TObjManager *objManager);

        //�������� ��� �������� ��. ����
        void __fastcall editBackObj(__int32 mode,__int32 idx,GMapBGObj *BGobj);
        //����������� ��������(�������� ������, ����� ���������� ������� �������� ���� pw � ph)
        void __fastcall rebuildBackObjListView();
        //��������� �������� � ��������� ���������
        void __fastcall editBackObjListViewBmp(__int32 mode, __int32 idx, const GMapBGObj& bgobj, UnicodeString caption);
        //������� ������� ��� ���������
        UnicodeString __fastcall createCaption(const GMapBGObj &MapBGobj);
        //���������, �� ���������� �� ��� ����� ��������� �� ���� ����� c ����� �� �����
        //__int32 __fastcall  checkIfExists(GMapBGObj *testBGobj);
        //������� ��� ������ � �����
        void __fastcall backObjAddEditDel(__int32 mode);
		//������������� �������� ��. ����
        void __fastcall RelinkBGobjProperties(bool rebuildListView);

        //������� ��� ������ � ������� ����
		void __fastcall gameObjAddEditDel(__int32 mode);
		bool __fastcall ParentObjAddEditDel(UnicodeString &gpname, int mode);
        //�������� ��� �������� ��. ����� � �������� ������ �������� �����
        //void __fastcall editGameObj(int mode, int idx, GPerson *Gpers);
        //��������� �������� � ��������� ���������
        void __fastcall editGameObjListViewBmp(TListItem* li, GPerson *Gpers, UnicodeString caption);
		bool __fastcall makeObjIcon(const GPerson *Gpers, BMPImage *bmp, TCommonResObjManager *cr);
		//����������� ��������� ��������
        void __fastcall rebuildGameObjListView();
		//������ �������� ���� (� ��� ����� ��������)
		void __fastcall ProcessLogicGameObjectsTick(unsigned __int32 ms);
		//������������ ��� ������� ���� �� �����
        void __fastcall repaintGameObjects();
        //����������� ������� ����� (���-�� ����������)
        void __fastcall rebuildGameObjects();
        //�������� ������� � ��������
        void __fastcall AddSpecialObj();
        //������������� ��������� ��������
        void __fastcall RelinkGameObjStates();

		//�������� � ������
        void __fastcall SetZoneVisibility();

        // ����������� ������� ������������ TMapGPerson �� Panel
        void __fastcall ReorderPanelComponents();

		void __fastcall ShowLoadErrorMessages(UnicodeString &errstr);

		//�������� ������� � ������
        void __fastcall AddNewLevelToProj(TObjManager *objManager);
        void __fastcall SetActiveProjLevel(__int32 idx);

		void __fastcall SaveAllToMemory();
        void __fastcall RestoreAllFromMemory(bool redraw);

        void __fastcall  FindAndShowObject(TMapPerson *obj);

		void __fastcall CreateDebugPopupScriptList();
		void __fastcall FreeDebugPopupScriptList();
		void __fastcall CreatePopupMoveTo();
		void __fastcall FreePopupMoveTo();
		void __fastcall NPopupMoveToClick(TObject *Sender);

        // �������� �������� ������ (devug)
        void __fastcall AddMarker(TMapPerson *obj, TObjMarker::TObjMarkerType type);
        void __fastcall DelMarker(__int32 idx);
        void __fastcall ClearMarkers();

        void __fastcall DropObjOnDrag();

        void __fastcall SetScale(double iscale);

		void __fastcall ShowObjPrpToolWindow(void* obj, __int32 objType);
		void __fastcall HideObjPrpToolWindow();
		void __fastcall CheckAndSwapPrpWindows(TForm *newForm);

		void __fastcall CreateLanguageMenu();
		void __fastcall FreeLanguageMenu();
		void __fastcall MDLanguageItemClick(TObject *Sender);

		void __fastcall CreateRenderFrame(bool rebuildBack);
		void __fastcall ApplyRenderBuffer(HDC hdc = NULL);

		void __fastcall CopyGameObject();
		void __fastcall PasteGameObject();
		bool __fastcall ProcessSelectionWithMouse(TMapPerson* pt, TMouseButton Button, TShiftState Shift,
													const int& srcX, const int& srcY, bool& hasLockedObject);
		bool __fastcall ProcessTilesSelection();
		bool __fastcall ProcessObjectsSelection(const __int32& scrX, __int32& scrY);

		void __fastcall CancelCollideLine();

		void __fastcall MoveOrCreateCollideNode(MoveOrCreateCollideNodeMode mode);
		void __fastcall ApplyDraggedCollideNodes();

		void __fastcall DoThreadProcessMapInfo();

		void __fastcall InitPrpWindows(TCommonResObjManager *mcr);
		bool __fastcall IsAnyPrpWindowVisible();
		void __fastcall CreateServiceData();
		void __fastcall AssignGraphicToObjects(UnicodeString path, std::list<TResourceItem> *list, UnicodeString *errorlist);

		void __fastcall NormalizeMapCursor(double& x, double& y);
		void __fastcall UpdateMapCursorCoordinates(__int32& scrX, __int32& scrY, bool snapX, bool snapY);

public:

		__int32			lvWHSizeIcon; //������ ������ (h=w) � ������ ������ �������� �����
		unsigned int	tickDuration;  //�������� tick
		__int32			HorzScrollBar_Position, VertScrollBar_Position;

		WORD			CF_ME;

		UnicodeString	map_file_name,
						h_file_name,
						proj_file_name,
						xml_proj_file_name,
						file_name;

		TEGrWndParams	*mpGrBGWndParams, // ��������� ���� �������������� ������� �� ��� ������
						*mpGrObjWndParams;
		TEdPrObjWndParams *mpEdPrObjWndParams;

		UnicodeString Lang,
					PathIni,
					PathPngRes,
					PathPngRes0,
					PathPattern,
					PathObjPalette;

		bool __fastcall IsProjExist(){return proj_exist;}
		bool __fastcall IsMapExist(){return map_exist;}
		void __fastcall SetSaveFlag(){is_save = true;}

		bool __fastcall RefreshFonts();

		void __fastcall MakeMapBackup();
		void __fastcall CopyFromBGObjWindow(TGridRect iCopyRect);

		bool __fastcall TextMessagesManager(UnicodeString *id, bool buttons = false);

		const TCommonTextManager* __fastcall GetCommonTextManager();
		UnicodeString __fastcall GetCurrentLanguage();
		const TCommonResObjManager* __fastcall GetCurrentResObjManager() const;
		const TObjManager* __fastcall GetCurrentObjManager() const;

		//�������� �������������� �����-���������
		//!!�� ������ ����� ������������� ������� Free()!!!
		TFEditGrObj* __fastcall getSelectionBlock(TCommonResObjManager *commonResManager, bool allowChangePath);
		//��������� �������� ��� ������� ���������
		void __fastcall resizeBmpForListView(BMPImage* ibmp, __int32 w,__int32 h, double scale);
		//������� ������ (������) ����������� ���������
		void __fastcall makeEmptyPreview(BMPImage* ibmp, UnicodeString caption, __int32 w, __int32 h);

		void __fastcall CheckLevelForWarningsAndErrors(TStringList *str_list);

		void* __fastcall FindAndShowObjectByName(UnicodeString name, bool switchToObj);

		void __fastcall DragObjListAdd(TMapPerson* pt);
		void __fastcall DragObjListClear();
		__int32 __fastcall DragObjListCount();
		bool __fastcall DragObjListContains(TMapPerson* pt);
		void __fastcall RemoveDragObjFromList(const UnicodeString Name);
		void __fastcall RemoveDragObjFromList(const TMapPerson* pt);
		TMapPerson* __fastcall GetDragObjFromList(__int32 idx);
		TMapPerson* __fastcall GetLastSelectedDragObj() const;
		TMapPerson* __fastcall DragObjListAddMulti(const TRect &SelectionRect, bool UnselectDublicates);

		void __fastcall AddGlobalScriptsToAllLevels(bool newMap);
		void __fastcall AddGlobalTriggerToAllLevels(bool newMap);
		void __fastcall OnChangeCplxObjList(TMapGPerson* obj, UnicodeString oldParentObj);
		bool __fastcall OnChangeObjName(TMapPerson* obj, UnicodeString oldName);
		void __fastcall OnChangeObjZone(TMapGPerson* obj);
		void __fastcall OnChangeObjParameter();
		void __fastcall OnChangeObjPosition(TMapGPerson* obj, double x, double y);
		void __fastcall OnChangeDirectorName(TMapSpGPerson* obj, UnicodeString OldName);
		void __fastcall OnChangeDirectorNode(TMapSpGPerson* obj);
		void __fastcall OnScriptsWndHide();
		void __fastcall OnScriptStartClick(UnicodeString name);
		void __fastcall OnChangeTriggerName(TMapSpGPerson* obj, UnicodeString OldName);
		void __fastcall OnChangeTextId(TMapTextBox *obj);
		void __fastcall OnChangeTextObjSizes(TMapTextBox *obj, __int32 w, __int32 h);

		__int32 __fastcall getBG_H(){return p_h;}
		__int32 __fastcall getBG_W(){return p_w;}

		void __fastcall ObjectsChanged();

		void __fastcall CreateZoneList();

		void __fastcall  AddDebugMessage(UnicodeString mes, void* obj);
		TMapPerson* __fastcall FindObjectPointer(const UnicodeString& name);
		void* __fastcall FindObjectPointer(__int32 srcX, __int32 srcY, __int32 &dx, __int32 &dy, bool useParentIntersection);
		void __fastcall  ChangeZone(__int32 newZone);
		void __fastcall  DrawMap(bool rebuildBack);   //draw notification
		void __fastcall  DbgOnChangeState(TMapGPerson *mp);
		void __fastcall  DbgStopTimer();
		void* __fastcall DbgGetFocusedObj();

		void __fastcall InitRenderGL();
		void __fastcall ReleaseRenderGL();
		void __fastcall LoadTexturesToVRAMGL();
		void __fastcall SetChildRenderGLForm(TRenderGLForm* pForm);

		void __fastcall GetTileSize(short &w, short &h) const;
		__int32 __fastcall GetLayersCount() const;
		void __fastcall GetMapSize(__int32 &w, __int32 &h) const;
		void __fastcall GetDisplaySize(__int32 &w, __int32 &h) const;

		BMPImage* __fastcall CreatePatternImage(TColor color);
		__int32 __fastcall GetGeneralObjErrorResIdx();

		__fastcall TForm_m(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm_m *Form_m;
//---------------------------------------------------------------------------
#endif
