object Form_opt: TForm_opt
  Left = 296
  Top = 208
  BorderStyle = bsDialog
  Caption = 'mSettings'
  ClientHeight = 322
  ClientWidth = 521
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Pitch = fpFixed
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 228
    Top = 289
    Width = 147
    Height = 25
    Caption = 'mApply'
    ModalResult = 1
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 147
    Top = 289
    Width = 75
    Height = 25
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 1
    OnClick = Button2Click
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 521
    Height = 281
    ActivePage = TabMain
    Align = alTop
    TabOrder = 2
    ExplicitWidth = 511
    object TabMain: TTabSheet
      Caption = 'mMainProperties'
      ExplicitWidth = 503
      object GroupBox_m: TGroupBox
        Left = 19
        Top = 126
        Width = 233
        Height = 115
        Caption = 'mMapSizeInScreensGroupName'
        TabOrder = 1
        object Label7: TLabel
          Left = 10
          Top = 52
          Width = 36
          Height = 13
          Caption = 'mWidth'
        end
        object Label8: TLabel
          Left = 11
          Top = 28
          Width = 39
          Height = 13
          Caption = 'mHeight'
        end
        object Label10: TLabel
          Left = 8
          Top = 77
          Width = 64
          Height = 13
          Caption = 'mSizeInPixels'
        end
        object Label11: TLabel
          Left = 137
          Top = 78
          Width = 3
          Height = 13
        end
      end
      object GroupBox_d: TGroupBox
        Left = 19
        Top = 8
        Width = 233
        Height = 112
        Caption = 'mMapSizeInTilesGroupName'
        TabOrder = 0
        object Label3: TLabel
          Left = 10
          Top = 52
          Width = 36
          Height = 13
          Caption = 'mWidth'
        end
        object Label4: TLabel
          Left = 11
          Top = 28
          Width = 39
          Height = 13
          Caption = 'mHeight'
        end
        object Label5: TLabel
          Left = 8
          Top = 77
          Width = 64
          Height = 13
          Caption = 'mSizeInPixels'
        end
        object Label6: TLabel
          Left = 137
          Top = 78
          Width = 3
          Height = 13
        end
      end
      object GroupBox_zone: TGroupBox
        Left = 258
        Top = 8
        Width = 233
        Height = 112
        Caption = 'mZonesGroupName'
        TabOrder = 2
        object Label12: TLabel
          Left = 11
          Top = 28
          Width = 88
          Height = 13
          Caption = 'mZonesCountDots'
        end
        object Label9: TLabel
          Left = 11
          Top = 47
          Width = 214
          Height = 44
          AutoSize = False
          Caption = 'mZonesHint'
          WordWrap = True
        end
      end
      object GroupBox_res: TGroupBox
        Left = 258
        Top = 126
        Width = 233
        Height = 115
        Caption = 'mMapResoures'
        TabOrder = 3
        object Label2: TLabel
          Left = 11
          Top = 48
          Width = 214
          Height = 64
          AutoSize = False
          Caption = 'mIgnoreUnoptimizedResouresHint'
          WordWrap = True
        end
        object CBmIgnoreUnoptimizedResoures: TCheckBox
          Left = 13
          Top = 17
          Width = 204
          Height = 30
          Caption = 'mIgnoreUnoptimizedResoures'
          TabOrder = 0
          WordWrap = True
        end
      end
    end
    object TabFirstTime: TTabSheet
      Caption = 'mOnStartSettings'
      ImageIndex = 4
      ExplicitWidth = 503
      object Label18: TLabel
        Left = 25
        Top = 51
        Width = 98
        Height = 13
        Caption = 'mScriptOnStartLevel'
      end
      object Label19: TLabel
        Left = 25
        Top = 21
        Width = 62
        Height = 13
        Caption = 'mActiveZone'
      end
      object Label20: TLabel
        Left = 25
        Top = 66
        Width = 253
        Height = 40
        AutoSize = False
        Caption = 'mScriptOnStartLevelHint'
        WordWrap = True
      end
      object Label1: TLabel
        Left = 25
        Top = 148
        Width = 69
        Height = 13
        Caption = 'mPaletteName'
      end
      object EdFirstScript: TEdit
        Left = 341
        Top = 48
        Width = 132
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 32
        TabOrder = 0
        OnChange = EdFirstScriptChange
      end
      object CheckBoxTileCameraOnStart: TCheckBox
        Left = 25
        Top = 112
        Width = 312
        Height = 17
        Caption = 'mSetCameraOnTileNo'
        TabOrder = 1
        OnClick = CheckBoxTileCameraOnStartClick
      end
      object EditPaletteColor: TEdit
        Left = 341
        Top = 145
        Width = 132
        Height = 21
        MaxLength = 32
        TabOrder = 2
      end
    end
  end
end
