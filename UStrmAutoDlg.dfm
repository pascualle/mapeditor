object FStrmAutoDlg: TFStrmAutoDlg
  Left = 227
  Top = 108
  BorderStyle = bsDialog
  ClientHeight = 250
  ClientWidth = 277
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  DesignSize = (
    277
    250)
  PixelsPerInch = 96
  TextHeight = 13
  object RadioGroupFrames: TRadioGroup
    Left = 30
    Top = 14
    Width = 216
    Height = 65
    Caption = 'mFileList'
    ItemIndex = 0
    Items.Strings = (
      'mAllFiles'
      'mSelectedFilesOnly')
    TabOrder = 0
  end
  object RadioGroupDirection: TRadioGroup
    Left = 30
    Top = 85
    Width = 216
    Height = 65
    Caption = 'mAutoCreateAnimationDirectionCaption'
    ItemIndex = 0
    Items.Strings = (
      'mTopDown'
      'mDownUp')
    TabOrder = 1
  end
  object CheckBox1: TCheckBox
    Left = 30
    Top = 160
    Width = 161
    Height = 17
    Caption = 'mFrameTime'
    TabOrder = 2
    OnClick = CheckBox1Click
  end
  object Edit_time: TEdit
    Left = 192
    Top = 160
    Width = 54
    Height = 21
    MaxLength = 3
    NumbersOnly = True
    TabOrder = 3
    Text = '0'
  end
  object ButtonCancel: TButton
    Left = 37
    Top = 216
    Width = 76
    Height = 26
    Anchors = [akLeft, akBottom]
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 4
  end
  object ButtonOk: TButton
    Left = 119
    Top = 216
    Width = 121
    Height = 26
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'mApply'
    ModalResult = 1
    TabOrder = 5
  end
end
