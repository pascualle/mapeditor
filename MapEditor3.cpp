//---------------------------------------------------------------------------

#include <tchar.h>
#include <vcl.h>
#pragma hdrstop
#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>

USEFORM("UOppositeState.cpp", FOppState);
USEFORM("UObjProperty.cpp", FGOProperty);
USEFORM("UObjPoolPropertyEdit.cpp", FOPPEdit);
USEFORM("UObjPoolProperty.cpp", FObjPoolProperty);
USEFORM("UnitScale.cpp", FormScale);
USEFORM("UnitOpt.cpp", Form_opt);
USEFORM("UObjPattern.cpp", FObjPattern);
USEFORM("UObjParent.cpp", FObjParent);
USEFORM("UObjOrder.cpp", FObjOrder);
USEFORM("UTextManager.cpp", FormTextManager);
USEFORM("UStrmAutoDlg.cpp", FStrmAutoDlg);
USEFORM("UStateList.cpp", FStateList);
USEFORM("UState.cpp", FEditState);
USEFORM("UWait.cpp", FWait);
USEFORM("UTxtObjProperty.cpp", FormTxtObjProperty);
USEFORM("UScript.cpp", FScript);
USEFORM("USaveVarList.cpp", FSaveVarList);
USEFORM("USriptList.cpp", FScriptList);
USEFORM("USpObjProperty.cpp", FSpObjProperty);
USEFORM("USndObjPropertyEdit.cpp", FSOPEdit);
USEFORM("USndObjProperty.cpp", FSndObjProperty);
USEFORM("UAbour.cpp", Form_about);
USEFORM("UCreateMessages.cpp", FMessages);
USEFORM("UCollideNodeProperty.cpp", FCollideNodeProperty);
USEFORM("UBGOType.cpp", FEditBGOType);
USEFORM("UBGEditWindow.cpp", FBGObjWindow);
USEFORM("MainUnit.cpp", Form_m);
USEFORM("UnitJNCount.cpp", FJNCount);
USEFORM("UnitFindObj.cpp", FormFindObj);
USEFORM("UnitEditorOpt.cpp", FEditorOptions);
USEFORM("UEditObjPattern.cpp", FOPEdit);
USEFORM("UEditObj.cpp", FEditGrObj);
USEFORM("UDirector.cpp", FDirectorProperty);
USEFORM("UDefProp.cpp", FDefProperties);
USEFORM("UFTileSizes.cpp", FTileSizes);
USEFORM("UFragmentProperty.cpp", FBOProperty);
USEFORM("UFPath.cpp", FProjPath);
USEFORM("UFileSelect.cpp", FFileSelect);
//---------------------------------------------------------------------------
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd)
{
  (void)hInstance;
  (void)hPrevInstance;
  (void)lpCmdLine;
  (void)nShowCmd;
  try
  {
    Application->Initialize();
    Application->Title = "Map Editor 3";
    Application->CreateForm(__classid(TForm_m), &Form_m);
		Application->CreateForm(__classid(TFObjOrder), &FObjOrder);
		Application->CreateForm(__classid(TFGOProperty), &FGOProperty);
		Application->CreateForm(__classid(TFSpObjProperty), &FSpObjProperty);
		Application->CreateForm(__classid(TFormTxtObjProperty), &FormTxtObjProperty);
		Application->CreateForm(__classid(TFDirectorProperty), &FDirectorProperty);
		Application->CreateForm(__classid(TFSndObjProperty), &FSndObjProperty);
		Application->CreateForm(__classid(TFObjPoolProperty), &FObjPoolProperty);
		Application->CreateForm(__classid(TFCollideNodeProperty), &FCollideNodeProperty);
		Application->CreateForm(__classid(TFBGObjWindow), &FBGObjWindow);
		Application->CreateForm(__classid(TFormFindObj), &FormFindObj);
		Application->Run();
  }
  catch (Exception &exception)
  {
    Application->ShowException(&exception);
  }
  return 0;
}
//---------------------------------------------------------------------------
