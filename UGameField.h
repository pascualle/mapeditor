#ifndef UGameFieldH
#define UGameFieldH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include "Graphics.hpp"

class TMapGPerson;
class TObjPattern;
class TMapDirector;
class TCommonResObjManager;
class TObjManager;

//---------------------------------------------------------------------------

namespace PredefProperties
{
  enum Properties
  {
	PRP_ENABLE		= 0,
	PRP_GRAVITY,
	PRP_SPEED,
	PRP_COUNT
  };

  const char PropertyNames[PRP_COUNT][32]=
  {
	{"PRP_ENABLE\0"},
	{"PRP_GRAVITY\0"},
	{"PRP_SPEED\0"}
  };

  enum PrRect
  {
	RECT_TOP        = 0,
	RECT_LEFT       = 1,
	RECT_BOTTOM     = 2,
	RECT_RIGHT      = 3
  };
}; 
//---------------------------------------------------------------------------

struct TGameData
{
  __int32		StartZone,
				p_w,
				p_h;

  const TObjManager	*m_pObjManager;
  const TCommonResObjManager *m_pCommonResObjManager;
};

//---------------------------------------------------------------------------

class GameField
{
public:

 __fastcall GameField();
 __fastcall ~GameField();

 void __fastcall SetData(const TGameData *data);
 void __fastcall Repaint();

 virtual void __fastcall Process(TMapGPerson *pr, bool *next_frame) = 0;   // process timer

protected:

 UnicodeString __fastcall getPropString(int idx);
 int __fastcall GetStateDirection(UnicodeString name);

 TGameData    m_data;

 int            sz_mapW,        // sz_mapH=map_h*p_h;
                sz_mapH,        // sz_mapW=map_w*p_w;
                pnt_count,
                p_h,            // ������ ��������� �����
                p_w,            // ������ ��������� ����� (� ��������)
                map_h,          // ������ ����� � ������
                map_w,          // ������ ����� � �����x
                sz_map,         // map_w*map_h;
                m_curZone,
                scale;
};

//---------------------------------------------------------------------------

class TPlatformGameField : public GameField
{
public:

 __fastcall TPlatformGameField()/*:traceScript(false)*/{hero = NULL;}
 __fastcall ~TPlatformGameField(){};

 void __fastcall Process(TMapGPerson *pr, bool *next_frame);

 void __fastcall StartScript(UnicodeString name, UnicodeString headerString,
									void* Owner, void* Second, bool checkMultiple, bool forceTrace);

 // pr - ���������
 // opr -- ������ ��������
 // TileUniqIdx -- ���� >=0 ��� �������� ��� ��������� ������� � ���������� �����
 bool __fastcall OnCollide(TMapGPerson *pr, TMapGPerson *opr, int TileUniqIdx = -1);
 bool __fastcall OnEndAnimation(TMapGPerson *mp);
 bool __fastcall OnChangeValue(TMapGPerson *pt, UnicodeString prpname);
 bool __fastcall OnChangeNode(TMapGPerson *mp, TMapDirector *dir);

 //bool traceScript;
 bool stopOnStartScript;

 TMapGPerson  *hero;

private:

 bool tileOnceCollide;

 //pr functions
 //void __fastcall prSetTop(int val, TMapGPerson *p);
 //void __fastcall prSetLeft(int val, TMapGPerson *p);
 void __fastcall prSetState(TMapGPerson *p, UnicodeString stateName);
 void __fastcall prSetVarLalue(TMapGPerson *p, UnicodeString varname, double val);
 void __fastcall prSetOppositeState(TMapGPerson *p, UnicodeString state);
 bool __fastcall prCheckBGDirector(int pos, TMapGPerson *pr);
 void __fastcall getPrCollideRect(TMapGPerson *p, int *rect);
 void __fastcall getPrViewRect(TMapGPerson *p, int *rect);
 void __fastcall prAddXYToPosition(TMapGPerson *pr, double x, double y);
 bool __fastcall prLookForObj(TMapGPerson *p, int footY, int faceY);
 int __fastcall  prDistanseForObj(TMapGPerson *pr, TMapGPerson *opr);

  //main
  bool __fastcall DoActions(TMapGPerson *pr, bool *next_frame);
  void __fastcall DoLogic(TMapGPerson *pr);
  bool __fastcall SetNextDirectorPos(TMapGPerson *pr);

 //map functions
 void __fastcall checkToMove(TMapGPerson *pr, int *omap_type, int x, int y, int check);
 int __fastcall  getYFragment(int y,unsigned char align);
 int __fastcall  getXFragment(int x,unsigned char align);

 void __fastcall DoTrigger(UnicodeString name, void *Owner, void *Second, bool forceTrace = false);

};
//---------------------------------------------------------------------------
#endif

