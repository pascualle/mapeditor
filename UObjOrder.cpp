//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UObjOrder.h"
#include "UObjCode.h"
#include "UObjManager.h"
#include "MainUnit.h"
#include "ULocalization.h"
#include "UMapCollideLines.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TFObjOrder *FObjOrder;

static const int IMG_VISIBLE_IDX = 1;
static const int IMG_VISIBLE_LOCK_IDX = 3;
static const int IMG_UNVISIBLE_IDX = 2;
static const int IMG_UNVISIBLE_LOCK_IDX = 4;
static const int IMG_LOCK_IDX = 5;
static const int IMG_UNLOCK_IDX = 6;

//---------------------------------------------------------------------------
__fastcall TFObjOrder::TFObjOrder(TComponent* Owner)
 : TForm(Owner)
 , m_pObjManager(NULL)
 , mpCurrentLst(NULL)
{
	mMode = MODE_OBJ;
	mRefreshSelection = false;
	mCreateList = false;
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::Localize()
{
	Localization::Localize(ActionList1);
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::SetObjManager(TObjManager *om)
{
    assert(om);
	m_pObjManager = om;
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::FormDestroy(TObject */*Sender*/)
{
	mBucket.clear();
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::ComboBoxChange(TObject */*Sender*/)
{
	assert(m_pObjManager);
	mMode = static_cast<TOrderMode>(ComboBox->ItemIndex);
	switch(mMode)
	{
		case MODE_OBJ:
			mpCurrentLst = m_pObjManager->map_u;
			break;
		case MODE_DECOR_BACK:
			mpCurrentLst = m_pObjManager->decor_b;
			break;
		case MODE_DECOR_FORE:
			mpCurrentLst = m_pObjManager->decor_f;
			break;
		case MODE_TRIGGER:
			mpCurrentLst = m_pObjManager->triggers;
			break;
		case MODE_DIRECTOR:
			mpCurrentLst = m_pObjManager->directors;
			break;
		case MODE_COLLIDESHAPE:
			mpCurrentLst = m_pObjManager->collideshapes;
	}
	UpdateButtonsState();
	CreateList();
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::RebuildList()
{
	if(Visible)
	{
		ComboBoxChange(NULL);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::FormShow(TObject */*Sender*/)
{
	ComboBoxChange(NULL);
}
//---------------------------------------------------------------------------

const TFObjOrder::TListNodeItem* __fastcall TFObjOrder::FindInBucket(const TTreeNode *node) const
{
	std::list<TListNodeItem>::const_iterator it;
	for (it = mBucket.begin(); it != mBucket.end(); ++it)
	{
		if(it->node == node)
		{
			return &(*it);
		}
	}
	return nullptr;
}
//---------------------------------------------------------------------------

TTreeNode* __fastcall TFObjOrder::FindInBucket(const UnicodeString name) const
{
	std::list<TListNodeItem>::const_iterator it;
	for (it = mBucket.begin(); it != mBucket.end(); ++it)
	{
		if(it->name == name)
		{
			return it->node;
		}
	}
	return nullptr;
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::RebuildCurrentPointerList()
{
	if(mMode == MODE_COLLIDESHAPE)
	{
		return;
	}
	__int32 i, j;
	void *ptr;
	assert(mpCurrentLst);
	TList *tlist = new TList();
	const __int32 len = TreeView->Items->Count;
	for(i = 0; i < len; i++)
	{
		const TListNodeItem *item = FindInBucket(TreeView->Items->Item[i]);
		assert(item);
		ptr = Form_m->FindObjectPointer(item->name);
		if(ptr != NULL)
		{
			tlist->Add(ptr);
		}
	}
	mpCurrentLst->Clear();
	mpCurrentLst->Assign(tlist, laCopy, NULL);
	delete tlist;
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::CreateList()
{
	__int32 i;
	UnicodeString caption;
	TTreeNode *cur;

	mCreateList = true;

	TTreeNodes *nodes = TreeView->Items;

	mBucket.clear();
	nodes->Clear();

	assert(m_pObjManager);
	assert(mpCurrentLst);

	if(mMode != MODE_COLLIDESHAPE)
	{
		for(i = 0; i < mpCurrentLst->Count; i++)
		{
			void* mo = mpCurrentLst->Items[i];
			if(mMode <= MODE_DECOR_FORE)
			{
				const TMapGPerson *mp = (TMapGPerson*)mo;
				if(!mp->ParentObj.IsEmpty())
				{
					continue;
				}
				AddNodeBranch(mp, nullptr);
			}
			else
			{
				TListNodeItem nitem;
				TMapSpGPerson *mp = (TMapSpGPerson*)mo;
				if(mMode == MODE_TRIGGER)
				{
					caption = mp->Name + _T(" (") + Localization::Text(_T("mZone")) + _T(": ") + mp->GetZone() + _T(")");
				}
				else
				{
					caption = mp->Name;
				}
				cur = nodes->Add(nullptr, caption);
				cur->StateIndex = -1;
				cur->StateIndex = GetImgStatusIdx(mp);
				nitem.name = mp->Name;
				nitem.node = cur;
				mBucket.push_back(nitem);
			}
		}
		StatusBar1->SimpleText = Localization::Text(_T("mCount")) + _T(": ") + IntToStr(mpCurrentLst->Count);
	}
	else // MODE_COLLIDESHAPE
	{
		for(i = 0; i < mpCurrentLst->Count; i++)
		{
			const TCollideChainShape *sh = static_cast<const TCollideChainShape *>(mpCurrentLst->Items[i]);
			AddNodeBranch(sh, nullptr);
		}
		StatusBar1->SimpleText = Localization::Text(_T("mCount")) + _T(": ") + IntToStr(m_pObjManager->GetObjTypeCount(TObjManager::COLLIDESHAPE));
	}

	RebuildSelectionList();
	mCreateList = false;
}
//---------------------------------------------------------------------------

const int& __fastcall TFObjOrder::GetImgStatusIdx(const TMapGPerson *mp) const
{
	if(mMode == MODE_COLLIDESHAPE)
	{
		return mp->IsLocked() ? IMG_LOCK_IDX : IMG_UNLOCK_IDX;
	}
	else
	{
		return mp->CustomVisible ?
					(mp->IsLocked() ? IMG_VISIBLE_LOCK_IDX : IMG_VISIBLE_IDX) :
					(mp->IsLocked() ? IMG_UNVISIBLE_LOCK_IDX : IMG_UNVISIBLE_IDX);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::AddNodeBranch(const TCollideChainShape *sh, TTreeNode *root)
{
	TTreeNode *cur;
	TTreeNodes *nodes = TreeView->Items;
	TListNodeItem nitem;
	UnicodeString caption = sh->Name;
	root = nodes->Add(root, caption);
	nitem.name = sh->Name;
	nitem.node = root;
	mBucket.push_back(nitem);
	const TCollideChainShape::TCollideNodeList& list = sh->GetNodeList();
	for(TCollideChainShape::TCollideNodeList::const_iterator it = list.begin(); it != list.end(); ++it)
	{
		const TMapCollideLineNode& cn = *it;
		caption = cn.Name;
		cur = nodes->AddChild(root, caption);
		nitem.name = cn.Name;
		nitem.node = cur;
		mBucket.push_back(nitem);
		cur->StateIndex = -1;
		cur->StateIndex = GetImgStatusIdx(&cn);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::AddNodeBranch(const TMapGPerson *mp, TTreeNode *root)
{
	TTreeNodes *nodes = TreeView->Items;
	UnicodeString caption;
	TListNodeItem nitem;
	TTreeNode *cur;
	if(mp->ParentObj.IsEmpty())
	{
		caption = mp->Name + _T(" (") + Localization::Text(_T("mZone")) + _T(": ") + mp->GetZone() + _T(")");
		cur = nodes->Add(root, caption);
	}
	else
	{
		caption = mp->Name;
		cur = nodes->AddChild(root, caption);
	}
	nitem.name = mp->Name;
	nitem.node = cur;
	mBucket.push_back(nitem);
	cur->StateIndex = -1;
	cur->StateIndex = GetImgStatusIdx(mp);
	if(mp->IsChildExist())
	{
	  for(__int32 j = 0; j < MAX_ANIMATON_JOIN_NODES; j++)
	  {
		 UnicodeString chname = mp->GetChild(j);
		 if(!chname.IsEmpty())
		 {
			 TMapGPerson *cmp = (TMapGPerson*)Form_m->FindObjectPointer(chname);
			 if(cmp != NULL)
			 {
				AddNodeBranch(cmp, cur);
			 }
		 }
	  }
	  cur->Expand(true);
	}
}
//---------------------------------------------------------------------------

//ACTIONS
void __fastcall TFObjOrder::ActUpExecute(TObject *)
{
	if(mMode == MODE_COLLIDESHAPE)
	{
		return;
	}
 TTreeNode *sel = TreeView->Selected;
 if(sel == NULL || mCreateList)
 {
	return;
 }
 if(sel->Index == 0)
 {
	return;
 }
 __int32 idx = sel->AbsoluteIndex - 1;
 bool expanded = sel->Expanded;
 __int32 stIndex = sel->StateIndex;
 if(sel->Parent == NULL)
 {
	while(TreeView->Items->Item[idx]->Parent != NULL && idx != 0)
	{
		idx--;
    }
 }
 else
 {
     return;
 }
 {
	//���������� �� ������ ���������� ���� ���������� �����������
	TTreeNode *next_sel = TreeView->Items->Item[idx];
	const TListNodeItem* nitem = FindInBucket(sel);
	assert(nitem);
	TMapGPerson *ptr_current = (TMapGPerson *)Form_m->FindObjectPointer(nitem->name);
	nitem = FindInBucket(next_sel);
	assert(nitem);
	TMapGPerson *ptr_next = (TMapGPerson *)Form_m->FindObjectPointer(nitem->name);
	if( ptr_current->GetZone() != LEVELS_INDEPENDENT_IDX &&
		ptr_next->GetZone() == LEVELS_INDEPENDENT_IDX)
	{
		return;
	}
 }
 sel->MoveTo(TreeView->Items->Item[idx], naInsert);
 sel->StateIndex = -1;
 sel->StateIndex = stIndex;
 if(sel->HasChildren)
 {
	sel->Expanded = !expanded;
	sel->Expanded = expanded;
 }
 RebuildCurrentPointerList();
 Form_m->ObjectsChanged(); 
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::ActDownExecute(TObject *)
{
	if(mMode == MODE_COLLIDESHAPE)
	{
		return;
	}
	TTreeNode *next, *node, *next_sel;
	TTreeNode *sel = TreeView->Selected;
	if(sel == NULL || mCreateList)
	{
		return;
	}
	if(sel->Parent != NULL)
	{
		return;
	}
	bool expanded = sel->Expanded;
	__int32 stIndex = sel->StateIndex;
	next_sel = NULL;
	next = sel;
	for(__int32 i = 0; i < 2; i++)
	{
        do
		{
			if(next)
			{
				node = next->GetLastChild();
				if(node)
				{
					next = node->GetNext();
				}
				else
				{
					next = next->GetNext();
				}
			}
		}
		while(next && next->Parent != NULL);
		if(i == 0)
		{
			next_sel = next;
        }
	}
	if(sel == next)
	{
		return;
	}
	//���������� �� ������ ���������� ���� ���������� �����������
	if(next_sel)
	{
		const TListNodeItem* nitem = FindInBucket(sel);
		assert(nitem);
		TMapGPerson *ptr_current = (TMapGPerson *)Form_m->FindObjectPointer(nitem->name);
		nitem = FindInBucket(next_sel);
		assert(nitem);
		TMapGPerson *ptr_next = (TMapGPerson *)Form_m->FindObjectPointer(nitem->name);
		if( ptr_current->GetZone() == LEVELS_INDEPENDENT_IDX &&
			ptr_next->GetZone() != LEVELS_INDEPENDENT_IDX)
		{
			return;
		}
	}
	if(next == NULL)
	{
		if(next_sel != NULL)
		{
			sel->MoveTo(next_sel, naAdd);
		}
	}
	else
	{
		sel->MoveTo(next, naInsert);
	}
	if(sel->HasChildren)
	{
		sel->Expanded = !expanded;
		sel->Expanded = expanded;
	}
	sel->StateIndex = -1;
	sel->StateIndex = stIndex;
	RebuildCurrentPointerList();
	Form_m->ObjectsChanged();
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::FormKeyPress(TObject */*Sender*/, char &Key)
{
	if(Key == VK_ESCAPE)
	{
		Close();
    }
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::ListBoxKeyDown(TObject */*Sender*/, WORD &Key,
	  TShiftState Shift)
{
	if(Shift.Contains(ssCtrl))
	{
		if(Key == VK_UP)
		{
			Key = 0;
			ActUp->Execute();
		}
		else
		if(Key == VK_DOWN)
		{
			Key = 0;
			ActDown->Execute();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::Bt_OptionsClick(TObject */*Sender*/)
{
	ActOptions->Execute();
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::Bt_DeleteClick(TObject */*Sender*/)
{
	ActDelete->Execute();
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::ActOptionsExecute(TObject *Sender)
{
	Form_m->ActMapObjEditExecute(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::TreeViewDblClick(TObject *Sender)
{
	Form_m->ActMapObjEditExecute(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::ActDeleteExecute(TObject *Sender)
{
	Form_m->ActMapObjDelExecute(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::TreeViewChange(TObject */*Sender*/, TTreeNode */*Node*/)
{
	if(mCreateList || mRefreshSelection)
	{
		return;
    }

	UpdateButtonsState();

	Form_m->DragObjListClear();
	for(unsigned __int32 i = 0; i < TreeView->SelectionCount; ++i)
	{
		TTreeNode *sel = TreeView->Selections[i];
		if(sel == NULL)
		{
			continue;
		}
		const TListNodeItem* nitem = FindInBucket(sel);
		assert(nitem);
		Form_m->DragObjListAdd((TMapGPerson*)Form_m->FindObjectPointer(nitem->name));
		if(TreeView->SelectionCount == 1)
		{
			Form_m->FindAndShowObjectByName(nitem->name, false);
		}
	}
	Form_m->DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::TreeViewKeyPress(TObject *Sender, char &Key)
{
    if(Key == VK_RETURN)
	{
         Key = 0;
		 ActOptions->Execute();
    }
	TreeViewChange(Sender, NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::TreeViewMouseDown(TObject *Sender,
	  TMouseButton /*Button*/, TShiftState /*Shift*/, __int32 /*X*/, __int32 /*Y*/)
{
	TreeViewChange(Sender, NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::FormDeactivate(TObject */*Sender*/)
{
	TreeView->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::FormActivate(TObject */*Sender*/)
{
	RebuildSelectionList();
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::BtVisibilityClick(TObject */*Sender*/)
{
	if(mMode == MODE_COLLIDESHAPE)
	{
		return;
	}
	TMapGPerson *obj;
	TTreeNode *sel;
	for(unsigned int i = 0; i < TreeView->SelectionCount; ++i)
	{
		sel = TreeView->Selections[i];
		if(sel == NULL)
		{
			continue;
		}
		const TListNodeItem* nitem = FindInBucket(sel);
		assert(nitem);
		obj = (TMapGPerson*)Form_m->FindObjectPointer(nitem->name);
		obj->SetCustomVisible(!obj->CustomVisible);
        sel->StateIndex = -1;
		sel->StateIndex = GetImgStatusIdx(obj);
	}
	Form_m->DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::BtHideAllClick(TObject */*Sender*/)
{
	if(mMode == MODE_COLLIDESHAPE)
	{
		return;
	}
	assert(mpCurrentLst);
	for(int i = 0; i < mpCurrentLst->Count; ++i)
	{
		TMapGPerson *mp = static_cast<TMapGPerson*>(mpCurrentLst->Items[i]);
		mp->SetCustomVisible(false);
		TTreeNode* tn = FindInBucket(mp->Name);
		tn->StateIndex = -1;
		tn->StateIndex = GetImgStatusIdx(mp);
	}
	Form_m->DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::BtVisibleAllClick(TObject */*Sender*/)
{
	if(mMode == MODE_COLLIDESHAPE)
	{
		return;
	}
	assert(mpCurrentLst);
	for(int  i = 0; i < mpCurrentLst->Count; ++i)
	{
		TMapGPerson *mp = static_cast<TMapGPerson*>(mpCurrentLst->Items[i]);
		mp->SetCustomVisible(true);
		TTreeNode* tn = FindInBucket(mp->Name);
		tn->StateIndex = -1;
		tn->StateIndex = GetImgStatusIdx(mp);
	}
	Form_m->DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::ActionLockExecute(TObject *Sender)
{
	TMapGPerson *obj;
	TTreeNode *sel;
	for(unsigned int i = 0; i < TreeView->SelectionCount; ++i)
	{
		sel = TreeView->Selections[i];
		if(sel == NULL)
		{
			continue;
		}
		const TListNodeItem* nitem = FindInBucket(sel);
		assert(nitem);
		obj = static_cast<TMapGPerson*>(Form_m->FindObjectPointer(nitem->name));
		if(obj->GetObjType() != objMAPCOLLIDESHAPE)
		{
			obj->Lock(!obj->IsLocked());
			sel->StateIndex = -1;
			sel->StateIndex = GetImgStatusIdx(obj);
		}
	}
	Form_m->DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::ActionLockAllExecute(TObject *Sender)
{
	LockAll(true);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::ActionUnlockAllExecute(TObject *Sender)
{
	LockAll(false);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::LockAll(bool val)
{
	assert(mpCurrentLst);

	if(mMode == MODE_COLLIDESHAPE)
	{
		TList* list = new TList();
		for(int i = 0; i < mpCurrentLst->Count; ++i)
		{
			list->Clear();
			TCollideChainShape *sh = static_cast<TCollideChainShape*>(mpCurrentLst->Items[i]);
			sh->CopyNodeList(list);
			for(int j = 0; j < list->Count; j++)
			{
				TMapCollideLineNode *l = static_cast<TMapCollideLineNode*>(list->Items[j]);
				l->Lock(val);
				TTreeNode* tn = FindInBucket(l->Name);
				tn->StateIndex = -1;
				tn->StateIndex = GetImgStatusIdx(l);
			}
		}
        delete list;
	}
	else
	{
		for(int i = 0; i < mpCurrentLst->Count; ++i)
		{
			TMapGPerson *mp = static_cast<TMapGPerson*>(mpCurrentLst->Items[i]);
			mp->Lock(val);
			TTreeNode* tn = FindInBucket(mp->Name);
			tn->StateIndex = -1;
			tn->StateIndex = GetImgStatusIdx(mp);
		}
	}

	Form_m->DrawMap(false);
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::RebuildSelectionList()
{
	mRefreshSelection = true;
	bool found = false;
	TList *list = new TList();
	TreeView->Selected = NULL;
	for(__int32 i = 0; i < Form_m->DragObjListCount(); ++i)
	{
		if(TreeView->Items != NULL)
		{
			TTreeNode *cur = TreeView->Items->GetFirstNode();
			const TMapPerson* mo = Form_m->GetDragObjFromList(i);
			while(cur != NULL)
			{
				const TListNodeItem* nitem = FindInBucket(cur);
				if(nitem)
				{
					if(nitem->name.Compare(mo->Name) == 0)
					{
                        if(Active)
        				{
                            if(!TreeView->Focused())
                                TreeView->SetFocus();
                        }
                        else
                        {
                        	ActiveControl = TreeView;
                        }
						list->Add(cur);
						found = true;
						break;
					}
				}
				cur = cur->GetNext();
			}
		}
	}
	if(!found)
	{
        if(Active)
        {
			ComboBox->SetFocus();
        }
        else
        {
			ActiveControl = ComboBox;
        }
	}
	else
	{
		TreeView->Select(list);
	}
	delete list;
	mRefreshSelection = false;
	UpdateButtonsState();
}
//---------------------------------------------------------------------------

void __fastcall TFObjOrder::UpdateButtonsState()
{
	BtVisibility->Enabled = mMode != MODE_COLLIDESHAPE;
	BtVisibleAll->Enabled = mMode != MODE_COLLIDESHAPE;
	BtHideAll->Enabled = mMode != MODE_COLLIDESHAPE;
	ActUp->Enabled = TreeView->SelectionCount == 1 && mMode != MODE_COLLIDESHAPE;
	ActDown->Enabled = TreeView->SelectionCount == 1 && mMode != MODE_COLLIDESHAPE;
	Bt_trup->Enabled = ActUp->Enabled;
	Bt_trdown->Enabled = ActDown->Enabled;
}
//---------------------------------------------------------------------------

