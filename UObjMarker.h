//---------------------------------------------------------------------------

#ifndef UObjMarkerH
#define UObjMarkerH

class TMapPerson;

class TObjMarker
{
	public:

		enum TObjMarkerType
		{
			Attention,
			Lock
		};

		explicit __fastcall TObjMarker(TObjMarkerType type, int duration, TMapPerson* obj);

		const TMapPerson* __fastcall GetContainedObject() const;
		const int& __fastcall GetInitialDuratuion() const;
		const int& __fastcall GetCurrentCounter() const;
		const TObjMarkerType __fastcall GetType() const;

		bool __fastcall Tick();

	private:
		TObjMarkerType mType;
		int mDuration;
		int mInitialDuration;
		TMapPerson *mpObj;
};

//---------------------------------------------------------------------------
#endif
