//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UObjCode.h"
#include "UEditObj.h"
#include "UObjManager.h"
#include "UObjParent.h"
#include "MainUnit.h"
#include "UDefProp.h"
#include "URenderGL.h"
#include "Clipbrd.hpp"
#include "UStrmAutoDlg.h"
#include "ULocalization.h"
#include "math.hpp"
#include <algorithm>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFObjParent *FObjParent;
//---------------------------------------------------------------------------

static const UnicodeString ANIMATION_SPEED_MILLISECONDS = _T("ms\0");
static UnicodeString CREATE_ANIMATION_MESSAGE;
static UnicodeString CREATE_EMPTY_FRAME_MESSAGE;

static const __int32 MINIMAL_TIMER_DURATION = 5;

static const __int32 MIN_RESIZE = 1;
static const __int32 MAX_RESIZE = 8192;

static const __int32 RENDER_DELAY_MS = 30;

static BMPImage *gspErrBmp = NULL;

__fastcall TFObjParent::TFObjParent(TComponent *Owner)
: TForm(Owner)
,	mDontCareSelection(false)
,	mTraceAnimation(false)
,	mpGameObj(NULL)
,	mAnimationFrameIdx(-1)
,	mpAnimation(NULL)
,	mDisableGridControls(false)
,	mPopupX(0)
,	mPopupY(0)
,	mFromPopup(false)
,	mDrawAnimations(false)
,	mDrawLogics(false)
,	mCheckBoxesProcessing(false)
,	mAnimatonDirection(0)
,	mScale(1.0f)
,   mJoinNodesCount(DEF_ANIMATON_JOIN_NODES)
,	mJoinNodeInited(false)
{
    int i;
	mpTempGameObj = new GPerson();
	mpTempResObjManager = new TCommonResObjManager();

	mpResList = new TList();

	CB_AniType->Enabled = false;
	PanelLeft->Enabled = false;
	Bt_propdef->Enabled = false;
	FrameGrid->Enabled = false;
	BtFrPrev->Enabled = false;
	BtBack->Enabled = false;
	BtStop->Enabled = false;
	BtPlay->Enabled = false;
	BtFrNext->Enabled = false;
	CBAutorepeat->Enabled = false;
	CBViewMode->Enabled = false;
	ComboBoxScale->Enabled = false;
	Edit_duration->Enabled = false;
	ComboBox_Event->Enabled = false;

	Bt_propdef->Tag = NativeInt(Bt_propdef->Enabled);

	if (gUsingOpenGL)
	{
		mCanvasColor = clGray;
	}
	else
	{
		mCanvasColor = clSilver;
	}

	mpDragObjList = new TList();
	mpSaveDragObjList = new TList();
	mpTempFilterList = new TStringList();
	DragObjListClear();
	ClearCopySelection();

	mLMBtn.moved = false;
	mLMBtn.pressed = false;

	FakeControl->Left = -100;
	FakeControl->Top = -100;

	StatusBar->Panels->Items[0]->Text = Localization::Text(_T("mAnimationSpeedMessage")) + IntToStr((int)Form_m->tickDuration) + ANIMATION_SPEED_MILLISECONDS;
	CREATE_ANIMATION_MESSAGE = Localization::Text(_T("mCreateAnimationMessage"));
	CREATE_EMPTY_FRAME_MESSAGE =  Localization::Text(_T("mCreateEmptyFrameMessage"));

	VLECollide->Tag = reinterpret_cast<NativeInt>(&mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE]);
	VLEView->Tag = reinterpret_cast<NativeInt>(&mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW]);

	VLENode1->Tag = reinterpret_cast<NativeInt>(&mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_NODE1]);
	mNRList.push_back(&mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_NODE1]);

	gspErrBmp = new BMPImage(new Graphics::TBitmap());
	mTimerEnabled = false;
	mTimerDuration = MINIMAL_TIMER_DURATION;

	mOldPaintBox1WndProc = PaintBoxGL->WindowProc;
	PaintBoxGL->WindowProc = CustomPaintBox1WndMethod;
	mRenderDelay = RENDER_DELAY_MS;

	mOldCBValue = _T("\0");
	mLastStreamFolder = _T("\0");
	mLastStreamFile = _T("\0");
	mCurrentStreamFilename = _T("\0");
	mpCurrentStreamBmp = NULL;

	Localization::Localize(this);
	Localization::Localize(ActionList1);
	Localization::Localize(PopupMenuObj);

	TabSetClick(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FormDestroy(TObject *)
{
	std::list<TCheckBox*>::iterator icb;
	for(icb = mDynCheckboxes.begin(); icb != mDynCheckboxes.end(); ++icb)
	{
	   TCheckBox* cb = *icb;
	   delete cb;
	}
	std::vector<TValueListEditor*>::iterator ivle;
	for(ivle = mVLENodes.begin(); ivle != mVLENodes.end(); ++ivle)
	{
		TValueListEditor* vle = *ivle;
		delete vle;
	}
	std::list<TCategoryPanel*>::iterator icp;
	for(icp = mDynPanels.begin(); icp != mDynPanels.end(); ++icp)
	{
		TCategoryPanel* cp = *icp;
		delete cp;
	}
	FilesList->Items->Clear();
	FoldersList->Items->Clear();
	ReleaseRenderGL();
	mBFList.clear();
	mNRList.clear();
	delete mpTempResObjManager;
	delete mpTempGameObj;
	ClearResList(mpResList);
	delete mpResList;
	delete mpDragObjList;
	delete mpSaveDragObjList;
	delete gspErrBmp;
	delete mpTempFilterList;
	if(mpCurrentStreamBmp)
	{
		delete mpCurrentStreamBmp;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ReleaseResources()
{
	if(mpTempResObjManager != NULL)
	{
		mpTempResObjManager->ClearGraphicResources();
    }
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ClearResList(TList *lst)//TTempBGobj list
{
	while (lst->Count)
	{
		TTempBGobj *o = static_cast<TTempBGobj*>(lst->Items[lst->Count - 1]);
		delete o;
		lst->Delete(lst->Count - 1);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::InitRenderGL()
{
	if (gUsingOpenGL)
	{
		TRect v;
		GetRenderRect(v);
		TRenderGL::InitWindow(PaintBoxGL->Handle, v, PaintBoxGL->ClientRect, true);
		TRenderGL::SetActiveCommonResObjManager(mpTempResObjManager);
		TRenderGL::SetBackdropImage(NULL, mCanvasColor);
	}
	Form_m->SetChildRenderGLForm(this);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ReleaseRenderGL()
{
	Form_m->SetChildRenderGLForm(NULL);
	TRenderGL::ReleaseWindows();
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::TabSetClick(TObject *)
{
	if (TabSet->TabIndex == 0)
	{
		ReleaseRenderGL();
		DragObjListClear();
		SaveFrameData();
		ToolBar->Visible = false;
		Splitter1->Visible = false;
		PanelLeft->Visible = false;
		PaintBoxGL->Enabled = false;
		PaintBoxGL->Visible = false;
		PanelAnimation->Visible = false;
		PanelMain->Align = alClient;
		PanelMain->Visible = true;
		SetDrawMode();
		InitRenderGL();
	}
	else
	{
		bool res = false;
		ReleaseRenderGL();
		TFEditGrObj *ed = Form_m->getSelectionBlock(mpTempResObjManager, false);
		if (ed != NULL)
		{
			res = true;
			ed->Free();
		}
		if(!res)
		{
			TabSet->TabIndex = 0;
			return;
		}
		mBeginFrameGridDrag = false;
		mFrameGridDrag = false;
		DragObjListClear();
		SaveFrameData();
		ToolBar->Visible = true;
		SetAniViewMode();
		SetDrawMode();
		if (TabSetAniMode->TabIndex == 0)
		{
			TabSetGraphicsMode->Visible = true;

			CategoryPanelGroup->Visible = false;
			CategoryPanelGroup->Align = alNone;
			CategoryPanelGroup->Height = 1;

			if (TabSetGraphicsMode->TabIndex == 0)
			{
				Panel_strm->Visible = false;
				Panel_strm->Align = alNone;
				Panel_strm->Height = 1;

				CategoryButtons_s->Align = alClient;
				CategoryButtons_s->Visible = true;
			}
			else
			{
				CategoryButtons_s->Visible = false;
				CategoryButtons_s->Align = alNone;
				CategoryButtons_s->Height = 1;

				Panel_strm->Align = alClient;
				Panel_strm->Visible = true;

				CreateStreamFolderAndFileList(mLastStreamFolder, mLastStreamFile);
			}
			VLEObj->Visible = true;
			VLEObj->Align = alBottom;
		}
		else
		{
			Panel_strm->Visible = false;
			Panel_strm->Align = alNone;
			Panel_strm->Height = 1;

			TabSetGraphicsMode->Visible = false;

			CategoryButtons_s->Visible = false;
			CategoryButtons_s->Align = alNone;
			CategoryButtons_s->Height = 1;

			CategoryPanelGroup->Align = alClient;
			CategoryPanelGroup->Visible = true;

			VLEObj->Visible = false;
			VLEObj->Align = alNone;

			RBCollideRectClick(NULL);
		}
		PanelLeft->Visible = true;
		Splitter1->Left = PanelAnimation->Left - Splitter1->Width;
		Splitter1->Visible = true;
		PanelAnimation->Visible = true;
		if(gUsingOpenGL)
		{
			PaintBoxGL->Enabled = true;
			FormResize(NULL);
			PaintBoxGL->Visible = true;
		}
		PanelMain->Visible = false;
		PanelMain->Align = alNone;
		InitRenderGL();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActShowRulersExecute(TObject *Sender)
{
	(void)Sender;
	ActShowRulers->Checked = !ActShowRulers->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActAniAndLogicExecute(TObject *Sender)
{
	(void)Sender;
	ActAniAndLogic->Checked = !ActAniAndLogic->Checked;
	TabSetClick(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::TabSetGraphicsModeClick(TObject *)
{
	TabSetClick(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CBViewModeChange(TObject *)
{
	SetAniViewMode();
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ToolButton15Click(TObject *Sender)
{
	(void)Sender;
	ColorDialog->Color = mCanvasColor;
	if(ColorDialog->Execute())
	{
		mCanvasColor = ColorDialog->Color;
		ReleaseRenderGL();
		InitRenderGL();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::SetAniViewMode()
{
	switch(CBViewMode->ItemIndex)
	{
		case 0:
			mDontCareSelection = false;
			mTraceAnimation = false;
		break;
		case 1:
			mDontCareSelection = false;
			mTraceAnimation = true;
		break;
		case 2:
			mDontCareSelection = true;
			mTraceAnimation = true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::SetDrawMode()
{
	if (TabSet->TabIndex == 0)
	{
		mDrawAnimations = false;
		mDrawLogics = false;
	}
	else
	{
		if (TabSetAniMode->TabIndex == 0)
		{
			mDrawAnimations = true;
			//mDrawAnimationsMaxOpacity = UOP_MAX_OPACITY;
			mDrawLogics = false;
			if(ActAniAndLogic->Checked)
			{
				mDrawLogics = true;
				//mDrawLogicsMaxOpacity = UOP_HALF_OPACITY;
			}
		}
		else
		{
			mDrawLogics = true;
			//mDrawLogicsMaxOpacity = UOP_MAX_OPACITY;
			mDrawAnimations = false;
			if(ActAniAndLogic->Checked)
			{
				mDrawAnimations = true;
				//mDrawAnimationsMaxOpacity = UOP_HALF_OPACITY;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::RBCollideRectClick(TObject *Sender)
{
	if(mCheckBoxesProcessing)
	{
        return;
    }
	mCheckBoxesProcessing = true;
	DragObjListClear();
	std::list<TCheckBox*>::iterator icb;
	if(Sender && static_cast<TCheckBox*>(Sender)->Checked == false)
	{
		Sender = NULL;
	}
	if(RBCollideRect->Checked && Sender != RBCollideRect)
	{
		RBCollideRect->Checked = false;
	}
	if(RBViewRect->Checked && Sender != RBViewRect)
	{
		RBViewRect->Checked = false;
	}
	if(RBNode1->Checked && Sender != RBNode1)
	{
		RBNode1->Checked = false;
	}
	for(icb = mDynCheckboxes.begin(); icb != mDynCheckboxes.end(); ++icb)
	{
		TCheckBox* cb = *icb;
		if(cb->Checked && Sender != cb)
		{
			cb->Checked = false;
		}
	}

	if(Sender == RBCollideRect)
	{
		VLECollide->Color = clWindow;
		DragObjListAdd(reinterpret_cast<TAnimationFrameData*>(VLECollide->Tag));
	}
	else
	{
		VLECollide->Color = clBtnFace;
	}
	if(Sender == RBViewRect)
	{
		DragObjListAdd(reinterpret_cast<TAnimationFrameData*>(VLEView->Tag));
		VLEView->Color = clWindow;
	}
	else
	{
		VLEView->Color = clBtnFace;
	}
	if(Sender == RBNode1)
	{
		DragObjListAdd(reinterpret_cast<TAnimationFrameData*>(VLENode1->Tag));
		VLENode1->Color = clWindow;
	}
	else
	{
		VLENode1->Color = clBtnFace;
	}
	for(icb = mDynCheckboxes.begin(); icb != mDynCheckboxes.end(); ++icb)
	{
		TCheckBox* cb = *icb;
		TValueListEditor* vle = reinterpret_cast<TValueListEditor*>(cb->Tag);
		if(Sender == cb)
		{
			DragObjListAdd(reinterpret_cast<TAnimationFrameData*>(vle->Tag));
			vle->Color = clWindow;
		}
		else
		{
			vle->Color = clBtnFace;
		}
		vle->Enabled = Sender == cb;
	}
	VLECollide->Enabled = Sender == RBCollideRect;
	VLEView->Enabled = Sender == RBViewRect;
	VLENode1->Enabled = Sender == RBNode1;
	TWinControl *c = this->ActiveControl;
	CP1->Color = Sender == RBCollideRect ? clHighlight : clBtnFace;
	CP2->Color = Sender == RBViewRect ? clHighlight : clBtnFace;
	CP3->Color = Sender == RBNode1 ? clHighlight : clBtnFace;
	std::list<TCategoryPanel*>::iterator icp;
	for(icp = mDynPanels.begin(); icp != mDynPanels.end(); ++icp)
	{
		TCategoryPanel* cp = *icp;
		TCheckBox* cb = reinterpret_cast<TCheckBox*>(cp->Tag);
		cp->Color = Sender == cb ? clHighlight : clBtnFace;
	}
	this->ActiveControl = c;
	mCheckBoxesProcessing = false;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::VLECollideDrawCell(TObject *Sender, __int32 ACol, __int32 ARow,
		  TRect &Rect, TGridDrawState State)
{
	if(!State.Contains(gdFixed))
	{
		TValueListEditor *ve = static_cast<TValueListEditor*>(Sender);
		ve->Canvas->Brush->Color = ve->Color;
		ve->Canvas->Pen->Color = ve->Color;
		ve->Canvas->Rectangle(Rect);
		ve->Canvas->Font->Color = clWindowText;
		ve->Canvas->TextOut(Rect.Left + 2, Rect.Top + 2, ve->Cells[ACol][ARow]);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::SetGameObj(GPerson *obj)
{
	assert(obj);

	mpGameObj = obj;
	Form_m->mainCommonRes->CopyTo(mpTempResObjManager);
	mpTempResObjManager->AssignObjMainPattern(mpTempGameObj);

	ComboBox_Event->Items->Clear();
	if(mpTempResObjManager->eventNameTypes->Count > 0)
	{
		ComboBox_Event->Items->Add(_T(""));
		ComboBox_Event->Items->AddStrings(mpTempResObjManager->eventNameTypes);
	}

	EditFilterChange(NULL);

	CBDefState->Clear();
	CBDefState->Items->Add(TEXT_STR_NONE);
	for(__int32 i = 0; i < mpTempResObjManager->states->Count; i++)
	{
		TState *state = static_cast<TState*>(mpTempResObjManager->states->Items[i]);
		CBDefState->AddItem(state->name, NULL);
	}
	__int32 idx = CBDefState->Items->IndexOf(mpGameObj->DefaultStateName);
	if(idx > 0)
	{
		CBDefState->ItemIndex = idx;
	}
	else
	{
		CBDefState->ItemIndex = 0;
    }

	PopupMenuGridPopup(NULL);
	CheckObjEditPosibility(false);

	mpTempResObjManager->CopyBitmapFrameDatabase(mpResList);
	Bt_propdef->Enabled = !mpTempResObjManager->IsMainGameObjPatternEmpty();
	Bt_propdef->Tag = NativeInt(Bt_propdef->Enabled);

	obj->CopyTo(mpTempGameObj);
	CheckBox_graphics->Checked = !mpTempGameObj->UseGraphics;
	CheckBox_res->Checked = mpTempGameObj->ResOptimization;
	Edit_name->Text = mpTempGameObj->GetName();
	if (mpTempGameObj->IsDecoration)
	{
		CheckBox_decoration->Checked = true;
	}
	CheckToSwitchDecorationAnimation();
	CreateListView();
	CreateAnimationFrames(0);
	CreateIcon();
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CreateIcon()
{
	BMPImage bmp;
	Image1->Stretch = false;
	Image1->Picture->Bitmap->PixelFormat = pf32bit;
	Image1->Transparent = false;
	if(Form_m->makeObjIcon(mpTempGameObj, &bmp, mpTempResObjManager))
	{
		Image1->AutoSize = false;
		Form_m->resizeBmpForListView(&bmp, Image1->Width, Image1->Height, 1.0f);
		BLENDFUNCTION bf;
		bf.BlendOp = AC_SRC_OVER;
		bf.BlendFlags = 0;
		bf.SourceConstantAlpha = 255;
		bf.AlphaFormat = AC_SRC_ALPHA;
		Image1->Picture->Bitmap->SetSize(Image1->Width, Image1->Height);
		Image1->Picture->Bitmap->Canvas->Pen->Color = clSilver;
		Image1->Picture->Bitmap->Canvas->Brush->Color = clSilver;
		Image1->Picture->Bitmap->Canvas->Rectangle(0, 0, Image1->Width, Image1->Height);
		::AlphaBlend(Image1->Picture->Bitmap->Canvas->Handle, 0, 0, Image1->Width, Image1->Height,
						bmp.Canvas->Handle, 0, 0, Image1->Width, Image1->Height, bf);
	}
	else
	{
		Image1->AutoSize = false;
		Image1->Picture->Bitmap->Assign(NULL);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ApplyChangesToGameObj()
{
	if (mpGameObj != NULL)
	{
		SaveFrameData();
		mpTempGameObj->SetName(Edit_name->Text);
		mpTempGameObj->ResOptimization = CheckBox_res->Checked;
		mpTempGameObj->IsDecoration = CheckBox_decoration->Checked;
		if(CBDefState->ItemIndex > 0)
		{
			mpTempGameObj->DefaultStateName = CBDefState->Text;
		}
		else
		{
			mpTempGameObj->DefaultStateName = _T("");
        }
		if (mpTempGameObj->IsDecoration)
		{
			mpTempGameObj->DecorType = CheckBox_graphics->Checked ? decorBACK : decorFORE;
		}
		else
		{
			mpTempGameObj->DecorType = decorNONE;
			mpTempGameObj->UseGraphics = !CheckBox_graphics->Checked;
		}
		mpTempGameObj->CopyTo(mpGameObj);

		mpTempResObjManager->CopyTo(Form_m->mainCommonRes);
		Form_m->rebuildBackObjListView(); // ����������� ������� ���� ������
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::Edit_nameChange(TObject *)
{
	Edit_nameChangeMain(Edit_name);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CheckBox_decorationClick(TObject *)
{
	UnicodeString string;
	mpTempGameObj->IsDecoration = CheckBox_decoration->Checked;
	CheckToSwitchDecorationAnimation();
	string = CheckBox_graphics->Caption;
	CheckBox_graphics->Caption = CheckBox_graphics->Hint;
	CheckBox_graphics->Hint = string;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CheckToSwitchDecorationAnimation()
{
	if (CheckBox_decoration->Checked)
	{
		Bt_propdef->Enabled = false;
		CheckBox_graphics->Checked = mpTempGameObj->DecorType == decorBACK;
	}
	else
	{
		if (Bt_propdef->Tag == NativeInt(1))
		{
			Bt_propdef->Enabled = true;
		}
		CheckBox_graphics->Checked = !mpTempGameObj->UseGraphics;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CheckBox_graphicsClick(TObject *)
{
	if (mpTempGameObj->IsDecoration)
	{
		mpTempGameObj->DecorType = CheckBox_graphics->Checked ? decorBACK : decorFORE;
	}
	else
	{
		mpTempGameObj->UseGraphics = !CheckBox_graphics->Checked;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::Bt_propdefClick(TObject *)
{
	GPersonProperties *prp = new GPersonProperties();
	prp->assignProperties(mpTempGameObj->GetBaseProperties());

	Application->CreateForm(__classid(TFDefProperties), &FDefProperties);
	FDefProperties->Caption = Bt_propdef->Caption;
	FDefProperties->SetProperties(prp);
	prp->assignProperties(mpTempGameObj->GetBaseProperties());
	if (FDefProperties->ShowModal() == mrOk)
	{
		FDefProperties->GetProperties(prp);
		mpTempGameObj->GetBaseProperties().assignProperties(*prp);
	}
	FDefProperties->Free();
	FDefProperties = NULL;

	delete prp;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::PopupMenuGridPopup(TObject *)
{
	bool active = CB_AniType->Items->Count != 0;
	bool edit = mAnimationFrameIdx >= 0 && mpAnimation;
	ActAddEmptyFrame->Enabled = active;
	ActDelFrame->Enabled = active && edit;
	ActAddCopyFrame->Enabled = active && edit;
	ActClearFrame->Enabled = active && edit;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::OnSelectFrame(__int32 idx)
{
	ClearCopySelection();
	DragObjListSave();
	DragObjListClear();
	DoSelectFrame(idx);
	DragObjListRestore();
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::DoSelectFrame(__int32 idx)
{
	int i;
	mAnimationFrameIdx = -1;

	mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE].Clear();
	mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE].type = LogicObjType::UOP_LOGIC_OBJ_COLLIDE;
	for(i = LogicObjType::UOP_LOGIC_OBJ_NODE1; i <= LogicObjType::UOP_LOGIC_OBJ_NODE16; i++)
	{
		mLogicDrawObjects[i].Clear();
		mLogicDrawObjects[i].type = static_cast<LogicObjType>(i);
	}

	if (mpAnimation && mpAnimation->GetFrameCount() > idx)
	{
		mAnimationFrameIdx = idx;
		mBFList.clear();
		const TAnimationFrame *af = mpAnimation->GetFrame(mAnimationFrameIdx);
		if(af != NULL)
		{
			af->CopyFrameDataTo(mBFList);
			mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE].posX = (double)af->GetCollideRect().Left;
			mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE].posY = (double)af->GetCollideRect().Top;
			mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE].rw = af->GetCollideRect().Width();
			mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE].rh = af->GetCollideRect().Height();
			TPoint pt;
			const BMPImage *bmp = mpTempResObjManager->GetGraphicResource(Form_m->mDirAssetIndex);
			assert(bmp);
			int idx = 0;
			for(i = LogicObjType::UOP_LOGIC_OBJ_NODE1; i <= LogicObjType::UOP_LOGIC_OBJ_NODE16; i++)
			{
				af->GetJoinNodeCoordinate(idx, pt);
				mLogicDrawObjects[i].posX = (double)pt.x;
				mLogicDrawObjects[i].posY = (double)pt.y;
				mLogicDrawObjects[i].rw = bmp->Width;
				mLogicDrawObjects[i].rh = bmp->Height;
				idx++;
			}
			mTimerDuration = af->GetDuration();
			if(mTimerDuration < MINIMAL_TIMER_DURATION)
			{
				mTimerDuration = MINIMAL_TIMER_DURATION;
			}
			Edit_duration->Text = IntToStr(af->GetDuration());
			ComboBox_Event->Text = af->GetEventId();
			CurrentFrameInfoEnabled(true);
		}
		else
		{
			mTimerDuration = MINIMAL_TIMER_DURATION;
			Edit_duration->Text = _T("0");
			ComboBox_Event->Text = _T("");
			CurrentFrameInfoEnabled(false);
		}
		LabelCurrent->Caption = IntToStr(idx);
	}
	else
	{
		mBFList.clear();
		LabelCurrent->Caption = Localization::Text(_T("mNoAnimationFramesString"));
		Edit_duration->Text = _T("0");
		ComboBox_Event->Text = _T("");
		CurrentFrameInfoEnabled(false);
	}
	SetLogicValuesToGrid(NULL);
	LabelFrafments->Caption = IntToStr(static_cast<int>(mBFList.size()));
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::SaveFrameData()
{
	if (mpAnimation && mAnimationFrameIdx >= 0)
	{
        int i;
		TAnimationFrame af;
		SaveObjValuesFromGrid();
		af.CreateFromList(mBFList);
		TRect r = TRect((__int32)mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW].posX,
					  (__int32)mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW].posY,
					  (__int32)mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW].posX + mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW].rw,
					  (__int32)mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW].posY + mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW].rh);
		mpAnimation->SetViewRect(r);
		r = TRect((__int32)mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE].posX,
					  (__int32)mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE].posY,
					  (__int32)mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE].posX + mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE].rw,
					  (__int32)mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE].posY + mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE].rh);
		af.SetCollideRect(r);
		TPoint pt;
		int idx = 0;
		for(i = LogicObjType::UOP_LOGIC_OBJ_NODE1; i <= LogicObjType::UOP_LOGIC_OBJ_NODE16; i++)
		{
			pt.x = (__int32)mLogicDrawObjects[i].posX;
			pt.y = (__int32)mLogicDrawObjects[i].posY;
			af.SetJoinNodeCoordinate(idx, pt);
			idx++;
		}
		try
		{
			af.SetDuration(StrToInt(Edit_duration->Text));
		}
		catch(...)
		{
		}
		if(ComboBox_Event->Items->IndexOf(ComboBox_Event->Text) >= 0)
		{
			af.SetEventId(ComboBox_Event->Text);
		}
		else
		{
			af.SetEventId(_T(""));
		}
		mpAnimation->SetFrame(af, mAnimationFrameIdx);
		mpAnimation->SetLooped(CBAutorepeat->Checked);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FrameGridSelectCell(TObject *, __int32 ACol, int, bool &)
{
	if(mDisableGridControls)
	{
        return;
    }
	SaveFrameData();
	OnSelectFrame(ACol);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::BtStopClick(TObject *)
{
	mTimerEnabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::BtPlayClick(TObject *)
{
	mTimerDuration = MINIMAL_TIMER_DURATION;
	if (mpAnimation && mAnimationFrameIdx >= 0)
	{
		const TAnimationFrame *af = mpAnimation->GetFrame(mAnimationFrameIdx);
		if(af != NULL)
		{
			mTimerDuration = af->GetDuration();
		}
	}
	if(mTimerDuration < MINIMAL_TIMER_DURATION)
	{
		mTimerDuration = MINIMAL_TIMER_DURATION;
	}
	mAnimatonDirection = 1;
	mTimerEnabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::BtBackClick(TObject *)
{
    mTimerDuration = MINIMAL_TIMER_DURATION;
	if (mpAnimation && mAnimationFrameIdx >= 0)
	{
		const TAnimationFrame *af = mpAnimation->GetFrame(mAnimationFrameIdx);
		if(af != NULL)
		{
			mTimerDuration = af->GetDuration();
		}
	}
	if(mTimerDuration < MINIMAL_TIMER_DURATION)
	{
		mTimerDuration = MINIMAL_TIMER_DURATION;
	}
	mAnimatonDirection = -1;
	mTimerEnabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::BtFrNextClick(TObject *)
{
	mAnimatonDirection = 1;
	mTimerEnabled = true;
	ProcessAnimation(mTimerDuration);
	mTimerEnabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::BtFrPrevClick(TObject *)
{
	mAnimatonDirection = -1;
	mTimerEnabled = true;
	ProcessAnimation(mTimerDuration);
	mTimerEnabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CB_AniTypeChange(TObject *)
{
	if(CB_AniType->Items->IndexOf(CB_AniType->Text) >= 0)
	{
		if(mpAnimation != NULL && mpTempGameObj->GetAnimation(CB_AniType->Text) == mpAnimation)
		{
			return;
		}
		CreateAnimationFrames(0);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ComboBox_EventChange(TObject *)
{
	if(ComboBox_Event->Items->IndexOf(ComboBox_Event->Text) >= 0)
	{
		if (mpAnimation && mAnimationFrameIdx >= 0)
		{
			TAnimationFrame *af = const_cast<TAnimationFrame*>(mpAnimation->GetFrame(mAnimationFrameIdx));
			if(af != NULL)
			{
				af->SetEventId(ComboBox_Event->Text);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CB_AniTypeEnter(TObject *Sender)
{
	TComboBox *cb = (TComboBox *)Sender;
	mOldCBValue = cb->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CB_AniTypeExit(TObject *Sender)
{
	TComboBox *cb = (TComboBox *)Sender;
	if(cb->Items->IndexOf(cb->Text) < 0)
	{
		if(cb->Text.Compare(mOldCBValue) != 0)
		{
			cb->Text = mOldCBValue;
			cb->OnChange(Sender);
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CB_AniTypeKeyPress(TObject *Sender, System::WideChar &Key)
{
	if(Key == VK_RETURN)
	{
		TComboBox *cb = (TComboBox *)Sender;
		cb->OnExit(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ProcessAnimation(unsigned __int32 ms)
{
	if(mTimerEnabled)
	{
		mTimerDuration -= ms;
		if(mTimerDuration <= 0)
		{
			mTimerDuration = MINIMAL_TIMER_DURATION;
			if(mpAnimation)
			{
				__int32 ct = mpAnimation->GetFrameCount();
				if(ct > 0)
				{
					if(mAnimatonDirection > 0)
					{
						if(FrameGrid->Col + 1 < mpAnimation->GetFrameCount())
						{
							FrameGrid->Col = FrameGrid->Col + 1;
						}
						else
						{
							FrameGrid->Col = 0;
						}
					}
					else
					{
						if(FrameGrid->Col - 1 >= 0)
						{
							FrameGrid->Col = FrameGrid->Col - 1;
						}
						else
						{
							FrameGrid->Col = mpAnimation->GetFrameCount() - 1;
						}
					}
					LabelCurrent->Caption = IntToStr(FrameGrid->Col);
					return;
				}
			}
			BtStop->Click();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::DoTick(unsigned __int32 ms)
{
	if (!mDrawAnimations && !mDrawLogics)
	{
		return;
	}
	ProcessAnimation(ms);
	if (gUsingOpenGL)
	{
		DrawFrame();
		TRenderGL::DrawScene();
	}
	else
	{
		mRenderDelay -= ms;
		if(mRenderDelay <= 0)
		{
			mRenderDelay = RENDER_DELAY_MS;
			DrawFrame();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CategoryButtons_sDrawIcon(TObject *Sender, const TButtonItem *Button, TCanvas *Canvas, TRect &Rect, TButtonDrawState State, __int32 &TextOffset)
{
	CBDrawIcon(Sender, Button, Canvas, Rect, State, TextOffset);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CategoryButtons_sEnter(TObject *)
{
	CategoryButtons_s->Color = clWindow;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CategoryButtons_sExit(TObject *)
{
	CategoryButtons_s->Color = clBtnFace;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActCreateIconExecute(TObject *)
{
	if(mpAnimation != NULL && mAnimationFrameIdx >= 0)
	{
		SaveFrameData();
		mpTempGameObj->SetIconData(mpAnimation->GetFrame(mAnimationFrameIdx));
		CreateIcon();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActAddEmptyFrameExecute(TObject *)
{
	SaveFrameData();
	if(mpAnimation == NULL)
	{
		if(CB_AniType->Items->IndexOf(CB_AniType->Text) < 0)
		{
            return;
        }
		TAnimation *a = new TAnimation(1);
		a->SetName(CB_AniType->Text);
		mpTempGameObj->AssignAnimation(a);
		CreateAnimationFrames(0);
	}
	else
	{
		__int32 idx = mAnimationFrameIdx;
		mAnimationFrameIdx = -1;
		mpAnimation->Resize(mpAnimation->GetFrameCount() + 1);
		CreateAnimationFrames(idx);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActAddCopyFrameExecute(TObject *)
{
	if(mpAnimation != NULL)
	{
        SaveFrameData();
		__int32 idx = mAnimationFrameIdx;
		mAnimationFrameIdx = -1;
		mpAnimation->InsertFrame(*mpAnimation->GetFrame(idx), idx /*+ 1*/);
		CreateAnimationFrames(idx);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActDelFrameExecute(TObject *)
{
	if(mpAnimation && mAnimationFrameIdx >= 0)
	{
		if(mpAnimation->GetFrameCount() <= 1)
		{
			mpTempGameObj->DeleteAnimation(mpAnimation->GetName());
			mpAnimation = NULL;
			PopupMenuGridPopup(NULL);
			return;
		}
		__int32 idx = mAnimationFrameIdx;
		mAnimationFrameIdx = -1;
		mpAnimation->DeleteFrame(idx);
		if(idx >= mpAnimation->GetFrameCount())
		{
			idx = mpAnimation->GetFrameCount() - 1;
        }
		CreateAnimationFrames(idx);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActClearFrameExecute(TObject *)
{
	if(mpAnimation && mAnimationFrameIdx >= 0)
	{
		__int32 idx = mAnimationFrameIdx;
		mAnimationFrameIdx = -1;
		mpAnimation->ClearFrame(idx);
		CreateAnimationFrames(idx);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActEditExecute(TObject *)
{
	if(TabSetGraphicsMode->TabIndex == 1) //stream object
	{
		return;
	}

	SaveFrameData();
	ReleaseRenderGL();
	TFEditGrObj *ed = Form_m->getSelectionBlock(mpTempResObjManager, false);
	if (ed != NULL)
	{
		UnicodeString catName = _T("");
		if (Form_m->mpGrObjWndParams != NULL)
		{
			ed->SetWndParams(Form_m->mpGrObjWndParams);
		}
		hash_t h = 0;
		TBaseItem *li = CategoryButtons_s->SelectedItem;
		if (li != NULL && li->ClassNameIs(_T("TButtonItem")))
		{
			TButtonItem *bi = static_cast<TButtonItem*>(li);
			h = static_cast<TTempBGobj*>(bi->Data)->o.getHash();
			catName = bi->Category->Caption;
		}
		ed->SetObjList(h, mpResList);
		if (ed->ShowModal() == mrOk)
		{
			bool das = mDrawAnimations;
			bool dls = mDrawLogics;
			mDrawAnimations = false;
			mDrawLogics = false;
			ClearCopySelection();
			DragObjListSave();
			DragObjListClear();
			ClearResList(mpResList);
			ed->GetObjListChanges(mpResList); //list of changes
			mpTempResObjManager->ApplyFrameDatabaseChangesFromList(mpResList);
			__int32 ct = mpResList->Count;
			for(__int32 i = 0; i < ct; i++)
			{
				TTempBGobj *to = static_cast<TTempBGobj*>(mpResList->Items[i]);
				if(to->o.w > 0 && to->o.h > 0)
				{
					TFrameObj ro = to->o;
					if(to->o.getHash() != to->oldHash)
					{
						mpTempGameObj->ReplaceBitmapFrame(to->oldHash, ro);
						__int32  pos = mpSaveDragObjList->IndexOf(reinterpret_cast<void*>(to->oldHash));
						if(pos >= 0)
						{
							mpSaveDragObjList->Delete(pos);
							mpSaveDragObjList->Add(reinterpret_cast<void*>(to->o.getHash()));
						}
					}
				}
				else
				{
					__int32  pos = mpSaveDragObjList->IndexOf(reinterpret_cast<void*>(to->oldHash));
					if(pos >= 0)
					{
						mpSaveDragObjList->Delete(pos);
					}
					mpTempGameObj->ReplaceBitmapFrame(to->oldHash, TFrameObj()); //if w or h == 0 -- delete
					if(h == to->oldHash)
					{
                    	h = 0;
					}
				}
			}
			ClearResList(mpResList);
			mpTempResObjManager->CopyBitmapFrameDatabase(mpResList); // create new res list
			ed->CreateListView(CategoryButtons_s, ImageList, mpResList);
			DoSelectFrame(mAnimationFrameIdx);
			DragObjListRestore();
			CreateIcon();
			if(!catName.IsEmpty() || h != 0)
			{
				__int32 ct = mpResList->Count;
				for (__int32 i = 0; i < ct; i++)
				{
					TTempBGobj *o = static_cast<TTempBGobj*>(mpResList->Items[i]);
					if(h != 0)
					{
						if (o->oldHash == h)
						{
							if(o->bi != NULL)
							{
								CategoryButtons_s->SelectedItem = o->bi;
								o->bi->Category->Collapsed = false;
								CategoryButtons_s->SelectedItem->ScrollIntoView();
							}
							break;
						}
					}
					else if(!catName.IsEmpty())
					{
						if(!o->oldGroup.IsEmpty())
						{
							if(o->oldGroup.Compare(catName) == 0)
							{
								if(o->bi != NULL)
								{
									o->bi->Category->ScrollIntoView();
                                }
								break;
							}
						}
					}
				}
			}
			mDrawAnimations = das;
			mDrawLogics = dls;
		}
		if (Form_m->mpGrObjWndParams == NULL)
		{
			Form_m->mpGrObjWndParams = new TEGrWndParams();
		}
		ed->GetWndParams(Form_m->mpGrObjWndParams); //���������� ��������� ����
		ed->Free();
	}
	InitRenderGL();
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CreateListView()
{
	ReleaseRenderGL();
	TFEditGrObj *ed = Form_m->getSelectionBlock(mpTempResObjManager, false);
	if (ed != NULL)
	{
		ed->CreateListView(CategoryButtons_s, ImageList, mpResList);
		ed->Free();
	}
	InitRenderGL();
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CreateAnimationFrames(__int32 idx)
{
	const UnicodeString aname = CB_AniType->Text;
	mDisableGridControls = true;
	SaveFrameData();
	mpAnimation = NULL;
	mAnimationFrameIdx = -1;
	mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW].Clear();
	mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW].type = LogicObjType::UOP_LOGIC_OBJ_VIEW;
	if (aname.IsEmpty() || CB_AniType->Items->IndexOf(aname) < 0)
	{
		return;
	}
	mpAnimation = mpTempGameObj->GetAnimation(aname);
	if(mpAnimation)
	{
		mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW].posX = (double)mpAnimation->GetViewRect().Left;
		mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW].posY = (double)mpAnimation->GetViewRect().Top;
		mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW].rw = mpAnimation->GetViewRect().Width();
		mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW].rh = mpAnimation->GetViewRect().Height();
		FrameGrid->Col = 0;
		FrameGrid->ColCount = mpAnimation->GetFrameCount();
		LabelCount->Caption = IntToStr(mpAnimation->GetFrameCount());
		CBAutorepeat->Checked = mpAnimation->GetLooped();
		if(FrameGrid->Col != idx)
		{
			FrameGrid->Col = idx;
		}
		OnSelectFrame(idx);
	}
	else
	{
		mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE].Clear();
		for(int i = LogicObjType::UOP_LOGIC_OBJ_NODE1; i <= LogicObjType::UOP_LOGIC_OBJ_NODE16; i++)
		{
			mLogicDrawObjects[i].Clear();
		}
		FrameGrid->ColCount = 0;
		LabelCount->Caption = _T("0");
		CBAutorepeat->Checked = true;
		Edit_duration->Text = _T("0");
		ComboBox_Event->Text = _T("");
		CurrentFrameInfoEnabled(false);
		SetLogicValuesToGrid(NULL);
	}
	FrameGrid->Repaint();
	mDisableGridControls = false;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FrameGridDrawCell(TObject *, System::LongInt ACol, System::LongInt ARow, TRect &Rect, TGridDrawState State)
{
	TColor color = TColor(clWebDarkGray);
	if (mpAnimation)
	{
		__int32 fc = mpAnimation->GetFrameCount();
		if (ACol < fc)
		{
			const TAnimationFrame *fr = mpAnimation->GetFrame(ACol);
			if(fr != NULL && !fr->IsEmpty())
			{
				if(State.Contains(gdSelected))
				{
					color = TColor(clWebSteelBlue);
				}
				else
				{
					color = TColor(clWebWhiteSmoke);
				}
			}
			else
			{
				if(State.Contains(gdSelected))
				{
					color = TColor(clWebDarkSlategray);
				}
            }
		}
	}
	FrameGrid->Canvas->Brush->Color = color;
	FrameGrid->Canvas->Rectangle(Rect);
	if(mFrameGridDrag)
	{
		if(ACol == mFrameGridPos.x && ARow == mFrameGridPos.y)
		{
			FrameGrid->Canvas->Brush->Color = clWebSlateBlue;
			FrameGrid->Canvas->Rectangle(Rect);
		}
		if(State.Contains(gdSelected))
		{
			TColor clr = FrameGrid->Canvas->Pen->Color;
			FrameGrid->Canvas->Pen->Color = clRed;
			FrameGrid->Canvas->MoveTo(Rect.Left + 1, Rect.Top + Rect.Height() / 2);
			FrameGrid->Canvas->LineTo(Rect.Left + 1 + Rect.Width() / 2, Rect.Top + Rect.Height() / 3);
			FrameGrid->Canvas->LineTo(Rect.Left + 1 + Rect.Width() / 2, Rect.Top + Rect.Height() - Rect.Height() / 3);
			FrameGrid->Canvas->LineTo(Rect.Left + 1, Rect.Top + Rect.Height() / 2);
			FrameGrid->Canvas->Pen->Color = clr;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FrameGridMouseDown(TObject*, TMouseButton Button, TShiftState Shift, int, int)
{
	if(Shift.Contains(ssShift) && Button == mbLeft && mFrameGridDrag == false)
	{
		mBeginFrameGridDrag = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FrameGridMouseUp(TObject*, TMouseButton, TShiftState, int, int)
{
	if(mpAnimation && mFrameGridDrag)
	{
		mpAnimation->MoveFrame(mFrameGridPos.x, FrameGrid->Col);
		OnSelectFrame(FrameGrid->Col);
	}
	mBeginFrameGridDrag = false;
	mFrameGridDrag = false;
	FrameGrid->Repaint();
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FrameGridKeyUp(TObject*, WORD&, TShiftState)
{
	mBeginFrameGridDrag = false;
	mFrameGridDrag = false;
	FrameGrid->Repaint();
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FrameGridMouseMove(TObject *, TShiftState Shift, int, int)
{
	if(Shift.Contains(ssShift) && mBeginFrameGridDrag && Shift.Contains(ssLeft))
	{
		mBeginFrameGridDrag = false;
		mFrameGridDrag = true;
		mFrameGridPos.x = FrameGrid->Col;
		mFrameGridPos.y = FrameGrid->Row;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::Splitter1CanResize(TObject *, __int32 &NewSize, bool &)
{
	if(gUsingOpenGL && NewSize != Splitter1->Left)
	{
		FormResize(NULL);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::GetRenderRect(TRect& r)
{
	r.Left = 0;
	if (gUsingOpenGL)
	{
		r.Top = 0;
	}
	else
	{
		r.Top = TabSet->Height + ToolBar->Height;
	}
	r.Right = Splitter1->Left;
	r.Bottom = PanelAnimation->Top;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::DrawFrame()
{
	if (!mDrawAnimations && !mDrawLogics)
	{
		return;
	}

	TRect r;
	TCanvas *c = NULL;
	GetRenderRect(r);

	if (!gUsingOpenGL)
	{
		c = this->Canvas;
		c->Brush->Color = mCanvasColor;
		c->FillRect(r);
	}
	else
	{
		r.Top = 0;
		r.Left = 0;
	}

	if(mpAnimation != NULL)
	{
		if(gUsingOpenGL && ErrorGLText->Visible == true)
		{
			ErrorGLText->Visible = false;
		}
		if(mDrawAnimations)
		{
			DrawObjects(c, r);
		}
		if(mDrawLogics)
		{
			DrawLogics(c, r);
		}
		DrawSelection(c, r);
		if(ActShowRulers->Checked)
		{
			DrawRulers(c, r);
		}
	}
	else
	{
		UnicodeString str;
		if(gUsingOpenGL && ErrorGLText->Visible == false)
		{
			ErrorGLText->Visible = true;
		}
		if(CB_AniType->Items->Count == 0)
		{
			str = CREATE_ANIMATION_MESSAGE;
		}
		else
		{
			str = CREATE_EMPTY_FRAME_MESSAGE;
		}
		__int32 x0 = r.Left * 2 + r.Width() / 2;
		__int32 y0 = r.Top * 2 + r.Height() / 2;
		if (gUsingOpenGL)
		{
			TRenderGL::SetColor(mCanvasColor);
			TRenderGL::FillRect(r.Left, r.Top, r.Width(), r.Height());
			if(ErrorGLText->Caption.Compare(str) != 0)
			{
				ErrorGLText->Caption = str;
            }
			x0 -= ErrorGLText->Width / 2;
			ErrorGLText->Top = y0;
			ErrorGLText->Left = x0;
		}
		else
		{
			x0 -= c->TextWidth(str) / 2;
			__int32 cl = ColorToRGB(c->Brush->Color) ^ 0x00FFFFFF;
			c->Font->Color = TColor(cl);
			c->TextOut(x0, y0, str);
		}
    }
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::DrawRulers(TCanvas *c, const TRect &r)
{
	if (!gUsingOpenGL)
	{
		c->Brush->Style = bsSolid;
		c->Pen->Mode = pmNop;
		c->Pen->Style = psSolid;
	}
	TPoint p[5];
	__int32 sz = Ceil((double)(MAX_BOUND * 2) * mScale);
	p[0].x = r.Left *2 + ((r.Width() - sz) / 2);
	p[0].y = r.Top * 2 + ((r.Height() - sz) / 2);
	p[1].x = p[0].x + sz;
	p[1].y = p[0].y;
	p[2].x = p[0].x + sz;
	p[2].y = p[0].y + sz;
	p[3].x = p[0].x;
	p[3].y = p[0].y + sz;
	p[4].x = p[0].x;
	p[4].y = p[0].y;
	if (gUsingOpenGL)
	{
		unsigned char ag = 100;
		unsigned char aw = 75;
		TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
		TRenderGL::SetColor(128, 128, 128, ag);
		TRenderGL::RenderLine(p[0].x, p[0].y, p[1].x, p[1].y);
		TRenderGL::SetLineStyle(TRenderGL::RGLLSDash);
		TRenderGL::SetColor(255, 255, 255, aw);
		TRenderGL::RenderLine(p[0].x, p[0].y, p[1].x, p[1].y);
		TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
		TRenderGL::SetColor(128, 128, 128, ag);
		TRenderGL::RenderLine(p[1].x, p[1].y, p[2].x, p[2].y);
		TRenderGL::SetLineStyle(TRenderGL::RGLLSDash);
		TRenderGL::SetColor(255, 255, 255, aw);
		TRenderGL::RenderLine(p[1].x, p[1].y, p[2].x, p[2].y);
		TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
		TRenderGL::SetColor(128, 128, 128, ag);
		TRenderGL::RenderLine(p[2].x, p[2].y, p[3].x, p[3].y);
		TRenderGL::SetLineStyle(TRenderGL::RGLLSDash);
		TRenderGL::SetColor(255, 255, 255, aw);
		TRenderGL::RenderLine(p[2].x, p[2].y, p[3].x, p[3].y);
		TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
		TRenderGL::SetColor(128, 128, 128, ag);
		TRenderGL::RenderLine(p[3].x, p[3].y, p[0].x, p[0].y);
		TRenderGL::SetLineStyle(TRenderGL::RGLLSDash);
		TRenderGL::SetColor(255, 255, 255, aw);
		TRenderGL::RenderLine(p[3].x, p[3].y, p[0].x, p[0].y);
		TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
		TRenderGL::SetColor(128, 128, 128, ag);
		TRenderGL::RenderLine(p[0].x + sz / 2, r.Top, p[0].x + sz / 2, r.Top + r.Height());
		TRenderGL::SetLineStyle(TRenderGL::RGLLSDash);
		TRenderGL::SetColor(255, 255, 255, aw);
		TRenderGL::RenderLine(p[0].x + sz / 2, r.Top, p[0].x + sz / 2, r.Top + r.Height());
		TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
		TRenderGL::SetColor(128, 128, 128, ag);
		TRenderGL::RenderLine(r.Left, p[0].y + sz / 2, r.Left + r.Width(), p[0].y + sz / 2);
		TRenderGL::SetLineStyle(TRenderGL::RGLLSDash);
		TRenderGL::SetColor(255, 255, 255, aw);
		TRenderGL::RenderLine(r.Left, p[0].y + sz / 2, r.Left + r.Width(), p[0].y + sz / 2);
	}
	else
	{
		c->Brush->Color = clSilver;
		c->Pen->Color = clGray;
		c->Pen->Style = psDot;
		c->Pen->Mode = pmNotXor;
		c->Polyline(p, 4);
		c->MoveTo(p[0].x + sz / 2, r.Top);
		c->LineTo(p[0].x + sz / 2, r.Top + r.Height());
		c->MoveTo(r.Left, p[0].y + sz / 2);
		c->LineTo(r.Left + r.Width(), p[0].y + sz / 2);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::DrawObjects(TCanvas *c, const TRect &r)
{
	if (mpAnimation)
	{
		__int32 ct;
		bool selres;
		std::list<TAnimationFrameData>::const_iterator fdata;
		if(mTraceAnimation)
		{
			ct = mpAnimation->GetFrameCount();
			for(__int32 i = 0; i < ct; i++)
			{
				std::list<TAnimationFrameData> lst;
				const TAnimationFrame *af = mpAnimation->GetFrame(i);
				if(i == mAnimationFrameIdx ||
				   i > mAnimationFrameIdx + 1 ||
				   i < mAnimationFrameIdx - 1)
				{
					continue;
				}
				if(af)
				{
					af->CopyFrameDataTo(lst);
				}
				for(fdata = lst.begin(); fdata != lst.end(); ++fdata)
				{
					selres = true;
					if(!mDontCareSelection)
					{
						selres = false;
						for(__int32 k = 0; k < mpDragObjList->Count; k++)
						{
							if(static_cast<TAnimationFrameData*>(mpDragObjList->Items[k])->frameId == fdata->frameId)
							{
								selres = true;
								break;
							}
						}
					}
					if(selres)
					{
                        __int32 di = 2;
						unsigned char drawAnimationsMaxOpacity =
								(unsigned char)((mAnimationFrameIdx < 0) ? 255 : fdata->a);
						if(ActAniAndLogic->Checked && TabSetAniMode->TabIndex != 0)
						{
							drawAnimationsMaxOpacity = drawAnimationsMaxOpacity / 2;
						}
						DrawFrameObject(c, r, (unsigned char)abs((drawAnimationsMaxOpacity / 2) / di), &(*fdata));
					}
				}
			}
		}
		for(fdata = mBFList.begin(); fdata != mBFList.end(); ++fdata)
		{
			unsigned char drawAnimationsMaxOpacity =
					(unsigned char)((mAnimationFrameIdx < 0) ? 255 : fdata->a);
			if(ActAniAndLogic->Checked && TabSetAniMode->TabIndex != 0)
			{
				drawAnimationsMaxOpacity = drawAnimationsMaxOpacity / 2;
			}
			DrawFrameObject(c, r, drawAnimationsMaxOpacity, &(*fdata));
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::DrawFrameObject(TCanvas *c, const TRect &r, unsigned char transparency, const TAnimationFrameData *fdata)
{
	assert(fdata);

	__int32 sx, sy;
	__int32 sh, dh;
	__int32 sw, dw;
	__int32 xx, yy;
	__int32 x0 = (r.Left * 2) + (r.Width() / 2);
	__int32 y0 = (r.Top * 2) + (r.Height() / 2);
	const BMPImage *bmp;
	if(fdata->isStreamFrame())
	{
		sx = sy = 0;
		sw = fdata->rw;
		sh = fdata->rh;
		if(mCurrentStreamFilename.Compare(fdata->strm_filename) != 0)
		{
			mCurrentStreamFilename = fdata->strm_filename;
			if(mpCurrentStreamBmp != NULL)
			{
				delete mpCurrentStreamBmp;
				mpCurrentStreamBmp = NULL;
			}
			UnicodeString path = Form_m->PathPngRes + fdata->strm_filename;
			if(FileExists(path))
			{
				Graphics::TBitmap *tbmp = new Graphics::TBitmap();
				if(read_png(path.c_str(), tbmp))
				{
					mpCurrentStreamBmp = new BMPImage(tbmp);
					if(mpCurrentStreamBmp->Width != sw ||
						mpCurrentStreamBmp->Height != sh )
					{
						delete mpCurrentStreamBmp;
						mpCurrentStreamBmp = NULL;
					}
				}
				else
				{
					delete tbmp;
				}
			}
		}
		if (gUsingOpenGL)
		{
			TRenderGL::FlushRenderNonResManagerImage();
        }
		bmp = mpCurrentStreamBmp;

		dw = Ceil((double)sw * mScale);
		dh = Ceil((double)sh * mScale);
	}
	else
	{
		const TFrameObj *fobj = mpTempResObjManager->GetBitmapFrame(fdata->frameId);
		assert(fobj);
		if(fobj != NULL)
		{
			sx = fobj->x;
			sy = fobj->y;
			sw = fobj->w;
			sh = fobj->h;
			bmp = mpTempResObjManager->GetGraphicResource(fobj->getResIdx());
		}

		dw = Ceil((double)fdata->rw * mScale);
		dh = Ceil((double)fdata->rh * mScale);
	}
	xx = x0 + Ceil(fdata->posX * mScale);
	yy = y0 + Ceil(fdata->posY * mScale);
	BLENDFUNCTION bf;
	bf.BlendOp = AC_SRC_OVER;
	bf.BlendFlags = 0;
	bf.SourceConstantAlpha = transparency;
	bf.AlphaFormat = AC_SRC_ALPHA;
	if (gUsingOpenGL)
	{
		float tr = static_cast<float>(transparency) / 255.0f;
		if(bmp == NULL)
		{
			 TRenderGL::SetColor(237, 28, 36, transparency); //red
			 TRenderGL::FillRect(xx, yy, dw, dh);
		}
		else
		{
			TRenderGL::RenderNonResManagerImage(bmp,
									DIT_Obj,
									sx, sy, sw, sh,
									xx, yy, dw, dh,
									tr);
		}
	}
	else
	{
		if(bmp == NULL)
		{
			Form_m->makeEmptyPreview(gspErrBmp, _T("x"), sw, sh);
			::AlphaBlend(c->Handle, xx, yy, dw, dh, gspErrBmp->Canvas->Handle, 0, 0, sw, sh, bf);
		}
		else
		{
			::AlphaBlend(c->Handle, xx, yy, dw, dh, bmp->Canvas->Handle, sx, sy, sw, sh, bf);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::DrawLogics(TCanvas *c, const TRect &r)
{
	if (mpAnimation)
	{
		TPoint pt[5];
		TAnimationFrameData *obj;
		if (gUsingOpenGL)
		{
			TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
		}
		else
		{
			c->Brush->Style = bsSolid;
			c->Pen->Mode = pmCopy;
			c->Pen->Style = psSolid;
		}
		__int32 x0 = (r.Left * 2) + (r.Width() / 2);
		__int32 y0 = (r.Top * 2) + (r.Height() / 2);
		unsigned char drawLogicsMaxOpacity = 255;
		if (TabSetAniMode->TabIndex == 0)
		{
			if(ActAniAndLogic->Checked)
			{
				drawLogicsMaxOpacity = 128;
			}
		}
		obj = &mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE];
		if(obj->rw > 0 && obj->rh > 0)
		{
			pt[4].x = pt[3].x = pt[0].x = x0 + Ceil(obj->posX * mScale);
			pt[4].y = pt[1].y = pt[0].y = y0 + Ceil(obj->posY * mScale);
			pt[2].x = pt[1].x = pt[0].x + Ceil((double)obj->rw * mScale);
			pt[3].y = pt[2].y = pt[0].y + Ceil((double)obj->rh * mScale);
			TColor cl = mCanvasColor == clRed ? clBlack : clRed;
			if (gUsingOpenGL)
			{
				TRenderGL::SetColor(cl);
				TRenderGL::RenderLine(pt[0].x, pt[0].y, pt[1].x, pt[1].y);
				TRenderGL::RenderLine(pt[1].x, pt[1].y, pt[2].x, pt[2].y);
				TRenderGL::RenderLine(pt[2].x, pt[2].y, pt[3].x, pt[3].y);
				TRenderGL::RenderLine(pt[3].x, pt[3].y, pt[0].x, pt[0].y);
			}
			else
			{
				c->Pen->Color = cl;
				c->Polyline(pt, 4);
			}
		}
		obj = &mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW];
		if(obj->rw > 0 && obj->rh > 0)
		{
			pt[4].x = pt[3].x = pt[0].x = x0 + Ceil(obj->posX * mScale);
			pt[4].y = pt[1].y = pt[0].y = y0 + Ceil(obj->posY * mScale);
			pt[2].x = pt[1].x = pt[0].x + Ceil((double)obj->rw * mScale);
			pt[3].y = pt[2].y = pt[0].y + Ceil((double)obj->rh * mScale);
			TColor cl = mCanvasColor == clBlue ? clBlack : clBlue;
			if (gUsingOpenGL)
			{
				TRenderGL::SetColor(cl);
				TRenderGL::RenderLine(pt[0].x, pt[0].y, pt[1].x, pt[1].y);
				TRenderGL::RenderLine(pt[1].x, pt[1].y, pt[2].x, pt[2].y);
				TRenderGL::RenderLine(pt[2].x, pt[2].y, pt[3].x, pt[3].y);
				TRenderGL::RenderLine(pt[3].x, pt[3].y, pt[0].x, pt[0].y);
			}
			else
			{
				c->Pen->Color = cl;
				c->Polyline(pt, 4);
			}
		}

		BLENDFUNCTION bf;
		bf.BlendOp = AC_SRC_OVER;
		bf.BlendFlags = 0;
		bf.SourceConstantAlpha = drawLogicsMaxOpacity;
		bf.AlphaFormat = AC_SRC_ALPHA;
		float tr = static_cast<float>(drawLogicsMaxOpacity) / 255.0f;
		const BMPImage *bmp = mpTempResObjManager->GetGraphicResource(Form_m->mDirAssetIndex);
		assert(bmp);
		std::list<TAnimationFrameData*>::const_iterator ifdata;
		for(ifdata = mNRList.begin(); ifdata != mNRList.end(); ++ifdata)
		{
			const TAnimationFrameData* fdata = *ifdata;
			pt[1].x = bmp->Width;
			pt[1].y = bmp->Height;
			pt[0].x = x0 + Ceil(fdata->posX * mScale) - pt[1].x / 2;
			pt[0].y = y0 + Ceil(fdata->posY * mScale) - pt[1].y;
			if (gUsingOpenGL)
			{
				TRenderGL::RenderImage(Form_m->mDirAssetIndex,
								DIT_Obj,
								0, 0, pt[1].x, pt[1].y,
								pt[0].x, pt[0].y, pt[1].x, pt[1].y,
								tr);
			}
			else
			{
				::AlphaBlend(c->Handle, pt[0].x, pt[0].y, pt[1].x, pt[1].y, bmp->Canvas->Handle, 0, 0, pt[1].x, pt[1].y, bf);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::DrawSelection(TCanvas *c, const TRect &r)
{
	TRect sr;
	TAnimationFrameData *obj;
	if (!gUsingOpenGL)
	{
		c->Brush->Style = bsSolid;
		c->Pen->Mode = pmNop;
		c->Pen->Style = psSolid;
	}
	__int32 x0 = (r.Left * 2) + r.Width() / 2;
	__int32 y0 = (r.Top * 2) + r.Height() / 2;
	__int32 ct = DragObjListCount();
	for (__int32 i = 0; i < ct; ++i)
	{
		obj = GetDragObjFromList(i);
		if(obj->type >= LogicObjType::UOP_LOGIC_OBJ_NODE1 &&
			obj->type <= LogicObjType::UOP_LOGIC_OBJ_NODE16)
		{
			sr.Left = x0 + Ceil(obj->posX * mScale) - 2 - obj->rw / 2;
			sr.Top = y0 + Ceil(obj->posY * mScale) - 2 - obj->rh;
			sr.Bottom = sr.Top + 4 + obj->rh;
			sr.Right = sr.Left + 4 + obj->rw;
		}
		else
		{
			sr.Left = x0 + Ceil(obj->posX * mScale) - 2;
			sr.Top = y0 + Ceil(obj->posY * mScale) - 2;
			sr.Bottom = sr.Top + 4 + Ceil((double)obj->rh * mScale);
			sr.Right = sr.Left + 4 + Ceil((double)obj->rw * mScale);
		}
		if (gUsingOpenGL)
		{
			TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
			TRenderGL::SetColor(clBlack);
			TRenderGL::RenderRect(sr.Left, sr.Top, sr.Width(), sr.Height());
			TRenderGL::SetLineStyle(TRenderGL::RGLLSDot);
			TRenderGL::SetColor(clWhite);
			TRenderGL::RenderRect(sr.Left, sr.Top, sr.Width(), sr.Height());
		}
		else
		{
			c->Brush->Color = clWhite;
			c->DrawFocusRect(sr);
    	}
	}

	if (mCopyRect.mSet)
	{
		if (gUsingOpenGL)
		{
			TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
			TRenderGL::SetColor(clBlack);
			TRenderGL::RenderRect(mCopyRect.mRect.Left, mCopyRect.mRect.Top, mCopyRect.mRect.Width(), mCopyRect.mRect.Height());
			TRenderGL::SetLineStyle(TRenderGL::RGLLSDot);
			TRenderGL::SetColor(clWhite);
			TRenderGL::RenderRect(mCopyRect.mRect.Left, mCopyRect.mRect.Top, mCopyRect.mRect.Width(), mCopyRect.mRect.Height());
		}
		else
		{
            c->Brush->Color = clWhite;
			c->DrawFocusRect(mCopyRect.mRect);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FormMouseDown(TObject *, TMouseButton Button, TShiftState Shift, __int32 X, __int32 Y)
{
	if(!mDrawAnimations && !mDrawLogics)
	{
		return;
    }
	if (Button == mbLeft)
	{
		mLMBtn.pos.x = 0;
		mLMBtn.pos.y = 0;
		mLMBtn.prev.x = X;
		mLMBtn.prev.y = Y;
		mLMBtn.moved = false;
		mLMBtn.pressed = true;
	}
	if (mpAnimation)
	{
		if(mCopyRect.mSet == false && Shift.Contains(ssAlt))
		{
			CheckCursorMode(X, Y, mScale);
		}

		if(mCopyRect.dragType == DragType::sNone)
		{
			TRect r;
			bool unselect;
			POINT p;
			GetCursorPos(&p);
			r.Left = X;
			r.Right = X;
			r.Top = Y;
			r.Bottom = Y;
			TAnimationFrameData *pt = FindObjectPointers(NULL, r);
			unselect = ProcessSelectionWithMouse(pt, Button, Shift, X, Y);
			if (!unselect && Button == mbRight)
			{
				PopupMenuObj->Popup(p.x, p.y); //����� �������� �����-����
			}
		}
	}
	FakeControl->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FormMouseUp(TObject *, TMouseButton, TShiftState Shift, int, int)
{
	if (mCopyRect.mSet)
	{
		DragObjListAddMulti(mCopyRect.mRect, Shift.Contains(ssCtrl));
		ClearCopySelection();
	}
	mLMBtn.moved = false;
	mLMBtn.pressed = false;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FormMouseMove(TObject *, TShiftState shstate, __int32 X, __int32 Y)
{
	if(!mDrawAnimations && !mDrawLogics)
	{
        return;
	}

	if (mCopyRect.mSet)
	{
		if (X > mCopyRect.mX0)
		{
			mCopyRect.mRect.Left = mCopyRect.mX0;
			mCopyRect.mRect.Right = X;
		}
		else
		{
			mCopyRect.mRect.Right = mCopyRect.mX0;
			mCopyRect.mRect.Left = X;
		}
		if (Y > mCopyRect.mY0)
		{
			mCopyRect.mRect.Top = mCopyRect.mY0;
			mCopyRect.mRect.Bottom = Y;
		}
		else
		{
			mCopyRect.mRect.Bottom = mCopyRect.mY0;
			mCopyRect.mRect.Top = Y;
		}
	}
	if(mLMBtn.pressed)
	{
		if(mCopyRect.dragType != DragType::sNone && shstate.Contains(ssAlt))
		{
			double xm = (double)(X - mLMBtn.prev.x);
			double ym = (double)(Y - mLMBtn.prev.y);
			ResizeSelectedObject((__int32)(xm / mScale), (__int32)(ym / mScale), shstate.Contains(ssShift));
			mLMBtn.moved = false;
		}
		else
		{
			double xm = (double)(X - mLMBtn.prev.x);
			double ym = (double)(Y - mLMBtn.prev.y);
			MoveSelectedObjects(xm / mScale, ym / mScale);
			mLMBtn.moved = true;
			mCopyRect.mProrotional = false;
		}
		mLMBtn.prev.x = X;
		mLMBtn.prev.y = Y;
	}
	else if(shstate.Contains(ssAlt))
	{
		CheckCursorMode(X, Y, mScale);
		mCopyRect.mProrotional = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CheckCursorMode(__int32 X, __int32 Y, double scale)
{
	if(mpDragObjList->Count == 1)
	{
		TPoint p;
		TRect r;
		GetRenderRect(r);
		TAnimationFrameData *obj = GetDragObjFromList(0);
		p.x = (r.Left * 2) + r.Width() / 2;
		p.y = (r.Top * 2) + r.Height() / 2;
		r.Left = (__int32)(p.x + obj->posX * scale) - 2;
		r.Top = (__int32)(p.y + obj->posY * scale) - 2;
		r.Bottom = (__int32)(r.Top + obj->rh * scale) + 4;
		r.Right = (__int32)(r.Left + obj->rw * scale) + 4;
		p.x = X;
		p.y = Y;
		if(p.y >= r.Top - 1 && p.y <= r.Bottom + 1)
		{
			if(p.x >= r.Right - 1 && p.x <= r.Right + 1)
			{
				mCopyRect.dragType = DragType::sRight;
				PaintBoxGL->Cursor = crSizeWE;
				return;
			}
		}
		if(p.x >= r.Left - 1 && p.x <= r.Right + 1)
		{
			if(p.y >= r.Bottom - 1 && p.y <= r.Bottom + 1)
			{
				mCopyRect.dragType = DragType::sBottom;
				PaintBoxGL->Cursor = crSizeNS;
				return;
			}
		}
	}
	if(mCopyRect.dragType != DragType::sNone)
	{
		PaintBoxGL->Cursor = crDefault;
	}
	mCopyRect.dragType = DragType::sNone;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FormKeyDown(TObject *, WORD &Key, TShiftState Shift)
{
	if((mDrawAnimations || mDrawLogics) )
	{
		if(Shift.Contains(ssCtrl))
		{
			__int32 dx = 0;
			__int32 dy = 0;
			bool move = false;

			if(Key == L'C' || Key == L'c')
			{
				ActObjCopy->Execute();
				return;
			}
			else
			if(Key == L'V' || Key == L'v')
			{
				ActObjPaste->Execute();
				return;
			}

			if (Key == VK_UP)
			{
				dy = -1;
				move = true;
			}
			if (Key == VK_DOWN)
			{
				dy = 1;
				move = true;
			}
			if (Key == VK_LEFT)
			{
				dx = -1;
				move = true;
			}
			if (Key == VK_RIGHT)
			{
				dx = 1;
				move = true;
			}
			if(move)
			{
				FakeControl->SetFocus();
				MoveSelectedObjects(dx, dy);
				Key = 0;
			}
			return;
		}
		switch(Key)
		{
			case VK_PRIOR:
				BtFrPrev->Click();
			break;
			case VK_NEXT:
				BtFrNext->Click();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ClearCopySelection()
{
	ZeroMemory(&mCopyRect, sizeof(mCopyRect));
}
//---------------------------------------------------------------------------

bool __fastcall TFObjParent::ProcessSelectionWithMouse(TAnimationFrameData *pt, TMouseButton Button, TShiftState Shift, __int32 X, __int32 Y)
{
	bool unselect = false;
	if (pt == NULL)
	{
		if (!Shift.Contains(ssCtrl))
		{
			DragObjListClear();
		}
		if (Button == mbLeft)
		{
			mCopyRect.mX0 = X;
			mCopyRect.mRect.Left = X;
			mCopyRect.mRect.Right = X;
			mCopyRect.mY0 = Y;
			mCopyRect.mRect.Top = Y;
			mCopyRect.mRect.Bottom = Y;
			mCopyRect.mSet = true;
		}
	}
	else
	{
		if (Shift.Contains(ssCtrl))
		{
			if (mpDragObjList->Count > 1)
			{
				if (DragObjListContains(pt))
				{
					RemoveDragObjFromList(pt);
					unselect = true;
				}
				else
				{
					DragObjListAdd(pt);
				}
			}
			else
			{
				DragObjListAdd(pt);
			}
		}
		else
		{
			if (!DragObjListContains(pt))
			{
				DragObjListClear();
				DragObjListAdd(pt);
			}
		}
	}
	return unselect;
}
//---------------------------------------------------------------------------

TAnimationFrameData *__fastcall TFObjParent::FindObjectPointers(TList *List, TRect &SelectionRect)
{
	__int32 i, k;
	TRect r;
	TAnimationFrameData *p;
	TAnimationFrameData *first = NULL;
	GetRenderRect(r);
	__int32 x0 = (r.Left * 2) + r.Width() / 2;
	__int32 y0 = (r.Top * 2) + r.Height() / 2;
	if(TabSetAniMode->TabIndex == 0) // animation mode
	{
		std::list<TAnimationFrameData>::iterator p;
		for(p = mBFList.begin(); p != mBFList.end(); ++p)
		{
			if (y0 + Ceil(p->posY * mScale) <= SelectionRect.Bottom &&
				y0 + Ceil((p->posY + (double)p->rh) * mScale) >= SelectionRect.Top &&
				x0 + Ceil(p->posX * mScale) <= SelectionRect.Right &&
				x0 + Ceil(((double)p->rw + p->posX) * mScale) >= SelectionRect.Left)
			{
				if(List == NULL)
				{
					first = &(*p);
				}
				else
				if (List->IndexOf(&(*p)) < 0)
				{
					List->Add(&(*p));
					first = &(*p);
				}
			}
		}
	}
	else //logic mode
	{
		std::list<TAnimationFrameData*>::iterator ip;
		for(ip = mNRList.begin(); ip != mNRList.end(); ++ip)
		{
			TAnimationFrameData* p = *ip;
			if (y0 + Ceil((p->posY - (double)p->rh) * mScale) <= SelectionRect.Bottom &&
				y0 + Ceil(p->posY * mScale) >= SelectionRect.Top &&
				x0 + Ceil((p->posX - (double)(p->rw / 2)) * mScale) <= SelectionRect.Right &&
				x0 + Ceil(((double)(p->rw / 2) + p->posX) * mScale) >= SelectionRect.Left)
			{
				if(List == NULL)
				{
					first = p;
				}
				else
				if (List->IndexOf(p) < 0)
				{
					List->Add(p);
					first = p;
				}
			}
		}

		p = &mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE];
		if ((y0 + Ceil(p->posY * mScale) <= SelectionRect.Bottom &&
				y0 + Ceil((p->posY + (double)p->rh) * mScale) >= SelectionRect.Top &&
				x0 + Ceil(p->posX * mScale) - 1 <= SelectionRect.Right &&
				x0 + Ceil(p->posX * mScale) + 1 >= SelectionRect.Left)
				||
			(y0 + Ceil(p->posY * mScale) <= SelectionRect.Bottom &&
				y0 + Ceil((p->posY + (double)p->rh) * mScale) >= SelectionRect.Top &&
				x0 + Ceil((p->posX + (double)p->rw) * mScale) - 1 <= SelectionRect.Right &&
				x0 + Ceil((p->posX + (double)p->rw) * mScale) + 1 >= SelectionRect.Left)
				||
			(x0 + Ceil(p->posX * mScale) <= SelectionRect.Right &&
				x0 + Ceil((p->posX + (double)p->rw) * mScale) >= SelectionRect.Left &&
				y0 + Ceil(p->posY * mScale) - 1 <= SelectionRect.Bottom &&
				y0 + Ceil(p->posY * mScale) + 1 >= SelectionRect.Top)
				||
			(x0 + Ceil(p->posX * mScale) <= SelectionRect.Right &&
				x0 + Ceil((p->posX + (double)p->rw) * mScale) >= SelectionRect.Left &&
				y0 + Ceil((p->posY + (double)p->rh) * mScale) - 1 <= SelectionRect.Bottom &&
				y0 + Ceil((p->posY + (double)p->rh) * mScale) + 1 >= SelectionRect.Top))
		{
			if(List == NULL)
			{
				if(first == NULL)
				{
					first = p;
                }
			}
			else
			if (List->IndexOf(p) < 0)
			{
				List->Add(p);
				if(first == NULL)
				{
					first = p;
                }
			}
		}

		p = &mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW];
		if ((y0 + Ceil(p->posY * mScale) <= SelectionRect.Bottom &&
				y0 + Ceil((p->posY + (double)p->rh) * mScale) >= SelectionRect.Top &&
				x0 + Ceil(p->posX * mScale) - 1 <= SelectionRect.Right &&
				x0 + Ceil(p->posX * mScale) + 1 >= SelectionRect.Left)
				||
			(y0 + Ceil(p->posY * mScale) <= SelectionRect.Bottom &&
				y0 + Ceil((p->posY + (double)p->rh) * mScale) >= SelectionRect.Top &&
				x0 + Ceil((p->posX + (double)p->rw) * mScale) - 1 <= SelectionRect.Right &&
				x0 + Ceil((p->posX + (double)p->rw) * mScale) + 1 >= SelectionRect.Left)
				||
			(x0 + Ceil(p->posX * mScale) <= SelectionRect.Right &&
				x0 + Ceil((p->posX + (double)p->rw) * mScale) >= SelectionRect.Left &&
				y0 + Ceil(p->posY * mScale) - 1 <= SelectionRect.Bottom &&
				y0 + Ceil(p->posY * mScale) + 1 >= SelectionRect.Top)
				||
			(x0 + Ceil(p->posX * mScale) <= SelectionRect.Right &&
				x0 + Ceil((p->posX + (double)p->rw) * mScale) >= SelectionRect.Left &&
				y0 + Ceil((p->posY + (double)p->rh) * mScale) - 1 <= SelectionRect.Bottom &&
				y0 + Ceil((p->posY + (double)p->rh) * mScale) + 1 >= SelectionRect.Top))
		{
			if(List == NULL)
			{
				if(first == NULL)
				{
					first = p;
				}
			}
			else
			if (List->IndexOf(p) < 0)
			{
				List->Add(p);
				if(first == NULL)
				{
					first = p;
				}
			}
		}
	}
	return first;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::DragObjListClear()
{
	mpDragObjList->Clear();
	mpLastDragObj = NULL;
	SaveObjValuesFromGrid();
	VLEObj->Tag = NativeInt(0);
	SetObjValuesToGrid();
	CheckObjEditPosibility(false);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::DragObjListAdd(TAnimationFrameData *pt)
{
	assert(pt);
	if (DragObjListContains(pt))
	{
		return;
	}
	mpDragObjList->Add(pt);
	mpLastDragObj = pt;
	if(TabSetAniMode->TabIndex == 0)
	{
        SaveObjValuesFromGrid();
		VLEObj->Tag = reinterpret_cast<NativeInt>(mpLastDragObj);
		SetObjValuesToGrid();
	}
	else //logic mode
	{
		std::list<TAnimationFrameData*>::iterator find = std::find(mNRList.begin(), mNRList.end(), pt);
		if(find != mNRList.end())
		{
			mNRList.remove(pt);
			mNRList.push_back(pt);

			std::list<TCheckBox*>::iterator icb;
			TValueListEditor* vle = VLENode1;
			if(pt == reinterpret_cast<TAnimationFrameData*>(vle->Tag))
			{
				RBNode1->SetFocus();
				RBNode1->Checked = true;
				for(icb = mDynCheckboxes.begin(); icb != mDynCheckboxes.end(); ++icb)
				{
					TCheckBox* cb = *icb;
					cb->Checked = false;
				}
			}
			else
			{
				RBNode1->Checked = false;
				std::vector<TValueListEditor*>::iterator ivle;
				for(ivle = mVLENodes.begin(); ivle != mVLENodes.end(); ++ivle)
				{
					vle = *ivle;
					if(pt == reinterpret_cast<TAnimationFrameData*>(vle->Tag))
					{
						for(icb = mDynCheckboxes.begin(); icb != mDynCheckboxes.end(); ++icb)
						{
							TCheckBox* cb = *icb;
							TValueListEditor* fvle = reinterpret_cast<TValueListEditor*>(cb->Tag);
							if(fvle == vle)
							{
								cb->SetFocus();
								cb->Checked = true;
							}
							else
							{
								cb->Checked = false;
							}
						}
						break;
					}
				}
			}
		}
	}
	CheckObjEditPosibility(false);
}
//---------------------------------------------------------------------------

TAnimationFrameData *__fastcall TFObjParent::DragObjListAddMulti(TRect &SelectionRect, bool UnselectDublicates)
{
	TAnimationFrameData *first = NULL;
	if (UnselectDublicates)
	{
		__int32 i, pos;
		TList *tlist = new TList();
		FindObjectPointers(tlist, SelectionRect);
		for (i = 0; i < tlist->Count; i++)
		{
			if ((pos = mpDragObjList->IndexOf(tlist->Items[i])) < 0)
			{
				mpDragObjList->Add(tlist->Items[i]);
				if (first == NULL)
				{
					first = static_cast<TAnimationFrameData*>(tlist->Items[i]);
				}
			}
			else
			{
				mpDragObjList->Delete(pos);
			}
		}
		delete tlist;
	}
	else
	{
		first = static_cast<TAnimationFrameData*>(FindObjectPointers(mpDragObjList, SelectionRect));
	}
	if (first != NULL)
	{
		mpLastDragObj = first;
		if(TabSetAniMode->TabIndex == 0)
		{
            SaveObjValuesFromGrid();
			VLEObj->Tag = reinterpret_cast<NativeInt>(mpLastDragObj);
			SetObjValuesToGrid();
		}
	}
	CheckObjEditPosibility(false);
	return mpLastDragObj;
}
//---------------------------------------------------------------------------

bool __fastcall TFObjParent::DragObjListContains(TAnimationFrameData *pt)
{
	assert(pt);
	return mpDragObjList->IndexOf(pt) >= 0;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::RemoveDragObjFromList(TAnimationFrameData *pt)
{
	for (__int32 i = 0; i < mpDragObjList->Count; i++)
	{
		if (mpDragObjList->Items[i] == pt)
		{
			mpDragObjList->Delete(i);
			CheckObjEditPosibility(false);
			return;
		}
	}
}
//---------------------------------------------------------------------------

__int32 __fastcall TFObjParent::DragObjListCount()
{
	return mpDragObjList->Count;
}
//---------------------------------------------------------------------------

TAnimationFrameData* __fastcall TFObjParent::GetDragObjFromList(__int32 idx)
{
	assert(idx >= 0 || idx < DragObjListCount());
	return static_cast<TAnimationFrameData*>(mpDragObjList->Items[idx]);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::DragObjListSave()
{
	mpSaveDragObjList->Clear();
	for (__int32 i = 0; i < mpDragObjList->Count; i++)
	{
		mpSaveDragObjList->Add(reinterpret_cast<void*>(static_cast<TAnimationFrameData*>(mpDragObjList->Items[i])->frameId));
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::DragObjListRestore()
{
	std::list<TAnimationFrameData>::iterator p;
	for(p = mBFList.begin(); p != mBFList.end(); ++p)
	{
		if(mpSaveDragObjList->IndexOf(reinterpret_cast<void*>(p->frameId)) >= 0)
		{
			DragObjListAdd(&(*p));
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::MoveSelectedObjects(double dx, double dy)
{
	TAnimationFrameData *obj;
	__int32 ct = DragObjListCount();
	for (__int32 i = 0; i < ct; ++i)
	{
		obj = GetDragObjFromList(i);
		obj->posX += dx;
		obj->posY += dy;
		if(obj->posX > (double)MAX_BOUND)
		{
			obj->posX = (double)MAX_BOUND;
		}
		if(obj->posY > (double)MAX_BOUND)
		{
			obj->posY = (double)MAX_BOUND;
		}
		if(obj->posX < (double)MIN_BOUND)
		{
			obj->posX = (double)MIN_BOUND;
		}
		if(obj->posY < (double)MIN_BOUND)
		{
			obj->posY = (double)MIN_BOUND;
		}
		if(TabSetAniMode->TabIndex == 0) //animation mode
		{
			SetObjValuesToGrid();
		}
		else
		if(TabSetAniMode->TabIndex == 1) //logic mode
		{
			SetLogicValuesToGrid(obj);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ResizeSelectedObject(__int32 dx, __int32 dy, bool proportional)
{
	if(DragObjListCount() == 1)
	{
		TAnimationFrameData *obj = GetDragObjFromList(0);
		__int32 w = obj->rw;
		__int32 h = obj->rh;

		w += proportional ? (abs(dx) > abs(dy) ? dx : dy) : dx;
		h += proportional ? (abs(dx) > abs(dy) ? dx : dy) : dy;
		if(w > MAX_RESIZE)
		{
			w = MAX_RESIZE;
		}
		if(h > MAX_RESIZE)
		{
			h = MAX_RESIZE;
		}
		if(w < MIN_RESIZE)
		{
			w = MIN_RESIZE;
		}
		if(h < MIN_RESIZE)
		{
			h = MIN_RESIZE;
		}

		if(proportional)
		{
			if(mCopyRect.mProrotional != proportional)
			{
				mCopyRect.mRect.left = (__int32)obj->posX;
				mCopyRect.mRect.Top = (__int32)obj->posY;
				mCopyRect.mRect.Right = mCopyRect.mRect.left + obj->rw;
				mCopyRect.mRect.Bottom = mCopyRect.mRect.Top + obj->rh;
			}
			__int32 dw = w - obj->rw;
			__int32 dh = h - obj->rh;
			obj->rw = w + dw;
			obj->rh = h + dh;
			obj->posX = (double)(mCopyRect.mRect.left - (obj->rw - mCopyRect.mRect.Width()) / 2);
			obj->posY = (double)(mCopyRect.mRect.Top - (obj->rh - mCopyRect.mRect.Height()) / 2);
		}
		else
		{
			obj->rw = w;
			obj->rh = h;
		}

		mCopyRect.mProrotional = proportional;

		if(TabSetAniMode->TabIndex == 0)
		{
			SetObjValuesToGrid();
		}
	}
}
//---------------------------------------------------------------------------

bool __fastcall TFObjParent::CheckAbilityToPlaceObj(__int32 &x, __int32 &y)
{
	TRect r;
	TPoint p;
	GetCursorPos(&p);
	p = FObjParent->ScreenToClient(p);
	GetRenderRect(r);
	__int32 x0 = (r.Left * 2) + r.Width() / 2;
	__int32 y0 = (r.Top * 2) + r.Height() / 2;
	x = Ceil((double)(p.x - x0) / mScale);
	y = Ceil((double)(p.y - y0) / mScale);
	return Ceil((double)MIN_BOUND * mScale) + x0 <= p.x && Ceil((double)MAX_BOUND * mScale) + x0 >= p.x &&
			Ceil((double)MIN_BOUND * mScale) + y0 <= p.y && Ceil((double)MAX_BOUND * mScale) + y0 >= p.y;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::PopupMenuObjPopup(TObject *)
{
	CheckObjEditPosibility(true);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::PopupMenuXYWHPopup(TObject *)
{
	CheckObjEditPosibility(true);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CheckObjEditPosibility(bool checkMouse)
{
	TBaseItem *li = CategoryButtons_s->SelectedItem;
	mPopupX = 0;
	mPopupY = 0;
	if(checkMouse)
	{
		checkMouse = CheckAbilityToPlaceObj(mPopupX, mPopupY);
	}
	else
	{
		checkMouse = true;
	}
	bool fragment = ((TabSetGraphicsMode->TabIndex == 0 && li != NULL && li->ClassNameIs(_T("TButtonItem"))) ||
					 (TabSetGraphicsMode->TabIndex == 1 && FilesList->Selected != NULL)) && checkMouse;
	bool animation = TabSetAniMode->TabIndex == 0 && mpAnimation != NULL;
	bool logic = TabSetAniMode->TabIndex == 1 && mpAnimation != NULL;
	bool edit = DragObjListCount() > 0;
	bool single = DragObjListCount() == 1;
	ActObjAdd->Enabled = animation && fragment;
	N6->Enabled = animation && fragment;
	ActObjDelete->Enabled = animation && edit;
	ActObjEdit->Enabled = animation && edit;
	ActObjFindParent->Enabled = animation && edit;
	ActObjLevelUp->Enabled = animation && edit && DragObjListCount() == 1;
	ActObjLevelDown->Enabled = animation && edit && DragObjListCount() == 1;
	ActObjCopy->Enabled = animation && edit;
	ActSelectAll->Enabled = (animation && !mBFList.empty()) || logic;
	ActDeselectAll->Enabled = (animation || logic) && edit;
	ActObjCopy->Enabled = (animation && edit) || (logic && edit);
	ActObjPaste->Enabled = animation || logic;
	ActCreateIcon->Enabled = animation;
	ActTabNext->Enabled = (animation && !mBFList.empty()) || logic;
	VLEObj->Enabled = animation && edit;
	ActionToCenter->Enabled = animation && single;
	ActionToDefaultSizes->Enabled = animation && single;

	N6->Visible = TabSetAniMode->TabIndex == 0;
	N8->Visible = TabSetAniMode->TabIndex == 0;
	N10->Visible = TabSetAniMode->TabIndex == 0;
	N20->Visible = TabSetAniMode->TabIndex == 0;
	N9->Visible = TabSetAniMode->TabIndex == 0;
	N7->Visible = TabSetAniMode->TabIndex == 0;
	N12->Visible = TabSetAniMode->TabIndex == 0;
	N13->Visible = TabSetAniMode->TabIndex == 0;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::N6Click(TObject *)
{
	mFromPopup = true;
	ActObjAdd->Execute();
	mFromPopup = false;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CategoryButtons_sSelectedItemChange(TObject *, const TButtonItem *)
{
	CheckObjEditPosibility(false);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CategoryButtons_sSelectedCategoryChange(TObject *, const TButtonCategory *)
{
	CheckObjEditPosibility(false);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActObjAddExecute(TObject *)
{
	if(mpAnimation && TabSetAniMode->TabIndex == 0) //animation mode
	{
		__int32 x, y;
		if(!mFromPopup)
		{
			mPopupX = 0;
			mPopupY = 0;
		}
		x = mPopupX;
		y = mPopupY;
		if(TabSetGraphicsMode->TabIndex == 0) // standart
		{
			TBaseItem *li = CategoryButtons_s->SelectedItem;
			if(li != NULL && li->ClassNameIs(_T("TButtonItem")))
			{
				TAnimationFrameData afd;
				TButtonItem *bi = static_cast<TButtonItem*>(li);
				TTempBGobj* to = static_cast<TTempBGobj*>(bi->Data);
				afd.posX = static_cast<double>(x);
				afd.posY = static_cast<double>(y);
				afd.rw = to->o.w;
				afd.rh = to->o.h;
				afd.SetFrameId(to->o.getHash());
				afd.type = LogicObjType::UOP_LOGIC_OBJ_BMP;

				const BMPImage* tbmp = mpTempResObjManager->GetGraphicResource(to->o.getResIdx());
				if(tbmp == NULL)
				{
					__int32 res_w, res_h, res_idx;
					UnicodeString res_fname;
					to->o.getSourceRes(&res_w, &res_h, &res_idx, &res_fname);
					if(res_fname.Length() > 0 && res_w >= 0 && res_h >= 0)
					{
						__int32 g_idx;
						if ((g_idx = mpTempResObjManager->GetGraphicResourceIndex(res_fname)) < 0)
						{
							Graphics::TBitmap *bmp = new Graphics::TBitmap();
							UnicodeString path = Form_m->PathPngRes + res_fname;
							read_png(path.c_str(), bmp);
							BMPImage *i2d = new BMPImage(bmp);
							g_idx = mpTempResObjManager->AddGraphicResource(i2d, res_fname);
							to->o.setSourceRes(bmp->Width, bmp->Height, g_idx, res_fname);
							mpTempResObjManager->AddBitmapFrame(to->o);
							delete i2d;
						}
						else
						{
							to->o.setSourceRes(res_w, res_h, g_idx, res_fname);
						}
					}
				}

				mBFList.push_back(afd);
				DragObjListClear();
				std::list<TAnimationFrameData>::iterator p = mBFList.end();
				p--;
				DragObjListAdd(&(*p));
				LabelFrafments->Caption = IntToStr(static_cast<__int32>(mBFList.size()));
			}
		}
		else if(TabSetGraphicsMode->TabIndex == 1) // stream
		{
			if(FilesList->Enabled && FoldersList->Selected != NULL && FilesList->Selected != NULL)
			{
				std::list<TAnimationFrameData>::iterator p;
				for(p = mBFList.begin(); p != mBFList.end(); ++p)
				{
					if(p->isStreamFrame())
					{
						Application->MessageBox(Localization::Text(_T("mOnlyOneStreamObjMessage")).c_str(),
													_T("Stream object\0"), MB_OK | MB_ICONERROR);
						return;
                    }
				}
				UnicodeString path = Form_m->PathPngRes +
							FoldersList->Selected->Caption + _T("\\") + FilesList->Selected->Caption;
				Graphics::TBitmap *bmp = new Graphics::TBitmap();
				if(FileExists(path))
				if(read_png(path.c_str(), bmp))
				{
					__int32 ew, eh;
					TAnimationFrameData afd;
					afd.posX = static_cast<double>(x);
					afd.posY = static_cast<double>(y);
					afd.rw = bmp->Width;
					afd.rh = bmp->Height;
					if(mpAnimation->HasStreamObject())
					{
						ew = mpAnimation->GetStreamObjectWidth();
						eh = mpAnimation->GetStreamObjectHeight();
					}
					else
					{
						ew = afd.rw;
						eh = afd.rh;
					}
					if(afd.rw != ew || afd.rh != eh)
					{
						UnicodeString mes = Localization::Text(_T("mWrongSizeStreamObj")) + IntToStr(ew) + _T("x") + IntToStr(eh);
						Application->MessageBox(mes.c_str(), _T("Stream object\0"), MB_OK | MB_ICONERROR);
					}
					else
					{
						afd.SetFrameId(0);
						afd.type = LogicObjType::UOP_LOGIC_OBJ_BMP;
						afd.setStreamFrame(true);
						afd.strm_filename = FoldersList->Selected->Caption + _T("\\") + FilesList->Selected->Caption;
						DragObjListClear();
						mBFList.push_back(afd);
						p = mBFList.end();
						p--;
						DragObjListAdd(&(*p));
						LabelFrafments->Caption = IntToStr(static_cast<__int32>(mBFList.size()));
                	}
				}
				delete bmp;
            }
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActObjDeleteExecute(TObject *)
{
	DeleteSelectedObjects();
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::DeleteSelectedObjects()
{
	if(TabSetAniMode->TabIndex == 0) //animation mode
	{
		TAnimationFrameData *obj;
		__int32 pos, ct = DragObjListCount();
		for (__int32 i = 0; i < ct; ++i)
		{
			obj = GetDragObjFromList(i);
			std::list<TAnimationFrameData>::iterator p;
			for(p = mBFList.begin(); p != mBFList.end(); ++p)
			{
				if(obj == &(*p))
				{
					mBFList.erase(p);
					break;
				}
			}
			LabelFrafments->Caption = IntToStr(static_cast<int>(mBFList.size()));
		}
		DragObjListClear();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActObjEditExecute(TObject *)
{
	void *obj = FindReference(mpLastDragObj);
	if(obj)
	{
		ActEdit->Execute();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActObjLevelUpExecute(TObject *)
{
	if(TabSetAniMode->TabIndex == 0 && DragObjListCount() == 1) //animation mode
	{
		__int32 i;
		TAnimationFrameData *obj;
		std::list<TAnimationFrameData>::iterator p;
		obj = mpLastDragObj;
		DragObjListClear();
		for(p = mBFList.begin(); p != mBFList.end(); ++p)
		{
			if(obj == &(*p))
			{
				std::list<TAnimationFrameData>::iterator p1 = p;
				p1++;
				if(p1 != mBFList.end())
				{
					TAnimationFrameData po = *p;
					TAnimationFrameData p1o = *p1;
					*p1 = po;
					*p = p1o;
					DragObjListAdd(&(*p1));
				}
				else
				{
					DragObjListAdd(&(*p));
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActObjLevelDownExecute(TObject *)
{
	if(TabSetAniMode->TabIndex == 0) //animation mode
	{
		__int32 i;
		TAnimationFrameData *obj;
		std::list<TAnimationFrameData>::iterator p;
		obj = mpLastDragObj;
		DragObjListClear();
		for(p = mBFList.begin(); p != mBFList.end(); ++p)
		{
			if(obj == &(*p))
			{
				std::list<TAnimationFrameData>::iterator p1 = p;
				p1--;
				if(p != mBFList.begin())
				{
					TAnimationFrameData po = *p;
					TAnimationFrameData p1o = *p1;
					*p1 = po;
					*p = p1o;
					DragObjListAdd(&(*p1));
				}
				else
				{
					DragObjListAdd(&(*p));
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActObjFindParentExecute(TObject *)
{
	FindReference(mpLastDragObj);
}
//---------------------------------------------------------------------------

void* __fastcall TFObjParent::FindReference(TAnimationFrameData *pt)
{
	if(pt == NULL)
	{
        return NULL;
    }
	if(TabSetAniMode->TabIndex == 0) //animation mode
	{
		if(pt->isStreamFrame())
		{
			assert(!pt->strm_filename.IsEmpty());
			pt->strm_filename = forceCorrectResFileExtention(pt->strm_filename);
			UnicodeString folder = ExtractFilePath(pt->strm_filename);
			CreateStreamFolderAndFileList(folder.SubString(1, folder.Length() - 1),
												ExtractFileName(pt->strm_filename));
		}
		else
		{
			__int32 cc = CategoryButtons_s->Categories->Count;
			for(__int32 i = 0; i < cc; i++)
			{
				__int32 ct = CategoryButtons_s->Categories->Items[i]->Items->Count;
				for(__int32 j = 0; j < ct; j++)
				{
					TBaseItem *li = CategoryButtons_s->Categories->Items[i]->Items->Items[j];
					if (li != NULL && li->ClassNameIs(_T("TButtonItem")))
					{
						TButtonItem *bi = static_cast<TButtonItem*>(li);
						if(pt->frameId == static_cast<TTempBGobj*>(bi->Data)->o.getHash())
						{
							TabSetGraphicsMode->TabIndex = 0;
							CategoryButtons_s->SelectedItem = bi;
							bi->Category->Collapsed = false;
							CategoryButtons_s->SelectedItem->ScrollIntoView();
							return bi->Data;
						}
					}
				}
			}
		}
	}
	return NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActTabNextExecute(TObject *)
{
	if(TabSetAniMode->TabIndex == 0) //animation mode
	{
		if(!mBFList.empty())
		{
			std::list<TAnimationFrameData>::iterator p;
			if(mpLastDragObj == NULL)
			{
				p = mBFList.begin();
				DragObjListClear();
				DragObjListAdd(&(*p));
			}
			else
			for(p = mBFList.begin(); p != mBFList.end(); ++p)
			{
				if(mpLastDragObj == &(*p))
				{
					p++;
					if(p == mBFList.end())
					{
						p = mBFList.begin();
					}
					DragObjListClear();
					DragObjListAdd(&(*p));
				}
			}
		}
	}
	else // logics mode
	{
		void *pt = mpLastDragObj;
		DragObjListClear();
		if(pt == NULL || pt == &mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_NODE3])
		{
			DragObjListAdd(&mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE]);
		}
		else if(pt == &mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE])
		{
			DragObjListAdd(&mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW]);
		}
		else if(pt == &mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW])
		{
			DragObjListAdd(&mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_NODE1]);
		}
		else
		{
            int n = 1;
			for (__int32 i = LogicObjType::UOP_LOGIC_OBJ_NODE1; i <= LogicObjType::UOP_LOGIC_OBJ_NODE15 && n < mJoinNodesCount; i++)
			{
				if(pt == &mLogicDrawObjects[i])
				{
					DragObjListAdd(&mLogicDrawObjects[i + 1]);
					break;
				}
				n++;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActSelectAllExecute(TObject *)
{
	if(TabSetAniMode->TabIndex == 0) //animation mode
	{
		DragObjListClear();
		std::list<TAnimationFrameData>::iterator p;
		for(p = mBFList.begin(); p != mBFList.end(); ++p)
		{
			DragObjListAdd(&(*p));
		}
	}
	else // logics mode
	{
		DragObjListClear();
		DragObjListAdd(&mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_COLLIDE]);
		DragObjListAdd(&mLogicDrawObjects[LogicObjType::UOP_LOGIC_OBJ_VIEW]);
		std::list<TAnimationFrameData*>::iterator p;
		for(p = mNRList.begin(); p != mNRList.end(); ++p)
		{
			DragObjListAdd(*p);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActDeselectAllExecute(TObject *)
{
	DragObjListClear();
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::SetLogicValuesToGrid(TAnimationFrameData *obj)
{
	TAnimationFrameData *data;
	data = reinterpret_cast<TAnimationFrameData*>(VLECollide->Tag);
	if(obj == NULL || data == obj)
	{
		VLECollide->Values[_T("X")] = (__int32)data->posX;
		VLECollide->Values[_T("Y")] = (__int32)data->posY;
		VLECollide->Values[_T("W")] = data->rw;
		VLECollide->Values[_T("H")] = data->rh;
	}
	data = reinterpret_cast<TAnimationFrameData*>(VLEView->Tag);
	if(obj == NULL || data == obj)
	{
		VLEView->Values[_T("X")] = (__int32)data->posX;
		VLEView->Values[_T("Y")] = (__int32)data->posY;
		VLEView->Values[_T("W")] = data->rw;
		VLEView->Values[_T("H")] = data->rh;
	}
	data = reinterpret_cast<TAnimationFrameData*>(VLENode1->Tag);
	if(obj == NULL || data == obj)
	{
		VLENode1->Values[_T("X")] = (__int32)data->posX;
		VLENode1->Values[_T("Y")] = (__int32)data->posY;
	}
	std::vector<TValueListEditor*>::iterator ivle;
	for(ivle = mVLENodes.begin(); ivle != mVLENodes.end(); ++ivle)
	{
		TValueListEditor* vle = *ivle;
		data = reinterpret_cast<TAnimationFrameData*>(vle->Tag);
		if(obj == NULL || data == obj)
		{
			vle->Values[_T("X")] = (__int32)data->posX;
			vle->Values[_T("Y")] = (__int32)data->posY;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::SetObjValuesToGrid()
{
	TAnimationFrameData *data;
	data = reinterpret_cast<TAnimationFrameData*>(VLEObj->Tag);
	if(data != NULL)
	{
		VLEObj->Values[_T("X")] = (__int32)data->posX;
		VLEObj->Values[_T("Y")] = (__int32)data->posY;
		if(DragObjListCount() == 1)
		{
			VLEObj->Values[_T("W")] = data->rw;
			VLEObj->Values[_T("H")] = data->rh;
		}
		else
		{
			VLEObj->Values[_T("W")] = _T("");
			VLEObj->Values[_T("H")] = _T("");
		}
		VLEObj->Values[Localization::Text(_T("mTransparencyText"))] = data->a;
	}
	else
	{
		VLEObj->Values[_T("X")] = _T("");
		VLEObj->Values[_T("Y")] = _T("");
		VLEObj->Values[_T("W")] = _T("");
		VLEObj->Values[_T("H")] = _T("");
		VLEObj->Values[Localization::Text(_T("mTransparencyText"))] = _T("");
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::SaveObjValuesFromGrid()
{
	if(VLEObj->Tag != NativeInt(0))
	{
		for(__int32 i = 0; i < VLEObj->RowCount; i++)
		{
			UnicodeString KeyName = VLEObj->Keys[VLEObj->Row];
			VLECollideValidate(VLEObj, 1, i, KeyName, VLEObj->Values[KeyName]);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::VLECollideValidate(TObject *Sender, System::LongInt, System::LongInt,
		  const UnicodeString KeyName, const UnicodeString KeyValue)
{
	TValueListEditor *le = static_cast<TValueListEditor*>(Sender);
	if(le)
	{
		TAnimationFrameData *data = reinterpret_cast<TAnimationFrameData*>(le->Tag);
		if(data)
		{
			__int32 val;
			try
			{
				val = StrToInt(KeyValue);
			}
			catch(...)
			{
				val = 0;
			}
			if(KeyName.Compare(_T("X")) == 0)
			{
				if(val > MAX_BOUND)
				{
					val = MAX_BOUND;
					le->Values[KeyName] = IntToStr(val);
				}
				if(val < MIN_BOUND)
				{
					val = MIN_BOUND;
					le->Values[KeyName] = IntToStr(val);
				}
				data->posX = (double)val;
			}
			if(KeyName.Compare(_T("Y")) == 0)
			{
				if(val > MAX_BOUND)
				{
					val = MAX_BOUND;
					le->Values[KeyName] = IntToStr(val);
				}
				if(val < MIN_BOUND)
				{
					val = MIN_BOUND;
					le->Values[KeyName] = IntToStr(val);
				}
				data->posY = (double)val;
			}
			if(KeyName.Compare(_T("W")) == 0)
			{
				if(data->type == LogicObjType::UOP_LOGIC_OBJ_BMP)
				{
					if(DragObjListCount() == 1)
					{
						if(val > MAX_RESIZE)
						{
							val = MAX_RESIZE;
							le->Values[KeyName] = IntToStr(val);
						}
						if(val < MIN_RESIZE)
						{
							val = MIN_RESIZE;
							le->Values[KeyName] = IntToStr(val);
						}
						data->rw = val;
					}
					else
					{
						le->Values[KeyName] = _T("");
					}
				}
				else
				{
					if(val < 0)
					{
						val = 0;
						le->Values[KeyName] = IntToStr(val);
					}
					data->rw = val;
				}
			}
			if(KeyName.Compare(_T("H")) == 0)
			{
				if(data->type == LogicObjType::UOP_LOGIC_OBJ_BMP)
				{
					if(DragObjListCount() == 1)
					{
						if(val > MAX_RESIZE)
						{
							val = MAX_RESIZE;
							le->Values[KeyName] = IntToStr(val);
						}
						if(val < MIN_RESIZE)
						{
							val = MIN_RESIZE;
							le->Values[KeyName] = IntToStr(val);
						}
						data->rh = val;
					}
					else
					{
						le->Values[KeyName] = _T("");
					}
				}
				else
				{
					if(val < 0)
					{
						val = 0;
						le->Values[KeyName] = IntToStr(val);
					}
					data->rh = val;
				}
			}
			if(KeyName.Compare(Localization::Text(_T("mTransparencyText"))) == 0)
			{
				if(val < 0)
				{
					val = 0;
					le->Values[KeyName] = IntToStr(val);
				}
				if(val > 255)
				{
					val = 255;
					le->Values[KeyName] = IntToStr(val);
				}
				data->a = static_cast<unsigned char>(val);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::VLECollideKeyPress(TObject *Sender, wchar_t &Key)
{
	if(Key == VK_RETURN)
	{
		TValueListEditor *le = static_cast<TValueListEditor*>(Sender);
		UnicodeString KeyName = le->Keys[le->Row];
		VLECollideValidate(le, le->Col, le->Row, KeyName, le->Values[KeyName]);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::Edit_durationKeyPress(TObject *, wchar_t &Key)
{
	if(Key == VK_RETURN)
	{
		Edit_durationExit(NULL);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::Edit_durationExit(TObject *)
{
	if (mpAnimation && mAnimationFrameIdx >= 0)
	{
		TAnimationFrame *af = const_cast<TAnimationFrame*>(mpAnimation->GetFrame(mAnimationFrameIdx));
		if(af != NULL)
		{
			try
			{
				af->SetDuration(StrToInt(Edit_duration->Text));
				mTimerDuration = af->GetDuration();
				if(mTimerDuration < MINIMAL_TIMER_DURATION)
				{
					mTimerDuration = MINIMAL_TIMER_DURATION;
				}
			}
			catch(...)
			{
			}
		}
	}
}
//---------------------------------------------------------------------------

//��� ����, ����� �� ����������� ��������� �������������� � ��������� ������� ����
void __fastcall TFObjParent::GetWndParams(TEdPrObjWndParams *params)
{
	if (params == NULL)
	{
		return;
	}
	params->init = true;
	params->Left = Left;
	params->Top = Top;
	params->Width = Width;
	params->Height = Height;
	params->scale = ComboBoxScale->ItemIndex;
	params->BgColor = mCanvasColor;
	params->splitterLV = PanelLeft->Width;
	params->joinNodesCount = mJoinNodesCount;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::SetWndParams(const TEdPrObjWndParams *params)
{
	if (params == NULL)
	{
		return;
	}

	mJoinNodesCount = params->joinNodesCount;
	InitJoinNodes();

	if(params->init == false)
	{
		return;
	}
	Left = params->Left;
	Top = params->Top;
	Width = params->Width;
	Height = params->Height;
	ComboBoxScale->ItemIndex = params->scale;
	mCanvasColor = params->BgColor;
	PanelLeft->Width = params->splitterLV;
	ComboBoxScaleChange(ComboBoxScale);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActObjCopyExecute(TObject *)
{
	if (DragObjListCount() == 0 || Form_m->CF_ME == 0)
	{
		return;
	}

	if(ActiveControl)
	{
		if(ActiveControl->ClassNameIs(_T("TEdit")))
		{
			TEdit *ed = static_cast<TEdit*>(ActiveControl);
			if(FakeControl != ed)
			{
				return;
            }
		}
		else
		if(ActiveControl->ClassNameIs(_T("TComboBox")))
		{
			return;
		}
		else
		if(ActiveControl->ClassNameIs(_T("TValueListEditor")))
		{
			return;
		}
	}

	SaveFrameData();

	__int32 i;
	BYTE ch[3];
	TMemoryStream *pCopyBuffer = new TMemoryStream();
	ch[0] = CLIPBOARD_VER;
	ch[1] = CLIPBOARD_TYPE_FRAME;
	pCopyBuffer->Clear();
	pCopyBuffer->Write(ch, 2);
	for (i = 0; i < DragObjListCount(); i++)
	{
		short w_val;
		UnicodeString sname;
		TAnimationFrameData* fd = GetDragObjFromList(i);
		switch(fd->type)
		{
			case LogicObjType::UOP_LOGIC_OBJ_COLLIDE:
			case LogicObjType::UOP_LOGIC_OBJ_VIEW:
			case LogicObjType::UOP_LOGIC_OBJ_NODE1:
			case LogicObjType::UOP_LOGIC_OBJ_NODE2:
			case LogicObjType::UOP_LOGIC_OBJ_NODE3:
			case LogicObjType::UOP_LOGIC_OBJ_NODE4:
			case LogicObjType::UOP_LOGIC_OBJ_NODE5:
			case LogicObjType::UOP_LOGIC_OBJ_NODE6:
			case LogicObjType::UOP_LOGIC_OBJ_NODE7:
			case LogicObjType::UOP_LOGIC_OBJ_NODE8:
			case LogicObjType::UOP_LOGIC_OBJ_NODE9:
			case LogicObjType::UOP_LOGIC_OBJ_NODE10:
			case LogicObjType::UOP_LOGIC_OBJ_NODE11:
			case LogicObjType::UOP_LOGIC_OBJ_NODE12:
			case LogicObjType::UOP_LOGIC_OBJ_NODE13:
			case LogicObjType::UOP_LOGIC_OBJ_NODE14:
			case LogicObjType::UOP_LOGIC_OBJ_NODE15:
			case LogicObjType::UOP_LOGIC_OBJ_NODE16:
			{
				ch[0] = objRESLOGICFRAME;
				pCopyBuffer->Write(ch, 1);
				hash_t t = (hash_t)fd->type;
				pCopyBuffer->Write(&t, static_cast<System::LongInt>(sizeof(hash_t)));
			}
			break;
			default:
				ch[0] = objRESBMPFRAME;
				pCopyBuffer->Write(ch, 1);
				pCopyBuffer->Write(&fd->frameId, static_cast<System::LongInt>(sizeof(hash_t)));
		}
		__int32 tval = (__int32)fd->posX;
		pCopyBuffer->Write(&tval, static_cast<System::LongInt>(sizeof(__int32)));
		tval = (__int32)fd->posY;
		pCopyBuffer->Write(&tval, static_cast<System::LongInt>(sizeof(__int32)));
		pCopyBuffer->Write(&fd->rw, static_cast<System::LongInt>(sizeof(__int32)));
		pCopyBuffer->Write(&fd->rh, static_cast<System::LongInt>(sizeof(__int32)));
		tval = fd->a;
		pCopyBuffer->Write(&tval, static_cast<System::LongInt>(sizeof(__int32)));
		tval = fd->getData();
		pCopyBuffer->Write(&tval, static_cast<System::LongInt>(sizeof(__int32)));
		sname = fd->strm_filename;
		w_val = (short)sname.Length();
		pCopyBuffer->Write(&w_val, static_cast<System::LongInt>(sizeof(short)));
		if(w_val > 0)
		{
			pCopyBuffer->Write(sname.c_str(), static_cast<System::LongInt>(sizeof(wchar_t) * w_val));
		}
		if(ch[0] == objRESBMPFRAME && !fd->isStreamFrame())
		{
			__int32 sw, sh, ridx;
			const TFrameObj *fobj = mpTempResObjManager->GetBitmapFrame(fd->frameId);
			assert(fobj);
			fobj->getSourceRes(&sw, &sh, &ridx, &sname);
			w_val = (short)sname.Length();
			pCopyBuffer->Write(&w_val, static_cast<System::LongInt>(sizeof(short)));
			if(w_val > 0)
			{
				pCopyBuffer->Write(sname.c_str(), static_cast<System::LongInt>(sizeof(wchar_t) * w_val));
			}
			sname = fobj->groupId;
			w_val = (short)sname.Length();
			pCopyBuffer->Write(&w_val, static_cast<System::LongInt>(sizeof(short)));
			if(w_val > 0)
			{
				pCopyBuffer->Write(sname.c_str(), static_cast<System::LongInt>(sizeof(wchar_t) * w_val));
			}
			pCopyBuffer->Write(&sw, static_cast<System::LongInt>(sizeof(__int32)));
			pCopyBuffer->Write(&sh, static_cast<System::LongInt>(sizeof(__int32)));
			pCopyBuffer->Write(&ridx, static_cast<System::LongInt>(sizeof(__int32)));
			pCopyBuffer->Write(&fobj->x, static_cast<System::LongInt>(sizeof(short)));
			pCopyBuffer->Write(&fobj->y, static_cast<System::LongInt>(sizeof(short)));
			pCopyBuffer->Write(&fobj->w, static_cast<System::LongInt>(sizeof(short)));
			pCopyBuffer->Write(&fobj->h, static_cast<System::LongInt>(sizeof(short)));
		}
	}
	ch[0] = objNONE;
	pCopyBuffer->Write(ch, 1);
	{
		HGLOBAL aDataHandle;
		void *aDataPtr;
		TClipboard *cb = new TClipboard();
		cb->Open();
		pCopyBuffer->Seek(0, soFromBeginning);
		aDataHandle = GlobalAlloc(GMEM_DDESHARE | GMEM_MOVEABLE, (size_t)pCopyBuffer->Size);
		try
		{
			aDataPtr = GlobalLock(aDataHandle);
			if (aDataPtr != NULL)
			{
				MoveMemory(aDataPtr, pCopyBuffer->Memory, (size_t)pCopyBuffer->Size);
				cb->Clear();
				cb->SetAsHandle(Form_m->CF_ME, (THandle)aDataHandle);
			}
			GlobalUnlock(aDataHandle);
		}
		catch(...)
		{
			GlobalFree(aDataHandle);
		}
		cb->Close();
		delete cb;
	}
	delete pCopyBuffer;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActObjPasteExecute(TObject *)
{
	if(Form_m->CF_ME == 0 || mpAnimation == NULL ||
					mAnimationFrameIdx < 0 || TabSet->TabIndex == 0)
	{
		return;
	}

	if(ActiveControl)
	{
		if(ActiveControl->ClassNameIs(_T("TEdit")))
		{
			TEdit *ed = static_cast<TEdit*>(ActiveControl);
			if(FakeControl != ed)
			{
				return;
			}
		}
		else
		if(ActiveControl->ClassNameIs(_T("TComboBox")))
		{
			return;
		}
		else
		if(ActiveControl->ClassNameIs(_T("TValueListEditor")))
		{
			return;
		}
	}

	TMemoryStream *pCopyBuffer = new TMemoryStream();
	{
		bool hasFormat;
		HGLOBAL aDataHandle;
		void *aDataPtr;
		TClipboard *cb = new TClipboard();
		hasFormat = cb->HasFormat(Form_m->CF_ME);
		if(hasFormat)
		{
			cb->Open();
			aDataHandle = (HGLOBAL)cb->GetAsHandle(Form_m->CF_ME);
			aDataPtr = GlobalLock(aDataHandle);
			if (aDataPtr != NULL)
			{
				pCopyBuffer->Clear();
				pCopyBuffer->Size = GlobalSize(aDataHandle);
				MoveMemory(pCopyBuffer->Memory, aDataPtr, (size_t)pCopyBuffer->Size);
			}
			GlobalUnlock(aDataHandle);
			cb->Close();
		}
		delete cb;
		if(!hasFormat)
		{
			delete pCopyBuffer;
			return;
		}
	}

	BYTE ch[3];
	wchar_t chstr[1024];

	pCopyBuffer->Read(ch, 2);
	if(CLIPBOARD_TYPE_FRAME != ch[1] || CLIPBOARD_VER != ch[0])
	{
		delete pCopyBuffer;
		return;
	}

	DragObjListClear();

	TAnimationFrameData afd;
	TFrameObj fobj;
	bool newFrames = false;
	pCopyBuffer->Read(ch, 1);
	bool error_stream_obj = false;
	while (objNONE != ch[0])
	{
		switch(ch[0])
		{
			case objRESLOGICFRAME:
			{
				if(TabSetAniMode->TabIndex != 1)
				{
					TabSetAniMode->TabIndex = 1;
				}
				pCopyBuffer->Read(&afd.frameId, static_cast<System::LongInt>(sizeof(hash_t)));
				afd.type = (LogicObjType)afd.frameId;
				afd.SetFrameId(0);
				__int32 tval;
				pCopyBuffer->Read(&tval, static_cast<System::LongInt>(sizeof(__int32)));
				afd.posX = (double)tval;
				pCopyBuffer->Read(&tval, static_cast<System::LongInt>(sizeof(__int32)));
				afd.posY = (double)tval;
				pCopyBuffer->Read(&afd.rw, static_cast<System::LongInt>(sizeof(__int32)));
				pCopyBuffer->Read(&afd.rh, static_cast<System::LongInt>(sizeof(__int32)));
				pCopyBuffer->Read(&tval, static_cast<System::LongInt>(sizeof(__int32)));
				afd.a = (unsigned char)tval;
				pCopyBuffer->Read(&tval, static_cast<System::LongInt>(sizeof(__int32)));
				afd.setData((unsigned short)tval);
				switch((__int32)afd.type)
				{
					case LogicObjType::UOP_LOGIC_OBJ_COLLIDE:
					case LogicObjType::UOP_LOGIC_OBJ_VIEW:
					case LogicObjType::UOP_LOGIC_OBJ_NODE1:
					case LogicObjType::UOP_LOGIC_OBJ_NODE2:
					case LogicObjType::UOP_LOGIC_OBJ_NODE3:
					case LogicObjType::UOP_LOGIC_OBJ_NODE4:
					case LogicObjType::UOP_LOGIC_OBJ_NODE5:
					case LogicObjType::UOP_LOGIC_OBJ_NODE6:
					case LogicObjType::UOP_LOGIC_OBJ_NODE7:
					case LogicObjType::UOP_LOGIC_OBJ_NODE8:
					case LogicObjType::UOP_LOGIC_OBJ_NODE9:
					case LogicObjType::UOP_LOGIC_OBJ_NODE10:
					case LogicObjType::UOP_LOGIC_OBJ_NODE11:
					case LogicObjType::UOP_LOGIC_OBJ_NODE12:
					case LogicObjType::UOP_LOGIC_OBJ_NODE13:
					case LogicObjType::UOP_LOGIC_OBJ_NODE14:
					case LogicObjType::UOP_LOGIC_OBJ_NODE15:
					case LogicObjType::UOP_LOGIC_OBJ_NODE16:
						mLogicDrawObjects[afd.type] = afd;
						DragObjListAdd(&mLogicDrawObjects[afd.type]);
				}
			}
			break;
			case objRESBMPFRAME:
			{
				short w_val;
				__int32 sw, sh, ridx;
				UnicodeString sname;

				if(TabSetAniMode->TabIndex != 0)
				{
					TabSetAniMode->TabIndex = 0;
				}
				__int32 tval;
				pCopyBuffer->Read(&afd.frameId, static_cast<System::LongInt>(sizeof(hash_t)));
				afd.type = LogicObjType::UOP_LOGIC_OBJ_BMP;
				pCopyBuffer->Read(&tval, static_cast<System::LongInt>(sizeof(__int32)));
				afd.posX = (double)tval;
				pCopyBuffer->Read(&tval, static_cast<System::LongInt>(sizeof(__int32)));
				afd.posY = (double)tval;
				pCopyBuffer->Read(&afd.rw, static_cast<System::LongInt>(sizeof(__int32)));
				pCopyBuffer->Read(&afd.rh, static_cast<System::LongInt>(sizeof(__int32)));
				pCopyBuffer->Read(&tval, static_cast<System::LongInt>(sizeof(__int32)));
				afd.a = (unsigned char)tval;
				pCopyBuffer->Read(&tval, static_cast<System::LongInt>(sizeof(__int32)));
				afd.setData((unsigned short)tval);
				pCopyBuffer->Read(&w_val, static_cast<System::LongInt>(sizeof(short)));
				afd.strm_filename = _T("");
				if(w_val > 0)
				{
					pCopyBuffer->Read(chstr, static_cast<System::LongInt>(w_val * sizeof(wchar_t)));
					chstr[w_val] = _T('\0');
					afd.strm_filename = UnicodeString(chstr);
				}
				if(!afd.isStreamFrame())
				{
					pCopyBuffer->Read(&w_val, static_cast<System::LongInt>(sizeof(short)));
					sname = _T("");
					if(w_val > 0)
					{
						pCopyBuffer->Read(chstr, static_cast<System::LongInt>(w_val * sizeof(wchar_t)));
						chstr[w_val] = L'\0';
						sname = UnicodeString(chstr);
					}
					pCopyBuffer->Read(&w_val, static_cast<System::LongInt>(sizeof(short)));
					if(w_val > 0)
					{
						pCopyBuffer->Read(chstr, static_cast<System::LongInt>(w_val * sizeof(wchar_t)));
						chstr[w_val] = L'\0';
						fobj.groupId = UnicodeString(chstr);
					}
					else
					{
						fobj.groupId = _T("");
					}
					pCopyBuffer->Read(&sw, static_cast<System::LongInt>(sizeof(__int32)));
					pCopyBuffer->Read(&sh, static_cast<System::LongInt>(sizeof(__int32)));
					pCopyBuffer->Read(&ridx, static_cast<System::LongInt>(sizeof(__int32)));
					pCopyBuffer->Read(&fobj.x, static_cast<System::LongInt>(sizeof(short)));
					pCopyBuffer->Read(&fobj.y, static_cast<System::LongInt>(sizeof(short)));
					pCopyBuffer->Read(&fobj.w, static_cast<System::LongInt>(sizeof(short)));
					pCopyBuffer->Read(&fobj.h, static_cast<System::LongInt>(sizeof(short)));
					fobj.setSourceRes(sw, sh, ridx, sname);

					if(mpTempResObjManager->GetBitmapFrame(afd.frameId))
					{
						mBFList.push_back(afd);
						std::list<TAnimationFrameData>::iterator p = mBFList.end();
						p--;
						DragObjListAdd(&(*p));
					}
					else
					{
						__int32 w, h, g_idx;
						UnicodeString fname;
						if(!newFrames)
						{
							ClearResList(mpResList);
						}
						TTempBGobj *o = new TTempBGobj();
						o->oldHash = 0;
						o->oldGroup = _T("");
						o->bi = NULL;
						fobj.copyTo(o->o);
						mpResList->Add(o);
						o->o.getSourceRes(&w, &h, NULL, &fname);
						if ((g_idx = mpTempResObjManager->GetGraphicResourceIndex(fname)) < 0)
						{
							Graphics::TBitmap *bmp = new Graphics::TBitmap();
							UnicodeString path = Form_m->PathPngRes + fname;
							read_png(path.c_str(), bmp);
							BMPImage *i2d = new BMPImage(bmp);
							g_idx = mpTempResObjManager->AddGraphicResource(i2d, fname);
							o->o.setSourceRes(bmp->Width, bmp->Height, g_idx, fname);
							delete i2d;
						}
						else
						{
							o->o.setSourceRes(w, h, g_idx, fname);
						}
						newFrames = true;

						mBFList.push_back(afd);
						std::list<TAnimationFrameData>::iterator p = mBFList.end();
						p--;
						DragObjListAdd(&(*p));
					}
				}
				else
				{
					bool validate = true;
					if(mpAnimation->HasStreamObject())
					{
						validate = (afd.rw == mpAnimation->GetStreamObjectWidth() &&
									afd.rh == mpAnimation->GetStreamObjectHeight());
					}
					if(validate)
					{
						std::list<TAnimationFrameData>::iterator p;
						for(p = mBFList.begin(); p != mBFList.end(); ++p)
						{
							if(p->isStreamFrame())
							{
								mBFList.erase(p);
								break;
							}
						}
						mBFList.push_back(afd);
						p = mBFList.end();
						p--;
						DragObjListAdd(&(*p));
					}
					else
					{
						error_stream_obj = true;
                    }
                }
			}
		}
		pCopyBuffer->Read(ch, 1);
	}
	delete pCopyBuffer;

	if(newFrames)
	{
		mpTempResObjManager->ApplyFrameDatabaseChangesFromList(mpResList);
		ClearResList(mpResList);
		mpTempResObjManager->CopyBitmapFrameDatabase(mpResList);
		CreateListView();
    }

	SetLogicValuesToGrid(NULL);

	LabelFrafments->Caption = IntToStr(static_cast<int>(mBFList.size()));

	if(error_stream_obj)
	{
		UnicodeString mes = Localization::Text(_T("mWrongSizeStreamObj")) + IntToStr(mpAnimation->GetStreamObjectWidth()) + _T("x") + IntToStr(mpAnimation->GetStreamObjectHeight());
		Application->MessageBox(mes.c_str(), _T("Stream object\0"), MB_OK | MB_ICONERROR);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CustomPaintBox1WndMethod(Winapi::Messages::TMessage &Message)
{
	switch(Message.Msg)
	{
		case WM_ERASEBKGND:
			Message.Result = 1;
		break;
		case WM_SIZE:
		{
			if (gUsingOpenGL)
			{
				TRect v;
				GetRenderRect(v);
				v.Left = 0;
				v.Top = 0;
				TRenderGL::Resize(v, PaintBoxGL->ClientRect);
			}
			Message.Result = 0;
		}
		break;
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			if(TRenderGL::IsEnabled())
			{
				HDC hdc = BeginPaint(PaintBoxGL->Handle, &ps);
				DrawFrame();
				if (gUsingOpenGL)
				{
					TRenderGL::DrawScene(hdc);
				}
				EndPaint(PaintBoxGL->Handle, &ps);
			}
			else
			{
				BeginPaint(PaintBoxGL->Handle, &ps);
				EndPaint(PaintBoxGL->Handle, &ps);
			}
			mRenderDelay = RENDER_DELAY_MS;
			Message.Result = 0;
		}
		break;
		default:
			mOldPaintBox1WndProc(Message);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FoldersListResize(TObject *Sender)
{
	TListView *lw = static_cast<TListView*>(Sender);
	lw->Columns->Items[0]->Width = lw->ClientWidth;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CreateStreamFolderAndFileList(UnicodeString activeFolder, UnicodeString activeFile)
{
	FoldersList->Enabled = false;
	FoldersList->Items->Clear();

	if(!Form_m->PathPngRes.IsEmpty() && DirectoryExists(Form_m->PathPngRes))
	{
		WIN32_FIND_DATAW ffd;
		ZeroMemory(&ffd, sizeof(WIN32_FIND_DATAW));
		UnicodeString dir = Form_m->PathPngRes + _T("*.*");
		HANDLE hFind = FindFirstFileW(dir.c_str(), &ffd);
		do
		{
		  if(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY &&
			((ffd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN) &&
			ffd.cFileName[0] != L'.')
		  {
			TListItem* it = FoldersList->Items->Add();
			it->ImageIndex = 1;
			it->Caption = UnicodeString(ffd.cFileName);
		  }
		}
		while(FindNextFileW(hFind, &ffd) != NULL);
		__int32 ct = FoldersList->Items->Count;
		if(ct > 0)
		{
            TabSetGraphicsMode->TabIndex = 1;
			FoldersList->Enabled = true;
			if(!activeFolder.IsEmpty())
			{
				for(__int32 i = 0; i < ct; i++)
				{
					if(activeFolder.Compare(FoldersList->Items->Item[i]->Caption) == 0)
					{
						mLastStreamFolder = activeFolder;
						FoldersList->Selected = FoldersList->Items->Item[i];
						FoldersList->Selected->MakeVisible(false);
						break;
					}
				}
			}
			if(FoldersList->Selected == NULL)
			{
				FoldersList->Selected = FoldersList->Items->Item[0];
				activeFolder = FoldersList->Selected->Caption;
            }
		}
	}
	FoldersList->Scroll(0, 0);
	CreateStreamFileList(activeFolder, activeFile);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FoldersListClick(TObject *Sender)
{
	if(TabSetGraphicsMode->TabIndex != 1)
	{
		return;
	}
	TListView* lw = static_cast<TListView*>(Sender);
	TListItem* it = lw->Selected;
	if(it != NULL)
	{
		if(lw == FoldersList)
		{
			CreateStreamFileList(it->Caption, mLastStreamFile);
		}
		else if(lw == FilesList)
		{
			mLastStreamFile = it->Caption;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CreateStreamFileList(UnicodeString activeFolder, UnicodeString activeFile)
{
	FilesList->Enabled = false;
	Button_AutoCreateStrmAnim->Enabled = false;
	FilesList->Items->Clear();

	if(TabSetGraphicsMode->TabIndex != 1 || !FoldersList->Enabled)
	{
		return;
	}

	UnicodeString ext = _T(".xxx");
	ext = forceCorrectResFileExtention(ext);
	UnicodeString path = Form_m->PathPngRes + activeFolder;
	if(!path.IsEmpty() && DirectoryExists(path))
	{
		WIN32_FIND_DATAW ffd;
		ZeroMemory(&ffd, sizeof(WIN32_FIND_DATAW));
		path = path + _T("\\*.*");
		HANDLE hFind = FindFirstFileW(path.c_str(), &ffd);
		do
		{
		  if(((ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY) &&
			((ffd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN))
		  {
			UnicodeString file = UnicodeString(ffd.cFileName);
			if(ext.Compare(ExtractFileExt(file)) == 0)
			{
				TListItem* it = FilesList->Items->Add();
				it->ImageIndex = 23;
				it->Caption = file;
			}
		  }
		}
		while(FindNextFileW(hFind, &ffd) != NULL);
		__int32 ct = FilesList->Items->Count;
		if(ct > 0)
		{
			FilesList->Enabled = true;
			Button_AutoCreateStrmAnim->Enabled = true;
			if(!activeFile.IsEmpty())
			{
				for(__int32 i = 0; i < ct; i++)
				{
					if(activeFile.Compare(FilesList->Items->Item[i]->Caption) == 0)
					{
						mLastStreamFile = activeFile;
						FilesList->Selected = FilesList->Items->Item[i];
						FilesList->Selected->MakeVisible(false);
						break;
					}
				}
			}
			if(FilesList->Selected == NULL)
			{
				FilesList->Selected = FilesList->Items->Item[0];
			}
		}
	}
	FilesList->Scroll(0, 0);
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::Button_AutoCreateStrmAnimClick(TObject *)
{
	if(mpAnimation && FilesList->Enabled && FilesList->Items->Count > 0)
	if(ID_OK == Application->MessageBox(Localization::Text(_T("mStreamAnimationAutocreate")).c_str(),
				Button_AutoCreateStrmAnim->Caption.c_str(), MB_OKCANCEL | MB_ICONQUESTION))
	{

		Application->CreateForm(__classid(TFStrmAutoDlg), &FStrmAutoDlg);
		FStrmAutoDlg->Caption = Button_AutoCreateStrmAnim->Caption;
		if(FStrmAutoDlg->ShowModal() == mrOk)
		{
			__int32 ct;
			__int32 frameDuration = -1;
			bool selectedItems = FStrmAutoDlg->RadioGroupFrames->ItemIndex == 1;
			bool backwardDirection = FStrmAutoDlg->RadioGroupDirection->ItemIndex == 1;
			if(FStrmAutoDlg->Edit_time->Enabled)
			{
				try
				{
					frameDuration = StrToInt(FStrmAutoDlg->Edit_time->Text);
				}
				catch(...)
				{
					frameDuration = -1;
                }
			}
			FStrmAutoDlg->Free();
			if(selectedItems)
			{
				ct = FilesList->SelCount;
			}
			else
			{
				ct = FilesList->Items->Count;
			}
			if(ct > 0)
			{
                const TAnimationFrame *af;
				std::list<TAnimationFrameData>::iterator p;
				__int32 ew, eh, fr_idx, a, res, step;
				__int32 err = 0;
				bool total_res = false;
				mDisableGridControls = true;
				FoldersList->Enabled = false;
				FilesList->Enabled = false;
				ClearCopySelection();
				DragObjListClear();
				SaveFrameData();
				mAnimationFrameIdx = -1;
				ew = 0;
				eh = 0;
				if(mpAnimation->GetFrameCount() < ct)
				{
					mpAnimation->Resize(ct);
				}
				fr_idx = 0;
				if(backwardDirection)
				{
					a = FilesList->Items->Count - 1;
					res = -1;
					step = -1;
				}
				else
				{
					a = 0;
					res = FilesList->Items->Count;
					step = 1;
				}
				for(; a != res; a += step)
				{
					bool res = false;
					if(selectedItems && !FilesList->Items->Item[a]->Selected)
					{
						continue;
					}
					TAnimationFrameData *pafd = NULL;
					af = mpAnimation->GetFrame(fr_idx);
					mBFList.clear();
					if(af != NULL)
					{
						af->CopyFrameDataTo(mBFList);
					}
					for(p = mBFList.begin(); p != mBFList.end(); ++p)
					{
						if(p->isStreamFrame())
						{
							pafd = &(*p);
							break;
						}
					}
					UnicodeString path = Form_m->PathPngRes +
								FoldersList->Selected->Caption + _T("\\") + FilesList->Items->Item[a]->Caption;
					Graphics::TBitmap *bmp = new Graphics::TBitmap();
					if(FileExists(path))
					{
						if(read_png(path.c_str(), bmp))
						{

							TAnimationFrameData afd;
							afd.posX = 0;
							afd.posY = 0;
							afd.rw = bmp->Width;
							afd.rh = bmp->Height;
							if(ew == 0)
							{
								ew = afd.rw;
								eh = afd.rh;
							}
							if(afd.rw == ew && afd.rh == eh)
							{
								afd.SetFrameId(0);
								afd.type = LogicObjType::UOP_LOGIC_OBJ_BMP;
								afd.setStreamFrame(true);
								afd.strm_filename = FoldersList->Selected->Caption + _T("\\") + FilesList->Items->Item[a]->Caption;
								if(pafd != NULL)
								{
									pafd->rw = afd.rw;
									pafd->rh = afd.rh;
									pafd->strm_filename = afd.strm_filename;
								}
								else
								{
									mBFList.push_back(afd);
								}
								TAnimationFrame saf;
								if(af != NULL)
								{
									af->CopyTo(&saf);
								}
								saf.CreateFromList(mBFList);
								if(frameDuration >= 0)
								{
									saf.SetDuration(frameDuration);
								}
								mpAnimation->SetFrame(saf, fr_idx);
								res = true;
								total_res = true;
							}
						}
					}
					delete bmp;
					if(!res)
					{
						if(pafd != NULL)
						{
							mBFList.erase(p);
							//pafd = NULL;
						}
						err++;
					}
					fr_idx++;
				}
				mpAnimation->Resize(mpAnimation->GetFrameCount()); // hack, change new strm w and h
				if(total_res)
				{
					ct = mpAnimation->GetFrameCount();
					for(a = 0; a < ct; a++)
					{
						const TAnimationFrame *af = mpAnimation->GetFrame(a);
						mBFList.clear();
						if(af != NULL)
						{
							af->CopyFrameDataTo(mBFList);
							for(p = mBFList.begin(); p != mBFList.end(); ++p)
							{
								if(p->isStreamFrame())
								{
									if(p->rw != mpAnimation->GetStreamObjectWidth() ||
									   p->rh != mpAnimation->GetStreamObjectHeight())
									{
                                        mBFList.erase(p);
										break;
									}
								}
							}
							TAnimationFrame saf;
							af->CopyTo(&saf);
							saf.CreateFromList(mBFList);
							mpAnimation->SetFrame(saf, a);
						}
					}
				}
				FoldersList->Enabled = true;
				FilesList->Enabled = true;
				mDisableGridControls = false;
				CreateAnimationFrames(0);
				if(err != 0)
				{
					Application->MessageBox(Localization::Text(_T("mStreamAnimationAutocreateWarningResult")).c_str(),
									Button_AutoCreateStrmAnim->Caption.c_str(), MB_OK | MB_ICONWARNING);

				}
			}
		}
		else
		{
			FStrmAutoDlg->Free();
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::FormResize(TObject *Sender)
{
	(void)Sender;
	TRect v;
	GetRenderRect(v);
	v.Top = TabSet->Height + ToolBar->Height;
	v.Left = 0;
	PaintBoxGL->Left = v.Left;
	PaintBoxGL->Top = v.Top;
	PaintBoxGL->Width = v.Width();
	PaintBoxGL->Height = v.Height();
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CheckBox_resClick(TObject *Sender)
{
	(void)Sender;
	if(CheckBox_res->Checked)
	{
		LabelGrOptHint->Caption = Localization::Text(_T("mAllowGraphicsOptimizationOnHint"));
	}
	else
	{
		LabelGrOptHint->Caption = Localization::Text(_T("mAllowGraphicsOptimizationOffHint"));
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::EditFilterChange(TObject *Sender)
{
	(void)Sender;
	__int32 pos;
	CB_AniType->Enabled = false;
	if(EditFilter->Text.IsEmpty())
	{
		CB_AniType->Items->Assign(mpTempResObjManager->aniNameTypes);
	}
	else
	{
		mpTempFilterList->Clear();
		for(__int32 i = 0; i < mpTempResObjManager->aniNameTypes->Count; i++)
		{
			pos = mpTempResObjManager->aniNameTypes->Strings[i].Pos(EditFilter->Text.UpperCase());
			if(pos > 0)
			{
				mpTempFilterList->Add(mpTempResObjManager->aniNameTypes->Strings[i]);
			}
		}
		CB_AniType->Items->Assign(mpTempFilterList);
	}
	if (CB_AniType->Items->Count == 0)
	{
		PanelLeft->Enabled = false;
		CB_AniType->Enabled = false;
		FrameGrid->Enabled = false;
		BtFrPrev->Enabled = false;
		BtBack->Enabled = false;
		BtStop->Enabled = false;
		BtPlay->Enabled = false;
		BtFrNext->Enabled = false;
		CBAutorepeat->Enabled = false;
		CBViewMode->Enabled = false;
		ComboBoxScale->Enabled = false;
		CurrentFrameInfoEnabled(false);
        CB_AniType->Text = _T("");
		CB_AniType->ItemIndex = -1;
		CreateAnimationFrames(0);
	}
	else
	{
		CB_AniType->Enabled = true;
		PanelLeft->Enabled = true;
		CB_AniType->Enabled = true;
		FrameGrid->Enabled = true;
		BtFrPrev->Enabled = true;
		BtBack->Enabled = true;
		BtStop->Enabled = true;
		BtPlay->Enabled = true;
		BtFrNext->Enabled = true;
		CBAutorepeat->Enabled = true;
		CBViewMode->Enabled = true;
		ComboBoxScale->Enabled = true;
		CurrentFrameInfoEnabled(true);
		pos = CB_AniType->Items->IndexOf(CB_AniType->Text);
		if(pos < 0)
		{
			CB_AniType->ItemIndex = 0;
			CreateAnimationFrames(0);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::CurrentFrameInfoEnabled(bool val)
{
	Edit_duration->Enabled = val;
	ComboBox_Event->Enabled = val;
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActImportFromXMLExecute(TObject *)
{
	//
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActionToCenterExecute(TObject *)
{
	if(mpDragObjList->Count == 1)
	{
		TPoint p;
		TRect r;
		GetRenderRect(r);
		TAnimationFrameData *obj = GetDragObjFromList(0);
		obj->posX = -obj->rw / 2;
		obj->posY = -obj->rh / 2;
		if(TabSetAniMode->TabIndex == 0)
		{
			SetObjValuesToGrid();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ActionToDefaultSizesExecute(TObject *)
{
	if(mpDragObjList->Count == 1)
	{
		TPoint p;
		TRect r;
		GetRenderRect(r);
		TAnimationFrameData *obj = GetDragObjFromList(0);
		const TFrameObj *fobj = mpTempResObjManager->GetBitmapFrame(obj->frameId);
		if(fobj != NULL)
		{
			obj->rw = fobj->w;
			obj->rh = fobj->h;
			if(TabSetAniMode->TabIndex == 0)
			{
				SetObjValuesToGrid();
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::ComboBoxScaleChange(TObject *)
{
	switch(ComboBoxScale->ItemIndex)
	{
		case 0: //0.125
			mScale = 0.125;
			break;
		case 1: //0.25
			mScale = 0.25;
			break;
		case 2: //0.5
			mScale = 0.5;
			break;
		case 3: //1.0
			mScale = 1.0;
			break;
		case 4: //2.0
			mScale = 2.0;
			break;
		case 5: //3.0
			mScale = 3.0;
			break;
		case 6: //4.0
			mScale = 4.0;
			break;
		case 7: //5.0
			mScale = 5.0;
			break;
		case 8: //6.0
			mScale = 6.0f;
			break;
		case 9: //7.0
			mScale = 7.0f;
			break;
		case 10: //8.0
			mScale = 8.0f;
			break;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFObjParent::InitJoinNodes()
{
	if(mJoinNodeInited)
	{
    	return;
	}

	int i, n = 1;
	CategoryPanelGroup->Enabled = false;
	for(i = LogicObjType::UOP_LOGIC_OBJ_NODE2; i <= LogicObjType::UOP_LOGIC_OBJ_NODE16 && n < mJoinNodesCount; i++)
	{
		TCategoryPanel* p = new TCategoryPanel(CategoryPanelGroup);
		TCheckBox* cb = new TCheckBox(p);
		TValueListEditor* vle = new TValueListEditor(p);
		p->Name = CP3->Name + IntToStr(i);
		p->Caption = "";
		p->ShowHint = CP3->ShowHint;
		p->ParentShowHint = CP3->ParentShowHint;
		p->Hint = CP3->Hint;
		p->TabOrder = (unsigned short)(CP3->TabOrder + i);
		p->Height = CP3->Height;
		cb->Align = RBNode1->Align;
		cb->Caption = RBNode1->Caption;
		cb->Name = "RBNode" + IntToStr(i);
		cb->OnClick = RBNode1->OnClick;
		cb->TabOrder = RBNode1->TabOrder;
		cb->Height = RBNode1->Height;
		vle->TabOrder = VLENode1->TabOrder;
		vle->Align = VLENode1->Align;
		vle->GridLineWidth = VLENode1->GridLineWidth;
		vle->FixedCols = VLENode1->FixedCols;
		vle->FixedColor = VLENode1->FixedColor;
		vle->Height = VLENode1->Height;
		vle->DefaultRowHeight = VLENode1->DefaultRowHeight;
		vle->BorderStyle = VLENode1->BorderStyle;
		vle->DisplayOptions = VLENode1->DisplayOptions;
		vle->DrawingStyle = VLENode1->DrawingStyle;
		vle->Options = VLENode1->Options;
		vle->ScrollBars = VLENode1->ScrollBars;
		vle->OnDrawCell = VLENode1->OnDrawCell;
		vle->OnKeyPress = VLENode1->OnKeyPress;
		vle->OnValidate = VLENode1->OnValidate;
		vle->Strings->AddStrings(VLENode1->Strings);
		vle->ColWidths[0] = VLENode1->ColWidths[0];
		vle->Tag = reinterpret_cast<NativeInt>(&mLogicDrawObjects[i]);
		cb->Tag = reinterpret_cast<NativeInt>(vle);
		p->Tag = reinterpret_cast<NativeInt>(cb);
		mDynCheckboxes.push_back(cb);
		mDynPanels.push_back(p);
		mVLENodes.push_back(vle);
		cb->Parent = p;
		vle->Parent = p;
		p->PanelGroup = CategoryPanelGroup;
		mNRList.push_back(&mLogicDrawObjects[i]);
		n++;
	}
	CategoryPanelGroup->Enabled = true;

	i = 1;
	RBNode1->Caption = RBNode1->Caption + IntToStr(i);
	i++;
	std::list<TCheckBox*>::iterator icb;
	for(icb = mDynCheckboxes.begin(); icb != mDynCheckboxes.end(); ++icb)
	{
	   TCheckBox* cb = *icb;
	   cb->Caption = cb->Caption + " " +  IntToStr(i);
	   i++;
	}

	mJoinNodeInited = true;
}
//---------------------------------------------------------------------------








