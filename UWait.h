//---------------------------------------------------------------------------

#ifndef UWaitH
#define UWaitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFWait : public TForm
{
__published:
	TPanel *Panel1;
	TProgressBar *ProgressBar;
	TStaticText *StaticText;
	TTimer *Timer1;
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall Timer1Timer(TObject *Sender);

private:

public:
	UnicodeString mText;
	__fastcall TFWait(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFWait *FWait;
//---------------------------------------------------------------------------
#endif
