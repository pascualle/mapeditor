//---------------------------------------------------------------------------
#ifndef ULocalizationH
#define ULocalizationH
//---------------------------------------------------------------------------
#include <map>
#include "UTypes.h"
#include <StdCtrls.hpp>

class Localization
{
 public:
	static void __fastcall Init();
	static void __fastcall Release();
	static void __fastcall GetLangAliasList(TStringList* list);
	static const UnicodeString& __fastcall GetLangName(const UnicodeString& alias_name);
	static void __fastcall Load(const UnicodeString& name);
	static const UnicodeString& __fastcall Text(const UnicodeString& alias);
	static const UnicodeString& __fastcall TextWithVar(const UnicodeString& alias, const UnicodeString& var);
	static void __fastcall Localize(TForm *form);
	static void __fastcall Localize(TActionList *alist);
	static void __fastcall Localize(TPopupMenu *pmenu);

 private:

	static void __fastcall TryLocalizeMainMenu(TMainMenu *mm);
	static void __fastcall TryLocalizeControls(TWinControl *ctrl);
	static void __fastcall TryLocalizeControl(TControl *ctrl);
	static void __fastcall LocalizeMainMenuItem(TMenuItem *mi);

	static std::map<const UnicodeString, UnicodeString> mMap;
	static std::map<const UnicodeString, UnicodeString> mLangDict;
	static std::map<const UnicodeString, UnicodeString> mResDict;
	static UnicodeString mTempStr;
};
#endif
