//---------------------------------------------------------------------------

#ifndef UEditObjH
#define UEditObjH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <FileCtrl.hpp>
#include <Dialogs.hpp>
#include <Buttons.hpp>
#include <ActnList.hpp>
#include <Menus.hpp>
#include <ImgList.hpp>
#include <CategoryButtons.hpp>
#include "UTypes.h"
#include <Vcl.Samples.Spin.hpp>
#include <System.Actions.hpp>
#include <System.ImageList.hpp>
//---------------------------------------------------------------------------

struct TEGrWndParams;
struct TEDitObjSelectionParams;
struct TTempBGobj;
class TCommonResObjManager;
class GMapBGObj;
class TFrameObj;
class BMPImage;

class TFEditGrObj : public TForm, TRenderGLForm
{
  friend class TFObjParent;

 __published:
	TStatusBar *StatusBar;
	TPanel *Panel1;
	TGroupBox *GroupBox_p;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TButton *Bt_accept;
	TLabel *Label5;
	TBevel *Bevel1;
	TFileListBox *FileListBox;
	TGroupBox *GroupBox_cl;
	TColorBox *ColorBox;
	TBitBtn *BitBt_up;
	TBitBtn *BitBt_right;
	TBitBtn *BitBt_left;
	TBitBtn *BitBt_down;
	TActionList *ActionList1;
	TMainMenu *MainMenu1;
	TMenuItem *N1;
	TMenuItem *N2;
	TMenuItem *N3;
	TAction *ActSelectAll;
	TAction *ActDeselectAll;
	TAction *ActCopy;
	TAction *ActPaste;
	TMenuItem *N4;
	TMenuItem *N5;
	TMenuItem *N6;
	TMenuItem *N7;
	TMenuItem *N8;
	TAction *ActShiftRight;
	TAction *ActShiftLeft;
	TAction *ActShiftUp;
	TAction *ActShiftDown;
	TMenuItem *N9;
	TMenuItem *N10;
	TMenuItem *N11;
	TMenuItem *N12;
	TMenuItem *N13;
	TSplitter *SplitterLV;
	TImageList *ImageList;
	TCategoryButtons *CategoryButtons_s;
	TPanel *Panel_main;
	TPanel *Panel2;
	TScrollBox *ScrollBox1;
	TTrackBar *TrackBar1;
	TSplitter *Splitter1;
	TPanel *Panel_1;
	TTrackBar *TrackBar2;
	TCheckBox *CheckBox_border;
	TMenuItem *N14;
	TAction *ActAdd;
	TAction *ActDel;
	TAction *ActClear;
	TMenuItem *N15;
	TMenuItem *N16;
	TMenuItem *N17;
	TMenuItem *N18;
	TMenuItem *N19;
	TMenuItem *N20;
	TPopupMenu *PopupMenu;
	TMenuItem *NEdit;
	TMenuItem *NDel;
	TMenuItem *MenuItem2;
	TMenuItem *N21;
	TAction *ActGroupCreate;
	TAction *ActGroupDelete;
	TMenuItem *N22;
	TMenuItem *N23;
	TAction *ActBGObjShiftLeft;
	TAction *ActBGObjShiftRight;
	TMenuItem *ActBGObjShiftLeft1;
	TMenuItem *ActBGObjShiftRight1;
	TMenuItem *N24;
	TMenuItem *N25;
	TMenuItem *NEGOMovetoSeparator;
	TMenuItem *NEGOMoveto;
	TMenuItem *NEGOMMMovetoSeparator;
	TMenuItem *NEGOMMMoveto;
	TPanel *PaintBox1;
	TScrollBar *HorzScrollBar;
	TEdit *FakeControl;
	TScrollBar *VertScrollBar;
	TPanel *Image1;
	TAction *ActionFindSelection;
	TMenuItem *N26;
	TBitBtn *BitBt_findselection;

	void __fastcall CSEd_xChange(TObject *Sender);
	void __fastcall FileListBoxClick(TObject *Sender);
	void __fastcall Bt_upClick(TObject *Sender);
	void __fastcall Bt_downClick(TObject *Sender);
	void __fastcall Bt_leftClick(TObject *Sender);
	void __fastcall Bt_rightClick(TObject *Sender);
	void __fastcall CSEd_xKeyPress(TObject *Sender, System::WideChar &Key);
	void __fastcall Sh_tMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall Sh_tMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall ColorBoxChange(TObject *Sender);
	void __fastcall FormKeyPress(TObject *Sender, char &Key);
	void __fastcall N3Click(TObject *Sender);
	void __fastcall ActSelectAllExecute(TObject *Sender);
	void __fastcall ActDeselectAllExecute(TObject *Sender);
	void __fastcall ActCopyExecute(TObject *Sender);
	void __fastcall ActPasteExecute(TObject *Sender);
	void __fastcall ActShiftRightExecute(TObject *Sender);
	void __fastcall ActShiftLeftExecute(TObject *Sender);
	void __fastcall ActShiftUpExecute(TObject *Sender);
	void __fastcall ActShiftDownExecute(TObject *Sender);
	void __fastcall FakeControlEnter(TObject *Sender);
	void __fastcall FakeControlKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall FakeControlKeyPress(TObject *Sender, char &Key);
	void __fastcall ScrollBox_oldMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall CategoryButtons_sDrawIcon(TObject *Sender, const TButtonItem *Button, TCanvas *Canvas, TRect &Rect, TButtonDrawState State, int &TextOffset);
	void __fastcall CategoryButtons_sEnter(TObject *Sender);
	void __fastcall CategoryButtons_sExit(TObject *Sender);
	void __fastcall CategoryButtons_sSelectedItemChange(TObject *Sender, const TButtonItem *Button);
	void __fastcall ScrollBox1Resize(TObject *Sender);
	void __fastcall TrackBar1Change(TObject *Sender);
	void __fastcall Panel_mainCanResize(TObject *Sender, int &NewWidth, int &NewHeight, bool &Resize);
	void __fastcall ActAddExecute(TObject *Sender);
	void __fastcall ActDelExecute(TObject *Sender);
	void __fastcall ActClearExecute(TObject *Sender);
	void __fastcall CategoryButtons_sKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall ActBGObjShiftLeftExecute(TObject *Sender);
	void __fastcall ActBGObjShiftRightExecute(TObject *Sender);
	void __fastcall ActGroupCreateExecute(TObject *Sender);
	void __fastcall ActGroupDeleteExecute(TObject *Sender);
	void __fastcall PopupMenuPopup(TObject *Sender);
	void __fastcall CategoryButtons_sAfterDrawButton(TObject *Sender, const TButtonItem *Button, TCanvas *Canvas, TRect &Rect, TButtonDrawState State);
	void __fastcall CategoryButtons_sSelectedCategoryChange(TObject *Sender, const TButtonCategory *Category);
	void __fastcall MainMenu1Change(TObject *Sender, TMenuItem *Source, bool Rebuild);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall HorzScrollBarScroll(TObject *Sender, TScrollCode ScrollCode, int &ScrollPos);
	void __fastcall SplitterLVMoved(TObject *Sender);
	void __fastcall Splitter1Moved(TObject *Sender);
	void __fastcall PaintBox1MouseEnter(TObject *Sender);
	void __fastcall PaintBox1MouseLeave(TObject *Sender);
	void __fastcall PaintBox1MouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall ActionFindSelectionExecute(TObject *Sender);

 private:

	bool key_press;
	__int32 scale[2];
	UnicodeString currentFileName;
	TRect copyRect;
	TColor CurrentBGColor;
	__int32 mUpdateTimer;
	__int32 mSizeTrackBarCounter;
	__int32 mCreatePreviewCounter;
	GMapBGObj *mpGameObjGM;
	TTempBGobj *mpActiveItem;
	TList *temp_res; //TTempBGobj list
	BMPImage *prev_bmp;
	TTrackBar *mpSizeTrackBar;
	TCommonResObjManager *mpResObjManager;
	TSpinEdit *CSEd_y;
	TSpinEdit *CSEd_x;
	TSpinEdit *CSEd_w;
	TSpinEdit *CSEd_h;
	TList* mpImageListBG;
	__int32 mRenderDelay;
	TWndMethod mOldPaintBox1WndProc;
	TLMBtn mLMBtn;

	enum
	{
		sTop, sLeft, sRight, sBottom, sCenter
	};

	enum
	{
		tgTop, tgBottom
	};

	struct TSelectionObject
	{
		POINT mPos0;
		double mLeft;
		double mTop;
		double mRight;
		double mBottom;
		__int32 mFixedWidth;
		__int32 mFixedHeight;
		bool mSet;
		bool mLockHW;

		__int32 getWidth() const {return (__int32)mRight - (__int32)mLeft;}
		__int32 getHeight() const {return (__int32)mBottom - (__int32)mTop;}
	}
	mSelectRect;

	virtual void __fastcall InitRenderGL(/*Graphics::TBitmap *bmp*/);
	virtual void __fastcall ReleaseRenderGL();

	void __fastcall clearAll();
	void __fastcall resetRect();
	void __fastcall onSelRectResize();
	void __fastcall checkCrVisible();
	void __fastcall setScale(int iscale, int tagWnd);
	void __fastcall ClearTempRes();

	void __fastcall CreatePopupMoveTo();
	void __fastcall FreePopupMoveTo();
	void __fastcall NPopupMoveToClick(TObject *Sender);
	bool __fastcall CheckBounds_LTRB(double &l, double &t, double &r, double &b);
	void __fastcall FindSelection();

		//���������� ������� ���������
	void __fastcall setRectSel_LTRB(double left, double top, double right, double bottom);
	void __fastcall setRectSel_XYWH(double x, double y, __int32 w, __int32 h);

		//�������� ������� ���������� ����������� + ��� �����, ������ ��� � ����������
		//file_name - (out) ��� ����� c ������ ����� � ����
		//sel_rect - (out) ������� ���������
	bool __fastcall getSelection(UnicodeString *file_name, TRect *sel_rect);

		//����������� ��� ��� ������ (������ ��� ��������������)
		//file_name - (in) ��� ����� (����� ��� ����)
		//sel_rect - (in) ������� ���������
		//BGColor  - (in) ���� ������������ (���� ��� ������������)
	bool __fastcall setSelection(const UnicodeString *file_name, const TRect *sel_rect, TColor BGColor);

	void __fastcall CreateListView(TCategoryButtons *cd, TImageList *il, TList *res);
	void __fastcall EditItem(TTempBGobj *item, BMPImage *bmp, int mode, TCategoryButtons *cd, TImageList *il);
	void __fastcall OnActivateItem(const TButtonItem *bi);
	void __fastcall SetActiveBGIndex(int idx);

	void __fastcall CreatePreview();
	void __fastcall DoCreatePreview();

	bool __fastcall SafePasteBGRect(double x, double y);

	BMPImage *__fastcall GetBitmapForIcon(const TFrameObj *o);

	void __fastcall InitImageListBG(int ct);
	void __fastcall ClearImageListBG();
	void __fastcall ClearImageListBGItem(int idx);
	void __fastcall AssignToImageListBG(int idx, const BMPImage *img, bool deleteOnRelease);
	const BMPImage* __fastcall GetFromImageListBG(int idx);

	void __fastcall DrawFrame();
	void __fastcall DrawSelection();
	void __fastcall ChangeSizes();
	void __fastcall CheckCursorView();

	void __fastcall ProcessTimer(unsigned int ms);

	void __fastcall CustomPaintBox1WndMethod(Winapi::Messages::TMessage &Message);

 public:

	void __fastcall Init(TCommonResObjManager *commonResObjManager);

		//�������� ���������� �����������
		//bmp          -- (out) ���������� �����������
		//TEDitObjSelectionParams -- ���� �� NULL, ����������� � ��������� ��������� � ������,
		//�����������-��������, ��� ������� �������� �����������
		//���� NULL, ������������ ��� ��������� �����������
	bool __fastcall getSelBitmap(BMPImage *out_bmp, const TEDitObjSelectionParams *params);

	void __fastcall SetObjList(hash_t activeObjId, const TList *iobjList);
	void __fastcall GetObjListChanges(TList *oobjList);

	void __fastcall SetBGObj(GMapBGObj *obj, int activeIdx, int w, int h);
	void __fastcall ApplyChangesToBGObj();

	void __fastcall SetImagePreviewMode();

	void __fastcall GetWndParams(TEGrWndParams *params);
	void __fastcall SetWndParams(const TEGrWndParams *params);

	virtual void __fastcall DoTick(unsigned int ms);

	void __fastcall getSourceImageSize(int *si_w, int *si_h);

	__fastcall TFEditGrObj(TComponent *Owner);
	__fastcall ~TFEditGrObj();
};

//---------------------------------------------------------------------------
extern PACKAGE TFEditGrObj *FEditGrObj;
//---------------------------------------------------------------------------
#endif
