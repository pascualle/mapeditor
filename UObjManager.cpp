#pragma hdrstop

#include "math.h"
#include "MainUnit.h"
#include "UObjManager.h"
#include "UObjCode.h"
#include "UScript.h"
#include "URenderGL.h"
#include "USaveVarList.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

static const __int32 MAX_MAP_SIZE = 65535;
static const __int32 ERR_NONE = -1;
static const __int32 ERR_PARENT = -10;
static const __int32 ERR_STATE = -11;
static const __int32 ERR_STATE_PARENT = -12;

static const _TCHAR ERR_ANIM_NAME[] = _T("?ANI_ERR?");
//---------------------------------------------------------------------------

__fastcall TCommonTextManager::TCommonTextManager()
{
	mpTextNames = new TStringList();
	mpLangNames = new TStringList();
	mpFonts = new TList();
}
//---------------------------------------------------------------------------

__fastcall TCommonTextManager::~TCommonTextManager()
{
	ClearAll();
	delete mpTextNames;
	delete mpLangNames;
	ClearFonts();
	delete mpFonts;
}
//---------------------------------------------------------------------------

void __fastcall TCommonTextManager::SetLanguages(const TStringList *sl)
{
	assert(sl);
	mpLangNames->Clear();
	mpLangNames->Assign(const_cast<TStringList*>(sl));
}
//---------------------------------------------------------------------------

void __fastcall TCommonTextManager::GetLanguages(TStringList *list)
{
	assert(list);
	list->Clear();
	list->Assign(const_cast<TStringList*>(mpLangNames));
}
//---------------------------------------------------------------------------

__int32 __fastcall TCommonTextManager::AddText(const TUnicodeText &iText, UnicodeString iName, UnicodeString iLanguage)
{
	__int32 pos, lang;

	if (mpLangNames->Count >= MAX_TXT_STRINGS)
	{
		return -MAX_TXT_STRINGS;
	}

	pos = mpTextNames->IndexOf(iName);
	if (pos < 0)
	{
		lang = mpLangNames->IndexOf(iLanguage);
		if (lang >= 0)
		{
			mpTextNames->Add(iName);
			for (__int32 i = 0; i < MAX_LANGUAGES; i++)
			{
				if (i != lang)
				{
					TUnicodeText ut;
					mTexts[i].push_back(ut);
				}
			}
			mTexts[lang].push_back(iText);
			UpdateText(iText, iName, iLanguage);
			pos--;
		}
		else
		{
			pos = -1;
		}
	}
	else
	{
		UpdateText(iText, iName, iLanguage);
	}
	return pos;
}
//---------------------------------------------------------------------------

bool __fastcall TCommonTextManager::UpdateText(const TUnicodeText &iText, UnicodeString iName, UnicodeString iLanguage)
{
	__int32 pos, lang;
	std::list<TUnicodeText>::iterator txt;
	pos = mpTextNames->IndexOf(iName);
	assert(&iText);
	if (pos >= 0)
	{
		lang = mpLangNames->IndexOf(iLanguage);
		if (lang >= 0)
		{
			txt = mTexts[lang].begin();
			std::advance(txt, pos);
			if(&(*txt) != &iText)
			{
				iText.CopyTo(&(*txt));
			}
		}
		for (__int32 i = 0; i < MAX_LANGUAGES; i++)
		{
			txt = mTexts[lang].begin();
			std::advance(txt, pos);
			if (txt != mTexts[lang].end())
			{
				txt->SetAttributes(iText.GetAttributes());
				txt->SetFontName(iText.GetFontName());
				txt->SetCanvasSize(iText.GetCanvasWidth(), iText.GetCanvasHeight());
			}
		}
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

const TUnicodeText *__fastcall TCommonTextManager::GetText(UnicodeString iName, UnicodeString iLanguage) const
{
	__int32 pos, lang;
	pos = mpTextNames->IndexOf(iName);
	if (pos >= 0)
	{
		lang = mpLangNames->IndexOf(iLanguage);
		if (lang >= 0)
		{
			std::list<TUnicodeText>::const_iterator txt;
			txt = mTexts[lang].begin();
			std::advance(txt, pos);
			return &(*txt);
		}
	}
	return NULL;
}
//---------------------------------------------------------------------------

__int32 __fastcall TCommonTextManager::GetTextID(UnicodeString iName) const
{
	return mpTextNames->IndexOf(iName);
}
//---------------------------------------------------------------------------

void __fastcall TCommonTextManager::DeleteText(UnicodeString iName)
{
	__int32 pos;
	std::list<TUnicodeText>::iterator txt;
	pos = mpTextNames->IndexOf(iName);
	if (pos >= 0)
	{
		for (__int32 i = 0; i < MAX_LANGUAGES; i++)
		{
			txt = mTexts[i].begin();
			std::advance(txt, pos);
			if (txt != mTexts[i].end())
			{
				mTexts[i].erase(txt);
			}
		}
		mpTextNames->Delete(pos);
	}
}
//---------------------------------------------------------------------------

bool __fastcall TCommonTextManager::ChangeName(UnicodeString iName, UnicodeString iNewName)
{
	__int32 pos;
	pos = mpTextNames->IndexOf(iName);
	if (pos >= 0)
	{
		mpTextNames->Strings[pos] = iNewName;
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

void __fastcall TCommonTextManager::GetNames(TStringList *list)
{
	assert(list);
	list->Clear();
	list->Assign(const_cast<TStringList*>(mpTextNames));
}
//---------------------------------------------------------------------------

__int32 __fastcall TCommonTextManager::GetCount()
{
	return mpTextNames->Count;
}
//---------------------------------------------------------------------------

void __fastcall TCommonTextManager::ClearAll()
{
	for (__int32 i = 0; i < MAX_LANGUAGES; i++)
	{
		mTexts[i].clear();
	}
	mpTextNames->Clear();
	ClearFonts();
}
//---------------------------------------------------------------------------

__int32 __fastcall TCommonTextManager::AddFont(const TImageFont *iFont)
{
	__int32 pos = -1;
	if (GetFont(iFont->GetName()) == NULL)
	{
		mpFonts->Add((void*)iFont);
		pos = mpFonts->Count - 1;
	}
	return pos;
}
//---------------------------------------------------------------------------

const TImageFont *__fastcall TCommonTextManager::GetFont(UnicodeString iName) const
{
	for (__int32 i = 0; i < mpFonts->Count; i++)
	{
		if (mpFonts->Items[i] != NULL)
		{
			TImageFont *font = (TImageFont*)mpFonts->Items[i];
			if (font->GetName().Compare(iName) == 0)
			{
				return font;
			}
		}
	}
	return NULL;
}
//---------------------------------------------------------------------------

void __fastcall TCommonTextManager::GetFontNames(TStringList *list)
{
	list->Clear();
	for (__int32 i = 0; i < mpFonts->Count; i++)
	{
		if (mpFonts->Items[i] != NULL)
		{
			TImageFont *font = (TImageFont*)mpFonts->Items[i];
			list->Add(font->GetName());
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TCommonTextManager::ClearFonts()
{
	for (__int32 i = 0; i < mpFonts->Count; i++)
	{
		if (mpFonts->Items[i] != NULL)
		{
			delete(TImageFont*)mpFonts->Items[i];
		}
	}
	mpFonts->Clear();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

__fastcall TCommonResObjManager::TCommonResObjManager()
{
	aniNameTypes = new TStringList();
	eventNameTypes = new TStringList();
	MainGameObjPattern = new TObjPattern();

	GParentObj = new TList();
	states = new TList();
	bgotypes = new TList();
	stateOppositeTable = new TStringList();

	mpBmpRes = new TList();
	mpBmpNames = new TStringList();

	mpBitmapFramesOrder = new TList();
}
//---------------------------------------------------------------------------

__fastcall TCommonResObjManager::~TCommonResObjManager()
{
	FreeAll();

	delete stateOppositeTable;
	delete bgotypes;
	delete states;
	delete GParentObj;

	delete MainGameObjPattern;
	delete aniNameTypes;
	delete eventNameTypes;

	delete mpBmpRes;
	delete mpBmpNames;

	delete mpBitmapFramesOrder;
}
//---------------------------------------------------------------------------

void __fastcall TCommonResObjManager::FreeAll()
{
	__int32 i;
	for (i = 0; i < GParentObj->Count; i++)
		delete(GPerson*)GParentObj->Items[i];
	GParentObj->Clear();

	for (i = 0; i < states->Count; i++)
		delete(TState*)states->Items[i];
	states->Clear();

	for (i = 0; i < bgotypes->Count; i++)
		delete(TBGobjProperties*)bgotypes->Items[i];
	bgotypes->Clear();

	aniNameTypes->Clear();
	eventNameTypes->Clear();
	stateOppositeTable->Clear();

	ClearGraphicResources();

	bitmapFrames.clear();
	mpBitmapFramesOrder->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TCommonResObjManager::ClearGraphicResources()
{
	for (__int32 i = 0; i < mpBmpRes->Count; i++)
	{
		if (mpBmpRes->Items[i] != NULL)
		{
			if (gUsingOpenGL && TRenderGL::IsInited())
			{
				TRenderGL::DeleteTexture(i);
			}
			delete (BMPImage*)mpBmpRes->Items[i];
		}
	}
	mpBmpRes->Clear();
	mpBmpNames->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TCommonResObjManager::CopyTo(TCommonResObjManager *copy)
{
	__int32 i;

	assert(copy);
	copy->FreeAll();

	MainGameObjPattern->CopyTo(copy->MainGameObjPattern);

	for (i = 0; i < GParentObj->Count; i++)
	{
		GPerson* ngp = new GPerson();
		static_cast<GPerson*>(GParentObj->Items[i])->CopyTo(ngp);
		copy->GParentObj->Add(ngp);
	}

	for (i = 0; i < states->Count; i++)
	{
		TState *nst = new TState();
		static_cast<TState*>(states->Items[i])->CopyTo(nst);
		copy->states->Add(nst);
	}

	for (i = 0; i < bgotypes->Count; i++)
	{
		TBGobjProperties *nbt = new TBGobjProperties();
		static_cast<TBGobjProperties*>(bgotypes->Items[i])->CopyTo(*nbt);
		copy->bgotypes->Add(nbt);
	}

	copy->BackObj = BackObj;

	copy->aniNameTypes->Assign(aniNameTypes);
	copy->eventNameTypes->Assign(eventNameTypes);
	copy->stateOppositeTable->Assign(stateOppositeTable);

	for (i = 0; i < mpBmpRes->Count; i++)
	{
		if (mpBmpRes->Items[i] != NULL)
		{
			Graphics::TBitmap *bmp = new Graphics::TBitmap();
			bmp->Assign(const_cast<Graphics::TBitmap*>(static_cast<BMPImage*>(mpBmpRes->Items[i])->GetBitmapData()));
			copy->mpBmpRes->Add(new BMPImage(bmp));
		}
		else
		{
			copy->mpBmpRes->Add(NULL);
        }
	}

	copy->mpBmpNames->Assign(mpBmpNames);

	for (std::hash_map<hash_t, TFrameObj>::iterator it = bitmapFrames.begin();
			it != bitmapFrames.end();
			++it)
	{
		copy->bitmapFrames[it->first] = it->second;
	}

	copy->mpBitmapFramesOrder->Assign(mpBitmapFramesOrder);
}
//---------------------------------------------------------------------------

bool __fastcall TCommonResObjManager::IsMainGameObjPatternEmpty()
{
	return MainGameObjPattern->IsVarsEmpty();
}
//---------------------------------------------------------------------------

void __fastcall TCommonResObjManager::AssignObjMainPattern(GPerson *gp)
{
	gp->AssignPattern(MainGameObjPattern);
}
//---------------------------------------------------------------------------

const std::list<PropertyItem>* __fastcall TCommonResObjManager::GetVariablesMainGameObjPattern() const
{
	return MainGameObjPattern->GetVars();
}
//---------------------------------------------------------------------------

void __fastcall TCommonResObjManager::AssignMainPattern(TObjPattern *p)
{
	__int32 i;
	if (p != MainGameObjPattern)
	{
		p->CopyTo(MainGameObjPattern);
	}
	for (i = 0; i < GParentObj->Count; i++)
	{
		((GPerson*)GParentObj->Items[i])->AssignPattern(MainGameObjPattern);
	}
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TCommonResObjManager::GetFileNameMainGameObjPattern() const
{
	return MainGameObjPattern->getFilename();
}
//---------------------------------------------------------------------------

__int32 __fastcall TCommonResObjManager::GetAnimationID(UnicodeString name) const
{
	__int32 len = aniNameTypes->Count;
	for (__int32 i = 0; i < len; i++)
		if (aniNameTypes->Strings[i].Compare(name) == 0)
			return i;
	return -1;
}
//---------------------------------------------------------------------------

__int32 __fastcall TCommonResObjManager::GetEventID(UnicodeString name) const
{
	__int32 len = eventNameTypes->Count;
	for (__int32 i = 0; i < len; i++)
		if (eventNameTypes->Strings[i].Compare(name) == 0)
			return i;
	return -1;
}
//---------------------------------------------------------------------------

__int32 __fastcall TCommonResObjManager::GetParentObjID(UnicodeString name) const
{
	__int32 len = GParentObj->Count;
	for (__int32 i = 0; i < len; i++)
	{
		GPerson *s = (GPerson*)GParentObj->Items[i];
		if (s->GetName().Compare(name) == 0)
			return i;
	}
	return -1;
}
//---------------------------------------------------------------------------

GPerson *__fastcall TCommonResObjManager::GetParentObjByName(const UnicodeString& name) const
{
	__int32 len = GParentObj->Count;
	for (__int32 i = 0; i < len; i++)
	{
		GPerson *s = (GPerson*)GParentObj->Items[i];
		if (s->GetName().Compare(name) == 0)
			return s;
	}
	return NULL;
}
//---------------------------------------------------------------------------

__int32 __fastcall TCommonResObjManager::GetStateID(UnicodeString name) const
{
	if (name.Compare(UnicodeString(SpecialObjConstants::VarStateName)) == 0)
	{
		return SpecialObjConstants::trOBJ_CHANGE_PRP_OPPOSITESTATE;
	}

	__int32 len = states->Count;
	for (__int32 i = 0; i < len; i++)
	{
		TState *s = (TState*)states->Items[i];
		if (s->name.Compare(name) == 0)
			return i;
	}
	return -1;
}
//---------------------------------------------------------------------------

__int32 __fastcall TCommonResObjManager::AddGraphicResource(const BMPImage *iBmp, UnicodeString iName)
{
	__int32 pos;
	pos = ExtractFileExt(iName).Length();
	if (pos > 0)
	{
		iName = iName.Delete(iName.Length() - pos + 1, pos);
	}
	pos = mpBmpNames->IndexOf(iName);
	if (pos < 0)
	{
		assert(iBmp);
		Graphics::TBitmap *bmp = new Graphics::TBitmap();
		bmp->Assign(const_cast<Graphics::TBitmap*>(iBmp->GetBitmapData()));
		bmp->PixelFormat = pf32bit;
		mpBmpNames->Add(iName);
		BMPImage *tbmp = new BMPImage(bmp);
		mpBmpRes->Add(tbmp);
		pos = mpBmpRes->Count - 1;
		if (gUsingOpenGL && TRenderGL::IsInited())
		{
			tbmp->mOpaqType = (__int32)TRenderGL::CreateTexture();
			TRenderGL::UpdateTextureData(pos);
		}
	}
	else
	{
		assert(mpBmpRes->Items[pos] == NULL);
		ReplaceGraphicResource(iBmp, pos);
    }
	return pos;
}
//---------------------------------------------------------------------------

const BMPImage *__fastcall TCommonResObjManager::GetGraphicResource(__int32 iIdx) const
{
	if (iIdx >= 0 && iIdx < mpBmpRes->Count)
	{
		return(const BMPImage*)mpBmpRes->Items[iIdx];
	}
	return NULL;
}
//---------------------------------------------------------------------------

bool __fastcall TCommonResObjManager::ReplaceGraphicResource(const BMPImage* iBmp, __int32 iIdx)
{
	if (iIdx >= 0 && iIdx < mpBmpRes->Count)
	{
		__int32 openGlResIdx = -1;
		assert(iBmp);
		if(mpBmpRes->Items[iIdx] != NULL)
		{
			openGlResIdx = ((BMPImage*)mpBmpRes->Items[iIdx])->mOpaqType;
			delete (BMPImage*)mpBmpRes->Items[iIdx];
        }
		Graphics::TBitmap *bmp = new Graphics::TBitmap();
		bmp->Assign(const_cast<Graphics::TBitmap*>(iBmp->GetBitmapData()));
		bmp->PixelFormat = pf32bit;
		mpBmpRes->Items[iIdx] = new BMPImage(bmp);
		if (gUsingOpenGL && TRenderGL::IsInited())
		{
			if(openGlResIdx < 0)
			{
				((BMPImage*)mpBmpRes->Items[iIdx])->mOpaqType = (__int32)TRenderGL::CreateTexture();
			}
			else
			{
				((BMPImage*)mpBmpRes->Items[iIdx])->mOpaqType = openGlResIdx;
            }
			TRenderGL::UpdateTextureData(iIdx);
		}
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

void __fastcall TCommonResObjManager::DeleteGraphicResource(UnicodeString iName)
{
	__int32 pos = GetGraphicResourceIndex(iName);
	if(pos >= 0)
	{
		if(mpBmpRes->Items[pos])
		{
			if (gUsingOpenGL)
			{
				assert(TRenderGL::IsInited());
				TRenderGL::DeleteTexture(pos);
			}
			delete (BMPImage*)mpBmpRes->Items[pos];
		}
		mpBmpRes->Items[pos] = NULL;
	}
}
//---------------------------------------------------------------------------

__int32 __fastcall TCommonResObjManager::GetGraphicResourceIndex(UnicodeString iName) const
{
	__int32 pos;
	pos = ExtractFileExt(iName).Length();
	if (pos > 0)
	{
		iName = iName.Delete(iName.Length() - pos + 1, pos);
	}
	pos = mpBmpNames->IndexOf(iName);
	if(pos >= 0 && mpBmpRes->Items[pos] == NULL)
	{
		pos = -1;
	}
	return pos;
}
//---------------------------------------------------------------------------

__int32 __fastcall TCommonResObjManager::GetGraphicResourceCount() const
{
	return mpBmpNames->Count;
}
//---------------------------------------------------------------------------

hash_t __fastcall TCommonResObjManager::AddBitmapFrame(const TFrameObj &obj)
{
	if(!obj.IsEmpty())
	{
		hash_t h = obj.getHash();
		const TFrameObj* bo = GetBitmapFrame(h);
		if (bo == NULL)
		{
			mpBitmapFramesOrder->Add(reinterpret_cast<void*>(h));
		}
		bitmapFrames[h] = obj;
		return h;
	}
    assert(0);
	return 0;
}
//---------------------------------------------------------------------------

void __fastcall TCommonResObjManager::DeleteBitmapFrame(const hash_t& hashValue)
{
	std::hash_map<hash_t, TFrameObj>::iterator it;
	it = bitmapFrames.find(hashValue);
	if (it != bitmapFrames.end())
	{
		bitmapFrames.erase(it);
	}
	__int32 pos;
	if((pos = mpBitmapFramesOrder->IndexOf(reinterpret_cast<void*>(hashValue))) >= 0)
	{
		mpBitmapFramesOrder->Delete(pos);
    }
}
//---------------------------------------------------------------------------

const TFrameObj* __fastcall TCommonResObjManager::GetBitmapFrame(const hash_t& hashValue) const
{
	std::hash_map<hash_t, TFrameObj>::const_iterator it;
	it = bitmapFrames.find(hashValue);
	if (it != bitmapFrames.end())
	{
		return &it->second;
	}
	return NULL;
}
//---------------------------------------------------------------------------

const TFrameObj* __fastcall TCommonResObjManager::GetBitmapFrameByIdx(__int32 idx) const
{
	if(idx < mpBitmapFramesOrder->Count)
	{
		return GetBitmapFrame(reinterpret_cast<hash_t>(mpBitmapFramesOrder->Items[idx]));
    }
	return NULL;
}
//---------------------------------------------------------------------------

const GMapBGObj* TCommonResObjManager::GetMapBGObj(__int32 idx) const
{
	std::map<const __int32, GMapBGObj>::const_iterator it;
	it = BackObj.find(idx);
	if(it != BackObj.end())
	{
		return &it->second;
	}
	return NULL;
}
//---------------------------------------------------------------------------

const TBGobjProperties* TCommonResObjManager::GetBGobjProperties(const UnicodeString& name)
{
	for (__int32 i = 0; i < bgotypes->Count; i++)
	{
		TBGobjProperties *obj = static_cast<TBGobjProperties*>(bgotypes->Items[i]);
		if(obj->name.Compare(name) == 0)
		{
			return obj;
		}
	}
	return NULL;
}
//---------------------------------------------------------------------------

__int32 __fastcall TCommonResObjManager::GetIndexByHash(const hash_t& hashValue) const
{
	return mpBitmapFramesOrder->IndexOf((reinterpret_cast<void*>(hashValue)));
}
//---------------------------------------------------------------------------

__int32 __fastcall TCommonResObjManager::GetBitmapFrameCount()
{
	return mpBitmapFramesOrder->Count;
}
//---------------------------------------------------------------------------

void __fastcall TCommonResObjManager::ReplaceBitmapFrameInAllAnimations(const hash_t& oldHash, const TFrameObj &newObj)
{
	__int32 ctg = GParentObj->Count;
	for (__int32 j = 0; j < ctg; j++)
	{
		static_cast<GPerson*>(GParentObj->Items[j])->ReplaceBitmapFrame(oldHash, newObj);
	}
}
//---------------------------------------------------------------------------

void __fastcall TCommonResObjManager::RenameEventNameInAllAnimations(UnicodeString oldName, UnicodeString newName)
{
	__int32 ctg = GParentObj->Count;
	for (__int32 j = 0; j < ctg; j++)
	{
		static_cast<GPerson*>(GParentObj->Items[j])->RenameEventNameInAnimations(oldName, newName);
	}
}
//---------------------------------------------------------------------------

void __fastcall TCommonResObjManager::CopyBitmapFrameDatabase(TList* olist)
{
	assert(olist);
	assert(olist->Count == 0);
	__int32 ct = GetBitmapFrameCount();
	for(__int32 i = 0; i < ct; i++)
	{
		const TFrameObj *obj = GetBitmapFrameByIdx(i);
		TTempBGobj *bo = new TTempBGobj();
        assert(obj);
		obj->copyTo(bo->o);
		bo->oldHash = reinterpret_cast<hash_t>(mpBitmapFramesOrder->Items[i]); //it->first;
		bo->oldGroup = bo->o.groupId;
		olist->Add(bo);
	}
}
//---------------------------------------------------------------------------

void __fastcall TCommonResObjManager::ApplyFrameDatabaseChangesFromList(TList* ilist)
{
	assert(ilist);

	__int32 ct = ilist->Count;
	for(__int32 i = 0; i < ct; i++)
	{
		TTempBGobj *to = static_cast<TTempBGobj*>(ilist->Items[i]);
		hash_t h = to->o.getHash();
		TFrameObj ro;
		if(to->oldHash != 0)
		{
			DeleteBitmapFrame(to->oldHash);
		}
		if(to->o.w > 0 && to->o.h > 0)
		{
			AddBitmapFrame(to->o);
			ro = to->o;
		}
		if(h != to->oldHash)
		{
			ReplaceBitmapFrameInAllAnimations(to->oldHash, ro);
        }
	}
}
//---------------------------------------------------------------------------

TAnimation *__fastcall TCommonResObjManager::CreateSimplyAnimation(UnicodeString name, hash_t *hashFrameArr, __int32 size_h, __int32 *animArr, __int32 size_i, __int32 align)
{
	#pragma argsused
	assert(hashFrameArr);
	assert(animArr);
	assert(size_h);
	assert(size_i);
	std::list<TAnimationFrameData> l;
	TAnimationFrameData afd;
	TAnimation *a = new TAnimation(size_i);
	for (__int32 i = 0; i < size_i; i++)
	{
		hash_t h = hashFrameArr[animArr[i]];
		if(h > 0)
		{
			const TFrameObj *f = GetBitmapFrame(h);
			assert(f);
            afd.a = 255;
			afd.setData(0);
			afd.SetFrameId(h);
			afd.type = LogicObjType::UOP_LOGIC_OBJ_BMP;
			switch(align)
			{
			case alUP:
				afd.posY = (double)f->h;
				afd.posX = (double)(-f->w / 2);
				break;
			case alLEFT:
				afd.posY = (double)(-f->h / 2);
				afd.posX = 0;
				break;
			case alCENTER:
				afd.posY = (double)(-f->h / 2);
				afd.posX = (double)(-f->w / 2);
				break;
			case alRIGHT:
				afd.posY = (double)(f->h / 2);
				afd.posX = (double)-f->w;
				break;
			case alDOWN:
				afd.posY = (double)-f->h;
				afd.posX = (double)(-f->w / 2);
				break;
			default:
				afd.posX = 0;
				afd.posY = 0;
			}
			afd.rw = f->w;
			afd.rh = f->h;
		}
		else
		{
			afd.setData(0);
			afd.a = 255;
			afd.SetFrameId(0);
			afd.posX = 0;
			afd.posY = 0;
			afd.rw = 0;
			afd.rh = 0;
		}
		l.push_back(afd);
		TAnimationFrame af;
		af.CreateFromList(l);
		a->SetFrame(af, i);
		l.clear();
	}
	a->SetName(name);
	a->SetLooped(true);
	return a;
}
//---------------------------------------------------------------------------

//������������� �������� ���� ��������
void __fastcall TCommonResObjManager::AssignGraphicToObjects(std::list<TResourceItem> *list, UnicodeString path, UnicodeString *errorlist)
{
	bool result;
	TRect r;
	__int32 i, j, k, gr_idx, err_list_restriction, err_obj_counter, err_bg_counter;
	Graphics::TBitmap *bmp, *t_bmp;
	BMPImage *i2d;
	UnicodeString name, str;
	GPerson *gp;
	GMapBGObj *mo;
	TEDitObjSelectionParams params;
	UnicodeString err_message;
	TStringList *err_obj_list;
	TStringList *err_bg_list;
	TStringList *err_snd_list;
	TStringList *err_fnt_list;
	std::list<TResourceItem>::iterator lit;
	std::list<TFrameObj*> bgFrameObjArray1;
	std::list<TFrameObj*> bgFrameObjArray2;
	std::list<TFrameObj*> *bgFrameObjArray = NULL;
	err_list_restriction = 20;

	err_obj_list = new TStringList();
	err_bg_list = new TStringList();
	err_snd_list = new TStringList();
	err_fnt_list = new TStringList();

	*errorlist = _T("\0");

  //��������������� ������� ��������
	for (lit = list->begin(); lit != list->end(); ++lit)
	{
		result = true;
		err_obj_list->Clear();
		err_bg_list->Clear();
		err_obj_counter = 0;
		err_bg_counter = 0;

		const ResourceTypes ltype = lit->mType;
		switch(ltype)
		{
			case RESOURCE_TYPE_FONTDATA:
			if(errorlist != NULL)
			{
				str = forceCorrectFontFileExtention(path + _T("\\") + lit->mName);
				if(!validateFontResource(str))
				{
					err_fnt_list->Add(Localization::Text(_T("mOpenFontFileError")) + _T(" ") + str);
				}
			}
			continue;

			case RESOURCE_TYPE_SOUNDDATA:
			if(errorlist != NULL)
			{
				str = forceCorrectSoundFileExtention(path + _T("\\") + lit->mName);
				if(!validateSoundResource(str, NULL))
				{
					err_snd_list->Add(Localization::Text(_T("mOpenSoundFileError")) + _T(" ") + str);
				}
			}
			continue;

			case RESOURCE_TYPE_IMAGE:
			{
				str = path + lit->mName;
				bmp = new Graphics::TBitmap();
				i2d = new BMPImage(bmp);

				if(!read_png(str.c_str(), bmp))
				{
					err_message = Localization::TextWithVar(_T("mOpenGraphicsFileError_s"), str);
					result = false;
				}

				gr_idx = -1;
				if(result)
				{
					gr_idx = AddGraphicResource(i2d, lit->mName);
				}

				if(bgFrameObjArray == NULL)
				{
				   bgFrameObjArray = &bgFrameObjArray1;
				   const __int32 backObjSize = static_cast<__int32>(BackObj.size());
				   for(j = 0; j < backObjSize; j++)
				   {
						const GMapBGObj& mo = BackObj[j];
						for(k = 0; k < mo.GetFrameObjCount(); k++)
						{
							TFrameObj* bo = mo.GetFrameObj(k);
							if(bo == NULL)
							{
								continue;
							}
							if(bo->IsEmpty() == true)
							{
								continue;
							}
							bo->getSourceRes(NULL, NULL, NULL, &name);
							if(name.Compare(lit->mName) == 0)
							{
								if(!result)
								{
									if(err_bg_list->Count < err_list_restriction)
									{
										const UnicodeString err_str = Localization::Text(_T("mBgObject")) + _T(" �") + IntToStr(j);
										if(err_bg_list->IndexOf(err_str) < 0)
										{
										   err_bg_list->Add(err_str);
										}
									}
									else
									{
										err_bg_counter++;
									}
								}
								bo->setSourceRes(lit->mWidth, lit->mHeight, gr_idx, name);
							 }
							 else
							 {
								bgFrameObjArray->push_back(bo);
							 }
						}
				   }
				}
				else
				{
					std::list<TFrameObj*> *bgFrameObjArray_t;
					if(bgFrameObjArray1.empty())
					{
						bgFrameObjArray_t = &bgFrameObjArray1;
					}
					else
					{
						bgFrameObjArray_t = &bgFrameObjArray2;
					}
					for (std::list<TFrameObj*>::iterator it = bgFrameObjArray->begin();
							it != bgFrameObjArray->end();
							++it)
					{
						TFrameObj* bo = *it;
						if(bo != NULL)
						{
							bo->getSourceRes(NULL, NULL, NULL, &name);
							if(name.Compare(lit->mName) == 0)
							{
								if(!result)
								{
									if(err_bg_list->Count < err_list_restriction)
									{
										const UnicodeString err_str = Localization::Text(_T("mBgObject")) + _T(" �") + IntToStr(j);
										if(err_bg_list->IndexOf(err_str) < 0)
										{
											err_bg_list->Add(err_str);
										}
									}
									else
									{
										err_bg_counter++;
									}
								}
								bo->setSourceRes(lit->mWidth, lit->mHeight, gr_idx, name);
							}
							else
							{
								bgFrameObjArray_t->push_back(bo);
							}
						}
					}
					bgFrameObjArray->clear();
					bgFrameObjArray = bgFrameObjArray_t;
				}

				for (std::hash_map<hash_t, TFrameObj>::iterator it = bitmapFrames.begin();
						it != bitmapFrames.end();
						++it)
				{
					it->second.getSourceRes(NULL, NULL, NULL, &name);
					if(name.Compare(lit->mName) == 0)
					{
						if(!result)
						{
							if(err_obj_list->Count < err_list_restriction)
							{
								for(j = 0; j < GParentObj->Count; j++)
								{
									TList *tfolist = new TList();
									gp = (GPerson *)GParentObj->Items[j];
									gp->GetBitmapFrameList(tfolist);
									for(k = 0; k < tfolist->Count; k++)
									{
										const TFrameObj* bo = GetBitmapFrame((hash_t)tfolist->Items[k]);
										if(bo == NULL)
										{
											continue;
										}
										if(bo == &it->second)
										{
											if(err_obj_list->IndexOf(gp->GetName()) < 0)
											{
												err_obj_list->Add(gp->GetName());
											}
											break;
										}
									}
									delete tfolist;
								}
							}
							else
							{
								err_obj_counter++;
							}
						}
						it->second.setSourceRes(lit->mWidth, lit->mHeight, gr_idx, name);
					}
				}
				if(!result && errorlist != NULL)
				{
					if(!(*errorlist).IsEmpty())
					{
						*errorlist += _T("\n\n");
					}

					if(err_obj_list->Count > 0 || err_bg_list->Count)
					{
						err_message += _T("\n") + Localization::Text(_T("mThisResourceUsedBy")) + _T(":");
						for(j = 0; j < err_bg_list->Count; j++)
						{
							err_message += (j == 0 ? _T("\n") : _T(", ")) + err_bg_list->Strings[j];
						}
						if(err_bg_counter > 0)
						{
							err_message += _T(" ... ") + Localization::Text(_T("mAndMore")) + _T(" ") + IntToStr(err_bg_counter);
						}

						for(j = 0; j < err_obj_list->Count; j++)
						{
							err_message += (j == 0 ? _T("\n") : _T(", ")) + err_obj_list->Strings[j];
						}
						if(err_obj_counter > 0)
						{
							err_message += _T(" ... ") + Localization::Text(_T("mAndMore")) + _T(" ") + IntToStr(err_obj_counter);
						}
					}
					*errorlist += err_message;
				}
				delete i2d;
			}
		}
	}

	if(errorlist != NULL && err_snd_list->Count > 0)
	{
		err_message = _T("\n\n");
		for(j = 0; j < err_snd_list->Count; j++)
		{
			err_message += err_snd_list->Strings[j] + _T("\n");
		}
		*errorlist += err_message;
	}
	if(errorlist != NULL && err_fnt_list->Count > 0)
	{
		err_message = _T("\n\n");
		for(j = 0; j < err_fnt_list->Count; j++)
		{
			err_message += err_fnt_list->Strings[j] + _T("\n");
		}
		*errorlist += err_message;
	}

	delete err_obj_list;
	delete err_bg_list;
	delete err_snd_list;
	delete err_fnt_list;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

__fastcall TObjManager::TObjManager(TCommonResObjManager *icommonRes)
{
	commonRes = icommonRes;

	scripts = new TList();

	for (__int32 i = 0; i < OBJ_TYPE_COUNT; i++)
	{
		arrays[i] = new TList();
	}
	map_u = arrays[CHARACTER_OBJECT];
	decor_b = arrays[BACK_DECORATION];
	decor_f = arrays[FORE_DECORATION];
	triggers = arrays[TRIGGER];
	directors = arrays[DIRECTOR];
	collideshapes = arrays[COLLIDESHAPE];

	mpTempCollideNodesList = new TList();
	mRefreshTempCollideNodesList = true;

	map_undo = map = NULL;
	old_mp_w = old_mp_h = mp_w = mp_h = startZone = mesCount = znCount = 0;
	old_md_w = old_md_h = 0;
	tileCameraPosOnStart = -1;
    ignoreUnoptimizedResoures = false;

	StartScriptName = _T("");

	m_pCashe = new TList();
}
//---------------------------------------------------------------------------

__fastcall TObjManager::~TObjManager()
{
	__int32 i;
	if (map != NULL)
	{
		delete[]map;
		delete[]map_undo;
	}

	for (i = 0; i < scripts->Count; i++)
	{
		delete (TGScript*)scripts->Items[i];
    }
	delete scripts;
	for (i = 0; i < map_u->Count; i++)
	{
		TMapGPerson* o = (TMapGPerson*)map_u->Items[i];
		delete o;
	}
	delete map_u;
	for (i = 0; i < decor_b->Count; i++)
	{
		delete (TMapGPerson*)decor_b->Items[i];
	}
	delete decor_b;
	for (i = 0; i < decor_f->Count; i++)
	{
		delete (TMapGPerson*)decor_f->Items[i];
	}
	delete decor_f;
	for (i = 0; i < triggers->Count; i++)
	{
		delete (TMapTrigger*)triggers->Items[i];
	}
	delete triggers;
	for (i = 0; i < directors->Count; i++)
	{
		delete (TMapDirector*)directors->Items[i];
	}
	delete directors;
	for (i = 0; i < collideshapes->Count; i++)
	{
		delete (TCollideChainShape*)collideshapes->Items[i];
	}
	delete collideshapes;
	delete mpTempCollideNodesList;

	for (i = 0; i < m_pCashe->Count; i++)
	{
        DelCashe(i);
	}
	delete m_pCashe;
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::ClearAll()
{
	__int32 i;

	for (i = 0; i < map_u->Count; i++)
		delete(TMapGPerson*)map_u->Items[i];
	map_u->Clear();

	for (i = 0; i < decor_b->Count; i++)
		delete(TMapGPerson*)decor_b->Items[i];
	decor_b->Clear();

	for (i = 0; i < decor_f->Count; i++)
		delete(TMapGPerson*)decor_f->Items[i];
	decor_f->Clear();

	for (i = 0; i < triggers->Count; i++)
		delete(TMapTrigger*)triggers->Items[i];
	triggers->Clear();

	for (i = 0; i < directors->Count; i++)
		delete(TMapDirector*)directors->Items[i];
	directors->Clear();

	for (i = 0; i < scripts->Count; i++)
		delete(TGScript*)scripts->Items[i];
	scripts->Clear();

	for (i = 0; i < collideshapes->Count; i++)
		delete (TCollideChainShape*)collideshapes->Items[i];
	collideshapes->Clear();

	mpTempCollideNodesList->Clear();
	ClearCashe();
	mRefreshTempCollideNodesList = true;
}
//---------------------------------------------------------------------------

//bug with pointer dublicates
void __fastcall TObjManager::CheckObjForWrongLinks()
{
	__int32 i, j;
	for (i = 0; i < map_u->Count; i++)
	{
		TMapGPerson* mp = (TMapGPerson*)map_u->Items[i];
		if(mp->IsChildExist())
		{
			for(j = 0; j < MAX_ANIMATON_JOIN_NODES; j++)
			{
				UnicodeString chname = mp->GetChild(j);
				if(!chname.IsEmpty())
				{
					TMapGPerson *cmp = (TMapGPerson*)FindObjectPointer(chname);
					if(cmp != NULL)
					{
						cmp->ParentObj = mp->Name;
					}
					else
					{
						mp->AddChild(j, UnicodeString());
                    }
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

TMapTextBox* __fastcall TObjManager::CreateMapTextBox(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString ParentName)
{
	TMapTextBox* obj = new TMapTextBox(x, y, pos_x, pos_y, ParentName, this);
	map_u->Add((void*)obj);
	return obj;
}
//---------------------------------------------------------------------------

TMapTextBox* __fastcall TObjManager::CreateMapTextBox()
{
	TMapTextBox* obj = new TMapTextBox(this);
	map_u->Add((void*)obj);
	return obj;
}
//---------------------------------------------------------------------------

TMapSoundScheme* __fastcall TObjManager::CreateMapSoundScheme()
{
	TMapSoundScheme* obj = new TMapSoundScheme(this);
	map_u->Add((void*)obj);
	return obj;
}
//---------------------------------------------------------------------------

TMapSoundScheme* __fastcall TObjManager::CreateMapSoundScheme(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString ParentName)
{
	TMapSoundScheme* obj = new TMapSoundScheme(x, y, pos_x, pos_y, ParentName, this);
	map_u->Add((void*)obj);
	return obj;
}
//---------------------------------------------------------------------------

TMapObjPool* __fastcall TObjManager::CreateMapObjPool()
{
	TMapObjPool* obj = new TMapObjPool(this);
	map_u->Add((void*)obj);
	return obj;
}
//---------------------------------------------------------------------------

TMapObjPool* __fastcall TObjManager::CreateMapObjPool(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString ParentName)
{
	TMapObjPool* obj = new TMapObjPool(x, y, pos_x, pos_y, ParentName, this);
	map_u->Add((void*)obj);
	return obj;
}
//---------------------------------------------------------------------------

TMapGPerson* __fastcall TObjManager::CreateMapGPerson()
{
	TMapGPerson* obj = new TMapGPerson(this);
	map_u->Add((void*)obj);
	return obj;
}
//---------------------------------------------------------------------------

TMapGPerson* __fastcall TObjManager::CreateMapGPerson(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString ParentName)
{
	TMapGPerson* obj = new TMapGPerson(x, y, pos_x, pos_y, ParentName, this);
	map_u->Add((void*)obj);
	return obj;
}
//---------------------------------------------------------------------------

TMapTrigger* __fastcall TObjManager::CreateMapTrigger(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString ParentName)
{
	TMapTrigger* obj = new TMapTrigger(x, y, pos_x, pos_y, ParentName, this);
	triggers->Add((void*)obj);
	return obj;
}
//---------------------------------------------------------------------------

TMapTrigger* __fastcall TObjManager::CreateMapTrigger()
{
	TMapTrigger* obj = new TMapTrigger(this);
	triggers->Add((void*)obj);
	return obj;
}
//---------------------------------------------------------------------------

TMapDirector* __fastcall TObjManager::CreateMapDirector(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString ParentName)
{
	TMapDirector* obj = new TMapDirector(x, y, pos_x, pos_y, ParentName, this);
	directors->Add((void*)obj);
	return obj;
}
//---------------------------------------------------------------------------

TMapDirector* __fastcall TObjManager::CreateMapDirector()
{
	TMapDirector* obj = new TMapDirector(this);
	directors->Add((void*)obj);
	return obj;
}
//---------------------------------------------------------------------------

TCollideChainShape* __fastcall TObjManager::CreateCollideChainShape()
{
	mRefreshTempCollideNodesList = true;
	TCollideChainShape* sh = new TCollideChainShape();
	sh->SetName(AutoNameObj(sh));
	collideshapes->Add((void*)sh);
	return sh;
}
//---------------------------------------------------------------------------

TMapCollideLineNode* __fastcall TObjManager::CreateMapCollideNode(double x, double y, __int32 *pos_x, __int32 *pos_y, const UnicodeString& ParentName)
{
	TCollideChainShape* sh = NULL;
	if(ParentName.IsEmpty() == true)
	{
		sh = CreateCollideChainShape();
	}
	else
	{
		TMapPerson* mp = FindObjectPointer(ParentName);
		if(mp != NULL && mp->GetObjType() == objMAPCOLLIDESHAPE)
		{
			sh = static_cast<TCollideChainShape*>(mp);
		}
	}
	if(sh != NULL)
	{
		mRefreshTempCollideNodesList = true;
		TMapCollideLineNode& cn = sh->CreateNode(x, y, pos_x, pos_y);
		cn.Name = AutoNameObj(&cn);
		return &cn;
	}
	else
	{
		return NULL;
	}
}
//---------------------------------------------------------------------------

TGScript* __fastcall TObjManager::CreateScript(TGScript* temp)
{
	TGScript* obj = new TGScript();
	if(temp!= NULL)
	{
		temp->CopyTo(obj);
	}
	scripts->Add(obj);
	return obj;
}
//---------------------------------------------------------------------------

bool __fastcall TObjManager::DeleteObject(TMapPerson* mp)
{
	bool res;
	__int32 pos;
	assert(mp);
	ClearCasheFor(mp);
    res = false;
	switch(mp->GetObjType())
	{
		case objMAPOBJ:
		{
			TMapGPerson* mgp = static_cast<TMapGPerson*>(mp);
			switch(mgp->GetDecorType())
			{
				case decorNONE:
					if((pos = map_u->IndexOf(mp)) >= 0)
					{
						map_u->Delete(pos);
						res = true;
					}
					break;
				case decorBACK:
					if((pos = decor_b->IndexOf(mp)) >= 0)
					{
						decor_b->Delete(pos);
						res = true;
					}
					break;
				case decorFORE:
					if((pos = decor_f->IndexOf(mp)) >= 0)
					{
						decor_f->Delete(pos);
						res = true;
					}
			}
		}
		break;
		case objMAPTEXTBOX:
			if((pos = map_u->IndexOf(mp)) >= 0)
			{
				map_u->Delete(pos);
				res = true;
			}
		break;
		case objMAPSOUNDSCHEME:
			if((pos = map_u->IndexOf(mp)) >= 0)
			{
				map_u->Delete(pos);
				res = true;
			}
		break;
		case objMAPOBJPOOL:
			if((pos = map_u->IndexOf(mp)) >= 0)
			{
				map_u->Delete(pos);
				res = true;
			}
		break;
		case objMAPTRIGGER:
			if((pos = triggers->IndexOf(mp)) >= 0)
			{
				triggers->Delete(pos);
				res = true;
			}
		break;
		case objMAPDIRECTOR:
			if((pos = directors->IndexOf(mp)) >= 0)
			{
				directors->Delete(pos);
				res = true;
			}
		break;
		case objMAPCOLLIDENODE:
		{
			const __int32 len = collideshapes->Count;
			for (__int32 i = 0; i < len; i++)
			{
				TCollideChainShape* sh = (TCollideChainShape*)collideshapes->Items[i];
				if(sh->DeleteNode((const TMapCollideLineNode*)mp) == true)
				{
                    mRefreshTempCollideNodesList = true;
					if(sh->GetNodesCount() <= 1)
					{
						delete sh;
						collideshapes->Delete(i);
					}
					return true;
				}
			}
		}
		return false;
		case objMAPCOLLIDESHAPE:
			if((pos = collideshapes->IndexOf(mp)) >= 0)
			{
                mRefreshTempCollideNodesList = true;
				collideshapes->Delete(pos);
				res = true;
			}
		break;
		default:
			assert(0);
	}
	delete mp;
	return res;
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::DeleteGroup(UnicodeString gpname)
{
	__int32 i, ct = 0;
	TList *lst;
	for (__int32 k = 0; k < 3; k++)
	{
		switch(k)
		{
		case 0:
			lst = map_u;
			break;
		case 1:
			lst = decor_b;
			break;
		case 2:
			lst = decor_f;
		}
		for (i = 0; i < lst->Count; i++)
		{
            TMapPerson *mo = (TMapPerson*)lst->Items[i];
			if(mo->GetObjType() == objMAPOBJPOOL)
			{
				TMapObjPool *mop = (TMapObjPool*)mo;
				for(__int32 mop_pi = 0; mop_pi < mop->GetItemsCount(); mop_pi++)
				{
					ObjPoolPropertyItem mopit;
					mop->GetItemByIndex(mop_pi, mopit);
					if(mopit.mName.Compare(gpname) == 0)
					{
						mop->DeleteItem(gpname);
						mop_pi--;
					}
				}
			}
			else
			if(mo->getGParentName().Compare(gpname) == 0)
			{
				delete mo;
				lst->Delete(i);
				i--;
				ct++;
			}
		}
	}
	return ct;
}
//---------------------------------------------------------------------------

bool __fastcall TObjManager::DeleteScript(TGScript* spt)
{
	bool res;
	__int32 pos;
    res = false;
	assert(spt);
	if((pos = scripts->IndexOf(spt)) >= 0)
	{
		scripts->Delete(pos);
		res = true;
	}
	delete spt;
	return res;
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TObjManager::AutoNameObj(TMapPerson *obj)
{
	__int32 idx;
	bool res;
	UnicodeString str, prefix;
	switch(obj->GetObjType())
	{
	case objMAPOBJ:
		prefix = obj->getGParentName() + _T("_\0");
		idx = GetObjTypeCount(TObjManager::CHARACTER_OBJECT) + 1;
		break;
	case objMAPTRIGGER:
		prefix = _T("TRIG_\0");
		idx = GetObjTypeCount(TObjManager::TRIGGER) + 1;
		break;
	case objMAPDIRECTOR:
		prefix = _T("NODE_\0");
		idx = GetObjTypeCount(TObjManager::DIRECTOR) + 1;
		break;
	case objMAPTEXTBOX:
		prefix = _T("TXTBOX_\0");
		idx = GetObjTypeCount(TObjManager::CHARACTER_OBJECT) + 1;
		break;
	case objMAPOBJPOOL:
		prefix = _T("POOL_OBJECT_\0");
		idx = GetObjTypeCount(TObjManager::CHARACTER_OBJECT) + 1;
		break;
	case objMAPCOLLIDENODE:
		prefix = _T("COLLIDENODE_\0");
		idx = GetCollideNodesCount();
		break;
	case objMAPCOLLIDESHAPE:
		prefix = _T("COLLIDESHAPE_\0");
		idx = GetObjTypeCount(TObjManager::COLLIDESHAPE) + 1;
		break;
	default:
		prefix = _T("ERRORTYPE_\0");
		idx = 0;
	}
	do
	{
		str = prefix + IntToStr(idx);
		res = CheckIfMGONameExists(str, NULL);
		idx++;
	}
	while (res == true);
	return str;
}
//---------------------------------------------------------------------------

bool __fastcall TObjManager::CheckIfMGONameExists(UnicodeString str, void *pItem) const
{
	__int32 i, j;
	TMapPerson *mo;
	TObjManager::TObjType tlist[OBJ_TYPE_COUNT] =
	{
		TObjManager::CHARACTER_OBJECT,
		TObjManager::BACK_DECORATION,
		TObjManager::FORE_DECORATION,
		TObjManager::TRIGGER,
		TObjManager::DIRECTOR,
		TObjManager::COLLIDESHAPE
	};
	for(j = 0; j < OBJ_TYPE_COUNT; j++)
	{
		__int32 len = GetObjTypeCount(tlist[j]);
		for (i = 0; i < len; i++)
		{
			mo = (TMapPerson*)GetObjByIndex(tlist[j], i);
			if (pItem == mo)
			{
				continue;
			}
			else if (mo->Name.Compare(str) == 0)
			{
				return true;
			}
			if(tlist[j] == TObjManager::COLLIDESHAPE)
			{
				TCollideChainShape* sh = static_cast<TCollideChainShape*>(mo);
				if(sh->FindNode(str) != nullptr)
				{
					return true;
                }
            }
		}
	}
	return false;
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::OrderChildsBeforeMain(TMapGPerson *op)
{
	__int32 idx, i, j, len;
	if (op == NULL)
	{
		return;
	}
	len = GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
	for (i = 0; i < len; i++)
	{
		if (GetObjByIndex(TObjManager::CHARACTER_OBJECT, i) == op)
		{
			idx = i;
			break;
		}
	}
	if(op->IsChildExist())
	{
		for (i = 0; i < MAX_ANIMATON_JOIN_NODES; i++)
		{
			UnicodeString chname = op->GetChild(i);
			if(!chname.IsEmpty())
			{
				TMapGPerson *child = (TMapGPerson*)FindObjectPointer(chname);
				if (child == NULL)
				{
					op->AddChild(i, UnicodeString());
				}
				else
				{
					for (j = 0; j < len; j++)
					{
						if (GetObjByIndex(TObjManager::CHARACTER_OBJECT, j) == child)
						{
							if (idx > j)
							{
								map_u->Move(j, idx);
							}
							break;
						}
					}
					OrderChildsBeforeMain(child);
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::ChangeScriptNameInAllObjects(UnicodeString oldName, UnicodeString currentName, __int32 sptZone, __int32 sptMode)
{
	__int32 j, k;
	TMapGPerson *p;
	for (j = 0; j < map_u->Count; j++)
	{
		p = (TMapGPerson*)map_u->Items[j];
		for (k = 0; k < ::Actions::sptCOUNT; k++)
		{
			UnicodeString scr = p->getScriptName(::Actions::Names[k]);
			if (scr.Compare(oldName) == 0 && !scr.IsEmpty())
			{
				UnicodeString str = currentName;
				if (UnicodeString(::Actions::Names[k]).Compare(UnicodeString(::Actions::Names[sptMode])) != 0 && ::Actions::sptONENDANIM != sptMode)
					str = _T("\0");
				if (p->GetZone() != sptZone && sptZone != ZONES_INDEPENDENT_IDX && sptZone != LEVELS_INDEPENDENT_IDX)
					str = _T("\0");
				p->setScriptName(::Actions::Names[k], str);
			}
		}
	}
	for (j = 0; j < triggers->Count; j++)
	{
		p = (TMapGPerson*)triggers->Items[j];
		for (k = 0; k < ::Actions::sptCOUNT; k++)
		{
			UnicodeString scr = p->getScriptName(::Actions::Names[k]);
			if (scr.Compare(oldName) == 0 && !scr.IsEmpty())
			{
				UnicodeString str = currentName;
				if (UnicodeString(::Actions::Names[k]).Compare(UnicodeString(::Actions::Names[sptMode])) != 0 && ::Actions::sptONENDANIM != sptMode)
					str = _T("\0");
				if (p->GetZone() != sptZone && sptZone != ZONES_INDEPENDENT_IDX && sptZone != LEVELS_INDEPENDENT_IDX)
					str = _T("\0");
				p->setScriptName(::Actions::Names[k], str);
			}
		}
	}
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::GetAnimationID(UnicodeString name) const
{
	return commonRes->GetAnimationID(name);
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::GetScriptID(UnicodeString name) const
{
	if (name.Compare(UnicodeString(SpecialObjConstants::VarBlankActionName)) == 0)
	{
		return SpecialObjConstants::trOBJ_CHANGE_PRP_BLANKACTION;
	}
	/*
	__int32 len = scripts->Count;
	for (__int32 i = 0; i < len; i++)
	{
		TGScript *s = (TGScript*)scripts->Items[i];
		if (s->Name.Compare(name) == 0)
			return i;
	}
    */
	return -1;
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::GetGameObjID(UnicodeString name) const
{
	if (name.Compare(UnicodeString(SpecialObjConstants::VarObjNameInitiator)) == 0)
	{
		return SpecialObjConstants::trOBJ_CHANGE_PRP_INITIATOR;
	}
	else if (name.Compare(UnicodeString(SpecialObjConstants::VarObjNameSecond)) == 0)
	{
		return SpecialObjConstants::trOBJ_CHANGE_PRP_SECOND;
	}

	__int32 len = map_u->Count;
	__int32 index = 0;
	for (__int32 i = 0; i < len; i++)
	{
		const TMapPerson *s = (const TMapGPerson*)map_u->Items[i];
		if(s->GetObjType() == objMAPOBJPOOL)
		{
			const TMapObjPool *op = static_cast<const TMapObjPool*>(s);
			index += op->GetTolalObjCount();
			continue;
		}
		else if(s->GetObjType() == objMAPSOUNDSCHEME)
		{
        	continue;
        }
		else if (s->Name.Compare(name) == 0)
		{
			return index;
		}
		index++;
	}
	return -1;
}
//---------------------------------------------------------------------------
__int32 __fastcall TObjManager::GetStateID(UnicodeString name) const
{
	return commonRes->GetStateID(name);
}

//---------------------------------------------------------------------------
__int32 __fastcall TObjManager::GetTriggerID(UnicodeString name) const
{
	__int32 len = triggers->Count;
	for (__int32 i = 0; i < len; i++)
	{
		TMapTrigger *s = (TMapTrigger*)triggers->Items[i];
		if (s->Name.Compare(name) == 0)
			return i;
	}
	return -1;
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::GetDirectorID(UnicodeString name) const
{
	__int32 len = directors->Count;
	for (__int32 i = 0; i < len; i++)
	{
		TMapDirector *d = (TMapDirector*)directors->Items[i];
		if (d->Name.Compare(name) == 0)
			return i;
	}
	return -1;
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::GetParentObjID(UnicodeString name) const
{
	return commonRes->GetParentObjID(name);
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::GetCollideShapeID(const UnicodeString& name) const
{
	const __int32 len = collideshapes->Count;
	for (__int32 i = 0; i < len; i++)
	{
		TCollideChainShape *sh = (TCollideChainShape*)collideshapes->Items[i];
		if (sh->Name.Compare(name) == 0)
			return i;
	}
	return -1;
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::GetTextBoxID(UnicodeString name) const
{
	__int32 len = map_u->Count;
	for (__int32 i = 0; i < len; i++)
	{
		const TMapGPerson *gcl = (TMapGPerson*)map_u->Items[i];
		if(gcl->GetObjType() == objMAPTEXTBOX)
		{
			const TMapTextBox *cl = (TMapTextBox*)gcl;
			if (cl->Name.Compare(name) == 0)
				return i;
        }
	}
	return -1;
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::GetEventID(UnicodeString name) const
{
	return commonRes->GetEventID(name);
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::RefreshAllTextsObjects()
{
	__int32 len = map_u->Count;
	for (__int32 i = 0; i < len; i++)
	{
		const TMapGPerson *gcl = (TMapGPerson*)map_u->Items[i];
		if(gcl->GetObjType() == objMAPTEXTBOX)
		{
			TMapTextBox *cl = (TMapTextBox*)gcl;
			ClearCasheFor(cl);
        }
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::ClearAllObjects()
{
	__int32 i;
	for (i = 0; i < map_u->Count; i++)
		delete(TMapGPerson*)map_u->Items[i];
	map_u->Clear();
	for (i = 0; i < decor_b->Count; i++)
		delete(TMapGPerson*)decor_b->Items[i];
	decor_b->Clear();
	for (i = 0; i < decor_f->Count; i++)
		delete(TMapGPerson*)decor_f->Items[i];
	decor_f->Clear();
	for (i = 0; i < triggers->Count; i++)
		delete(TMapTrigger*)triggers->Items[i];
	triggers->Clear();
	for (i = 0; i < directors->Count; i++)
		delete(TMapDirector*)directors->Items[i];
	directors->Clear();
	ClearCashe();
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::SetToParentProperties(TMapGPerson *pr)
{
	GPerson *gp;
	for (__int32 i = 0; i < commonRes->GParentObj->Count; i++)
	{
		gp = (GPerson*)commonRes->GParentObj->Items[i];
		if (pr->getGParentName().Compare(gp->GetName()) == 0)
		{
			pr->SetProperties(gp->GetBaseProperties());
			break;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::SetToParentPropertiesAll()
{
	for (__int32 i = 0; i < map_u->Count; i++)
		SetToParentProperties((TMapGPerson*)map_u->Items[i]);
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::RealignPosAll()
{
	__int32 i;
	for (i = 0; i < map_u->Count; i++)
		((TMapGPerson*)map_u->Items[i])->OnScrollBarChange();
	for (i = 0; i < decor_b->Count; i++)
		((TMapGPerson*)decor_b->Items[i])->OnScrollBarChange();
	for (i = 0; i < decor_f->Count; i++)
		((TMapGPerson*)decor_f->Items[i])->OnScrollBarChange();
	for (i = 0; i < triggers->Count; i++)
		((TMapTrigger*)triggers->Items[i])->OnScrollBarChange();
	for (i = 0; i < directors->Count; i++)
		((TMapDirector*)directors->Items[i])->OnScrollBarChange();
	for (i = 0; i < collideshapes->Count; i++)
		((TCollideChainShape*)collideshapes->Items[i])->OnScrollBarChange();
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::ScalePosAll(__int32 p_w, __int32 p_h, __int32 tp_w, __int32 tp_h, bool remainMapSizeMode)
{
	TMapPerson *gp;
	double x, y;
	__int32 i, j;

	if(commonRes)
	{
		TFrameObj *fobj;
		const __int32 ct = static_cast<__int32>(commonRes->BackObj.size());
		for (i = 0; i < ct; i++)
		{
			const GMapBGObj& bobj = commonRes->BackObj[i];
			for (j = 0; j < bobj.GetFrameObjCount(); j++)
			{
				fobj = bobj.GetFrameObj(j);
				if(fobj != NULL)
				{
					fobj->y += (short)(ceil(((double)fobj->y / (double)tp_h) * (double)(p_h - tp_h)));
					fobj->x += (short)(ceil(((double)fobj->x / (double)tp_w) * (double)(p_w - tp_w)));
					fobj->w = (short)p_w;
					fobj->h = (short)p_h;
				}
            }
		}
	}

	TList *lst;
	if(!remainMapSizeMode)
	for (j = 0; j < 6; j++)
	{
		switch(j)
		{
		case 0:
			lst = decor_b;
			break;
		case 1:
			lst = map_u;
			break;
		case 2:
			lst = decor_f;
			break;
		case 3:
			lst = triggers;
			break;
		case 4:
			lst = directors;
			break;
		case 5:
			lst = GetCollideNodesList();
			break;
		}
		for (i = 0; i < lst->Count; i++)
		{
			gp = (TMapPerson*)lst->Items[i];
			gp->getCoordinates(&x, &y);
			if(remainMapSizeMode)
			{
				y += ceil((y / (double)tp_h) * (double)(p_h - tp_h));
				x += ceil((x / (double)tp_w) * (double)(p_w - tp_w));
			}
			else
			{
				y += ceil((y / (double)tp_h) * (double)(p_h - tp_h));
				x += ceil((x / (double)tp_w) * (double)(p_w - tp_w));
            }
			gp->setCoordinates(x, y, 0, 0);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::CheckAndDeleteBrokenCollidelines(__int32 p_w, __int32 p_h)
{
	TRect r;
	r.Left = 0;
	r.Top = 0;
	r.Bottom = mp_h * p_h;
	r.Right = mp_w * p_w;
    mRefreshTempCollideNodesList = true;
	for (__int32 i = 0; i < collideshapes->Count; i++)
	{
		TCollideChainShape* sh = (TCollideChainShape*)collideshapes->Items[i];
		sh->TrimNodes(r);
		if(sh->GetNodesCount() <= 1)
		{
			delete sh;
			collideshapes->Delete(i);
			i--;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::CreateListObjOutOfBoundsMap(TRect *r, TList *ipList)
{
	__int32 i, j;
	double x, y;
	const TMapPerson *gp;
	TList *lst;

	assert(ipList);

	for (j = 0; j < 6; j++)
	{
		switch(j)
		{
		case 0:
			lst = decor_b;
			break;
		case 1:
			lst = map_u;
			break;
		case 2:
			lst = decor_f;
			break;
		case 3:
			lst = triggers;
			break;
		case 4:
			lst = directors;
			break;
		case 5:
			lst = GetCollideNodesList();
			break;
		}
		for (i = 0; i < lst->Count; i++)
		{
			gp = (TMapPerson*)lst->Items[i];
			gp->getCoordinates(&x, &y);
			if ((__int32)x > r->Width() || (__int32)y > r->Height() ||
				(__int32)x + gp->Width < r->Left || (__int32)y + gp->Height < r->Top)
			{
				if (gp->GetObjType() == objMAPCOLLIDENODE)
				{
					gp = FindObjectPointer(gp->getGParentName());
					if(gp != NULL && gp->GetObjType() != objMAPCOLLIDESHAPE)
					{
						gp = NULL;
					}
				}
				if (gp != NULL && ipList->IndexOf((void*)gp) < 0)
				{
					ipList->Add((void*)gp);
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::CheckForWarningsAndErrors(TStringList *str_list)
{
	__int32 i;
	TMapGPerson *mp;
	TList *lst;
	bool state_err = false;
	bool null_animation = false;

  //empty state check
	__int32 len = GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
	for (i = 0; i < len; i++)
	{
		mp = static_cast<TMapGPerson*>(GetObjByIndex(TObjManager::CHARACTER_OBJECT, i));
		if (mp->State_Name.IsEmpty() &&
				mp->GetObjType() != objMAPTEXTBOX &&
				mp->GetObjType() != objMAPSOUNDSCHEME &&
				mp->GetObjType() != objMAPOBJPOOL)
		{
			if (!state_err)
			{
				state_err = true;
				str_list->Add(Localization::Text(_T("mEmptyStateListHeaderError")) + _T(": "));
			}
			str_list->Add(mp->Name);
		}
	}

  //null-animation check
	for (__int32 k = 0; k < 3; k++)
	{
		switch(k)
		{
		case 0:
			lst = map_u;
			break;
		case 1:
			lst = decor_b;
			break;
		case 2:
			lst = decor_f;
		}
		for (i = 0; i < lst->Count; i++)
		{
			__int32 j;
			GPerson *gp;
			bool found = false;
			mp = (TMapGPerson*)lst->Items[i];
			for (j = 0; j < commonRes->GParentObj->Count; j++)
			{
				gp = (GPerson*)commonRes->GParentObj->Items[j];
				if (mp->getGParentName().Compare(gp->GetName()) == 0)
				{
					found = true;
					break;
				}
			}
			if (found)
			{
				UnicodeString str;
				__int32 count;
			/* if(k == 0) */
				{
					TState *state;
					found = false;
					if (mp->State_Name.IsEmpty())
					{
						continue;
					}
					for (j = 0; j < commonRes->states->Count; j++)
					{
						state = (TState*)commonRes->states->Items[j];
						if (mp->State_Name.Compare(state->name) == 0)
						{
							found = true;
							break;
						}
					}
					if (!found)
					{
						continue;
					}
					count = 0;
					TAnimation *animation = gp->GetAnimation(state->animation);
					if (animation != NULL)
					{
						count = animation->GetFrameCount();
					}
				}
				if (count == 0)
				{
					if (!null_animation)
					{
						null_animation = true;
						str_list->Add(Localization::Text(_T("mCurrentStateEmptyAnimationError")) + _T(": "));
					}
					str = mp->Name;
					if (k == 0)
						str = str + _T(" (") + mp->State_Name + _T(")");
					str_list->Add(str);
				}
			}
		}
	}

  //check for out of map bounds objects
	{
		TRect r;
		r.Top = 0;
		r.Left = 0;
		r.Bottom = mp_h * Form_m->getBG_H();
		r.Right = mp_w * Form_m->getBG_W();
		lst = new TList();
		CreateListObjOutOfBoundsMap(&r, lst);
		if (lst->Count > 0)
		{
			str_list->Add(Localization::Text(_T("mOutOfMapBoundsObjListWarning")) + _T(": "));
			for (i = 0; i < lst->Count; i++)
			{
				mp = (TMapGPerson*)lst->Items[i];
				str_list->Add(mp->Name);
			}
		}
		delete lst;
	}
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::GetObjTypeCount(TObjType type) const
{
	return arrays[type]->Count;
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::GetTotalMapObjectsCount() const
{
	__int32 b_count = GetObjTypeCount(BACK_DECORATION);
	__int32 f_count = GetObjTypeCount(FORE_DECORATION);
	__int32 o_real_count = b_count + f_count + GetTotalMapCharacterObjectsCount();
	return o_real_count;
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::GetTotalMapCharacterObjectsCount() const
{
	__int32 o_count = GetObjTypeCount(CHARACTER_OBJECT);
	__int32 o_real_count = o_count;
	for (__int32 j = 0; j < o_count; j++)
	{
		const TMapGPerson *gp = (TMapGPerson*)GetObjByIndex(CHARACTER_OBJECT, j);
		if(gp->GetObjType() == objMAPOBJPOOL)
		{
			o_real_count--;
			const TMapObjPool *op = (TMapObjPool*)gp;
			o_real_count += op->GetTolalObjCount();
		}
	}
	UnicodeString name = UnicodeString(SOUNDSCHEME_NAME);
	if(FindObjectPointer(name) != NULL)
	{
		o_real_count--;
	}
	return o_real_count;
}
//---------------------------------------------------------------------------

void* __fastcall TObjManager::GetObjByIndex(TObjType type, __int32 idx) const
{
	assert(arrays[type]->Count >= idx);
	return arrays[type]->Items[idx];
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::GetScriptsCount() const
{
	return scripts->Count;
}
//---------------------------------------------------------------------------

TGScript* __fastcall TObjManager::GetScriptByIndex(__int32 idx) const
{
	assert(scripts->Count >= idx);
	return static_cast<TGScript*>(scripts->Items[idx]);
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::SortAllGlobalScriptsByOrder()
{
	/*
	for (__int32 m = 0; m < GetScriptsCount(); m++)
	{
		TGScript *spt0 = GetScriptByIndex(m);
		if (spt0->Zone == LEVELS_INDEPENDENT_IDX)
		{
			if (m - 1 >= 0)
			{
				TGScript *spt = GetScriptByIndex(m - 1);
				if (spt->Zone != LEVELS_INDEPENDENT_IDX)
				{
					scripts->Move(m, 0);
				}
			}
		}
	}
    */
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::SychronizeGlobalScripts(const TObjManager *etalon)
{
	/*
	assert(etalon);
	for (__int32 j = 0; j < etalon->GetScriptsCount(); j++)
	{
		TGScript *spt0 = etalon->GetScriptByIndex(j);
		if (spt0->Zone == LEVELS_INDEPENDENT_IDX)
		{
			__int32 i;
			TGScript *spt;
			for (i = 0; i < GetScriptsCount(); i++)
			{
				spt = GetScriptByIndex(i);
				if (spt->Name.Compare(spt0->Name) == 0)
				{
					if(DeleteScript(spt))
					{
						i--;
					}
				}
			}
			//��������� ���������� �������
			spt = new TGScript();
			spt0->CopyTo(spt);
			scripts->Insert(j, spt);
		}
	}
    */
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::SortAllGlobalTriggersByOrder()
{
   //������������� ��� �������� � ������ ������ (��� ������������� �� ������� ��������)
	for (__int32 m = 0; m < GetObjTypeCount(TRIGGER); m++)
	{
		TMapSpGPerson *tr0 = (TMapSpGPerson*)GetObjByIndex(TRIGGER, m);
		if (tr0->GetZone() == LEVELS_INDEPENDENT_IDX)
		{
			if (m - 1 >= 0)
			{
				TMapSpGPerson *tr = (TMapSpGPerson*)GetObjByIndex(TRIGGER, m - 1);
				if (tr->GetZone() != LEVELS_INDEPENDENT_IDX)
				{
					triggers->Move(m, 0);
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::SychronizeGlobalTriggers(const TObjManager *etalon, TStringList *errorList, __int32 x0, __int32 y0)
{
	__int32 j;
	assert(etalon);
	assert(errorList);
	TStringList *coordinates = new TStringList();
	for (j = 0; j < GetObjTypeCount(TRIGGER); j++)
	{
		TMapSpGPerson *tr = (TMapSpGPerson*)GetObjByIndex(TRIGGER, j);
		if (tr->GetZone() == LEVELS_INDEPENDENT_IDX)
		{
			double dx, dy;
			TRect *point = new TRect();
			tr->getCoordinates(&dx, &dy);
			point->left = (__int32)dx;
			point->top = (__int32)dy;
			coordinates->AddObject(tr->Name, (TObject*)point);
			if(DeleteObject(tr))
			{
				j--;
            }
		}
	}

	for (j = 0; j < etalon->GetObjTypeCount(TRIGGER); j++)
	{
		TMapSpGPerson *tr0 = (TMapSpGPerson*)etalon->GetObjByIndex(TRIGGER, j);
		if (tr0->GetZone() == LEVELS_INDEPENDENT_IDX)
		{
			__int32 i;
			bool found = false;
			TMapSpGPerson *tr;
			for (i = 0; i < GetObjTypeCount(TRIGGER); i++)
			{
				tr = (TMapSpGPerson*)GetObjByIndex(TRIGGER ,i);
				if (tr->Name.Compare(tr0->Name) == 0)
				{
					if(DeleteObject(tr))
					{
						i--;
                    }
				}
			}
			//�������, ������� ����� ���������, ������� ��� ������ ��� ���������
			if (GetObjTypeCount(TRIGGER) >= MAX_TRIGGER_ON_MAP)
			{
				if(errorList->IndexOf(tr0->Name) < 0)
				{
					errorList->Add(tr0->Name);
                }
				tr0->SetZone(ZONES_INDEPENDENT_IDX);
				continue;
			}
			//��������� ���������� ��������
			tr = new TMapTrigger(this);
			tr0->CopyTo(tr);
			for (i = 0; i < coordinates->Count; i++)
			{
				TRect *point = (TRect*)coordinates->Objects[i];
				if (tr->Name.Compare(coordinates->Strings[i]) == 0)
				{
					tr->setCoordinates(point->Left, point->Top, 0, 0);
					found = true;
				}
			}
			if (!found)
			{
				tr->setCoordinates(x0, y0, 0, 0);
			}
			triggers->Insert(j, tr);
		}
	}

	for (j = 0; j < coordinates->Count; j++)
	{
		delete(TRect*)coordinates->Objects[j];
	}
	delete coordinates;
}
//---------------------------------------------------------------------------

//PATTERN

//---------------------------------------------------------------------------

void __fastcall TObjManager::AssignMainPattern(TObjPattern * /* p */ )
{
	__int32 i;

	commonRes->AssignMainPattern(commonRes->MainGameObjPattern);

	for (i = 0; i < map_u->Count; i++)
		((TMapGPerson*)map_u->Items[i])->GetProperties().assignPattern(commonRes->MainGameObjPattern);
	for (i = 0; i < decor_b->Count; i++)
		((TMapGPerson*)decor_b->Items[i])->GetProperties().assignPattern(commonRes->MainGameObjPattern);
	for (i = 0; i < decor_f->Count; i++)
		((TMapGPerson*)decor_f->Items[i])->GetProperties().assignPattern(commonRes->MainGameObjPattern);
	for (i = 0; i < triggers->Count; i++)
		((TMapTrigger*)triggers->Items[i])->GetProperties().assignPattern(commonRes->MainGameObjPattern);
	for (i = 0; i < directors->Count; i++)
		((TMapDirector*)directors->Items[i])->GetProperties().assignPattern(commonRes->MainGameObjPattern);
	ClearCashe();
}
//---------------------------------------------------------------------------

TMapPerson* __fastcall TObjManager::FindObjectPointer(const UnicodeString& name) const
{
	TList *lst;
	if(name.IsEmpty())
	{
        return NULL;
    }
	for (__int32 k = 0; k < 7; k++)
	{
		switch(k)
		{
			case 0:
				lst = triggers;
				break;
			case 1:
				lst = directors;
				break;
			case 2:
				lst = decor_b;
				break;
			case 3:
				lst = map_u;
				break;
			case 4:
				lst = decor_f;
				break;
			case 5:
				lst = collideshapes;
				break;
			case 6:
				{
					const __int32 len = collideshapes->Count;
					for (__int32 i = 0; i < len; i++)
					{
						TCollideChainShape *o = static_cast<TCollideChainShape *>(collideshapes->Items[i]);
						TMapCollideLineNode* cn = o->FindNode(name);
						if(cn != NULL)
						{
                        	return cn;
						}
					}
				}
				return NULL;
		}
		const __int32 len = lst->Count;
		for (__int32 i = 0; i < len; i++)
		{
			TMapPerson *go = static_cast<TMapPerson *>(lst->Items[i]);
			if (name.Compare(go->Name) == 0)
			{
				return go;
			}
		}
	}
	return NULL;
}
//---------------------------------------------------------------------------

void *__fastcall TObjManager::FindObjectPointer(__int32 X, __int32 Y, __int32 &dx, __int32 &dy, bool useParentIntersection)
{
	__int32 i, k, len;
	TList *lst;
	TMapPerson *p;
	for (k = 0; k < 6; k++)
	{
		switch(k)
		{
			case 1:
				lst = directors;
				break;
			case 0:
				lst = triggers;
				break;
			case 2:
				lst = decor_f;
				break;
			case 3:
				lst = map_u;
				break;
			case 4:
				lst = decor_b;
				break;
			case 5:
				lst = GetCollideNodesList();
				break;
		}
		len = lst->Count - 1;
		for (i = len; i >= 0; i--)
		{
			p = (TMapPerson*)lst->Items[i];
			if (p->CustomVisible && p->Visible)
			{
				if(p->Intersect(X, Y, X, Y))
				{
					double sx, sy;
					if(useParentIntersection &&
						(p->GetObjType() == objMAPTEXTBOX ||
							p->GetObjType() == objMAPSOUNDSCHEME ||
							p->GetObjType() == objMAPOBJPOOL))
					{
						TMapGPerson *gp = (TMapGPerson*)p;
						if(!gp->ParentObj.IsEmpty())
						{
							TMapGPerson *pgp = (TMapGPerson*)FindObjectPointer(gp->ParentObj);
							if(pgp != NULL && pgp->Intersect(X, Y, X, Y))
							{
								p = pgp;
							}
						}
					}
					p->getScreenCoordinates(&sx, &sy);
					dx = X - (__int32)sx;
					dy = Y - (__int32)sy;
					return p;
				}
			}
		}
	}
	dx = dy = 0;
	return NULL;
}
//---------------------------------------------------------------------------

void *__fastcall TObjManager::FindObjectPointers(TList *List, const TRect &SelectionRect)
{
	__int32 i, k, len;
	TList *lst;
	void *first = NULL;
	TMapPerson *p;
	for (k = 0; k < 6; k++)
	{
		switch(k)
		{
			case 1:
				lst = directors;
				break;
			case 0:
				lst = triggers;
				break;
			case 2:
				lst = decor_b;
				break;
			case 3:
				lst = map_u;
				break;
			case 4:
				lst = decor_f;
				break;
			case 5:
				lst = GetCollideNodesList();
				break;
		}
		len = lst->Count - 1;
		for (i = len; i >= 0; i--)
		{
			p = (TMapPerson*)lst->Items[i];
			if (p->CustomVisible && p->Visible)
			{
				if(p->Intersect(SelectionRect.Left, SelectionRect.Top, SelectionRect.Right, SelectionRect.Bottom))
				{
					if (List->IndexOf(p) < 0)
					{
						List->Add(p);
						first = p;
					}
				}
			}
		}
	}
	return first;
}
//---------------------------------------------------------------------------

//RENDER

//---------------------------------------------------------------------------

TMapPersonCasheData *__fastcall TObjManager::GetCashe(__int32 idx)
{
	if (idx < 0 || idx >= m_pCashe->Count)
	{
		return NULL;
    }
	return (TMapPersonCasheData*)m_pCashe->Items[idx];
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::DelCashe(__int32 idx)
{
	if (idx < 0 || idx >= m_pCashe->Count)
	{
		return;
	}
	if (m_pCashe->Items[idx] != NULL)
	{
		TMapPersonCasheData *cashe = (TMapPersonCasheData*)m_pCashe->Items[idx];
		if(cashe->m_DynamicObjFrameHash != 0)
		{
			commonRes->DeleteBitmapFrame(cashe->m_DynamicObjFrameHash);
		}
		if(!cashe->m_DynamicObjResId.IsEmpty())
		{
			commonRes->DeleteGraphicResource(cashe->m_DynamicObjResId);
		}
		if(!cashe->m_Err_bmp_name.IsEmpty())
		{
			commonRes->DeleteGraphicResource(cashe->m_Err_bmp_name);
		}
		delete cashe;
	}
	m_pCashe->Items[idx] = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::ClearCashe()
{
	__int32 k, i;
	TList *lst;
	for (k = 0; k < 5; k++)
	{
		switch(k)
		{
		case 0:
			lst = decor_b;
			break;
		case 1:
			lst = map_u;
			break;
		case 2:
			lst = decor_f;
			break;
		case 3:
			lst = triggers;
			break;
		case 4:
			lst = directors;
		}
		for (i = 0; i < lst->Count; i++)
		{
			((TMapPerson*)lst->Items[i])->CasheDataIndex = -1;
		}
	}
	k = 0;
	for (; k < m_pCashe->Count; k++)
	{
		if (m_pCashe->Items[k] != NULL)
		{
			TMapPersonCasheData *cashe = (TMapPersonCasheData*)m_pCashe->Items[k];
			if(cashe->m_DynamicObjFrameHash != 0)
			{
				commonRes->DeleteBitmapFrame(cashe->m_DynamicObjFrameHash);
			}
			if(!cashe->m_DynamicObjResId.IsEmpty())
			{
				commonRes->DeleteGraphicResource(cashe->m_DynamicObjResId);
			}
			if(!cashe->m_Err_bmp_name.IsEmpty())
			{
				commonRes->DeleteGraphicResource(cashe->m_Err_bmp_name);
			}
			delete cashe;
		}
	}
	m_pCashe->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::ClearCasheFor(TMapPerson *pr)
{
	if (pr == NULL)
		return;
	DelCashe(pr->CasheDataIndex);
	pr->CasheDataIndex = -1;
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::AddCashe(TMapPersonCasheData *c)
{
	__int32 i;
	for (i = 0; i < m_pCashe->Count; i++)
		if (m_pCashe->Items[i] == NULL)
		{
			m_pCashe->Items[i] = (void*)c;
			return i;
		}
	m_pCashe->Add((void*)c);
	return i;
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::CreateCashe(TMapPerson *pr)
{
	__int32 i, err;
	bool found;
	UnicodeString animName;
	TMapPersonCasheData *cashe;

	if (pr->GetObjType() == objMAPCOLLIDENODE ||
			pr->GetObjType() == objMAPCOLLIDESHAPE)
	{
		return;
	}

	TMapGPerson *gpr = static_cast<TMapGPerson*>(pr);

	cashe = new TMapPersonCasheData();
	DelCashe(pr->CasheDataIndex);
	err = ERR_NONE;

	animName = _T("");
	switch(pr->GetObjType())
	{
	  case objMAPOBJ:
		cashe->m_stateName = gpr->State_Name;
		if (!gpr->State_Name.IsEmpty())
		{
			for (i = 0; i < commonRes->states->Count; i++)
			{
				UnicodeString stname = ((TState*)commonRes->states->Items[i])->name;
				if (gpr->State_Name.Compare(stname) == 0)
				{
					cashe->m_pState = (TState*)commonRes->states->Items[i];
					animName = cashe->m_pState->animation;
					break;
				}
			}
		}
		break;
		case objMAPTEXTBOX:
			{
				TMapTextBox *txpr = (TMapTextBox *)pr;
				if(txpr->GetTextWidth() <= 0 || txpr->GetTextHeight() <= 0)
				{
					animName = UnicodeString(ANIM_TEXTBOX_ICON_NAME);
				}
				else
				{
					animName = UnicodeString(ANIM_TEXTBOX_TEXT_NAME);
                }
			}
		break;
		case objMAPSOUNDSCHEME:
			{
				animName = UnicodeString(ANIM_SOUNDSCHEME_NAME);
			}
		break;
		case objMAPOBJPOOL:
			{
				animName = UnicodeString(ANIM_OBJPOOL_NAME);
			}
		break;
		case objMAPTRIGGER:
			if(pr->GetZone() != LEVELS_INDEPENDENT_IDX)
			{
				animName = UnicodeString(ANIM_TRIGGER_R_NAME);
			}
			else
			{
				animName = UnicodeString(ANIM_TRIGGER_G_NAME);
            }
		break;
		case objMAPDIRECTOR:
			animName = UnicodeString(ANIM_DIRECTOR_NAME);
		break;
		default:
		break;
	}

	if (pr->GetObjType() != objMAPCOLLIDENODE)
	{
		found = false;
		for (i = 0; i < commonRes->GParentObj->Count; i++)
			if (pr->getGParentName().Compare(((GPerson*)commonRes->GParentObj->Items[i])->GetName()) == 0)
			{
				cashe->m_pParentRes = (GPerson*)commonRes->GParentObj->Items[i];
				found = true;
				break;
			}
		if (!found)
		{
			if (err == ERR_NONE)
				err = ERR_PARENT;
			else
				err = ERR_STATE_PARENT;
		}
	}

	if (pr->GetObjType() == objMAPOBJ ||
			pr->GetObjType() == objMAPTEXTBOX ||
			pr->GetObjType() == objMAPSOUNDSCHEME ||
			pr->GetObjType() == objMAPOBJPOOL)
	{
		if (!gpr->ParentObj.IsEmpty())
		{
			cashe->m_pParentLinkObj = (TMapGPerson*)FindObjectPointer(gpr->ParentObj);
			if (cashe->m_pParentLinkObj == NULL) // copy-paste or delete parent case
			{
                assert(0);
				gpr->ParentObj = _T("");
			}
		}

		if (gpr->IsChildExist())
		{
			for (i = 0; i < MAX_ANIMATON_JOIN_NODES; i++)
			{
                UnicodeString chname = gpr->GetChild(i);
				if(!chname.IsEmpty())
				{
					const TMapGPerson *ptr = static_cast<TMapGPerson*>(FindObjectPointer(chname));
					if (ptr != NULL)
					{
						cashe->m_pChildsLinkObj[i] = ptr;

					}
					else // copy-paste case
					{
						//assert(0);
						gpr->AddChild(i, UnicodeString());
					}
				}
			}
		}
	}

	if (pr->GetObjType() == objMAPDIRECTOR)
	{
		cashe->directors[0] = (TMapDirector*)FindObjectPointer(((TMapDirector*)pr)->nearNodes[0]);
		cashe->directors[1] = (TMapDirector*)FindObjectPointer(((TMapDirector*)pr)->nearNodes[1]);
		cashe->directors[2] = (TMapDirector*)FindObjectPointer(((TMapDirector*)pr)->nearNodes[2]);
		cashe->directors[3] = (TMapDirector*)FindObjectPointer(((TMapDirector*)pr)->nearNodes[3]);
	}

	if (err != ERR_PARENT && err != ERR_STATE_PARENT)
	{
		bool delAnim = false;
		const TAnimation *anim = NULL;
		if (!animName.IsEmpty())
		{
			anim = cashe->m_pParentRes->GetAnimation(animName);

			if(anim == NULL && pr->GetObjType() == objMAPTEXTBOX)
			{
				if(animName.Compare(UnicodeString(ANIM_TEXTBOX_TEXT_NAME)) == 0)
				{
					hash_t hashFrameArr[1];
					__int32 animArr[1];
					TMapTextBox* tb = static_cast<TMapTextBox*>(gpr);
					Graphics::TBitmap *gbmp = new Graphics::TBitmap();
                    gbmp->PixelFormat = pf32bit;
					BMPImage *tbmp = new BMPImage(gbmp);
					RenderTextToImage(tb, tbmp);
					TFrameObj b;
					b.w = (unsigned short)tbmp->Width;
					b.h = (unsigned short)tbmp->Height;
					b.x = b.y = 0;
					UnicodeString name = _T("?textboxobj") + IntToStr(reinterpret_cast<NativeInt>(gpr));
					__int32 gr_idx = commonRes->GetGraphicResourceIndex(name);
					if(gr_idx > 0)
					{
						commonRes->ReplaceGraphicResource(tbmp, gr_idx);
					}
					else
					{
						gr_idx = commonRes->AddGraphicResource(tbmp, name);
					}
					delete tbmp;
					b.setSourceRes(b.w, b.h, gr_idx, name);
					hash_t h = commonRes->AddBitmapFrame(b);
					hashFrameArr[0] = h;
					animArr[0] = 0;
					anim = commonRes->CreateSimplyAnimation(UnicodeString(ANIM_TEXTBOX_TEXT_NAME), hashFrameArr, 1, animArr, 1, alCENTER);
					assert(anim);
					cashe->m_DynamicObjFrameHash = h;
					cashe->m_DynamicObjResId = name;
					delAnim = true;
					Form_m->LoadTexturesToVRAMGL();
				}
			}

			if(anim != NULL && anim->HasStreamObject())
			{
				unsigned short w, h;
				w = anim->GetStreamObjectWidth();
				h = anim->GetStreamObjectHeight();
				Graphics::TBitmap *gbmp = new Graphics::TBitmap();
				gbmp->PixelFormat = pf32bit;
				BMPImage *tbmp = new BMPImage(gbmp);
				TFrameObj b;
				b.w = w;
				b.h = h;
				b.x = b.y = 0;
				UnicodeString name = _T("?strmobj") + pr->Name;
				__int32 gr_idx = commonRes->GetGraphicResourceIndex(name);
				if(gr_idx > 0)
				{
					commonRes->ReplaceGraphicResource(tbmp, gr_idx);
				}
				else
				{
					gr_idx = commonRes->AddGraphicResource(tbmp, name);
				}
				delete tbmp;
				b.setSourceRes(b.w, b.h, gr_idx, name);
				hash_t hash = commonRes->AddBitmapFrame(b);
				cashe->m_DynamicObjFrameHash = hash;
				cashe->m_DynamicObjResId = name;
				Form_m->LoadTexturesToVRAMGL();
			}
		}
		pr->CasheDataIndex = AddCashe(cashe);
		gpr->AssignAnimation(anim);
		if(delAnim)
		{
			delete anim;
        }

		if (pr->GetObjType() == objMAPOBJ)
		{
			gpr->InspectEndAnimation = true;
		}
		pr->OnScrollBarChange();
	}
	else
	{
		pr->CasheDataIndex = err;
		delete cashe;
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::RenderTextToImage(const TMapTextBox* tb, BMPImage *bmp)
{
	enum TMapTexBoxError
	{
		MTE_None = 0,
		MTE_WrongTextId,
		MTE_WrongFont
	};
	TMapTexBoxError err;
	::Text renderText;
    const TUnicodeText *ftext;
	const TImageFont *font;
	err = MTE_None;
	::InitText(&renderText);
	renderText.mpCanvas = bmp;
	font = Form_m->GetCommonTextManager()->GetFont(tb->GetFontName());
	renderText.mAttributes = tb->mAttributes;
	if(tb->GetTextType() != TMapTextBox::MTBT_Dynamic)
	{
		ftext = Form_m->GetCommonTextManager()->GetText(tb->GetTextId(), Form_m->GetCurrentLanguage());
	}
	else
	{
		ftext = NULL;
	}
	if((ftext != NULL || tb->GetTextType() == TMapTextBox::MTBT_Dynamic) && font)
	{
		renderText.mpFont = font->GetData();
		if(tb->GetTextType() != TMapTextBox::MTBT_Dynamic)
		{
			if(ftext->GetTextData() != NULL)
			{
				renderText.mppStrings = (wchar_t**)MALLOC(ftext->GetTextData()->mStringsCount * sizeof(wchar_t*), "foo");
				for(__int32 j = 0; j < ftext->GetTextData()->mStringsCount; j++)
				{
					const wchar_t *a = ftext->GetTextData()->mppStrings[j];
					__int32 len = wcslen(a);
					renderText.mppStrings[j] = (wchar_t*)MALLOC(sizeof(wchar_t) * (len + 1), "foo");
					wcscpy(renderText.mppStrings[j], a);
					renderText.mppStrings[j][len] = L'\0';
					renderText.mStringsCount++;
					renderText.mStringsCountMax++;
				}
			}
		}
		::SetTextCanvas((u16)tb->GetTextWidth(), (u16)tb->GetTextHeight(), &renderText);
	}
	else
	{
		err = MTE_WrongTextId;
		bmp->SetSize(tb->GetTextWidth(), tb->GetTextHeight());
	}
	if(font == NULL)
	{
		err = MTE_WrongFont;
	}
	switch(err)
	{
		case MTE_WrongTextId:
		{
			unsigned char r, g, b;
			TColor color = renderText.mAttributes.mCanvasColor;
			r = (unsigned char)(((__int32)color) >> 16);
			g = (unsigned char)(((__int32)color) >> 8);
			b = (unsigned char)color;
			bmp->Canvas->Brush->Color = TColor(RGB(b, g, r));
			bmp->Canvas->FillRect(TRect(0, 0, tb->GetTextWidth(), tb->GetTextHeight()));
			bmp->Canvas->Font->Size = 6;
			bmp->Canvas->Font->Name = _T("Small Fonts");
			bmp->Canvas->Font->Color = clRed;
			bmp->Canvas->TextOut(1, 2, _T("Error: text ID not found "));
			if(renderText.mAttributes.mCanvasColor == clTRANSPARENT)
			{
				bmp->ResetAlpha();
				bmp->FillColorWithTransparent(clTRANSPARENT);
			}
			else
			{
				bmp->ResetAlpha();
			}
		}
		break;
		case MTE_WrongFont:
		{
        	unsigned char r, g, b;
			TColor color = renderText.mAttributes.mCanvasColor;
			r = (unsigned char)(((__int32)color) >> 16);
			g = (unsigned char)(((__int32)color) >> 8);
			b = (unsigned char)color;
			bmp->Canvas->Brush->Color = TColor(RGB(b, g, r));
			bmp->Canvas->FillRect(TRect(0, 0, tb->GetTextWidth(), tb->GetTextHeight()));
			bmp->Canvas->Font->Size = 6;
			bmp->Canvas->Font->Name = _T("Small Fonts");
			bmp->Canvas->Font->Color = clRed;
			bmp->Canvas->TextOut(1, 2, _T("Error: font '") + tb->GetFontName() + _T("' not found "));
			if(renderText.mAttributes.mCanvasColor == clTRANSPARENT)
			{
				bmp->ResetAlpha();
				bmp->FillColorWithTransparent(clTRANSPARENT);
			}
			else
			{
				bmp->ResetAlpha();
			}
		}
		break;
		default:
		break;
	}
	if(err == MTE_None && tb->GetTextType() == TMapTextBox::MTBT_Dynamic)
	{
        unsigned char r, g, b;
		TColor color = renderText.mAttributes.mCanvasColor;
		r = (unsigned char)(((__int32)color) >> 16);
		g = (unsigned char)(((__int32)color) >> 8);
		b = (unsigned char)color;
		bmp->Canvas->Brush->Color = TColor(RGB(b, g, r));
		bmp->Canvas->FillRect(TRect(0, 0, tb->GetTextWidth(), tb->GetTextHeight()));
		bmp->Canvas->Font->Size = 6;
		bmp->Canvas->Font->Name = _T("Small Fonts");
		bmp->Canvas->Font->Color = clGreen;
		bmp->Canvas->TextOut(1, 2, _T("Dynamic text "));
		if(renderText.mAttributes.mCanvasColor == clTRANSPARENT)
		{
			bmp->ResetAlpha();
			bmp->FillColorWithTransparent(clTRANSPARENT);
		}
		else
		{
			bmp->ResetAlpha();
		}
	}
	::ReleaseText(&renderText);
}
//---------------------------------------------------------------------------

//�������� � ��������� ����, ������������� ������ ������� ������� �������
bool __fastcall TObjManager::CheckForErrors(TMapGPerson *pr)
{
	if(pr->CasheDataIndex < 0)
	{
		if(pr->GetObjType() != objMAPCOLLIDENODE)
		{
			hash_t ha[1];
			__int32 aa[1];
			pr->ShowRect(false);
			ha[0] = 0;
			aa[0] = 0;
			TAnimation *errAnimation =
				commonRes->CreateSimplyAnimation(UnicodeString(ERR_ANIM_NAME), ha, 1, aa, 1, alCENTER);
			pr->AssignAnimation(errAnimation);
		}
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::RebuildCasheFor(TMapPerson* pr)
{
	GetPrCashe(pr);
}
//---------------------------------------------------------------------------

TMapPersonCasheData *__fastcall TObjManager::GetPrCashe(TMapPerson *pr)
{
	UnicodeString parentMame;
	TMapPersonCasheData *cashe;
	bool recreate_cashe;

	assert(pr);

	if (pr->GetObjType() == objMAPCOLLIDENODE || pr->GetObjType() == objMAPCOLLIDESHAPE)
	{
		return NULL;
	}

	TMapGPerson *gpr = static_cast<TMapGPerson*>(pr);

	cashe = GetCashe(pr->CasheDataIndex);
	if (cashe == NULL)
	{
		if (gpr->CasheDataIndex != -1)
			if (CheckForErrors(gpr))
				return NULL;
		CreateCashe(pr);
		if (CheckForErrors(gpr))
			return NULL;
		cashe = GetCashe(pr->CasheDataIndex);
	}

	parentMame = pr->getGParentName();
	recreate_cashe = (cashe == NULL);

	if (pr->GetObjType() != objMAPCOLLIDENODE)
	{
		//�������� �� ���������� ����� � ���������
		if (!recreate_cashe)
		{
			recreate_cashe = (parentMame.Compare(cashe->m_pParentRes->GetName()) != 0);
		}
	}

	//�������� �� ���������� ����� � ����������
	if (pr->GetObjType() == objMAPOBJ)
	{
		if (!recreate_cashe)
		{
			if (pr->GetObjType() == objMAPOBJ)
				recreate_cashe = (gpr->State_Name.Compare(cashe->m_stateName) != 0);
		}
	}

	if (recreate_cashe)
	{
		CreateCashe(pr);
		if (CheckForErrors(gpr))
			return NULL;
		cashe = GetCashe(pr->CasheDataIndex);
		parentMame = pr->getGParentName();
	}

	return cashe;
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::ChangeChildPosition(TMapGPerson *pr)
{
	TMapPersonCasheData *cashe;
	const TMapGPerson *opr;
	__int32 i, len;
	if(pr->GetAnimation() == NULL)
	{
    	return;
	}
	cashe = GetPrCashe(pr);
	if (cashe == NULL)
	{
		return;
	}
	const TAnimationFrame* f = pr->GetCurrentAnimationFrame();
	for (i = 0; i < MAX_ANIMATON_JOIN_NODES; i++)
	{
		opr = cashe->m_pChildsLinkObj[i];
		if(opr != NULL)
		{
			double x, y, nx, ny;
			if(f == NULL)
			{
				nx = ny = 0.0f;
			}
			else
			{
				TPoint pt;
				f->GetJoinNodeCoordinate(i, pt);
				nx = static_cast<double>(pt.x);
				ny = static_cast<double>(pt.y);
			}
			pr->getCoordinates(&x, &y);
			const_cast<TMapGPerson*>(opr)->DoSetCoordinates(x + nx, y + ny, 0, 0);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::CreateErrStateImage(TMapGPerson *mo, TMapPersonCasheData *cashe)
{
	TRect r;
	Graphics::TBitmap *bmp;
	if(cashe->m_Err_bmp_idx >= 0)
	{
		const UnicodeString str = _T("?") + mo->Name;
		assert(cashe->m_Err_bmp_name.Compare(str) == 0);
	}
	BMPImage *tbmp;
	bmp = new Graphics::TBitmap();
	r.Top = 0;
	r.Left = 0;
	r.Bottom = 18;
	r.Right = ERROR_IMAGE_WIDTH;
	if(mo->Width > r.Right)
	{
		r.Right = mo->Width;
	}
	if(mo->Height > r.Bottom)
	{
		r.Bottom = mo->Height;
	}
	bmp->SetSize(r.Width(), r.Height());
	bmp->Canvas->Brush->Color = clRed;
	bmp->Canvas->FillRect(r);
	bmp->Canvas->Brush->Color = clGray;
	bmp->Canvas->Brush->Style = bsDiagCross;
	bmp->Canvas->Pen->Color = clYellow;
	bmp->Canvas->Rectangle(r);
	bmp->Canvas->Font->Size = 4;
	bmp->Canvas->Font->Name = _T("Small Fonts");
	bmp->Canvas->Font->Color = clWhite;
	bmp->Canvas->TextOut(1, 2, mo->Name);
	bmp->Canvas->TextOut(1, 10, mo->State_Name);
	mo->ShowRect(false);
	tbmp = new BMPImage(bmp);
	tbmp->ResetAlpha();
	if(cashe->m_Err_bmp_idx < 0)
	{
		cashe->m_Err_bmp_name = _T("?") + mo->Name;
		cashe->m_Err_bmp_idx = commonRes->AddGraphicResource(tbmp, cashe->m_Err_bmp_name);
	}
	else
	{
		commonRes->ReplaceGraphicResource(tbmp, cashe->m_Err_bmp_idx);
	}
	delete tbmp;
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::DrawMapObject(TMapGPerson *mo, TCanvas *canvas)
{
	TMapPersonCasheData *cashe;
	const TFrameObj *fobj;
	const BMPImage *bmp;
	std::list<TAnimationFrameData> rlist;
	bool res;
	double sx, sy;
	double sh, dh;
	double sw, dw;
	double offScale;

	cashe = GetPrCashe(mo);
	if (cashe == NULL)
	{
		return;
	}

	rlist = mo->GetAnimationFrameData();
	if(!rlist.empty())
	{
		double x, y;
		bool fbreak;
		std::list<TAnimationFrameData>::const_iterator fdata_iter;
		for(fdata_iter = rlist.begin(); fdata_iter != rlist.end(); ++fdata_iter)
		{
			res = false;
			const TAnimationFrameData& afd = *fdata_iter;

			hash_t hash = afd.frameId;
			if(afd.isStreamFrame())
			{
				hash = cashe->m_DynamicObjFrameHash;
			}

			mo->getCoordinates(&x, &y);

			if(hash != 0)
			{
				fobj = commonRes->GetBitmapFrame(hash);
				if(fobj == NULL)
				{
					assert(fobj);
					continue;
                }

				if (fobj->getResIdx() >= 0)
				{
					bmp = commonRes->GetGraphicResource(fobj->getResIdx());
					sx = fobj->x;
					sy = fobj->y;
					sw = fobj->w;
					sh = fobj->h;
					res = bmp != NULL;
				}

				if(afd.isStreamFrame())
				{
					if(cashe->m_DynamicObjTempFilename.Compare(afd.strm_filename) != 0)
					{
						res = false;
						bmp = NULL;
						cashe->m_DynamicObjTempFilename = afd.strm_filename;
						UnicodeString path = Form_m->PathPngRes + afd.strm_filename;
						if(FileExists(path))
						{
							Graphics::TBitmap *tbmp = new Graphics::TBitmap();
							if(read_png(path.c_str(), tbmp))
							{
								if(tbmp->Width != sw || tbmp->Height != sh)
								{
									delete tbmp;
								}
								else
								{
									BMPImage *tbmp2d = new BMPImage(tbmp);
									commonRes->ReplaceGraphicResource(tbmp2d, fobj->getResIdx());
									delete tbmp2d;
									bmp = commonRes->GetGraphicResource(fobj->getResIdx());
									assert(bmp);
									res = true;
								}
							}
							else
							{
								delete tbmp;
							}
						}
					}
				}
			}

			if (!res)
			{
				CreateErrStateImage(mo, cashe);
				bmp = commonRes->GetGraphicResource(cashe->m_Err_bmp_idx);
				assert(bmp);
				sh = bmp->Height;
				sw = bmp->Width;
				sx = 0.0f;
				sy = 0.0f;
			}

			if (mo->mFixedSizes)
			{
				offScale = 1.0f;
			}
			else
			{
				offScale = gsGlobalScale;
			}
			dw = (double)afd.rw * offScale;
			dh = (double)afd.rh * offScale;

			if (gUsingOpenGL)
			{
				float tr = (1.0f * (float)afd.a) / 255.0f;
				__int32 tp = mo->GetObjType();
				if (mo->mFixedSizes == false && (tp == objMAPOBJ || tp == objMAPTEXTBOX))
				{
					if (mo->IsInterlacedTransparency())
					{
						tr = (GAMEOBJ_INNACTIVE_TRANSPARENCY_GL * (float)afd.a) / 255.0f;
					}
					x = (afd.posX + x) * gsGlobalScale - (double)*mo->off_x;
					y = (afd.posY + y) * gsGlobalScale - (double)*mo->off_y;
				}
				else
				{
					tr = SYSTEMOBJ_TRANSPARENCY_GL;
					if (mo->IsInterlacedTransparency())
					{
						tr = GAMEOBJ_INNACTIVE_TRANSPARENCY_GL;
					}
					x = (afd.posX + x) * gsGlobalScale + ((double)sw * gsGlobalScale - (double)dw) / 2.0f - (double)*mo->off_x - (gsGlobalScale + 1.0f) / 2.0f;
					y = (afd.posY + y) * gsGlobalScale + ((double)sh * gsGlobalScale - (double)dh) - (double)*mo->off_y;
				}

				if(res)
				{
					TRenderGL::RenderImage(fobj->getResIdx(),
										DIT_Obj,
										(float)sx,
										(float)sy,
										(float)sw,
										(float)sh,
										(float)x,
										(float)y,
										(float)dw,
										(float)dh,
										tr);
				}
				else //error
				{
					TRenderGL::RenderImage(cashe->m_Err_bmp_idx,
										DIT_Obj,
										(float)sx,
										(float)sy,
										(float)sw,
										(float)sh,
										(float)x,
										(float)y,
										(float)dw,
										(float)dh,
										tr);
				}
			}
			else
			{
				BLENDFUNCTION bf;
				bf.BlendOp = AC_SRC_OVER;
				bf.BlendFlags = 0;
				bf.SourceConstantAlpha = afd.a;
				bf.AlphaFormat = AC_SRC_ALPHA;
				__int32 tp = mo->GetObjType();
				if (tp == objMAPOBJ || tp == objMAPTEXTBOX)
				{
					if (mo->IsInterlacedTransparency())
					{
						bf.SourceConstantAlpha = (GAMEOBJ_INNACTIVE_TRANSPARENCY_GDI * afd.a) / 255;
					}
					x = (afd.posX + x) * gsGlobalScale - (double)*mo->off_x;
					y = (afd.posY + y) * gsGlobalScale - (double)*mo->off_y;
				}
				else
				{
					bf.SourceConstantAlpha = SYSTEMOBJ_TRANSPARENCY_GDI;
					if (mo->IsInterlacedTransparency())
					{
						bf.SourceConstantAlpha = GAMEOBJ_INNACTIVE_TRANSPARENCY_GDI;
					}
					x = (afd.posX + x) * gsGlobalScale + ((double)sw * gsGlobalScale - (double)dw) / 2.0f - (double)*mo->off_x - (gsGlobalScale + 1.0f) / 2.0f;
					y = (afd.posY + y) * gsGlobalScale + ((double)sh * gsGlobalScale - (double)dh) - (double)*mo->off_y;
				}
				::AlphaBlend(canvas->Handle,
								(__int32)x,
								(__int32)y,
								(__int32)dw,
								(__int32)dh,
								bmp->Canvas->Handle,
								(__int32)sx,
								(__int32)sy,
								(__int32)sw,
								(__int32)sh,
								bf);
			}
			if(!res)
			{
				break;
			}
		}

		if(mo->showEnabled == false)
		{
			__int32 j;
			double dx, dy;
			double x, y, x1, y1;
			if (gUsingOpenGL)
			{
				TRenderGL::SetColor(255, 0, 0, 200);
				TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
			}
			else
			{
				canvas->Brush->Style = bsClear;
				canvas->Pen->Color = clRed;
				canvas->Pen->Style = psSolid;
				canvas->Pen->Mode = pmCopy;
			}
			mo->getScreenCoordinates(&x, &y);
			x1 = x + mo->Width;
			y1 = y + mo->Height;
			if (gUsingOpenGL)
			{
				TRenderGL::RenderLine((float)x, (float)y, (float)x1, (float)y1);
			}
			else
			{
				canvas->MoveTo((__int32)x, (__int32)y);
				canvas->LineTo((__int32)x1, (__int32)y1);
			}
			if (gUsingOpenGL)
			{
				TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
			}
		}
	}
	else //error
	{
		double x, y;
		mo->getScreenCoordinates(&x, &y);
		sh = ERROR_IMAGE_HEIGHT;
		sw = ERROR_IMAGE_WIDTH;
		dw = (__int32)((double)sw * gsGlobalScale);
		dh = (__int32)((double)sh * gsGlobalScale);
		if (gUsingOpenGL)
		{
			float tr = 1.0f;
			if (mo->GetObjType() == objMAPOBJ)
			{
				if (mo->IsInterlacedTransparency())
				{
					tr = GAMEOBJ_INNACTIVE_TRANSPARENCY_GL;
				}
			}
			else
			{
				tr = SYSTEMOBJ_TRANSPARENCY_GL;
			}

			TRenderGL::RenderImage(Form_m->GetGeneralObjErrorResIdx(),
									DIT_Obj,
									0,
									0,
									(float)sw,
									(float)sh,
									(float)x,
									(float)y,
									(float)dw,
									(float)dh,
									tr);
		}
		else
		{
			BLENDFUNCTION bf;
			bf.BlendOp = AC_SRC_OVER;
			bf.BlendFlags = 0;
			bf.SourceConstantAlpha = 255;
			bf.AlphaFormat = AC_SRC_ALPHA;

			if (mo->GetObjType() == objMAPOBJ)
			{
				if (mo->IsInterlacedTransparency())
				{
					bf.SourceConstantAlpha = GAMEOBJ_INNACTIVE_TRANSPARENCY_GDI;
				}
			}
			else
			{
				bf.SourceConstantAlpha = SYSTEMOBJ_TRANSPARENCY_GDI;
			}
			bmp = commonRes->GetGraphicResource(Form_m->GetGeneralObjErrorResIdx());
            assert(bmp);
			::AlphaBlend(canvas->Handle,
							(__int32)x,
							(__int32)y,
							(__int32)dw,
							(__int32)dh,
							bmp->Canvas->Handle,
							0,
							0,
							(__int32)sw,
							(__int32)sh,
							bf);
		}
    }
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::DrawDirectorLines(TMapDirector *ipDirector, TMapDirector *ipSelectedDirector, TCanvas *canvas, __int32 scrollbar_x, __int32 scrollbar_y)
{
	TMapPersonCasheData *cashe;
	__int32 j;
	double x, y, x1, y1;
	if (gUsingOpenGL)
	{
		TRenderGL::SetColor(0, 255, 0, 200);
		TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
		if (ipSelectedDirector != NULL)
		{
			if (ipSelectedDirector == ipDirector)
			{
				TRenderGL::SetColor(0, 255, 0, 200);
			}
			else
			{
				TRenderGL::SetColor(0, 255, 0, 100);
				TRenderGL::SetLineStyle(TRenderGL::RGLLSDot);
			}
		}
	}
	else
	{
		canvas->Brush->Style = bsClear;
		canvas->Pen->Color = clGreen;
		canvas->Pen->Style = psSolid;
		canvas->Pen->Mode = pmCopy;
		if (ipSelectedDirector != NULL)
		{
			if (ipSelectedDirector == ipDirector)
				canvas->Pen->Style = psSolid;
			else
				canvas->Pen->Style = psDot;
		}
	}
	cashe = GetPrCashe(ipDirector);
	ipDirector->getCoordinates(&x, &y);
	x *= gsGlobalScale;
	y *= gsGlobalScale;
	x -= (double)scrollbar_x;
	y -= (double)scrollbar_y;
	for (j = 0; j < 4; j++)
		if (NULL != cashe->directors[j])
		{
			cashe->directors[j]->getCoordinates(&x1, &y1);
			if (gUsingOpenGL)
			{
				TRenderGL::RenderLine((float)x,
										(float)y,
										(float)(x1 * gsGlobalScale - (double)scrollbar_x),
										(float)(y1 * gsGlobalScale - (double)scrollbar_y));
			}
			else
			{
				canvas->MoveTo((__int32)x, (__int32)y);
				canvas->LineTo((__int32)(x1 * gsGlobalScale - (double)scrollbar_x),
								(__int32)(y1 * gsGlobalScale - (double)scrollbar_y));
			}
		}
	if (gUsingOpenGL)
	{
		TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::DrawCollideShapes(const TMapCollideLineNode* ipSelectedNode,
												bool drawDragPoints,
												TCanvas *canvas,
												__int32 scrollbar_x, __int32 scrollbar_y)
{
	if (gUsingOpenGL)
	{
		TRenderGL::SetLineStyle(TRenderGL::RGLLSSolid);
		for (__int32 i = 0; i < collideshapes->Count; i++)
		{
			const TCollideChainShape *sh = static_cast<TCollideChainShape*>(collideshapes->Items[i]);
			const TCollideChainShape::TCollideNodeList& list = sh->GetNodeList();
			const TMapCollideLineNode* n1 = NULL;
			const TMapCollideLineNode* n2;
			for(TCollideChainShape::TCollideNodeList::const_iterator it = list.begin(); it != list.end(); ++it)
			{
				double cx1, cx2, cy1, cy2;
				const TMapCollideLineNode& cn = *it;
				if(n1 == NULL)
				{
					n1 = &cn;
					continue;
				}
				n2 = &cn;
				n1->getCoordinates(&cx1, &cy1);
				n2->getCoordinates(&cx2, &cy2);
				if(n1->IsErrorMark() && n2->IsErrorMark())
				{
					TRenderGL::SetColor(255, 0, 0, 200);
				}
				else
				{
					TRenderGL::SetColor(0, 0, 255, 200);
				}
				cx1 *= gsGlobalScale;
				cy1 *= gsGlobalScale;
				cx1 -= (double)scrollbar_x;
				cy1 -= (double)scrollbar_y;
				cx2 *= gsGlobalScale;
				cy2 *= gsGlobalScale;
				cx2 -= (double)scrollbar_x;
				cy2 -= (double)scrollbar_y;
				TRenderGL::RenderLine((float)cx1, (float)cy1, (float)cx2, (float)cy2);
				if(drawDragPoints)
				{
					const bool is_end = n2 == &list.back();
					if(is_end && ipSelectedNode == NULL)
					{
						TRenderGL::RenderRect((float)(cx2 - (double)n2->Width / 2.0),
												(float)(cy2 - (double)n2->Height / 2.0),
												(float)n2->Width,
												(float)n2->Height);
					}
					if(ipSelectedNode == NULL)
					{
						TRenderGL::RenderRect((float)(cx1 - (double)n1->Width / 2.0),
												(float)(cy1 - (double)n1->Height / 2.0),
												(float)n1->Width,
												(float)n1->Height);
					}
				}
				n1 = n2;
			}
		}
	}
	else
	{
		canvas->Brush->Style = bsClear;
		canvas->Pen->Style = psSolid;
		canvas->Pen->Mode = pmCopy;
		for (__int32 i = 0; i < collideshapes->Count; i++)
		{
			const TCollideChainShape *sh = static_cast<TCollideChainShape*>(collideshapes->Items[i]);
			const TCollideChainShape::TCollideNodeList& list = sh->GetNodeList();
			const TMapCollideLineNode* n1 = NULL;
			const TMapCollideLineNode* n2;
			for(TCollideChainShape::TCollideNodeList::const_iterator it = list.begin(); it != list.end(); ++it)
			{
				double cx1, cx2, cy1, cy2;
				const TMapCollideLineNode& cn = *it;
				if(n1 == NULL)
				{
					n1 = &cn;
					continue;
				}
				n2 = &cn;
				n1->getCoordinates(&cx1, &cy1);
				n2->getCoordinates(&cx2, &cy2);
				if(n1->IsErrorMark() && n2->IsErrorMark())
				{
					canvas->Pen->Color = clRed;
				}
				else
				{
					canvas->Pen->Color = clBlue;
				}
				cx1 *= gsGlobalScale;
				cy1 *= gsGlobalScale;
				cx1 -= (double)scrollbar_x;
				cy1 -= (double)scrollbar_y;
				cx2 *= gsGlobalScale;
				cy2 *= gsGlobalScale;
				cx2 -= (double)scrollbar_x;
				cy2 -= (double)scrollbar_y;
				canvas->MoveTo((__int32)cx1, (__int32)cy1);
				canvas->LineTo((__int32)cx2, (__int32)cy2);
				if(drawDragPoints)
				{
					const bool is_end = n2 == &list.back();
					if(is_end && ipSelectedNode == NULL)
					{
						const TPoint p((__int32)(cx2 - (double)n1->Width / 2.0),
											(__int32)(cy2 - (double)n1->Height / 2.0));
						canvas->Rectangle(TRect(p, n2->Width, n2->Height));
					}
					if(ipSelectedNode == NULL)
					{
						const TPoint p((__int32)(cx1 - (double)n1->Width / 2.0),
											(__int32)(cy1 - (double)n1->Height / 2.0));
						canvas->Rectangle(TRect(p, n1->Width, n1->Height));
					}
				}
				n1 = n2;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::CopyTo(TObjManager *copy) const
{
	__int32 i;

	copy->ClearAll();
	if (copy->map != NULL)
	{
		delete[]copy->map;
		delete[]copy->map_undo;
	}

	if (map != NULL)
	{
		copy->map = new unsigned short[mp_w * mp_h];
		copy->map_undo = new unsigned short[mp_w * mp_h];
		memcpy(copy->map, map, mp_w * mp_h*sizeof(unsigned short));
		memcpy(copy->map_undo, map, mp_w * mp_h*sizeof(unsigned short));
	}
	else
	{
		copy->map = NULL;
		copy->map_undo = NULL;
	}

	copy->mp_w = mp_w;
	copy->mp_h = mp_h;
	copy->old_mp_w = old_mp_w;
	copy->old_mp_h = old_mp_h;
	copy->old_md_w = old_md_w;
	copy->old_md_h = old_md_h;
	copy->znCount = znCount;
	copy->mesCount = mesCount;
	copy->startZone = startZone;
	copy->tileCameraPosOnStart = tileCameraPosOnStart;
	copy->StartScriptName = StartScriptName;

	for (i = 0; i < map_u->Count; i++)
	{
		TMapGPerson* mgp = (TMapGPerson*)map_u->Items[i];
		if(mgp->GetObjType() == objMAPTEXTBOX)
		{
			TMapTextBox *md = copy->CreateMapTextBox();
			((TMapTextBox*)map_u->Items[i])->CopyTo(md);
		}
		else if(mgp->GetObjType() == objMAPSOUNDSCHEME)
		{
			TMapSoundScheme *sch = copy->CreateMapSoundScheme();
			((TMapSoundScheme*)map_u->Items[i])->CopyTo(sch);
		}
		else if(mgp->GetObjType() == objMAPOBJPOOL)
		{
			TMapObjPool *opl = copy->CreateMapObjPool();
			((TMapObjPool*)map_u->Items[i])->CopyTo(opl);
		}
		else
		{
			TMapGPerson *gp = copy->CreateMapGPerson();
			mgp->CopyTo(gp);
			mgp->DirStateIdx = -1;
			mgp->TraceTriggerActions = false;
			mgp->InspectStates = false;
		}
	}
	for (i = 0; i < decor_b->Count; i++)
	{
		TMapGPerson *de = copy->CreateMapGPerson();
		((TMapGPerson*)decor_b->Items[i])->CopyTo(de);
	}
	for (i = 0; i < decor_f->Count; i++)
	{
		TMapGPerson *de = copy->CreateMapGPerson();
		((TMapGPerson*)decor_f->Items[i])->CopyTo(de);
	}
	for (i = 0; i < scripts->Count; i++)
	{
		copy->CreateScript((TGScript*)scripts->Items[i]);
	}
	for (i = 0; i < triggers->Count; i++)
	{
		TMapTrigger *tr = copy->CreateMapTrigger();
		((TMapTrigger*)triggers->Items[i])->CopyTo(tr);
	}
	for (i = 0; i < directors->Count; i++)
	{
		TMapDirector *md = copy->CreateMapDirector();
		((TMapDirector*)directors->Items[i])->CopyTo(md);
	}
	for (i = 0; i < collideshapes->Count; i++)
	{
		TCollideChainShape *cn = copy->CreateCollideChainShape();
		((TCollideChainShape*)collideshapes->Items[i])->CopyTo(cn);
	}
    copy->mRefreshTempCollideNodesList = true;
	copy->commonRes = commonRes;
}
//---------------------------------------------------------------------------

TList* __fastcall TObjManager::GetCollideNodesList()
{
	if(mRefreshTempCollideNodesList == true)
	{
    	mRefreshTempCollideNodesList = false;
		mpTempCollideNodesList->Clear();
		for (__int32 i = 0; i < collideshapes->Count; i++)
		{
			const TCollideChainShape *o = (TCollideChainShape*)collideshapes->Items[i];
			o->CopyNodeList(mpTempCollideNodesList);
		}
	}
	return mpTempCollideNodesList;
}
//---------------------------------------------------------------------------

__int32 __fastcall TObjManager::GetCollideNodesCount()
{
	return GetCollideNodesList()->Count;
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::MarkCrosslines(const TMapCollideLineNode* n)
{
	assert(n);
	const TCollideChainShape* osh = n->GetParent();
	const TMapCollideLineNode* pre_n = NULL;
	const TMapCollideLineNode* mid_n = NULL;
	const TMapCollideLineNode* nex_n = NULL;
	gCurrentTimeMark = Now();
	if(osh->GetPreMidNextNodes(n, &pre_n, &mid_n, &nex_n))
	{
		assert(mid_n);
		for (__int32 i = 0; i < collideshapes->Count; i++)
		{
			TCollideChainShape *o = (TCollideChainShape*)collideshapes->Items[i];
			o->MarkIfCrossline(pre_n, mid_n, nex_n);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::MarkCrosslines(const TCollideChainShape* sh)
{
	assert(sh);
	gCurrentTimeMark = Now();
	const TCollideChainShape::TCollideNodeList& nlist = sh->GetNodeList();
	for(TCollideChainShape::TCollideNodeList::const_iterator it = nlist.begin(); it != nlist.end(); ++it)
	{
		const TMapCollideLineNode* pre_n = NULL;
		const TMapCollideLineNode* mid_n = NULL;
		const TMapCollideLineNode* nex_n = NULL;
        const TMapCollideLineNode& n = *it;
		if(sh->GetPreMidNextNodes(&n, &pre_n, &mid_n, &nex_n))
		{
			assert(mid_n);
			for (__int32 i = 0; i < collideshapes->Count; i++)
			{
				TCollideChainShape *o = (TCollideChainShape*)collideshapes->Items[i];
				o->MarkIfCrossline(pre_n, mid_n, nex_n);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjManager::ResetCrosslinesErrorMarkers()
{
	gCurrentTimeMark = 0;
}
//---------------------------------------------------------------------------

const TCollideChainShape* __fastcall TObjManager::FindCollideChainShape(const double& x, const double& y, const double& findRadius,
																		const TCollideChainShape* dragShape) const
{
	bool post_check = false;
	double tx = x;
	double ty = y;
	for (__int32 i = 0; i < collideshapes->Count; i++)
	{
		const TCollideChainShape *o = static_cast<TCollideChainShape*>(collideshapes->Items[i]);
		if(dragShape != NULL && dragShape == o)
		{
			post_check = true;
			continue;
		}
		if(o->IsPointOnLine(tx, ty, findRadius, NULL, NULL, NULL) == true)
		{
			return o;
		}
	}
	if(post_check && dragShape->IsPointOnLine(tx, ty, findRadius, NULL, NULL, NULL) == true)
	{
		return dragShape;
	}
	return nullptr;
}
//---------------------------------------------------------------------------

bool __fastcall TObjManager::ChangeNodeIndexInCollideShape(const UnicodeString& collideShapeName,
												const UnicodeString& nodeName,
												const UnicodeString& toNodeName)
{
	TMapPerson* obj = FindObjectPointer(collideShapeName);
	if(obj->GetObjType() == objMAPCOLLIDESHAPE)
	{
		TCollideChainShape* sh = static_cast<TCollideChainShape*>(obj);
		return sh->MoveNode(nodeName, toNodeName);
	}
	return false;
}
//---------------------------------------------------------------------------

 TObjManager::TCreateCollideNodeType __fastcall TObjManager::TryCreateCollideNode(double& x, double& y, const double& findRadius,
														const TMapCollideLineNode* dragNode,
														const TCollideChainShape** osh,
														const TMapCollideLineNode** on1, const TMapCollideLineNode** on2) const
{
	TCreateCollideNodeType res = CCNT_NONE;
	const TMapCollideLineNode *n1 = NULL;
	const TMapCollideLineNode *n2 = NULL;
	const TCollideChainShape* dragsh = NULL;
	if(dragNode != NULL)
	{
		TMapPerson* obj = FindObjectPointer(dragNode->getGParentName());
		assert(obj);
		assert(obj->GetObjType() == objMAPCOLLIDESHAPE);
		dragsh = static_cast<TCollideChainShape*>(obj);
	}
	const TCollideChainShape* sh = FindCollideChainShape(x, y, findRadius, dragsh);
	if(dragNode != NULL && dragsh != sh && sh != NULL)
	{
		res = CCNT_ERRORSHAPE;
	}
	else if(sh == NULL)
	{
		res = CCNT_NEWSHAPE;
	}
	else
	{
		if(dragNode == NULL && sh->IsPointOnNode(x, y, NULL, &n1) == true)
		{
            res = CCNT_ERRORNODETOSHAPE;
			if(sh->IsFirstNode(n1) || sh->IsLastNode(n1))
			{
				double wx, wy;
				n1->getCoordinates(&wx, &wy);
				if(std::fabs(x - wx) <= F_EPS && std::fabs(y - wy) <= F_EPS)
				{
					res = CCNT_ADDNODETOSHAPE;
				}
			}
		}
		else if(sh->IsPointOnLine(x, y, findRadius, dragNode, &n1, &n2) == true)
		{
			assert(n1);
			assert(n2);
			res = CCNT_INSERTNODETOSHAPE;
		}
	}
	if(on1 != NULL)
	{
		*on1 = n1;
	}
	if(on2 != NULL)
	{
		*on2 = n2;
	}
	if(osh != NULL)
	{
		*osh = sh;
	}
	return res;
}
//---------------------------------------------------------------------------

