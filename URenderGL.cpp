//---------------------------------------------------------------------------

#define USE_OPENGL_1_RENDER
#define WINDOWS_APP

#include "windows.h"
#include "UObjCode.h"
#include "URenderGL.h"
#include "render2dgl1.h"
#include "UObjManager.h"
#include "MainUnit.h"

//---------------------------------------------------------------------------

HWND  TRenderGL::mhWnd[RGL_MAX_GL_WINDOWS] = {0};
HDC   TRenderGL::mghDC[RGL_MAX_GL_WINDOWS] = {0};
HGLRC TRenderGL::mghRC = NULL;
TRGLWinId TRenderGL::mCurrentWindowId = 0;
TRect TRenderGL::mViewport;
bool TRenderGL::mEnabled = false;
bool TRenderGL::mPatternImage = false;
const TCommonResObjManager* TRenderGL::mpCommonResObjManager = NULL;
std::vector<DrawImageFunctionData> TRenderGL::mDirectBindData;
const void* TRenderGL::mpDirectBindImage = NULL;
float TRenderGL::mCr = 0.0f;
float TRenderGL::mCg = 0.0f;
float TRenderGL::mCb = 0.0f;
float TRenderGL::mCa = 0.0f;
int TRenderGL::mTextureCount = 0;
unsigned int TRenderGL::mSPTexture[TEXTURE_SP_COUNT];
bool TRenderGL::mSPTextureInit = false;

bool gUsingOpenGL = false;

//---------------------------------------------------------------------------

static void TRenderGL_CheckError(const char* op)
{
#ifdef SDK_DEBUG
	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		OS_Warning(_T("after %s() glError (0x%x)\n"), op, error);
		assert(0);
	}
#else
	(void)op;
#endif
}
//---------------------------------------------------------------------------

void __fastcall TRenderGL::ReleaseWindows()
{
	ClearRenderList();
	if(mSPTextureInit)
	{
		glRender_DeleteTexture(mSPTexture[TEXTURE_SP_DIRECT]);
		glRender_DeleteTexture(mSPTexture[TEXTURE_SP_TRANSPARENCY_PATTERN]);
		mSPTextureInit = false;
	}

	if(mpCommonResObjManager != NULL)
	{
		__int32 r_count = mpCommonResObjManager->GetGraphicResourceCount();
		for (__int32 i = 0; i < r_count; i++)
		{
			DeleteTexture(i);
		}
		mpCommonResObjManager = NULL;
    }

	ReleaseBG();
	if(glRender_IsRenderPlaneInit(BGSELECT_SUB3))
	{
		glRender_PlaneRelease(BGSELECT_SUB3);
    }
	for(int i = 0; i < RGL_MAX_GL_WINDOWS; i++)
	{
		if(mghRC != NULL)
		{
			wglDeleteContext(mghRC);
		}
		if(mghDC[i])
		{
			ReleaseDC(mhWnd[i], mghDC[i]);
		}
		mghRC = NULL;
		mghDC[i] = NULL;
		mhWnd[i] = NULL;
	}
	mPatternImage = false;
	mPatternImage = NULL;
	wglMakeCurrent(NULL, NULL);
	mCurrentWindowId = 0;
	mDirectBindData.clear();
	mpDirectBindImage = NULL;
	glRender_Release();
	mCa = 0.0f;
}
//---------------------------------------------------------------------------

bool __fastcall TRenderGL::IsInited()
{
	return glRender_IsRenderPlaneInit(BGSELECT_SUB3);
}
//---------------------------------------------------------------------------

TRGLWinId __fastcall TRenderGL::InitWindow(HWND hWnd, TRect viewport, TRect window, bool windowMode)
{
	HDC cdc;
	TRGLWinId id = -1;

	for(int i = 0; i < RGL_MAX_GL_WINDOWS; i++)
	{
		if(mhWnd[i] == NULL)
		{
			id = i;
			mhWnd[id] = hWnd;
			break;
        }
	}

	if(id == -1)
	{
		assert(0);
		return -1;
    }

	mCurrentWindowId = id;

	if(windowMode)
	{
		cdc = mghDC[id] = GetDC(mhWnd[id]);
	}
	else
	{
		cdc = reinterpret_cast<HDC>(mhWnd[id]);
		mghDC[id] = NULL;
	}
	if(!SetupPixelFormat(cdc, windowMode))
	{
		ReleaseWindows();
		assert(0);
		return -1;
	}
	if(mghRC == NULL)
	{
		mghRC = wglCreateContext(cdc);
    }
	wglMakeCurrent(cdc, mghRC);

	mEnabled = true;
	mPatternImage = false;

	if(glRender_IsGraphicsInit() == FALSE)
	{
		struct RenderPlaneInitParams p;
		p.mBGType = BGSELECT_SUB3;
		p.mSizes.mViewWidth = viewport.Width();
		p.mSizes.mViewHeight = viewport.Height();
		p.mX = window.Left - viewport.Left;
		p.mY = window.Bottom - viewport.Bottom;
		p.mMaxRenderObjectsOnPlane = 0;

		glRender_Init();
		glRender_PlaneInit(&p);
		glRender_SetBackdropFn(DrawBackdrop);
		glRender_SetActiveBGForGraphics(BGSELECT_SUB3);
		Resize(viewport, window);
		glRender_RestoreDevice();

		mSPTexture[TEXTURE_SP_DIRECT] = glRender_CreateTexture();
		mSPTexture[TEXTURE_SP_TRANSPARENCY_PATTERN] = glRender_CreateTexture();
		mSPTextureInit = true;
	}

	assert(DIT_Obj == MAX_TILE_BG_LAYERS);
	mDirectBindData.clear();
	mpDirectBindImage = NULL;
	return id;
}
//---------------------------------------------------------------------------

void __fastcall TRenderGL::SetActiveWindow(TRGLWinId WinID)
{
	if(WinID >= RGL_MAX_GL_WINDOWS || mhWnd[WinID] == NULL)
	{
		assert(0);
		return;
	}
	mCurrentWindowId = WinID;
	HDC cdc;
	if(mghDC[WinID] != NULL)
	{
		cdc = mghDC[WinID];
	}
	else
	{
		cdc = reinterpret_cast<HDC>(mhWnd[WinID]);
	}
	wglMakeCurrent(cdc, mghRC);
	mDirectBindData.clear();
	mpDirectBindImage = NULL;
	if(mCa != 0.0f)
	{
		glClearColor(mCr, mCg, mCb, mCa);
		TRenderGL_CheckError("glClearColor");
	}
}
//---------------------------------------------------------------------------

void __fastcall TRenderGL::Enable(bool val)
{
	mEnabled = val;
}
//---------------------------------------------------------------------------

bool __fastcall TRenderGL::IsEnabled()
{
	return mEnabled;
}
//---------------------------------------------------------------------------

void __fastcall TRenderGL::SetBackdropImage(const BMPImage* pImage, TColor altColor)
{
	if(glRender_IsRenderPlaneInit(BGSELECT_SUB3))
	{
		if(pImage == NULL)
		{
			int cl = (int)altColor;
			unsigned char ch = (unsigned char)cl;
			mCr = (float)ch / 255.0f;
			ch = (unsigned char)(cl >> 8);
			mCg = (float)ch / 255.0f;
			ch = (unsigned char)(cl >> 16);
			mCb = (float)ch / 255.0f;
			mPatternImage = false;
			mCa = 1.0f;
			glClearColor(mCr, mCg, mCb, mCa);
			TRenderGL_CheckError("glClearColor");
		}
		else
		{
			mCa = 0.0f;
			glRender_LoadTextureToVRAM(pImage, mSPTexture[TEXTURE_SP_TRANSPARENCY_PATTERN]);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			mPatternImage = true;
		}
	}
}
//---------------------------------------------------------------------------

bool __fastcall TRenderGL::SetupPixelFormat(HDC hdc, bool windowMode)
{
    int pixelformat;
	DWORD dwFlags;

	if(windowMode)
	{
		dwFlags = PFD_DRAW_TO_WINDOW |   // support window
					PFD_SUPPORT_OPENGL |   // support OpenGL
					PFD_DOUBLEBUFFER;      // double buffered
	}
	else
	{
		dwFlags = PFD_SUPPORT_OPENGL |  // Use OpenGL.
					PFD_SUPPORT_GDI |
					PFD_DRAW_TO_BITMAP; // Pixel format for bitmap
	}

	PIXELFORMATDESCRIPTOR pfd = {
        sizeof(PIXELFORMATDESCRIPTOR),  //  size of this pfd
        1,                     // version number
		dwFlags,			   // flags
        PFD_TYPE_RGBA,         // RGBA type
        32,                    // 32-bit color depth
        0, 0, 0, 0, 0, 0,      // color bits ignored
        0,                     // no alpha buffer
        0,                     // shift bit ignored
        0,                     // no accumulation buffer
        0, 0, 0, 0,            // accum bits ignored
        32,                    // 32-bit z-buffer
        0,                     // no stencil buffer
        0,                     // no auxiliary buffer
        PFD_MAIN_PLANE,        // main layer
        0,                     // reserved
        0, 0, 0                // layer masks ignored
	};

	if ((pixelformat = ChoosePixelFormat(hdc, &pfd)) == 0)
	{
        return false;
    }

	if (SetPixelFormat(hdc, pixelformat, &pfd) == false)
    {
        return false;
    }

    return true;
} 
//---------------------------------------------------------------------------

void __fastcall TRenderGL::Resize(TRect viewport, TRect window)
{
	if(glRender_IsRenderPlaneInit(BGSELECT_SUB3))
	{
		mViewport = viewport;
		RenderPlaneSizeParams p;
		p.mViewWidth = viewport.Width();
		p.mViewHeight = viewport.Height();
		glRender_PlaneResize(BGSELECT_SUB3, &p);
		glRender_PlanePosition(BGSELECT_SUB3, window.Left - viewport.Left, window.Bottom - viewport.Bottom);
		glRender_Resize(window.Width(), window.Height());
	}
}
//---------------------------------------------------------------------------

void __fastcall TRenderGL::SetActiveCommonResObjManager(const TCommonResObjManager* ipCM)
{
	assert(glRender_IsRenderPlaneInit(BGSELECT_SUB3));
	assert(mghRC != NULL); // do this before InitWindow()
	assert(ipCM);
	assert(ipCM != mpCommonResObjManager);
	assert(mTextureCount == 0);
	if(ipCM != mpCommonResObjManager && mpCommonResObjManager != NULL)
	{
		__int32 r_count = mpCommonResObjManager->GetGraphicResourceCount();
		for (__int32 i = 0; i < r_count; i++)
		{
			DeleteTexture(i);
		}
	}
	mpCommonResObjManager = ipCM;
	if(ipCM->GetGraphicResourceCount() > 0)
	{
		__int32 r_count = mpCommonResObjManager->GetGraphicResourceCount();
		for (__int32 i = 0; i < r_count; i++)
		{
			const BMPImage* img = mpCommonResObjManager->GetGraphicResource(i);
			if(img != NULL)
			{
				assert(img->mOpaqType < 0);
				const_cast<BMPImage*>(img)->mOpaqType = CreateTexture();
				UpdateTextureData(i);
            }
		}
	}
}
//---------------------------------------------------------------------------

int __fastcall TRenderGL::CreateTexture()
{
	assert(glRender_IsRenderPlaneInit(BGSELECT_SUB3));
	assert(mpCommonResObjManager != NULL);
	mTextureCount++;
	return glRender_CreateTexture();
}
//---------------------------------------------------------------------------

void __fastcall TRenderGL::UpdateTextureData(int idx)
{
	assert(glRender_IsRenderPlaneInit(BGSELECT_SUB3));
	assert(mpCommonResObjManager != NULL);
	assert(mpCommonResObjManager->GetGraphicResourceCount() > idx);
	const BMPImage* img = mpCommonResObjManager->GetGraphicResource(idx);
	assert(img);
	assert(img->mOpaqType > 0);
	glRender_LoadTextureToVRAM(img, img->mOpaqType);
}
//---------------------------------------------------------------------------

void __fastcall TRenderGL::DeleteTexture(int idx)
{
	assert(glRender_IsRenderPlaneInit(BGSELECT_SUB3));
	assert(mpCommonResObjManager != NULL);
	assert(mpCommonResObjManager->GetGraphicResourceCount() > idx);
	const BMPImage* img = mpCommonResObjManager->GetGraphicResource(idx);
	if(img && img->mOpaqType > 0)
	{
		mTextureCount--;
		glRender_DeleteTexture(img->mOpaqType);
		const_cast<BMPImage*>(img)->mOpaqType = -1;
    }
}
//---------------------------------------------------------------------------

void __fastcall TRenderGL::InitBG()
{
	if(glRender_IsRenderPlaneInit(BGSELECT_SUB3) && mpCommonResObjManager != NULL)
	{
		short tw, th;
		__int32 mw, mh, me;
		Form_m->GetTileSize(tw, th);
		Form_m->GetMapSize(mw, mh);
		ReleaseBG();
		me = (mw / tw + 1) * (mh / th + 1);
		glRender_SetupBGLayersData(BGSELECT_SUB3,
									Form_m->GetLayersCount(),
									mpCommonResObjManager->GetGraphicResourceCount(),
									me);
	}
}
//---------------------------------------------------------------------------

void __fastcall TRenderGL::ReleaseBG()
{
	ClearRenderList();
	glRender_ReleaseBGLayersData(BGSELECT_SUB3);
}
//---------------------------------------------------------------------------

void __fastcall TRenderGL::ClearRenderList()
{
	glRender_ClearFrameBuffer(BGSELECT_SUB3);
}
//---------------------------------------------------------------------------

void __fastcall TRenderGL::DrawScene(HDC hdc)
{
	if(!glRender_IsRenderPlaneInit(BGSELECT_SUB3))
	{
		return;
	}

	if(mEnabled)
	{
		if(hdc == NULL)
		{
			if(mghDC[mCurrentWindowId] != NULL)
			{
				hdc = mghDC[mCurrentWindowId];
			}
			else
			{
				hdc = reinterpret_cast<HDC>(mhWnd[mCurrentWindowId]);
			}
			assert(mghRC != NULL);
		}
		glRender_DrawFrame();
		if(hdc == NULL)
		{
			glFinish();
			glFlush();
		}
		else
		{
			SwapBuffers(hdc);
		}
	}
	else
	{
    	ClearRenderList();
    }
}
//---------------------------------------------------------------------------

void TRenderGL::DrawBackdrop(s32 *currentTextureID)
{
	// backdrop pattern image (transparency)
	if(mPatternImage)
	{
		if(glRender_IsTexturesEnabled() == FALSE)
		{
			glEnable(GL_TEXTURE_2D);
		}
        assert(mSPTexture[TEXTURE_SP_TRANSPARENCY_PATTERN] > 0);
		glRender_ForceBindTexture(mSPTexture[TEXTURE_SP_TRANSPARENCY_PATTERN]);
		*currentTextureID = mSPTexture[TEXTURE_SP_TRANSPARENCY_PATTERN];
		float w2n, h2n;
		w2n = 0.0f;
		glGetTexLevelParameterfv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &w2n);
		glGetTexLevelParameterfv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &h2n);
		if(w2n != 0.0f)
		{
			float w = mViewport.Width() / w2n;
			float h = mViewport.Height() / h2n;
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			glBegin(GL_QUADS);
				glTexCoord2f(0.0f, 0.0f);
				glVertex3f(0.0f, 0.0f, 0.0f);
				glTexCoord2f(w + 0.0f, 0.0f);
				glVertex3f(0.0f + mViewport.Width(), 0.0f, 0.0f);
				glTexCoord2f(w + 0.0f, h + 0.0f);
				glVertex3f(0.0f + mViewport.Width(), 0.0f + mViewport.Height(), 0.0f);
				glTexCoord2f(0.0f, h + 0.0f);
				glVertex3f(0.0f, 0.0f + mViewport.Height(), 0.0f);
			glEnd();
		}
		if(glRender_IsTexturesEnabled() == FALSE)
		{
			glDisable(GL_TEXTURE_2D);
		}
	}

	if(mDirectBindData.empty() == false)
	{
		if(glRender_IsTexturesEnabled() == FALSE)
		{
			glEnable(GL_TEXTURE_2D);
		}
		assert(mSPTexture[TEXTURE_SP_DIRECT] > 0);
		std::vector<DrawImageFunctionData>::const_iterator it = mDirectBindData.begin();
		for(; it != mDirectBindData.end(); ++it)
		{
			if(mpDirectBindImage != it->mpSrcData)
			{
				mpDirectBindImage = it->mpSrcData;
				BindDirectTexture(it->mpSrcData);
				*currentTextureID = mSPTexture[TEXTURE_SP_DIRECT];
			}
			else
			{
				glRender_ForceBindTexture(mSPTexture[TEXTURE_SP_DIRECT]);
			}
			float x = (float)it->mSrcSizeData[SRC_DATA_X] / (float)it->mpSrcData->mWidth2n;
			float y = (float)it->mSrcSizeData[SRC_DATA_Y] / (float)it->mpSrcData->mHeight2n;
			float w = (float)it->mSrcSizeData[SRC_DATA_W] / (float)it->mpSrcData->mWidth2n;
			float h = (float)it->mSrcSizeData[SRC_DATA_H] / (float)it->mpSrcData->mHeight2n;
			glColor4f(1.0f, 1.0f, 1.0f, (float)it->mAlpha / 255.0f);
			glBegin(GL_QUADS);
				glTexCoord2f(x, y);
				glVertex3f(it->mX, it->mY, 0.0f);

				glTexCoord2f(w + x, y);
				glVertex3f(it->mX + (float)it->mSrcSizeData[SRC_DATA_W] * it->mScaleX, it->mY, 0.0f);

				glTexCoord2f(w + x, h + y);
				glVertex3f(it->mX + (float)it->mSrcSizeData[SRC_DATA_W] * it->mScaleX, it->mY + (float)it->mSrcSizeData[SRC_DATA_H] * it->mScaleY, 0.0f);

				glTexCoord2f(x, h + y);
				glVertex3f(it->mX, it->mY + (float)it->mSrcSizeData[SRC_DATA_H] * it->mScaleY, 0.0f);
			glEnd();
		}
		mDirectBindData.clear();
		if(glRender_IsTexturesEnabled() == FALSE)
		{
			glDisable(GL_TEXTURE_2D);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TRenderGL::SetColor(TColor color)
{
	if(!glRender_IsRenderPlaneInit(BGSELECT_SUB3))
	{
		return;
	}
	u32 cl = color;
	u8 ch = (u8)cl;
	u8 cr = ch;
	ch = (u8)(cl >> 8);
	u8 cg = ch;
	ch = (u8)(cl >> 16);
	u8 cb = ch;
	u8 ca = 255;
	glRender_SetColor(cr, cg, cb, ca);
}
//----------------------------------------------------------------------------------------------------------------

void __fastcall TRenderGL::SetColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
	if(!glRender_IsRenderPlaneInit(BGSELECT_SUB3))
	{
		return;
	}
	glRender_SetColor(r, g, b, a);
}
//----------------------------------------------------------------------------------------------------------------

void __fastcall TRenderGL::SetLineStyle(RGLLineStyle iStyle)
{
	glRender_SetLineStyle(iStyle);
}
//----------------------------------------------------------------------------------------------------------------

void __fastcall TRenderGL::RenderLine(double iX1, double iY1, double iX2, double iY2)
{
	if(!glRender_IsRenderPlaneInit(BGSELECT_SUB3))
    {
		return;
	}
	glRender_DrawLine((float)iX1, (float)iY1, (float)iX2, (float)iY2);
}
//----------------------------------------------------------------------------------------------------------------

void __fastcall TRenderGL::RenderCircle(double iX, double iY, double iR)
{
	if(!glRender_IsRenderPlaneInit(BGSELECT_SUB3))
    {
    	return;
    }
	double xaf, yaf, xa, ya, step, xa_last, ya_last, a;
	int vert = 36;
	step  = (2.0f * 3.14159265358979323846f) / (double)vert;
	xa_last = iX + iR;
	ya_last = iY;
	a = 0.0f;
	for(int i = 0; i < vert; ++i)
	{
		a += step;
		xaf = iR * (double)Cos(a);
		yaf = iR * (double)Sin(a);
		xa = iX + xaf;
		ya = iY + yaf;
		glRender_DrawLine((float)xa_last, (float)ya_last, (float)xa, (float)ya);
		xa_last = xa;
		ya_last = ya;
	}
}
//----------------------------------------------------------------------------------------------------------------

void __fastcall TRenderGL::FillRect(double iX, double iY, double iWidth, double iHeight)
{
	if(!glRender_IsRenderPlaneInit(BGSELECT_SUB3))
    {
    	return;
	}
	glRender_ColorRect((float)iX, (float)iY, (float)iWidth, (float)iHeight);
}
//----------------------------------------------------------------------------------------------------------------

void __fastcall TRenderGL::RenderRect(double iX, double iY, double iWidth, double iHeight)
{
	if(!glRender_IsRenderPlaneInit(BGSELECT_SUB3))
    {
    	return;
	}
	glRender_DrawLine((float)iX, (float)iY, (float)(iX + iWidth), (float)iY);
	glRender_DrawLine((float)(iX + iWidth), (float)iY, (float)(iX + iWidth), (float)(iY + iHeight));
	glRender_DrawLine((float)(iX + iWidth), (float)(iY + iHeight), (float)iX, (float)(iY + iHeight));
	glRender_DrawLine((float)iX, (float)(iY + iHeight), (float)iX, (float)iY);
}
//----------------------------------------------------------------------------------------------------------------

void __fastcall TRenderGL::RenderImage(int idx,
							DrawImageType type,
							double iSrcX, double iSrcY,
							double iSrcWidth, double iSrcHeight,
							double iDestX, double iDestY,
							double iDestWidth, double iDestHeight,
							double iTransparency)
{
	if(!glRender_IsRenderPlaneInit(BGSELECT_SUB3) || mpCommonResObjManager == NULL)
	{
		return;
	}

	DrawImageFunctionData f;

	assert(mpCommonResObjManager);
	f.mpSrcData = mpCommonResObjManager->GetGraphicResource(idx);
	assert(f.mpSrcData);
	if(f.mpSrcData->mWidth2n > 0 && f.mpSrcData->mHeight2n > 0)
	{
		f.mpClipRect = NULL;
		f.mX = (float)iDestX;
		f.mY = (float)iDestY;
		f.mAffineOriginX = 0.0f;
		f.mAffineOriginY = 0.0f;
		f.mSin = 0.0f;
		f.mCos = 1.0f;
		f.mScaleX = (float)(iDestWidth / iSrcWidth);
		f.mScaleY = (float)(iDestHeight / iSrcHeight);
		f.mType = type;
		f.mFillWithColor = 0;
		f.mSrcSizeData[SRC_DATA_X] = (unsigned short)iSrcX;
		f.mSrcSizeData[SRC_DATA_Y] = (unsigned short)iSrcY;
		f.mSrcSizeData[SRC_DATA_W] = (unsigned short)iSrcWidth;
		f.mSrcSizeData[SRC_DATA_H] = (unsigned short)iSrcHeight;
		f.mAlpha = (u8)(iTransparency * 255.0f);
		f.text = 0;

		glRender_DrawImage(&f);
	}
}
//----------------------------------------------------------------------------------------------------------------

void __fastcall TRenderGL::RenderNonResManagerImage(const BMPImage* pImage,
							DrawImageType type,
							double iSrcX, double iSrcY,
							double iSrcWidth, double iSrcHeight,
							double iDestX, double iDestY,
							double iDestWidth, double iDestHeight,
							double iTransparency)
{
	if(!glRender_IsRenderPlaneInit(BGSELECT_SUB3))
    {
    	return;
	}

	DrawImageFunctionData f;
	assert(pImage);
	f.mpSrcData = pImage;
	if(f.mpSrcData->mWidth2n > 0 && f.mpSrcData->mHeight2n > 0)
	{
		f.mpClipRect = NULL;
		f.mX = (float)iDestX;
		f.mY = (float)iDestY;
		f.mAffineOriginX = 0.0f;
		f.mAffineOriginY = 0.0f;
		f.mSin = 0.0f;
		f.mCos = 1.0f;
		f.mScaleX = (float)(iDestWidth / iSrcWidth);
		f.mScaleY = (float)(iDestHeight / iSrcHeight);
		f.mType = type;
		f.mFillWithColor = 0;
		f.mSrcSizeData[SRC_DATA_X] = (unsigned short)iSrcX;
		f.mSrcSizeData[SRC_DATA_Y] = (unsigned short)iSrcY;
		f.mSrcSizeData[SRC_DATA_W] = (unsigned short)iSrcWidth;
		f.mSrcSizeData[SRC_DATA_H] = (unsigned short)iSrcHeight;
		f.mAlpha = (u8)(iTransparency * 255.0f);
		f.text = 0;
		mDirectBindData.push_back(f);
		glRender_DummyDraw();
	}
}
//----------------------------------------------------------------------------------------------------------------

void __fastcall TRenderGL::FlushRenderNonResManagerImage()
{
	mpDirectBindImage = NULL;
}
//----------------------------------------------------------------------------------------------------------------

void __fastcall TRenderGL::BindDirectTexture(const BMPImage* pImage)
{
	assert(mSPTexture[TEXTURE_SP_DIRECT] > 0);
	glRender_LoadTextureToVRAM(pImage, mSPTexture[TEXTURE_SP_DIRECT]);
}
//----------------------------------------------------------------------------------------------------------------


