//---------------------------------------------------------------------------

#ifndef UCreateMessagesH
#define UCreateMessagesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ActnList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <Dialogs.hpp>
#include <ImgList.hpp>
#include <Tabs.hpp>
#include <CategoryButtons.hpp>
#include <AppEvnts.hpp>
#include "texts.h"
#include <System.Actions.hpp>
#include <System.ImageList.hpp>
//---------------------------------------------------------------------------

class TUnicodeText;
class TImageFont;
class BMPImage;

class TFMessages : public TForm
{
__published:

    TStatusBar *StatusBar;
    TMainMenu *MainMenu;
    TToolBar *ToolBar1;
    TActionList *ActionList1;
    TMenuItem *N2;
    TToolButton *ToolButton8;
    TImageList *ImageList;
    TAction *ActSave;
    TAction *ActLoad;
	TAction *ActSizes;
    TAction *ActNewMessage;
    TAction *ActDelMessage;
    TPanel *Panel_m;
    TPanel *Panel;
    TSplitter *Splitter1;
    TSplitter *Splitter2;
    TCategoryButtons *Alphabet;
    TApplicationEvents *ApplicationEvents1;
    TPanel *PanelG;
    TScrollBox *ScrollBox;
    TImage *Image;
	TToolButton *ToolButton13;
	TAction *ActColor;
	TMenuItem *N12;
	TMenuItem *NALeft;
	TMenuItem *NARight;
	TMenuItem *NAHCenter;
	TMenuItem *N13;
	TMenuItem *NATop;
	TMenuItem *NABottom;
	TMenuItem *NAVCenter;
	TMenuItem *N15;
	TMenuItem *N10;
	TColorDialog *ColorDialog;
	TComboBox *CB_Font;
	TLabel *Label2;
	TEdit *CB_List;
	TMemo *Memo;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall MemoChange(TObject *Sender);
    void __fastcall DoDblClick(TObject *Sender);
    void __fastcall ListViewKeyPress(TObject *Sender, char &Key);
    void __fastcall ActSizesExecute(TObject *Sender);
    void __fastcall ActLoadExecute(TObject *Sender);
    void __fastcall ApplicationEvents1Message(tagMSG &Msg, bool &Handled);
    void __fastcall AlphabetDrawIcon(TObject *Sender,
      const TButtonItem *Button, TCanvas *Canvas, TRect &Rect,
      TButtonDrawState State, int &TextOffset);
    void __fastcall AlphabetCategoryCollapase(TObject *Sender,
      const TButtonCategory *Category);
    void __fastcall AlphabetMouseDown(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y);
	void __fastcall CB_FontChange(TObject *Sender);
	void __fastcall ActColorExecute(TObject *Sender);
	void __fastcall NALeftClick(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall FormDestroy(TObject *Sender);

private:

	int dispW, dispH, currPage, currMessage;
	bool UnderConstruction;

    BMPImage *mpCanvas;
	TUnicodeText *mpCurrText;
	::Text mRenderText;
    const TImageFont *mpFont;

	void __fastcall CreateListView();
	void __fastcall RefreshAlignment();

	void __fastcall LoadFile(UnicodeString file_name, UnicodeString *errMessage = NULL);

public:

	bool __fastcall Init(TUnicodeText *ipCurrText);
	void __fastcall EmptyMessage();
	bool __fastcall ViewOnly(int id);
	bool __fastcall AssignFont(UnicodeString ifontname);

    void __fastcall setDisplaySize(int w,int h);
    __fastcall TFMessages(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFMessages *FMessages;
//---------------------------------------------------------------------------
#endif
