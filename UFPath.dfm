object FProjPath: TFProjPath
  Left = 327
  Top = 225
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  ClientHeight = 128
  ClientWidth = 449
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Pitch = fpFixed
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 21
    Top = 23
    Width = 86
    Height = 13
    Caption = 'mResPathWindow'
  end
  object Bevel1: TBevel
    Left = 406
    Top = 39
    Width = 27
    Height = 21
  end
  object Button1: TButton
    Left = 224
    Top = 91
    Width = 83
    Height = 25
    Caption = 'mApply'
    ModalResult = 1
    TabOrder = 2
  end
  object EdPngPath: TEdit
    Left = 21
    Top = 39
    Width = 385
    Height = 21
    TabStop = False
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    TabOrder = 0
  end
  object BtG_3dot: TButton
    Left = 407
    Top = 40
    Width = 25
    Height = 19
    Caption = '...'
    TabOrder = 1
    OnClick = BtG_3dotClick
  end
  object ButtonCancel: TButton
    Left = 142
    Top = 91
    Width = 76
    Height = 25
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 3
  end
end
