//---------------------------------------------------------------------------

#ifndef USaveH
#define USaveH

#include "USaveLoadStreamData.h"

//---------------------------------------------------------------------------


void WriteAnsiStringData(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h,
								UnicodeString fieldName, const UnicodeString& str);

void WriteWideStringData(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h,
								const TSaveLoadStreamData::TSLFieldType& ftype, UnicodeString fieldName, const UnicodeString& wstr);

#endif

