//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "URenderThread.h"
#include "MainUnit.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

__fastcall TRenderThread::TRenderThread(TThreadFunction fn)
	: TThread(false)
{
	mpFn = fn;
	FreeOnTerminate = true;
}
//---------------------------------------------------------------------------

void __fastcall TRenderThread::DoProcess()
{
	if(mpFn != NULL)
	{
		mpFn();
    }
}
//---------------------------------------------------------------------------

void __fastcall TRenderThread::Execute()
{
	DoProcess();
	Terminate();
	WaitFor();
}
//---------------------------------------------------------------------------
