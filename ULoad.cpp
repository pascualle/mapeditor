#include "UnitFindObj.h"

#pragma hdrstop

#include "ULoad.h"
#include "MainUnit.h"
#include "UScript.h"
#include "UObjCode.h"
#include "inifiles.hpp"
#include "UObjManager.h"
#include "UBGEditWindow.h"
#include "URenderThread.h"
#include "UWait.h"
#include "URenderGL.h"
#include "ULocalization.h"
#include "UObjOrder.h"

 //#define LINUX_WINE_VER

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

static TSaveLoadStreamData* gspIOStreamData = nullptr;
static UnicodeString gsErrorString;
static UnicodeString gsErrorResString;
static TStringList *pgGlobalPaternTabStrings = nullptr;
static std::list<TResourceItem> *gsStrLst = nullptr;

//---------------------------------------------------------------------------

void __fastcall TForm_m::ProjLoad(UnicodeString infilename, bool commandline, UnicodeString *errmes)
{
	if(AskToSave() == ID_CANCEL)
	{
		return;
	}

	if(PathPngRes.IsEmpty())
	{
		const UnicodeString emes = Localization::Text(_T("mResPathError"));
		if(commandline)
		{
			if(errmes != nullptr)
			{
				*errmes = *errmes + _T("ERROR: ") + emes + _T("\n");
			}
		}
		else
		{
			Application->MessageBox(emes.c_str(),
									Localization::Text(_T("mMessageBoxWarning")).c_str(),
									MB_OK|MB_ICONWARNING);
        }
	}

	if(infilename.IsEmpty())
	{
		OpenDialog->Title = ActProjLoad->Caption;
		OpenDialog->InitialDir = ExtractFilePath(proj_file_name);
		OpenDialog->Filter = _T("Map editor project (*.meproj)|*.meproj|Map editor project (*.mpf)|*.mpf");
		if(!OpenDialog->Execute())
		{
			return;
		}
	}
	else
	{
		OpenDialog->FileName = infilename;
	}

	proj_file_name = OpenDialog->FileName;

	if (FObjOrder->Visible)
	{
		FObjOrder->Hide();
	}

	HideObjPrpToolWindow();

	if(FBGObjWindow->Visible)
	{
		FBGObjWindow->Hide();
	}

	TSaveLoadStreamData::TSaveLoadStreamDataInit initdata;

	const UnicodeString ext = ExtractFileExt(proj_file_name).LowerCase();

	if(ext == _T(".mpf"))
	{
		initdata.type = TSaveLoadStreamData::TSLDataType::LoadAsBinary;
		initdata.fileName = proj_file_name;
	}
	else if(ext == _T(".meproj"))
	{
		initdata.type = TSaveLoadStreamData::TSLDataType::LoadProject;
		initdata.fileName = proj_file_name;
	}
	else
	{
		return;
	}

	gspIOStreamData = new TSaveLoadStreamData(initdata, false);
	if(gspIOStreamData->Init() == false)
	{
		const UnicodeString emes = Localization::Text(_T("mReadFileError")) + _T(": ") + proj_file_name;
		if(commandline)
		{
			if(errmes != NULL)
			{
				*errmes = *errmes + _T("ERROR: ") + emes + _T("\n");
			}
		}
		else
		{
			Application->MessageBox(emes.c_str(),
									Localization::Text(_T("mError")).c_str(),
									MB_OK|MB_ICONERROR);
		}

		delete gspIOStreamData;
		return;
    }

	gsStrLst = nullptr;
	gsErrorString = _T("");
	pgGlobalPaternTabStrings = new TStringList();

	if(mainCommonRes != NULL)
	{
		mainCommonRes->ClearGraphicResources();
	}
	ReleaseRenderGL();

#ifndef LINUX_WINE_VER
	Application->CreateForm(__classid(TFWait), &FWait);
	FWait->Left = Form_m->Left + (Form_m->Width - FWait->Width) / 2;
	FWait->Top = Form_m->Top + (Form_m->Height - FWait->Height) / 2;
	FWait->Enabled = true;
#endif

#ifdef LINUX_WINE_VER
	DoThreadProcessProjLoad();
#else

	new TRenderThread(&DoThreadProcessProjLoad); // will free on finish automatically

	if(FWait->Enabled)
	{
		FWait->ShowModal();
	}
	FWait->Free();
	FWait = nullptr;
#endif

/*
	if(gspIOStreamData->type == TSaveLoadStreamData::TSLDataType::LoadAsBinary)
	{
		fclose(initdata.stream.f);
	}
	else if(gspIOStreamData->type == TSaveLoadStreamData::TSLDataType::LoadProject)
	{
		XMLDocument->DocumentElement->GetChildNodes()->Clear();
		XMLDocument->Active = false;
	}
*/

	delete gspIOStreamData;
    gspIOStreamData = nullptr;

	InitRenderGL();

	if(gsStrLst != nullptr)
	{
		AssignGraphicToObjects(PathPngRes, gsStrLst, &gsErrorResString);
		gsStrLst->clear();
		delete gsStrLst;
		gsStrLst = nullptr;
	}
	RefreshFonts();

	if(!gsErrorString.IsEmpty())
	{
		if(commandline)
		{
			if(errmes != nullptr)
			{
				*errmes = *errmes + _T("ERROR: ") + gsErrorString + _T("\n");
			}
		}
		else
		{
			Application->MessageBox(gsErrorString.c_str(),
									Localization::Text(_T("mError")).c_str(),
									MB_OK|MB_ICONERROR);
        }
	}
	else
	{
		proj_exist = true;
		m_pObjManager = nullptr;
		SetActiveProjLevel(0);
	}

	TabSetObjInit();
	if(pgGlobalPaternTabStrings->Count > 0)
	{
		TabSetObj->Tabs->Clear();
		TabSetObj->Tabs->AddStrings(pgGlobalPaternTabStrings);
		mGameObjListViewCount = pgGlobalPaternTabStrings->Count;
		TabSetObj->TabIndex = 0;
	}
	delete pgGlobalPaternTabStrings;
	pgGlobalPaternTabStrings = NULL;

	CreateMap(false);    //������� ��� ������ ����� (���� ����� ��� �������)
	LoadTexturesToVRAMGL();
	AddGlobalScriptsToAllLevels(false);
	AddGlobalTriggerToAllLevels(false);
	RelinkBGobjProperties(false); //���������� �������� ������� ��������
	RelinkGameObjStates(); //���������� ���������
	rebuildBackObjListView();
	rebuildGameObjListView(); //����������� ���� ������ �������� �����
	rebuildGameObjects();//������������ ��� ������� ���� �� �����
	ActShowRectObj->Checked = showRectGameObj;
	ActGrid->Checked = showGrid;
	file_name = proj_file_name;
	RefreshFileName();
	ReorderPanelComponents();
	CreateZoneList();
	SetDrMode(dr_mode);
	if(commandline == true)
	{
		if(errmes != NULL && !gsErrorResString.IsEmpty())
		{
			*errmes = *errmes + _T("ERROR: ") + gsErrorResString + _T("\n");
		}
	}
	else
	{
		ShowLoadErrorMessages(gsErrorResString);
	}
	InitPrpWindows(mainCommonRes);
	CreateLanguageMenu();
	is_save = false;
}
//---------------------------------------------------------------------------

void ReadWideStringData(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, const TSaveLoadStreamData::TSLFieldType& ftype, UnicodeString fieldName, UnicodeString& str)
{
	size_t w;
	wchar_t wch[65535];
    w = 0;
	if(f.type == TSaveLoadStreamData::TSLDataType::LoadProject)
	{
		TSaveLoadStreamCharArray a = {65535 * sizeof(wchar_t), wch};
		w = f.readFn(&a, TSaveLoadStreamData::TSLFieldType::ftWideStringArray, fieldName, h) / sizeof(wchar_t);
	}
	else
	{
		f.readFn(&w, ftype, fieldName + _T("sz"), h);
		if(w > 0)
		{
			TSaveLoadStreamCharArray a = {w * sizeof(wchar_t), wch};
			f.readFn(&a, TSaveLoadStreamData::TSLFieldType::ftWideStringArray, fieldName, h);
		}
	}
	wch[w] = L'\0';
	str = UnicodeString(wch);
}
//---------------------------------------------------------------------------

void ReadAnsiStringData(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, UnicodeString fieldName, UnicodeString& str, int ver)
{
	if(ver >= 35)
	{
		ReadWideStringData(f, h, TSaveLoadStreamData::TSLFieldType::ftU16, fieldName, str);
	}
	else
	{
		size_t w;
		char ch[1024];
		w = 0;
		f.readFn(&w, TSaveLoadStreamData::TSLFieldType::ftU16, fieldName + _T("sz"), h);
		if(w > 0)
		{
			TSaveLoadStreamCharArray a = {w, ch};
			f.readFn(&a, TSaveLoadStreamData::TSLFieldType::ftAnsiStringArray, fieldName, h);
		}
		ch[w] = '\0';
		str = UnicodeString(ch);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::DoThreadProcessProjLoad()
{
	__int32			dw_val, dw_val1;
	unsigned short  w_val;
	int				ld, ver;
	UnicodeString	string;
	bool			pattern_load_ok, found;
	TObjManager		*objManager = nullptr;

	TSaveLoadStreamHandle h;
	gspIOStreamData->readFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("MapEditorProjectData"), h);
	TSaveLoadStreamData& f = *gspIOStreamData;

	try
	{
		if(!h.IsValid())
		{
			throw Exception(_T("MapEditorProjectData section"));
		}

		proj_exist = map_exist = false;

		if(m_pObjManager != NULL)
		{
			m_pObjManager->ClearAll();
		}

		CreateMap(true);

		TabLevels->Tabs->Clear();
		TabLevels->Tabs->Add(_T("0"));

		{
			for(int i = 0; i < m_pLevelList->Count; i++)
			{
				delete (TObjManager*)m_pLevelList->Items[i];
			}
		}

		m_pLevelList->Clear();
		m_pObjManager = NULL;

		if(mainCommonRes != NULL)
		{
			delete mainCommonRes;
		}
		mainCommonRes = new TCommonResObjManager();
		CreateServiceData();

		{
			unsigned char ch_val[4];

			f.readFn(&ch_val[0], TSaveLoadStreamData::TSLFieldType::ftChar, _T("h1"), h);
			f.readFn(&ch_val[1], TSaveLoadStreamData::TSLFieldType::ftChar, _T("h2"), h);
			f.readFn(&ch_val[2], TSaveLoadStreamData::TSLFieldType::ftChar, _T("h3"), h);

			if(!(ch_val[0]=='m' && ch_val[1]== 'p' && ch_val[2] >= 15 /*FILEFORMAT_VER*/))
			{
				throw Exception(Localization::Text(_T("mNonCompatibleFileTypeError")));
			}

			ver = ch_val[2];
		}

		p_w = p_h = 0;
		d_w = d_h = 0;
		m_w = m_h = 0;
		m_px = m_py = -1;

		ReadAnsiStringData(f, h , _T("rawrespath"), string, ver);

		//____________settings:
		{
			unsigned char ch_val[4];
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("settings"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("settings section"));
			}

			//short	������ (W � H) ������ � ���� ������ ��������
			//readFn(&w_val,sizeof(short),1);
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("whiconsize"), h1);

			//BYTE	�������� ���� ������� �����
			//readFn(ch_val,1,1);
			f.readFn(&ch_val, TSaveLoadStreamData::TSLFieldType::ftChar, _T("objectborders"), h1);

			//BYTE 	�����
			//readFn(ch_val,1,1);
			f.readFn(&ch_val, TSaveLoadStreamData::TSLFieldType::ftChar, _T("grid"), h1);

			//short	������ ��������
			//readFn(&w_val,sizeof(short),1);
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("animdelay"), h1);

			//____________������� �����:
			//short	����
			//readFn(&w_val,sizeof(short),1);
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("lrcount"), h1);
			lrCount = w_val;

			//short	������ ��������� (W)
			//readFn(&w_val,sizeof(short),1);
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("pw"), h1);
			p_w = w_val;
			//short	������ ��������� (H)
			//readFn(&w_val,sizeof(short),1);
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("ph"), h1);
			p_h = w_val;
		}

		// ____________ grres:
		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("rawres"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("rawres section"));
			}
			gsStrLst = LoadResArray(f, h1, ver);
		}

		// ____________ bgobjects:
		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("bgobjects"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("bgobjects section"));
			}

			//WORD        ���������� ������ ���� (���������)
			//readFn(&w_val, sizeof(short), 1);
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("bgobjproperties"), h1);
			for(int i = 0; i < w_val; i++)
			{
				TSaveLoadStreamHandle h2;
				f.readFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("bgobjproperty_") + IntToStr(i), h2);
				if(!h2.IsValid())
				{
					throw Exception(_T("bgobjproperty_") + IntToStr(i));
				}
				TBGobjProperties *bp = new TBGobjProperties();
				bp->LoadFromFile(f, h2, ver);
				mainCommonRes->bgotypes->Add(bp);
			}
		}

		// ____________ mapbgobjs:
		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("mapbgobjs"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("mapbgobjs section"));
			}

			ImportBackObjPalette(f, h1, ver);
		}

		// ____________ states:
		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("states"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("states section"));
			}

			//readFn(&w_val,sizeof(short),1);
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("statescount"), h1);
			for(int i = 0; i < w_val; i++)
			{
				TSaveLoadStreamHandle h2;
				f.readFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("state_") + IntToStr(i), h2);
				if(!h2.IsValid())
				{
					throw Exception(_T("state_") + IntToStr(i));
				}
				TState* s = new TState();
				s->LoadFromFile(f, h2, ver);
				mainCommonRes->states->Add(s);
			}
		}

		// _________ stateslookuptable:
		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("stateslookuptable"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("stateslookuptable section"));
			}

			//WORD         ���������� ������������ ���������
			//readFn(&w_val,sizeof(short),1);
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("stateslookuptablecount"), h1);
			int j = w_val;
			for(int i = 0; i < j; i++)
			{
				TSaveLoadStreamHandle h2;
				f.readFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("stateslookup_") + IntToStr(i), h2);
				if(!h2.IsValid())
				{
					throw Exception(_T("stateslookup_") + IntToStr(i));
				}

				ReadAnsiStringData(f, h2, _T("name"), string, ver);
				mainCommonRes->stateOppositeTable->Add(string);
			}
		}

		// _________ animations:
		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("animations"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("animations section"));
			}

			//WORD        ���������� ����� ��������
			//readFn(&w_val,sizeof(short),1);
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("animationscount"), h1);
			int j = w_val;
			mainCommonRes->aniNameTypes->Clear();
			for(int i = 0; i < j; i++)
			{
				TSaveLoadStreamHandle h2;
				f.readFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("animation_") + IntToStr(i), h2);
				if(!h2.IsValid())
				{
					throw Exception(_T("animation_") + IntToStr(i));
				}

				ReadAnsiStringData(f, h2, _T("name"), string, ver);

				if(string.Pos(_T("ANIM_")) != 1)
				{
					string = _T("ANIM_") + string;
				}
				mainCommonRes->aniNameTypes->Add(string);
			}
		}

		// _________ events:
		if(ver > 26)
		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("events"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("events section"));
			}

			//WORD        ���������� ����� �������
			//readFn(&w_val,sizeof(short),1, gsIOStreamData);
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("eventscount"), h1);
			int j = w_val;
			mainCommonRes->eventNameTypes->Clear();
			for(int i = 0; i < j; i++)
			{
				TSaveLoadStreamHandle h2;
				f.readFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("event_") + IntToStr(i), h2);
				if(!h2.IsValid())
				{
					throw Exception(_T("event_") + IntToStr(i));
				}

				ReadAnsiStringData(f, h2, _T("name"), string, ver);
				mainCommonRes->eventNameTypes->Add(string);
			}
		}

		// _________ propertiestemplate:
		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("propertiestemplate"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("propertiestemplate section"));
			}

			mainCommonRes->MainGameObjPattern->loadFromFile(f, h1, ver, nullptr);
		}

		// ____________ frameobjects
		if(ver > 18)
		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("frameobjects"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("frameobjects section"));
			}

			LoadFrameObjInf(f, h1, ver);
        }

		// _________ gameobjpalette:
		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("gameobjpalette"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("gameobjpalette section"));
			}

			ImportGameObjPalette(f, h1, ver);
		}

		// _________ levels:
		{
            unsigned char ch[4];
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("levels"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("levels section"));
			}


			f.readFn(ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("levelscount"), h1);
			int m_ct = ch[0];
            int level_idx = 0;

			do
			{
				TSaveLoadStreamHandle h2;
				f.readFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("level_") + IntToStr(level_idx), h2);
				if(!h2.IsValid())
				{
					throw Exception(_T("level_") + IntToStr(level_idx));
				}

                level_idx++;

				objManager = new TObjManager(mainCommonRes);
				objManager->AssignMainPattern(mainCommonRes->MainGameObjPattern);

				ReadAnsiStringData(f, h2, _T("textspath"), string, ver);

				{
					unsigned char rgbch[4];

					f.readFn(&rgbch[0], TSaveLoadStreamData::TSLFieldType::ftChar, _T("r"), h);
					f.readFn(&rgbch[1], TSaveLoadStreamData::TSLFieldType::ftChar, _T("g"), h);
					f.readFn(&rgbch[2], TSaveLoadStreamData::TSLFieldType::ftChar, _T("b"), h);

					dw_val = 0;
					dw_val = rgbch[2];
					dw_val <<= 8;
					dw_val |= rgbch[1];
					dw_val <<= 8;
					dw_val |= rgbch[0];
				}

				//short	������ ������ � ���������� (W)
				//readFn(&w_val,sizeof(short),1);
				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("dw"), h2);
				if(d_w <= 0)
				{
					d_w = w_val * p_w;
				}
				//short	������ ������ � ���������� (H)
				//readFn(&w_val,sizeof(short),1);
				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("dh"), h2);
				if(d_h <= 0)
				{
					d_h = w_val * p_h;
				}

				//short	����
				//readFn(&w_val,sizeof(short),1);
				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("zones"), h2);
				objManager->znCount = w_val;

				//WORD	�������� ����
				short startzone;
				f.readFn(&startzone, TSaveLoadStreamData::TSLFieldType::ftS16, _T("activezone"), h2);
				objManager->startZone = startzone;

				//WORD        ���� >=0 ��������� ������ �� ���������� ����� (��� �������� � �������)
				//readFn(&w_val,sizeof(short),1);
				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("cameratileidx"), h2);
				objManager->tileCameraPosOnStart = (unsigned short)w_val;

				if(ver > 33)
				{
					//WORD         ������������ ������� � �������� "�� ����������� � �����������"
					//readFn(&w_val,sizeof(short),1);
					f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("ignoreunoptimizedresoures"), h2);
					objManager->ignoreUnoptimizedResoures = w_val > 0;
				}

				//__int32  ������ �����
				//readFn(&dw_val,sizeof(__int32),1);
				f.readFn(&dw_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("mpw"), h2);
				objManager->mp_w = dw_val;

				//__int32  ������ �����
				//readFn(&dw_val1,sizeof(__int32),1);
				f.readFn(&dw_val1, TSaveLoadStreamData::TSLFieldType::ftS32, _T("mph"), h2);
				objManager->mp_h = dw_val1;

				//BYTE[]   �����
				if(objManager->map != nullptr)
				{
					delete[] objManager->map;
					delete[] objManager->map_undo;
				}
				objManager->map = new unsigned short[objManager->mp_w*objManager->mp_h];
				objManager->map_undo = new unsigned short[objManager->mp_w*objManager->mp_h];

				{
					TSaveLoadStreamHandle h3;

					f.readFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("tiledata"), h3);

					size_t sz = objManager->mp_w * objManager->mp_h;
					if(ver == 11)
					{
						unsigned char *chmap = new unsigned char[sz];
						if(sz > 0)
						{
							TSaveLoadStreamCharArray a = {sz, chmap};
							f.readFn(&a, TSaveLoadStreamData::TSLFieldType::ftDataArray, _T("hex"), h3);
						}
						for(size_t tidx = 0; tidx < sz; tidx++)
						{
							objManager->map[tidx] = chmap[tidx];
						}
						delete[] chmap;
					}
					else
					{
						if(sz > 0)
						{
							TSaveLoadStreamCharArray a = {sz * sizeof(unsigned short), objManager->map};
							f.readFn(&a, TSaveLoadStreamData::TSLFieldType::ftDataArray, _T("hex"), h3);
						}
					}
				}

				memcpy(objManager->map_undo, objManager->map, objManager->mp_w * objManager->mp_h * sizeof(unsigned short));

				//____________mapobjects
				//  ���� ����� ����������� 3 ����--
				//  1-��� ��������� ������� �����
				//  2-��������
				//  3-��������� ��������� �����
				for(ld = 0; ld < 3; ld++)
				{
					TSaveLoadStreamHandle h3;

					TObjManager::TObjType tlist[3] =
					{
						TObjManager::BACK_DECORATION,
						TObjManager::CHARACTER_OBJECT,
						TObjManager::FORE_DECORATION
					};

					switch(tlist[ld])
					{
						case TObjManager::BACK_DECORATION:
							f.readFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("bgdecor"), h3);
						break;
						case TObjManager::CHARACTER_OBJECT:
							f.readFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("mapobjects"), h3);
						break;
						case TObjManager::FORE_DECORATION:
							f.readFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("fgdecor"), h3);
						break;
						default:
						break;
					}

					if(!h3.IsValid())
					{
						throw Exception(_T("mapobjects section"));
					}

					//WORD    ���������� �������� �� �����
					//readFn(&w_val,sizeof(short),1);
					{
						f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("mapobjectscount"), h3);
						int j = w_val;
						for(int i = 0; i < j; i++)
						{
							TSaveLoadStreamHandle h4;
							f.readFn(&h3, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("obj_") + IntToStr(i), h4);

							LoadGameObjectFrom(f, h4, ver, objManager);

							if(ver >= 35)
							{
								f.readFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftDebugCheckSum, _T("checksum"), h4);
							}
						}
					}
				}

				// ___________triggers/directors

				//  ���� ����� ����������� 2 ����--
				//  1-�������
				//  2-��������
				for(ld = 0; ld < 2; ld++)
				{
					TSaveLoadStreamHandle h3;

					TObjManager::TObjType tlist[2] =
					{
						TObjManager::TRIGGER, TObjManager::DIRECTOR
					};

					switch(tlist[ld])
					{
						case TObjManager::TRIGGER:
							f.readFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("triggers"), h3);
						break;
						case TObjManager::DIRECTOR:
							f.readFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("directors"), h3);
						break;
						default:
						break;
					}

					if(!h3.IsValid())
					{
						throw Exception(_T("mapobjects section"));
					}

					//TMapSpGPerson *sgp;
					//short	���������� �������� �� �����
					//readFn(&w_val, sizeof(short), 1);
					{
						f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("mapobjectscount"), h3);
						int j = w_val;
						for(int i = 0; i < j; i++)
						{
							TSaveLoadStreamHandle h4;
							f.readFn(&h3, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("obj_") + IntToStr(i), h4);

							LoadGameObjectFrom(f, h4, ver, objManager);

							if(ver >= 35)
							{
								f.readFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftDebugCheckSum, _T("checksum"), h4);
							}
						}
					}
				}

				// ____________collideshapes:

				if(ver > 17)
				{
					TSaveLoadStreamHandle h3;
					f.readFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("collideshapes"), h3);

					if(!h3.IsValid())
					{
						throw Exception(_T("collideshapes section"));
					}

					//short	����������
					//readFn(&w_val, sizeof(short), 1);
					f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("collideshapescount"), h3);
					int j = w_val;
					for(int i = 0; i < j; i++)
					{
						TSaveLoadStreamHandle h4;
						f.readFn(&h3, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("obj_") + IntToStr(i), h4);

						LoadGameObjectFrom(f, h4, ver, objManager);

						if(ver >= 35)
						{
							f.readFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftDebugCheckSum, _T("checksum"), h4);
						}
					}
				}

				//sounds
				if(ver >= 29)
				{
					bool locked = false;
					bool customvisible = true;

					TSaveLoadStreamHandle h3;
					f.readFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("soundscheme"), h3);

					if(!h3.IsValid())
					{
						throw Exception(_T("soundscheme section"));
					}

					if(ver >= 35)
					{
						unsigned char ch_val[2];
						f.readFn(ch_val, TSaveLoadStreamData::TSLFieldType::ftChar, _T("locked"), h3);
						locked = ch_val[0] != 0;
						f.readFn(ch_val, TSaveLoadStreamData::TSLFieldType::ftChar, _T("customvisible"), h3);
						customvisible = ch_val[0] != 0;
					}

					f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("soundscount"), h3);

					__int32 mss_x, mss_y;
					f.readFn(&mss_x, TSaveLoadStreamData::TSLFieldType::ftS32, _T("x"), h3);
					f.readFn(&mss_y, TSaveLoadStreamData::TSLFieldType::ftS32, _T("y"), h3);

					if(w_val > 0)
					{
						TMapSoundScheme* mss;
						mss = (TMapSoundScheme*)newGObject(objManager, objMAPSOUNDSCHEME,
															UnicodeString(SOUNDSCHEME_NAME), mss_x, mss_y,
																ZONES_INDEPENDENT_IDX, UnicodeString(SOUNDSCHEME_NAME),
																	ObjCreateModeLoading);

						mss->Lock(locked);
						mss->SetCustomVisible(customvisible);

						__int32 sndresct = w_val;
						for (int i = 0; i < sndresct; i++)
						{
							SoundSchemePropertyItem pi;
							TSaveLoadStreamHandle h4;

							f.readFn(&h3, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("sound_") + IntToStr(i), h4);

							if(!h4.IsValid())
							{
								throw Exception(_T("sound_") + IntToStr(i));
							}

							ReadAnsiStringData(f, h4, _T("name"), pi.mName, ver);

							ReadWideStringData(f, h4, TSaveLoadStreamData::TSLFieldType::ftU16, _T("path"), pi.mFileName);

							f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("type"), h4);
							pi.mType = static_cast<SoundSchemePropertyItem::SoundSchemeResourceType>(w_val);

							mss->AddItem(pi);
						}
					}

					if(ver >= 35)
					{
						f.readFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftDebugCheckSum, _T("checksum"), h3);
					}
				}

				//scripts
				{
					TSaveLoadStreamHandle h3;
					f.readFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("scripts"), h3);

					if(!h3.IsValid())
					{
						throw Exception(_T("scripts section"));
					}

					f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("scriptscount"), h3);
					/*
					//____________�������:
					// WORD    ���������� ��������
					readFn(&w_val,sizeof(short),1);
					for(i=0; i<w_val; i++)
					{
						TGScript *spt = objManager->CreateScript(NULL);
						spt->LoadFromFile(ver);
					}
					*/
				}

				ReadAnsiStringData(f, h2, _T("startscript"), objManager->StartScriptName, ver);

				{
					TSaveLoadStreamHandle h3;
					f.readFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("mapobjvarscount"), h3);

					if(!h3.IsValid())
					{
						throw Exception(_T("mapobjvarscount section"));
					}

					LoadObjVarValues(f, h3, mapObj, ver, objManager);

					if(ver >= 35)
					{
						f.readFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftDebugCheckSum, _T("checksum"), h3);
					}
				}

				{
					TSaveLoadStreamHandle h3;
					f.readFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("trigvarscount"), h3);

					if(!h3.IsValid())
					{
						throw Exception(_T("trigvarscount section"));
					}

					LoadObjVarValues(f, h3, triggerObj, ver, objManager);

					if(ver >= 35)
					{
						f.readFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftDebugCheckSum, _T("checksum"), h3);
					}
				}

				m_w = objManager->mp_w * p_w; //������������� ��� CreateMap
				m_h = objManager->mp_h * p_h;
				objManager->old_mp_w = objManager->mp_w;  //������������ ������������ �����
				objManager->old_mp_h = objManager->mp_h;
				objManager->old_md_w = objManager->mp_w / (d_w / p_w);
				objManager->old_md_h = objManager->mp_h / (d_h / p_h);
				if(objManager->old_md_w == 0 && objManager->mp_w > 0)
				{
					objManager->old_md_w = 1;
				}
				if(objManager->old_md_h == 0 && objManager->mp_h > 0)
				{
					objManager->old_md_h = 1;
				}

				objManager->CheckObjForWrongLinks(); // old bug protection

				AddNewLevelToProj(objManager);
				objManager = NULL;
				map_exist = true;
				m_ct--;
			}
			while(m_ct > 0);
		}

		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("gpersonvarscount"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("gpersonvarscount section"));
			}
			LoadObjVarValues(f, h1, parentObj, ver, nullptr);
		}

		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("textsdata"), h1);
			if(!h1.IsValid())
			{
				throw Exception(_T("textsdata section"));
			}
			LoadProjTexts(f, h1, ver);
		}

		pgGlobalPaternTabStrings->Clear();

		if(ver >= 21)
		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("objectstabs"), h1);

			if(!h1.IsValid())
			{
				throw Exception(_T("objectstabs section"));
			}

			//readFn(&w_val, sizeof(short), 1);
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("objectstabscount"), h1);
			int j = w_val;
			for(int i = 0; i < j; i++)
			{
				TSaveLoadStreamHandle h2;
				f.readFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("objectstab_") + IntToStr(i), h2);

				if(!h2.IsValid())
				{
					throw Exception(_T("objectstab_") + IntToStr(i));
				}

				ReadWideStringData(f, h2, TSaveLoadStreamData::TSLFieldType::ftU16, _T("name"), string);
				pgGlobalPaternTabStrings->Add(string);
			}
		}
	}
	catch(Exception &e)
	{
		proj_exist = map_exist = false;

		if(objManager != nullptr)
		{
			delete objManager;
		}

		for(int i = 0; i < m_pLevelList->Count; i++)
		{
			delete (TObjManager*)m_pLevelList->Items[i];
		}
		m_pLevelList->Clear();

		if(mainCommonRes != nullptr)
		{
			delete mainCommonRes;
		}
		mainCommonRes = new TCommonResObjManager();

		CreateServiceData();

		gsErrorString = Localization::Text(_T("mReadFileError")) + _T(": ") + e.Message;

		if(gsStrLst != nullptr)
		{
			gsStrLst->clear();
			delete gsStrLst;
			gsStrLst = nullptr;
		}
	}

	if(m_pObjManager != nullptr)
	{
		if(m_pObjManager->znCount >= 0)
		{
			ListBoxZone->ItemIndex = 0;
			znCurZone = 0;
		}
	}

#ifndef LINUX_WINE_VER
	FWait->Enabled = false;
	FWait->Close();
#endif
}
//---------------------------------------------------------------------------

static void LoadGameObjectEditorData(TSaveLoadStreamData& io, TSaveLoadStreamHandle &h, TGameObjectEditorData& d, unsigned char ver)
{
	if(ver >= 35)
	{
		unsigned char ch_val;
		io.readFn(&ch_val, TSaveLoadStreamData::TSLFieldType::ftChar, _T("locked"), h);
		d.locked = ch_val > 0;
		io.readFn(&ch_val, TSaveLoadStreamData::TSLFieldType::ftChar, _T("customvisible"), h);
		d.customvisible = ch_val > 0;
	}
}
//---------------------------------------------------------------------------

TMapPerson* __fastcall TForm_m::LoadGameObjectFrom(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, unsigned char ver, TObjManager* ipObjManager)
{
	__int32 zone;
	__int32 dw_val;
	BYTE objType;
	short w_val;
	UnicodeString name, f1, f2, remark, objName;
	TTransformProperties t;
    TGameObjectEditorData ed;

	remark = _T("");

	const TObjCreateMode mode = f.type == TSaveLoadStreamData::TSLDataType::PasteClipboard ? ObjCreateModeClipboard : ObjCreateModeLoading;

	if(!h.IsValid())
	{
		throw Exception(_T("LoadGameObjectFrom"));
	}

	f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("objtype"), h);
	objType = (BYTE)w_val;

	LoadGameObjectEditorData(f, h, ed, ver);

	switch(objType)
	{
		case objMAPCOLLIDELINE_OBSOLETE:
		{
			ReadAnsiStringData(f, h, _T("name"), name, ver);
			f.readFn(&dw_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("var1"), h);
			f.readFn(&dw_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("var2"), h);
			f.readFn(&dw_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("var3"), h);
			f.readFn(&dw_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("var4"), h);
			return nullptr;
		}

		case objMAPCOLLIDESHAPE:
		{
			__int32 x, y;

			ReadAnsiStringData(f, h, _T("name"), name, ver);

			//mode = &LoadFromFileFn == iReadFn ? ObjCreateModeLoading : ObjCreateModeClipboard;
			if (f.type == TSaveLoadStreamData::TSLDataType::PasteClipboard)
			{
				TMapPerson *obj = (TMapPerson*)ipObjManager->FindObjectPointer(name);
				if(obj != NULL)
				{
					ipObjManager->DeleteObject(obj);
				}
			}

			TCollideChainShape *sh = ipObjManager->CreateCollideChainShape();
			sh->SetName(name);

			//DWORD ���������� ����� collideshape
			f.readFn(&dw_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("nodescount"), h);
			dw_val = static_cast<short>(dw_val);
			for(__int32 i = 0; i < dw_val; i++)
			{
				TGameObjectEditorData ned;
				TSaveLoadStreamHandle h1;
				f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("node_") + IntToStr(i), h1);

				if(!h1.IsValid())
				{
					throw Exception(_T("node_") + IntToStr(i));
				}

				LoadGameObjectEditorData(f, h, ned, ver);

				f.readFn(&x, TSaveLoadStreamData::TSLFieldType::ftS32, _T("x"), h1);
				f.readFn(&y, TSaveLoadStreamData::TSLFieldType::ftS32, _T("y"), h1);
				TMapCollideLineNode* n = ipObjManager->CreateMapCollideNode((double)x, (double)y, &HorzScrollBar_Position, &VertScrollBar_Position, sh->Name);
				n->ApplyCoordinates();
				n->Lock(ned.locked);
				n->SetCustomVisible(ned.customvisible);
			}

			if (f.type == TSaveLoadStreamData::TSLDataType::PasteClipboard)
			{
				Sleep(100);
				ipObjManager->MarkCrosslines(sh);
			}

			return sh;
		}

		case objMAPOBJ:
		{
			TMapGPerson *gp;
			double x, y;
            char ch;

			ReadAnsiStringData(f, h, _T("parentname"), name, ver);
			ReadAnsiStringData(f, h, _T("name"), objName, ver);

            zone = 0;
			f.readFn(&zone, TSaveLoadStreamData::TSLFieldType::ftS16, _T("zone"), h);

			t.Load(f, h, ver);

			f.readFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("activelink"), h);
			ReadAnsiStringData(f, h, _T("director"), f1, ver);
			ReadAnsiStringData(f, h, _T("directorfield"), f2, ver);
			ReadAnsiStringData(f, h, _T("memo"), remark, ver);

			t.GetPosition(x, y);
			gp = (TMapGPerson*)newGObject(ipObjManager, objType, name, x, y, zone, objName, mode);

			if(gp != nullptr)
			{
                __int32 i;

				gp->NextLinkedDirector = f1;
				gp->setRemarks(remark);

				ReadAnsiStringData(f, h, _T("state"), gp->State_Name, ver);

				if(ver == 15 && !name.IsEmpty())
				{
					GPerson *tgp = (GPerson *)mainCommonRes->GetParentObjByName(name);
					if(tgp->IsDecoration)
					{
						TState* s = (TState*)mainCommonRes->states->Items[0];
						gp->State_Name = s->animation;
					}
				}

				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("statescount"), h);
				dw_val = w_val;
				for(i = 0; i < dw_val; i++)
				{
					TSaveLoadStreamHandle h1;
					f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("state_") + IntToStr(i), h1);

					if(!h1.IsValid())
					{
						throw Exception(_T("state_") + IntToStr(i));
					}

					ReadAnsiStringData(f, h1, _T("name"), f1, ver);
					gp->addDirStateItem(f1);
				}

				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("scriptscount"), h);
				dw_val = w_val;
				for(i = 0; i < dw_val; i++)
				{
					TSaveLoadStreamHandle h1;
					f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("script_") + IntToStr(i), h1);

					if(!h1.IsValid())
					{
						throw Exception(_T("script_") + IntToStr(i));
					}

					ReadAnsiStringData(f, h1, _T("action"), f1, ver);
					ReadAnsiStringData(f, h1, _T("script"), f2, ver);
					gp->setScriptName(f1, f2);
				}

				ReadAnsiStringData(f, h, _T("objlink"), gp->ParentObj, ver);

				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("objlinkcount"), h);
				dw_val = w_val;
				for(i = 0; i < dw_val; i++)
				{
					TSaveLoadStreamHandle h1;
					f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("objlink_") + IntToStr(i), h1);

					if(!h1.IsValid())
					{
						throw Exception(_T("objlink_") + IntToStr(i));
					}

					ReadAnsiStringData(f, h1, _T("name"), f1, ver);
					gp->AddChild(i, f1);
				}

				gp->Lock(ed.locked);
				gp->SetCustomVisible(ed.customvisible);
			}
			return gp;
		}

		case objMAPTEXTBOX:
		{
			TMapTextBox *tgp;
			double x, y;

			ReadAnsiStringData(f, h, _T("parentname"), name, ver);
			ReadAnsiStringData(f, h, _T("name"), objName, ver);

            zone = 0;
			f.readFn(&zone, TSaveLoadStreamData::TSLFieldType::ftS16, _T("zone"), h);

			t.Load(f, h, ver);

			ReadAnsiStringData(f, h, _T("memo"), remark, ver);

			t.GetPosition(x, y);
			tgp = (TMapTextBox*)newGObject(ipObjManager, objType, name, x, y, zone, objName, mode);

			if(tgp != nullptr)
			{
				__int32 sc, cc;

				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("txttype"), h);
				tgp->SetTextType((TMapTextBox::TMapTexBoxType)w_val);

				ReadAnsiStringData(f, h, _T("txtid"), f1, ver);
				tgp->SetTextId(f1);

				ReadAnsiStringData(f, h, _T("font"), f1, ver);
				tgp->SetFontName(f1);

				f.readFn(&dw_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("rbgabackdrop"), h);
				{
					unsigned char txtcr1 = static_cast<unsigned char>(dw_val);
					unsigned char txtcg1 = static_cast<unsigned char>(dw_val >> 8);
					unsigned char txtcb1 = static_cast<unsigned char>(dw_val >> 16);
					unsigned char txtcr2 = static_cast<unsigned char>(clTRANSPARENT);
					unsigned char txtcg2 = static_cast<unsigned char>(clTRANSPARENT >> 8);
					unsigned char txtcb2 = static_cast<unsigned char>(clTRANSPARENT >> 16);
					if(txtcr1 == txtcr2 && txtcg1 == txtcg2 && txtcb1 == txtcb2)
					{
						dw_val = clTRANSPARENT;
					}
				}
				tgp->SetFillColor(TColor(dw_val));

				f.readFn(&dw_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("alignment"), h);
				tgp->SetAlignment((TextAlignment)dw_val);

				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("wcanvas"), h);
				tgp->SetTextWidth(w_val);

				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("hcanvas"), h);
				tgp->SetTextHeight(w_val);

				if(25 <= ver)
				{
					sc = 0;
					f.readFn(&sc, TSaveLoadStreamData::TSLFieldType::ftU16, _T("dynamictextstringscount"), h);
					cc = 0;
					f.readFn(&cc, TSaveLoadStreamData::TSLFieldType::ftU16, _T("dynamictextcharsperstring"), h);
					tgp->SetDynamicTextSizes(sc, cc);
				}

				if(26 <= ver)
				{
					ReadAnsiStringData(f, h, _T("objlink"), tgp->ParentObj, ver);
				}

				tgp->Lock(ed.locked);
				tgp->SetCustomVisible(ed.customvisible);
			}
			return tgp;
		}

		case objMAPSOUNDSCHEME:
		{
			__int32 i;
			TMapSoundScheme *ssgp;
			double x, y;

			t.Load(f, h, ver);

			t.GetPosition(x, y);
			ssgp = (TMapSoundScheme*)newGObject(ipObjManager, objType,
												UnicodeString(SOUNDSCHEME_NAME), x, y,
													ZONES_INDEPENDENT_IDX, UnicodeString(SOUNDSCHEME_NAME),
														mode);
			__int32 sndresct = 0;
			f.readFn(&sndresct, TSaveLoadStreamData::TSLFieldType::ftU16, _T("soundscount"), h);
			for (i = 0; i < sndresct; i++)
			{
				SoundSchemePropertyItem pi;
				TSaveLoadStreamHandle h1;
				f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("sound_") + IntToStr(i), h1);

				if(!h1.IsValid())
				{
					throw Exception(_T("sound_") + IntToStr(i));
				}

				ReadAnsiStringData(f, h1, _T("name"), pi.mName, ver);

				ReadWideStringData(f, h1, TSaveLoadStreamData::TSLFieldType::ftU16, _T("path"), pi.mFileName);

				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("type"), h1);
				pi.mType = static_cast<SoundSchemePropertyItem::SoundSchemeResourceType>(w_val);

				ssgp->AddItem(pi);
			}

			ssgp->Lock(ed.locked);
			ssgp->SetCustomVisible(ed.customvisible);
			return ssgp;
		}

		case objMAPOBJPOOL:
		{
			__int32 i;
			TMapObjPool* opgp;
			double x, y;

			ReadAnsiStringData(f, h, _T("name"), objName, ver);

			zone = 0;
			f.readFn(&zone, TSaveLoadStreamData::TSLFieldType::ftS16, _T("zone"), h);

			t.Load(f, h, ver);

			t.GetPosition(x, y);
			opgp = (TMapObjPool*)newGObject(ipObjManager, objType, UnicodeString(OBJPOOL_NAME), x, y, zone, objName, mode);

			__int32 poolct = 0;
			f.readFn(&poolct, TSaveLoadStreamData::TSLFieldType::ftU16, _T("poolcount"), h);
			for (i = 0; i < poolct; i++)
			{
				ObjPoolPropertyItem pi;
				TSaveLoadStreamHandle h1;

				f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("pool_") + IntToStr(i), h1);

				if(!h1.IsValid())
				{
					throw Exception(_T("pool_") + IntToStr(i));
				}

				ReadAnsiStringData(f, h1, _T("name"), pi.mName, ver);

				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("objcount"), h1);
				pi.mCount = w_val;
				opgp->AddItem(pi);
			}

			opgp->Lock(ed.locked);
			opgp->SetCustomVisible(ed.customvisible);
			return opgp;
		}

		case objMAPTRIGGER:
		case objMAPDIRECTOR:
		{
			TMapSpGPerson *sgp;
			double x, y;

			ReadAnsiStringData(f, h, _T("name"), objName, ver);

			zone = 0;
			f.readFn(&zone, TSaveLoadStreamData::TSLFieldType::ftS16, _T("zone"), h);

			t.Load(f, h, ver);

			ReadAnsiStringData(f, h, _T("memo"), remark, ver);

			if(objType == objMAPTRIGGER)
				name = UnicodeString(TRIGGER_NAME);
			else
				name = UnicodeString(DIRECTOR_NAME);
			t.GetPosition(x, y);
			sgp = (TMapSpGPerson*)newGObject(ipObjManager, objType, name, x, y, zone, objName, mode);

			if(sgp != nullptr)
			{
				char ch;

				sgp->setRemarks(UnicodeString(remark));

				ReadAnsiStringData(f, h, _T("targetobj"), sgp->objName, ver);

				f.readFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("tilemapchangedirection"), h);
				sgp->mapDirection = ch;

				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("tilemapindexormathop"), h);
				sgp->mapIndex = w_val;

				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("tilemapchangecount"), h);
				sgp->mapCount = w_val;

				// (trMAP,trPROPERTIES)
				f.readFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("triggertype"), h);
				sgp->Type = ch;

				if(objType == objMAPTRIGGER)
				{
					TMapTrigger* trig;
					short k, int_val, len;
					trig = (TMapTrigger*)sgp;

					int_val = 0;
					f.readFn(&int_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("changestatescount"), h);
					len = int_val;
					for(k = 0; k < len; k++)
					{
						int_val = 0;
						TSaveLoadStreamHandle h1;
						f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("state_") + IntToStr(k), h1);
						if(!h1.IsValid())
						{
							throw Exception(_T("state_") + IntToStr(k));
						}
						ReadAnsiStringData(f, h1, _T("name"), name, ver);
						if(!name.IsEmpty())
						{
							trig->stateList->Add(name);
						}
					}

					int_val = 0;
					f.readFn(&int_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("actionscount"), h);
					len = int_val;
					for(k = 0; k < len; k++)
					{
						TSaveLoadStreamHandle h1;
						UnicodeString key, name;
						f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("action_") + IntToStr(k), h1);
						if(!h1.IsValid())
						{
							throw Exception(_T("action_") + IntToStr(k));
						}
						ReadAnsiStringData(f, h1, _T("action"), key, ver);
						ReadAnsiStringData(f, h1, _T("script"), name, ver);
						trig->setScriptName(key, name);
					}

					ReadAnsiStringData(f, h, _T("teleportobj"), trig->objTeleport, ver);

					int_val = 0;
					f.readFn(&int_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("teleportmaptileidx"), h);
					trig->tileTeleport = int_val;
				}
				else if(objType == objMAPDIRECTOR)
				{
					__int32 k;
					TMapDirector *dir = (TMapDirector*)sgp;

					for(k = 0; k < 4; k++)
					{
						TSaveLoadStreamHandle h1;

						f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("joinnode_") + IntToStr(k), h1);

						if(!h1.IsValid())
						{
							throw Exception(_T("changestate_") + IntToStr(k));
						}

						ReadAnsiStringData(f, h1, _T("name"), dir->nearNodes[k], ver);
					}

					ReadAnsiStringData(f, h, _T("nodetrigger"), dir->nodeTrigger, ver);

					if(ver > 26)
					{
						ReadAnsiStringData(f, h, _T("event"), dir->objTeleport, ver);
					}
				}
			}
			return sgp;
		}
	}

	return nullptr;
}
//---------------------------------------------------------------------------

std::list<TResourceItem> *__fastcall TForm_m::LoadResArray(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, int ver)
{
	__int32 i, j;
	unsigned short w_val;

	std::list<TResourceItem> *str_lst = new std::list<TResourceItem>;

	assert(h.IsValid());

	f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("rawrescount"), h);

	j = w_val;
	for(i = 0; i < j; i++)
	{
		__int32 ww, hh, b_ct;
		UnicodeString sname;
		TResourceItem it;

        TSaveLoadStreamHandle h1;
		f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("resitem_") + IntToStr(i), h1);

		if(!h1.IsValid())
		{
			throw Exception(_T("resitem_") + IntToStr(i));
		}

		ReadAnsiStringData(f, h1, _T("name"), sname, ver);

		// short	W ������� �����-���������
		f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("w"), h1);
		ww = w_val;

		// short	H
		f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("h"), h1);
		hh = w_val;
		// short	type
		if(ver >= 24)
		{
			//readFn(&w_val, sizeof(short), 1);
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("type"), h1);
			it.mType = static_cast<ResourceTypes>(w_val);
		}
		else
		{
			it.mType = RESOURCE_TYPE_IMAGE;
		}
		it.mName = sname;
		it.mWidth = (unsigned short)ww;
		it.mHeight = (unsigned short)hh;
		it.mSize = 0;
		str_lst->push_back(it);
	}
	return str_lst;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ImportBackObjPalette(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, int ver_file)
{
	(void)ver_file;

	unsigned short w_val;

	f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("mapbgobjcount"), h);
	int j = (unsigned short)w_val;

	for(int i = 0; i < j; i++)
	{
		TSaveLoadStreamHandle h1;

		f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("bgo_") + IntToStr(i), h1);

		GMapBGObj &mo = mainCommonRes->BackObj[i];

		if(!h1.IsValid())
		{
			TFrameObj *bo = new TFrameObj();
			bo->x = 0;
			bo->w = 0;
			bo->y = 0;
			bo->h = 0;
			bo->bgPropertyName = _T("");
			bo->setSourceRes(0, 0, -1, _T(""));
			mo.AssignFrameObj(bo, 0);

			continue;
		}

		f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("srccount"), h1);

		int k = w_val;
		for(int a = 0; a < k; a++)
		{
			unsigned char ch;
			__int32 x, y, width, height;
			UnicodeString sname, prp;
			TSaveLoadStreamHandle h2;

			f.readFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("src_") + IntToStr(a), h2);

			if(!h2.IsValid())
			{
				throw Exception(_T("src_") + IntToStr(a));
			}

			ReadAnsiStringData(f, h2, _T("name"), sname, ver_file);

			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("x"), h2);
			x = w_val;
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("y"), h2);
			y = w_val;
			f.readFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("w"), h2);
			width = ch;
			f.readFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("h"), h2);
			height = ch;

			ReadAnsiStringData(f, h2, _T("prp"), prp, ver_file);

			TFrameObj *bo = new TFrameObj();
			bo->x = (short)x;
			bo->w = (short)width;
			bo->y = (unsigned short)y;
			bo->h = (unsigned short)height;
			bo->bgPropertyName = prp;
			bo->setSourceRes(0, 0, -1, sname);
			mo.AssignFrameObj(bo, a);
		}

		ReadAnsiStringData(f, h1, _T("memo"), mo.Memo, ver_file);
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::ImportGameObjPalette(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, int ver_file)
{
	int i, j, hcount;
	unsigned short w_val;
	TList *synh_lst;
	GPerson *gp, *gp1;
	bool found;

	synh_lst = new TList();

	f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("gpersoncount"), h);
	j = w_val;
	for(i = 0; i < j; i++)
	{
		unsigned char ch;
		UnicodeString gpid;
		TSaveLoadStreamHandle h1;

		f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("gperson_") + IntToStr(i), h1);

		if(!h1.IsValid())
		{
			throw Exception(_T("gperson_") + IntToStr(i));
		}

		ReadAnsiStringData(f, h1, _T("name"), gpid, ver_file);

		gp = new GPerson();
		gp->SetName(GPerson::IdToName(gpid));

		mainCommonRes->AssignObjMainPattern(gp);

		if(ver_file >= 4)
		{
			f.readFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("resoptimization"), h1);
			gp->ResOptimization = ch == 1;
			f.readFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("usegraphics"), h1);
			gp->UseGraphics = ch == 1;
		}

		{
			__int32 animCount = 0;
			f.readFn(&animCount, TSaveLoadStreamData::TSLFieldType::ftU16, _T("animationscount"), h1);
			for(__int32 a = 0; a < animCount; a++)
			{
				UnicodeString aniname;
				TSaveLoadStreamHandle h2;

				f.readFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("animation_") + IntToStr(a), h2);

				if(!h2.IsValid())
				{
                    continue;
				}

				ReadAnsiStringData(f, h2, _T("name"), aniname, ver_file);

				w_val = 0;
				f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("loop"), h2);

				if(ver_file >= 33)
				{
					unsigned short val = 0;
					f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("joinnodescount"), h2);
					Form_m->mpEdPrObjWndParams->joinNodesCount = val;
				}
				else
				{
				   Form_m->mpEdPrObjWndParams->joinNodesCount = 3;
				}

				__int32 frameCount = 0;
				f.readFn(&frameCount, TSaveLoadStreamData::TSLFieldType::ftU16, _T("framescount"), h2);

				TAnimation *animation = new TAnimation(frameCount);

				animation->SetName(aniname);
				animation->SetLooped((bool)w_val);

				for(__int32 fr = 0; fr < frameCount; fr++)
				{
					std::list<TAnimationFrameData> flst;
					TAnimationFrame aframe;
					TSaveLoadStreamHandle h3;
					short old_alpha = 255;

					f.readFn(&h2, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("frame_") + IntToStr(fr), h3);

					if(!h3.IsValid())
					{
						throw Exception(_T("frame_") + IntToStr(fr));
					}

					__int32 framePartsCount = 0;
					f.readFn(&framePartsCount, TSaveLoadStreamData::TSLFieldType::ftU16, _T("framepartscount"), h3);

					if(ver_file >= 22)
					{
						unsigned short val;
						f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("duration"), h3);
						aframe.SetDuration(val);

						if(ver_file >= 23 && ver_file < 28)
						{
							f.readFn(&old_alpha, TSaveLoadStreamData::TSLFieldType::ftU16, _T("oldalpha"), h3);
						}
						if(ver_file >= 27)
						{
							UnicodeString eventname;
							ReadAnsiStringData(f, h3, _T("event"), eventname, ver_file);
							aframe.SetEventId(eventname);
						}
					}

					for(__int32 frp = 0; frp < framePartsCount; frp++)
					{
						short val;
						TAnimationFrameData afd;
						TSaveLoadStreamHandle h4;

						f.readFn(&h3, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("framepart_") + IntToStr(frp), h4);

						if(!h4.IsValid())
						{
							throw Exception(_T("framepart_") + IntToStr(frp));
						}

						if(ver_file < 31)
						{
							f.readFn(&afd.frameId, TSaveLoadStreamData::TSLFieldType::ftS32, _T("hashid"), h4);
						}
						else
						{
							assert(sizeof(hash_t) == sizeof(__int32));
							//readFn(&afd.frameId, sizeof(hash_t), 1);
							f.readFn(&afd.frameId, TSaveLoadStreamData::TSLFieldType::ftS32, _T("hashid"), h4);
						}

						afd.type = LogicObjType::UOP_LOGIC_OBJ_BMP;

						val = 0;
						f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("offx"), h4);
						afd.posX = (double)val;

						val = 0;
						f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("offy"), h4);
						afd.posY = (double)val;

						val = 0;
						f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("rw"), h4);
						afd.rw = val;

						val = 0;
						f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("rh"), h4);
						afd.rh = val;

						if(ver_file >= 28)
						{
							f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("alpha"), h4);
							afd.a = (unsigned char)val;
						}

						val = 0;
						f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("transformdata"), h4);
						afd.setData((unsigned short)val);

						if(ver_file >= 28)
						{
							ReadAnsiStringData(f, h4, _T("streamfile"), afd.strm_filename, ver_file);
						}

						if(afd.strm_filename.IsEmpty())
						{
							CheckForWrongHash(afd);
						}

						flst.push_back(afd);
					}

					// colliderect
					{
						TRect r;
						short val;

						val = 0;
						f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("colliderectw"), h3);
						r.Right = val;

						val = 0;
						f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("colliderecth"), h3);
						r.Bottom = val;

						val = 0;
						f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("colliderectoffx"), h3);
						r.Left = val;

						val = 0;
						f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("colliderectoffy"), h3);
						r.Top = val;

						r.Right += r.Left;
						r.Bottom += r.Top;
						aframe.CreateFromList(flst);

						if(ver_file >= 23 && ver_file < 28)
						{
							aframe.SetAlpha((unsigned char)old_alpha);
						}

						aframe.SetCollideRect(r);
                    }

					TPoint pt;
					for(__int32 n = 0; n < Form_m->mpEdPrObjWndParams->joinNodesCount; n++)
					{
						short val;
						TSaveLoadStreamHandle h4;

						f.readFn(&h3, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("joinnode_") + IntToStr(n), h4);

						if(!h4.IsValid())
						{
							throw Exception(_T("joinnode_") + IntToStr(n));
						}

						val = 0;
						f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("offx"), h4);
						pt.x = val;

						val = 0;
						f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("offy"), h4);
						pt.y = val;

						aframe.SetJoinNodeCoordinate(n, pt);
					}

					if(!animation->SetFrame(aframe, fr))
					{
						aframe.Clear();
					}
				}

				// viewrect
				{
					TRect r;
					short val;

					val = 0;
					f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("viewrectw"), h2);
					r.Right = val;

					val = 0;
					f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("viewrecth"), h2);
					r.Bottom = val;

					val = 0;
					f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("viewrectoffx"), h2);
					r.Left = val;

					val = 0;
					f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("viewrectoffy"), h2);
					r.Top = val;

					r.Right += r.left;
					r.Bottom += r.top;

					animation->SetViewRect(r);
				}

				gp->AssignAnimation(animation);
			}

			f.readFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("isdecoration"), h1);
			gp->IsDecoration = ch > 0;

			f.readFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("decorationtype"), h1);
			gp->DecorType = static_cast<TMapObjDecorType>(ch);
			if(!gp->IsDecoration)
			{
				gp->DecorType = decorNONE;
			}

			ReadAnsiStringData(f, h1, _T("defaultstate"), gp->DefaultStateName, ver_file);

			gp->mGroupId = 1;
			f.readFn(&gp->mGroupId, TSaveLoadStreamData::TSLFieldType::ftChar, _T("grouptabindex"), h1);

			w_val = 0;
			f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("iconfragmentscount"), h1);
			if(w_val > 0)
			{
				std::list<TAnimationFrameData> lst;
				TAnimationFrame *aframe = new TAnimationFrame();
				for(__int32 frp = 0; frp < w_val; frp++)
				{
					short val;
					TAnimationFrameData afd;
					TSaveLoadStreamHandle h2;

					f.readFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("iconfragment_") + IntToStr(frp), h2);

					if(!h2.IsValid())
					{
						throw Exception(_T("iconfragment_") + IntToStr(frp));
					}

					if(ver_file < 31)
					{
						f.readFn(&afd.frameId, TSaveLoadStreamData::TSLFieldType::ftS32, _T("hashid"), h2);
					}
					else
					{
						assert(sizeof(hash_t) == sizeof(__int32));
						//readFn(&afd.frameId, sizeof(hash_t), 1);
						f.readFn(&afd.frameId, TSaveLoadStreamData::TSLFieldType::ftS32, _T("hashid"), h2);
					}

					afd.type = LogicObjType::UOP_LOGIC_OBJ_BMP;
					CheckForWrongHash(afd);

					val = 0;
					f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("offx"), h2);
					afd.posX = (double)val;

					val = 0;
					f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftS16, _T("offy"), h2);
					afd.posY = (double)val;

					val = 0;
					f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("rw"), h2);
					afd.rw = val;

					val = 0;
					f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("rh"), h2);
					afd.rh = val;

					lst.push_back(afd);
				}
				aframe->CreateFromList(lst);
				gp->SetIconData(aframe);
				delete aframe;
			}
		}

		synh_lst->Add(gp);
	}

	for(i = 0; i < mainCommonRes->GParentObj->Count; i++)
	{
		gp = (GPerson *)mainCommonRes->GParentObj->Items[i];
		found = false;
		if(gp == NULL)
		{
			continue;
		}
		for(j = 0; j < synh_lst->Count; j++)
		{
			gp1 = (GPerson *)synh_lst->Items[j];
			if(gp->GetName().Compare(gp1->GetName()) == 0)
			{
				found = true;
				break;
			}
		}
		if(!found)
		{
			synh_lst->Add(gp);
			mainCommonRes->GParentObj->Items[i] = NULL;
		}
	}

	for(j = 0; j<mainCommonRes->GParentObj->Count; j++)
	{
		if(mainCommonRes->GParentObj->Items[j] != NULL)
		{
			delete (GPerson *)mainCommonRes->GParentObj->Items[j];
		}
	}

	delete mainCommonRes->GParentObj;
 	mainCommonRes->GParentObj = synh_lst;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::CheckForWrongHash(TAnimationFrameData& afd)
{
	if(mainCommonRes->GetBitmapFrame(afd.frameId) == NULL)
	{
		const __int32 fr_ct = mainCommonRes->GetBitmapFrameCount();
		for(__int32 i = 0; i < fr_ct; i++)
		{
			const TFrameObj* obj = mainCommonRes->GetBitmapFrameByIdx(i);
			hash_t initial_hash;
			if(obj->GetInitialHash(initial_hash) == true)
			{
				if(initial_hash == afd.frameId)
				{
					afd.SetFrameId(obj->getHash());
					return;
				}
			}
		}

		throw Exception(_T("CheckForWrongHash exception"));
	}
}
//---------------------------------------------------------------------------
 /*
int __fastcall TForm_m::LoadFrameObjInfOld(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, hash_t **harray, int ver_file)
{
	unsigned short w_val;
	int j, k, x, y, w, h, ct;
	UnicodeString sname;
	TFrameObj *bo;

	f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftWord, _T("frameobjectscount"), h);
	k = w_val;
	if(k > 0)
	{
		(*harray) = new hash_t[k];
	}

	for(j = 0; j < k; j++)
	{
		TSaveLoadStreamHandle h1;

		f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("frameobject_") + IntToStr(j), h1);

		if(!h1.IsValid())
		{
			throw Exception(_T("frameobject_") + IntToStr(j));
		}

		readFn(&w_val, sizeof(short), 1);
		f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftWord, _T("frameobjectscount"), h1);
		ct = w_val;

		if(ct > 0)
		{
			readFn(ch_val, 1, ct);
		}
		if(ct != 0)
		{
			ch_val[ct] = 0;
			sname = UnicodeString((char*)ch_val);
			if(sname.Compare(_T("dir")) == 0 || sname.Compare(_T("trig")) == 0)
			{
				sname = _T("");
			}
		}
		else
		{
			sname = _T("");
		}
		// short	        X ��������� �� ���������
		readFn(&w_val, sizeof(short), 1);
		x = w_val;
		// short	        Y
		readFn(&w_val, sizeof(short), 1);
		y = w_val;
		if(ver_file < 17)
		{
			//BYTE	        W
			w_val = 0;
			readFn(&w_val, sizeof(char), 1);
			w = w_val;
			//BYTE	        H
			w_val = 0;
			readFn(&w_val, sizeof(char), 1);
			h = w_val;
		}
		else
		{
			//WORD	        W
			readFn(&w_val, sizeof(short), 1);
			w = w_val;
			//WORD	        H
			readFn(&w_val, sizeof(short), 1);
			h = w_val;
		}
		if(!sname.IsEmpty())
		{
			TFrameObj bo;
			bo.y = (short)y;
			bo.x = (short)x;
			bo.w = (unsigned short)w;
			bo.h = (unsigned short)h;
			bo.setSourceRes(0, 0, -1, sname); //�������������� ��������� ��� �������
			hash_t h = mainCommonRes->AddBitmapFrame(bo);
			(*harray)[j] = h;
		}
		else
		{
			(*harray)[j] = 0;
		}
	}

	return k;
}
//---------------------------------------------------------------------------
*/

void __fastcall TForm_m::LoadFrameObjInf(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, int ver_file)
{
	int j, k, ct;
	UnicodeString sname;

    k = 0;
	f.readFn(&k, TSaveLoadStreamData::TSLFieldType::ftU16, _T("frameobjectscount"), h);
	for(j = 0; j < k; j++)
	{
		TSaveLoadStreamHandle h1;
		hash_t hash_validation;
		TFrameObj bo;
		bool valid = false;
		sname = _T("");
		ct = 0;

		f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("fo_") + IntToStr(j), h1);

		if(!h1.IsValid())
		{
			throw Exception(_T("fo_") + IntToStr(j));
		}

		if(ver_file >= 31)
		{
			assert(sizeof(hash_t) == sizeof(__int32));
			//readFn(&afd.frameId, sizeof(hash_t), 1);
			f.readFn(&hash_validation, TSaveLoadStreamData::TSLFieldType::ftS32, _T("hashid"), h1);
		}

		ReadAnsiStringData(f, h1, _T("src"), sname, ver_file);
		valid = !sname.IsEmpty();

		bo.x = 0;
		f.readFn(&bo.x, TSaveLoadStreamData::TSLFieldType::ftU16, _T("x"), h1);

		bo.y = 0;
		f.readFn(&bo.y, TSaveLoadStreamData::TSLFieldType::ftU16, _T("y"), h1);

		bo.w = 0;
		f.readFn(&bo.w, TSaveLoadStreamData::TSLFieldType::ftU16, _T("w"), h1);

		bo.h = 0;
		f.readFn(&bo.h, TSaveLoadStreamData::TSLFieldType::ftU16, _T("h"), h1);

		bo.setSourceRes(0, 0, -1, sname);

		if(ver_file >= 20)
		{
			ReadAnsiStringData(f, h1, _T("group"), bo.groupId, ver_file);
		}

		if(valid)
		{
			mainCommonRes->AddBitmapFrame(bo);
		}

		if(ver_file >= 31)
		{
			bo.SetInitialHash(hash_validation);
		}
	}
}
//---------------------------------------------------------------------------

bool __fastcall TForm_m::LoadObjVarValues(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, int obj_type, int ver, TObjManager *objManager)
{
	int objCount;
	double f_val;
	unsigned short w_val;
	GPersonProperties *prp;

	switch(obj_type)
	{
		case mapObj:
			objCount = objManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT);
		break;
		case parentObj:
			objCount = mainCommonRes->GParentObj->Count;
		break;
		case triggerObj:
			objCount = objManager->GetObjTypeCount(TObjManager::TRIGGER);
		break;
		default:
			return false;
	}

	const std::list<PropertyItem>* lvars = mainCommonRes->MainGameObjPattern->GetVars();

	if(ver >= 4)
	{
		f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("varscount"), h);
    }
	else
	{
		w_val = (unsigned short)lvars->size();
	}

	for(int i = 0; i < objCount; i++)
	{
		UnicodeString name;
		TSaveLoadStreamHandle h1;

		switch(obj_type)
		{
			case mapObj:
			{
				TMapGPerson* mp = static_cast<TMapGPerson*>(objManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, i));
				//sound scheme �� �������� ��������
				if (mp->GetObjType() == objMAPSOUNDSCHEME)
				{
					continue;
				}
				name = mp->Name;
				prp = &mp->GetProperties();
			}
			break;

			case parentObj:
			{
				GPerson* obj = static_cast<GPerson*>(mainCommonRes->GParentObj->Items[i]);
				name = GPerson::IdToName(obj->GetName());
				prp = &obj->GetBaseProperties();
			}
			break;

			case triggerObj:
			{
				TMapSpGPerson* obj = static_cast<TMapSpGPerson*>(objManager->GetObjByIndex(TObjManager::TRIGGER, i));
				name = obj->Name;
				prp = &obj->GetProperties();
            }
			break;

			default:
				prp = nullptr;
				name = _T("");
				break;
		}

		f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, name, h1);

		if(!h1.IsValid())
		{
            continue;
		}

		std::list<PropertyItem>::const_iterator iv;

		for (iv = lvars->begin(); iv != lvars->end(); ++iv)
		{
			TSaveLoadStreamHandle h2;

			f.readFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, iv->name, h2);

			if(!h2.IsValid())
			{
				throw Exception(iv->name + " section");
			}

			//BYTE[]      ������������������ ����=(���������� �������� �� �����*��������� ����� ������� ������� �����)
			// ...	������� � ����� �����, ��� ����, ���� ����� ������ �� ������ �����������, ���-�� ���� ���-�� ���������
			// ...	������������������ ��������� � ������������������� �������� �� �����
			if(ver < 19)
			{
				__int32 ival = 0;
				f.readFn(&ival, TSaveLoadStreamData::TSLFieldType::ftS32, _T("value"), h2);
				f_val = static_cast<double>(ival);
			}
			else
			{
				f.readFn(&f_val, TSaveLoadStreamData::TSLFieldType::ftDouble, _T("value"), h2);
			}

			prp->setVarValue(f_val, iv->name);

			if(obj_type == triggerObj)
			{
				char val;
				f.readFn(&val, TSaveLoadStreamData::TSLFieldType::ftChar, _T("checked"), h2);
				prp->setChecked(val > 0, iv->name);
			}
		}
	}

	return true;
}
//---------------------------------------------------------------------------

void __fastcall TForm_m::LoadProjTexts(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, int ver)
{
	int ct;
	TStringList* list, *langlist;
	TUnicodeText* text;
	UnicodeString lang;

	wchar_t* wtxt;

	if(ver < 18)
	{
        return;
    }

	ct = 0;
	f.readFn(&ct, TSaveLoadStreamData::TSLFieldType::ftChar, _T("langcount"), h);
	if(ct == 0)
	{
		return;
	}

	ct = 0;
	f.readFn(&ct, TSaveLoadStreamData::TSLFieldType::ftU16, _T("stringscount"), h);
	if(ct == 0)
	{
		return;
	}

	list = new TStringList();
	langlist = new TStringList();
	mpTexts->GetLanguages(langlist);

	for(int j = 0; j < ct; j++)
	{
		TSaveLoadStreamHandle h1;

		f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("string_") + IntToStr(j), h1);

		if(!h1.IsValid())
		{
			throw Exception(_T("string_") + IntToStr(j));
		}

		UnicodeString wstr;
		ReadWideStringData(f, h1, TSaveLoadStreamData::TSLFieldType::ftChar, _T("id"), wstr);
		list->Add(wstr);
	}

	for(int i = 0; i < MAX_LANGUAGES; i++)
	{
		TSaveLoadStreamHandle h1;
		lang = _T("");

		f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("lang_") + IntToStr(i), h1);

		if(!h1.IsValid())
		{
			throw Exception(_T("lang_") + IntToStr(i));
		}

		ReadWideStringData(f, h1, TSaveLoadStreamData::TSLFieldType::ftChar, _T("langname"), lang);

		if(!lang.IsEmpty())
		{
			langlist->Strings[i] = lang;
			mpTexts->SetLanguages(langlist);
			if(langlist->Count > 0)
			{
				mCurrentLanguage = langlist->Strings[0];
			}
			else
			{
				mCurrentLanguage = TEXT_NAME_DEFAULT;
			}

			for(int j = 0; j < ct; j++)
			{
				TSaveLoadStreamHandle h2;

				f.readFn(&h1, TSaveLoadStreamData::TSLFieldType::ftNewSection, list->Strings[j], h2);

				if(!h2.IsValid())
				{
					throw Exception(list->Strings[j]);
				}

				TUnicodeText text;
				//BYTE margin
				//DWORD back color
				//DWORD alignment
				//WORD width
				//WORD height
				//WORD font name char count
				//BYTES[] font name
				//WORD strings count
				//{
				  //WORD string char count
				  //BYTES[] string (unicode)
				//}
				text.Load(f, h2, (char)ver);
				mpTexts->AddText(text, list->Strings[j], lang);
			}
		}
	}

	delete langlist;
	delete list;
}
//---------------------------------------------------------------------------

