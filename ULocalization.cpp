//---------------------------------------------------------------------------

#pragma hdrstop

#include "ULocalization.h"
#include "MainUnit.h"
#include <ValEdit.hpp>

#include "pugiconfig.hpp"
#include "pugixml.hpp"

//---------------------------------------------------------------------------
#pragma package(smart_init)

std::map<const UnicodeString, UnicodeString> Localization::mMap;
std::map<const UnicodeString, UnicodeString> Localization::mLangDict;
std::map<const UnicodeString, UnicodeString> Localization::mResDict;
UnicodeString Localization::mTempStr;

#define DEFAULT_RES_NAME _T("LOCALIZATION_ENG")

//---------------------------------------------------------------------------

void __fastcall Localization::Init()
{
    const __int32 resCount = 2;
	const _TCHAR gsResList[resCount][64] =
	{
		{DEFAULT_RES_NAME},
		{_T("LOCALIZATION_RU")}
	};

	mLangDict.clear();
	mResDict.clear();
	for(int i = 0; i < resCount; i++)
	{
		TResourceStream* resStream = new TResourceStream((unsigned int)HInstance, gsResList[i], RT_RCDATA);
		resStream->Position = 0;

		pugi::xml_document doc;
		pugi::xml_parse_result result;

		{
			const int bsize = resStream->Size;
			const int wzise = bsize / sizeof(wchar_t) + 1;
			wchar_t* data = new wchar_t[wzise];
			resStream->Read(data, bsize);
			data[wzise - 1] = L'\0';
			std::string utf8data = pugi::as_utf8(data);
			delete[] data;

			result = doc.load_buffer(utf8data.c_str(), utf8data.length());
		}

		if(result.status == pugi::status_ok && !doc.empty())
		{
			pugi::xml_attribute alias = doc.first_child().attribute("alias");
			pugi::xml_attribute text = doc.first_child().attribute("text");
			if(!alias.empty() && !text.empty())
			{
				UnicodeString a = UnicodeString(pugi::as_wide(alias.value()).c_str());
				mLangDict[a] = UnicodeString(pugi::as_wide(text.value()).c_str());
				mResDict[a] = UnicodeString(gsResList[i]);
			}
		}
		delete resStream;
	}
}
//---------------------------------------------------------------------------

void __fastcall Localization::Release()
{
  mMap.clear();
  mLangDict.clear();
  mResDict.clear();
}
//---------------------------------------------------------------------------

const UnicodeString& __fastcall Localization::Text(const UnicodeString& alias)
{
  std::map<const UnicodeString, UnicodeString>::const_iterator it;
  it = mMap.find(alias);
  if (it != mMap.end())
  {
    return mMap[alias];
  }
  return alias;
}
//---------------------------------------------------------------------------

const UnicodeString& __fastcall Localization::TextWithVar(const UnicodeString& alias, const UnicodeString& var)
{
  std::map<const UnicodeString, UnicodeString>::const_iterator it;
  it = mMap.find(alias);
  if (it != mMap.end())
  {
    mTempStr = mMap[alias];
    const __int32 pos = mTempStr.Pos(_T("%s"));
    if(pos != 0)
    {
      const __int32 ln = mTempStr.Length();
      mTempStr = mTempStr.SubString(0, pos - 1) + var + mTempStr.SubString(pos + 2, ln - pos - 1);
      return mTempStr;
    }
  }
  return alias;
}
//---------------------------------------------------------------------------

void __fastcall Localization::GetLangAliasList(TStringList* list)
{
	assert(list);
	list->Clear();
	std::map<const UnicodeString, UnicodeString>::const_iterator it;
	for (it = mLangDict.begin(); it != mLangDict.end(); ++it)
	{
		list->Add(it->first);
	}
}
//---------------------------------------------------------------------------

const UnicodeString& __fastcall Localization::GetLangName(const UnicodeString& alias_name)
{
  std::map<const UnicodeString, UnicodeString>::const_iterator it;
  it = mLangDict.find(alias_name);
  if (it != mLangDict.end())
  {
	return it->second;
  }
  mTempStr = _T("");
  return mTempStr;
}
//---------------------------------------------------------------------------

void __fastcall Localization::Load(const UnicodeString& name)
{
	(void)name;
	HGLOBAL res_handle;
	HRSRC res;

	UnicodeString resId;
	std::map<const UnicodeString, UnicodeString>::const_iterator it;
	it = mResDict.find(name);
	if (it != mResDict.end())
	{
		resId = it->second;
	}
	else
	{
		resId = UnicodeString(DEFAULT_RES_NAME);
	}

	HMODULE hMod = GetModuleHandle(Application->ExeName.c_str());
	res = FindResource(hMod, resId.c_str(), RT_RCDATA);
	if (res == NULL)
	{
		assert(0);
		return;
	}

	res_handle = LoadResource(NULL, res);

	if (res_handle == NULL)
	{
		assert(0);
		return;
	}

	pugi::xml_document doc;
	pugi::xml_parse_result result;

	{
		const UnicodeString data = UnicodeString((wchar_t*)LockResource(res_handle));

		std::string utf8data = pugi::as_utf8(data.c_str());
		result = doc.load_buffer(utf8data.c_str(), utf8data.length());
	}

	mMap.clear();

	if(result.status == pugi::status_ok && !doc.empty())
	{
		pugi::xml_node n = doc.first_child();

		{
			pugi::xml_attribute alias = n.attribute("alias");
			pugi::xml_attribute text = n.attribute("text");
			UnicodeString a = UnicodeString(pugi::as_wide(alias.value()).c_str());
			mMap[a] = UnicodeString(pugi::as_wide(text.value()).c_str());
		}

		for (pugi::xml_node it = n.first_child(); it; it = it.next_sibling())
		{
			pugi::xml_attribute alias = it.attribute("alias");
			pugi::xml_attribute text = it.attribute("text");
			if(!alias.empty() && !text.empty())
			{
				UnicodeString a = UnicodeString(pugi::as_wide(alias.value()).c_str());
				mMap[a] = UnicodeString(pugi::as_wide(text.value()).c_str());
			}
		}
	}

	FreeResource(res_handle);
}
//---------------------------------------------------------------------------

void __fastcall Localization::Localize(TForm *form)
{
  TryLocalizeMainMenu(form->Menu);
  TryLocalizeControls(form);
}
//---------------------------------------------------------------------------

void __fastcall Localization::TryLocalizeMainMenu(TMainMenu *mm)
{
  if(mm != NULL)
  {
    TMenuItemAutoFlag f = mm->AutoHotkeys;
    mm->AutoHotkeys = maManual;
    LocalizeMainMenuItem(mm->Items);
    mm->AutoHotkeys = f;
  }
}
//---------------------------------------------------------------------------

void __fastcall Localization::LocalizeMainMenuItem(TMenuItem *mi)
{
  for(__int32 i = 0; i < mi->Count; i++)
  {
    TMenuItem *curr_item = mi->Items[i];
    if(curr_item->Caption.IsEmpty() == false)
    {
      UnicodeString txt = curr_item->Caption;
      const __int32 pos = txt.Pos(_T("&"));
      if(pos > 0)
      {
        txt.Delete(pos, 1);
      }
      txt = Text(txt);
      curr_item->Caption = txt;
      curr_item->Hint = txt + _T("|");
    }
    LocalizeMainMenuItem(curr_item);
  }
}
//---------------------------------------------------------------------------

void __fastcall Localization::Localize(TActionList *alist)
{
  if(alist != NULL)
  {
    for(__int32 i = 0; i < alist->ActionCount; i++)
    {
      TContainedAction *a = alist->Actions[i];
      if(a->Caption.IsEmpty() == false)
      {
        UnicodeString txt = a->Caption;
        const __int32 pos = txt.Pos(_T("&"));
        if(pos > 0)
        {
          txt.Delete(pos, 1);
        }
        txt = Text(txt);
        a->Caption = txt;
        a->Hint = txt + _T("|");
      }
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall Localization::Localize(TPopupMenu *pmenu)
{
  if(pmenu != NULL)
  {
    TMenuItemAutoFlag f = pmenu->AutoHotkeys;
    pmenu->AutoHotkeys = maManual;
    LocalizeMainMenuItem(pmenu->Items);
    pmenu->AutoHotkeys = f;
  }
}
//---------------------------------------------------------------------------

void __fastcall Localization::TryLocalizeControls(TWinControl *ctrl)
{
  if(ctrl == NULL)
  {
    return;
  }
  TryLocalizeControl(ctrl);
  for(__int32 i = 0; i < ctrl->ControlCount; i++)
  {
	TWinControl *c = dynamic_cast<TWinControl*>(ctrl->Controls[i]);
	if(c != NULL)
	{
		TryLocalizeControls(c);
	}
	else
	{
		TryLocalizeControl(ctrl->Controls[i]);
	}
  }
}
//---------------------------------------------------------------------------

void __fastcall Localization::TryLocalizeControl(TControl *c)
{
    if(c == NULL)
    {
      return;
    }
    //forms
    {
      TForm *f = dynamic_cast<TForm*>(c);
      if(f != NULL)
      {
        const UnicodeString txt = Text(f->Caption);
        f->Caption = txt;
        return;
      }
    }
    //panels
    {
      TPanel *p = dynamic_cast<TPanel*>(c);
      if(p != NULL)
      {
		UnicodeString txt = Text(p->Caption);
		p->Caption = txt;
		txt = Text(p->Hint);
		p->Hint = txt;
        return;
      }
    }
	//category panels
    {
      TCategoryPanel *p = dynamic_cast<TCategoryPanel*>(c);
      if(p != NULL)
      {
		UnicodeString txt = Text(p->Caption);
		p->Caption = txt;
		txt = Text(p->Hint);
		p->Hint = txt;
        return;
      }
	}
	//labels
    {
      TLabel *l = dynamic_cast<TLabel*>(c);
      if(l != NULL)
      {
		UnicodeString txt = Text(l->Caption);
		l->Caption = txt;
		txt = Text(l->Hint);
		l->Hint = txt;
		return;
      }
    }
	//static images
    {
	  TImage *i = dynamic_cast<TImage*>(c);
	  if(i != NULL)
	  {
		const UnicodeString txt = Text(i->Hint);
		i->Hint = txt;
        return;
      }
	}
	//static texts
    {
      TStaticText *st = dynamic_cast<TStaticText*>(c);
      if(st != NULL)
      {
		const UnicodeString txt = Text(st->Caption);
		st->Caption = txt;
		return;
      }
    }
    //editboxes
    {
      TEdit *e = dynamic_cast<TEdit*>(c);
      if(e != NULL)
      {
        UnicodeString txt = Text(e->Text);
        e->Text = txt;
        txt = Text(e->TextHint);
        e->TextHint = txt;
        return;
      }
    }
    //memos
    {
      TMemo *m = dynamic_cast<TMemo*>(c);
      if(m != NULL)
      {
        const UnicodeString txt = Text(m->Text);
        m->Text = txt;
        return;
      }
    }
    //groupboxes
    {
      TGroupBox *gb = dynamic_cast<TGroupBox*>(c);
      if(gb != NULL)
      {
        const UnicodeString txt = Text(gb->Caption);
        gb->Caption = txt;
        return;
      }
    }
    //tabsets
    {
      TTabSet *ts = dynamic_cast<TTabSet*>(c);
      if(ts != NULL)
      {
		for(__int32 i = 0; i < ts->Tabs->Count; i++)
		{
		  const UnicodeString txt = Text(ts->Tabs->Strings[i]);
		  ts->Tabs->Strings[i] = txt;
		}
        return;
      }
    }
	//buttons
    {
	  TButton *b = dynamic_cast<TButton*>(c);
	  if(b != NULL)
      {
		UnicodeString txt = Text(b->Caption);
		b->Caption = txt;
		txt = Text(b->Hint);
		b->Hint = txt;
		return;
      }
	}
	//bitbtn buttons
	{
	  TBitBtn *b = dynamic_cast<TBitBtn*>(c);
	  if(b != NULL)
	  {
		UnicodeString txt = Text(b->Caption);
		b->Caption = txt;
		txt = Text(b->Hint);
		b->Hint = txt;
		return;
	  }
	}
	//TTabSheet
    {
	  TTabSheet *t = dynamic_cast<TTabSheet*>(c);
	  if(t != NULL)
	  {
		UnicodeString txt = Text(t->Caption);
		t->Caption = txt;
		txt = Text(t->Hint);
		t->Hint = txt;
		return;
	  }
	}
	//TValueListEditor
	{
	  TValueListEditor *vle = dynamic_cast<TValueListEditor*>(c);
	  if(vle != NULL)
	  {
		__int32 i;
		const UnicodeString txt = Text(vle->Hint);
		vle->Hint = txt;
		for(i = 0; i < vle->TitleCaptions->Count; i++)
		{
			const UnicodeString txt = Text(vle->TitleCaptions->Strings[i]);
			vle->TitleCaptions->Strings[i] = txt;
		}
		for(i = 0; i < vle->Strings->Count; i++)
		{
			const UnicodeString txt = vle->Strings->Strings[i];
			__int32 pos = txt.Pos(_T("="));
			if(pos > 1)
			{
				const UnicodeString key = txt.SubString(0, pos - 1);
				const UnicodeString val = vle->Values[key];
				vle->Strings->Strings[i] = Text(key) + "=" + val;
			}
		}
		return;
	  }
	}
	//TListView
	{
	  TListView *vl = dynamic_cast<TListView*>(c);
	  if(vl != NULL)
	  {
		if(vl->Columns != NULL)
		{
			__int32 i;
			for(i = 0; i < vl->Columns->Count; i++)
			{
				TListColumn* lc = vl->Column[i];
				const UnicodeString txt = Text(lc->Caption);
				lc->Caption = txt;
			}
		}
		return;
	  }
	}
	//TComboBox
    {
	  TComboBox *cb = dynamic_cast<TComboBox*>(c);
	  if(cb != NULL)
	  {
	  	__int32 i;
		const __int32 idx = cb->ItemIndex;
		for(i = 0; i < cb->Items->Count; i++)
		{
		  const UnicodeString txt = Text(cb->Items->Strings[i]);
		  cb->Items->Strings[i] = txt;
		}
		const UnicodeString txt = Text(cb->Hint);
		cb->Hint = txt;
		cb->ItemIndex = idx;
        return;
      }
	}
	//TCheckBox
    {
	  TCheckBox *cb = dynamic_cast<TCheckBox*>(c);
	  if(cb != NULL)
	  {
		UnicodeString txt = Text(cb->Caption);
		cb->Caption = txt;
		txt = Text(cb->Hint);
		cb->Hint = txt;
        return;
      }
	}
	//TRadioButton
    {
	  TRadioButton *rb = dynamic_cast<TRadioButton*>(c);
	  if(rb != NULL)
	  {
		UnicodeString txt = Text(rb->Caption);
		rb->Caption = txt;
		txt = Text(rb->Hint);
		rb->Hint = txt;
        return;
      }
	}
	//TRadioGroup
    {
	  TRadioGroup *rg = dynamic_cast<TRadioGroup*>(c);
	  if(rg != NULL)
	  {
	  	__int32 i;
		const __int32 idx = rg->ItemIndex;
		for(i = 0; i < rg->Items->Count; i++)
		{
		  const UnicodeString txt = Text(rg->Items->Strings[i]);
		  rg->Items->Strings[i] = txt;
		}
		UnicodeString txt = Text(rg->Hint);
		rg->Hint = txt;
		rg->ItemIndex = idx;
		txt = Text(rg->Caption);
		rg->Caption = txt;
        return;
	  }
	}
}
//---------------------------------------------------------------------------

