//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UnitJNCount.h"
#include "UTypes.h"
#include "MainUnit.h"
#include "ULocalization.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFJNCount *FJNCount;
//---------------------------------------------------------------------------
__fastcall TFJNCount::TFJNCount(TComponent* Owner)
	: TForm(Owner)
{
	CSpinCount = new TSpinEdit(GroupBox_f);
	CSpinCount->Parent = GroupBox_f;
	CSpinCount->Left = Label2->Left + Label2->Width * 2;
	CSpinCount->Top = Label2->Top - 4;
	CSpinCount->Width = 81;
	CSpinCount->Height = 22;
	CSpinCount->MaxValue = MAX_ANIMATON_JOIN_NODES;
	CSpinCount->MinValue = DEF_ANIMATON_JOIN_NODES;
	CSpinCount->TabOrder = 0;
	CSpinCount->Value = DEF_ANIMATON_JOIN_NODES;
	CSpinCount->OnChange = OnSpinChange;
	CSpinCount->Visible = true;

	Localization::Localize(this);
}
//---------------------------------------------------------------------------
void __fastcall TFJNCount::FormShow(TObject *)
{
	OnSpinChange(NULL);
}
//---------------------------------------------------------------------------
void __fastcall TFJNCount::FormDestroy(TObject *)
{
	delete CSpinCount;
}
//---------------------------------------------------------------------------
void __fastcall TFJNCount::OnSpinChange(TObject *)
{
	if(CSpinCount->Value > 0)
	{
		__int32 ct = CSpinCount->Value;
		if(ct > MAX_ANIMATON_JOIN_NODES)
		{
			CSpinCount->Value = MAX_ANIMATON_JOIN_NODES;
		}
		else if(ct < DEF_ANIMATON_JOIN_NODES)
		{
			CSpinCount->Value = DEF_ANIMATON_JOIN_NODES;
		}
	}
}
//---------------------------------------------------------------------------
