//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UState.h"
#include "MainUnit.h"
#include "UObjManager.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TFEditState *FEditState;

//---------------------------------------------------------------------------
__fastcall TFEditState::TFEditState(TComponent* Owner)
        : TForm(Owner)
{
	Localization::Localize(this);

	int i;
	GPerson* gp;
	CB_dir->Clear();
	for(i = 0; i < Movement::dirCOUNT; i++)
	{
		CB_dir->AddItem(Movement::Name[i], reinterpret_cast<TObject*>(i * 2));
	}

	mpTempFilterList = new TStringList();

	EditFilterChange(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFEditState::FormDestroy(TObject *Sender)
{
	(void)Sender;
	delete mpTempFilterList;
}
//---------------------------------------------------------------------------

void __fastcall TFEditState::CheckBoxAlienChanged()
{
}
//---------------------------------------------------------------------------

void __fastcall TFEditState::SetState(TState *state)
{
	int i;
	if(state == NULL)
	{
		return;
	}
	SetVarName(state->name);
	CB_dir->ItemIndex = 0;
	for(i = 0; i < CB_dir->Items->Count; i++)
	{
	  if(state->direction == static_cast<Movement::Direction>(reinterpret_cast<__int32>(CB_dir->Items->Objects[i])))
	  {
		CB_dir->ItemIndex = i;
		break;
	  }
	}
	CB_AniType->ItemIndex = 0;
	for(i = 0; i < CB_AniType->Items->Count; i++)
	{
	  if(state->animation == CB_AniType->Items->Strings[i])
	  {
		CB_AniType->ItemIndex = i;
		break;
	  }
	}
	CheckBoxAlienChanged();
}
//---------------------------------------------------------------------------

void __fastcall TFEditState::GetState(TState *state)
{
	if(state == NULL)
	{
		return;
    }
	state->name = GetVarName();
	state->direction = static_cast<Movement::Direction>(reinterpret_cast<__int32>(CB_dir->Items->Objects[CB_dir->ItemIndex]));
	if(CB_AniType->ItemIndex > 0)
	{
		state->animation = CB_AniType->Text;
	}
	else
	{
		state->animation = _T("");
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditState::FormShow(TObject */*Sender*/)
{
	CB_dirChange(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFEditState::Ed_nameChange(TObject */*Sender*/)
{
	Edit_nameChangeMain(Ed_name);
}
//---------------------------------------------------------------------------

void __fastcall TFEditState::CB_dirChange(TObject */*Sender*/)
{
	int idx;
	Graphics::TBitmap *bmp;
	idx = (int)CB_dir->Items->Objects[CB_dir->ItemIndex];
	if(idx >= 0)
	{
		bmp = new Graphics::TBitmap;
		Form_m->ImageListDir->GetBitmap(idx, bmp);
		Image->Picture->Bitmap->Assign(bmp);
		delete bmp;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFEditState::EditScriptChange(TObject */*Sender*/)
{
	Edit_nameChangeMain(Ed_name);
}
//---------------------------------------------------------------------------

void __fastcall TFEditState::SetVarName(UnicodeString txt)
{
	if(!EditPrefix->Text.IsEmpty())
	{
		int pos = txt.Pos(EditPrefix->Text);
		if(pos > 0)
		{
			txt.Delete(pos, EditPrefix->Text.Length());
		}
	}
	Ed_name->Text = txt;
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TFEditState::GetVarName()
{
	return EditPrefix->Text + Ed_name->Text.Trim();
}
//---------------------------------------------------------------------------

void __fastcall TFEditState::EditFilterChange(TObject *Sender)
{
	(void)Sender;
	__int32 pos;
	CB_AniType->Enabled = false;
	if(EditFilter->Text.IsEmpty())
	{
		CB_AniType->Items->Assign(Form_m->mainCommonRes->aniNameTypes);
	}
	else
	{
		mpTempFilterList->Clear();
		for(__int32 i = 0; i < Form_m->mainCommonRes->aniNameTypes->Count; i++)
		{
			pos = Form_m->mainCommonRes->aniNameTypes->Strings[i].Pos(EditFilter->Text.UpperCase());
			if(pos > 0)
			{
				mpTempFilterList->Add(Form_m->mainCommonRes->aniNameTypes->Strings[i]);
			}
		}
		CB_AniType->Items->Assign(mpTempFilterList);
	}
	if (CB_AniType->Items->Count == 0)
	{
		CB_AniType->Items->Add(TEXT_STR_NONE);
	}
	else
	{
		CB_AniType->Items->Insert(0, TEXT_STR_NONE);
	}

	pos = CB_AniType->Items->IndexOf(CB_AniType->Text);
	if(pos < 0)
	{
		if(CB_AniType->Items->Count == 1)
		{
			CB_AniType->ItemIndex = 0;
		}
		else
		{
			CB_AniType->ItemIndex = 1;
		}
	}

	CB_AniType->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TFEditState::CB_AniTypeEnter(TObject *Sender)
{
	TComboBox *cb = (TComboBox *)Sender;
	mOldCBValue = cb->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFEditState::CB_AniTypeExit(TObject *Sender)
{
	TComboBox *cb = (TComboBox *)Sender;
	if(cb->Items->IndexOf(cb->Text) < 0)
	{
		if(cb->Text.Compare(mOldCBValue) != 0)
		{
			cb->Text = mOldCBValue;
        }
	}
	if(cb->Items->IndexOf(cb->Text) < 0)
	{
		cb->ItemIndex = 0;
	}
}
//---------------------------------------------------------------------------

