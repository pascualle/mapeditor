//---------------------------------------------------------------------------

#ifndef UFragmentPropertyH
#define UFragmentPropertyH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ActnList.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include <System.Actions.hpp>
#include <System.ImageList.hpp>
//---------------------------------------------------------------------------

class	TBGobj;
class	GMapBGObj;
class	TCommonResObjManager;

class TFBOProperty : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *GroupBox_type;
	TListView *ListView;
	TPopupMenu *PopupMenu;
	TMenuItem *NEdit;
	TMenuItem *N8;
	TMenuItem *NSRight;
	TMenuItem *NSLeft;
	TActionList *ActionList;
	TAction *ActEdit;
	TAction *ActShiftRight;
	TAction *ActShiftLeft;
	TButton *ButtonOk;
	TButton *Button2;
	TImageList *ImageList;
	TStatusBar *StatusBar;
	TGroupBox *GroupBox;
	TMemo *Memo;
	TComboBox *CB_Type;
	TBevel *Bevel3;
	TButton *Bt_3dot;
	TToolBar *ToolBar1;
	TToolButton *ToolButton1;
	TToolButton *ToolButton2;
	TToolButton *ToolButton3;
	void __fastcall ListViewKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall ActNewExecute(TObject *Sender);
	void __fastcall ActEditExecute(TObject *Sender);
	void __fastcall ActDelExecute(TObject *Sender);
	void __fastcall ActShiftRightExecute(TObject *Sender);
	void __fastcall ActShiftLeftExecute(TObject *Sender);
	void __fastcall ListViewDblClick(TObject *Sender);
	void __fastcall ListViewEnter(TObject *Sender);
	void __fastcall ListViewExit(TObject *Sender);
	void __fastcall MemoExit(TObject *Sender);
	void __fastcall Bt_3dotClick(TObject *Sender);
	void __fastcall CB_TypeExit(TObject *Sender);
	void __fastcall ListViewClick(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
private:
		GMapBGObj 	*tGMapBGObj, *mpCurrentObj;
		bool		inited, readOnly;

		void __fastcall GrResEdit(TListItem* li,int mode);
		void __fastcall refreshImg();
		void __fastcall ShowTypePreview(UnicodeString name);
public:
		void __fastcall assignBGobj(GMapBGObj *BGobj);
		void __fastcall applyChangesToBGobj();
		void __fastcall setReadOnly(bool val);

		__fastcall TFBOProperty(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFBOProperty *FBOProperty;
//---------------------------------------------------------------------------
#endif
