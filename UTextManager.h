//---------------------------------------------------------------------------

#ifndef UTextManagerH
#define UTextManagerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <ActnList.hpp>
#include <Grids.hpp>
#include <Tabs.hpp>
#include <ExtCtrls.hpp>
#include <System.Actions.hpp>
//---------------------------------------------------------------------------
class TFormTextManager : public TForm
{
__published:
	TMainMenu *MainMenu;
	TMenuItem *N1;
	TMenuItem *N3;
	TMenuItem *N4;
	TMenuItem *N2;
	TMenuItem *N5;
	TToolBar *ToolBar1;
	TToolButton *ToolButton5;
	TToolButton *ToolButton1;
	TActionList *ActionList1;
	TAction *ActSave;
	TAction *ActLoad;
	TAction *ActNewMessage;
	TAction *ActDelMessage;
	TStatusBar *StatusBar;
	TMenuItem *N6;
	TAction *ActEdit;
	TToolButton *ToolButton2;
	TMenuItem *N7;
	TStringGrid *TntStringGrid;
	TTabSet *TabSet;
	TToolButton *ToolButton3;
	TEdit *EditFilter;
	TPanel *PanelButtons;
	TButton *Button1;
	TButton *Button2;
	void __fastcall ActNewMessageExecute(TObject *Sender);
	void __fastcall ActDelMessageExecute(TObject *Sender);
	void __fastcall ActEditExecute(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall TntStringGridDblClick(TObject *Sender);
	void __fastcall TntStringGridKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall TabSetClick(TObject *Sender);
	void __fastcall EditFilterChange(TObject *Sender);

private:

	void __fastcall RefreshTable(UnicodeString selected);
	void __fastcall RefreshBar();
	void __fastcall AddTextRow(int idx, UnicodeString textName);
  void __fastcall InitGridColumns();

	TStringList *mpNames;
	TStringList *mpFilteredNames;
	UnicodeString mLang;

public:
	__fastcall TFormTextManager(TComponent* Owner);

	bool __fastcall Find(UnicodeString name);
	UnicodeString __fastcall GetCurrentName();
	void __fastcall ShowButtons(bool buttons);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormTextManager *FormTextManager;
//---------------------------------------------------------------------------
#endif
