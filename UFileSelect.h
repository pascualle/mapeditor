//---------------------------------------------------------------------------

#ifndef UFileSelectH
#define UFileSelectH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.FileCtrl.hpp>
//---------------------------------------------------------------------------
class TFFileSelect : public TForm
{
__published:	// IDE-managed Components
	TFileListBox *FileListBox;
	TStatusBar *StatusBar;
	TPanel *PanelBt;
	TButton *Button1;
	TButton *Button2;
private:	// User declarations
public:		// User declarations
	__fastcall TFFileSelect(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFFileSelect *FFileSelect;
//---------------------------------------------------------------------------
#endif
