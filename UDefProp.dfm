object FDefProperties: TFDefProperties
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  ClientHeight = 428
  ClientWidth = 361
  Color = clBtnFace
  Constraints.MinHeight = 375
  Constraints.MinWidth = 369
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnDestroy = FormDestroy
  DesignSize = (
    361
    428)
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 409
    Width = 361
    Height = 19
    Panels = <>
  end
  object ScrollBox: TScrollBox
    Left = 0
    Top = 0
    Width = 361
    Height = 376
    HorzScrollBar.Tracking = True
    VertScrollBar.Tracking = True
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
  end
  object Button2: TButton
    Left = 78
    Top = 381
    Width = 76
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 1
  end
  object Button1: TButton
    Left = 160
    Top = 381
    Width = 123
    Height = 25
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'mApply'
    ModalResult = 1
    TabOrder = 2
  end
end
