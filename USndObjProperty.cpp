//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "USndObjProperty.h"
#include "MainUnit.h"
#include "USndObjPropertyEdit.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFSndObjProperty *FSndObjProperty;
//---------------------------------------------------------------------------

static const _TCHAR SFX_TEXT[] = _T("SFX");
static const _TCHAR BGM_TEXT[] = _T("BGM");

//---------------------------------------------------------------------------

__fastcall TFSndObjProperty::TFSndObjProperty(TComponent* Owner)
	: TPrpFunctions(Owner)
{
	CSEd_x = new TSpinEdit(GroupBoxPos);
	CSEd_x->Parent = GroupBoxPos;
	CSEd_x->Left = 16;
	CSEd_x->Top = 20;
	CSEd_x->Width = 55;
	CSEd_x->Height = 22;
	CSEd_x->TabOrder = 0;
	CSEd_x->OnChange = CSEd_xChange;
	CSEd_x->OnEnter = CSEd_xEnter;
	CSEd_x->OnExit = CSEd_xExit;
    CSEd_x->Visible = true;
	CSEd_y = new TSpinEdit(GroupBoxPos);
	CSEd_y->Parent = GroupBoxPos;
	CSEd_y->Left = 84;
	CSEd_y->Top = 20;
	CSEd_y->Width = 53;
	CSEd_y->Height = 22;
	CSEd_y->TabOrder = 1;
	CSEd_y->OnChange = CSEd_xChange;
	CSEd_y->OnEnter = CSEd_xEnter;
	CSEd_y->OnExit = CSEd_xExit;
	CSEd_y->Visible = true;

	mOldCSEd_x = _T("");
	t_MapGP = NULL;
	mpCommonResObjManager = NULL;
	disableChanges = true;
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::Localize()
{
	Localization::Localize(this);
	Localization::Localize(ActionList1);
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::SaveAllValues(TWinControl *iParent)
{
  if(iParent == NULL)
	return;

  TControl *c;
  for(int i = 0; i < iParent->ControlCount; i++)
  {
	c = iParent->Controls[i];
	if(c == NULL)continue;

	if( c->ClassNameIs(_T("TPanel")) ||
		c->ClassNameIs(_T("TGroupBox")) ||
		c->ClassNameIs(_T("TForm")) )
	{
		SaveAllValues((TWinControl*)c);
	}
	else
	{
		if(c->ClassNameIs(_T("TSpinEdit")))
		{
			if(((TSpinEdit*)c)->OnExit != NULL)
				((TSpinEdit*)c)->OnExit(c);
		}
		if(c->ClassNameIs(_T("TListView")))
		{
			if(((TListView*)c)->OnExit != NULL)
				((TListView*)c)->OnExit(c);
        }
	}
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::FormHide(TObject *)
{
	mOldCSEd_x = _T("");
	SaveAllValues(this);
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::FormShow(TObject *)
{
	AssignSoundSchemeObj(t_MapGP);
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::FakeEnterActiveControl()
{
	TWinControl* c = ActiveControl;
	if(c == NULL)
	{
		return;
    }
	if(c->ClassNameIs(_T("TSpinEdit")))
	{
		((TSpinEdit*)c)->OnEnter(c);
		return;
	}
	if(c->ClassNameIs(_T("TListView")))
	{
		((TListView*)c)->OnEnter(c);
		return;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::Init(TCommonResObjManager *commonResObjManager)
{
	mpCommonResObjManager = commonResObjManager;
	assert(mpCommonResObjManager);

	if(t_MapGP != NULL)
	{
		AssignSoundSchemeObj(t_MapGP);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::AssignSoundSchemeObj(TMapSoundScheme *sgp)
{
	if(t_MapGP == sgp)
	{
		return;
	}

	if(t_MapGP != NULL)
	{
		if(Visible)
		{
			mOldCSEd_x = _T("");
			SaveAllValues(this);
		}
		t_MapGP->UnregisterPrpWnd();
	}

	t_MapGP = sgp;
	if(t_MapGP != NULL)
	{
		t_MapGP->RegisterPrpWnd(this);
    }
	else
	{
		return;
	}

	Caption = Localization::Text(_T("mGameObjectProperties")) + _T(" [") + t_MapGP->getGParentName() + _T("]");

	disableChanges = true;

    double x, y;
	sgp->getCoordinates(&x, &y);
	CSEd_x->Value = (__int32)x;
	CSEd_y->Value = (__int32)y;

    CreateList();

	disableChanges = false;

	CheckActionsStatus();

	Lock(t_MapGP->IsLocked());

	if(Visible)
	{
		FakeEnterActiveControl();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::CSEd_xChange(TObject *Sender)
{
  if(((TSpinEdit *)Sender)->Text.IsEmpty())return;
  try
  {
	StrToInt(((TSpinEdit *)Sender)->Text);
  }
  catch(...)
  {
	return;
  }

  if(!disableChanges)
	if(t_MapGP != NULL)
	  Form_m->OnChangeObjPosition(t_MapGP, CSEd_x->Value, CSEd_y->Value);
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::CSEd_xEnter(TObject *Sender)
{
	mOldCSEd_x = ((TSpinEdit *)Sender)->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::SetTransform(const TTransformProperties& transform)
{
	if(!Visible)
	{
		return;
	}
	double x, y;
	transform.GetPosition(x, y);
	CSEd_x->Value = static_cast<int>(x);
	CSEd_y->Value = static_cast<int>(y);
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::CSEd_xExit(TObject *Sender)
{
  UnicodeString str = ((TSpinEdit *)Sender)->Text.Trim();

  if(mOldCSEd_x.IsEmpty())
	return;

  if(mOldCSEd_x.Compare(str) != 0)
	if(t_MapGP != NULL && !disableChanges)
	  Form_m->OnChangeObjPosition(t_MapGP, CSEd_x->Value, CSEd_y->Value);
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::CheckActionsStatus()
{
	TListItem* si = ListView->Selected;
	ActEditItem->Enabled = si != NULL;
	ActDelItem->Enabled = si != NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::ActNewItemExecute(TObject *)
{
	AddEditDel(-1, mAdd);
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::ActDelItemExecute(TObject *)
{
	TListItem* si = ListView->Selected;
	if(si != NULL)
	{
		AddEditDel(si->Index, mDel);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::ActEditItemExecute(TObject *)
{
	TListItem* si = ListView->Selected;
	if(si != NULL)
	{
		AddEditDel(si->Index, mEdit);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::ListViewDblClick(TObject *)
{
	ActEditItem->Execute();
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::FormKeyDown(TObject *, WORD &Key, TShiftState)
{
	if(Key == VK_RETURN)
	{
		ActEditItem->Execute();
		Key = 0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::ListViewClick(TObject *)
{
	CheckActionsStatus();
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::CreateList()
{
    ListView->Items->Clear();
	if(t_MapGP == NULL)
	{
		return;
	}
	__int32 idx, ct;
	ct = t_MapGP->GetItemsCount();
	for(idx = 0; idx < ct; idx++)
	{
		SoundSchemePropertyItem i;
		t_MapGP->GetItemByIndex(idx, i);
		TListItem *it = ListView->Items->Add();
		FillListItem(it, &i);
	}
	if(ct > 0)
	{
		ListView->Items->Item[0]->Selected = true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::FillListItem(TListItem *it, SoundSchemePropertyItem *i)
{
    it->SubItems->Clear();
	it->Caption = i->mName;
	it->SubItems->Add(i->mFileName);
	switch(i->mType)
	{
		case SoundSchemePropertyItem::SSRT_SFX:
			it->SubItems->Add(SFX_TEXT);
		break;
		case SoundSchemePropertyItem::SSRT_BG:
			it->SubItems->Add(BGM_TEXT);
		break;
		default:
		break;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::AddEditDel(__int32 idx, __int32 mode)
{
	if(t_MapGP == NULL)
	{
		return;
	}
	bool res;
	UnicodeString oldName;
	switch(mode)
	{
		case mDel:
			t_MapGP->DeleteItem(ListView->Items->Item[idx]->Caption);
			ListView->Items->Delete(idx);
		break;
		case mAdd:
		case mEdit:
			Application->CreateForm(__classid(TFSOPEdit), &FSOPEdit);
			FSOPEdit->Label_name->Caption = ListView->Columns->Items[0]->Caption;
			FSOPEdit->Label_namefile->Caption = ListView->Columns->Items[1]->Caption;
			FSOPEdit->Label_type->Caption = ListView->Columns->Items[2]->Caption;
			if(mode == mEdit)
			{
				SoundSchemePropertyItem item;
				if(!t_MapGP->GetItemByIndex(idx, item))
				{
					FSOPEdit->Free();
					FSOPEdit = NULL;
					return;
				}
				oldName = item.mName;
				FSOPEdit->SetName(item.mName);
				FSOPEdit->Edit_filename->Text = item.mFileName;
				FSOPEdit->ComboBox_type->ItemIndex = (item.mType == SoundSchemePropertyItem::SSRT_SFX) ? 0 : 1;
			}
			do
			{
				res = true;
				if (FSOPEdit->ShowModal() == mrOk)
				{
					SoundSchemePropertyItem newitem;
					newitem.mFileName = FSOPEdit->Edit_filename->Text.Trim();
					newitem.mName = FSOPEdit->GetName();
					newitem.mType = FSOPEdit->ComboBox_type->ItemIndex == 0 ? SoundSchemePropertyItem::SSRT_SFX : SoundSchemePropertyItem::SSRT_BG;
					if (FSOPEdit->Ed_name->Text.IsEmpty())
					{
						FSOPEdit->ActiveControl = FSOPEdit->Ed_name;
						Application->MessageBox(Localization::Text(_T("mIDNameError")).c_str(),
												Localization::Text(_T("mError")).c_str(),
												MB_OK | MB_ICONERROR);
						res = false;
					}
					else if (newitem.mFileName.IsEmpty())
					{
						FSOPEdit->ActiveControl = FSOPEdit->Edit_filename;
						Application->MessageBox(Localization::Text(_T("mFileNameError")).c_str(),
												Localization::Text(_T("mError")).c_str(),
												MB_OK | MB_ICONERROR);
						res = false;
					}
					else
					{
						bool valid;
						UnicodeString filepath = Form_m->PathPngRes + newitem.mFileName;
						if(newitem.mType == SoundSchemePropertyItem::SSRT_SFX)
						{
							valid = validateSoundResource(filepath, &newitem.mFileSize);
						}
						else
						{
							valid = validateSoundResource(filepath, NULL);
						}
						if(valid)
						{
							__int32 pos, ct;
							ct = t_MapGP->GetItemsCount();
							for (pos = 0; pos < ct; pos++)
							{
								SoundSchemePropertyItem i;
								t_MapGP->GetItemByIndex(pos, i);
								if (i.mName.Compare(newitem.mName) == 0 && idx != pos)
								{
									FSOPEdit->ActiveControl = FSOPEdit->Ed_name;
									Application->MessageBox(Localization::Text(_T("mIDNameExist")).c_str(),
															Localization::Text(_T("mError")).c_str(),
															MB_OK | MB_ICONERROR);
									res = false;
									break;
								}
								if (i.mFileName.Compare(newitem.mFileName) == 0 && idx != pos)
								{
									FSOPEdit->ActiveControl = FSOPEdit->Edit_filename;
									Application->MessageBox(Localization::Text(_T("mFileNameExist")).c_str(),
															Localization::Text(_T("mError")).c_str(),
															MB_OK | MB_ICONERROR);
									res = false;
									break;
								}
							}
						}
						else
						{
							FSOPEdit->ActiveControl = FSOPEdit->Edit_filename;
							UnicodeString foerrmess = Localization::Text(_T("mOpenFileError")) + newitem.mFileName;
							Application->MessageBox(foerrmess.c_str(), Localization::Text(_T("mError")).c_str(), MB_OK | MB_ICONERROR);
							res = false;
                        }
					}
					if (res)
					{
						if (mode == mAdd)
						{
							TListItem *it = ListView->Items->Add();
							FillListItem(it, &newitem);
							t_MapGP->AddItem(newitem);
						}
						else
						{
							TListItem *it = ListView->Items->Item[idx];
							FillListItem(it, &newitem);
							t_MapGP->ChangeItem(oldName, newitem);
						}
					}
				}
				else
				{
					res = true;
				}
			}
			while(!res);
			FSOPEdit->Free();
			FSOPEdit = NULL;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::ListViewExit(TObject *)
{
	//nope
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::ListViewEnter(TObject *)
{
	//nope
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::Reset()
{
	AssignSoundSchemeObj(nullptr);
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::EmulatorMode(bool mode)
{
	(void)mode;
}
//---------------------------------------------------------------------------

void __fastcall TFSndObjProperty::Lock(bool value)
{
	ImageLock->Visible = value;
}
//---------------------------------------------------------------------------

