//---------------------------------------------------------------------------

#ifndef UObjPatternH
#define UObjPatternH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------

class TObjPattern;
class TSaveLoadStreamData;

class TFObjPattern : public TForm
{
__published:	// IDE-managed Components
        TButton *ButtonCancel;
        TButton *ButtonOk;
	TMainMenu *MainMenu1;
	TMenuItem *N1;
	TMenuItem *N4;
	TMenuItem *N5;
	TMenuItem *N7;
	TMenuItem *N6;
	TMenuItem *N2;
	TMenuItem *N3;
	TOpenDialog *OpenDialog;
	TSaveDialog *SaveDialog;
	TPageControl *PageControl1;
	TTabSheet *TabSheet1;
	TTabSheet *TabSheet2;
	TListBox *ListBox;
	TBitBtn *Bt_add;
	TBitBtn *Bt_edit;
	TBitBtn *Bt_del;
	TPanel *Panel1;
	TBitBtn *Bt_up;
	TBitBtn *Bt_down;
	TStaticText *StaticText1;
	TLabel *Label1;
	TLabel *Label2;
	TStaticText *StaticText2;
	TLabel *Label3;
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall Bt_addClick(TObject *Sender);
        void __fastcall UpDownClick(TObject *Sender, TUDBtnType Button);
        void __fastcall Bt_upClick(TObject *Sender);
        void __fastcall Bt_downClick(TObject *Sender);
        void __fastcall N3Click(TObject *Sender);
        void __fastcall N4Click(TObject *Sender);
        void __fastcall CSEd_grChange(TObject *Sender);
        void __fastcall N6Click(TObject *Sender);
        void __fastcall N7Click(TObject *Sender);
	void __fastcall ListBoxDblClick(TObject *Sender);
	void __fastcall ListBoxKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
private:
        TObjPattern     *t_pattern;
        //TList           *items;
        bool            change, prewiewMode;
        UnicodeString      sDefaultExt, sPtDir, sFilter;

        void __fastcall refreshListBox();
        void __fastcall clearAll();
        bool __fastcall checkUniqName(UnicodeString name);
        int  __fastcall getItemIndex(UnicodeString name_var);
        int  __fastcall askToSave();
        bool __fastcall checkToSave();

        enum{mEdit,mAdd,mDel};
        void __fastcall editItem(int mode);

		bool __fastcall saveToFile(UnicodeString file);

public:
        //img_ct - (out)���������� �������� �� ������
        //t_items- (out)��������� �������� �������
        //file   - (in) ��� �����
        //mesErr - (out) ���� �� NULL, ��������� �� ������ �� �������������, � ������������� ���� ����������
        //bool       __fastcall loadFromFile(UnicodeString file, UnicodeString *mesErr=NULL);
        //���������� ���������� loadFromFile
        void __fastcall clearListItems(TList* list);

        //defDir - ���. ��  ���������
        //onlyFromDir - ���� ���� ���� ����������, ����� ����� ��������� ������ �� defDir
        //UnicodeString __fastcall selectPattertFile(UnicodeString defDir, bool onlyFromDir);

        //preview mode. No editing
        //bool __fastcall PreviewFile(UnicodeString file);

        void __fastcall SetPattern(TObjPattern *p);
        void __fastcall GetPattern(TObjPattern *p);

        __fastcall TFObjPattern(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFObjPattern *FObjPattern;
//---------------------------------------------------------------------------
#endif
