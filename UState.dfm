object FEditState: TFEditState
  Left = 366
  Top = 230
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'mGameObjectState'
  ClientHeight = 251
  ClientWidth = 376
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel
    Left = 8
    Top = 11
    Width = 35
    Height = 13
    Caption = 'mName'
  end
  object GroupBoxMovement: TGroupBox
    Left = 8
    Top = 37
    Width = 360
    Height = 72
    Caption = 'mMovementDirection'
    TabOrder = 2
    object Image: TImage
      Left = 22
      Top = 25
      Width = 11
      Height = 10
      Transparent = True
    end
    object Label1: TLabel
      Left = 37
      Top = 44
      Width = 308
      Height = 24
      AutoSize = False
      Caption = 'mMovementDirectionDescription'
      WordWrap = True
    end
    object CB_dir: TComboBox
      Left = 39
      Top = 20
      Width = 282
      Height = 21
      Style = csDropDownList
      TabOrder = 0
      OnChange = CB_dirChange
    end
  end
  object Ed_name: TEdit
    Left = 93
    Top = 8
    Width = 242
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 64
    TabOrder = 1
    OnChange = Ed_nameChange
  end
  object GroupBoxAnimation: TGroupBox
    Left = 8
    Top = 112
    Width = 360
    Height = 83
    Caption = 'mAnimation'
    TabOrder = 3
    object CB_AniType: TComboBox
      Left = 39
      Top = 49
      Width = 282
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 1
      OnEnter = CB_AniTypeEnter
      OnExit = CB_AniTypeExit
    end
    object EditFilter: TEdit
      Left = 39
      Top = 20
      Width = 282
      Height = 21
      TabOrder = 0
      TextHint = 'mFilter'
      OnChange = EditFilterChange
    end
  end
  object Button: TButton
    Left = 170
    Top = 220
    Width = 134
    Height = 25
    Caption = 'mApply'
    ModalResult = 1
    TabOrder = 4
  end
  object ButtonCancel: TButton
    Left = 89
    Top = 220
    Width = 75
    Height = 25
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 5
  end
  object EditPrefix: TEdit
    Left = 45
    Top = 8
    Width = 45
    Height = 21
    Cursor = crArrow
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    ReadOnly = True
    TabOrder = 0
    Text = 'STATE_'
  end
end
