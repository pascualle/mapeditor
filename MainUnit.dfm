object Form_m: TForm_m
  Left = 343
  Top = 240
  ActiveControl = ListBoxZone
  Caption = 'Map editor'
  ClientHeight = 799
  ClientWidth = 1184
  Color = clBtnFace
  Constraints.MinHeight = 300
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnKeyUp = FormKeyUp
  OnResize = FormResize
  OnShow = FormShow
  TextHeight = 13
  object Splitter: TSplitter
    Left = 0
    Top = 663
    Width = 1184
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    AutoSnap = False
    Color = clBtnFace
    ParentColor = False
    OnMoved = Splitter2Moved
    ExplicitTop = 561
    ExplicitWidth = 857
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 781
    Width = 1184
    Height = 18
    Panels = <
      item
        Width = 50
      end
      item
        Width = 50
      end
      item
        Width = 50
      end
      item
        Width = 50
      end
      item
        Width = 50
      end>
    ParentFont = True
    UseSystemFont = False
    OnMouseDown = StatusBar1MouseDown
    ExplicitTop = 780
    ExplicitWidth = 1180
  end
  object HorzScrollBar: TScrollBar
    Left = 0
    Top = 644
    Width = 1157
    Height = 17
    Ctl3D = False
    DoubleBuffered = True
    Max = 1
    PageSize = 0
    ParentCtl3D = False
    ParentDoubleBuffered = False
    TabOrder = 0
    TabStop = False
    OnScroll = VertScrollBarScroll
  end
  object VertScrollBar: TScrollBar
    Left = 1163
    Top = 42
    Width = 17
    Height = 596
    Ctl3D = False
    DoubleBuffered = True
    Kind = sbVertical
    Max = 1
    PageSize = 0
    ParentCtl3D = False
    ParentDoubleBuffered = False
    TabOrder = 1
    TabStop = False
    OnScroll = VertScrollBarScroll
  end
  object ToolBar: TToolBar
    Left = 0
    Top = 0
    Width = 1184
    Height = 26
    Color = clBtnFace
    EdgeBorders = [ebTop, ebBottom]
    GradientEndColor = clBtnFace
    Images = ImageList1
    GradientDrawingOptions = []
    ParentColor = False
    TabOrder = 3
    Transparent = False
    ExplicitWidth = 1180
    object ToolButton6: TToolButton
      Left = 0
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 14
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 8
      Top = 0
      Action = ActProjLoad
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton2: TToolButton
      Left = 31
      Top = 0
      Action = ActProjSave
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton1: TToolButton
      Left = 54
      Top = 0
      Width = 10
      Caption = 'ToolButton1'
      ImageIndex = 11
      Style = tbsSeparator
    end
    object TBObjects: TToolButton
      Left = 64
      Top = 0
      Action = ActObjMode
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
    end
    object TBBackground: TToolButton
      Left = 87
      Top = 0
      Action = ActBackMode
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
    end
    object TBCollideNodes: TToolButton
      Left = 110
      Top = 0
      Action = ActCollideNodesMode
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton8: TToolButton
      Left = 133
      Top = 0
      Width = 10
      Caption = 'ToolButton8'
      ImageIndex = 20
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 143
      Top = 0
      Action = ActLockDecor
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
    end
    object ToolButton4: TToolButton
      Left = 166
      Top = 0
      Width = 10
      Caption = 'ToolButton4'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButtonEdit: TToolButton
      Left = 176
      Top = 0
      Action = ActEditMode
      Down = True
      Grouped = True
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
    end
    object ToolButtonStart: TToolButton
      Left = 199
      Top = 0
      Action = ActStartEmul
      Grouped = True
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
    end
    object ToolButtonSSeparator: TToolButton
      Left = 222
      Top = 0
      Width = 10
      Caption = 'ToolButtonSSeparator'
      ImageIndex = 14
      Style = tbsSeparator
    end
    object ToolButtonObjManager: TToolButton
      Left = 232
      Top = 0
      Action = ActObjOrder
      ParentShowHint = False
      ShowHint = True
    end
    object ToolButton7: TToolButton
      Left = 255
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 20
      Style = tbsSeparator
    end
    object ToolButtonSStart: TToolButton
      Left = 263
      Top = 0
      Action = ActStart
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
    end
    object ToolButtonSPause: TToolButton
      Left = 286
      Top = 0
      Action = ActPause
      ParentShowHint = False
      ShowHint = True
      Style = tbsCheck
    end
    object ToolButtonSStep: TToolButton
      Left = 309
      Top = 0
      Action = ActStep
      ParentShowHint = False
      ShowHint = True
    end
  end
  object Panel_e1: TPanel
    Left = 0
    Top = 666
    Width = 1184
    Height = 115
    Align = alBottom
    BevelOuter = bvNone
    Ctl3D = False
    DoubleBuffered = False
    FullRepaint = False
    ParentCtl3D = False
    ParentDoubleBuffered = False
    TabOrder = 4
    ExplicitTop = 665
    ExplicitWidth = 1180
    object SplitterObjPal_Zones: TSplitter
      Left = 1056
      Top = 0
      Width = 5
      Height = 115
      Align = alRight
      AutoSnap = False
      Beveled = True
      Color = clBtnFace
      ParentColor = False
      OnMoved = Splitter2Moved
      ExplicitLeft = 596
      ExplicitHeight = 111
    end
    object Panel_layer: TPanel
      Left = 1061
      Top = 0
      Width = 123
      Height = 115
      Align = alRight
      BevelOuter = bvNone
      FullRepaint = False
      TabOrder = 0
      ExplicitLeft = 1057
      object TabSet: TTabSet
        Left = 0
        Top = 98
        Width = 123
        Height = 17
        Align = alBottom
        AutoScroll = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Pitch = fpFixed
        Font.Style = []
        SoftTop = True
        Style = tsSoftTabs
        Tabs.Strings = (
          'mZones')
        TabIndex = 0
      end
      object ListBoxZone: TListBox
        Left = 0
        Top = 0
        Width = 123
        Height = 98
        Align = alClient
        BevelInner = bvNone
        BevelKind = bkFlat
        BevelOuter = bvNone
        Ctl3D = True
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 1
        OnClick = ListBoxZoneClick
        OnEnter = ListBoxZoneEnter
      end
    end
    object PanelObj: TPanel
      Left = 0
      Top = 0
      Width = 1056
      Height = 115
      Align = alClient
      BevelOuter = bvNone
      Ctl3D = False
      FullRepaint = False
      ParentCtl3D = False
      ShowCaption = False
      TabOrder = 1
      ExplicitWidth = 1052
      object ListView_u: TListView
        Left = 0
        Top = 0
        Width = 1056
        Height = 98
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        BevelKind = bkFlat
        Columns = <>
        Ctl3D = True
        DoubleBuffered = True
        DragCursor = crDefault
        DragMode = dmAutomatic
        HideSelection = False
        IconOptions.AutoArrange = True
        ReadOnly = True
        ParentDoubleBuffered = False
        PopupMenu = PopupMenu_u
        TabOrder = 1
        OnDblClick = ListView_uDblClick
        OnEndDrag = ListView_uEndDrag
        OnEnter = ListView_sEnter
        OnKeyDown = ListView_uKeyDown
        OnMouseDown = ListView_uMouseDown
        OnStartDrag = ListView_uStartDrag
        ExplicitWidth = 1052
      end
      object TabSetObj: TTabSet
        Left = 0
        Top = 98
        Width = 1056
        Height = 17
        Align = alBottom
        DoubleBuffered = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentDoubleBuffered = False
        PopupMenu = PopupMenuParentTabs
        SoftTop = True
        Style = tsSoftTabs
        Tabs.Strings = (
          '0')
        TabIndex = 0
        OnClick = TabSetObjClick
        ExplicitWidth = 1052
      end
      object ListBoxDebug: TListBox
        Left = 0
        Top = 0
        Width = 108
        Height = 85
        BevelInner = bvNone
        BevelKind = bkFlat
        BevelOuter = bvNone
        Ctl3D = True
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        OnDblClick = ListBoxDebugDblClick
      end
    end
  end
  object TabLevels: TTabControl
    Left = 0
    Top = 26
    Width = 1184
    Height = 22
    Align = alTop
    TabOrder = 5
    Tabs.Strings = (
      '0')
    TabIndex = 0
    OnChange = TabLevelsChange
    ExplicitWidth = 1180
  end
  object FakeControl: TButton
    Left = 8
    Top = 94
    Width = 29
    Height = 25
    Caption = 'fake'
    TabOrder = 6
  end
  object Sh_sqr: TPanel
    Left = 841
    Top = 541
    Width = 16
    Height = 14
    BevelOuter = bvNone
    TabOrder = 7
  end
  object PaintBox1: TPanel
    Left = 18
    Top = 54
    Width = 46
    Height = 34
    ParentCustomHint = False
    BevelOuter = bvNone
    BiDiMode = bdLeftToRight
    Ctl3D = False
    DoubleBuffered = False
    FullRepaint = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBiDiMode = False
    ParentBackground = False
    ParentCtl3D = False
    ParentDoubleBuffered = False
    ParentFont = False
    ParentShowHint = False
    ShowCaption = False
    ShowHint = False
    TabOrder = 8
    OnDblClick = PaintBox1DblClick
    OnDragOver = PaintBox1DragOver
    OnMouseDown = Shape_crMouseDown
    OnMouseLeave = PaintBox1MouseLeave
    OnMouseMove = PaintBox1MouseMove
    OnMouseUp = PaintBox1MouseUp
  end
  object ActionList1: TActionList
    Images = ImageList1
    OnExecute = ActionList1Execute
    Left = 156
    Top = 54
    object ActNew: TAction
      Category = 'file'
      Caption = 'mNewProject'
      ImageIndex = 0
      OnExecute = ActNewExecute
    end
    object ActMapSaveTo: TAction
      Category = 'file'
      Caption = 'mExportDataTo'
      ImageIndex = 2
      OnExecute = ActMapSaveToExecute
    end
    object ActMapSave: TAction
      Category = 'file'
      Caption = 'mExportData'
      ImageIndex = 2
      OnExecute = ActMapSaveExecute
    end
    object ActLockGameObject: TAction
      Category = 'obj'
      Caption = 'mLockGameObject'
      Hint = 'mLockGameObject'
      ImageIndex = 37
      ShortCut = 49228
      OnExecute = ActLockGameObjectExecute
    end
    object ActXMLSave: TAction
      Category = 'file'
      Caption = 'mExportXML'
      ImageIndex = 2
      OnExecute = ActXMLSaveExecute
    end
    object ActExit: TAction
      Category = 'file'
      Caption = 'mExit'
      OnExecute = ActExitExecute
    end
    object ActColor: TAction
      Category = 'bg'
      Caption = 'mChangeBackdrop'
    end
    object ActInfo: TAction
      Category = 'view'
      Caption = 'mMapInfo'
      ImageIndex = 4
      OnExecute = ActInfoExecute
    end
    object ActProjSave: TAction
      Category = 'file'
      Caption = 'mSaveProject'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = ActProjSaveExecute
    end
    object ActGrid: TAction
      Category = 'view'
      Caption = 'mGrid'
      Checked = True
      OnExecute = ActGridExecute
    end
    object ActHideObjects: TAction
      Category = 'view'
      Caption = 'mHideGameObjects'
      OnExecute = ActHideObjectsExecute
    end
    object ActCloneMap: TAction
      Category = 'file'
      Caption = 'mCloneMap'
      OnExecute = ActCloneMapExecute
    end
    object ActPath: TAction
      Category = 'options'
      Caption = 'mResPathSettings'
      ImageIndex = 3
      OnExecute = ActPathExecute
    end
    object ActBackAdd: TAction
      Category = 'bg'
      Caption = 'mAddItem'
      ImageIndex = 5
      OnExecute = ActBackAddExecute
    end
    object ActBackEdit: TAction
      Category = 'bg'
      Caption = 'mEditProperties'
      ImageIndex = 7
      OnExecute = ActBackEditExecute
    end
    object ActBackDel: TAction
      Category = 'bg'
      Caption = 'mDeleteItem'
      ImageIndex = 6
      OnExecute = ActBackDelExecute
    end
    object ActObjAdd: TAction
      Category = 'obj'
      Caption = 'mAddGameObject'
      Hint = 'mAddGameObject'
      ImageIndex = 5
      OnExecute = ActObjAddExecute
    end
    object ActObjDel: TAction
      Category = 'obj'
      Caption = 'mDeleteGameObject'
      Hint = 'mDeleteGameObject'
      ImageIndex = 6
      OnExecute = ActObjDelExecute
    end
    object ActProjSaveAs: TAction
      Category = 'file'
      Caption = 'mSaveProjectAs'
      ImageIndex = 2
      ShortCut = 49235
      OnExecute = ActProjSaveAsExecute
    end
    object ActObjEdit: TAction
      Category = 'obj'
      Caption = 'mGameObjectsProperties'
      Hint = 'mGameObjectsProperties'
      ImageIndex = 7
      OnExecute = ActObjEditExecute
    end
    object ActProjLoad: TAction
      Category = 'file'
      Caption = 'mOpenProject'
      ImageIndex = 1
      ShortCut = 16460
      OnExecute = ActProjLoadExecute
    end
    object ActMapObjEdit: TAction
      Category = 'obj'
      Caption = 'mProperties'
      Hint = 'mProperties'
      ImageIndex = 7
      OnExecute = ActMapObjEditExecute
    end
    object ActMapObjDel: TAction
      Category = 'obj'
      Caption = 'mDoDelete'
      Hint = 'mDoDelete'
      ImageIndex = 6
      OnExecute = ActMapObjDelExecute
    end
    object ActMapObjClone: TAction
      Category = 'obj'
      Caption = 'mClone'
      OnExecute = ActMapObjCloneExecute
    end
    object ActShowRectObj: TAction
      Category = 'view'
      Caption = 'mShowGameObjectWorkZones'
      Checked = True
      OnExecute = ActShowRectObjExecute
    end
    object ActSetVatDef: TAction
      Category = 'obj'
      Caption = 'mSetGameObjectsPropertiesByDefault'
      Hint = 'mSetGameObjectsPropertiesByDefault'
      OnExecute = ActSetVatDefExecute
    end
    object ActVarList: TAction
      Category = 'file'
      Caption = 'mGenerateConstants'
      ImageIndex = 4
      OnExecute = ActVarListExecute
    end
    object ActClearMap: TAction
      Category = 'file'
      Caption = 'mClearMap'
      OnExecute = ActClearMapExecute
    end
    object ActSetVarDefGroup: TAction
      Category = 'obj'
      Caption = 'mSetThisGroupPropertiesByDefault'
      Hint = 'mSetThisGroupPropertiesByDefault'
      OnExecute = ActSetVarDefGroupExecute
    end
    object ActSetVarparentToDef: TAction
      Category = 'obj'
      Caption = 'mSetThisObjectPropertiesByPattern'
      Hint = 'mSetThisObjectPropertiesByPattern'
      OnExecute = ActSetVarparentToDefExecute
    end
    object ActEditMode: TAction
      Category = 'mode'
      Caption = 'mEditing'
      Hint = 'mEditing'
      ImageIndex = 10
      ShortCut = 115
      OnExecute = ActEditModeExecute
    end
    object ActCursorMode: TAction
      Category = 'mode'
      Caption = 'mCursor'
      Hint = 'mCursor'
      ImageIndex = 11
    end
    object ActAniDisableAll: TAction
      Category = 'animation'
      Caption = 'mSwitchOffForAll'
    end
    object ActAniEnableObj: TAction
      Category = 'animation'
      Caption = 'mSwitchOn'
      OnExecute = ActAniEnableObjExecute
    end
    object ActAniDisableObj: TAction
      Category = 'animation'
      Caption = 'mSwitchOff'
      OnExecute = ActAniDisableObjExecute
    end
    object ActAniEnableBG: TAction
      Category = 'animation'
      Caption = 'mSwitchOn'
    end
    object ActAniDisableBG: TAction
      Category = 'animation'
      Caption = 'mSwitchOff'
    end
    object ActEditorOpt: TAction
      Category = 'options'
      Caption = 'mEditorSettings'
      ImageIndex = 3
      OnExecute = ActEditorOptExecute
    end
    object ActObjOrder: TAction
      Category = 'obj'
      Caption = 'mObjectManager'
      Hint = 'mObjectManager'
      ImageIndex = 12
      ShortCut = 16463
      OnExecute = ActObjOrderExecute
    end
    object ActLockDecor: TAction
      Category = 'obj'
      Caption = 'mLockDecorations'
      Hint = 'mLockDecorations'
      ImageIndex = 13
      ShortCut = 49220
      OnExecute = ActLockDecorExecute
    end
    object ActMessages: TAction
      Category = 'file'
      Caption = 'mTextEditor'
      ImageIndex = 16
      ShortCut = 49221
      OnExecute = ActMessagesExecute
    end
    object ActObjPattern: TAction
      Category = 'options'
      Caption = 'mPropertiesTemplate'
      Hint = 'mPropertiesTemplate'
      ImageIndex = 17
      OnExecute = ActObjPatternExecute
    end
    object ActScript: TAction
      Category = 'options'
      Caption = 'mMiniscriptsEditor'
      Hint = 'mMiniscriptsEditor'
      ImageIndex = 17
      ShortCut = 121
      OnExecute = ActScriptExecute
    end
    object ActStates: TAction
      Category = 'options'
      Caption = 'mStateTypes'
      Hint = 'mStateTypes'
      ImageIndex = 17
      OnExecute = ActStatesExecute
    end
    object ActTypes: TAction
      Category = 'bg'
      Caption = 'mTileObjectTypes'
      ImageIndex = 17
      OnExecute = ActTypesExecute
    end
    object ActOppositeStates: TAction
      Category = 'options'
      Caption = 'mStateCorrespondingMap'
      Hint = 'mStateCorrespondingMap'
      ImageIndex = 17
      OnExecute = ActOppositeStatesExecute
    end
    object ActAnimation: TAction
      Category = 'obj'
      Caption = 'mAnimation'
      Hint = 'mAnimation'
      OnExecute = ActAnimationExecute
    end
    object ActStartEmul: TAction
      Category = 'mode'
      Caption = 'mStartDebuger'
      Hint = 'mStartDebuger'
      ImageIndex = 18
      ShortCut = 116
      OnExecute = ActStartEmulExecute
    end
    object ActStart: TAction
      Category = 'animation'
      Caption = 'mStart'
      Hint = 'mStart'
      ImageIndex = 20
      ShortCut = 117
      OnExecute = ActStartExecute
    end
    object ActPause: TAction
      Category = 'animation'
      Caption = 'mPause'
      Hint = 'mPause'
      ImageIndex = 21
      ShortCut = 118
      OnExecute = ActPauseExecute
    end
    object ActStep: TAction
      Category = 'animation'
      Caption = 'mStepForward'
      Hint = 'mStepForward'
      ImageIndex = 19
      ShortCut = 119
      OnExecute = ActStepExecute
    end
    object ActShowBGTransMap: TAction
      Category = 'view'
      Caption = 'mShowPassAndOpaqTiles'
      OnExecute = ActShowBGTransMapExecute
    end
    object ActClearDbgMessages: TAction
      Category = 'debug'
      Caption = 'mClearMessages'
      OnExecute = ActClearDbgMessagesExecute
    end
    object ActStopOnScript: TAction
      Category = 'debug'
      Caption = 'mStopOnMiniscriptStart'
      OnExecute = ActStopOnScriptExecute
    end
    object ActResetTraceScripts: TAction
      Category = 'debug'
      Caption = 'mResetMiniscriptDebugFlags'
      OnExecute = ActResetTraceScriptsExecute
    end
    object ActStartScript: TAction
      Category = 'debug'
      Caption = 'mStartMiniscript'
    end
    object ActObjMode: TAction
      Category = 'mode'
      Caption = 'mModeObj'
      Hint = 'mModeObj'
      ImageIndex = 22
      ShortCut = 112
      OnExecute = ActObjModeExecute
    end
    object ActBackMode: TAction
      Category = 'mode'
      Caption = 'mModeBG'
      Hint = 'mModeBG'
      ImageIndex = 23
      ShortCut = 113
      OnExecute = ActBackModeExecute
    end
    object ShowNonActiveZoneObj: TAction
      Category = 'view'
      Caption = 'mShowInnactiveZonesGameObjects'
      Checked = True
      OnExecute = ShowNonActiveZoneObjExecute
    end
    object ActFindNext: TAction
      Category = 'obj'
      Caption = 'mFindNextObject'
      Hint = 'mFindNextObject'
      ShortCut = 16393
      OnExecute = ActFindNextExecute
    end
    object ActShowFocused: TAction
      Category = 'obj'
      Caption = 'mMarkSelectedObject'
      Hint = 'mMarkSelectedObject'
      ShortCut = 16461
      OnExecute = ActShowFocusedExecute
    end
    object ActFindObj: TAction
      Category = 'obj'
      Caption = 'mFindParent'
      Hint = 'mFindParent'
      ImageIndex = 24
      ShortCut = 16454
      OnExecute = ActFindObjExecute
    end
    object ActDbgChangeZone: TAction
      Category = 'debug'
      Caption = 'mChangeActiveZone'
      Hint = 'mChangeActiveZone'
      OnExecute = ActDbgChangeZoneExecute
    end
    object ActCmplxEnableDragChilds: TAction
      Category = 'obj'
      Caption = 'mLockCompositeParts'
      Checked = True
      OnExecute = ActCmplxEnableDragChildsExecute
    end
    object ActHidePath: TAction
      Category = 'view'
      Caption = 'mHideDirectors'
      OnExecute = ActHidePathExecute
    end
    object ActNewMap: TAction
      Category = 'file'
      Caption = 'mNewMap'
      ImageIndex = 0
      ShortCut = 16462
      OnExecute = ActNewMapExecute
    end
    object ActMapDel: TAction
      Category = 'file'
      Caption = 'mDeleteMap'
      OnExecute = ActMapDelExecute
    end
    object ActMapChangeIndex: TAction
      Category = 'file'
      Caption = 'mChangeMapIndex'
      ImageIndex = 15
      OnExecute = ActMapChangeIndexExecute
    end
    object ActMapOptions: TAction
      Category = 'file'
      Caption = 'mMapSettings'
      ImageIndex = 3
      OnExecute = ActMapOptionsExecute
    end
    object ActAnimTypes: TAction
      Category = 'options'
      Caption = 'mAnimationTypes'
      ImageIndex = 17
      OnExecute = ActAnimTypesExecute
    end
    object ActTileOptions: TAction
      Category = 'bg'
      Caption = 'mTileObjectProperties'
      ImageIndex = 3
      OnExecute = ActTileOptionsExecute
    end
    object ActBGCopy: TAction
      Category = 'bg'
      Caption = 'mCopy'
      ImageIndex = 34
      ShortCut = 16451
      OnExecute = ActBGCopyExecute
    end
    object ActBGPaste: TAction
      Category = 'bg'
      Caption = 'mPaste'
      ImageIndex = 33
      SecondaryShortCuts.Strings = (
        '')
      ShortCut = 16470
      OnExecute = ActBGPasteExecute
    end
    object ActBGPasteMulti: TAction
      Category = 'bg'
      Caption = 'mMultiple'
      ShortCut = 49238
      OnExecute = ActBGPasteMultiExecute
    end
    object ActUndo: TAction
      Category = 'bg'
      Caption = 'mCancel'
      ShortCut = 16474
      OnExecute = ActUndoExecute
    end
    object ActDelSelection: TAction
      Caption = 'mDeselect'
      ShortCut = 16452
      OnExecute = ActDelSelectionExecute
    end
    object ActSelectAll: TAction
      Caption = 'mSelectAll'
      ShortCut = 16449
      OnExecute = ActSelectAllExecute
    end
    object ActShowHint: TAction
      Caption = 'ActShowHint'
      ShortCut = 16456
      OnExecute = ActShowHintExecute
    end
    object ActCollideNodesMode: TAction
      Category = 'mode'
      Caption = 'mModeCollideLines'
      Hint = 'mModeCollideLines'
      ImageIndex = 25
      ShortCut = 114
      OnExecute = ActCollideNodesModeExecute
    end
    object ActGoToBGReference: TAction
      Category = 'bg'
      Caption = 'mFindParent'
      ShortCut = 16466
      OnExecute = ActGoToBGReferenceExecute
    end
    object ActEventTypes: TAction
      Category = 'options'
      Caption = 'mEventTypes'
      ImageIndex = 17
      OnExecute = ActEventTypesExecute
    end
    object ActNewCollideShape: TAction
      Category = 'collidelines'
      Caption = 'mNewCollideShape'
      Hint = 'mNewCollideShape'
      OnExecute = ActNewCollideShapeExecute
    end
    object ActNewCollideNode: TAction
      Category = 'collidelines'
      Caption = 'mNewCollideNode'
      Hint = 'mNewCollideNode'
      OnExecute = ActNewCollideNodeExecute
    end
    object ActAnimJNCount: TAction
      Category = 'options'
      Caption = 'mAnimationJNCount'
      ImageIndex = 17
      OnExecute = ActAnimJNCountExecute
    end
  end
  object MainMenu1: TMainMenu
    Images = ImageList1
    OnChange = MainMenu1Change
    Left = 156
    Top = 102
    object N1: TMenuItem
      Caption = 'mFile'
      object N5: TMenuItem
        Action = ActNew
      end
      object N32: TMenuItem
        Action = ActProjLoad
      end
      object N94: TMenuItem
        Action = ActProjSave
      end
      object N35: TMenuItem
        Action = ActProjSaveAs
      end
      object N17: TMenuItem
        Caption = '-'
      end
      object N3: TMenuItem
        Action = ActMapSave
      end
      object N3_1: TMenuItem
        Action = ActMapSaveTo
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object N20: TMenuItem
        Action = ActEditorOpt
      end
      object N14: TMenuItem
        Action = ActPath
      end
      object N51: TMenuItem
        Caption = '-'
      end
      object N2: TMenuItem
        Action = ActExit
      end
    end
    object N76: TMenuItem
      Caption = 'mProject'
      object N7: TMenuItem
        Action = ActNewMap
      end
      object N50: TMenuItem
        Action = ActClearMap
      end
      object N47: TMenuItem
        Action = ActMapDel
      end
      object N91: TMenuItem
        Action = ActCloneMap
      end
      object N77: TMenuItem
        Action = ActMapChangeIndex
      end
      object N78: TMenuItem
        Action = ActMapOptions
      end
      object N72: TMenuItem
        Caption = '-'
      end
      object N10: TMenuItem
        Action = ActInfo
      end
      object N46: TMenuItem
        Action = ActVarList
      end
      object N33: TMenuItem
        Caption = '-'
      end
      object N62: TMenuItem
        Action = ActMessages
      end
      object N21: TMenuItem
        Caption = '-'
      end
      object N73: TMenuItem
        Action = ActAnimTypes
      end
      object mAnimationJNCount1: TMenuItem
        Action = ActAnimJNCount
      end
      object N34: TMenuItem
        Action = ActObjPattern
      end
      object N93: TMenuItem
        Action = ActEventTypes
      end
      object N65: TMenuItem
        Action = ActStates
      end
      object N4: TMenuItem
        Action = ActOppositeStates
      end
      object N64: TMenuItem
        Action = ActScript
      end
    end
    object N11: TMenuItem
      Caption = 'mTileObjects'
      object N13: TMenuItem
        Action = ActUndo
      end
      object N81: TMenuItem
        Action = ActBGCopy
      end
      object N82: TMenuItem
        Action = ActBGPaste
      end
      object N83: TMenuItem
        Action = ActBGPasteMulti
      end
      object N90: TMenuItem
        Caption = '-'
      end
      object N88: TMenuItem
        Action = ActSelectAll
      end
      object N84: TMenuItem
        Action = ActDelSelection
      end
      object N80: TMenuItem
        Caption = '-'
      end
      object N67: TMenuItem
        Action = ActTypes
      end
      object N79: TMenuItem
        Action = ActTileOptions
      end
      object N95: TMenuItem
        Caption = '-'
      end
      object N25: TMenuItem
        Action = ActGoToBGReference
      end
    end
    object N12: TMenuItem
      Caption = 'mGameObjects'
      object N30: TMenuItem
        Action = ActObjEdit
      end
      object N27: TMenuItem
        Action = ActObjAdd
      end
      object N28: TMenuItem
        Action = ActObjDel
      end
      object N87: TMenuItem
        Caption = '-'
      end
      object CtrlC1: TMenuItem
        Action = ActBGCopy
      end
      object N15: TMenuItem
        Action = ActBGPaste
      end
      object N16: TMenuItem
        Caption = '-'
      end
      object N89: TMenuItem
        Action = ActSelectAll
      end
      object N85: TMenuItem
        Action = ActDelSelection
      end
      object N60: TMenuItem
        Caption = '-'
      end
      object N26: TMenuItem
        Action = ActSetVatDef
      end
      object N29: TMenuItem
        Caption = '-'
      end
      object N61: TMenuItem
        Action = ActLockDecor
      end
      object DragDrop1: TMenuItem
        Action = ActCmplxEnableDragChilds
      end
      object N57: TMenuItem
        Caption = '-'
      end
      object NLockGameObject: TMenuItem
        Action = ActLockGameObject
      end
      object N63: TMenuItem
        Caption = '-'
      end
      object N68: TMenuItem
        Action = ActFindObj
        Caption = 'mFindObject'
      end
      object N53: TMenuItem
        Action = ActFindNext
      end
      object N58: TMenuItem
        Action = ActShowFocused
      end
      object N52: TMenuItem
        Caption = '-'
      end
      object N59: TMenuItem
        Action = ActObjOrder
      end
    end
    object MDebug: TMenuItem
      Caption = 'mDebug'
      object MDStopOnScript: TMenuItem
        Action = ActStopOnScript
      end
      object MDScripts: TMenuItem
        Caption = 'mStartMiniscript'
        OnClick = MDScriptsClick
      end
      object N71: TMenuItem
        Caption = '-'
      end
      object N69: TMenuItem
        Action = ActDbgChangeZone
      end
      object N70: TMenuItem
        Caption = '-'
      end
      object MDTraceScript: TMenuItem
        Action = ActResetTraceScripts
      end
      object MDResetRunOnceScripts: TMenuItem
        Caption = 'mResetAllMiniscriptRunOnceFlag'
        OnClick = MDResetRunOnceScriptsClick
      end
      object MDResetAll: TMenuItem
        Caption = 'mResetAll'
        OnClick = MDResetAllClick
      end
      object N86: TMenuItem
        Caption = '-'
      end
      object MDClearDebugMessages: TMenuItem
        Action = ActClearDbgMessages
      end
    end
    object N19: TMenuItem
      Caption = 'mView'
      object NVAnimation: TMenuItem
        Action = ActAnimation
      end
      object MMVHideHints: TMenuItem
        AutoCheck = True
        Caption = 'mAutohintsOff'
      end
      object N22: TMenuItem
        Action = ActHideObjects
      end
      object N75: TMenuItem
        Action = ActHidePath
      end
      object N48: TMenuItem
        Action = ShowNonActiveZoneObj
      end
      object N44: TMenuItem
        Action = ActShowRectObj
      end
      object N23: TMenuItem
        Caption = '-'
      end
      object N49: TMenuItem
        Action = ActShowBGTransMap
      end
      object N24: TMenuItem
        Action = ActGrid
      end
      object N92: TMenuItem
        Caption = '-'
      end
      object MVLanguages: TMenuItem
        Caption = 'mCurrentLangugage'
      end
    end
    object N9: TMenuItem
      Caption = 'mHelp'
      object N45: TMenuItem
        Caption = 'mAbout'
        OnClick = N45Click
      end
    end
  end
  object Timer_cr: TTimer
    Interval = 100
    OnTimer = Timer_crTimer
    Left = 164
    Top = 246
  end
  object OpenDialog: TOpenDialog
    Left = 84
    Top = 54
  end
  object SaveDialog: TSaveDialog
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 84
    Top = 102
  end
  object ImageList1: TImageList
    ColorDepth = cd32Bit
    AllocBy = 34
    Masked = False
    Left = 84
    Top = 150
    Bitmap = {
      494C010128006C00220010001000FFFFFFFF2110FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000B0000000010020000000000000B0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000094918E0094918E007B7B7A007B7B7A007B7B
      7A007B7B7A0094918E0094918E00000000000000000000000000000000000000
      000000000000000000000000000094918E006B6B6B006B6B6B006B6B6B006B6B
      6B006B6B6B006B6B6B0094918E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094918E0094918E007B7B7A007B7B7A007B7B7A007B7B7A0094918E009491
      8E00000000000000000000000000000000000000000000000000000000000000
      000094918E0094918E007B7B7A007B7B7A007B7B7A007B7B7A0094918E009491
      8E0038383700383837004A48450094918E000000000000000000000000000000
      000094918E006B6B6B006B6B6B006B6B6B006B6B6B006B6B6B006B6B6B009E92
      8F00000000007B7B7A004A48450094918E000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000009491
      8E004A4845003838370026262700262627002626270026262700383837004A48
      450094918E000000000000000000000000000000000094918E0094918E007B7B
      7A007B7B7A007B7B7A007B7B7A0094918E0094918E0038383700383837004A48
      450094918E00383837003838370094918E000000000094918E006B6B6B006B6B
      6B006B6B6B006B6B6B006B6B6B006B6B6B009E928F00000000007B7B7A004A48
      450094918E00000000004A48450094918E000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000009491
      8E00383837002626270026262700383837003838370026262700262627003838
      370094918E0000000000000000000000000094918E004A484500383837002626
      2700262627002626270026262700383837004A48450094918E00383837003838
      370094918E0038383700383837007B7B7A0094918E004A4845007B7B7A000000
      00000000000000000000000000007B7B7A004A48450094918E007B7B7A004A48
      450094918E00000000004A4845007B7B7A000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7A002626270026262700262627003D3D3D003D3D3D0026262700262627002626
      27007B7B7A0000000000000000000000000094918E0038383700262627002626
      2700383837003838370026262700262627003838370094918E00383837003838
      37007B7B7A0038383700383837007B7B7A0094918E004A484500000000000000
      00006B6B6B006B6B6B0000000000000000004A48450094918E007B7B7A004A48
      45007B7B7A00000000004A4845007B7B7A000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7A002626270026262700262627003D3D3D003D3D3D0026262700262627002626
      27007B7B7A000000000000000000000000007B7B7A0026262700262627002626
      27003D3D3D003D3D3D002626270026262700262627007B7B7A00383837003838
      37007B7B7A0038383700383837007B7B7A007B7B7A004A484500000000000000
      00007B7B7A007B7B7A0000000000000000004A4845007B7B7A007B7B7A004A48
      45007B7B7A00000000004A4845007B7B7A000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7A002626270026262700262627003D3D3D003D3D3D0026262700262627002626
      27007B7B7A000000000000000000000000007B7B7A0026262700262627002626
      27003D3D3D003D3D3D002626270026262700262627007B7B7A00383837003838
      37007B7B7A0038383700383837007B7B7A007B7B7A004A484500000000000000
      00007B7B7A007B7B7A0000000000000000004A4845007B7B7A00000000004A48
      45007B7B7A00000000004A4845007B7B7A000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7A00262627002626270026262700262627002626270026262700262627002626
      27007B7B7A000000000000000000000000007B7B7A0026262700262627002626
      27003D3D3D003D3D3D002626270026262700262627007B7B7A00383837003838
      37007B7B7A0038383700383837007B7B7A007B7B7A004A484500000000000000
      00007B7B7A007B7B7A0000000000000000004A4845007B7B7A00000000004A48
      45007B7B7A00000000004A4845007B7B7A000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7A00262627002626270026262700262627002626270026262700262627002626
      27007B7B7A000000000000000000000000007B7B7A0026262700262627002626
      270026262700262627002626270026262700262627007B7B7A00383837003838
      37007B7B7A003D3D3D003D3D3D007B7B7A007B7B7A004A484500000000000000
      0000000000000000000000000000000000004A4845007B7B7A004A4845004A48
      45007B7B7A004A4845004A4845007B7B7A000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7A003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D
      3D007B7B7A000000000000000000000000007B7B7A0026262700262627002626
      270026262700262627002626270026262700262627007B7B7A003D3D3D003D3D
      3D007B7B7A004A4845007B7B7A00000000007B7B7A004A484500000000000000
      0000000000000000000000000000000000004A4845007B7B7A004A4845004A48
      45007B7B7A004A4845007B7B7A00000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000808080008080
      8000808080000000000000000000000000000000000000000000000000000000
      00007B7B7A004A484500000000000000000000000000000000004A4845007B7B
      7A00000000000000000000000000000000007B7B7A003D3D3D003D3D3D003D3D
      3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D007B7B7A004A4845007B7B
      7A00000000004A4845007B7B7A00000000007B7B7A004A4845004A4845004A48
      45004A4845004A4845004A4845004A4845004A4845007B7B7A004A4845007B7B
      7A00000000004A4845007B7B7A0000000000000000000000000000000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000808080000000000000000000000000000000000000000000000000000000
      00007B7B7A004A484500000000000000000000000000000000004A4845007B7B
      7A0000000000000000000000000000000000000000007B7B7A004A4845000000
      00007B7B7A004A484500000000004A4845007B7B7A00000000004A4845007B7B
      7A00000000004A4845007B7B7A00000000000000000000000000000000000000
      00007B7B7A007B7B7A00000000004A4845007B7B7A00000000004A4845007B7B
      7A00000000004A4845007B7B7A00000000000000000000000000000000000000
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000008080
      8000000000000000000000000000000000000000000000000000000000000000
      00007B7B7A004A484500000000000000000000000000000000004A4845007B7B
      7A0000000000000000000000000000000000000000007B7B7A004A4845000000
      00007B7B7A004A484500000000004A4845007B7B7A007B7B7A004A4845007B7B
      7A007B7B7A004A48450094918E0000000000000000007B7B7A007B7B7A000000
      00007B7B7A004A484500000000004A4845007B7B7A004A4845004A4845008389
      BE007B7B7A004A48450094918E00000000000000000000000000000000000000
      000000000000FF000000FF000000FF000000FF000000FF000000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094918E004A4845007B7B7A0000000000000000007B7B7A004A4845009491
      8E0000000000000000000000000000000000000000007B7B7A004A4845000000
      000094918E004A4845007B7B7A004A4845007B7B7A007B7B7A004A4845009491
      8E004A48450094918E000000000000000000000000007B7B7A004A4845000000
      000094918E004A4845004A4845004A4845008B8B8A007B7B7A004A4845009491
      8E004D4D4D0094918E0000000000000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000094918E004A4845004A4845004A4845004A48450094918E000000
      0000000000000000000000000000000000000000000094918E004A4845007B7B
      7A000000000094918E007B7B7A004A48450094918E004A48450094918E000000
      0000000000000000000000000000000000000000000094918E004A4845004A48
      45000000000094918E007B7B7A004A48450094918E004D4D4D0094918E000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000808080008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000094918E004A48
      45004A4845004A4845004A48450094918E000000000000000000000000000000
      000000000000000000000000000000000000000000000000000094918E004A48
      45004A4845004A4845004A48450094918E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001C8A
      CC001C8ACC008C645F008C645F008C645F008C645F008C645F008C645F008C64
      5F008C645F008C645F008C645F00000000000000000000000000000000000000
      000000000000A4766C0074424400744244007442440074424400744244007442
      4400744244007442440074424400000000000000000000000000000000000000
      0000000000000000000080808000808080008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000C0C0C0000000
      0000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C0000000000000000000000000000000000000000000000000001C8ACC006FC7
      EC006FC7EC00C0B5AB00F4E2CE00F4E2CE00F4E2CE00F4E2CE00F4E2CE00F4E2
      CE00F4E2CE00F4E2CE008C645F00000000000000000000000000000000000000
      000000000000A4766C00FCFCF800F6ECD800F6ECD800F6ECD800F6ECD800F6EC
      D800F6ECD800F7DFC70074424400000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000C0C0C0000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C00000000000000000000000000000000000000000001C8ACC0080E9FA0080E9
      FA0080E9FA00C0B5AB00F4E2CE00F3D3B200F3D3B200F3D3B200F3D3B200F3D3
      B200F3D3B200F4E2CE008C645F00000000000000000000000000000000000000
      000000000000A4766C00F6ECD800F5D7B700F5D7B700F5D7B700E6D4C200E6D4
      C200F5D7B700E6D4C20074424400000000000000000000000000000000000000
      000000000000FF000000FF000000FF000000FF000000FF000000808080000000
      0000000000000000000000000000000000000000000000000000C0C0C0000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C00000000000000000000000000000000000000000001C8ACC0080E9FA0080E9
      FA0080E9FA00C0B5AB00F4E2CE00F4E2CE00F4E2CE00F4E2CE00F4E2CE00F4E2
      CE00F4E2CE00F3D3B2008C645F00000000000000000000000000000000000000
      000000000000A4766C00F6ECD800FCC69400FCC69400FCC69400FCC69400FCC6
      9400FCC69400E6D4C20074424400000000000000000000000000000000000000
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF0000008080
      8000000000000000000000000000000000000000000000000000C0C0C0000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C00000000000000000000000000000000000000000001C8ACC0080E9FA0080E9
      FA0080E9FA00C0B5AB00F4E2CE00F3D3B200F3D3B200F3D3B200F3D3B200F3D3
      B200F3D3B200F4E2CE008C645F000000000000000000A4766C00744244007442
      440074424400A4766C00FCFCF800F5D7B700F5D7B700FCC69400F5D7B700F5D7
      B700F5D7B700E6D4C20084524C0000000000000000000000000000000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000808080000000000000000000000000000000000000000000C0C0C0000000
      00000000000000000000C0C0C00000000000000000000000000000000000C0C0
      C00000000000000000000000000000000000000000001C8ACC0080E9FA0080E9
      FA0080E9FA00C0B5AB00F4E2CE00F4E2CE00F4E2CE00F4E2CE00F4E2CE00F4E2
      CE00F4E2CE00F3D3B2008C645F000000000000000000A4766C00FCFCF800F6EC
      D800F6ECD800A4766C00FCFCF800F5D7B700F5D7B700F7DFC700F5D7B700F5D7
      B700F5D7B700E6D4C20084524C00000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000808080008080
      8000808080000000000000000000000000000000000000000000C0C0C0000000
      00000000000000000000C0C0C00000000000000000000000000000000000C0C0
      C00000000000000000000000000000000000000000001C8ACC00AEEDF800AEED
      F800AEEDF800C0B5AB00F4E2CE00F3D3B200F3D3B200F3D3B200F3D3B200F3D3
      B200F3D3B200F4E2CE008C645F000000000000000000A4766C00F6ECD800F5D7
      B700F5D7B700A4766C00FCFCF800FCC69400FCC69400FCC69400FCC69400FCC6
      9400FCC69400E6D4C2008C5E5C00000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000C0C0C0000000
      00000000000000000000C0C0C00000000000000000000000000000000000C0C0
      C00000000000000000000000000000000000000000001C8ACC00AEEDF800AEED
      F800AEEDF800C0B5AB00F4E2CE00F4E2CE00F4E2CE00F4E2CE00F4E2CE00F4E2
      CE00F4E2CE00F4E2CE008C645F000000000000000000A4766C00F6ECD800FCC6
      9400FCC69400A4766C00FCFCF800F7DFC700F7DFC700F7DFC700F5D7B700F7DF
      C700F6ECD800E6D4C2008C5E5C00000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000C0C0C0000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001C8ACC00AEEDF800AEED
      F800AEEDF800C0B5AB00FAF4EE00FAF4EE00FAF4EE00FAF4EE00FAF4EE00FAF4
      EE00C0B5AB00B69276008C645F000000000000000000A4766C00FCFCF800F5D7
      B700F5D7B700A4766C00FCFCF800FCFCF800FCFCF800FCFCF800FCFCF800E6D4
      C200C4B2AC00A49294009C665C00000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C0000000000000000000C0C0C000C0C0C000C0C0
      C00000000000000000000000000000000000000000001C8ACC00D7F5F900D7F5
      F900D7F5F900C0B5AB00FAF4EE00FAF4EE00FAF4EE00FAF4EE00FAF4EE00C0B5
      AB00B6927600F47A4400000000000000000000000000A4766C00F6ECD800F5D7
      B700F5D7B700A4766C00FCFCF800FCFCF800FCFCF800FCFCF800FCFCF800A476
      6C00A4766C00A4766C00A4766C00000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001C8ACC00D7F5F900D7F5
      F900D7F5F900C0B5AB00FAF4EE00FAF4EE00FAF4EE00FAF4EE00FAF4EE00F3D3
      B200B69276001C8ACC00000000000000000000000000A4766C00FCFCF800FCC6
      9400FCC69400A4766C00FCFCF800FCFCF800FCFCF800FCFCF800FCFCF800A476
      6C00E4A25400B4725C0000000000000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001C8ACC00D7F5F900FAF4
      EE00FAF4EE00C0B5AB00C0B5AB00C0B5AB00C0B5AB00C0B5AB00C0B5AB00C0B5
      AB0087B5BF001C8ACC00000000000000000000000000A4766C00FCFCF800F7DF
      C700F7DFC700A4766C00A4766C00A4766C00A4766C00A4766C00A4766C00A476
      6C00A4766C000000000000000000000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000000000000000000000000000C0C0C0000000
      000000000000000000000000000000000000000000001C8ACC00D7F5F900FAF4
      EE0087B5BF009CC6CC0087B5BF0087B5BF0087B5BF0087B5BF009CC6CC00D7F5
      F9006FC7EC001C8ACC00000000000000000000000000A4766C00FCFCF800FCFC
      F800FCFCF800FCFCF800FCFCF800E6D4C200C4B2AC00A49294009C665C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C0C0C0000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001C8ACC00D7F5F900C0B5
      AB00B6927600C0B5AB00C0B5AB00C0B5AB00C0B5AB00C0B5AB00B6927600D7F5
      F9006FC7EC001C8ACC00000000000000000000000000A4766C00FCFCF800FCFC
      F800FCFCF800FCFCF800FCFCF800A4766C00A4766C00A4766C00A4766C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF000000FF000000FF00000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C0C0C00000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001C8ACC009CC6
      CC0074868C00F3D3B200FAF4EE00FAF4EE00FAF4EE00C0B5AB0074868C006FC7
      EC001C8ACC0000000000000000000000000000000000A4766C00FCFCF800FCFC
      F800FCFCF800FCFCF800FCFCF800A4766C00E4A25400A4766C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001C8A
      CC001C8ACC008C645F0074868C008C645F0074868C008C645F001C8ACC001C8A
      CC000000000000000000000000000000000000000000A4766C00A4766C00A476
      6C00A4766C00A4766C00A4766C00A4766C00B4725C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C0C0C00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A5A5A500846363008463
      6300846363000000000084636300846363000000000084636300846363000000
      0000846363008463630000000000000000000000000000000000000000000000
      0000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C0000000000000000000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000000000000000
      0000000000000000000000000000000000000000000084636300C6636300C642
      000000C6210000C6210000000000000000000000000000C6210000C621000000
      0000000000000000000084636300000000000000000000000000000000000000
      00000000000000000000C0C0C000000000000000000000000000C0C0C0000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000000000000000
      0000000000000000000000000000000000000000000084636300C6636300C642
      0000C642000000C6210042E7210000000000000000000000000000C621000000
      00000000000000C6210084636300000000000000000000000000000000000000
      00000000000000000000C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C0C0C0000000
      00000000000000000000C0C0C000000000000000000000000000000000000000
      00000000000000000000C0C0C0000000000000000000C0C0C000000000000000
      00000000000000000000000000000000000000000000C0C0C000000000000000
      000000000000C0C0C00000000000000000000000000084636300C6636300C642
      0000C6420000C6420000C642000000C621000000000000000000000000000000
      000042E7210000C6210084636300000000000000000000000000000000000000
      00000000000000000000C0C0C000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000000000000000
      00000000000000000000C0C0C000000000000000000000000000000000000000
      00000000000000000000C0C0C0000000000000000000C0C0C000000000000000
      00000000000000000000000000000000000000000000C0C0C000000000000000
      00000000000000000000C0C0C000000000000000000084636300C6636300C642
      0000C6420000C6420000C6420000C642000000C62100000000000000000042E7
      210000C6210000C6210084636300000000000000000000000000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C0000000000000000000C0C0C000C0C0C000C0C0
      C00000000000000000000000000000000000C0C0C0000000000000000000C0C0
      C000C0C0C000C0C0C000C0C0C00000000000C0C0C000C0C0C000C0C0C0000000
      00000000000000000000C0C0C0000000000000000000C0C0C000000000000000
      000000000000C0C0C000C0C0C000C0C0C00000000000C0C0C000C0C0C000C0C0
      C000C0C0C0000000000000000000C0C0C0000000000084636300F7CEA500C6A5
      0000C6A50000C6A50000C6A50000C6A500005337000000000000000000000000
      000042E7210000C6210084636300000000000000000000000000C0C0C0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C0000000000000000000C0C0C000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084636300F7CEA500C6A5
      0000C6A5000084E7E70084E7E700000000000000000000000000C68400000000
      00000000000042E7210084636300000000000000000000000000C0C0C0000000
      00000000000000000000C0C0C00000000000000000000000000000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C0000000000000000000C0C0C000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084636300F7CEA500C6A5
      000084E7E70000FFFF0000FFFF000000000000000000C6A50000C6A500000000
      000000000000C684000084636300000000000000000000000000C0C0C0000000
      00000000000000000000C0C0C00000000000000000000000000000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000000000000000000000000000000000000000
      00000000000000000000C0C0C0000000000000000000C0C0C000000000000000
      00000000000000000000000000000000000000000000C0C0C000000000000000
      0000000000000000000000000000000000000000000084636300F7CEA500C6A5
      000084E7E70000FFFF0000FFFF000000000000000000C6A50000C6A500000000
      000000000000C6A5000084636300000000000000000000000000C0C0C0000000
      00000000000000000000C0C0C00000000000000000000000000000000000C0C0
      C00000000000000000000000000000000000000000000000000000000000C0C0
      C0000000000000000000C0C0C000000000000000000000000000000000000000
      00000000000000000000C0C0C0000000000000000000C0C0C000000000000000
      00000000000000000000000000000000000000000000C0C0C000000000000000
      0000C0C0C0000000000000000000000000000000000084636300F7CEA500C6A5
      0000C6A5000084E7E70084E7E7000000000000000000C6A50000C6A500000000
      000000000000C6A5000084636300000000000000000000000000C0C0C0000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C00000000000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C0000000000000000000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C00000000000C0C0C000000000000000
      0000000000000000000000000000000000000000000084636300F7CEA500C6A5
      0000C6A50000C6A50000C6A500005345000000000000C6A50000C6A500000000
      000053450000C6A5000084636300000000000000000000000000C0C0C0000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084636300F7CEA500F7CE
      A500F7CEA500F7CEA500F7CEA500F7CEA50000000000F7CEA500F7CEA5000000
      0000F7CEA500F7CEA50084636300000000000000000000000000C0C0C0000000
      000000000000000000000000000000000000000000000000000000000000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A5A5A500846363008463
      6300846363008463630084636300846363008463630084636300846363008463
      63008463630084636300A5A5A500000000000000000000000000C0C0C0000000
      0000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848C9400737384007373
      8400B5BDBD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000848C940073738400B58C8C00525A
      5A0073738400B5BDBD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000808080000000
      0000808080008080800000000000808080008080800000000000808080008080
      8000000000008080800080808000000000000000000000000000CBCBCA00CBCB
      CA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCB
      CA00CBCBCA00CBCBCA00CBCBCA00CBCBCA000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005AB5F7004A84D60073738400B58C
      8C00525A5A0073738400B5BDBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000CBCBCA00FDFD
      FB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCB
      CA00FDFDFB00FDFDFB00CBCBCA00FDFDFB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B5CECE005AB5F7004A84D6007373
      84008C7B7B00525A5A0073738400B5BDBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CBCBCA00FDFD
      FB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCB
      CA00FDFDFB00FDFDFB00CBCBCA00FDFDFB000000000000000000000000000000
      0000000000000303020000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084C6F7005AB5F7004A84
      D60073738400B58C8C00525A5A00848C9400B5CECE00B5CECE00B5CECE00B5CE
      CE00000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000CBCBCA00CBCB
      CA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCB
      CA00CBCBCA00CBCBCA00CBCBCA00CBCBCA000000000000000000000000000000
      0000000000000303020000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084C6F7005AB5
      F7004A84D600737384008C7B7B00525A5A007373840073738400525A5A00525A
      5A0073738400848C9400B5CECE00000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000CBCBCA00FDFD
      FB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCB
      CA00FDFDFB00FDFDFB00CBCBCA00FDFDFB000000000000000000000000000000
      0000000000000303020003030200030302000303020003030200030302000303
      02000303020003030200030302000303020000000000000000000000000084C6
      F7005AB5F7004A84D600848C94008C7B7B00B58C8C00D6B59C00F7D6B500D6B5
      9C0073738400525A5A008C7B7B00B5CECE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CBCBCA00FDFD
      FB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCB
      CA00FDFDFB00FDFDFB00CBCBCA00FDFDFB000000000000000000000000000000
      000000000000030302007F8281007F8281007F8281007F8281007F8281007F82
      81007F8281007F8281007F828100030302000000000000000000000000000000
      000084C6F700B5BDBD00B58C8C00F7D6B500FFEFC600FFFFDE00FFFFDE00FFFF
      DE00FFEFC6008C7B7B00525A5A00848C94000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000CBCBCA00CBCB
      CA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCB
      CA00CBCBCA00CBCBCA00CBCBCA00CBCBCA000000000000000000030302000303
      02000303020003030200CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCB
      CA00CBCBCA00CBCBCA00CBCBCA00030302000000000000000000000000000000
      000000000000B5BDBD00F7D6B500FFEFC600FFEFC600FFFFDE00FFFFDE00FFFF
      DE00FFFFDE00FFEFC600525A5A00737384000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000CBCBCA00FDFD
      FB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCB
      CA00FDFDFB00FDFDFB00CBCBCA00FDFDFB000000000000000000000000000000
      00000000000003030200CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCB
      CA00CBCBCA00CBCBCA00CBCBCA00030302000000000000000000000000000000
      000000000000D6B59C00FFEFC600F7D6B500FFFFDE00FFFFDE00FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00B58C8C00525A5A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CBCBCA00FDFD
      FB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCB
      CA00FDFDFB00FDFDFD00CBCBCA00FDFDFB000000000000000000000000000000
      0000000000000303020003030200030302000303020003030200030302000303
      0200030302000303020003030200030302000000000000000000000000000000
      000000000000D6B59C00FFEFC600FFEFC600FFEFC600FFFFDE00FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00D6B59C00737384000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000CBCBCA00CBCB
      CA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCB
      CA00CBCBCA00CBCBCA00CBCBCA00CBCBCA000000000000000000000000000000
      0000000000000303020000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F7D6B500FFEFC600FFEFC600FFEFC600FFFFDE00FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00D6B59C00737384000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000CBCBCA00FDFD
      FB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCB
      CA00FDFDFB00FDFDFB00CBCBCA00FDFDFB000000000000000000000000000000
      0000000000000303020000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6B59C00FFEFC600FFEFC600F7D6B500FFEFC600FFFFDE00FFFF
      DE00FFFFDE00FFEFC600B58C8C00848C94000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CBCBCA00FDFD
      FB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCB
      CA00FDFDFB00FDFDFB00CBCBCA00FDFDFB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6D6D600F7D6B500FFFFDE00FFFFDE00FFEFC600FFEFC600F7D6
      B500FFEFC600F7D6B5008C7B7B00B5CECE000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000000000000000000000000000CBCBCA00CBCB
      CA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCBCA00CBCB
      CA00CBCBCA00CBCBCA00CBCBCA00CBCBCA000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6B59C00FFEFC600FFFFDE00FFEFC600FFEFC600FFEF
      C600F7D6B500B58C8C00B5CECE00000000000000000080808000808080000000
      0000808080008080800000000000808080008080800000000000808080008080
      8000000000008080800080808000000000000000000000000000CBCBCA00FDFD
      FB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCBCA00FDFDFB00FDFDFB00CBCB
      CA00FDFDFB00FDFDFB00CBCBCA00FDFDFB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B5BDBD00D6B59C00D6B59C00D6B59C00D6B5
      9C00D6B59C00C6D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C68463000000000000000000844284004263
      6300844284000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600FFFFFF00FFFF
      FF00C6C6C600FFFFFF00C6A56300F7FFFF008484420084634200A5A5A500A5A5
      A500A5A5A50042636300FFFFFF000000000000000000A5A5A500846363008463
      6300846363008463630084636300846363008463630084636300846363008463
      63008463630084636300A5A5A500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600FFFFFF00FFFF
      FF00C6C6C600C6A56300F7FFFF00F7FFFF00F7FFFF00C6A5630084848400A5A5
      A500A5A5A500A5A5A50084848400000000000000000084636300C6636300C642
      000000C6210000C6210000C6210000C6210000C6210000C6210000C6210000C6
      210000C6210000C6210084636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600C6C6C600C6C6
      C600C6A56300F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00C6A5420084A5
      8400A5A5A500A5A5A50042636300000000000000000084636300C6636300C642
      0000C642000000C6210042E7210042E7210000C6210042E7210000C6210042E7
      210000C6210000C6210084636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600FFFFFF00FFFF
      FF0084634200C6A58400F7FFFF00F7FFFF00F7FFFF00C6A5630084848400A5A5
      A500A5A5A500A5A5A50084636300000000000000000084636300C6636300C642
      0000C6420000C6420000C642000000C6210042E7210000C6210042E7210000C6
      210042E7210000C6210084636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600FFFFFF00FFFF
      FF00C6C6C600C6840000C6C68400F7FFFF0084A5630084636300A5A5A500A5A5
      A500A5A5A50084636300FFFFFF00000000000000000084636300C6636300C642
      0000C6420000C6420000C6420000C642000000C6210042E7210000C6210042E7
      210000C6210000C6210084636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600C6C6C600C6C6
      C60084634200C6A52100C6A52100C6A56300C6A52100C6A54200846363004263
      630084636300C6C6C600C6C6C600000000000000000084636300F7CEA500C6A5
      0000C6A50000C6A50000C6A50000C6A50000C6840000C684000042E7210000C6
      210042E7210000C6210084636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600FFFFFF00FFFF
      FF0084634200C6A52100C6C64200C6C62100C6C64200C6C64200C6C642008463
      2100FFFFFF00C6C6C600FFFFFF00000000000000000084636300F7CEA500C6A5
      0000C6A5000084E7E70084E7E700C6A50000C6A50000C6A50000C6840000C684
      0000C684000042E7210084636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600FFFFFF00FFFF
      FF0084634200C6842100C6A54200C6A52100C6A52100C6A52100C6A521008463
      2100FFFFFF00C6C6C600FFFFFF00000000000000000084636300F7CEA500C6A5
      000084E7E70000FFFF0000FFFF0084E7E700C6A50000C6A50000C6A50000C6A5
      0000C6A50000C684000084636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600C6C6C600C6C6
      C600846342008463210084632100846321008463210084632100846321008463
      2100C6C6C600C6C6C600C6C6C600000000000000000084636300F7CEA500C6A5
      000084E7E70000FFFF0000FFFF0084E7E700C6A50000C6A50000C6A50000C6A5
      0000C6A50000C6A5000084636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600FFFFFF00FFFF
      FF00C6C6C600FFFFFF00FFFFFF00C6C6C600FFFFFF00FFFFFF00C6C6C600FFFF
      FF00FFFFFF00C6C6C600FFFFFF00000000000000000084636300F7CEA500C6A5
      0000C6A5000084E7E70084E7E700C6A50000C6A50000C6A50000C6A50000C6A5
      0000C6A50000C6A5000084636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600FFFFFF00FFFF
      FF00C6C6C600FFFFFF00FFFFFF00C6C6C600FFFFFF00FFFFFF00C6C6C600FFFF
      FF00FFFFFF00C6C6C600FFFFFF00000000000000000084636300F7CEA500C6A5
      0000C6A50000C6A50000C6A50000C6A50000C6A50000C6A50000C6A50000C6A5
      0000C6A50000C6A5000084636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600000000000000000084636300F7CEA500F7CE
      A500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500F7CEA500F7CEA50084636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6C600FFFFFF00FFFF
      FF00C6C6C600FFFFFF00FFFFFF00C6C6C600FFFFFF00FFFFFF00C6C6C600FFFF
      FF00FFFFFF00C6C6C600FFFFFF000000000000000000A5A5A500846363008463
      6300846363008463630084636300846363008463630084636300846363008463
      63008463630084636300A5A5A500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C600FF000000FF000000FF000000C6C6
      C600FF000000FF000000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004242420000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00848484000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00004242420000C6210042424200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008484840084848400848484008484840084848400848484008484
      8400848484000000000000000000000000000000000000000000000000000000
      00000000000000000000FF000000FF000000C6C6C60000000000C6C6C600C6C6
      C600FF000000FF00000000000000000000000000000000000000FFFFFF00FFFF
      FF008484840000000000848484000000000000FFFF0000000000848484000000
      000084848400FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000084000000E7000000C62100424242000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084848400C6C6C60084848400C6C6C600C6C6C600C6C6C60084848400C6C6
      C600848484000000000000000000000000000000000000000000C6C6C600C6C6
      C6000000000000000000FF000000FF000000FF000000C6C6C60000000000C6C6
      C600FF000000FF00000000000000000000000000000000000000FFFFFF00C6C6
      C6000000000000FFFF00848484000000000000FFFF00000000008484840000FF
      FF0000000000C6C6C600FFFFFF00000000000000000000000000000000000000
      00000084000000E7000000E7000000C621004242420000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484000000000084848400C6C6C600C6C6C600C6C6C600848484000000
      00008484840000000000000000000000000000000000C6C6C600848400008484
      0000C6C6C60000000000C6C6C600FF000000FF000000FF000000FF000000FF00
      0000FF000000FF00000000000000000000000000000000000000FFFFFF000000
      0000848484008484840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF008484
      84008484840000000000FFFFFF00000000000000000000000000000000000000
      00000084000000E7000000E7000000E7000000C6210042424200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084848400C6C6C60084848400C6C6C600C6C6C600C6C6C60084848400C6C6
      C6008484840000000000000000000000000000000000C6C6C600848400008484
      0000C6C6C6000000000000000000C6C6C600FF000000FF000000FF000000FF00
      0000FF000000FF00000000000000000000000000000000000000FFFFFF008484
      8400000000000000000000FFFF0084848400000000008484840000FFFF000000
      00000000000084848400FFFFFF00000000000000000000000000000000000000
      00000084000000E7000000FF000000E7000000E7000000C62100424242000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484000000000084848400C6C6C600C6C6C600C6C6C600848484000000
      0000848484000000000000000000000000000000000000000000848484008484
      0000C6C6C6000000000000000000FF000000C6C6C60084840000C6C6C6000000
      0000FF000000FF00000000000000000000000000000000000000FFFFFF000000
      000000FFFF0000FFFF0000FFFF0000000000FFFFFF000000000000FFFF0000FF
      FF0000FFFF0000000000FFFFFF00000000000000000000000000000000000000
      00000084000000E7210000E7000000FF000000E7000000E7000000C621004242
      4200000000000000000000000000000000000000000000000000000000000000
      000084848400C6C6C60084848400C6C6C600C6C6C600C6C6C60084848400C6C6
      C600848484000000000000000000000000000000000000000000C6C6C6008484
      0000848400008484000084840000FF000000FF000000FF000000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000FFFFFF008484
      8400000000000000000000FFFF0084848400000000008484840000FFFF000000
      00000000000084848400FFFFFF00000000000000000000000000000000000000
      00000084000042E7000000E7210000E7000000FF000000E7000000E7000000C6
      2100424242000000000000000000000000000000000000000000000000000000
      0000848484008484840084848400848484008484840084848400848484008484
      8400848484000000000000000000000000000000000000000000000000008484
      840084840000848400008484000084840000FF000000FF000000FF000000FF00
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00848484008484840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF008484
      840084848400FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000084000042E7A50000FF000000E7210000E7000000FF000000E7000000E7
      0000008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C60084840000C6C6C60000000000C6C6C60084840000C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF000000000000FFFF00848484000000000000FFFF00000000008484840000FF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000084000042E7A50000FF000000FF000000E7210000E7000042E7A5000084
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C60084840000C6C6C60000000000C6C6C60084840000C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF008484840000000000848484000000000000FFFF0000000000848484000000
      000084848400FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000084000084E7210000FF000000FF000000FF000000E72100008400000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008484840084840000C6C6C600848400008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00848484000000000084848400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000084000084E7210000FF000000FF000084E7210000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C6C600848400008484000084840000C6C6C60000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000840000C6DEC60000FF000084E721000084000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6008484
      000084840000848400008484000084840000C6C6C60000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      000000840000C6DEC600C6DEC600008400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6008484
      0000848400008484000084840000C6C6C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000840000C6DEC60000840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000846300008463
      0000846300008463000084630000846300008463000084630000846300008463
      0000846300008463000084630000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084424200844242008442
      4200844242008442420084424200844242000000000084630000C6840000C684
      0000C6840000C6840000C6840000C6840000C6840000C6840000C6840000C684
      0000C6840000C6840000C6840000846300000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084424200C6A5A500C6A5
      A500C6A5A500C6A5A500C6A5A500844242000000000084630000C6840000C684
      0000C6840000C6840000C6840000C6840000C6840000C6840000C6840000C684
      0000C6840000C6840000C6840000846300000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084424200C6A5A500C6A5
      A500C6A5A500C6A5A500C6A5A500844242000000000084630000C6840000C684
      0000C6840000C6840000C6840000C6840000C6840000C6840000C6840000C684
      0000C6840000C6840000C6840000846300000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000846363008463630084636300846363008463630084636300846363008463
      630084636300C6A5A500C6A5A500844242000000000084630000008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000084000000840000846300000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000084636300F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A50084636300C6A5A500C6A5A5008442420000000000C6A50000008400000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000008400000084000000840000846300000000000000000000000000008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000084636300F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A50084636300C6A5A500C6A5A500844242004242420042424200424242004242
      420042424200424242004242420042424200FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000846300000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00848484000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000084636300F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500846363008442420084424200844242004242420000C6E70042A5E7004284
      C60000C6E70042A5E70000C6E70042424200FFFF0000FFFF0000F7CEA500FFFF
      0000FFFF0000F7CEA500FFFF00008463000000000000FFFFFF00000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0084848400FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000084424200844242008442
      420084636300F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500846363000000000000000000000000004242420000C6E70042A5E7004242
      420000C6E70042A5E70000C6E70042424200FFFF0000FFFF0000FFFF000000FF
      FF0000FFFF00FFFF0000FFFF00008463000000000000FFFFFF00000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF008484840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000084424200C6A5A500C6A5
      A50084636300F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500846363000000000000000000000000004242420000C6E70042A5E7004242
      42000063840042A5E70000C6E70042424200FFFF0000FFFF0000FFFF000000FF
      FF0000FFFF00FFFF0000FFFF00008463000000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000084424200C6A5A500C6A5
      A50084636300F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500846363000000000000000000000000004242420000C6E70042A5E7004284
      C60000C6E70042A5E70000C6E70042424200FFFF0000FFFF0000F7CEA500FFFF
      0000FFFF0000F7CEA500FFFF00008463000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000084424200C6A5A500C6A5
      A50084636300F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500846363000000000000000000000000004242420000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0042424200C6A5000084630000846300008463
      00008463000084630000846300000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000084424200C6A5A500C6A5
      A500846363008463630084636300846363008463630084636300846363008463
      630084636300000000000000000000000000000000004242420000FFFF004242
      42000063840000FFFF0042424200000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084424200C6A5A500C6A5
      A500C6A5A500C6A5A500C6A5A500844242000000000000000000000000000000
      000000000000000000000000000000000000000000004242420000FFFF004242
      42000063840000FFFF0042424200000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084424200844242008442
      4200844242008442420084424200844242000000000000000000000000000000
      000000000000000000000000000000000000000000004242420000FFFF0000FF
      FF0000FFFF0000FFFF0042424200000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000424242004242
      4200424242004242420000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400000000000000000000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000C684210000000000000000000000000042424200424242004242
      4200000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000FFFF00000000000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400008484000000000000000000000000000000000000FFFF00000000000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400008484000000000000000000000000000000000000000000000000000000
      0000C6842100F7FFFF00C68421004242420084636300A5A5A500A5A5A500A5A5
      A500846363004242420000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00848484000000
      0000008484000084840000848400008484000084840000848400008484000084
      84000084840000848400000000000000000000000000FFFFFF00848484000000
      0000008484000084840000848400008484000084840000848400008484000084
      840000848400008484000000000000000000000000000000000000000000C684
      2100F7FFFF00F7FFFF00F7FFFF00C6842100A5A5A500A5A5A500A5A5A500A5A5
      A500A5A5A5008463630000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000FFFF00848484000000
      0000008484000084840000848400008484000084840000848400008484000084
      8400008484000084840000848400000000000000000000FFFF00848484000000
      0000008484000084840000848400008484000084840000848400008484000084
      8400008484000084840000848400000000000000000000000000C6842100F7FF
      FF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00C6842100A5A5A500A5A5A500A5A5
      A500A5A5A500A5A5A50042424200000000000000000000000000000000000000
      0000FFFFFF000000000000000000FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0084848400C6C6
      C600000000000000000000000000000000000084840000848400008484000084
      84000084840000848400008484000000000000000000FFFFFF0084848400C6C6
      C600000000000000000000000000000000000084840000848400008484000084
      84000084840000848400008484000000000000000000C6842100F7FFFF00F7FF
      FF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00C6842100A5A5A500A5A5
      A500A5A5A500A5A5A50042424200000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0084848400C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0084848400C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6842100F7FF
      FF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00C6842100A5A5A500A5A5A500A5A5
      A500A5A5A500A5A5A50042424200000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00848484000084
      00000084000000008400000084004284E7004284E700C6C6C600848484000000
      00000000000000000000000000000000000000000000FFFFFF00848484000084
      00000084000000008400000084004284E7004284E700C6C6C600848484000000
      000000000000000000000000000000000000000000000000000084630000C684
      2100F7FFFF00F7FFFF00F7FFFF00C6842100A5A5A500A5A5A500A5A5A500A5A5
      A500A5A5A5008463630000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000FFFF00848484000084
      00000084000000008400000084004284E7004284E700C6C6C600848484000000
      0000000000000000000000000000000000000000000000FFFF00848484000084
      00000084000000008400000084004284E7004284E700C6C6C600848484000000
      000000000000000000000000000000000000000000000000000084630000C6C6
      4200C6842100F7FFFF00C68421004242420084636300A5A5A500A5A5A500A5A5
      A500846363004242420000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      FF000000FF0000FFFF0000FFFF00FF000000FF000000C6C6C600000000000000
      0000000000000084000000000000000000000000000000000000848484000000
      FF000000FF0000FFFF0000FFFF00FF000000FF000000C6C6C600000000000000
      000000000000000000000000000000000000000000000000000084630000C6C6
      4200C6C64200C6842100C6C64200C6C64200C6C6420042424200424242004242
      4200000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      FF000000FF0000FFFF0000FFFF00FF000000FF000000C6C6C600000000000084
      0000008400000084000000840000848484000000000000000000848484000000
      FF000000FF0000FFFF0000FFFF00FF000000FF00000000000000C6840000C684
      0000C6840000C6840000C684000000000000000000000000000084630000C6C6
      4200C6C64200C6C64200C6C64200C6C64200C6C64200C6C64200846300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084848400C684
      0000C68400008400E7008400E70000FF000000FF0000C6C6C600000000000084
      000000840000008400000084000000000000000000000000000084848400C684
      0000C68400008400E7008400E70000FF000000FF000000000000C6840000C684
      0000C6840000C6840000C684000000000000000000000000000084630000C6C6
      4200C6C64200C6C64200C6C64200C6C64200C6C64200C6C64200846300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084848400C684
      0000C68400008400E7008400E70000FF000000FF0000C6C6C600000000000000
      000000000000008400000000000000000000000000000000000084848400C684
      0000C68400008400E7008400E70000FF000000FF0000C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000846300008463
      0000846300008463000084630000846300008463000084630000846300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400848484008484840084848400848484008484
      840084848400848484008484840000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0084848400000000000000000000000000FFFF
      FF0084848400C6C6C6008484840084848400C6C6C6008484840084848400FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000840000008400000084000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0084848400000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000840000008400000084000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000840000000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0084848400000000000000000000000000FFFF
      FF0084848400C6C6C600848484008484840084848400C6C6C600C6C6C600FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000840000008400000084000084848400848484008484
      8400000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000008400000084000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000848484000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00848484000000000000000000000000000000000000000000000000000000
      0000008400000084000000840000848484008484840084848400000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF008484840084848400C6C6C6008484840084848400C6C6C60084848400FFFF
      FF00000000000000000000000000000000000000000000000000000000000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000848484000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00848484000000000000000000000000000000000000840000008400000084
      0000008400000084000000840000008400000084000000840000848484000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000084
      0000008400000084000000840000008400000084000000840000008400000084
      0000848484000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00848484000000000000000000000000000000000000840000008400000084
      0000008400000084000000840000008400000084000000840000848484000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00C6C6C600C6C6C60084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000840000008400000084000084848400848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000840000008400000084
      0000008400000084000000840000008400000084000000840000848484000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000840000008400000084000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008400000084000000840000848484008484840084848400000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF0084848400C6C6C600C6C6C600FFFFFF00FFFFFF0000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000840000008400000084000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008400000084000000840000848484000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400848484008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008400000084000000840000848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      000000000000000000005A5A5A00000000007373730073737300000000007373
      7300636363000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      000000000000949494005A5A5A005A5A5A00639CCE00639CCE005A5A5A00A5A5
      A500428484000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000FFFF00000000000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000008484
      84008484840063CECE0031CEFF0031CEFF0031CEFF0031CEFF0031CEFF0031CE
      FF00000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000000000000000000000000000FFFFFF0000FFFF000000
      0000008484000084840000848400008484000084840000848400008484000084
      8400008484000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000D6D6
      D600636363006363630031CEFF004ACEFF00639C9C003163630031CEFF0031CE
      CE005A5A5A005A5A5A0000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00000000000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      840000848400008484000000000000000000000000000000000000000000D6D6
      D6009CCEFF0031CEFF0031CEFF009CFFFF0063CECE0000848400319CCE0031CE
      FF00639CCE000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000008484000084840000000000000000000000000000000000000000006363
      6300E7E7E700319C9C0031CEFF009CFFFF0063CECE000084840031CEFF00319C
      9C00313131000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000006363
      6300FFFFFF00DEDEDE00C6C6C6009CFFFF0063CECE00008484005A5A5A009CCE
      FF00000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000D6D6
      D600D6D6D600D6D6D600D6D6D600CECECE0084FFFF0063CECE005A5A5A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000006363
      6300E7E7E700DEDEDE00D6D6D600CECECE00CECECE00CECECE005A5A5A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000006363
      6300A5A5A50094949400D6D6D600CECECE00CECECE00CECECE005A5A5A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006363
      63006363630063636300D6D6D600CECECE00CECECE00CECECE005A5A5A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700DEDEDE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000B00000000100010000000000800500000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFE01FE01FC7FF00FF000F008
      F83FE00780008044F83FE00700001E04F83FE00700003304F83FE00700003304
      F83FE00700003324F83FE00700003324F83FE00700003F00F83FE00700013F01
      C007F3CF00090009C007F3CF9249F249E00FF3CF92019201F01FF18F90039003
      F83FF81F881F881FFC7FFFFFC0FFC0FFE007E001F801FC7FC007C001F801F83F
      CFE78001F801F01FCFE78001F801E00FCE6780018001C007CC6780018001C007
      CC6780018001F83FCC6780018001F83FC00780018001F83FC00F80038001F83F
      FC7F80038003F83FFC7F80038007F83FF44F8003801FF83FF01F8003801FF83F
      F83FC007803FFC7FFC7FE00F807FFFFFFCF3FC7FFFFFFFFFFB6DF83FFFFFFFFF
      8001F01FFC01803F8001F44FFC00003F8001FC7FCCFC3F338001FC7F9CFC3F39
      8001C00F001C38008001C007000C30008001CC67000C30008001CC679CFC3F39
      8001CC67CCFC3F338001CE67EC0000378001CFE7FE00007F8001CFE7FFFFFFFF
      8001C007FFFFFFFFFFFFE007FFFFFFFF87FFFFFFFFFFFFFF03FF9249C000FFFF
      01FFBFFDC000FFFF00FFFFFFC000FBFF800FBFFDC000FBFFC001BFFDC000F800
      E000FFFFC000F800F000BFFDC000C000F800BFFDC000F800F800FFFFC000F800
      F800BFFDC000FBFFF800BFFDC000FBFFF800FFFFC000FFFFF800BFFDC000FFFF
      FC019249C000FFFFFE03FFFFFFFFFFFF9FFFFFFFFFFFFFFF87FFFFFFFEC7FFFF
      81FFFFFF80018001807FE00780018001801FE007800180018007E00780018001
      8001E007800180018000E007800180018000E007800180018001E00780018001
      8007E00780018001801FE00780018001807FE0078001800181FFFFFF80018001
      87FFFFFF800180019FFFFFFFFFFFFFFFFE018000FBFFFFFFFC018000F1FF9007
      FC438000F0FF8007CC238000F07F800784038000F03F800786038000F01F8007
      C6138000F00F8007C0078000F0078001E00F8000F0078001E23F8000F00F8007
      E23F8000F01F801FF07F8000F03F807FF07F8000F07F81FFC07F8001F0FF87FF
      C0FF8003F1FF9FFFFFFF8007FBFFFFFFFFFFC001FF00FF00FF808000FF00FF00
      FF808000FF00FF00FF808000F000F000F0008000F000F000F0008000E000F000
      F000000000000000F000000000010001800700000003000380070000000F0007
      80070000001F001F80070001001F001F800781FF001F001F80FF81FF01FF01FF
      80FF81FF01FF01FFFFFFC3FF01FF01FFFFFFFFFFFFFFFF9F000F000FFFFFFF0F
      00070007FB8FFF0F00030003F003EE1F00010001E003E61F00000000C001E03F
      000000008001E03F00000000C001E007001F001FC003E00F001B000FC003E01F
      80018000C00FE03FC000C000C01FE07FC000C000C01FE0FFC001C000C01FE1FF
      C01BC00FFFFFE3FFFFFFFFFFFFFFE7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      C007FFFFFFFFFC01C007FC7FFFFFF800C007F83FFFFFF000C007F83FFFFFE000
      C007E00FE00FE001C007C007C007803FC007C007C007001FC007C007C007001F
      C007E00FE00F001FC007F83FFFFF803FC00FF83FFFFFE0FFC01FFC7FFFFFE0FF
      C03FFFFFFFFFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC001FFFF
      E003001F8031FD27E003000F8031F807E00300078031E00FE00300038001E003
      E00300018001E007E00300008001E007E003001F8FF1E00FE003001F8FF1E01F
      E003001F8FF1E01FE0038FF18FF1E01FE007FFF98FF1E01FE00FFF758FF5E01F
      E01FFF8F8001FFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object PopupMenu_u: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu_uPopup
    Left = 56
    Top = 392
    object N37: TMenuItem
      Action = ActObjEdit
      ShortCut = 13
    end
    object N38: TMenuItem
      Caption = '-'
    end
    object N39: TMenuItem
      Action = ActObjAdd
      ShortCut = 45
    end
    object N40: TMenuItem
      Action = ActObjDel
      ShortCut = 46
    end
    object N54: TMenuItem
      Caption = '-'
    end
    object N56: TMenuItem
      Action = ActSetVarparentToDef
    end
    object N55: TMenuItem
      Action = ActSetVarDefGroup
    end
    object N66: TMenuItem
      Caption = '-'
    end
    object NPMUMoveto: TMenuItem
      Caption = 'mMoveToTab'
    end
  end
  object PopupMenu_s: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu_sPopup
    Left = 240
    Top = 120
    object N18: TMenuItem
      Action = ActBackEdit
      ShortCut = 13
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object N31: TMenuItem
      Action = ActBackAdd
      ShortCut = 45
    end
    object N36: TMenuItem
      Action = ActBackDel
      ShortCut = 46
    end
  end
  object PopupMenu_img: TPopupMenu
    Left = 84
    Top = 198
    object N41: TMenuItem
      Action = ActMapObjEdit
    end
    object N42: TMenuItem
      Caption = '-'
    end
    object NPMI_NewShape: TMenuItem
      Action = ActNewCollideShape
    end
    object NPMI_NewNode: TMenuItem
      Action = ActNewCollideNode
    end
    object N74: TMenuItem
      Action = ActMapObjClone
    end
    object N43: TMenuItem
      Action = ActMapObjDel
    end
    object DbgSeparator: TMenuItem
      Caption = '-'
    end
    object DbgTrigActionsTrace: TMenuItem
      Caption = 'mDebugWatchMiniscriptFlags'
      OnClick = DbgTrigActionsTraceClick
    end
    object DbgStateTrace: TMenuItem
      Caption = 'mDebugWatchStates'
      OnClick = DbgStateTraceClick
    end
    object DbgStopOnStateChange: TMenuItem
      Caption = 'mDebugStopOnChangeState'
      OnClick = DbgStopOnStateChangeClick
    end
    object DbgResetDirList: TMenuItem
      Caption = 'mResetDebugWatchDirectors'
    end
    object DbgSeparator1: TMenuItem
      Caption = '-'
    end
    object NPopupStartScript: TMenuItem
      Caption = 'mStartMiniscript'
    end
  end
  object ImageListDir: TImageList
    ColorDepth = cd32Bit
    AllocBy = 9
    Height = 10
    Masked = False
    Width = 11
    Left = 492
    Top = 54
    Bitmap = {
      494C01010A00180009000B000A00FFFFFFFF2110FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000002C0000001E0000000100200000000000A014
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF00FF0000000000000000000000000080808000FFFFFF00808080000000
      00000000000000000000FF00FF00FF00FF000000000000000000000000008080
      8000FFFFFF0080808000000000000000000000000000FF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00C0C0C0000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00C0C0C000000000000000000000000000C0C0
      C000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000C0C0C000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF0000000000000000000000000000000000C0C0C00000000000FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF0080808000000000000000
      0000FFFFFF00FFFFFF0080808000FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00C0C0C000C0C0C000C0C0C000FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00C0C0C000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF00FF000000000000000000000000000000000000000000000000000000
      00000000000000000000FF00FF00FF00FF000000000000000000000000000000
      00000000000000000000000000000000000000000000FF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
      0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0000000000000000000000000080808000FFFFFF00808080000000
      00000000000000000000FF00FF00FF00FF000000000000000000000000008080
      8000FFFFFF0080808000000000000000000000000000FF00FF00FF00FF000000
      0000000000000000000080808000FFFFFF008080800000000000000000000000
      0000FF00FF00FF00FF0000000000000000000000000080808000FFFFFF008080
      8000000000000000000000000000FF00FF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF000000
      0000000000000000000000000000C0C0C000FFFFFF00FFFFFF00FFFFFF000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      000000000000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF000000000000000000FFFFFF00C0C0C0000000000000000000000000000000
      000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF0000000000C0C0C00000000000000000000000000000000000FFFFFF000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF0080808000FFFFFF00FFFFFF000000
      00000000000080808000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00C0C0C000C0C0C000FFFFFF00FFFFFF000000
      0000FF00FF000000000000000000000000000000000000000000000000000000
      00000000000000000000FF00FF00FF00FF000000000000000000000000000000
      00000000000000000000000000000000000000000000FF00FF00FF00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF00FF00FF00FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000
      0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0000000000000000000000000080808000FFFFFF00808080000000
      00000000000000000000FF00FF00FF00FF000000000000000000000000008080
      8000FFFFFF0080808000000000000000000000000000FF00FF00FF00FF000000
      0000000000000000000080808000FFFFFF008080800000000000000000000000
      0000FF00FF00FF00FF0000000000000000000000000080808000FFFFFF008080
      8000000000000000000000000000FF00FF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C000C0C0C000FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00C0C0C000C0C0
      C000C0C0C000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00C0C0C000C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0080808000FFFF
      FF00FFFFFF00000000000000000080808000FFFFFF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00808080000000000000000000FFFFFF00FFFF
      FF0080808000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF0000000000C0C0C0000000000000000000000000000000
      0000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF000000
      0000000000000000000000000000C0C0C00000000000FFFFFF00FFFFFF000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00C0C0C000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF00C0C0C000000000000000000000000000C0C0C000FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      000000000000C0C0C000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF0000000000000000000000000000000000C0C0C000FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00C0C0C00000000000000000000000000000000000FFFFFF000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF000000000000000000FFFFFF0000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FF00FF000000000000000000000000000000000000000000000000000000
      00000000000000000000FF00FF00FF00FF000000000000000000000000000000
      00000000000000000000000000000000000000000000FF00FF00FF00FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF00FF00FF00FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000FF00FF00424D3E000000000000003E000000
      280000002C0000001E0000000100010000000000F00000000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000}
  end
  object ImageListSpIcons: TImageList
    ColorDepth = cd32Bit
    AllocBy = 3
    Height = 24
    Masked = False
    Width = 24
    Left = 340
    Top = 54
    Bitmap = {
      494C010105001000030018001800FFFFFFFF2110FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000600000003000000001002000000000000048
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000A54
      9F000A549F00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000963C1000963C1004BB4
      FB007CCBFF001973CF000963C100FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000A549F004BB4FB007CCBFF0069C5
      FF00CDEEFF00CDEEFF004BB4FB000A549F000A549F00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0005417D003F99E80069C5FF0069C5FF004BB4FB004BB4
      FB00CDEEFF00CDEEFF00CDEEFF00ADE1FF003F99E8000A549F00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF000A549F002F8ADF007CCBFF0069C5FF0069C5FF004BB4FB004BB4FB004BB4
      FB00ADE1FF00ADE1FF00ADE1FF00ADE1FF00ADE1FF008ED4FF002F8ADF000963
      C100FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF000963
      C10069C5FF007CCBFF0069C5FF0069C5FF0050BCFF003AB4FF003AB4FF003AB4
      FF008ED4FF008ED4FF008ED4FF008ED4FF008ED4FF008ED4FF008ED4FF007CCB
      FF000963C100FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF002F8A
      DF007CCBFF007CCBFF0069C5FF0050BCFF004BB4FB003AB4FF0021ABFF0021AB
      FF007CCBFF0069C5FF0069C5FF0069C5FF007CCBFF0069C5FF007CCBFF0069C5
      FF002F8ADF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF002F8A
      DF007CCBFF0069C5FF0050BCFF004BB4FB003AB4FF0050BCFF003AB4FF0021AB
      FF0050BCFF0069C5FF0069C5FF0050BCFF0050BCFF004BB4FB0050BCFF0050BC
      FF002F8ADF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF002F8A
      DF0069C5FF0050BCFF0050BCFF0050BCFF0069C5FF0050BCFF0050BCFF0050BC
      FF0050BCFF0050BCFF004BB4FB0069C5FF004BB4FB003AB4FF0021ABFF003AB4
      FF002F8ADF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF002F8A
      DF0069C5FF0050BCFF0069C5FF0050BCFF003AB4FF003AB4FF004BB4FB0069C5
      FF0069C5FF004BB4FB004BB4FB004BB4FB0050BCFF0050BCFF0021ABFF0021AB
      FF002F8ADF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF002F8A
      DF0069C5FF0069C5FF003AB4FF004BB4FB0050BCFF0050BCFF0069C5FF004BB4
      FB00ADE1FF007CCBFF0050BCFF003AB4FF004BB4FB004BB4FB0050BCFF003AB4
      FF002F8ADF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF000A549F0050BC
      FF0050BCFF003AB4FF0050BCFF003AB4FF0069C5FF003F99E8000963C1001973
      CF008ED4FF007CCBFF007CCBFF0069C5FF0050BCFF003AB4FF004BB4FB004BB4
      FB0050BCFF000A549F00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF000963C10069C5
      FF003AB4FF004BB4FB0050BCFF004BB4FB001973CF000963C1000963C1001973
      CF008ED4FF007CCBFF0069C5FF0069C5FF007CCBFF0050BCFF0050BCFF003AB4
      FF0050BCFF000963C100FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF000A549F004BB4
      FB0050BCFF004BB4FB001973CF000A549F000963C1000963C1000A549F001973
      CF008ED4FF007CCBFF0069C5FF0050BCFF0050BCFF0050BCFF0069C5FF0050BC
      FF0050BCFF000A549F00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF003F99
      E8003F99E8000963C1000963C1000963C1000963C1000963C1000963C1001973
      CF008ED4FF007CCBFF0069C5FF0069C5FF0050BCFF003AB4FF0021ABFF0050BC
      FF003F99E800FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF000A54
      9F001973CF002F8ADF000963C1000963C1000963C1000A549F000963C1001973
      CF008ED4FF007CCBFF0069C5FF0050BCFF004BB4FB003AB4FF004BB4FB002F8A
      DF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF000A549F002F8ADF002F8ADF000963C1000963C1000963C1000963
      C1008ED4FF007CCBFF0069C5FF0069C5FF0050BCFF002F8ADF000A549F00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF000A549F001973CF002F8ADF000963C1001973
      CF008ED4FF007CCBFF0069C5FF002F8ADF000A549F00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000A549F002F8ADF003F99
      E8007CCBFF003F99E8000A549F00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000A54
      9F000A549F00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600C6C6
      C600C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600C6C6
      C600C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00D4D2D400D4D2
      D400D4D2D400D4D2D400D4D2D400FF00FF00FF00FF00FF00FF00D4D2D400D4D2
      D400D4D2D400D4D2D400D4D2D400D4D2D400FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00CCCECC00CCCECC00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00C6C6C600C6C6C600C6C6C600848484008484
      84008484840084848400C6C6C600C6C6C600C6C6C600FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00C6C6C600C6C6C600C6C6C600848484008484
      84008484840084848400C6C6C600C6C6C600C6C6C600FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00D4D2D400D4D2D400FC1A1400FC1A
      1400FC1A1400FC1A1400FC1A1400D4D2D400D4D2D400FF00FF00FC1A1400FC1A
      1400FC1A1400FC1A1400FC1A1400FC1A1400FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00C0C0C00004666400040204009C9A9C00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00C6C6C600C6C6C60084848400FF000000FF000000FF000000FF00
      0000848484000000FF000000FF000000FF0084848400C6C6C600C6C6C600FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00C6C6C600C6C6C60084848400F7CEA500F7CEA500F7CEA500F7CE
      A500F7CEA500F7CEA500F7CEA500F7CEA50084848400C6C6C600C6C6C600FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00D4D2D400FC1A1400FC1A1C00FC1A1400FC02
      0400FC020400FC020400FC1A1400FC1A1C00FC1A1400D4D2D400FC1A1C00FC02
      0400FC020400FC020400FC020400FC020400FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00C0C0C0000466640004020400CCCECC00CCCECC009C9A9C00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00C6C6C60084848400FF000000FF000000FF000000FF000000FF000000FF00
      0000848484000000FF000000FF000000FF000000FF000000FF0084848400C6C6
      C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00C6C6C60084848400F7CEA500F7CEA500F7CEA500F7CEA50000000000F7CE
      A500F7CEA50000000000F7CEA500F7CEA500F7CEA500F7CEA50084848400C6C6
      C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00D4D2D400FC1A1400FC1A1C00FC1A1400FC1A1C00FC1A
      1400FC1A1400FC1A1C00FC1A1400FC1A1C00F42E2C00FC1A1400FC1A1400FC02
      0400FC1A1400FC1A1400FC1A1400FC1A1400FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C0C0
      C000046664000466640004020400FCFEFC00FCFEFC00CCCECC00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6
      C60084848400FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000848484000000FF000000FF000000FF000000FF000000FF000000FF008484
      8400C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6
      C60084848400F7CEA500F7CEA500F7CEA500F7CEA500F7CEA50000000000C6C6
      C6000000000000000000F7CEA500F7CEA500F7CEA500F7CEA500F7CEA5008484
      8400C6C6C600FF00FF00FF00FF00FF00FF00DCDEDC00DCDEDC00DCDEDC00DCDE
      DC00DCDEDC00DCDADC00D4D2D400FC1A1400FC020400FC1A1400D4D2D400D4D2
      D400D4D2D400D4D2D400D4D2D400D4D2D400FC1A1400FC1A1C00FC1A1400FC02
      0400FC1A1400D4D2D400D4D2D400D4D2D400FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C0C0C0000466
      6400046664000402040074767400FCFEFC00FCFEFC00FCFEFC009C9A9C00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C6008484
      8400FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000848484000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0084848400C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00C6C6C6008484
      8400F7CEA5008484840084848400F7CEA500F7CEA500F7CEA500000000000000
      0000C6C6C60000000000F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A50084848400C6C6C600FF00FF00FF00FF008482040084820400848204008482
      04008482040094962C00C4C28400FC1A1400FC1A1C00FC1A1400D4D2D400D4D2
      D400B4B2640094962C0094962C00B4B26400B4B26400C4C28400FC1A1400FC02
      0400FC1A1400D4D2D400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C0C0C000046664000466
      6400CCCECC0004020400BCBEBC00FCFEFC00FCFEFC00FCFEFC00CCCECC00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000848484000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF00C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600F7CE
      A500F7CEA500FF000000FF0000008484840084848400F7CEA50000000000F7CE
      A500F7CEA50000000000F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500F7CEA500C6C6C600FF00FF00FF00FF008482040084820400848204008482
      04008482040094962C00B4B26400C4C28400FC1A1400F42E2C00FC1A1400FC1A
      1400B4B26400C4C28400C4C28400C4C28400C4C28400C4C28400FC1A1C00FC02
      0400FC1A1400D4D2D400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00C0C0C0000466640004666400BCBE
      BC007476740004020400CCCECC00FCFEFC00FCFEFC00FCFEFC008C8A8C00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C60084848400FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000848484000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF0084848400C6C6C600FF00FF00FF00FF00C6C6C60084848400F7CE
      A500F7CEA500C6C6C600FF000000FF000000FF00000084848400F7CEA500F7CE
      A500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500F7CEA50084848400C6C6C600FF00FF008482040084820400848204008482
      0400848204008482040094962C00B4B26400D4D2D400FC1A1C00FC1A1C00F42E
      2C00FC1A1C00FC1A1C00FC1A1C00FC1A1C00FC1A1C00FC1A1C00FC1A1400FC02
      0400FC1A1400D4D2D400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00C0C0C000C0C0C000C0C0C0000466640004FEFC00BCBEBC0004FE
      FC000402040074767400CCCECC00FCFEFC00FCFEFC00FCFEFC00FCFEFC009C9A
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000848484000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00C6C6C600FF00FF00FF00FF00C6C6C600F7CEA500F7CE
      A500F7CEA500F7CEA500FF000000FF000000FF000000FF00000084848400F7CE
      A500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500F7CEA500F7CEA500C6C6C600FF00FF00DCDEDC00D4D6D400848204008482
      040084820400D4D6D400DCDADC00D4D2D400D4D2D400D4D2D400D4D2D400FC1A
      1C00FC1A1C00FC1A1C00FC1A1C00FC1A1400FC1A1400FC1A1400FC1A1C00FC02
      0400FC1A1400D4D2D400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C0C0
      C000C0C0C00004666400046664000466640004FEFC00BCBEBC0004FEFC00BCBE
      BC00040204009C9A9C009C9A9C00CCCECC00FCFEFC00FCFEFC00FCFEFC008C8A
      8C00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C60084848400FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000848484000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0084848400C6C6C600C6C6C60084848400F7CEA500F7CE
      A500F7CEA500F7CEA500C6C6C600FF000000FF000000FF000000FF0000008484
      840084848400F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500F7CEA500F7CEA50084848400C6C6C600FF00FF00FF00FF00D4D6D4008482
      04008482040084820400DCDEDC00DCDEDC00D4D2D400D4D2D400D4D2D400D4D2
      D400D4D2D400D4D2D400C4C28400B4B26400C4C28400D4D2D400FC1A1400FC02
      0400FC1A1400D4D2D400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000466
      6400FCFEFC0004FEFC000466640004FEFC00BCBEBC0004FEFC00BCBEBC0004FE
      FC000402040074767400747674009C9A9C00FCFEFC00FCFEFC00FCFEFC008C8A
      8C00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C60084848400FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000848484000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0084848400C6C6C600C6C6C60084848400F7CEA500F7CE
      A500F7CEA500F7CEA500F7CEA500C6C6C600FF000000FF000000FF000000FF00
      0000FF00000084848400F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500F7CEA500F7CEA50084848400C6C6C600FF00FF00FF00FF00DCDEDC008482
      040084820400848204008482040094962C009C9E3C009C9E3C0094962C009C9E
      3C0094962C009C9E3C009C9E3C009C9E3C00B4B26400D4D2D400FC1A1400FC02
      0400FC1A1400D4D2D400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000466
      640004FEFC00BCBEBC0004666400BCBEBC0004FEFC00BCBEBC0004FEFC00BCBE
      BC00040204008C8A8C00747674008C8A8C00FCFEFC00FCFEFC00FCFEFC008C8A
      8C00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C60084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0084848400C6C6C600C6C6C60084848400F7CEA500F7CE
      A500F7CEA500F7CEA500F7CEA500F7CEA500C6C6C600FF000000FF000000FF00
      0000FF000000FF00000084848400F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500F7CEA500F7CEA50084848400C6C6C600FF00FF00FF00FF00FF00FF00D4D6
      D400848204008482040094962C00B4B26400C4C28400C4C28400C4C28400C4C2
      8400C4C28400C4C28400C4C28400C4C28400C4C28400FC1A1C00FC1A1400FC1A
      1C00FC1A1400D4D2D400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000466
      6400FCFEFC0004FEFC000466640004FEFC00BCBEBC0004FEFC00BCBEBC00BCBE
      BC00040204008C8A8C00CCCECC008C8A8C00FCFEFC00FCFEFC00FCFEFC008C8A
      8C00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600848484000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840084848400C6C6C600C6C6C60084848400F7CEA500F7CE
      A500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500FF000000FF000000FF00
      0000000000000000FF000000FF0084848400F7CEA500F7CEA500F7CEA500F7CE
      A500F7CEA500F7CEA50084848400C6C6C600FF00FF00FF00FF00FF00FF00DCDE
      DC00848204008482040094962C00C4C28400FC1A1C00FC1A1400FC1A1C00FC1A
      1400FC1A1C00FC1A1400FC1A1C00FC1A1400FC1A1C00FC1A1400FC020400FC1A
      1400D4D2D400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000466
      6400FCFEFC0004FEFC000466640004FEFC00FCFEFC00FCFEFC00FCFEFC00CCCE
      CC000402040074767400FCFEFC008C8A8C00FCFEFC00FCFEFC00FCFEFC008C8A
      8C00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600848484000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF008484
      8400FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF00000084848400C6C6C600C6C6C60084848400F7CEA500F7CE
      A500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500FF000000FF00
      00000000FF000000FF000000FF000000FF00F7CEA500F7CEA500F7CEA500F7CE
      A500F7CEA500F7CEA50084848400C6C6C600FF00FF00FF00FF00FF00FF00FF00
      FF00D4D6D400848204009C9E3C00C4C28400FC1A1400FC1A1C00FC1A1400FC02
      0400FC020400FC020400FC020400FC020400FC020400FC1A1C00FC1A1400D4D2
      D400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000466
      6400FCFEFC0004FEFC0004666400FCFEFC00FCFEFC00FCFEFC00FCFEFC00CCCE
      CC000402040074767400FCFEFC008C8A8C00FCFEFC00FCFEFC00FCFEFC008C8A
      8C00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600848484000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF008484
      8400FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF00000084848400C6C6C600C6C6C60084848400F7CEA500F7CE
      A500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500C6C6
      C6000000FF000000FF000000FF000000FF0084848400F7CEA500F7CEA500F7CE
      A500F7CEA500F7CEA50084848400C6C6C600FF00FF00FF00FF00FF00FF00FF00
      FF00DCDEDC008482040094962C00B4B26400D4D2D400D4D2D400FC1A1400FC1A
      1400FC1A1400FC1A1400FC1A1400FC1A1400FC1A1400FC1A1C00D4D2D400FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000466
      6400FCFEFC0004FEFC000466640004FEFC00FCFEFC00FCFEFC00FCFEFC00CCCE
      CC0004020400040204008C8A8C00CCCECC00FCFEFC00FCFEFC00FCFEFC008C8A
      8C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C6000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF008484
      8400FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000C6C6C600FF00FF00FF00FF00C6C6C600F7CEA500F7CE
      A500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500C6C6C6000000FF000000FF000000FF000000FF0084848400F7CEA500F7CE
      A500F7CEA500F7CEA500C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00DCDEDC008482040094962C0094962C00D4D2D400D4D2D400D4D2
      D400B4B26400C4C28400C4C28400D4D2D400D4D2D400FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C0C0
      C000C0C0C00004666400046664000466640004FEFC00FCFEFC00FCFEFC00CCCE
      CC000402040074767400CCCECC00FCFEFC00FCFEFC00FCFEFC00FCFEFC008C8A
      8C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600848484000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF008484
      8400FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF00000084848400C6C6C600FF00FF00FF00FF00C6C6C60084848400F7CE
      A500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A500F7CEA500C6C6C6000000FF000000FF000000FF000000FF0084848400F7CE
      A500F7CEA50084848400C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00DCDEDC00848204008482040084820400D4D6D400D4D2D400D4D6
      D4009C9E3C009C9E3C00D4D2D400FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00C0C0C000C0C0C000C0C0C0000466640004FEFC00FCFEFC00CCCE
      CC000402040074767400BCBEBC00FCFEFC00FCFEFC00FCFEFC00FCFEFC009C9A
      9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C6000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF008484
      8400FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600F7CE
      A500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500000000000000
      0000C6C6C600F7CEA500C6C6C6000000FF000000FF000000FF0084848400F7CE
      A500F7CEA500C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00D4D6D400848204008482040084820400DCDEDC008482
      04008482040084820400DCDEDC00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00C0C0C0000466640004FEFC00FCFE
      FC007476740004020400CCCECC00FCFEFC00FCFEFC00FCFEFC008C8A8C00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C6008484
      84000000FF000000FF000000FF000000FF000000FF000000FF000000FF008484
      8400FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      000084848400C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00C6C6C6008484
      8400F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CE
      A50000000000F7CEA500F7CEA500F7CEA500C6C6C6000000FF000000FF00F7CE
      A50084848400C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00DCDEDC00848204008482040084820400D4D6D4008482
      04008482040084820400DCDEDC00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BCBEBC000466640004FE
      FC007476740004020400BCBEBC00FCFEFC00FCFEFC00FCFEFC00CCCECC00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6
      C600848484000000FF000000FF000000FF000000FF000000FF000000FF008484
      8400FF000000FF000000FF000000FF000000FF000000FF000000FF0000008484
      8400C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6
      C60084848400F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500C6C6C6000000
      0000C6C6C600F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA5008484
      8400C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00E4E6
      E400DCDEDC00DCDEDC00DCDEDC00D4D6D4008482040084820400848204008482
      040084820400DCDEDC00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C0C0C0000466
      640004FEFC000402040074767400FCFEFC00FCFEFC00FCFEFC009C9A9C00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00C6C6C600848484000000FF000000FF000000FF000000FF000000FF008484
      8400FF000000FF000000FF000000FF000000FF000000FF00000084848400C6C6
      C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00C6C6C60084848400F7CEA500F7CEA500F7CEA500F7CEA50000000000F7CE
      A500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA500F7CEA50084848400C6C6
      C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00DCDE
      DC00848204008482040084820400848204008482040084820400848204008482
      040084820400D4D6D400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C0C0
      C000046664007476740004020400FCFEFC00FCFEFC00CCCECC00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00C6C6C600C6C6C600848484000000FF000000FF000000FF008484
      8400FF000000FF000000FF000000FF00000084848400C6C6C600C6C6C600FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00C6C6C600C6C6C60084848400F7CEA500F7CEA500C6C6C6000000
      000000000000F7CEA500F7CEA500F7CEA50084848400C6C6C600C6C6C600FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00DCDA
      DC00848204008482040084820400848204008482040084820400848204008482
      0400DCDEDC00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00BCBEBC000466640004020400CCCECC00CCCECC009C9A9C00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00C6C6C600C6C6C600C6C6C600848484008484
      8400FF00000084848400C6C6C600C6C6C600C6C6C600FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00C6C6C600C6C6C600C6C6C600848484008484
      8400F7CEA50084848400C6C6C600C6C6C600C6C6C600FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00DCDE
      DC00848204008482040084820400848204008482040084820400848204008482
      0400D4D6D400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00C0C0C00004666400040204009C9A9C00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600C6C6
      C600C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C6C6C600C6C6
      C600C6C6C600C6C6C600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00E4E6
      E400DCDADC00DCDEDC00D4D6D400DCDADC00D4D6D400DCDADC00D4D6D400DCDA
      DC00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00CCCECC00C0C0C000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00424D3E000000000000003E000000
      2800000060000000300000000100010000000000400200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000}
  end
  object ImageListTrig: TImageList
    ColorDepth = cd32Bit
    AllocBy = 2
    Height = 10
    Masked = False
    Width = 9
    Left = 420
    Top = 54
    Bitmap = {
      494C010102001000020009000A00FFFFFFFF2110FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000240000000A0000000100200000000000A005
      000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
      FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF00FF00FF00FF00FF00FF0000000000FF00000000000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00000000000000FF00
      FF00FF00FF00FF00FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00FF00FF0000000000FF00
      0000FF0000000000FF0000000000FF00FF00FF00FF00FF00FF00FF00FF000000
      0000FF000000FF0000000000FF0000000000FF00FF00FF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF00FF0000000000FF000000FF000000FF0000000000FF000000FF000000
      0000FF00FF00FF00FF0000000000FF000000FFFFFF00FFFFFF00FFFFFF000000
      FF0000000000FF00FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF0000000000FF000000FF000000FF000000000000000000FF000000FFFF
      FF00FF000000FF0000000000FF00FFFFFF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF00FF0000000000FF000000FF000000
      FF0000000000000000000000FF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFF
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF00FF000000FF000000FF000000FF00000000000000000000000000FF00FFFF
      FF000000FF00FF000000FF000000FF000000FF00000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF00FF000000FF000000FF000000FF00
      000000000000000000000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00C0C0
      C000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF00FF00000000000000FF000000
      FF00FF000000FF000000FF00000000000000FF00FF00FF00FF00000000000000
      FF000000FF00FF000000FF000000FF00000000000000FF00FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF00FF00FF00FF000000000000000000000000000000000000000000FF00
      FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
      0000FF00FF00FF00FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      28000000240000000A0000000100010000000000500000000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000}
  end
  object PopupMenuParentTabs: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu_uPopup
    Left = 56
    Top = 448
    object PMPTRenameTab: TMenuItem
      Caption = 'mRenameTab'
      ImageIndex = 4
      OnClick = PMPTRenameTabClick
    end
    object MenuItem5: TMenuItem
      Caption = '-'
    end
    object PMPTAddTab: TMenuItem
      Caption = 'mAddTab'
      ImageIndex = 5
      OnClick = PMPTAddTabClick
    end
  end
end
