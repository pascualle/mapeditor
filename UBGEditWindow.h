//---------------------------------------------------------------------------

#ifndef UBGEditWindowH
#define UBGEditWindowH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <ActnList.hpp>
#include <Menus.hpp>
#include <ExtCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtDlgs.hpp>
#include <System.Actions.hpp>
#include <list>
//---------------------------------------------------------------------------

class GMapBGObj;

class TFBGObjWindow : public TForm
{
__published:
	TDrawGrid *DrawGrid;
	TToolBar *ToolBar1;
	TStatusBar *StatusBar;
	TToolButton *ToolButton1;
	TToolButton *ToolButtonAutoCreate;
	TToolButton *TBGrid;
	TActionList *ActionList1;
	TAction *ActGrid;
	TToolButton *ToolButtonStayOnTop;
	TPanel *Panel1;
	TComboBox *CB_Zoom;
	TLabel *Label2;
	TPopupMenu *PopupMenu;
	TAction *ActDel;
	TAction *ActEdit;
	TMenuItem *N1;
	TMenuItem *N2;
	TAction *ActCopy;
	TAction *ActPaste;
	TMenuItem *N3;
	TMenuItem *N4;
	TMenuItem *N5;
	TAction *ActStayOnTop;
	TAction *ActAutoCreate;
	TToolButton *ToolButton2;
	TPanel *Panel2;
	TComboBox *CB_Layer;
	TToolButton *ToolButton3;
	TStaticText *StaticText;
	TToolButton *ToolButton4;
	TToolButton *ToolButton5;
	TAction *ActBGLayerVisibility;

	void __fastcall ActGridExecute(TObject *Sender);
	void __fastcall ActEditExecute(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall DrawGridDblClick(TObject *Sender);
	void __fastcall DrawGridMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall DrawGridDragDrop(TObject *Sender, TObject *Source, int X, int Y);
	void __fastcall DrawGridDragOver(TObject *Sender, TObject *Source, int X, int Y, TDragState State, bool &Accept);
	void __fastcall DrawGridStartDrag(TObject *Sender, TDragObject *&DragObject);
	void __fastcall ActCopyExecute(TObject *Sender);
	void __fastcall ActPasteExecute(TObject *Sender);
	void __fastcall CB_LayerChange(TObject *Sender);
	void __fastcall DrawGridKeyPress(TObject *Sender, char &Key);
	void __fastcall ActStayOnTopExecute(TObject *Sender);
	void __fastcall CB_ZoomChange(TObject *Sender);
	void __fastcall ActAutoCreateExecute(TObject *Sender);
	void __fastcall ActDelExecute(TObject *Sender);
	void __fastcall DrawGridDrawCell(TObject *Sender, System::LongInt ACol, System::LongInt ARow,
          TRect &Rect, TGridDrawState State);
	void __fastcall DrawGridSelectCell(TObject *Sender, System::LongInt ACol, System::LongInt ARow,
          bool &CanSelect);
	void __fastcall ActBGLayerVisibilityExecute(TObject *Sender);

public:

	struct SelItem
	{
		GMapBGObj *gobj;
		int Left;
		int Top;
		int ItemIndex;
	};

	__fastcall TFBGObjWindow(TComponent *Owner);
	void __fastcall Localize();

	void __fastcall SetGridSize(int iW, int iH);
	void __fastcall SetColor(TColor iColor);
	void __fastcall SetScale(float scale);
	void __fastcall ProcessChanges();
	int __fastcall GetMaximumCapacity();
	int __fastcall GetColCount();
	int __fastcall GetRowCount();
	GMapBGObj *__fastcall GetBGObj(int iCol, int iRow);
	int __fastcall GetSelectedIndex();
	void __fastcall FindBGObj(unsigned int iIdx);
	void __fastcall ArrangePositions();

private:

	void __fastcall GetSelectionList(std::list<SelItem>& lst);
	void __fastcall ClearSelectionList();
	void __fastcall Paste(int col, int row);
	void __fastcall AddEditDel(int mode);

	float mScale;
	int mColWidth;
	int mRowHeight;
	bool mDrawGrid;
    bool mOtherLayerVisible;
	GMapBGObj ***mppCells;
	std::list<SelItem> mSelList;
};

//---------------------------------------------------------------------------
extern PACKAGE TFBGObjWindow *FBGObjWindow;
//---------------------------------------------------------------------------
#endif
