//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "USpObjProperty.h"
#include "MainUnit.h"
#include "UScript.h"
#include "UObjManager.h"
#include "UState.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

#ifdef _WIN64
#pragma link "vclsmp.a"
#else
#pragma link "vclsmp.lib"
#endif

#pragma resource "*.dfm"

TFSpObjProperty *FSpObjProperty;

static const int OBJ_INITIATOR_IDX = 0;
static const int OBJ_SECOND_IDX = 1;
static const int OPPOSITESTATE_IDX = 0;
static const int BLANKACTION_IDX = 0;

//---------------------------------------------------------------------------
__fastcall TFSpObjProperty::TFSpObjProperty(TComponent* Owner)
 : TPrpFunctions(Owner)
{
	CSEd_x = new TSpinEdit(GroupBoxPos);
	CSEd_x->Parent = GroupBoxPos;
	CSEd_x->Left = 16;
	CSEd_x->Top = 20;
	CSEd_x->Width = 55;
	CSEd_x->Height = 22;
	CSEd_x->TabOrder = 0;
	CSEd_x->OnChange = CSEd_xChange;
	CSEd_x->OnEnter = CSEd_xEnter;
	CSEd_x->OnExit = CSEd_xExit;
    CSEd_x->Visible = true;
	CSEd_y = new TSpinEdit(GroupBoxPos);
	CSEd_y->Parent = GroupBoxPos;
	CSEd_y->Left = 84;
	CSEd_y->Top = 20;
	CSEd_y->Width = 53;
	CSEd_y->Height = 22;
	CSEd_y->TabOrder = 1;
	CSEd_y->OnChange = CSEd_xChange;
	CSEd_y->OnEnter = CSEd_xEnter;
	CSEd_y->OnExit = CSEd_xExit;
    CSEd_y->Visible = true;
	CE_TileNUm = new TSpinEdit(TabSheet1);
	CE_TileNUm->Parent = TabSheet1;
	CE_TileNUm->Left = 236;
	CE_TileNUm->Top = 105;
	CE_TileNUm->Width = 55;
	CE_TileNUm->Height = 22;
	CE_TileNUm->TabOrder = 5;
	CE_TileNUm->OnChange = CE_TileNUmChange;
	CE_TileNUm->OnExit = CE_TileNUmExit;
	CE_TileNUm->OnEnter = EmptyOnEnter;
    CE_TileNUm->Visible = true;
	CE_map_count = new TSpinEdit(TS_map);
	CE_map_count->Parent = TS_map;
	CE_map_count->Left = 207;
	CE_map_count->Top = 108;
	CE_map_count->Width = 109;
	CE_map_count->Height = 22;
	CE_map_count->MaxValue = 65535;
	CE_map_count->TabOrder = 4;
	CE_map_count->OnChange = CE_map_countChange;
	CE_map_count->OnExit = CE_map_countExit;
	CE_map_count->OnEnter = EmptyOnEnter;
    CE_map_count->Visible = true;
	CE_map_idx = new TSpinEdit(TS_map);
	CE_map_idx->Parent = TS_map;
	CE_map_idx->Left = 207;
	CE_map_idx->Top = 136;
	CE_map_idx->Width = 109;
	CE_map_idx->Height = 22;
	CE_map_idx->MaxValue = 32000;
	CE_map_idx->MinValue = -1;
	CE_map_idx->TabOrder = 5;
	CE_map_idx->Value = -1;
	CE_map_idx->OnChange = CE_map_idxChange;
	CE_map_idx->OnExit = CE_map_idxExit;
	CE_map_idx->OnEnter = EmptyOnEnter;
	CE_map_idx->Visible = true;

	t_MapGP = NULL;
	mpCommonResObjManager = NULL;
	RB_ChPosNoChange->Checked = true;
	RB_ChPosNoChangeClick(NULL);
	CheckBox_newStateClick(NULL);
	icon_show_list = false;
	inited = false;
	disableChanges = true;
	NameObj = _T("");
	mOldCSEd_x = _T("");
	mOldMemo = _T("");
	mOldOnVar = _T("");
	mpPropertiesControl = new TPropertiesControl(ScrollBox);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::Localize()
{
	Localization::Localize(this);
	Localization::Localize(PopupMenu);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::FormDestroy(TObject */*Sender*/)
{
	ClearScrollBox();
	ClearScrollBoxActions();
	delete mpPropertiesControl;
	delete CSEd_x;
	delete CSEd_y;
	delete CE_TileNUm;
	delete CE_map_count;
	delete CE_map_idx;
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::SaveAllValues(TWinControl *iParent)
{
  if(iParent == NULL)
	return;

  TControl *c;
  for(int i = 0; i < iParent->ControlCount; i++)
  {
	c = iParent->Controls[i];
	if(c == NULL)continue;

	if( c->ClassNameIs(_T("TPanel")) ||
		c->ClassNameIs(_T("TGroupBox")) ||
		c->ClassNameIs(_T("TTabSheet")) ||
		c->ClassNameIs(_T("TPageControl")) ||
		c->ClassNameIs(_T("TScrollBox")) ||
		c->ClassNameIs(_T("TForm")))
	{
		SaveAllValues((TWinControl*)c);
	}
	else
	{
		if(c->ClassNameIs(_T("TSpinEdit")))
		{
			if(((TSpinEdit*)c)->OnExit != NULL)
				((TSpinEdit*)c)->OnExit(c);
		}
		else
			if(c->ClassNameIs(_T("TEdit")))
		{
			if(((TEdit*)c)->OnExit != NULL)
				((TEdit*)c)->OnExit(c);
		}
		else
		if(c->ClassNameIs(_T("TMemo")))
		{
			if(((TMemo*)c)->OnExit != NULL)
				((TMemo*)c)->OnExit(c);
		}
		else
		if(c->ClassNameIs(_T("TComboBox")))
		{
			if(((TComboBox*)c)->OnExit != NULL)
				((TComboBox*)c)->OnExit(c);
		}
	}
  }

  if(t_MapGP != NULL && iParent == this)
  {
	Form_m->AddGlobalTriggerToAllLevels(false);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::FormHide(TObject */*Sender*/)
{
	if(ActiveControl != NULL && ActiveControl->ClassNameIs(_T("TRadioButton")))
	{
		ActiveControl = NULL;
	}
	mOldCSEd_x = _T("");
	mOldMemo = _T("");
	mOldOnVar = _T("");
	SaveAllValues(this);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::FormShow(TObject */*Sender*/)
{
   AssignSpMapGobj(t_MapGP);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::EmptyOnEnter(TObject *Sender)
{
	(void)Sender;
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::FakeEnterActiveControl()
{
	TWinControl* c = ActiveControl;
	if(c == NULL)
	{
		return;
	}
	if(c->ClassNameIs(_T("TRadioButton")))
	{
        ActiveControl = NULL;
		return;
	}
	if(c->ClassNameIs(_T("TSpinEdit")))
	{
		((TSpinEdit*)c)->OnEnter(c);
		return;
	}
	if(c->ClassNameIs(_T("TEdit")))
	{
		((TEdit*)c)->OnEnter(c);
		return;
	}
	if(c->ClassNameIs(_T("TMemo")))
	{
		((TMemo*)c)->OnEnter(c);
		return;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::Init(TCommonResObjManager *commonResObjManager)
{
	mpCommonResObjManager = commonResObjManager;
	assert(mpCommonResObjManager);
	vars = mpCommonResObjManager->GetVariablesMainGameObjPattern();

	ClearScrollBox();
	mpPropertiesControl->Init(vars, TPropertiesControl::PCM_CHECKERS);
	mpPropertiesControl->SetOnCheck(OnPropertiesControlOnCheck);
	mpPropertiesControl->SetOnValidate(OnPropertiesControlValueValidate);

	inited = true;

	if(t_MapGP != NULL)
	{
		AssignSpMapGobj(t_MapGP);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::refresStates()
{
  TState        *st;
  int idx = CB_states->ItemIndex;
  CB_states->Clear();
  CB_states->AddItem(Localization::Text(_T("mPrpObjOppositeToCurrentState")), (TObject*)(-SpecialObjConstants::trOBJ_CHANGE_PRP_OPPOSITESTATE));
  for(int x = 0; x < mpCommonResObjManager->states->Count; x++)
  {
	st = (TState*)mpCommonResObjManager->states->Items[x];
	CB_states->AddItem(st->name, (TObject*)st->direction);
  }
  if(idx < 0)CB_states->ItemIndex = 0;
  else       CB_states->ItemIndex = idx;
}
//---------------------------------------------------------------------------


//������������ � ������� ��������
void __fastcall TFSpObjProperty::AssignSpMapGobj(TMapSpGPerson *sgp)
{
  double x, y;
  int i;

  if(t_MapGP == sgp)return;

  if(t_MapGP != NULL)
  {
	 if(Visible)
	 {
		//FakeExitActiveControl();
		mOldCSEd_x = _T("");
		mOldMemo = _T("");
		mOldOnVar = _T("");
		SaveAllValues(this);
	 }
	 t_MapGP->UnregisterPrpWnd();
  }

  t_MapGP = sgp;
  if(t_MapGP != NULL)
	 t_MapGP->RegisterPrpWnd(this);
  else
	 return;

  Caption = Localization::Text(_T("mGameObjectProperties")) + _T(" [") + t_MapGP->getGParentName() + _T("]");

  NameObj = t_MapGP->Name;
  mOldName = t_MapGP->Name;
  disableChanges = true;

  sgp->getCoordinates(&x, &y);
  CSEd_x->Value = (int)x;
  CSEd_y->Value = (int)y;

  refresStates();
  refreshNodes();

  ComboBox_zone->Items->Assign(Form_m->ListBoxZone->Items);
  ComboBox_zone->Items->Add(Localization::Text(GLOBAL_OJB_NAME));
  if(sgp->GetZone() >= 0)
  {
	ComboBox_zone->ItemIndex = sgp->GetZone();
  }
  else
  {
	switch(sgp->GetZone())
	{
		case ZONES_INDEPENDENT_IDX: ComboBox_zone->ItemIndex = ComboBox_zone->Items->Count - 2;
		break;
		case LEVELS_INDEPENDENT_IDX: ComboBox_zone->ItemIndex = ComboBox_zone->Items->Count - 1;
	}
  }
  ChangeZone(sgp->GetZone());
  ComboBox_zone->Hint = ComboBox_zone->Text;
  Edit_name->Text = t_MapGP->Name;
  Memo->Text = t_MapGP->getRemarks();                   

  switch(sgp->Type)
  {
    case trMAP:
		PageControl->ActivePage = TS_map;
    break;
    case trPROPERTIES:
		PageControl->ActivePage = TS_main;
  }

  if(PageControl->ActivePage == TS_main)  //change state
  {
	CheckBox_newState->Checked = false;
	ListBox->Clear();
	if(CB_states->Items->Count <= 0)
	{
	  CheckBox_newState->Enabled = false;
	}
	else
	{
		int j;
		CheckBox_newState->Enabled = true;
		CheckBox_newState->Checked = sgp->stateList->Count > 0;
		for(i=0; i<sgp->stateList->Count; i++)
		{
		  if(sgp->stateList->Strings[i].Compare(SpecialObjConstants::VarStateName) == 0)
		  {
			j = 0;
		  }
		  else
		  {
			j = CB_states->Items->IndexOf(sgp->stateList->Strings[i]);
		  }
		  if(j >= 0)
		  {
			ListBox->AddItem(CB_states->Items->Strings[j], CB_states->Items->Objects[j]);
		  }
		}
		CB_states->ItemIndex = 0;
	}

	//����� mapIndex ������������ ��� ��� �������� ������ �������
	CB_ParamTransmitOpt->ItemIndex = sgp->mapIndex;
	if(CB_ParamTransmitOpt->ItemIndex < 0)
	{
		CB_ParamTransmitOpt->ItemIndex = 0;
    }

	//change position
	RB_ChPosNoChange->Checked = true;
	if(CB_objPos->Items->Count <= 0)
	{
	 RB_ChPosObjCoord->Enabled = false;
	}
	else
	{
     RB_ChPosObjCoord->Enabled = true;
	 if(!sgp->objTeleport.IsEmpty()) i = CB_objPos->Items->IndexOf(sgp->objTeleport);
	 else                            i = -1;

	 if(i > 0)
	 {
          RB_ChPosObjCoord->Checked = true;
		  CB_objPos->ItemIndex = i;
     }
	 else CB_objPos->ItemIndex = 0;
    }

	if(sgp->tileTeleport >= 0)
    {
     RB_ChPosTileCoord->Checked = true;
     CE_TileNUm->Value = sgp->tileTeleport;
    }
	else CE_TileNUm->Value = 0;

	if( sgp->objName.Compare( UnicodeString(SpecialObjConstants::VarObjNameInitiator) )== 0)
	  i = OBJ_INITIATOR_IDX;
	else
	if( sgp->objName.Compare( UnicodeString(SpecialObjConstants::VarObjNameSecond) )== 0)
	  i = OBJ_SECOND_IDX;
	else
	  i = CB_obj->Items->IndexOf(sgp->objName);

    if(i < 0)i = 0;
	CB_obj->ItemIndex = i;
  }
  else
  if(PageControl->ActivePage == TS_map)
  {
	switch(sgp->mapDirection)
	{
		case trUP:    RBUp->Checked = true;
		break;
		case trDOWN:  RBDown->Checked = true;
		break;
		case trLEFT:  RBLeft->Checked = true;
		break;
		case trRIGHT: RBRight->Checked = true;
	}
	CE_map_idx->Value = sgp->mapIndex;
	CE_map_count->Value = sgp->mapCount;
  }

  RefreshProperties();
  refreshActions();

  CheckBox_newStateClick(NULL);
  RB_ChPosNoChangeClick(NULL);
  CB_statesEnter(NULL);

	Lock(t_MapGP->IsLocked());

  disableChanges = false;

  if(Visible)
  {
  	FakeEnterActiveControl();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::refreshNodes()
{
	int len, y;
	len = Form_m->m_pObjManager->GetObjTypeCount(TObjManager::DIRECTOR);
	CB_objPos->Items->Clear();
	for(y = 0; y < len; y++)
	{
		CB_objPos->Items->Add(((TMapDirector*)Form_m->m_pObjManager->GetObjByIndex(TObjManager::DIRECTOR, y))->Name);
	}
	CB_objPos->Items->Insert(0, TEXT_STR_NONE);
	if(t_MapGP != NULL)
	{
		int i = CB_objPos->Items->IndexOf(t_MapGP->objTeleport);
        if(i < 0)i = 0;
		CB_objPos->ItemIndex = i;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::ComboBox_zoneChange(TObject */*Sender*/)
{
 if(t_MapGP==NULL)return;

 ComboBox_zone->Hint = ComboBox_zone->Text;

 if(!disableChanges)
 {
   ComboBox_zoneExit(ComboBox_zone);
   Form_m->OnChangeObjZone(t_MapGP);
 }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::ComboBox_zoneExit(TObject */*Sender*/)
{
 if(!disableChanges && t_MapGP != NULL)
 {
   int idx = ComboBox_zone->ItemIndex;
   if(idx == ComboBox_zone->Items->Count - 2)
   {
		idx = ZONES_INDEPENDENT_IDX;
		TS_map->TabVisible = true;
   }
   else
   if(idx == ComboBox_zone->Items->Count - 1)
   {
		if(PageControl->ActivePage == TS_map)
		{
			if(t_MapGP->GetZone() >= 0)
			{
				ComboBox_zone->ItemIndex = t_MapGP->GetZone();
			}
			else
			{
				switch(t_MapGP->GetZone())
				{
					case ZONES_INDEPENDENT_IDX: ComboBox_zone->ItemIndex = ComboBox_zone->Items->Count - 2;
					break;
					case LEVELS_INDEPENDENT_IDX: ComboBox_zone->ItemIndex = ComboBox_zone->Items->Count - 1;
				}
			}
			Application->MessageBox(Localization::Text(_T("mTileModeViolationError")).c_str(),
									Localization::Text(_T("mError")).c_str(),
									MB_OK | MB_ICONERROR);
			return;
		}
		idx = LEVELS_INDEPENDENT_IDX;
		TS_map->TabVisible = false;
   }
   else
   {
		TS_map->TabVisible = true;
   }

   t_MapGP->SetZone(idx);

   ChangeZone(idx);
   refreshActions();
   Form_m->OnChangeObjZone(t_MapGP);
 }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::ChangeZone(int zone)
{
 int i;
 TMapGPerson  *gp;
 TMapDirector *dir;
 UnicodeString oldObjValue = CB_obj->Text;
 UnicodeString oldPosValue = CB_objPos->Text;

 if(zone < 0)
 {
	switch(zone)
	{
		case ZONES_INDEPENDENT_IDX: ComboBox_zone->ItemIndex = ComboBox_zone->Items->Count - 2;
		break;
		case LEVELS_INDEPENDENT_IDX: ComboBox_zone->ItemIndex = ComboBox_zone->Items->Count - 1;
	}
 }
 CB_obj->Clear();
 if(zone != LEVELS_INDEPENDENT_IDX)
 {
	for(i = 0; i < Form_m->m_pObjManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT); i++)
	{
		gp=(TMapGPerson*)Form_m->m_pObjManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, i);
		if(gp->GetDecorType() != decorNONE)continue;
		if(gp->GetZone() != zone && gp->GetZone() >= 0)continue;
		CB_obj->Items->Add(gp->Name);
	}
 }
 CB_obj->Items->Insert(OBJ_INITIATOR_IDX, Localization::Text(_T("mRecipientInitiatorCapitalized")));
 CB_obj->Items->Insert(OBJ_SECOND_IDX, Localization::Text(_T("mRecipientOtherCapitalized")));
 CB_obj->ItemIndex = 0;
 if(CB_obj->Items->Count > 0)
 {
   int pos;
   if((pos = CB_obj->Items->IndexOf(oldObjValue)) >= 0)
   {
		CB_obj->ItemIndex = pos;
   }
   else
   {
		CB_obj->ItemIndex = 0;
   }
 }

 CB_objPos->Clear();
 if(zone != LEVELS_INDEPENDENT_IDX)
 {
	refreshNodes();
	RB_ChPosObjCoord->Enabled = true;
 }
 else
 {
	if(RB_ChPosObjCoord->Checked)
	{
		RB_ChPosNoChange->Checked = true;
		RB_ChPosNoChangeClick(RB_ChPosNoChange);
	}
	RB_ChPosObjCoord->Enabled = false;
 }

 if(RB_ChPosTileCoord->Checked)
 {
	RB_ChPosNoChangeClick(RB_ChPosTileCoord);
 }
 else
 if(RB_ChPosObjCoord->Checked)
 {
	bool res = false;
	if(CB_objPos->Items->Count > 0)
	{
		int pos;
		if((pos = CB_objPos->Items->IndexOf(oldPosValue)) > 0)
		{
			CB_objPos->ItemIndex = pos;
			res = true;
		}
	}
	if(!res)
	{
		RB_ChPosNoChange->Checked = true;
		RB_ChPosNoChangeClick(RB_ChPosNoChange);
    }
 }
}
//---------------------------------------------------------------------------

//����������
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::ClearScrollBox()
{
	mpPropertiesControl->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::ClearScrollBoxActions()
{
	for(int i=0; i<ScrollBoxActions->ControlCount; i++)
	{
		delete (TControl*)ScrollBoxActions->Controls[i];
		i--;
	}
	ScrollBoxActions->VertScrollBar->Position = 0;
	ScrollBoxActions->HorzScrollBar->Position = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::RefreshProperties()
{
	if(!inited || t_MapGP==NULL)
	{
		return;
	}
	std::list<PropertyItem>::const_iterator iv;
	for (iv = vars->begin(); iv != vars->end(); ++iv)
	{
		double val = 0.0f;
		if (!t_MapGP->GetVarValue(&val, iv->name))
			if (!t_MapGP->GetDefValue(&val, iv->name))
				val = 0.0f;
        bool checked = false;
		if(!t_MapGP->GetCheckedValue(&checked, iv->name))
		{
			checked = false;
		}
		mpPropertiesControl->SetValue(val, iv->name);
		mpPropertiesControl->SetChecked(checked, iv->name);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::refreshActions()
{
 /*
 int            i,h,idx,type,j,count;
 TControl       *c;
 TComboBox      *ed;
 TCheckBox      *lb;
 UnicodeString     str;
 TGScript       *spt;
 bool           blankChecked;

 if(t_MapGP == NULL)return;

 ScrollBoxActions->Visible = false;
 ScrollBoxActions->Enabled = false;
 ClearScrollBoxActions();

 h=2;
 for(i = 0; i < ::Actions::sptCOUNT; i++)
 {
  bool first_time = true;
  ed = new TComboBox(ScrollBoxActions);
  ed->Parent = ScrollBoxActions;
  ed->PopupMenu = PopupMenu;
  ed->Width = 180;
  str = t_MapGP->getScriptName(UnicodeString(::Actions::Names[i]));
  ed->Name = _T("_CB") + IntToStr(i);
  ed->Text = _T("");
  ed->OnChange = OnScriptChange;
  switch(i)
  {
   case ::Actions::sptONCOLLIDE:
		type=::Actions::sptONCOLLIDE;
   break;
   case ::Actions::sptONTILECOLLIDE:
		type=::Actions::sptONTILECOLLIDE;
   break;
   case ::Actions::sptONCHANGENODE:
		type=::Actions::sptONCHANGENODE;
   break;
   case ::Actions::sptONCHANGE1:
   case ::Actions::sptONCHANGE2:
   case ::Actions::sptONCHANGE3:
		type=::Actions::sptONCHANGE1;
   break;
   case ::Actions::sptONENDANIM:
		type=::Actions::sptONENDANIM;
  }
  for(j=0; j<Form_m->m_pObjManager->GetScriptsCount(); j++)
  {
	spt = Form_m->m_pObjManager->GetScriptByIndex(j);

	if(type == ::Actions::sptONTILECOLLIDE)continue;

	if(first_time)
	{
		ed->AddItem(Localization::Text(_T("mEmptyValue")), reinterpret_cast<TObject*>(type));
		first_time = false;
	}

	if( spt->Zone == t_MapGP->GetZone() ||
		(t_MapGP->GetZone() != LEVELS_INDEPENDENT_IDX && spt->Zone == ZONES_INDEPENDENT_IDX) ||
		spt->Zone == LEVELS_INDEPENDENT_IDX )
	{
		if(spt->Mode == type || spt->Mode == ::Actions::sptONENDANIM)
		{
			ed->AddItem(spt->Name, reinterpret_cast<TObject*>(type));
		}
	}
  }
  count = ed->Items->Count;
  ed->Enabled = (count > 0);
  if(ed->Enabled)
  {
   if(str.Compare(UnicodeString(SpecialObjConstants::VarBlankActionName))==0)
   {
     blankChecked = true;
     idx = 0;
   }
   else
   {
     idx = ed->Items->IndexOf(str);
     blankChecked = false;
   }
   if(idx > 0)
     ed->ItemIndex = idx;
   else
     ed->ItemIndex = 0;
  }
  else idx = -1;
  ed->Top = h;
  ed->Left = 165;
  lb=new TCheckBox(ScrollBoxActions);
  lb->Parent = ScrollBoxActions;
  lb->WordWrap = false;
  lb->Width = 155;
  lb->Caption = ::Actions::Caption[i];
  lb->Top = h+(ed->Height-lb->Height)/2;
  lb->Left = 8;
  lb->Tag = NativeInt(ed);
  ed->Tag = NativeInt(lb);
  lb->Checked = (idx > 0) || (blankChecked && idx == 0);
  lb->Enabled = ed->Enabled;
  lb->OnClick = CheckBoxActionsClick;
  CheckBoxActionsClick(lb);
  h+=ed->Height+2;
 }
  ScrollBoxActions->Enabled = true;
  ScrollBoxActions->Visible = true;
  */
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::ScrollBoxResize(TObject */*Sender*/)
{
 ScrollBox->Update();
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::OnPropertiesControlOnCheck(const UnicodeString name, bool val)
{
	t_MapGP->SetCheckedValue(val, name);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::OnPropertiesControlValueValidate(const UnicodeString name, double val)
{
	if (!disableChanges && t_MapGP != NULL)
	{
		t_MapGP->SetVarValue(val, name);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CheckBoxActionsClick(TObject *Sender)
{
 TCheckBox *lb;
 TComboBox *ed;
 lb=(TCheckBox*)Sender;
 ed=(TComboBox*)lb->Tag;
 if(lb->Checked)
 {
   ed->Enabled = true;
   ed->Color = clWindow;
   OnScriptChange(ed);
 }
 else
 {
   ed->Enabled = false;
   ed->Color = clBtnFace;
   OnScriptChange(ed);
 }
}
//---------------------------------------------------------------------------
void __fastcall TFSpObjProperty::OnScriptChange(TObject *Sender)
{
  TCheckBox *lb;
  TComboBox *ed;
  UnicodeString str;
  if(t_MapGP != NULL && !disableChanges)
  {
	ed = (TComboBox*)Sender;
	lb = (TCheckBox*)ed->Tag;
	int i = 0;
	for(; i < ::Actions::sptCOUNT; i++)
	{
		if(lb->Caption.Compare(::Actions::Caption[i]) == 0)
		{
			break;
        }
	}
	if(lb->Checked)
	{
	  if(ed->ItemIndex == BLANKACTION_IDX)
		str = UnicodeString(SpecialObjConstants::VarBlankActionName);
	  else
		str = ed->Text;
	}
	else str = _T("");
	t_MapGP->setScriptName(UnicodeString(::Actions::Names[i]), str);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::ScrollBoxActionsResize(TObject */*Sender*/)
{
  if(ScrollBoxActions->Visible)
	ScrollBoxActions->Update();
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CB_statesEnter(TObject */*Sender*/)
{
 Image1->Top=CB_states->Top+(CB_states->Height-Image1->Height)/2;
 CB_statesChange(NULL);
 icon_show_list = false;
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::ListBoxExit(TObject */*Sender*/)
{
  CB_statesEnter(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::ListBoxClick(TObject */*Sender*/)
{
 int obj;
 Graphics::TBitmap *bmp;
 TRect r;
 if(ListBox->ItemIndex<0)return;
 r=ListBox->ItemRect(ListBox->ItemIndex);
 if(CB_states->ItemIndex >= 0)
 {
   bmp = new Graphics::TBitmap;
   obj = (int)ListBox->Items->Objects[ListBox->ItemIndex];
   if(obj == -SpecialObjConstants::trOBJ_CHANGE_PRP_OPPOSITESTATE)obj = 0;
   Form_m->ImageListDir->GetBitmap(obj, bmp);
   Image1->Picture->Bitmap->Assign(bmp);
   delete bmp;
   Image1->Top=ListBox->Top+r.Top+(r.Height()-Image1->Height)/2+3;
   icon_show_list = true;
 }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::EditScript(int mode)
{
  /*
  int i, idx, pos, j;
  UnicodeString oldName;
  UnicodeString KeyName = _T("");

  for(j=0; j<ScrollBoxActions->ControlCount; j++)
  {
	TControl *c=(TControl*)ScrollBoxActions->Controls[j];
	if(c->ClassNameIs(_T("TComboBox")))
	{
		TComboBox *cb = (TComboBox *)c;
		if(cb->Focused())
		{
		  KeyName = ((TCheckBox*)cb->Tag)->Caption;
		  oldName =  cb->Text;
		  break;
		}
	}
  }

  if(KeyName.IsEmpty())return;
  if(KeyName[1] == L'-')return;
  if(Form_m->ActStartEmul->Checked)return;

  if(Form_m->m_pObjManager->GetScriptsCount() >= MAX_SCRIPTS && mode == mAdd)
  {
	Application->MessageBox(Localization::Text(_T("mMaximumScriptsExceeded")).c_str(),
							Localization::Text(_T("mError")).c_str(),
							MB_OK | MB_ICONERROR);
	return;
  }

  for(i=0; i < ::Actions::sptCOUNT; i++)
  {
	if(::Actions::Caption[i].Compare(KeyName) == 0)
	{
		TGScript *spt = new TGScript();
		if(mode == mEdit)
		{
		  idx = Form_m->m_pObjManager->GetScriptID(oldName);
		  if(idx < 0)
		  {
			delete spt;
			return;
		  }
		  Form_m->m_pObjManager->GetScriptByIndex(idx)->CopyTo(spt);
		}
		else
		{
			idx = -1;
			spt->Zone = t_MapGP->GetZone();
			spt->Mode = (char)::Actions::GetScriptType(i);
		}
		Application->CreateForm(__classid(TFScript), &FScript);
		FScript->ActiveOnlyZone(spt->Zone < 0 ? t_MapGP->GetZone() : spt->Zone);
		FScript->ActiveOnlyMode(spt->Mode == ::Actions::sptONENDANIM ? i : spt->Mode);
		FScript->SetScript(spt);
		bool res;
		do
		{
		  res=true;
		  if(FScript->ShowModal() == mrOk)
		  {
		   FScript->GetScript(spt);
		   if(spt->Name.IsEmpty())
		   {
			Application->MessageBox(Localization::Text(_T("mScriptNameError")).c_str(),
									Localization::Text(_T("mError")).c_str(),
									MB_OK | MB_ICONERROR);
			 res=false;
		   }
		   else
		   {
			  for(pos=0; pos<Form_m->m_pObjManager->GetScriptsCount(); pos++)
			  {
			   if((Form_m->m_pObjManager->GetScriptByIndex(pos))->Name.Compare( spt->Name ) == 0 &&
					 pos != idx)
			   {
				Application->MessageBox(Localization::Text(_T("mScriptNameExist")).c_str(),
										Localization::Text(_T("mError")).c_str(),
										MB_OK | MB_ICONERROR);
				  res=false;
				  break;
			   }
			  }
		   }
		   if(res)
		   {
			  if(mode == mAdd)
			  {
				 Form_m->m_pObjManager->CreateScript(spt);
				 for(j = 0; j < ScrollBoxActions->ControlCount; j++)
				 {
					TControl *c=(TControl*)ScrollBoxActions->Controls[j];
					if(c->ClassNameIs(_T("TComboBox")))
					{
						TComboBox *cb = (TComboBox *)c;
						cb->Items->Add(spt->Name);
						if(cb->Focused())
						   cb->Text = spt->Name;
					}
				 }
				 Form_m->AddGlobalScriptsToAllLevels(false);
			  }
			  else
			  if(mode == mEdit)
			  {
				  spt->CopyTo((Form_m->m_pObjManager->GetScriptByIndex(idx)));
				  Form_m->m_pObjManager->ChangeScriptNameInAllObjects(oldName, spt->Name, spt->Zone, spt->Mode);
				  for(j=0; j<ScrollBoxActions->ControlCount; j++)
				  {
					TControl *c=(TControl*)ScrollBoxActions->Controls[j];
					if(c->ClassNameIs("TComboBox"))
					{
						TComboBox *cb = (TComboBox *)c;
						bool sameText = cb->Text.Compare(oldName) == 0;
						for(int k = 0; k < cb->Items->Count; k++)
						  if(cb->Items->Strings[k].Compare(oldName) == 0)
							 cb->Items->Strings[k] = spt->Name;
						if(sameText)
						  cb->Text = spt->Name;
						else
						  if(cb->Focused())
                             cb->Text = spt->Name;
					}
				  }
				  Form_m->AddGlobalScriptsToAllLevels(false);
			  }
		   }
		 }
		 else res = true;
		}while(!res);
		FScript->Free();
		FScript = NULL;
		if(spt!=NULL)
		{
		   delete spt;
        }
		break;
	}
  }
  */
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::N1Click(TObject */*Sender*/)
{
   EditScript(mAdd);
}
//---------------------------------------------------------------------------
void __fastcall TFSpObjProperty::N2Click(TObject */*Sender*/)
{
   EditScript(mEdit);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::Edit_nameKeyPress(TObject */*Sender*/, char &Key)
{
  if(VK_RETURN == Key)
  {
     Key = 0;
	 Edit_nameExit(NULL);
  }	
}
//---------------------------------------------------------------------------
void __fastcall TFSpObjProperty::Edit_nameChange(TObject */*Sender*/)
{
   Edit_nameChangeMain(Edit_name);
}
//---------------------------------------------------------------------------
void __fastcall TFSpObjProperty::Edit_nameEnter(TObject */*Sender*/)
{
   mOldName = Edit_name->Text;	
}
//---------------------------------------------------------------------------
void __fastcall TFSpObjProperty::Edit_nameExit(TObject */*Sender*/)
{
  if(t_MapGP == NULL)
  {
	return;
  }

	UnicodeString str = Edit_name->Text.Trim();
	if(str.Compare(mOldName) == 0)
	{
        return;
    }

  if(str.IsEmpty())
  {
	Application->MessageBox(Localization::Text(_T("mObjNameError")).c_str(),
							Localization::Text(_T("mError")).c_str(),
							MB_OK | MB_ICONERROR);
	Edit_name->Text = mOldName;
	return;
  }
  if(Form_m->m_pObjManager->CheckIfMGONameExists(str, t_MapGP) == true)
  {
	Application->MessageBox(Localization::Text(_T("mObjNameExist")).c_str(),
							Localization::Text(_T("mError")).c_str(),
							MB_OK | MB_ICONERROR);
	Edit_name->Text = mOldName;
	return;
  }
  
  if(!disableChanges)
	if(mOldName.Compare(str) != 0)
	{
	  t_MapGP->Name = str;
	  NameObj = str;
	  Form_m->OnChangeTriggerName(t_MapGP, mOldName);
	  t_MapGP->changeHint();
	  mOldName = str;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::Bt_find_selfClick(TObject */*Sender*/)
{
  Form_m->FindAndShowObjectByName(NameObj, false);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CSEd_xChange(TObject *Sender)
{
  if(((TSpinEdit *)Sender)->Text.IsEmpty())return;
  try
  {
	StrToInt(((TSpinEdit *)Sender)->Text);
  }
  catch(...)
  {
	return;
  }

  if(!disableChanges)
	if(t_MapGP != NULL)
	  Form_m->OnChangeObjPosition(t_MapGP, CSEd_x->Value, CSEd_y->Value);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CSEd_xEnter(TObject *Sender)
{
  mOldCSEd_x = ((TSpinEdit *)Sender)->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CSEd_xExit(TObject *Sender)
{
  UnicodeString str = ((TSpinEdit *)Sender)->Text.Trim();

  if(mOldCSEd_x.IsEmpty())
	return;

  if(mOldCSEd_x.Compare(str) != 0)
	if(t_MapGP != NULL && !disableChanges)
	  Form_m->OnChangeObjPosition(t_MapGP, CSEd_x->Value, CSEd_y->Value);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CB_objPosExit(TObject *Sender)
{
  TComboBox *cb = (TComboBox *)Sender;
  if(cb->Items->IndexOf(cb->Text.Trim()) < 0)
  {
	cb->Text = _T("");
  }

  if(t_MapGP != NULL && !disableChanges && RB_ChPosObjCoord->Checked)
  {
	if(CB_objPos->ItemIndex > 0)
	{
		t_MapGP->objTeleport = CB_objPos->Text;
		t_MapGP->tileTeleport = -1;
	}
	else
	{
		RB_ChPosNoChange->Checked = true;
		RB_ChPosNoChangeClick(RB_ChPosNoChange);
	}

	Form_m->OnChangeObjParameter();
  }
}
//---------------------------------------------------------------------------
void __fastcall TFSpObjProperty::RB_ChPosNoChangeClick(TObject */*Sender*/)
{
  if(RB_ChPosNoChange->Checked)
  {
     CB_objPos->Enabled = false;
     CB_objPos->Color = clBtnFace;
     CE_TileNUm->Enabled = false;
     CE_TileNUm->Color = clBtnFace;
	 Bt_find_node->Enabled = false;
	 if(t_MapGP != NULL && !disableChanges)
	 {
		t_MapGP->objTeleport = _T("");
		t_MapGP->tileTeleport = -1;
		Form_m->OnChangeObjParameter();
	 }
  }
  else
  if(RB_ChPosObjCoord->Checked)
  {
     CB_objPos->Enabled = true;
     CB_objPos->Color = clWindow;
     CE_TileNUm->Enabled = false;
     CE_TileNUm->Color = clBtnFace;
	 Bt_find_node->Enabled = true;
	 if(t_MapGP != NULL && !disableChanges)
	 {
		if(CB_objPos->ItemIndex > 0)
		{
			t_MapGP->objTeleport = CB_objPos->Text;
		}
		else
		{
			t_MapGP->objTeleport = _T("");
		}
		t_MapGP->tileTeleport = -1;
		Form_m->OnChangeObjParameter();
	 }
  }
  else
  if(RB_ChPosTileCoord->Checked)
  {
	 CB_objPos->Enabled = false;
	 CB_objPos->Color = clBtnFace;
	 CE_TileNUm->Enabled = true;
	 CE_TileNUm->Color = clWindow;
	 Bt_find_node->Enabled = false;
	 if(t_MapGP != NULL && !disableChanges)
	 {
		t_MapGP->objTeleport = _T("");
		t_MapGP->tileTeleport = CE_TileNUm->Value;
		Form_m->OnChangeObjParameter();
	 }
  }
}

//---------------------------------------------------------------------------
void __fastcall TFSpObjProperty::Bt_find_nodeClick(TObject */*Sender*/)
{
  if(CB_objPos->ItemIndex <= 0)
  {
	return;
  }
  Form_m->FindAndShowObjectByName(CB_objPos->Text, false);
}
//---------------------------------------------------------------------------
void __fastcall TFSpObjProperty::CE_TileNUmChange(TObject */*Sender*/)
{
  if(t_MapGP != NULL && !disableChanges)
  {
	if(RB_ChPosTileCoord->Checked)
	{
		t_MapGP->tileTeleport = CE_TileNUm->Value;
		Form_m->OnChangeObjParameter();
	}
	else t_MapGP->tileTeleport = -1;
  }
}
//---------------------------------------------------------------------------
void __fastcall TFSpObjProperty::CE_TileNUmExit(TObject */*Sender*/)
{
  if(t_MapGP != NULL && !disableChanges)
  {
	if(RB_ChPosTileCoord->Checked)
		t_MapGP->tileTeleport = CE_TileNUm->Value;
	else
		t_MapGP->tileTeleport = -1;
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CB_objExit(TObject *Sender)
{
  TComboBox *cb = (TComboBox *)Sender;
  if(cb != NULL && cb->Items->IndexOf(cb->Text.Trim()) < 0)
  {
  	cb->ItemIndex = 0;
  }

  if(t_MapGP != NULL && !disableChanges)
  {
	if(CB_obj->ItemIndex >= 0)
	{
	  if( CB_obj->ItemIndex == OBJ_INITIATOR_IDX )
		 t_MapGP->objName = UnicodeString(SpecialObjConstants::VarObjNameInitiator);
	  else
	  if( CB_obj->ItemIndex == OBJ_SECOND_IDX )
		 t_MapGP->objName = UnicodeString(SpecialObjConstants::VarObjNameSecond);
	  else
		 t_MapGP->objName = CB_obj->Text;
	}
	else t_MapGP->objName = _T("");

	Form_m->OnChangeObjParameter();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CB_objKeyPress(TObject *Sender, char &Key)
{
	if(Key == VK_RETURN)
	{
		TComboBox *cb = (TComboBox *)Sender;
		cb->OnExit(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::Bt_find_objClick(TObject */*Sender*/)
{
  int idx = CB_obj->ItemIndex;
  if(idx < 0)return;
  Form_m->FindAndShowObjectByName(CB_obj->Text, false);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CB_ParamTransmitOptChange(TObject */*Sender*/)
{
  if(t_MapGP != NULL  && !disableChanges && PageControl->ActivePage == TS_main)
  {
	 t_MapGP->mapIndex = CB_ParamTransmitOpt->ItemIndex;
	 Form_m->OnChangeObjParameter();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CB_ParamTransmitOptExit(TObject */*Sender*/)
{
  if(t_MapGP != NULL  && !disableChanges && PageControl->ActivePage == TS_main)
	 t_MapGP->mapIndex = CB_ParamTransmitOpt->ItemIndex;
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CheckBox_newStateClick(TObject */*Sender*/)
{
 if(CheckBox_newState->Checked)
 {
     CB_states->Enabled = true;
     CB_states->Color = clWindow;
     ListBox->Enabled = true;
     ListBox->Color = clWindow;
     Bt_st_add->Enabled = true;
	 Bt_st_del->Enabled = true;

	 if(t_MapGP != NULL && !disableChanges)
	 {
	   t_MapGP->stateList->Clear();
	   {
		 int obj, i;
		 for(i=0; i<ListBox->Count; i++)
		 {
		   obj = (int)ListBox->Items->Objects[i];
		   if(obj == -SpecialObjConstants::trOBJ_CHANGE_PRP_OPPOSITESTATE)
				t_MapGP->stateList->Add(UnicodeString(SpecialObjConstants::VarStateName));
		   else t_MapGP->stateList->Add(ListBox->Items->Strings[i]);
		 }
	   }
	   Form_m->OnChangeObjParameter();
	 }
 }
 else
 {
	 CB_states->Enabled = false;
	 CB_states->Color = clBtnFace;
	 ListBox->Enabled = false;
	 ListBox->Color = clBtnFace;
	 Bt_st_add->Enabled = false;
	 Bt_st_del->Enabled = false;
	 if(t_MapGP != NULL && !disableChanges)
	 {
	   t_MapGP->stateList->Clear();
	   Form_m->OnChangeObjParameter();
	 }
 }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CB_statesChange(TObject */*Sender*/)
{
 Graphics::TBitmap *bmp;
 if(CB_states->ItemIndex >= 0)
 {
   bmp = new Graphics::TBitmap;
   Form_m->ImageListDir->GetBitmap((int)CB_states->Items->Objects[CB_states->ItemIndex],bmp);
   Image1->Picture->Bitmap->Assign(bmp);
   delete bmp;
 }
 else
 {
   Image1->Picture->Bitmap->Assign(NULL);
 }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::Image1Click(TObject */*Sender*/)
{
  if(icon_show_list)
  {
	int idx = ListBox->ItemIndex;
	if(idx >= 0)
	  ShowStatePreview(ListBox->Items->Strings[idx]);
  }
  else
	if(!CB_states->Text.IsEmpty())ShowStatePreview(CB_states->Text);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::ShowStatePreview(UnicodeString name)
{
 int    i;
 TState *st;
 bool   res;
 res = false;
 for(i = 0; i < mpCommonResObjManager->states->Count; i++)
 {
  st =(TState *)mpCommonResObjManager->states->Items[i];
  if(st->name == name)
  {
    res = true;
    break;
  }
 }
 if(res)
 {
	Application->CreateForm(__classid(TFEditState), &FEditState);
	FEditState->SetState(st);
	FEditState->GroupBoxMovement->Enabled = false;
	FEditState->GroupBoxAnimation->Enabled = false;
	FEditState->Ed_name->Enabled = false;
	FEditState->Button->Enabled = false;
	FEditState->Button->Enabled = false;
	FEditState->ShowModal();
	FEditState->Free();
	FEditState = NULL;
 }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::Bt_st_addClick(TObject */*Sender*/)
{
 if(CB_states->ItemIndex >= 0)
 {
  if(ListBox->Items->IndexOf(CB_states->Text) < 0)
	 ListBox->AddItem(CB_states->Text, CB_states->Items->Objects[CB_states->ItemIndex]);
  CheckBox_newStateClick(NULL);
 }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::Bt_st_delClick(TObject */*Sender*/)
{
 int idx;
 idx = ListBox->ItemIndex;
 if(idx >= 0)
 {
   ListBox->DeleteSelected();
   ListBox->ItemIndex=idx-((idx>0)?1:0);
   CheckBox_newStateClick(NULL);
 }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::MemoExit(TObject */*Sender*/)
{
  if(!disableChanges)
  if(t_MapGP != NULL)
  {
	 UnicodeString str = Memo->Text.Trim();
	 if(mOldMemo.Compare(str) != 0)
	 {
	   t_MapGP->setRemarks(str);
	   Form_m->OnChangeObjParameter();
	 }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::MemoEnter(TObject */*Sender*/)
{
   mOldMemo = Memo->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::RBLeftClick(TObject */*Sender*/)
{
  if(t_MapGP != NULL && !disableChanges)
  {
	if(RBUp->Checked)t_MapGP->mapDirection = trUP;
	else
	if(RBDown->Checked)t_MapGP->mapDirection = trDOWN;
	else
	if(RBLeft->Checked)t_MapGP->mapDirection = trLEFT;
	else
	if(RBRight->Checked)t_MapGP->mapDirection = trRIGHT;
	Form_m->OnChangeObjParameter();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CE_map_countChange(TObject */*Sender*/)
{
  if(t_MapGP != NULL  && !disableChanges)
  {
	t_MapGP->mapCount = CE_map_count->Value;
	Form_m->OnChangeObjParameter();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CE_map_countExit(TObject */*Sender*/)
{
  if(t_MapGP != NULL  && !disableChanges)
	t_MapGP->mapCount = CE_map_count->Value;
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CE_map_idxChange(TObject */*Sender*/)
{
  if(t_MapGP != NULL && !disableChanges && PageControl->ActivePage == TS_map)
  {
	t_MapGP->mapIndex = CE_map_idx->Value;
	Form_m->OnChangeObjParameter();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::CE_map_idxExit(TObject */*Sender*/)
{
  if(t_MapGP != NULL  && !disableChanges && PageControl->ActivePage == TS_map)
	t_MapGP->mapIndex = CE_map_idx->Value;
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::PageControlChange(TObject */*Sender*/)
{
  if(t_MapGP != NULL && !disableChanges)
  {
	if(PageControl->ActivePage == TS_map)
	{
		t_MapGP->Clear();
		t_MapGP->Type = trMAP;
		//set map parameters
		RBLeftClick(NULL);
		CE_map_countChange(NULL);
		CE_map_idxChange(NULL);
	}
	else
	if(PageControl->ActivePage == TS_main)
	{
		t_MapGP->Clear();
		t_MapGP->Type = trPROPERTIES;
		//set obj parameters
		CB_objExit(NULL);
		CB_ParamTransmitOptChange(NULL);
		CheckBox_newStateClick(NULL);
		RB_ChPosNoChangeClick(NULL);
		//change properties
		std::list<PropertyItem>::const_iterator iv;
		for (iv = vars->begin(); iv != vars->end(); ++iv)
		{
			for(int j = 0; j < ScrollBox->ControlCount; j++)
			{
			  TControl* c = (TControl*)ScrollBox->Controls[j];
			  if(c->ClassNameIs(_T("TEdit")))
			  {
				TEdit *ed = (TEdit *)c;
				if(iv->name.Compare(ed->Hint) == 0)
				{
					double fval;
					try
					{
						fval = (double)StrToFloat(ed->Text);
					}
					catch(...)
					{
						fval = 0.0f;
					}
					TCheckBox* lb = (TCheckBox*)ed->Tag;
					t_MapGP->SetVarValue(fval, iv->name);
					t_MapGP->SetCheckedValue(lb->Checked, iv->name);
					break;
				}
			  }
			}
		  }
		  //change onActions events
		  for(int j=0; j<ScrollBoxActions->ControlCount; j++)
		  {
			TControl* c = (TControl*)ScrollBoxActions->Controls[j];
			if(c->ClassNameIs(_T("TCheckBox")))
			{
			   int i = 0;
			   UnicodeString str;
			   TCheckBox *cb = (TCheckBox *)c;
			   for(; i< ::Actions::sptCOUNT; i++)
			   {
				 if(cb->Caption.Compare(::Actions::Caption[i]) == 0)break;
			   }
			   if(!cb->Checked)str = _T("");
			   else
			   {
				 if(((TComboBox*)cb->Tag)->ItemIndex == BLANKACTION_IDX)
				   str = UnicodeString(SpecialObjConstants::VarBlankActionName);
				 else
				   str = ((TComboBox*)cb->Tag)->Text;
			   }
			   t_MapGP->setScriptName(UnicodeString(::Actions::Names[i]), str);
			}
		}
	}
  }
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::SetTransform(const TTransformProperties& transform)
{
	if(!Visible)
	{
		return;
	}
	double x, y;
	transform.GetPosition(x, y);
	CSEd_x->Value = static_cast<int>(x);
	CSEd_y->Value = static_cast<int>(y);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::refreshScripts()
{
   refreshActions();
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::Reset()
{
	AssignSpMapGobj(nullptr);
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::EmulatorMode(bool mode)
{
	(void)mode;
}
//---------------------------------------------------------------------------

void __fastcall TFSpObjProperty::Lock(bool value)
{
	ImageLock->Visible = value;
}
//---------------------------------------------------------------------------


