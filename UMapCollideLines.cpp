//---------------------------------------------------------------------------

#pragma hdrstop

#include "platform.h"
#include "system.h"
#include <math.h>
#include "assert.h"
#include "UMapCollideLines.h"
#include "MainUnit.h"
#include <cmath>
#include <algorithm>
#include <cfloat>

//---------------------------------------------------------------------------

const __int32 TMapCollideLineNode::gscCollideLineNodeDefSize = 8;

//---------------------------------------------------------------------------

__fastcall TMapCollideLineNode::TMapCollideLineNode(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString ParentName, TObjManager* Owner)
: TMapGPerson(x, y, pos_x, pos_y, ParentName, Owner)
{
	mPrevPosExist = false;
	mObjId = objMAPCOLLIDENODE;
	ShowRect(false);
	mWidth = gscCollideLineNodeDefSize;
	mHeight = gscCollideLineNodeDefSize;
	mZoneIndex = ZONES_INDEPENDENT_IDX;
	mFixedSizes = true;
	mErrorMark = Now();
	mpShapeParent = nullptr;
}
//---------------------------------------------------------------------------

__fastcall TMapCollideLineNode::TMapCollideLineNode(const TMapCollideLineNode& c)
: TMapGPerson(nullptr)
{
	mObjId = objMAPCOLLIDENODE;
	c.CopyTo(this);
}
//---------------------------------------------------------------------------

__fastcall TMapCollideLineNode::~TMapCollideLineNode()
{
}
//---------------------------------------------------------------------------

const TCollideChainShape* __fastcall TMapCollideLineNode::GetParent() const
{
	assert(mpShapeParent);
	return mpShapeParent;
}
//---------------------------------------------------------------------------

void __fastcall TMapCollideLineNode::CopyTo(TMapCollideLineNode *c) const
{
	TMapPerson::CopyTo(c);
	c->mPrevTransform = mPrevTransform;
	c->mPrevPosExist = mPrevPosExist;
	c->mErrorMark = mErrorMark;
	c->mpShapeParent = mpShapeParent;
}
//---------------------------------------------------------------------------

void __fastcall TMapCollideLineNode::ApplyCoordinates()
{
	assert(IsErrorMark() == false);
	mPrevTransform = mTransform;
	mPrevPosExist = true;
}
//---------------------------------------------------------------------------

bool __fastcall TMapCollideLineNode::GetPrevCoordinates(double& x, double& y)
{
	mPrevTransform.GetPosition(x, y);
	return mPrevPosExist;
}
//---------------------------------------------------------------------------

void __fastcall TMapCollideLineNode::inner_realign()
{
	double ox, oy;
	mOffPosX = Width >> 1;
	mOffPosY = Height >> 1;
	mTransform.GetPosition(ox, oy);
	mScreenLeft = ox * gsGlobalScale - static_cast<double>(mOffPosX) - static_cast<double>(*off_x);
	mScreenTop = oy * gsGlobalScale - static_cast<double>(mOffPosY) - static_cast<double>(*off_y);
}
//---------------------------------------------------------------------------

void __fastcall TMapCollideLineNode::setCoordinates(double x, double y, __int32 dx, __int32 dy)
{
	DoSetCoordinates(x, y, dx, dy);
}
//---------------------------------------------------------------------------

void __fastcall TMapCollideLineNode::OnScrollBarChange()
{
	inner_realign();
}
//---------------------------------------------------------------------------

void __fastcall TMapCollideLineNode::OnGlobalScale()
{
	inner_realign();
}
//---------------------------------------------------------------------------

void __fastcall TMapCollideLineNode::SetErrorMark()
{
	mErrorMark = gCurrentTimeMark;
}
//---------------------------------------------------------------------------

bool __fastcall TMapCollideLineNode::IsErrorMark() const
{
	return mErrorMark == gCurrentTimeMark;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

__fastcall TCollideChainShape::TCollideChainShape()
: TMapPerson(nullptr)
{
	mObjId = objMAPCOLLIDESHAPE;
	mWidth = 0;
	mHeight = 0;
	mFixedSizes = true;
}
//---------------------------------------------------------------------------

__fastcall TCollideChainShape::TCollideChainShape(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString GPName, TObjManager* Owner)
: TMapPerson(nullptr)
{
	(void)x;
	(void)y;
	(void)pos_x;
	(void)pos_y;
	(void)GPName;
	(void)Owner;
	assert(0);
}
//---------------------------------------------------------------------------

__fastcall TCollideChainShape::TCollideChainShape(TObjManager* Owner)
: TMapPerson(nullptr)
{
	(void)Owner;
	assert(0);
}
//---------------------------------------------------------------------------

__fastcall TCollideChainShape::~TCollideChainShape()
{
	mNodes.clear();
}
//---------------------------------------------------------------------------

__fastcall TCollideChainShape::TCollideChainShape(const TCollideChainShape& c)
: TMapPerson(nullptr)
{
	c.CopyTo(this);
}
//---------------------------------------------------------------------------

void __fastcall TCollideChainShape::CopyTo(TCollideChainShape *c) const
{
	assert(c);
	TMapPerson::CopyTo(c);
	c->mNodes = mNodes;
}
//---------------------------------------------------------------------------

TMapCollideLineNode& __fastcall TCollideChainShape::CreateNode(double x, double y, __int32 *pos_x, __int32 *pos_y)
{
	mNodes.push_back(TMapCollideLineNode(x, y, pos_x, pos_y, Name, nullptr));
	TMapCollideLineNode* n1 = &mNodes.back();
	n1->Parent_Name = Name;
	n1->mpShapeParent = this;
	return *n1;
}
//---------------------------------------------------------------------------

bool __fastcall TCollideChainShape::DeleteNode(const TMapCollideLineNode* n)
{
	for(TCollideNodeList::iterator it = mNodes.begin(); it != mNodes.end(); ++it)
	{
		const TMapCollideLineNode* cn = &(*it);
		if(n == cn)
		{
			mNodes.erase(it);
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------

__int32 __fastcall TCollideChainShape::GetNodesCount() const
{
	return mNodes.size();
}
//---------------------------------------------------------------------------

const TCollideChainShape::TCollideNodeList& __fastcall TCollideChainShape::GetNodeList() const
{
	return mNodes;
}
//---------------------------------------------------------------------------

void __fastcall TCollideChainShape::CopyNodeList(TList* list) const
{
	assert(list);
	for(TCollideNodeList::const_iterator it = mNodes.begin(); it != mNodes.end(); ++it)
	{
		const TMapCollideLineNode* cn = &(*it);
		list->Add((void*)cn);
	}
}
//---------------------------------------------------------------------------

void __fastcall TCollideChainShape::SetName(const UnicodeString& name)
{
	Name = name;
	for(TCollideNodeList::iterator it = mNodes.begin(); it != mNodes.end(); ++it)
	{
		TMapCollideLineNode& n = *it;
		n.Parent_Name = Name;
	}
}
//---------------------------------------------------------------------------

void __fastcall TCollideChainShape::OnScrollBarChange()
{
	for(TCollideNodeList::iterator it = mNodes.begin(); it != mNodes.end(); ++it)
	{
		TMapCollideLineNode& cn = *it;
		cn.OnScrollBarChange();
	}
}
//---------------------------------------------------------------------------

void __fastcall TCollideChainShape::OnGlobalScale()
{
	for(TCollideNodeList::iterator it = mNodes.begin(); it != mNodes.end(); ++it)
	{
		TMapCollideLineNode& cn = *it;
		cn.OnGlobalScale();
	}
}
//---------------------------------------------------------------------------

void __fastcall TCollideChainShape::TrimNodes(const TRect& r)
{
	TCollideNodeList tnodes;
	bool change = false;
	for(TCollideNodeList::iterator it = mNodes.begin(); it != mNodes.end(); ++it)
	{
		double x, y;
		TMapCollideLineNode& cn = *it;
		cn.getCoordinates(&x, &y);
		if ((__int32)x > r.Width() || (__int32)y > r.Height() ||
					(__int32)x < r.Left || (__int32)y < r.Top)
		{
			change = true;
		}
		else
		{
			tnodes.push_back(cn);
		}
	}
	if(change)
	{
		mNodes = tnodes;
	}
}
//---------------------------------------------------------------------------

bool __fastcall TCollideChainShape::IsFirstNode(const TMapCollideLineNode* n) const
{
	if(mNodes.empty() == false)
	{
		return &mNodes.front() == n;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TCollideChainShape::IsLastNode(const TMapCollideLineNode* n) const
{
	if(mNodes.empty() == false)
	{
		return &mNodes.back() == n;
	}
	return false;
}
//---------------------------------------------------------------------------

TMapCollideLineNode* __fastcall TCollideChainShape::FindNode(const UnicodeString& nodeName)
{
	for(TCollideNodeList::iterator it = mNodes.begin(); it != mNodes.end(); ++it)
	{
		TMapCollideLineNode& cn = *it;
		if(cn.Name.Compare(nodeName) == 0)
		{
			return &cn;
		}
	}
	return nullptr;
}
//---------------------------------------------------------------------------

bool __fastcall TCollideChainShape::MoveNode(const UnicodeString& nodeName, const UnicodeString& toNodeName)
{
	TCollideNodeList::iterator n = mNodes.end();
	TCollideNodeList::iterator nt = mNodes.end();
	__int32 i = 0;
	for(TCollideNodeList::iterator it = mNodes.begin(); it != mNodes.end() && i != 2; ++it)
	{
		const TMapCollideLineNode& cn = *it;
		if(cn.Name.Compare(nodeName) == 0)
		{
			n = it;
			i++;
		}
		else
		if(cn.Name.Compare(toNodeName) == 0)
		{
			nt = it;
			i++;
		}
	}
	if(n != mNodes.end() && nt != mNodes.end())
	{
		mNodes.splice(nt, mNodes, n);
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TCollideChainShape::GetPreMidNextNodes(const TMapCollideLineNode* orig_n,
														const TMapCollideLineNode** pre_n,
														const TMapCollideLineNode** mid_n,
														const TMapCollideLineNode** nex_n) const
{
	if(orig_n->GetParent() == this)
	{
		TCollideNodeList::const_iterator in = mNodes.begin();
		TCollideNodeList::const_iterator ine = mNodes.end();
		while (in != ine)
		{
			if (orig_n == &(*in))
			{
				*mid_n = &(*in);
				*pre_n = nullptr;
				*nex_n = nullptr;
				if(in != mNodes.begin())
				{
					--in;
					*pre_n = &(*in);
					++in;
				}
				if(++in != ine)
				{
					*nex_n = &(*in);
				}
				return true;
			}
			++in;
		}
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TCollideChainShape::MarkIfCrossline(const TMapCollideLineNode* pre_n,
								const TMapCollideLineNode* mid_n,
								const TMapCollideLineNode* nex_n)
{
	if(mid_n == nullptr || (pre_n == nullptr && nex_n == nullptr))
	{
		return false;
	}
	if(Crossline(mid_n, pre_n, nex_n))
	{
		const_cast<TMapCollideLineNode*>(mid_n)->SetErrorMark();
		if(pre_n)
		{
			const_cast<TMapCollideLineNode*>(pre_n)->SetErrorMark();
		}
		if(nex_n)
		{
			const_cast<TMapCollideLineNode*>(nex_n)->SetErrorMark();
		}
	}
	return false;
}
//---------------------------------------------------------------------------

static const double eps = 1e-8;

class _tpoint
{
public:
	double x, y;
	_tpoint(const double& _x, const double& _y)
    {
        x = _x;
        y = _y;
    }
};

class _tline
{
public:
	double a, b, c;
	_tline(const double& _a, const double& _b, const double& _c)
	{
		a = _a;
		b = _b;
		c = _c;
	}
};

static _tline _toline(const _tpoint& p1, const _tpoint& p2)
{
	const double a = p2.y - p1.y;
	const double b = p1.x - p2.x;
	const double c = - a * p1.x - b * p1.y;
	return _tline(a, b, c);
}

static __int32 _point_in_line (const _tline& l, const _tpoint& p)
{
	const double s = l.a * p.x + l.b * p.y + l.c;
	return s < - eps ? - 1 : s > eps ? 1 : 0;
}

static bool _point_in_box (const _tpoint& t, const _tpoint& p1, const _tpoint& p2)
{
	return  (std::fabs(t.x - std::min(p1.x, p2.x)) <= eps || std::min(p1.x, p2.x) <= t.x) &&
			(std::fabs(std::max(p1.x, p2.x) - t.x) <= eps || std::max(p1.x, p2.x) >= t.x) &&
			(std::fabs(t.y - std::min(p1.y, p2.y)) <= eps || std::min(p1.y, p2.y) <= t.y) &&
			(std::fabs(std::max(p1.y, p2.y) - t.y) <= eps || std::max(p1.y, p2.y) >= t.y);
}

static bool _point_in_ray (const _tpoint& p, const _tpoint& p1, const _tpoint& p2, const _tline& lp1p2)
{
	if (_point_in_line (lp1p2, p) != 0)
	{
		return false;
	}
	if (std::fabs(lp1p2.b) <= eps)
	{
		if (p2.y >= p1.y)
		{
			return p.y >= p1.y;
		}
		else
		{
			return p.y <= p1.y;
		}
	}
	if (p2.x >= p1.x)
	{
		return p.x >= p1.x;
	}
	else
	{
		return p.x <= p1.x;
	}
}

static const _tpoint _closest_point(const _tline& l, const _tpoint& p)
{
	const double k = (l.a * p.x + l.b * p.y + l.c) / (l.a * l.a + l.b * l.b);
	return _tpoint(p.x - l.a * k, p.y - l.b * k);
}

static const double _dot(const _tpoint& a, const _tpoint& b)
{
	return (a.x * b.x) + (a.y * b.y);
}

static double _dist(const _tpoint& a, const _tpoint& b)
{
	return std::sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

static double _dist_point_to_ray(const _tpoint& p, const _tpoint& p1, const _tpoint& p2)
{
	const _tline lp1p2 = _toline(p1, p2);
	const _tpoint t = _closest_point(lp1p2, p);
	if (_point_in_ray(t, p1, p2, lp1p2))
	{
		return _dist (p, t);
	}
	else
	{
		return DBL_MAX;
	}
}

static bool _circle_in_line(const _tpoint& t, const double& r, const _tpoint& p1, const _tpoint& p2)
{
	const double x1 = p1.x - t.x;
	const double y1 = p1.y - t.y;
	const double x2 = p2.x - t.x;
	const double y2 = p2.y - t.y;

	const double dx = x2 - x1;
	const double dy = y2 - y1;
	const double a = dx * dx + dy * dy;
	const double b = 2.0 * (x1 * dx + y1 * dy);
	const double c = x1 * x1 + y1 * y1 - r * r;

	if (-b < 0)
	{
		return (c < 0);
	}
	if (-b < (2.0 * a))
	{
		return ((4.0 * a * c - b * b) < 0.0);
	}
	else
	{
		return (a + b + c < 0.0);
	}
}

bool __fastcall TCollideChainShape::Crossline(const TMapCollideLineNode* dragNode,
												const TMapCollideLineNode* fixedPrevNode,
												const TMapCollideLineNode* fixedNextNode) const
{
	static const double POINT_TO_RAY_DISTANCE = 5.0;

	double x, y;
	assert(dragNode != nullptr);
	assert(fixedPrevNode != nullptr || fixedNextNode != nullptr);
	const TMapCollideLineNode* n1 = nullptr;
	const TMapCollideLineNode* n2;

	dragNode->getCoordinates(&x, &y);
	const _tpoint p1 = _tpoint(x, y);

	if(fixedPrevNode != nullptr)
	{
		fixedPrevNode->getCoordinates(&x, &y);
	}
	const _tpoint f1p = _tpoint(x, y);
	const _tline fl1 = _toline(p1, f1p);

	if(fixedNextNode != nullptr)
	{
		fixedNextNode->getCoordinates(&x, &y);
	}
	const _tpoint f2p = _tpoint(x, y);
	const _tline fl2 = _toline(p1, f2p);

	const _tline *pline[2];
	const _tpoint *ppoint[2];
	pline[0] = pline[1] = nullptr;

	for(TCollideNodeList::const_iterator it = mNodes.begin(); it != mNodes.end(); ++it)
	{
		const TMapCollideLineNode& cn = *it;
		if(n1 == nullptr)
		{
			n1 = &cn;
			continue;
		}
		n2 = &cn;
		if((n1 == fixedPrevNode && n2 == dragNode) || (n1 == dragNode && n2 == fixedNextNode))
		{
			n1 = n2;
			continue;
		}

		n1->getCoordinates(&x, &y);
		const _tpoint p3 = _tpoint(x, y);
		n2->getCoordinates(&x, &y);
		const _tpoint p4 = _tpoint(x, y);
		const _tline l2 = _toline(p3, p4);

		if(fixedPrevNode != nullptr)
		{
			pline[0] = &fl1;
			ppoint[0] = &f1p;
		}
		if(fixedNextNode != nullptr)
		{
			pline[1] = &fl2;
			ppoint[1] = &f2p;
		}

		if(n2 == fixedPrevNode || n1 == fixedNextNode)
		{
			double dp;
			if(n1 == fixedNextNode)
			{
				dp = _dist_point_to_ray(p1, p3, p4);
				pline[1] = nullptr;
			}
			else
			{
				dp = _dist_point_to_ray(p1, p4, p3);
				pline[0] = nullptr;
			}
			if(dp <= POINT_TO_RAY_DISTANCE)
			{
				return true;
			}
		}

		for(__int32 l = 0; l < 2; l++)
		{
			if(pline[l] != nullptr)
			{
				bool iscross;
				const __int32 sign1 = _point_in_line(*pline[l], p3) * _point_in_line(*pline[l], p4);
				const __int32 sign2 = _point_in_line(l2, p1) * _point_in_line(l2, *ppoint[l]);
				if(abs(sign1) <= eps && abs(sign2) <= eps)
				{
					iscross = _point_in_box(p1, p3, p4) || _point_in_box(*ppoint[l], p3, p4) ||
						   _point_in_box(p3, p1, *ppoint[l]) || _point_in_box(p4, p1, *ppoint[l]);
				}
				else
				{
					iscross = sign1 <= eps && sign2 <= eps;
				}
				if(iscross)
				{
					return true;
				}
			}
		}
		n1 = n2;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TCollideChainShape::IsPointOnLine(double& x, double& y, const double& findRadius,
													const TMapCollideLineNode* dragNode,
													const TMapCollideLineNode** on1, const TMapCollideLineNode** on2) const
{
	const _tpoint t = _tpoint(x, y);
	const TMapCollideLineNode* n1 = nullptr;
	const TMapCollideLineNode* n2;
	for(TCollideNodeList::const_iterator it = mNodes.begin(); it != mNodes.end(); ++it)
	{
		double cx1, cx2, cy1, cy2;
		const TMapCollideLineNode& cn = *it;
		if(n1 == nullptr)
		{
			n1 = &cn;
			continue;
		}
		n2 = &cn;
		n1->getCoordinates(&cx1, &cy1);
		n2->getCoordinates(&cx2, &cy2);

		const _tpoint p1 = _tpoint(cx1, cy1);
		const _tpoint p2 = _tpoint(cx2, cy2);
		if(_circle_in_line(t, findRadius, p1, p2))
		{
			if(on1 != nullptr)
			{
				*on1 = n1;
			}
			if(on2 != nullptr)
			{
				*on2 = n2;
			}
			if(dragNode != nullptr && (dragNode == n1 || dragNode == n2))
			{
			}
			else
			{
				const _tline l = _toline(p1, p2);
				const _tpoint lp = _closest_point(l, t);
				x = lp.x;
                y = lp.y;
				return true;
			}
		}
		n1 = n2;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TCollideChainShape::IsPointOnNode(const double& x, const double& y,
													const TMapCollideLineNode* dragNode,
													const TMapCollideLineNode** on) const
{
	const _tpoint t = _tpoint(x, y);
	for(TCollideNodeList::const_iterator it = mNodes.begin(); it != mNodes.end(); ++it)
	{
		double cx1, cy1;
		const TMapCollideLineNode& cn = *it;
		if(dragNode != nullptr && dragNode == &cn)
		{
			continue;
		}
		cn.getCoordinates(&cx1, &cy1);

		const double offX = cn.Width / 2.0;
		const double offY = cn.Height / 2.0;
		const _tpoint p1 = _tpoint(cx1 - offX, cy1 - offY);
		const _tpoint p2 = _tpoint(cx1 + offX, cy1 + offY);
		if(_point_in_box(t, p1, p2))
		{
			if(on != nullptr)
			{
				*on = &cn;
			}
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------

void __fastcall TCollideChainShape::SetVisible(bool val)
{
	mVisible = val;
	for(TCollideNodeList::iterator it = mNodes.begin(); it != mNodes.end(); ++it)
	{
		it->SetVisible(val);
	}
}
//---------------------------------------------------------------------------

void __fastcall TCollideChainShape::SetCustomVisible(bool val)
{
	mCustomVisible = val;
	for(TCollideNodeList::iterator it = mNodes.begin(); it != mNodes.end(); ++it)
	{
		it->SetCustomVisible(val);
	}
}
//---------------------------------------------------------------------------

