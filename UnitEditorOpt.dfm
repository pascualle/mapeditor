object FEditorOptions: TFEditorOptions
  Left = 363
  Top = 246
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'mEditorSettings'
  ClientHeight = 492
  ClientWidth = 374
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Pitch = fpFixed
  Font.Style = []
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  TextHeight = 13
  object ButtonCancel: TButton
    Left = 73
    Top = 458
    Width = 75
    Height = 24
    Caption = 'mCancel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpFixed
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 1
  end
  object ButtonOk: TButton
    Left = 154
    Top = 458
    Width = 147
    Height = 24
    Caption = 'mApply'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpFixed
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 0
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 374
    Height = 449
    ActivePage = TabSheet1
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Pitch = fpFixed
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    ExplicitWidth = 370
    object TabSheet1: TTabSheet
      Caption = 'mGlobalCommon'
      object GroupBoxAnim: TGroupBox
        Left = 5
        Top = 124
        Width = 354
        Height = 65
        Caption = 'mAnimation'
        TabOrder = 1
        object Label2: TLabel
          Left = 11
          Top = 28
          Width = 122
          Height = 13
          Caption = 'mAnimationSpeedCaption'
        end
      end
      object GroupBoxView: TGroupBox
        Left = 5
        Top = 8
        Width = 354
        Height = 113
        Caption = 'mView'
        TabOrder = 0
        object Label1: TLabel
          Left = 9
          Top = 27
          Width = 70
          Height = 13
          Caption = 'mObjIconsSize'
        end
        object Label11: TLabel
          Left = 9
          Top = 53
          Width = 68
          Height = 13
          Caption = 'mDefaultScale'
        end
        object Label7: TLabel
          Left = 9
          Top = 78
          Width = 100
          Height = 13
          Caption = 'mInterfaceLanguage'
        end
        object ComboBox: TComboBox
          Left = 277
          Top = 50
          Width = 68
          Height = 21
          Style = csDropDownList
          ItemIndex = 2
          TabOrder = 0
          Text = 'x1'
          Items.Strings = (
            'x0.25'
            'x0.5'
            'x1'
            'x2'
            'x4')
        end
        object ComboBoxLang: TComboBox
          Left = 248
          Top = 75
          Width = 97
          Height = 21
          Style = csDropDownList
          TabOrder = 1
          Items.Strings = (
            '')
        end
      end
      object GroupBoxGraphicsResTypes: TGroupBox
        Left = 5
        Top = 192
        Width = 354
        Height = 63
        Caption = 'mGraphicsRes'
        TabOrder = 2
        object Label13: TLabel
          Left = 9
          Top = 28
          Width = 96
          Height = 13
          Caption = 'mGraphicsResTypes'
        end
        object ComboBox_GrType: TComboBox
          Left = 277
          Top = 24
          Width = 68
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 0
          Text = 'BMP'
          OnChange = ComboBox_GrTypeChange
          Items.Strings = (
            'BMP'
            'PNG')
        end
      end
      object GroupBoxFx32: TGroupBox
        Left = 5
        Top = 324
        Width = 354
        Height = 85
        Caption = 'mExportParameters'
        TabOrder = 4
        object Label5: TLabel
          Left = 9
          Top = 28
          Width = 51
          Height = 13
          Caption = 'mFx32Size'
        end
        object Label8: TLabel
          Left = 9
          Top = 53
          Width = 58
          Height = 13
          Caption = 'mWcharSize'
        end
        object ComboBoxFx32: TComboBox
          Left = 176
          Top = 24
          Width = 169
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 0
          Text = 'mFx32Float'
          OnChange = ComboBox_GrTypeChange
          Items.Strings = (
            'mFx32Float'
            'mFx32Fixed'
            'Nintendo DS')
        end
        object ComboBoxWchar: TComboBox
          Left = 176
          Top = 49
          Width = 169
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 1
          Text = '4 bytes (default)'
          OnChange = ComboBox_GrTypeChange
          Items.Strings = (
            '4 bytes (default)'
            '2 bytes (Windows OS)')
        end
      end
      object GroupBoxSoundResTypes: TGroupBox
        Left = 5
        Top = 258
        Width = 354
        Height = 63
        Caption = 'mSoundRes'
        TabOrder = 3
        object Label6: TLabel
          Left = 9
          Top = 28
          Width = 85
          Height = 13
          Caption = 'mSoundResTypes'
        end
        object ComboBox_SndType: TComboBox
          Left = 277
          Top = 24
          Width = 68
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 0
          Text = 'WAV'
          OnChange = ComboBox_SndTypeChange
          Items.Strings = (
            'WAV')
        end
      end
    end
    object TabSheetFMessages: TTabSheet
      Caption = 'mTextEditor'
      ImageIndex = 1
      object GroupBox1: TGroupBox
        Left = 5
        Top = 8
        Width = 354
        Height = 83
        Caption = 'mCanvasSize'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Pitch = fpFixed
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label3: TLabel
          Left = 14
          Top = 24
          Width = 36
          Height = 13
          Caption = 'mWidth'
        end
        object Label4: TLabel
          Left = 14
          Top = 52
          Width = 39
          Height = 13
          Caption = 'mHeight'
        end
      end
      object GroupBoxLanguages: TGroupBox
        Left = 5
        Top = 97
        Width = 354
        Height = 168
        Caption = 'mLocalizations'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Pitch = fpFixed
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object Button1: TButton
          Left = 14
          Top = 132
          Width = 171
          Height = 25
          Caption = 'mEdit'
          TabOrder = 1
          OnClick = Button1Click
        end
        object ListBoxLang: TCheckListBox
          Left = 14
          Top = 20
          Width = 171
          Height = 113
          ItemHeight = 17
          TabOrder = 0
          OnClickCheck = ListBoxLangClickCheck
        end
      end
    end
    object PageBackdrop: TTabSheet
      Caption = 'mBackdrop'
      ImageIndex = 2
      object GroupBox_b: TGroupBox
        Left = 5
        Top = 8
        Width = 354
        Height = 265
        Caption = 'mBackdropTransparentColor'
        TabOrder = 0
        object LabelBTC: TLabel
          Left = 9
          Top = 203
          Width = 136
          Height = 13
          Caption = 'mBackdropTransparentColor'
        end
        object Label9: TLabel
          Left = 9
          Top = 234
          Width = 52
          Height = 13
          Caption = 'mGridColor'
        end
        object Image: TImage
          Left = 9
          Top = 17
          Width = 336
          Height = 175
        end
        object ButtonBTC: TButton
          Left = 176
          Top = 198
          Width = 169
          Height = 25
          Caption = 'mChange'
          TabOrder = 0
          OnClick = ButtonBTCClick
        end
        object ButtonGC: TButton
          Left = 176
          Top = 229
          Width = 169
          Height = 25
          Caption = 'mChange'
          TabOrder = 1
          OnClick = ButtonBTCClick
        end
      end
    end
  end
  object ColorDialog: TColorDialog
    Left = 240
    Top = 24
  end
end
