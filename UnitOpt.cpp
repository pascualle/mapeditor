#include <vcl.h>
#pragma hdrstop

#include "UTypes.h"
#include "UnitOpt.h"
#include "UObjPattern.h"
#include "UEditObjPattern.h"
#include "UCreateMessages.h"
#include "UObjCode.h"
#include "MainUnit.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm_opt *Form_opt;

static const __int32 MIN_VIEW_SIZE = 2;
static const __int32 MAX_VIEW_SIZE = 512;

static const __int32 MAX_MAP_SIZE = MAX_MAP_SIZE_PX / MIN_VIEW_SIZE;

//---------------------------------------------------------------------------
__fastcall TForm_opt::TForm_opt(TComponent* Owner)
        : TForm(Owner)
{
    mMode = mDel;
	CSpinEditDH = new TSpinEdit(GroupBox_d);
	CSpinEditDH->Parent = GroupBox_d;
	CSpinEditDH->Left = 136;
	CSpinEditDH->Top = 24;
	CSpinEditDH->Width = 81;
	CSpinEditDH->Height = 22;
	CSpinEditDH->MaxValue = MAX_VIEW_SIZE;
	CSpinEditDH->MinValue = MIN_VIEW_SIZE;
	CSpinEditDH->TabOrder = 0;
	CSpinEditDH->Value = 24;
	CSpinEditDH->OnChange = CSpinEditPHChange;
	CSpinEditDH->Visible = true;
	CSpinEditDW = new TSpinEdit(GroupBox_d);
	CSpinEditDW->Parent = GroupBox_d;
	CSpinEditDW->Left = 136;
	CSpinEditDW->Top = 48;
	CSpinEditDW->Width = 81;
	CSpinEditDW->Height = 22;
	CSpinEditDW->MaxValue = MAX_VIEW_SIZE;
	CSpinEditDW->MinValue = MIN_VIEW_SIZE;
	CSpinEditDW->TabOrder = 1;
	CSpinEditDW->Value = 32;
	CSpinEditDW->OnChange = CSpinEditPHChange;
	CSpinEditDW->Visible = true;
	CSpinEditMH = new TSpinEdit(GroupBox_m);
	CSpinEditMH->Parent = GroupBox_m;
	CSpinEditMH->Left = 136;
	CSpinEditMH->Top = 24;
	CSpinEditMH->Width = 81;
	CSpinEditMH->Height = 22;
	CSpinEditMH->MaxValue = MAX_MAP_SIZE;
	CSpinEditMH->MinValue = 1;
	CSpinEditMH->TabOrder = 0;
	CSpinEditMH->Value = 2;
	CSpinEditMH->OnChange = CSpinEditPHChange;
	CSpinEditMH->Visible = true;
	CSpinEditMW = new TSpinEdit(GroupBox_m);
	CSpinEditMW->Parent = GroupBox_m;
	CSpinEditMW->Left = 136;
	CSpinEditMW->Top = 48;
	CSpinEditMW->Width = 81;
	CSpinEditMW->Height = 22;
	CSpinEditMW->MaxValue = MAX_MAP_SIZE;
	CSpinEditMW->MinValue = 1;
	CSpinEditMW->TabOrder = 1;
	CSpinEditMW->Value = 2;
	CSpinEditMW->OnChange = CSpinEditPHChange;
	CSpinEditMW->Visible = true;
	CSpinEditZone = new TSpinEdit(GroupBox_zone);
	CSpinEditZone->Parent = GroupBox_zone;
	CSpinEditZone->Left = 128;
	CSpinEditZone->Top = 24;
	CSpinEditZone->Width = 81;
	CSpinEditZone->Height = 22;
	CSpinEditZone->MaxValue = 64;
	CSpinEditZone->MinValue = 1;
	CSpinEditZone->TabOrder = 0;
	CSpinEditZone->Value = 1;
	CSpinEditZone->OnChange = CSpinEditZoneChange;
	CSpinEditZone->Visible = true;
	CSpinEditZoneOnStart = new TSpinEdit(TabFirstTime);
	CSpinEditZoneOnStart->Parent = TabFirstTime;
	CSpinEditZoneOnStart->Left = 392;
	CSpinEditZoneOnStart->Top = 18;
	CSpinEditZoneOnStart->Width = 81;
	CSpinEditZoneOnStart->Height = 22;
	CSpinEditZoneOnStart->MaxValue = 1;
	CSpinEditZoneOnStart->TabOrder = 0;
	CSpinEditZoneOnStart->OnChange = CSpinEditZoneOnStartChange;
	CSpinEditZoneOnStart->Visible = true;
	CE_TileCameraOnStart = new TSpinEdit(TabFirstTime);
	CE_TileCameraOnStart->Parent = TabFirstTime;
	CE_TileCameraOnStart->Left = 392;
	CE_TileCameraOnStart->Top = 107;
	CE_TileCameraOnStart->Width = 81;
	CE_TileCameraOnStart->Height = 22;
	CE_TileCameraOnStart->MaxValue = 65535;
	CE_TileCameraOnStart->TabOrder = 3;
	CE_TileCameraOnStart->OnChange = CSpinEditZoneOnStartChange;
	CE_TileCameraOnStart->Visible = true;

	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TForm_opt::FormDestroy(TObject *)
{
	delete CSpinEditDH;
	delete CSpinEditDW;
	delete CSpinEditMH;
	delete CSpinEditMW;
	delete CSpinEditZone;
	delete CSpinEditZoneOnStart;
	delete CE_TileCameraOnStart;
}
//---------------------------------------------------------------------------

void __fastcall TForm_opt::CSpinEditPHChange(TObject *Sender)
{
	UnicodeString str;
	TSpinEdit *c = (TSpinEdit*)Sender;
	if(c->Text.IsEmpty())
	{
		return;
	}

	__int32 mw;
	__int32 mh;
	short p_w, p_h;
	Form_m->GetTileSize(p_w, p_h);

	mw = p_w * CSpinEditDW->Value * CSpinEditMW->Value;
	if(mw > MAX_MAP_SIZE_PX)
	{
		CSpinEditMW->Value = (MAX_MAP_SIZE_PX / p_w) / CSpinEditDW->Value;
	}
	mw = p_w * CSpinEditDW->Value * CSpinEditMW->Value;
	while(mw > MAX_MAP_SIZE_PX)
	{
		CSpinEditMW->Value = CSpinEditMW->Value - 1;
		mw = p_w * CSpinEditDW->Value * CSpinEditMW->Value;
	}

	mh = CSpinEditDH->Value * p_h * CSpinEditMH->Value;
	if(mh > MAX_MAP_SIZE_PX)
	{
		CSpinEditMH->Value = (MAX_MAP_SIZE_PX / p_h) / CSpinEditDH->Value;
	}
	mh = CSpinEditDH->Value * p_h * CSpinEditMH->Value;
	while(mh > MAX_MAP_SIZE_PX)
	{
		CSpinEditMH->Value = CSpinEditMH->Value - 1;
		mh = CSpinEditDH->Value * p_h * CSpinEditMH->Value;
	}

	str = IntToStr((__int32)(p_w * CSpinEditDW->Value)) + _T("x") + IntToStr((__int32)(CSpinEditDH->Value * p_h));
	Label6->Caption = str;
	str=IntToStr(mw) + _T("x")+ IntToStr(mh);
	Label11->Caption = str;
}
//---------------------------------------------------------------------------

void __fastcall TForm_opt::FormShow(TObject */*Sender*/)
{
	CSpinEditPHChange(CSpinEditDH);
	CSpinEditZoneChange(NULL);
	CheckCheckBoxTileCameraOnStart();
}
//---------------------------------------------------------------------------

//First start events
void __fastcall TForm_opt::EdFirstScriptChange(TObject */*Sender*/)
{
	Edit_nameChangeMain(EdFirstScript);
}
//---------------------------------------------------------------------------

void __fastcall TForm_opt::CSpinEditZoneChange(TObject */*Sender*/)
{
	CSpinEditZoneOnStart->MaxValue = CSpinEditZone->Value - 1;
	if(CSpinEditZoneOnStart->Value > CSpinEditZoneOnStart->MaxValue)
	{
		CSpinEditZoneOnStart->Value = CSpinEditZoneOnStart->MaxValue;
	}
}
//---------------------------------------------------------------------------

//Messages

void __fastcall TForm_opt::CSpinEditZoneOnStartChange(TObject */*Sender*/)
{
	if(CSpinEditZoneOnStart->MaxValue == 0 && CSpinEditZoneOnStart->MinValue == 0)
	{
		CSpinEditZoneOnStart->Value = 0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_opt::CheckCheckBoxTileCameraOnStart()
{
	if(!CheckBoxTileCameraOnStart->Checked)
	{
		CE_TileCameraOnStart->Enabled = false;
		CE_TileCameraOnStart->Color = clBtnFace;
	}
	else
	{
		CE_TileCameraOnStart->Enabled = true;
		CE_TileCameraOnStart->Color = clWindow;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm_opt::CheckBoxTileCameraOnStartClick(TObject */*Sender*/)
{
	CheckCheckBoxTileCameraOnStart();
}
//---------------------------------------------------------------------------

void __fastcall TForm_opt::SetMode(const EditMode mode)
{
	mMode = mode;
}
//---------------------------------------------------------------------------

const EditMode __fastcall TForm_opt::GetMode() const
{
	return mMode;
}
//---------------------------------------------------------------------------

void __fastcall TForm_opt::Button1Click(TObject*)
{
	PostMessage(Form_m->Handle, MEWM_WNDOPT, 0, mrOk);
}
//---------------------------------------------------------------------------

void __fastcall TForm_opt::Button2Click(TObject*)
{
	PostMessage(Form_m->Handle, MEWM_WNDOPT, 0, mrCancel);
}
//---------------------------------------------------------------------------

void __fastcall TForm_opt::FormClose(TObject* /*Sender*/, TCloseAction& /*Action*/)
{
	PostMessage(Form_m->Handle, MEWM_WNDOPT, 0, mrCancel);
}
//---------------------------------------------------------------------------

