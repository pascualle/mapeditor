object FScript: TFScript
  Left = 219
  Top = 192
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'mMiniscript'
  ClientHeight = 418
  ClientWidth = 672
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  DesignSize = (
    672
    418)
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 11
    Width = 35
    Height = 13
    Caption = 'mName'
  end
  object Label2: TLabel
    Left = 8
    Top = 45
    Width = 53
    Height = 13
    Caption = 'mCondition'
  end
  object Label14: TLabel
    Left = 343
    Top = 11
    Width = 32
    Height = 13
    Caption = 'mZone'
  end
  object Label16: TLabel
    Left = 406
    Top = 45
    Width = 38
    Height = 13
    Caption = 'mAction'
  end
  object Edit_name: TEdit
    Left = 48
    Top = 8
    Width = 220
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 64
    TabOrder = 0
    OnChange = Edit_nameChange
  end
  object PageControl: TPageControl
    Left = 8
    Top = 59
    Width = 395
    Height = 320
    ActivePage = TabEndAnim
    TabOrder = 3
    object TabCollide: TTabSheet
      Caption = 'mCollision'
      object Bevel5: TBevel
        Left = 6
        Top = 4
        Width = 378
        Height = 110
        Shape = bsFrame
      end
      object Label4: TLabel
        Left = 189
        Top = 24
        Width = 7
        Height = 13
        Caption = '&&'
      end
      object Label6: TLabel
        Left = 12
        Top = 6
        Width = 37
        Height = 13
        Caption = 'mGroup'
      end
      object Label7: TLabel
        Left = 204
        Top = 6
        Width = 35
        Height = 13
        Caption = 'mName'
      end
      object Bevel1: TBevel
        Left = 155
        Top = 42
        Width = 27
        Height = 50
      end
      object Bevel2: TBevel
        Left = 346
        Top = 42
        Width = 27
        Height = 50
      end
      object Label11: TLabel
        Left = 9
        Top = 93
        Width = 114
        Height = 13
        Caption = 'mLogicalANDDescription'
      end
      object ListBox_gr: TListBox
        Left = 12
        Top = 42
        Width = 143
        Height = 50
        ItemHeight = 13
        TabOrder = 1
      end
      object CB_col_obj: TComboBox
        Left = 204
        Top = 20
        Width = 170
        Height = 21
        Style = csDropDownList
        TabOrder = 4
      end
      object CB_col_gr: TComboBox
        Left = 12
        Top = 20
        Width = 170
        Height = 21
        Style = csDropDownList
        TabOrder = 0
      end
      object Bt_gradd: TBitBtn
        Left = 157
        Top = 43
        Width = 24
        Height = 24
        Hint = 'mAdd'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD000DDDDDDDDDDDD02227DD
          DDDDDDDDD02227DDDDDDDDD000222777DDDDDD02222222227DDDDD0222222222
          7DDDDD02222222227DDDDDD000222777DDDDDDDDD02227DDDDDDDDDDD02227DD
          DDDDDDDDDD777DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = Bt_graddClick
      end
      object Bt_grdel: TBitBtn
        Left = 157
        Top = 67
        Width = 24
        Height = 24
        Hint = 'mDoDelete'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDD777777777DDDDDD09999999997DDDDD0999999999
          7DDDDD09999999997DDDDDD000000000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = Bt_grdelClick
      end
      object Bt_objdel: TBitBtn
        Left = 348
        Top = 67
        Width = 24
        Height = 24
        Hint = 'mDoDelete'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDD777777777DDDDDD09999999997DDDDD0999999999
          7DDDDD09999999997DDDDDD000000000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        OnClick = Bt_objdelClick
      end
      object Bt_objadd: TBitBtn
        Left = 348
        Top = 43
        Width = 24
        Height = 24
        Hint = 'mAdd'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD000DDDDDDDDDDDD02227DD
          DDDDDDDDD02227DDDDDDDDD000222777DDDDDD02222222227DDDDD0222222222
          7DDDDD02222222227DDDDDD000222777DDDDDDDDD02227DDDDDDDDDDD02227DD
          DDDDDDDDDD777DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = Bt_objaddClick
      end
      object ListBox_obj: TListBox
        Left = 204
        Top = 42
        Width = 142
        Height = 50
        ItemHeight = 13
        TabOrder = 5
      end
      object GroupBoxInitiator: TGroupBox
        Left = 6
        Top = 124
        Width = 378
        Height = 73
        Caption = 'mInitiator'
        TabOrder = 8
        object EditStateInitiator: TEdit
          Left = 8
          Top = 18
          Width = 168
          Height = 21
          Enabled = False
          ReadOnly = True
          TabOrder = 0
          Text = 'mPrpObjState'
        end
        object CB_act_ParamInitiator: TComboBox
          Left = 9
          Top = 45
          Width = 168
          Height = 21
          Style = csDropDownList
          TabOrder = 3
        end
        object CB_Op_ParamInitiator: TComboBox
          Left = 183
          Top = 43
          Width = 52
          Height = 21
          Style = csDropDownList
          ItemIndex = 1
          TabOrder = 4
          Text = '=='
          Items.Strings = (
            '<='
            '=='
            '>='
            '!=')
        end
        object CB_Op_StateInitiator: TComboBox
          Left = 183
          Top = 18
          Width = 52
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 1
          Text = '=='
          Items.Strings = (
            '=='
            '!=')
        end
        object CB_StateNameInitiator: TComboBox
          Left = 241
          Top = 18
          Width = 129
          Height = 21
          Style = csDropDownList
          ParentShowHint = False
          ShowHint = False
          TabOrder = 2
        end
      end
      object GroupBox2: TGroupBox
        Left = 6
        Top = 208
        Width = 378
        Height = 73
        Caption = 'mSecondParticipant'
        TabOrder = 9
        object EditStateSecond: TEdit
          Left = 8
          Top = 18
          Width = 168
          Height = 21
          Enabled = False
          ReadOnly = True
          TabOrder = 0
          Text = 'mPrpObjState'
        end
        object CB_act_ParamSecond: TComboBox
          Left = 8
          Top = 44
          Width = 168
          Height = 21
          Style = csDropDownList
          TabOrder = 3
        end
        object CB_Op_ParamSecond: TComboBox
          Left = 183
          Top = 43
          Width = 52
          Height = 21
          Style = csDropDownList
          ItemIndex = 1
          TabOrder = 4
          Text = '=='
          Items.Strings = (
            '<='
            '=='
            '>='
            '!=')
        end
        object CB_Op_StateSecond: TComboBox
          Left = 183
          Top = 18
          Width = 52
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 1
          Text = '=='
          Items.Strings = (
            '=='
            '!=')
        end
        object CB_StateNameSecond: TComboBox
          Left = 241
          Top = 18
          Width = 129
          Height = 21
          Style = csDropDownList
          ParentShowHint = False
          ShowHint = False
          TabOrder = 2
        end
      end
    end
    object TabTileCollide: TTabSheet
      Caption = 'mCollisionWithBgObject'
      ImageIndex = 3
      TabVisible = False
      object Label10: TLabel
        Left = 3
        Top = 7
        Width = 165
        Height = 13
        Caption = 'mCollisionWithBgObjectDescription'
      end
      object GroupBox3: TGroupBox
        Left = 5
        Top = 26
        Width = 378
        Height = 75
        Caption = 'mInitiator'
        TabOrder = 0
        object EditStateInitiatorTile: TEdit
          Left = 8
          Top = 18
          Width = 168
          Height = 21
          Enabled = False
          ReadOnly = True
          TabOrder = 0
          Text = 'mPrpObjState'
        end
        object CB_act_ParamInitiatorTile: TComboBox
          Left = 8
          Top = 44
          Width = 168
          Height = 21
          Style = csDropDownList
          TabOrder = 3
        end
        object CB_Op_ParamInitiatorTile: TComboBox
          Left = 183
          Top = 43
          Width = 52
          Height = 21
          Style = csDropDownList
          ItemIndex = 1
          TabOrder = 4
          Text = '=='
          Items.Strings = (
            '<='
            '=='
            '>='
            '!=')
        end
        object CB_Op_StateInitiatorTile: TComboBox
          Left = 183
          Top = 18
          Width = 52
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 1
          Text = '=='
          Items.Strings = (
            '=='
            '!=')
        end
        object CB_StateNameInitiatorTile: TComboBox
          Left = 241
          Top = 18
          Width = 129
          Height = 21
          Style = csDropDownList
          ParentShowHint = False
          ShowHint = False
          TabOrder = 2
        end
      end
      object GroupBox4: TGroupBox
        Left = 5
        Top = 101
        Width = 378
        Height = 56
        Caption = 'mSecondParticipant'
        TabOrder = 1
        object Label8: TLabel
          Left = 7
          Top = 22
          Width = 62
          Height = 13
          Caption = 'mBgObjectId'
        end
      end
    end
    object TabChangeDirector: TTabSheet
      Caption = 'mActNameChangeNode'
      ImageIndex = 4
      object Label15: TLabel
        Left = 3
        Top = 7
        Width = 123
        Height = 13
        Caption = 'mChangeNodeDescription'
      end
      object GroupBox5: TGroupBox
        Left = 5
        Top = 26
        Width = 378
        Height = 75
        Caption = 'mInitiator'
        TabOrder = 0
        object EditStateInitiatorNode: TEdit
          Left = 8
          Top = 18
          Width = 168
          Height = 21
          Enabled = False
          ReadOnly = True
          TabOrder = 0
          Text = 'mPrpObjState'
        end
        object CB_act_ParamInitiatorNode: TComboBox
          Left = 8
          Top = 44
          Width = 168
          Height = 21
          Style = csDropDownList
          TabOrder = 3
        end
        object CB_Op_ParamInitiatorNode: TComboBox
          Left = 183
          Top = 43
          Width = 52
          Height = 21
          Style = csDropDownList
          ItemIndex = 1
          TabOrder = 4
          Text = '=='
          Items.Strings = (
            '<='
            '=='
            '>='
            '!=')
        end
        object CB_Op_StateInitiatorNode: TComboBox
          Left = 183
          Top = 18
          Width = 52
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 1
          Text = '=='
          Items.Strings = (
            '=='
            '!=')
        end
        object CB_StateNameInitiatorNode: TComboBox
          Left = 241
          Top = 18
          Width = 129
          Height = 21
          Style = csDropDownList
          ParentShowHint = False
          ShowHint = False
          TabOrder = 2
        end
      end
      object GroupBox6: TGroupBox
        Left = 5
        Top = 101
        Width = 378
        Height = 56
        Caption = 'mDirector'
        TabOrder = 1
        object CB_node: TComboBox
          Left = 8
          Top = 20
          Width = 362
          Height = 21
          Style = csDropDownList
          TabOrder = 0
        end
      end
    end
    object TabChange: TTabSheet
      Caption = 'mPropertyValueChange'
      ImageIndex = 2
      object Label3: TLabel
        Left = 10
        Top = 7
        Width = 58
        Height = 13
        Caption = 'mParameter'
      end
      object Label5: TLabel
        Left = 305
        Top = 7
        Width = 34
        Height = 13
        Caption = 'mValue'
      end
      object CB_param: TComboBox
        Left = 10
        Top = 22
        Width = 231
        Height = 21
        Style = csDropDownList
        TabOrder = 0
      end
      object CB_Op: TComboBox
        Left = 247
        Top = 22
        Width = 52
        Height = 21
        Style = csDropDownList
        ItemIndex = 1
        TabOrder = 1
        Text = '=='
        Items.Strings = (
          '<='
          '=='
          '>='
          '!=')
      end
    end
    object TabEndAnim: TTabSheet
      Caption = 'mUniversal'
      ImageIndex = 2
      object Label9: TLabel
        Left = 153
        Top = 122
        Width = 66
        Height = 13
        Caption = 'mNoCondition'
      end
    end
  end
  object ButtonCancel: TButton
    Left = 98
    Top = 385
    Width = 76
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'mCancel'
    ModalResult = 2
    TabOrder = 6
    ExplicitTop = 384
  end
  object ButtonOk: TButton
    Left = 180
    Top = 385
    Width = 389
    Height = 25
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'mApply'
    ModalResult = 1
    TabOrder = 5
    ExplicitTop = 384
    ExplicitWidth = 385
  end
  object TabControl: TTabControl
    Left = 406
    Top = 59
    Width = 258
    Height = 320
    TabOrder = 4
    object Bevel3: TBevel
      Left = 224
      Top = 43
      Width = 27
      Height = 98
    end
    object Label12: TLabel
      Left = 13
      Top = 6
      Width = 74
      Height = 13
      Caption = 'mTriggersChain'
    end
    object CB_trig: TComboBox
      Left = 8
      Top = 21
      Width = 216
      Height = 21
      Style = csDropDownList
      TabOrder = 0
      OnExit = CB_trigExit
    end
    object Bt_trdel: TBitBtn
      Left = 226
      Top = 68
      Width = 24
      Height = 24
      Hint = 'mDoDelete'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDD777777777DDDDDD09999999997DDDDD0999999999
        7DDDDD09999999997DDDDDD000000000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = Bt_trdelClick
    end
    object Bt_tradd: TBitBtn
      Left = 226
      Top = 44
      Width = 24
      Height = 24
      Hint = 'mAdd'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD000DDDDDDDDDDDD02227DD
        DDDDDDDDD02227DDDDDDDDD000222777DDDDDD02222222227DDDDD0222222222
        7DDDDD02222222227DDDDDD000222777DDDDDDDDD02227DDDDDDDDDDD02227DD
        DDDDDDDDDD777DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = Bt_traddClick
    end
    object Bt_trdown: TBitBtn
      Left = 226
      Top = 116
      Width = 24
      Height = 24
      Hint = 'mMoveDown'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD777DDD
        DDDDDDDDD0CCC7DDDDDDDDDD0CCCCC7DDDDDDDD0CCCCCCC7DDDDDD0CCCCCCCCC
        7DDDDD0000CCC7777DDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DD
        DDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DD
        DDDDDDDDD0CCC7DDDDDDDDDDDD000DDDDDDDDDDDDDDDDDDDDDDD}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      OnClick = Bt_trdownClick
    end
    object Bt_trup: TBitBtn
      Left = 226
      Top = 92
      Width = 24
      Height = 24
      Hint = 'mMoveUp'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDD000DDDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DD
        DDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DDDDDDDDDDD0CCC7DD
        DDDDDDDDD0CCC7DDDDDDDD0000CCC7777DDDDD0CCCCCCCCC7DDDDDD0CCCCCCC7
        DDDDDDDD0CCCCC7DDDDDDDDDD0CCC7DDDDDDDDDDDD777DDDDDDD}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = Bt_trupClick
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 146
      Width = 242
      Height = 159
      Caption = 'mAfterTriggersChain'
      TabOrder = 7
      object Label13: TLabel
        Left = 9
        Top = 16
        Width = 80
        Height = 13
        Caption = 'mChangeHeroTo'
      end
      object Bevel4: TBevel
        Left = 209
        Top = 100
        Width = 27
        Height = 22
      end
      object CB_new_hero: TComboBox
        Left = 9
        Top = 30
        Width = 227
        Height = 21
        Style = csDropDownList
        TabOrder = 0
      end
      object CheckBox_end: TCheckBox
        Left = 9
        Top = 134
        Width = 128
        Height = 17
        Caption = 'mGotoLevel'
        TabOrder = 5
        OnClick = CheckBox_endClick
      end
      object CheckBox_zone: TCheckBox
        Left = 9
        Top = 59
        Width = 106
        Height = 17
        Caption = 'mGotoZone'
        TabOrder = 1
        OnClick = CheckBox_zoneClick
      end
      object CheckBoxMessageID: TCheckBox
        Left = 9
        Top = 83
        Width = 196
        Height = 17
        Caption = 'mShowText'
        TabOrder = 2
        OnClick = CheckBoxMessageIDClick
      end
      object Bt_3dot: TButton
        Left = 211
        Top = 101
        Width = 24
        Height = 20
        Caption = '...'
        TabOrder = 4
        OnClick = Bt_3dotClick
      end
      object EditMessageName: TEdit
        Left = 9
        Top = 101
        Width = 202
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 32
        TabOrder = 3
        OnChange = Edit_nameChange
      end
    end
    object ListBox_trig: TListBox
      Left = 8
      Top = 43
      Width = 216
      Height = 98
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = False
      TabOrder = 2
      OnExit = ListBox_trigExit
    end
    object Bt_find_trig: TBitBtn
      Left = 226
      Top = 21
      Width = 24
      Height = 20
      Hint = 'mFindOnMap'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000FC02FC004985
        D40084C6F400C7D1D100FAEFC700B08D8800B5B8BC00FCFCDA0056595B005CB6
        F10072758400868F9200F5D7B000B6C8CE00D3B39E008E787C000BAA60000000
        0000BA58A6000000000091A58A6000000000D91AF8A6000000000291A58BDDDD
        000000291AF8AA88ABD0000291BF5ECEA8FD0000265C47774F8B000006C44777
        748A00000E4C7777775800000E44477777EA00000C44477777EA00000E44C477
        745B000003C7744C4CFD000000E47444C5D000000006EEEEE300}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = Bt_find_trigClick
    end
  end
  object CheckBox_multi: TCheckBox
    Left = 48
    Top = 29
    Width = 236
    Height = 17
    Caption = 'mThisMiniscriptMultiple'
    TabOrder = 2
  end
  object ComboBox_zone: TComboBox
    Left = 377
    Top = 8
    Width = 287
    Height = 21
    Style = csDropDownList
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnChange = ComboBox_zoneChange
  end
end
