//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UTxtObjProperty.h"
#include "MainUnit.h"
#include "UObjManager.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormTxtObjProperty *FormTxtObjProperty;
//---------------------------------------------------------------------------

__fastcall TFormTxtObjProperty::TFormTxtObjProperty(TComponent* Owner)
	: TPrpFunctions(Owner)
{
	mpTextObj = NULL;
	mpEditField = NULL;
	mOldName = _T("");
	mOldVal = _T("");
	mNameObj = _T("");
	mEmulatorMode = false;
	mDisableChanges = true;
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::Localize()
{
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::Init(TCommonResObjManager *)
{
	Form_m->RefreshFonts();

	ComboBox_zone->Items->Assign(Form_m->ListBoxZone->Items);

	CB_Font->Items->Clear();
	Form_m->mpTexts->GetFontNames((TStringList*)CB_Font->Items);

	if (mpTextObj != NULL)
	{
		AssignTxtObj(mpTextObj);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::BitBtn_find_objClick(TObject *)
{
	Form_m->FindAndShowObjectByName(mNameObj, false);
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::Edit_nameEnter(TObject *)
{
	mOldName = Edit_name->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::Edit_nameExit(TObject *)
{
	if (mpTextObj == NULL)
	{
		return;
	}

	UnicodeString str = Edit_name->Text.Trim();
	if(str.Compare(mOldName) == 0)
	{
		return;
	}

	if (str.IsEmpty())
	{
		Application->MessageBox(Localization::Text(_T("mObjNameError")).c_str(),
						Localization::Text(_T("mError")).c_str(),
						MB_OK | MB_ICONERROR);
		Edit_name->Text = mOldName;
		return;
	}
	if (Form_m->m_pObjManager->CheckIfMGONameExists(str, mpTextObj) == true)
	{
		Application->MessageBox(Localization::Text(_T("mObjNameExist")).c_str(),
					Localization::Text(_T("mError")).c_str(),
					MB_OK | MB_ICONERROR);
		Edit_name->Text = mOldName;
		return;
	}
	if (!mDisableChanges && mOldName.Compare(str) != 0)
	{
		mpTextObj->Name = str;
		mNameObj = str;
		Form_m->OnChangeObjName(mpTextObj, mOldName);
		mpTextObj->changeHint();
		mOldName = str;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::Edit_nameKeyPress(TObject *, wchar_t &Key)
{
	if (VK_RETURN == Key)
	{
		Key = 0;
		Edit_nameExit(NULL);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::RBStaticModeClick(TObject *)
{
	if(!RBStaticMode->Checked)
	{
		EditTextId->Enabled = false;
		Bt_3dot->Enabled = false;
		EditTextId->Color = clBtnFace;
		MemoStaticText->Visible = false;
		EditDynamicTextStrings->Enabled = true;
		EditDynamicTextMaxChars->Enabled = true;
		EditDynamicTextStrings->Color = clWindow;
		EditDynamicTextMaxChars->Color = clWindow;
		if (mpTextObj && !mDisableChanges)
		{
			mpTextObj->SetTextId(_T(""));
			mpTextObj->SetTextType(TMapTextBox::MTBT_Dynamic);
			Form_m->OnChangeTextId(mpTextObj);
		}
	}
	else
	{
		EditTextId->Enabled = true;
		EditTextId->Color = clWindow;
		Bt_3dot->Enabled = true;
		MemoStaticText->Visible = true;
		EditDynamicTextStrings->Enabled = false;
		EditDynamicTextMaxChars->Enabled = false;
		EditDynamicTextStrings->Color = clBtnFace;
		EditDynamicTextMaxChars->Color = clBtnFace;
		UnicodeString str = EditTextId->Text.Trim();
		if (mpTextObj && !mDisableChanges)
		{
			mpTextObj->SetTextId(str);
			mpTextObj->SetTextType(TMapTextBox::MTBT_Static);
			Form_m->OnChangeTextId(mpTextObj);
		}
    }
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::CSEd_xEnter(TObject *Sender)
{
	mpEditField = Sender;
	mOldVal = ((TEdit*)Sender)->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::CSEd_xExit(TObject *Sender)
{
	UnicodeString str = ((TEdit*)Sender)->Text.Trim();

	if(Sender == mpEditField)
	{
		if (!mDisableChanges && mOldVal.Compare(str) != 0)
		{

			if (mpTextObj != NULL)
			{
				int x, y;
				bool res = true;
				try
				{
					x = StrToInt(CSEd_x->Text);
					y = StrToInt(CSEd_y->Text);
				}
				catch(...)
				{
					res = false;
				}
				if(res)
				{
					Form_m->OnChangeObjPosition(mpTextObj, x, y);
				}
			}
		}
		CSEd_xEnter(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::CSEd_xKeyPress(TObject *Sender, wchar_t &Key)
{
	if(VK_RETURN == Key)
	{
		Key = 0;
		CSEd_xExit(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::CSEd_wEnter(TObject *Sender)
{
	mpEditField = Sender;
	mOldVal = ((TEdit*)Sender)->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::CSEd_wKeyPress(TObject *Sender, wchar_t &Key)
{
	if(VK_RETURN == Key)
	{
		Key = 0;
		CSEd_wExit(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::CSEd_wExit(TObject *Sender)
{
	UnicodeString str = ((TEdit*)Sender)->Text.Trim();

	if(Sender == mpEditField)
	{
		if (!mDisableChanges && mOldVal.Compare(str) != 0)
		{
			if (mpTextObj != NULL)
			{
				int w, h;
				bool res = true;
				try
				{
					w = StrToInt(CSEd_w->Text);
					h = StrToInt(CSEd_h->Text);
				}
				catch(...)
				{
					res = false;
				}
				if(res)
				{
					Form_m->OnChangeTextObjSizes(mpTextObj, w, h);
				}
			}
		}
		CSEd_wEnter(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::EditTextIdEnter(TObject *Sender)
{
	mpEditField = Sender;
	mOldVal = ((TEdit*)Sender)->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::ComboBox_TextIDExit(TObject *Sender)
{
	UnicodeString str = EditTextId->Text.Trim();

	if(Sender == mpEditField)
	{
		if (!mDisableChanges && mOldVal.Compare(str) != 0)
		{
			if (mpTextObj != NULL)
			{
				FillAttributes(str, true);
				CB_FontExit(NULL);
				ComboBoxHExit(NULL);
				mpEditField = CSEd_w;
				CSEd_wExit(CSEd_w);
				mpTextObj->SetTextId(str);
				Form_m->OnChangeTextId(mpTextObj);
			}
		}
		EditTextIdEnter(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::EditTextIdKeyPress(TObject *Sender, wchar_t &Key)
{
	if(VK_RETURN == Key)
	{
		Key = 0;
		ComboBox_TextIDExit(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::Bt_3dotClick(TObject *)
{
	UnicodeString str = EditTextId->Text.Trim();
	mOldVal = str;
	bool res = Form_m->TextMessagesManager(&str, true);
	if(res)
	{
		mpEditField = EditTextId;
		EditTextId->Text = str;
		ComboBox_TextIDExit(EditTextId);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::ComboBox_zoneChange(TObject *)
{
	ComboBox_zone->Hint = ComboBox_zone->Text;
	ComboBox_zoneExit(ComboBox_zone);
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::ComboBox_zoneExit(TObject *)
{
	if (mpTextObj == NULL)
	{
		return;
	}
	if (!mDisableChanges)
	{
		int idx = ComboBox_zone->ItemIndex;
		if (idx == ComboBox_zone->Items->Count - 1)
		{
			idx = -1;
        }
		mpTextObj->SetZone(idx);
		Form_m->OnChangeObjZone(mpTextObj);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::CB_FontExit(TObject *)
{
	if (mpTextObj == NULL)
	{
		return;
	}
	if (!mDisableChanges)
	{
		int idx = CB_Font->ItemIndex;
		if(idx >= 0)
		{
			mpTextObj->SetFontName(CB_Font->Text);
			Form_m->OnChangeTextId(mpTextObj);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::CB_FontChange(TObject *)
{
	CB_FontExit(ComboBox_zone);
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::ComboBoxHExit(TObject *)
{
	if (mpTextObj == NULL || ComboBoxH->ItemIndex < 0 || ComboBoxV->ItemIndex < 0)
	{
		return;
	}
	int ta = 0;
	if(ComboBoxH->ItemIndex == 0)
    {
    	ta = TEXT_ALIGNMENT_LEFT;
    }
    else
	if(ComboBoxH->ItemIndex == 1)
    {
    	ta = TEXT_ALIGNMENT_RIGHT;
    }
    else
	if(ComboBoxH->ItemIndex == 2)
    {
    	ta = TEXT_ALIGNMENT_HCENTER;
    }

	if(ComboBoxV->ItemIndex == 0)
    {
		ta |= TEXT_ALIGNMENT_TOP;
    }
    else
	if(ComboBoxV->ItemIndex == 1)
    {
    	ta |= TEXT_ALIGNMENT_BOTTOM;
    }
    else
	if(ComboBoxV->ItemIndex == 2)
    {
    	ta |= TEXT_ALIGNMENT_VCENTER;
	}
	if (!mDisableChanges)
	{
		mpTextObj->SetAlignment(static_cast<TextAlignment>(ta));
		Form_m->OnChangeTextId(mpTextObj);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::ComboBoxHChange(TObject *)
{
	ComboBoxHExit(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::SetRemarks(UnicodeString str)
{
	if (!Visible)
	{
		return;
    }
	Memo->Text = str;
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::SetTransform(const TTransformProperties& transform)
{
	if(!Visible)
	{
		return;
	}
	double x, y;
	transform.GetPosition(x, y);
	CSEd_x->Text = IntToStr(static_cast<int>(x));
	CSEd_y->Text = IntToStr(static_cast<int>(y));
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::SetZone(int zone)
{
	if (!Visible)
	{
		return;
	}
	if (zone < 0)
	{
		ComboBox_zone->ItemIndex = ComboBox_zone->Items->Count - 1;
	}
	else
	{
		ComboBox_zone->ItemIndex = zone;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::SetSizes(int w, int h)
{
	if (!Visible)
	{
		return;
    }
	CSEd_w->Text = IntToStr(w);
	CSEd_h->Text = IntToStr(h);
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::Bt_3dotColorClick(TObject *)
{
	ColorDialog->Color = ShapeColor->Brush->Color;
	if(ColorDialog->Execute())
	{
		ShapeColor->Brush->Color = ColorDialog->Color;
		CheckBoxColorClick(NULL);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::CheckBoxColorClick(TObject *)
{
	if (mpTextObj == NULL)
	{
		return;
	}
	if(CheckBoxColor->Checked)
	{
		if (!mDisableChanges)
		{
			mpTextObj->SetFillColor(ShapeColor->Brush->Color);
			Form_m->OnChangeTextId(mpTextObj);
		}
		Bt_3dotColor->Enabled = true;
	}
	else
	{
		if (!mDisableChanges)
		{
			mpTextObj->SetFillColor(clTRANSPARENT);
			Form_m->OnChangeTextId(mpTextObj);
		}
		Bt_3dotColor->Enabled = false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::FakeEnterActiveControl()
{
	TWinControl *c = ActiveControl;
	if (c == NULL)
	{
		return;
    }
	if (c->ClassNameIs(_T("TComboBox")))
	{
		if (((TComboBox*)c)->OnEnter != NULL)
		{
			((TComboBox*)c)->OnEnter(c);
		}
		return;
	}
	if (c->ClassNameIs(_T("TEdit")))
	{
		if (((TEdit*)c)->OnEnter != NULL)
		{
			((TEdit*)c)->OnEnter(c);
		}
		return;
	}
	if (c->ClassNameIs(_T("TMemo")))
	{
		if (((TMemo*)c)->OnEnter != NULL)
		{
			((TMemo*)c)->OnEnter(c);
		}
		return;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::SaveAllValues(TWinControl *iParent)
{
	if (iParent == NULL)
		return;

	TControl *c;
	for (int i = 0; i < iParent->ControlCount; i++)
	{
		c = iParent->Controls[i];
		if (c == NULL)
			continue;

		if (c->ClassNameIs(_T("TPanel")) ||
			c->ClassNameIs(_T("TGroupBox")) ||
			c->ClassNameIs(_T("TTabSheet")) ||
			c->ClassNameIs(_T("TPageControl")) ||
			c->ClassNameIs(_T("TScrollBox")) ||
			c->ClassNameIs(_T("TForm")))
		{
			SaveAllValues((TWinControl*)c);
		}
		else
		{
			if (c->ClassNameIs(_T("TEdit")))
			{
				if (((TEdit*)c)->OnExit != NULL)
					((TEdit*)c)->OnExit(c);
			}
			else if (c->ClassNameIs(_T("TMemo")))
			{
				if (((TMemo*)c)->OnExit != NULL)
					((TMemo*)c)->OnExit(c);
			}
			else if (c->ClassNameIs(_T("TComboBox")))
			{
				if (((TComboBox*)c)->OnExit != NULL)
					((TComboBox*)c)->OnExit(c);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::FormShow(TObject *)
{
	AssignTxtObj(mpTextObj);
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::FormHide(TObject *)
{
	mOldVal = _T("");
	SaveAllValues(this);
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::AssignTxtObj(TMapTextBox *Gmp)
{
	double x, y;
	if (mpTextObj == Gmp)
	{
		return;
	}

	if (mpTextObj != NULL)
	{
		if (Visible)
		{
			mOldVal = _T("");
			SaveAllValues(this);
		}
		mpTextObj->UnregisterPrpWnd();
	}

	mpTextObj = Gmp;
	if (mpTextObj != NULL)
	{
		mpTextObj->RegisterPrpWnd(this);
	}
	else
	{
		return;
	}

	Caption = Localization::Text(_T("mGameObjectProperties")) + " [" + mpTextObj->getGParentName() + "]";

	mDisableChanges = true;

	mpTextObj->getCoordinates(&x, &y);
	CSEd_x->Text = IntToStr((int)x);
	CSEd_y->Text = IntToStr((int)y);

	UnicodeString strid = _T("");
	if(Gmp->GetTextType() == TMapTextBox::MTBT_Static)
	{
		strid = Gmp->GetTextId();
    }
	FillAttributes(strid, false);

	mNameObj = mpTextObj->Name;
	mOldName = mpTextObj->Name;

	if (mpTextObj->GetZone() >= 0)
	{
		ComboBox_zone->ItemIndex = mpTextObj->GetZone();
	}
	else
	{
		ComboBox_zone->ItemIndex = ComboBox_zone->Items->Count - 1;
	}
	ComboBox_zone->Hint = ComboBox_zone->Text;

	Edit_name->Text = mpTextObj->Name;
	Memo->Clear();
	Memo->Text = mpTextObj->getRemarks();

	if(mpTextObj->GetTextType() == TMapTextBox::MTBT_Dynamic)
	{
		RBDynamicMode->Checked = true;
	}
	else
	{
		RBStaticMode->Checked = true;
    }
	RBStaticModeClick(NULL);

	Lock(mpTextObj->IsLocked());

	mDisableChanges = false;

	if (Visible)
	{
		FakeEnterActiveControl();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::FillAttributes(UnicodeString str, bool reset)
{
	int i;
	const TCommonTextManager *textmanager = Form_m->GetCommonTextManager();
	UnicodeString lang = Form_m->GetCurrentLanguage();
	const TUnicodeText *ftext = textmanager->GetText(str, lang);

	MemoStaticText->Lines->Clear();
	if(ftext != NULL)
	{
		if(ftext->GetTextData() != NULL)
		{
			for(int j = 0; j < ftext->GetTextData()->mStringsCount - 1; j++)
			{
				MemoStaticText->Lines->Add(UnicodeString());
			}
			for(int j = 0; j < ftext->GetTextData()->mStringsCount; j++)
			{
				wchar_t *a = ftext->GetTextData()->mppStrings[j];
				MemoStaticText->Lines->Strings[j] = UnicodeString(a);
			}
		}
    }

	if(ftext == NULL && reset == true)
	{
        return;
    }

	if(!reset)
	{
		int sc, cc;
		mpTextObj->GetDynamicTextSizes(sc, cc);
		EditDynamicTextStrings->Text = IntToStr(sc);
		EditDynamicTextMaxChars->Text = IntToStr(cc);
	}

	if(reset)
	{
		EditTextId->Text = str;
	}
	else
	{
		EditTextId->Text = mpTextObj->GetTextId();
	}

	if(reset)
	{
		int w, h;
		ftext->GetCanvasSize(w, h);
		CSEd_w->Text = IntToStr(w);
		CSEd_h->Text = IntToStr(h);
	}
	else
	{
		CSEd_w->Text = IntToStr((int)mpTextObj->GetTextWidth());
		CSEd_h->Text = IntToStr((int)mpTextObj->GetTextHeight());
	}
	CB_Font->ItemIndex = -1;
	if(CB_Font->Items->Count > 0)
	{
		if(reset)
		{
			lang = ftext->GetFontName();
		}
		else
		{
			lang = mpTextObj->GetFontName();
        }
		i = CB_Font->Items->IndexOf(lang);
		if(i >= 0)
		{
			CB_Font->ItemIndex = i;
		}
	}

	if(reset)
	{
		i = static_cast<int>(ftext->GetAttributes().mAlignment);
	}
	else
	{
		i = static_cast<int>(mpTextObj->GetAlignment());
    }
	if((i & TEXT_ALIGNMENT_HCENTER) == TEXT_ALIGNMENT_HCENTER)
    {
		ComboBoxH->ItemIndex = 2;
    }
    else
	if((i & TEXT_ALIGNMENT_LEFT) == TEXT_ALIGNMENT_LEFT)
    {
		ComboBoxH->ItemIndex = 0;
    }
    else
	if((i & TEXT_ALIGNMENT_RIGHT) == TEXT_ALIGNMENT_RIGHT)
    {
		ComboBoxH->ItemIndex = 1;
    }

	if((i & TEXT_ALIGNMENT_VCENTER) == TEXT_ALIGNMENT_VCENTER)
    {
		ComboBoxV->ItemIndex = 2;
    }
    else
	if((i & TEXT_ALIGNMENT_TOP) == TEXT_ALIGNMENT_TOP)
    {
		ComboBoxV->ItemIndex = 0;
    }
    else
	if((i & TEXT_ALIGNMENT_BOTTOM) == TEXT_ALIGNMENT_BOTTOM)
    {
		ComboBoxV->ItemIndex = 1;
	}

	TColor color;
	if(reset)
	{
		color = ftext->GetAttributes().mCanvasColor;
	}
	else
	{
		color = mpTextObj->GetFillColor();
    }
	if(color == clTRANSPARENT)
	{
		CheckBoxColor->Checked = false;
		ShapeColor->Brush->Color = clWhite;
	}
	else
	{
		CheckBoxColor->Checked = true;
		ShapeColor->Brush->Color = color;
	}
	CheckBoxColorClick(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::EditDynamicTextStringsExit(TObject *Sender)
{
	UnicodeString str = ((TEdit*)Sender)->Text.Trim();
	if (!mDisableChanges && mpTextObj)
	{
		int sc, cc;
		try
		{
			sc = StrToInt(EditDynamicTextStrings->Text);
			cc = StrToInt(EditDynamicTextMaxChars->Text);
		}
		catch(...)
		{
			return;
		}
		mpTextObj->SetDynamicTextSizes(sc, cc);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::Reset()
{
	AssignTxtObj(nullptr);
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::EmulatorMode(bool mode)
{
	mEmulatorMode = mode;
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::StatusBar1DrawPanel(TStatusBar *StatusBar, TStatusPanel *Panel, const TRect &Rect)
{
	(void)Panel;
	if(mpTextObj->IsLocked())
	{
		Form_m->ImageList1->Draw(StatusBar->Canvas, Rect.Left, Rect.Top, LOCK_IMAGE_IDX);
	}
	else
	{
		StatusBar->Canvas->Brush->Style = bsSolid;
		StatusBar->Canvas->Brush->Color = StatusBar->Color;
		StatusBar->Canvas->FillRect(Rect);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTxtObjProperty::Lock(bool value)
{
	(void)value;
	StatusBar1->Refresh();
}
//---------------------------------------------------------------------------

