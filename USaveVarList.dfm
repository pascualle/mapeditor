object FSaveVarList: TFSaveVarList
  Left = 332
  Top = 178
  Action = ActSelectAll
  BorderIcons = [biSystemMenu]
  ClientHeight = 529
  ClientWidth = 640
  Color = clBtnFace
  Constraints.MinHeight = 330
  Constraints.MinWidth = 400
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Pitch = fpFixed
  Font.Style = []
  KeyPreview = True
  Position = poMainFormCenter
  OnClick = ActSelectAllExecute
  OnShow = FormShow
  DesignSize = (
    640
    529)
  TextHeight = 13
  object Bt_save: TButton
    Left = 82
    Top = 480
    Width = 381
    Height = 25
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'mSaveToFile'
    TabOrder = 1
    OnClick = Bt_saveClick
    ExplicitTop = 279
  end
  object Bt_exit: TButton
    Left = 478
    Top = 480
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'mClose'
    ModalResult = 1
    TabOrder = 0
    ExplicitTop = 279
  end
  object Memo: TMemo
    Left = 8
    Top = 8
    Width = 621
    Height = 465
    TabStop = False
    Anchors = [akLeft, akTop, akRight, akBottom]
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 2
    WordWrap = False
    ExplicitHeight = 264
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 510
    Width = 640
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    ExplicitTop = 309
  end
  object SaveDialog: TSaveDialog
    Filter = 'C header file (*.h)|*.h'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 96
    Top = 16
  end
  object ActionList1: TActionList
    Left = 32
    Top = 16
    object ActSelectAll: TAction
      ShortCut = 16449
      Visible = False
      OnExecute = ActSelectAllExecute
    end
  end
end
