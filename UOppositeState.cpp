//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UOppositeState.h"
#include "UObjManager.h"
#include "UState.h"
#include "MainUnit.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFOppState *FOppState;
//---------------------------------------------------------------------------

__fastcall TFOppState::TFOppState(TComponent* Owner)
        : TForm(Owner)
{
	int i, len;
	len = Form_m->mainCommonRes->states->Count;
	for(i = 0; i < len; i++)
	{
		TState* s = (TState*)Form_m->mainCommonRes->states->Items[i];
		ComboBoxState->Items->Add(s->name);
	}
	ComboBoxInvState->Items->Assign(ComboBoxState->Items);
	if(len>0)
	{
		ComboBoxInvState->ItemIndex = 0;
		ComboBoxState->ItemIndex = 0;
	}
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFOppState::SetStateStrigCondition(UnicodeString str)
{
	int idx,len;
	idx = str.Pos(_T("="));
	len = str.Length();
	UnicodeString name = _T("");
	UnicodeString value = _T("");
	if(idx>0 && idx<len)
	{
		name = str.SubString(1,idx-1);
		value = str.SubString(idx+1,len-idx);
		idx = ComboBoxState->Items->IndexOf(name);
		if(idx>=0)
		{
			ComboBoxState->ItemIndex = idx;
			idx = ComboBoxInvState->Items->IndexOf(value);
			if(idx>=0)ComboBoxInvState->ItemIndex = idx;
		}
		if(idx<0)
		{
			ComboBoxInvState->ItemIndex = 0;
			ComboBoxState->ItemIndex = 0;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFOppState::GetStateStrigCondition(UnicodeString *str)
{
	*str = ComboBoxState->Text + _T("=") + ComboBoxInvState->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFOppState::ButtonOkClick(TObject */*Sender*/)
{
	if(ComboBoxInvState->ItemIndex == ComboBoxState->ItemIndex)
	{
		Application->MessageBox(Localization::Text(_T("mConditionError")).c_str(),
								Localization::Text(_T("mError")).c_str(),
								MB_OK | MB_ICONERROR);
	}
	else
	{
		ModalResult = mrOk;
	}
}
//---------------------------------------------------------------------------

