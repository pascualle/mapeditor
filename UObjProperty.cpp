//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UObjProperty.h"
#include "UScript.h"
#include "UState.h"
#include "MainUnit.h"
#include "UObjManager.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TFGOProperty *FGOProperty;

//---------------------------------------------------------------------------

__fastcall TFGOProperty::TFGOProperty(TComponent *Owner) : TPrpFunctions(Owner)
{
	inited = false;
	icon_show_list = false;
	emulator_mode = false;
	NameObj = _T("");
	disableChanges = true;
	exitwitherror = true;
	t_MapGP = NULL;
	mOldVal = _T("");
	mpCommonResObjManager = NULL;
	mpPropertiesControl = new TPropertiesControl(ScrollBox);
	mpEditField = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::Localize()
{
	Localization::Localize(this);
  	Localization::Localize(PopupMenu);
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::FormDestroy(TObject *)
{
	ClearScrollBox();
	delete mpPropertiesControl;
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::Init(TCommonResObjManager *resManager)
{
	int i;

	mpCommonResObjManager = resManager;
	assert(mpCommonResObjManager);
	vars = mpCommonResObjManager->GetVariablesMainGameObjPattern();

	ValueListEditor->Strings->Clear();
	for (i = 0; i < ::Actions::sptCOUNT; i++)
	{
		const UnicodeString str = ::Actions::Caption[i] + _T("=");
		ValueListEditor->Strings->Add(str);
		ValueListEditor->ItemProps[i]->EditStyle = esPickList;
		ValueListEditor->ItemProps[i]->PickList->Clear();
	}

	ClearScrollBox();
	mpPropertiesControl->Init(vars, TPropertiesControl::PCM_NORMAL);
	mpPropertiesControl->SetOnValidate(OnPropertiesControlValueValidate);

	ComboBox_zone->Items->Assign(Form_m->ListBoxZone->Items);

	inited = true;

	if (t_MapGP != NULL)
	{
		assignMapGobj(t_MapGP);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::refreshNodes()
{
	int len, y;
	len = Form_m->m_pObjManager->GetObjTypeCount(TObjManager::DIRECTOR);
	ComboBox_director->Items->Clear();
	for (y = 0; y < len; y++)
	{
		ComboBox_director->Items->Add(((TMapDirector*)Form_m->m_pObjManager->GetObjByIndex(TObjManager::DIRECTOR ,y))->Name);
    }
	ComboBox_director->Items->Insert(0, TEXT_STR_NONE);
	if (t_MapGP != NULL)
	{
		int idx = ComboBox_director->Items->IndexOf(t_MapGP->NextLinkedDirector);
		if (idx > 0)
		{
			ComboBox_director->ItemIndex = idx;
		}
		else
		{
			ComboBox_director->ItemIndex = 0;
        }
	}
	else
	{
		ComboBox_director->ItemIndex = 0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::refreshCplxList()
{
	refreshCplxTab(false);
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::refreshCplxTab(bool editMode)
{
	if (t_MapGP == NULL)
	{
		return;
	}
	int joinidx = 0;
	TEdit *lb = NULL;
	TBitBtn *bt = NULL;
	UnicodeString text;
	for(int s = 0; s < 3; s++)
	{
		switch(s)
		{
			case 0:
				joinidx = 0;
				lb = ED_CplxObjParent1;
				bt = Bt_find_own_node1;
			break;
			case 1:
				joinidx = 1;
				lb = ED_CplxObjParent2;
				bt = Bt_find_own_node2;
			break;;
			case 2:
				joinidx = 2;
				lb = ED_CplxObjParent3;
				bt = Bt_find_own_node3;
		}
		if(editMode)
		{
			text = lb->Text.Trim();
		}
		else
		{
			text = t_MapGP->GetChild(joinidx);
		}
		TMapPerson *tp = NULL;
		if(!text.IsEmpty())
		{
			for (int i = 0; i < Form_m->m_pObjManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT); i++)
			{
				tp = (TMapPerson*)Form_m->m_pObjManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, i);
				if(tp->Name.Compare(text) == 0)
				{
					break;
				}
				tp = NULL;
			}
		}
		if(tp != NULL)
		{
			if (tp == t_MapGP || tp->GetZone() != t_MapGP->GetZone())
			{
				if(t_MapGP != NULL)
				{
					t_MapGP->AddChild(joinidx, UnicodeString());
				}
				tp = NULL;
			}
		}
		else if(!text.IsEmpty())
		{
			if(t_MapGP != NULL)
			{
				t_MapGP->AddChild(joinidx, UnicodeString());
			}
		}
		if(tp != NULL)
		{
			lb->Text = text;
			bt->Enabled = true;
		}
		else
		{
			lb->Text = _T("");
			bt->Enabled = false;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ED_CplxObjParent1Exit(TObject *Sender)
{
	UnicodeString text = static_cast<TEdit*>(Sender)->Text.Trim();
	UnicodeString errmes = _T("\0");
	if(!text.IsEmpty())
	{
		TMapGPerson *mp = NULL;
		for (int i = 0; i < Form_m->m_pObjManager->GetObjTypeCount(TObjManager::CHARACTER_OBJECT); i++)
		{
			mp = (TMapGPerson*)Form_m->m_pObjManager->GetObjByIndex(TObjManager::CHARACTER_OBJECT, i);
			if(mp->Name.Compare(text) == 0)
			{
            	break;
            }
			mp = NULL;
		}
		if(mp != NULL)
		{
			if (mp == t_MapGP)
			{
				static_cast<TEdit*>(Sender)->Text = _T("\0");
            }
			if(mp->GetZone() != t_MapGP->GetZone())
			{
				errmes = Localization::Text(_T("mObjZoneWarning"));
				static_cast<TEdit*>(Sender)->Text = _T("\0");
            }
		}
		else
		{
			errmes = Localization::Text(_T("mObjFindWarning"));
			static_cast<TEdit*>(Sender)->Text = _T("\0");
		}
		if(!errmes.IsEmpty())
		{
			Application->MessageBox(errmes.c_str(), Localization::Text(_T("mError")).c_str(), MB_ICONERROR);
		}
		refreshCplxTab(true);
    }
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ED_CplxObjParent1KeyPress(TObject *Sender, wchar_t &Key)
{
	if(Key == VK_RETURN)
	{
		Key = 0;
		ED_CplxObjParent1Exit(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ButtonJoinClick(TObject *)
{
	if(!disableChanges)
	if(t_MapGP != NULL)
	{
		int joinidx = 0;
		TEdit *lb = NULL;
		for(int s = 0; s < 3; s++)
		{
			switch(s)
			{
				case 0:
					joinidx = 0;
					lb = ED_CplxObjParent1;
				break;
				case 1:
					joinidx = 1;
					lb = ED_CplxObjParent2;
				break;
				case 2:
					joinidx = 2;
					lb = ED_CplxObjParent3;
			}
			UnicodeString oldName = t_MapGP->GetChild(joinidx);
			UnicodeString newName = lb->Text.Trim();
			if (!oldName.IsEmpty())
			{
				if(oldName.Compare(newName) != 0)
				{
					TMapGPerson *op = (TMapGPerson*)Form_m->FindObjectPointer(oldName);
					if (op != NULL)
					{
						op->ParentObj = _T("");
						Form_m->m_pObjManager->ClearCasheFor(op);
					}
				}
				if(newName.IsEmpty())
				{
					t_MapGP->AddChild(joinidx, UnicodeString());
                }
			}
			if (!newName.IsEmpty())
			{
				TMapGPerson *op = (TMapGPerson*)Form_m->FindObjectPointer(newName);
				if(op->GetObjType() == objMAPSOUNDSCHEME ||
					op->GetObjType() == objMAPOBJPOOL)
				{
                	op = NULL;
                }
				if (op != NULL)
				{
                    double x, y;
					t_MapGP->AddChild(joinidx, newName);
					op->ParentObj = t_MapGP->Name;
					op->SetZone(t_MapGP->GetZone());
					Form_m->m_pObjManager->ClearCasheFor(op);
					Form_m->m_pObjManager->ClearCasheFor(t_MapGP);
					t_MapGP->getCoordinates(&x, &y);
					t_MapGP->setCoordinates(x, y, 0, 0);
					//����������� ��� childs �������� ������ �� ��������� � ������
					Form_m->m_pObjManager->OrderChildsBeforeMain(t_MapGP);
				}
				else
				{
					t_MapGP->AddChild(joinidx, UnicodeString());
				}
			}
		}
		refreshCplxList();
		Form_m->OnChangeCplxObjList(t_MapGP, _T(""));
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ButtonDisconnectClick(TObject *)
{
	if(!disableChanges)
	if(t_MapGP != NULL)
	{
		UnicodeString oldParentName = t_MapGP->ParentObj;
		refreshCplxList();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::refresStates()
{
	TState *state;
	CB_dir->Clear();
	CB_dir->Items->Add(TEXT_STR_NONE);

	for (int i = 0; i < Form_m->mainCommonRes->states->Count; i++)
	{
		state = (TState*)Form_m->mainCommonRes->states->Items[i];
		CB_dir->AddItem(state->name, (TObject*)state->direction);
	}

	if (t_MapGP != NULL)
	{
		int idx = CB_dir->Items->IndexOf(t_MapGP->State_Name);
		if (idx > 0)
			CB_dir->ItemIndex = idx;
		else
			CB_dir->ItemIndex = 0;
	}
	else
	{
		CB_dir->ItemIndex = 0;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::EmulatorMode(bool mode)
{
	emulator_mode = mode;
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::FormShow(TObject * /* Sender */ )
{
	assignMapGobj(t_MapGP);
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::FormHide(TObject * /* Sender */ )
{
	if(exitwitherror)
	{
    	return;
	}
	mOldVal = _T("");
	SaveAllValues(this);
}
//---------------------------------------------------------------------------

bool __fastcall TFGOProperty::assignMapGobj(TMapGPerson *Gmp)
{
	double x, y;
	int i, len;
	UnicodeString caption;
	bool find;
	TGScript *spt;

	exitwitherror = false;

	if (t_MapGP == Gmp)
	{
		return true;
	}

	if (t_MapGP != NULL)
	{
		if (Visible)
		{
			mOldVal = _T("");
			SaveAllValues(this);
		}
		t_MapGP->UnregisterPrpWnd();
	}

	t_MapGP = Gmp;
	if (t_MapGP != NULL)
	{
		t_MapGP->RegisterPrpWnd(this);
	}
	else
	{
		return true;
	}

	Caption = Localization::Text(_T("mGameObjectProperties")) + _T(" [") + t_MapGP->getGParentName() + _T("]");

	disableChanges = true;

	{
		t_MapGP->getCoordinates(&x, &y);
		CSEd_x->Text = IntToStr((int)x);
		CSEd_y->Text = IntToStr((int)y);
		caption = _T("");

		NameObj = t_MapGP->Name;
		mOldName = t_MapGP->Name;

		if (t_MapGP->GetZone() >= 0)
		{
			ComboBox_zone->ItemIndex = t_MapGP->GetZone();
		}
		else
		{
			ComboBox_zone->ItemIndex = ComboBox_zone->Items->Count - 1;
		}
		ComboBox_zone->Hint = ComboBox_zone->Text;

		Edit_name->Text = t_MapGP->Name;
		Memo->Clear();
		Memo->Text = t_MapGP->getRemarks();

		ComboBox_director->Enabled = true;
		CB_dir->Enabled = true;
		Bt_find->Enabled = true;
		ScrollBox->Visible = true;
		TS_script->Enabled = true;
		TS_script->TabVisible = true;
		ScrollBox->Enabled = true;
		ScrollBox->Visible = true;
		ComboBox_zone->Enabled = true;
		TabComplexObj->Enabled = true;
		TabComplexObj->TabVisible = true;
		ED_CplxObjParent1->Text = _T("");
		ED_CplxObjParent2->Text = _T("");
		ED_CplxObjParent3->Text = _T("");
		CSEd_x->Enabled = true;
		CSEd_y->Enabled = true;
		bool decor = false;
		if (!t_MapGP->ParentObj.IsEmpty())
		{
			ComboBox_director->Enabled = false;
			Bt_find->Enabled = false;
			TS_script->Enabled = true;
			TS_script->TabVisible = true;
			ComboBox_zone->Enabled = false;
			CSEd_x->Enabled = false;
			CSEd_y->Enabled = false;
		}
		else if (t_MapGP->GetDecorType() != decorNONE)
		{
			ComboBox_director->Enabled = false;
			Bt_find->Enabled = false;
			ScrollBox->Enabled = false;
			ScrollBox->Visible = false;
			TS_script->Enabled = false;
			TS_script->TabVisible = false;
			TabComplexObj->Enabled = false;
			TabComplexObj->TabVisible = false;
			refreshNodes();
			refresStates();
			CB_dirChange(NULL);
			decor = true;
		}

		if(!decor)
		{
			if (emulator_mode)
			{
				GroupBox2->Enabled = false;
			}
			else
			{
				GroupBox2->Enabled = true;
			}
			refreshCplxList();
			refreshVars();
			refreshNodes();
			refreshScripts();
			refresStates();
			CB_dirChange(NULL);
		}

		Lock(t_MapGP->IsLocked());
	}

	disableChanges = false;

	if (Visible)
	{
		FakeEnterActiveControl();
	}

	return true;
}
//---------------------------------------------------------------------------

int __fastcall TFGOProperty::GetScriptType(int idx)
{
	int type = -1;
	switch(idx)
	{
	case ::Actions::
		sptONCOLLIDE : type = ::Actions::sptONCOLLIDE;
		break;

	case ::Actions::
		sptONTILECOLLIDE : type = ::Actions::sptONTILECOLLIDE;
		break;

	case ::Actions::
		sptONCHANGENODE : type = ::Actions::sptONCHANGENODE;
		break;

	case ::Actions::
		sptONCHANGE1 :
	case ::Actions::
		sptONCHANGE2 :
	case ::Actions::
		sptONCHANGE3 : type = ::Actions::sptONCHANGE1;
		break;

	case ::Actions::
		sptONENDANIM : type = ::Actions::sptONENDANIM;
	}
	return type;
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ClearScrollBox()
{
	mpPropertiesControl->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::refreshVars()
{
	if (!inited)
		return;
	if (t_MapGP == NULL)
		return;

	std::list<PropertyItem>::const_iterator iv;
	for (iv = vars->begin(); iv != vars->end(); ++iv)
	{
		double val;
		if (!t_MapGP->GetVarValue(&val, iv->name))
		{
			if (!t_MapGP->GetDefValue(&val, iv->name))
			{
				val = 0.0f;
			}
		}
		mpPropertiesControl->SetValue(val, iv->name);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::refreshScripts()
{
	/*
	int y, x, len, i;
	bool find;
	UnicodeString caption;
	TGScript *spt;

	if (t_MapGP == NULL)
		return;

	x = ValueListEditor->Strings->Count;
	len = Form_m->m_pObjManager->GetScriptsCount();
	for (y = 0; y < x; y++)
	{
		find = false;
		caption = t_MapGP->getScriptName(UnicodeString(::Actions::Names[y]));
		if (!caption.IsEmpty())
			for (i = 0; i < len; i++)
			{
				spt = Form_m->m_pObjManager->GetScriptByIndex(i);
				if (spt->Name.Compare(caption) == 0)
				{
					if (spt->Mode == ::Actions::sptONENDANIM)
					{
						ValueListEditor->Strings->Strings[y] = ::Actions::Caption[y] + _T("=") + caption;
					}
					else
					{
						if (GetScriptType(y) != spt->Mode)
							caption = _T("");
						ValueListEditor->Strings->Strings[y] = ::Actions::Caption[y] + _T("=") + caption;
					}
					find = true;
					break;
				}
			}

		if (!find)
		{
			ValueListEditor->Strings->Strings[y] = ::Actions::Caption[y] + _T("=");
		}
	}
    */
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ScrollBoxResize(TObject * /* Sender */ )
{
	ScrollBox->Update();
}
//---------------------------------------------------------------------------

//���� �����
void __fastcall TFGOProperty::Edit_nameChange(TObject * /* Sender */ )
{
	Edit_nameChangeMain(Edit_name);
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::Edit_nameEnter(TObject * /* Sender */ )
{
	mOldName = Edit_name->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::Edit_nameExit(TObject * /* Sender */ )
{
	if (t_MapGP == NULL)
	{
		return;
	}

	const UnicodeString str = Edit_name->Text.Trim();
	if(str.Compare(mOldName) == 0)
	{
        return;
    }

	if (str.IsEmpty())
	{
		Application->MessageBox(Localization::Text(_T("mObjNameError")).c_str(),
								Localization::Text(_T("mError")).c_str(),
								MB_OK | MB_ICONERROR);
		Edit_name->Text = mOldName;
		return;
	}
	if (Form_m->m_pObjManager->CheckIfMGONameExists(str, t_MapGP) == true)
	{
		Application->MessageBox(Localization::Text(_T("mObjNameExist")).c_str(),
								Localization::Text(_T("mError")).c_str(),
								MB_OK | MB_ICONERROR);
		Edit_name->Text = mOldName;
		return;
	}
	if (!disableChanges && mOldName.Compare(str) != 0)
	{
		t_MapGP->Name = str;
		NameObj = str;
		Form_m->OnChangeObjName(t_MapGP, mOldName);
		t_MapGP->changeHint();
		mOldName = str;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::Edit_nameKeyPress(TObject * /* Sender */ , char &Key)
{
	if (VK_RETURN == Key)
	{
		Key = 0;
		Edit_nameExit(NULL);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ValueListEditorGetPickList(TObject * /* Sender */ , const UnicodeString KeyName, TStrings *Values)
{
	int idx, i, type;
	TGScript *spt;
	idx = -1;

	if (KeyName.IsEmpty())
	{
		Values->Clear();
		return;
	}

	if (KeyName[1] == '-')
		return;

	for (i = 0; i < ::Actions::sptCOUNT; i++)
	{
		if(::Actions::Caption[i].Compare(KeyName) == 0)
		{
			idx = i;
			break;
		}
	}
	Values->Clear();

	/*
	type = GetScriptType(idx);
	for (i = 0; i < Form_m->m_pObjManager->GetScriptsCount(); i++)
	{
		spt = Form_m->m_pObjManager->GetScriptByIndex(i);
		if (spt->Mode == type || spt->Mode == ::Actions::sptONENDANIM)
			if (spt->Zone == t_MapGP->GetZone() || (t_MapGP->GetZone() != LEVELS_INDEPENDENT_IDX && spt->Zone == ZONES_INDEPENDENT_IDX) || spt->Zone == LEVELS_INDEPENDENT_IDX)
			{
				Values->Add(spt->Name);
			}
	}
    */
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ValueListEditorValidate(TObject * /* Sender */ , int /* ACol */ , int /* ARow */ , const UnicodeString KeyName, const UnicodeString KeyValue)
{
	int idx, i, type;
	TGScript *spt;
	idx = -1;

	if (KeyName.IsEmpty())
	{
		return;
	}

	for (i = 0; i < ::Actions::sptCOUNT; i++)
		if(::Actions::Caption[i].Compare(KeyName) == 0)
		{
			idx = i;
			break;
		}
	type = GetScriptType(idx);

	/*
	if (KeyName[1] != '-')
	{
		for (i = 0; i < Form_m->m_pObjManager->GetScriptsCount(); i++)
		{
			spt = Form_m->m_pObjManager->GetScriptByIndex(i);
			if (spt->Mode == type || spt->Mode == ::Actions::sptONENDANIM)
				if (spt->Name.Compare(KeyValue) == 0)
				{
					ValueListEditor->Values[KeyName] = KeyValue;
					if (!disableChanges)
						if (t_MapGP != NULL)
							t_MapGP->setScriptName(::Actions::Names[idx], KeyValue);
					return;
				}
		}
	}
    */

	ValueListEditor->Values[KeyName] = _T("");
	if (!disableChanges)
		if (t_MapGP != NULL)
			t_MapGP->setScriptName(::Actions::Names[idx], _T(""));
}

//---------------------------------------------------------------------------
void __fastcall TFGOProperty::ValueListEditorExit(TObject * /* Sender */ )
{
	if (!disableChanges)
		if (t_MapGP != NULL)
		{
			int x, i, k, len;
			UnicodeString caption;
			TGScript *spt;
			t_MapGP->clearScripts();
			len = ValueListEditor->Strings->Count;
			x = Form_m->m_pObjManager->GetScriptsCount();
			/*
			for (i = 0; i < len; i++)
			{
				caption = ValueListEditor->Strings->ValueFromIndex[i];
				if (!caption.IsEmpty())
					for (k = 0; k < x; k++)
					{
						spt = (TGScript*)Form_m->m_pObjManager->GetScriptByIndex(k);
						if (spt->Name.Compare(caption) == 0)
						{
							if (spt->Mode == ::Actions::sptONENDANIM)
							{
								t_MapGP->setScriptName(::Actions::Names[i], caption);
							}
							else
							{
								if (GetScriptType(i) != spt->Mode)
									caption = _T("");
								t_MapGP->setScriptName(::Actions::Names[i], caption);
							}
							break;
						}
					}
			}
            */
		}
}
//---------------------------------------------------------------------------

//STATES-DIRECTORS
void __fastcall TFGOProperty::CB_dirChange(TObject * /* Sender */ )
{
	Graphics::TBitmap *bmp;

	CB_dirExit(CB_dir);

	if (CB_dir->ItemIndex > 0)
	{
		bmp = new Graphics::TBitmap;
		Form_m->ImageListDir->GetBitmap((int)CB_dir->Items->Objects[CB_dir->ItemIndex], bmp);
		Image->Picture->Bitmap->Assign(bmp);
		delete bmp;
	}
	else
	{
		Image->Picture->Bitmap->Assign(NULL);
	}
	if (!disableChanges)
		Form_m->OnChangeObjParameter();
}

//---------------------------------------------------------------------------
void __fastcall TFGOProperty::CB_dirExit(TObject * /* Sender */ )
{
	if (t_MapGP != NULL)
		if (!disableChanges)
		{
			if (CB_dir->ItemIndex > 0)
			{
				t_MapGP->State_Name = CB_dir->Text;
			}
			else
			{
				t_MapGP->State_Name = _T("");
			}
		}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ComboBox_zoneChange(TObject * /* Sender */ )
{
	ComboBox_zone->Hint = ComboBox_zone->Text;
	ComboBox_zoneExit(ComboBox_zone);
}

//---------------------------------------------------------------------------
void __fastcall TFGOProperty::ComboBox_zoneExit(TObject * /* Sender */ )
{
	if (t_MapGP == NULL)
		return;
	if (!disableChanges)
	{
		int idx = ComboBox_zone->ItemIndex;
		if (idx == ComboBox_zone->Items->Count - 1)
			idx = -1;
		t_MapGP->SetZone(idx);

   /* for(int i=0; i<t_MapGP->Childs->Count; i++)
		   {
			 TMapGPerson* mp = (TMapGPerson*)Form_m->FindObjectPointer(t_MapGP->Childs->Strings[i]);
			 if(mp != NULL)
				mp->zoneIndex = t_MapGP->zoneIndex;
		   } */
		Form_m->OnChangeObjZone(t_MapGP);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ComboBox_directorExit(TObject *Sender)
{
	if (t_MapGP == NULL)
		return;

	int pos;
	TComboBox *cb = (TComboBox*)Sender;
	if ((pos = cb->Items->IndexOf(cb->Text.Trim())) < 0)
	{
		ComboBox_director->ItemIndex = 0;
	}
	else
	{
		ComboBox_director->ItemIndex = pos;
	}

	if (ComboBox_director->ItemIndex > 0)
	{
		t_MapGP->NextLinkedDirector = ComboBox_director->Text;
	}
	else
	{
		t_MapGP->NextLinkedDirector = _T("");
	}

	t_MapGP->ResetPrevTransform();
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ComboBox_directorKeyPress(TObject *Sender, char &Key)
{
	if (Key == VK_RETURN)
	{
		TComboBox *cb = (TComboBox*)Sender;
		cb->OnExit(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ImageClick(TObject * /* Sender */ )
{
	if (!CB_dir->Text.IsEmpty())
		ShowStatePreview(CB_dir->Text);
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ShowStatePreview(UnicodeString name)
{
	int i;
	TState *st;
	bool res;
	res = false;
	for (i = 0; i < Form_m->mainCommonRes->states->Count; i++)
	{
		st = (TState*)Form_m->mainCommonRes->states->Items[i];
		if (st->name == name)
		{
			res = true;
			break;
		}
	}
	if (res)
	{
		Application->CreateForm(__classid(TFEditState), &FEditState);
		FEditState->SetState(st); //������������ � ������� ��������
		FEditState->GroupBoxMovement->Enabled = false;
		FEditState->GroupBoxAnimation->Enabled = false;
		FEditState->EditPrefix->Enabled = false;
		FEditState->Ed_name->Enabled = false;
		FEditState->Button->Enabled = false;
		FEditState->ShowModal();
		FEditState->Free();
		FEditState = NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::Bt_findClick(TObject * /* Sender */ )
{
	int idx = ComboBox_director->ItemIndex;
	if (idx <= 0)
		return;
	Form_m->FindAndShowObjectByName(ComboBox_director->Text, false);
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::BitBtn_find_objClick(TObject * /* Sender */ )
{
	Form_m->FindAndShowObjectByName(NameObj, false);
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::Bt_find_parentClick(TObject *Sender)
{
	if(t_MapGP == NULL)
	{
		return;
	}
	UnicodeString text = _T("");
	if(Sender == Bt_find_own_node1)
	{
		text = ED_CplxObjParent1->Text.Trim();
	}
	if(Sender == Bt_find_own_node2)
	{
		text = ED_CplxObjParent2->Text.Trim();
	}
	if(Sender == Bt_find_own_node3)
	{
		text = ED_CplxObjParent3->Text.Trim();
	}
	Form_m->FindAndShowObjectByName(text, false);
}
//---------------------------------------------------------------------------

//������� ����� ������
void __fastcall TFGOProperty::N1Click(TObject *)
{
	if (ValueListEditor->Row <= 0 || !ValueListEditor->Focused())
	{
		return;
	}
	EditScript(mAdd);
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::N2Click(TObject *)
{
	if (ValueListEditor->Row <= 0 || !ValueListEditor->Focused())
	{
		return;
    }
	EditScript(mEdit);
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::EditScript(int mode)
{
	/*
	int i, idx, pos;
	UnicodeString oldName;
	UnicodeString KeyName = ValueListEditor->Keys[ValueListEditor->Row];

	if (t_MapGP == NULL)
		return;

	if (KeyName.IsEmpty())
		return;
	if (KeyName[1] == '-')
		return;
	if (Form_m->ActStartEmul->Checked)
		return;

	if (Form_m->m_pObjManager->GetScriptsCount() >= MAX_SCRIPTS && mode == mAdd)
	{
		Application->MessageBox(Localization::Text(_T("mMaximumScriptsExceeded")).c_str(),
								Localization::Text(_T("mError")).c_str(),
								MB_OK | MB_ICONERROR);
		return;
	}

	for (i = 0; i < ::Actions::sptCOUNT; i++)
	{
		if(::Actions::Caption[i].Compare(KeyName) == 0)
		{
			TGScript *spt = new TGScript();
			if (mode == mEdit)
			{
				idx = Form_m->m_pObjManager->GetScriptID(ValueListEditor->Values[KeyName]);
				if (idx < 0)
				{
					delete spt;
					return;
				}
				(Form_m->m_pObjManager->GetScriptByIndex(idx))->CopyTo(spt);
				oldName = spt->Name;
			}
			else
			{
				idx = -1;
				spt->Zone = t_MapGP->GetZone();
				spt->Mode = (char)GetScriptType(i);
			}
			Application->CreateForm(__classid(TFScript), &FScript);
			FScript->ActiveOnlyZone(spt->Zone < 0 ? t_MapGP->GetZone() : spt->Zone);
			FScript->ActiveOnlyMode(spt->Mode == ::Actions::sptONENDANIM ? i : spt->Mode);
			FScript->SetScript(spt);
			bool res;
			do
			{
				res = true;
				if (FScript->ShowModal() == mrOk)
				{
					FScript->GetScript(spt);
					if (spt->Name.IsEmpty())
					{
						Application->MessageBox(Localization::Text(_T("mScriptNameError")).c_str(),
												Localization::Text(_T("mError")).c_str(),
												MB_OK | MB_ICONERROR);
						res = false;
					}
					else
					{
						for (pos = 0; pos < Form_m->m_pObjManager->GetScriptsCount(); pos++)
							if (((TGScript*)Form_m->m_pObjManager->GetScriptByIndex(pos))->Name.Compare(spt->Name) == 0 && pos != idx)
							{
								Application->MessageBox(Localization::Text(_T("mScriptNameExist")).c_str(),
														Localization::Text(_T("mError")).c_str(),
														MB_OK | MB_ICONERROR);
								res = false;
								break;
							}
					}
					if (res)
					{
						if (mode == mAdd)
						{
							Form_m->m_pObjManager->CreateScript(spt);
							Form_m->AddGlobalScriptsToAllLevels(false);
						}
						else if (mode == mEdit)
						{
							spt->CopyTo((Form_m->m_pObjManager->GetScriptByIndex(idx)));
							Form_m->m_pObjManager->ChangeScriptNameInAllObjects(oldName, spt->Name, spt->Zone, spt->Mode);
							for (pos = 0; pos < ::Actions::sptCOUNT; pos++)
							{
								UnicodeString scr = ValueListEditor->Strings->ValueFromIndex[pos];
								if (scr.Compare(oldName) == 0 && !scr.IsEmpty())
								{
									ValueListEditor->Strings->ValueFromIndex[pos] = spt->Name;
                                }
							}
							Form_m->AddGlobalScriptsToAllLevels(false);
						}
						ValueListEditor->Strings->Values[KeyName] = spt->Name;
					}
				}
				else
				{
					res = true;
                }
			}
			while (!res);
			FScript->Free();
			FScript = NULL;
			if (spt != NULL)
			{
				delete spt;
            }
			Form_m->OnChangeObjParameter();
			break;
		}
	}
    */
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ValueListEditorMouseDown(TObject * /* Sender */ , TMouseButton Button, TShiftState /* Shift */ , int /* X */ , int Y)
{
	if (Form_m->ActStartEmul->Checked)
		return;

	if (Button == mbRight)
	{
		int h = ValueListEditor->RowCount * ValueListEditor->DefaultRowHeight + ValueListEditor->RowCount * ValueListEditor->GridLineWidth;
		if (Y > ValueListEditor->DefaultRowHeight && Y < h)
		{
			ValueListEditor->Row = Y / (ValueListEditor->DefaultRowHeight + ValueListEditor->GridLineWidth);
			TPoint p;
			GetCursorPos(&p);
			PopupMenu->Popup(p.x, p.y);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::ValueListEditorKeyDown(TObject * /* Sender */ , WORD &Key, TShiftState Shift)
{
	if (Key == VK_INSERT)
	{
		Key = 0;
		N1->Click();
	}
	else if (Shift.Contains(ssCtrl) && (Key == 'e' || Key == 'E'))
	{
		Key = 0;
		N2->Click();
	}
}
//---------------------------------------------------------------------------

//��� ����� ��������� -- ��������� ���������� �������
void __fastcall TFGOProperty::CSEd_x1Change(TObject *Sender)
{
	if (((TEdit*)Sender)->Text.IsEmpty())
		return;
	try
	{
		StrToInt(((TEdit*)Sender)->Text);
	}
	catch(...)
	{
		return;
	}

	if (!disableChanges)
		if (t_MapGP != NULL && mpEditField == Sender)
		{
			int x, y;
			bool res = true;
			try
			{
				x = StrToInt(CSEd_x->Text);
				y = StrToInt(CSEd_y->Text);
			}
			catch(...)
			{
            	res = false;
			}
			if(res)
			{
				Form_m->OnChangeObjPosition(t_MapGP, x, y);
            }
		}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::CSEd_x1Enter(TObject *Sender)
{
	mpEditField = Sender;
	mOldVal = ((TEdit*)Sender)->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::CSEd_x1Exit(TObject *Sender)
{
	UnicodeString str = ((TEdit*)Sender)->Text.Trim();

	if (mOldVal.IsEmpty())
		return;

	if (mOldVal.Compare(str) != 0)
		if (t_MapGP != NULL)
		{
			int x, y;
			bool res = true;
			try
			{
				x = StrToInt(CSEd_x->Text);
				y = StrToInt(CSEd_y->Text);
			}
			catch(...)
			{
            	res = false;
			}
			if(res)
			{
				Form_m->OnChangeObjPosition(t_MapGP, x, y);
            }
		}

	mpEditField = NULL;
}
//---------------------------------------------------------------------------

//����������
void __fastcall TFGOProperty::MemoEnter(TObject *)
{
	mOldVal = Memo->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::MemoExit(TObject *)
{
	if (!disableChanges)
		if (t_MapGP != NULL)
		{
			UnicodeString str = Memo->Text.Trim();
			if (mOldVal.Compare(str) != 0)
			{
				t_MapGP->setRemarks(str);
				Form_m->OnChangeObjParameter();
			}
		}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::SetRemarks(UnicodeString str)
{
	if (!Visible)
		return;
	Memo->Text = str;
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::SetTransform(const TTransformProperties& transform)
{
	if(!Visible)
	{
		return;
	}
	double x, y;
	transform.GetPosition(x, y);
	CSEd_x->Text = IntToStr(static_cast<int>(x));
	CSEd_y->Text = IntToStr(static_cast<int>(y));
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::SetVariables(const GPersonProperties *)
{
	if (!Visible)
		return;
	refreshVars();
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::SetScript(UnicodeString KeyName, UnicodeString ScriptName)
{
	if (!Visible)
		return;
	refreshScripts();
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::SetNode(UnicodeString NodeName)
{
	if (!Visible)
		return;
	int idx = ComboBox_director->Items->IndexOf(NodeName);
	if (idx < 0)
		idx = 0;
	ComboBox_director->ItemIndex = idx;
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::SetZone(int zone)
{
	if (!Visible)
		return;
	if (zone < 0)
		ComboBox_zone->ItemIndex = ComboBox_zone->Items->Count - 1;
	else
		ComboBox_zone->ItemIndex = zone;
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::SetState(UnicodeString name)
{
	if (!Visible)
		return;
	int idx = CB_dir->Items->IndexOf(name);
	if (idx < 0)
		idx = 0;
	CB_dir->ItemIndex = idx;
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::OnPropertiesControlValueValidate(const UnicodeString name, double val)
{
	if (!disableChanges && t_MapGP != NULL)
	{
		t_MapGP->SetVarValue(val, name);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::SetVarValue(double val, UnicodeString var_name)
{
	if (!inited || t_MapGP == NULL)
	{
		return;
	}
	mpPropertiesControl->SetValue(val, var_name);
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::FakeEnterActiveControl()
{
	TWinControl *c = ActiveControl;
	if (c == NULL)
		return;
	if (c->ClassNameIs(_T("TSpinEdit")))
	{
		if (((TSpinEdit*)c)->OnEnter != NULL)
		{
			((TSpinEdit*)c)->OnEnter(c);
        }
		return;
	}
	if (c->ClassNameIs(_T("TEdit")))
	{
		if (((TEdit*)c)->OnEnter != NULL)
		{
			((TEdit*)c)->OnEnter(c);
		}
		return;
	}
	if (c->ClassNameIs(_T("TMemo")))
	{
		if (((TMemo*)c)->OnEnter != NULL)
		{
			((TMemo*)c)->OnEnter(c);
        }
		return;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::SaveAllValues(TWinControl *iParent)
{
	if (iParent == NULL || exitwitherror)
		return;

	TControl *c;
	for (int i = 0; i < iParent->ControlCount; i++)
	{
		c = iParent->Controls[i];
		if (c == NULL)
			continue;

		if (c->ClassNameIs(_T("TPanel")) ||
				c->ClassNameIs(_T("TGroupBox")) ||
				c->ClassNameIs(_T("TTabSheet")) ||
				c->ClassNameIs(_T("TPageControl")) ||
				c->ClassNameIs(_T("TScrollBox")) ||
				c->ClassNameIs(_T("TForm")))
		{
			SaveAllValues((TWinControl*)c);
		}
		else
		{
			if (c->ClassNameIs(_T("TSpinEdit")))
			{
				if (((TSpinEdit*)c)->OnExit != NULL)
					((TSpinEdit*)c)->OnExit(c);
			}
			else if (c->ClassNameIs(_T("TEdit")))
			{
				if (((TEdit*)c)->OnExit != NULL)
					((TEdit*)c)->OnExit(c);
			}
			else if (c->ClassNameIs(_T("TMemo")))
			{
				if (((TMemo*)c)->OnExit != NULL)
					((TMemo*)c)->OnExit(c);
			}
			else if (c->ClassNameIs(_T("TComboBox")))
			{
				if (((TComboBox*)c)->OnExit != NULL)
					((TComboBox*)c)->OnExit(c);
			}
			if (c->ClassNameIs(_T("TValueListEditor")))
			{
				((TValueListEditor*)c)->EditorMode = false;

				if (((TValueListEditor*)c)->OnExit != NULL)
					((TValueListEditor*)c)->OnExit(c);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::Reset()
{
	assignMapGobj(nullptr);
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::Lock(bool value)
{
	(void)value;
	StatusBar1->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall TFGOProperty::StatusBar1DrawPanel(TStatusBar *StatusBar, TStatusPanel *Panel, const TRect &Rect)
{
	(void)Panel;
	if(t_MapGP->IsLocked())
	{
		Form_m->ImageList1->Draw(StatusBar->Canvas, Rect.Left, Rect.Top, LOCK_IMAGE_IDX);
	}
	else
	{
		StatusBar->Canvas->Brush->Style = bsSolid;
		StatusBar->Canvas->Brush->Color = StatusBar->Color;
		StatusBar->Canvas->FillRect(Rect);
    }
}
//---------------------------------------------------------------------------

