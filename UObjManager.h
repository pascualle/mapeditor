//---------------------------------------------------------------------------

#ifndef UObjManagerH
#define UObjManagerH
//---------------------------------------------------------------------------

#include "ExtCtrls.hpp"
#include "UObjCode.h"
#include "UMapCollideLines.h"
#include <hash_map>
#include "UTypes.h"
#include "texts.h"
#include <map>
#include <vector>

struct  TLMBtn;
struct  TEGrWndParams;
class   TMapGPerson;
class   TMapPerson;
class   GPerson;
class   TObjPattern;
class   TFEditGrObj;
class   TPlatformGameField;
class   TState;
class   TMapPersonCasheData;
class   TMapCollideNode;
class   TMapCollideLine;
class   TMapDirector;
class   TMapPerson;
class   BMPImage;
class   TImageFont;
class	TAnimation;
class   TMapTextBox;
class   TGScript;
class   TMapTrigger;
class   TMapDirector;

namespace Vcl
{
	namespace Categorybuttons
	{
		class   TButtonItem;
	}
}
//---------------------------------------------------------------------------

//��������� �������� TFrameObj ��� ��������������
struct TTempBGobj
{
	hash_t oldHash;//[3];
	UnicodeString oldGroup;//[2];
	TFrameObj o;
	Vcl::Categorybuttons::TButtonItem *bi;
};

class TCommonTextManager
{
 public:

	__fastcall TCommonTextManager();
	virtual __fastcall ~TCommonTextManager();

	void __fastcall SetLanguages(const TStringList* sl);
	void __fastcall GetLanguages(TStringList* olist);
	int __fastcall AddText(const TUnicodeText &text, UnicodeString iName, UnicodeString iLanguage);
	bool __fastcall UpdateText(const TUnicodeText &text, UnicodeString iName, UnicodeString iLanguage);
	const TUnicodeText* __fastcall GetText(UnicodeString iName, UnicodeString iLanguage) const;
	void __fastcall DeleteText(UnicodeString iName);
	int __fastcall GetTextID(UnicodeString iName) const;
	bool __fastcall ChangeName(UnicodeString iName, UnicodeString iNewName);
	int __fastcall GetCount();
	void __fastcall GetNames(TStringList* olist);
	void __fastcall ClearAll();

	int __fastcall AddFont(const TImageFont *iFont);
	const TImageFont* __fastcall GetFont(UnicodeString iName) const;
	void __fastcall GetFontNames(TStringList* list);
	void __fastcall ClearFonts();

 private:

	std::list<TUnicodeText> mTexts[MAX_LANGUAGES];
	TStringList	*mpTextNames;
	TStringList	*mpLangNames;
	TList		*mpFonts;
};
//---------------------------------------------------------------------------

class TCommonResObjManager
{
friend class TFObjParent;

 public:

	__fastcall TCommonResObjManager();
	virtual __fastcall ~TCommonResObjManager();

	TObjPattern     *MainGameObjPattern; //������� ������
	TStringList     *aniNameTypes;//���� ��������
	TStringList     *eventNameTypes;//���� �������

	TList           *GParentObj; //��������� ������� ���� (GPerson)
	TList           *states;
	std::map<const __int32, GMapBGObj> BackObj;  //����� ���� (TBGobj)
	TList           *bgotypes;
	TStringList     *stateOppositeTable;

	//������������� �������� ���� �������� �����
	//lst- ���� � TFrameObj (��������� TForm_m::CreateImgListForSave)
	void __fastcall AssignGraphicToObjects(std::list<TResourceItem> *list, UnicodeString path, UnicodeString *errorlist);

	int __fastcall AddGraphicResource(const BMPImage* iBmp, UnicodeString iName);
	const BMPImage* __fastcall GetGraphicResource(int iIdx) const;
	bool __fastcall ReplaceGraphicResource(const BMPImage* iBmp, int iIdx);
	void __fastcall DeleteGraphicResource(UnicodeString iName);
	int __fastcall GetGraphicResourceIndex(UnicodeString iName) const;
	int __fastcall GetGraphicResourceCount() const;
	void __fastcall ClearGraphicResources();

	void __fastcall AssignMainPattern(TObjPattern* p);
	UnicodeString __fastcall GetFileNameMainGameObjPattern() const;
	bool __fastcall IsMainGameObjPatternEmpty();
	void __fastcall AssignObjMainPattern(GPerson *gp);
	const std::list<PropertyItem>* __fastcall GetVariablesMainGameObjPattern() const;

	__int32 __fastcall GetAnimationID(UnicodeString name) const;
	__int32 __fastcall GetParentObjID(UnicodeString name) const;
	__int32 __fastcall GetStateID(UnicodeString name) const;
	__int32 __fastcall GetEventID(UnicodeString name) const;

	GPerson* __fastcall GetParentObjByName(const UnicodeString& name) const;
	const GMapBGObj* GetMapBGObj(__int32 idx) const;
	hash_t __fastcall AddBitmapFrame(const TFrameObj &obj);
	void __fastcall DeleteBitmapFrame(const hash_t& hashValue);
	const TFrameObj* __fastcall GetBitmapFrame(const hash_t& hashValue) const;
	__int32 __fastcall GetIndexByHash(const hash_t& hashValue) const;
	const TFrameObj* __fastcall GetBitmapFrameByIdx(int idx) const;
	int __fastcall GetBitmapFrameCount();
	TAnimation* __fastcall CreateSimplyAnimation(UnicodeString name, hash_t *hashFrameArr, __int32 size_h,
												__int32* animArr, __int32 size_i, __int32 align);
	void __fastcall RenameEventNameInAllAnimations(UnicodeString oldName, UnicodeString newName);
	const TBGobjProperties* GetBGobjProperties(const UnicodeString& name);

	void __fastcall CopyTo(TCommonResObjManager *copy);

 private:

 	void __fastcall FreeAll();

	void __fastcall CopyBitmapFrameDatabase(TList* olist); //TTempBGobj list
	void __fastcall ApplyFrameDatabaseChangesFromList(TList* ilist); //TTempBGobj list
	void __fastcall ReplaceBitmapFrameInAllAnimations(const hash_t& oldHash, const TFrameObj &newObj);

	TList *mpBmpRes;
	TStringList *mpBmpNames;
	std::hash_map<hash_t, TFrameObj> bitmapFrames;
	TList *mpBitmapFramesOrder;
};
//---------------------------------------------------------------------------

class TObjManager
{

 friend class TMapGPerson;
 friend class TMapPerson;
 friend class TFObjOrder;
 friend class TFScriptList;
 friend class TPlatformGameField;

public:

  enum TObjType
  {
	BACK_DECORATION = 0,
	CHARACTER_OBJECT,
	FORE_DECORATION,
	TRIGGER,
	DIRECTOR,
	COLLIDESHAPE,
	OBJ_TYPE_COUNT
  };

  enum TCreateCollideNodeType
  {
	CCNT_NONE = 0,
	CCNT_NEWSHAPE,
	CCNT_ADDNODETOSHAPE,
	CCNT_INSERTNODETOSHAPE,
	CCNT_ERRORSHAPE,
	CCNT_ERRORNODETOSHAPE
  };

  __fastcall TObjManager(TCommonResObjManager *icommonRes);
  virtual __fastcall ~TObjManager();

 TCommonResObjManager *commonRes;

 unsigned short  *map;
 unsigned short  *map_undo;
 __int32          mp_w, mp_h,
				  old_mp_w, old_mp_h,
				  old_md_w, old_md_h,
				  znCount,
                  mesCount,
                  startZone,
				  tileCameraPosOnStart;
 bool ignoreUnoptimizedResoures;

 UnicodeString	  StartScriptName;

 TMapTextBox* __fastcall CreateMapTextBox();
 TMapTextBox* __fastcall CreateMapTextBox(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString ParentName);
 TMapGPerson* __fastcall CreateMapGPerson();
 TMapGPerson* __fastcall CreateMapGPerson(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString ParentName);
 TMapTrigger* __fastcall CreateMapTrigger(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString GParentName);
 TMapTrigger* __fastcall CreateMapTrigger();
 TMapDirector* __fastcall CreateMapDirector(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString GParentName);
 TMapDirector* __fastcall CreateMapDirector();
 TGScript* __fastcall CreateScript(TGScript* temp);
 TMapSoundScheme* __fastcall CreateMapSoundScheme();
 TMapSoundScheme* __fastcall CreateMapSoundScheme(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString ParentName);
 TMapObjPool* __fastcall CreateMapObjPool();
 TMapObjPool* __fastcall CreateMapObjPool(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString ParentName);

 TCreateCollideNodeType __fastcall TryCreateCollideNode(double& x, double& y, const double& findRadius,
 														const TMapCollideLineNode* dragNode,
 														const TCollideChainShape** osh,
														const TMapCollideLineNode** on1,  const TMapCollideLineNode** on2) const;
 TCollideChainShape* __fastcall CreateCollideChainShape();
 TMapCollideLineNode* __fastcall CreateMapCollideNode(double x, double y, __int32 *pos_x, __int32 *pos_y, const UnicodeString& ParentName);
 __int32 __fastcall GetCollideNodesCount();
 TList* __fastcall GetCollideNodesList();
 void __fastcall MarkCrosslines(const TMapCollideLineNode* n);
 void __fastcall MarkCrosslines(const TCollideChainShape* sh);
 void __fastcall ResetCrosslinesErrorMarkers();
 bool __fastcall ChangeNodeIndexInCollideShape(const UnicodeString& collideShapeName,
												const UnicodeString& nodeName,
												const UnicodeString& toNodeName);

 bool __fastcall DeleteObject(TMapPerson* mp);
 bool __fastcall DeleteScript(TGScript* spt);
 __int32 __fastcall DeleteGroup(UnicodeString gpname);

 UnicodeString __fastcall AutoNameObj(TMapPerson *obj);

 bool __fastcall CheckIfMGONameExists(UnicodeString str, void *pItem) const;
 void __fastcall OrderChildsBeforeMain(TMapGPerson *op);
 void __fastcall ChangeScriptNameInAllObjects(UnicodeString oldName, UnicodeString currentName, __int32 sptZone, __int32 sptMode);

 void __fastcall ClearAll();
 void __fastcall ClearAllObjects();

 __int32 __fastcall GetObjTypeCount(TObjType type) const;
 __int32 __fastcall GetTotalMapObjectsCount() const;
 __int32 __fastcall GetTotalMapCharacterObjectsCount() const;
 void* __fastcall GetObjByIndex(TObjType type, __int32 idx) const;

 __int32 __fastcall GetScriptsCount() const;
 TGScript* __fastcall GetScriptByIndex(__int32 idx) const;
 void __fastcall SortAllGlobalScriptsByOrder();
 void __fastcall SychronizeGlobalScripts(const TObjManager *etalon);

 void __fastcall SortAllGlobalTriggersByOrder();
 void __fastcall SychronizeGlobalTriggers(const TObjManager *etalon, TStringList *errorList, __int32 x0, __int32 y0);

 void __fastcall AssignMainPattern(TObjPattern* p);

 void __fastcall RealignPosAll();
 void __fastcall ScalePosAll(__int32 p_w, __int32 p_h, __int32 tp_w, __int32 tp_h, bool remainMapSizeMode);
 void __fastcall CreateListObjOutOfBoundsMap(TRect *r, TList *ipList);

 void __fastcall SetToParentProperties(TMapGPerson* pr);
 void __fastcall SetToParentPropertiesAll();

 void __fastcall DrawMapObject(TMapGPerson* mo, TCanvas* render_bmp);
 void __fastcall DrawDirectorLines(TMapDirector* ipDirector, TMapDirector* ipSelectedDirector, TCanvas *canvas, __int32 scrollbar_x, __int32 scrollbar_y);
 void __fastcall DrawCollideShapes(const TMapCollideLineNode* ipSelectedNode, bool drawDragPoints, TCanvas *canvas, __int32 scrollbar_x, __int32 scrollbar_y);
 void __fastcall RebuildCasheFor(TMapPerson* pr);
 void __fastcall ClearCashe();
 void __fastcall ClearCasheFor(TMapPerson* pr);

 __int32 __fastcall GetAnimationID(UnicodeString name) const;
 __int32 __fastcall GetScriptID(UnicodeString name) const;
 __int32 __fastcall GetGameObjID(UnicodeString name) const;
 __int32 __fastcall GetStateID(UnicodeString name) const;
 __int32 __fastcall GetParentObjID(UnicodeString name) const;
 __int32 __fastcall GetTriggerID(UnicodeString name) const;
 __int32 __fastcall GetDirectorID(UnicodeString name) const;
 __int32 __fastcall GetCollideShapeID(const UnicodeString& name) const;
 __int32 __fastcall GetEventID(UnicodeString name) const;

 __int32 __fastcall GetTextBoxID(UnicodeString name) const;
 void __fastcall RefreshAllTextsObjects();

 void __fastcall CheckAndDeleteBrokenCollidelines(__int32 p_w, __int32 p_h);
 void __fastcall CheckForWarningsAndErrors(TStringList *str_list);

 TMapPerson* __fastcall FindObjectPointer(const UnicodeString& name) const;
 void* __fastcall FindObjectPointer(__int32 srcX, __int32 srcY, __int32 &dx, __int32 &dy, bool useParentIntersection);
 void* __fastcall FindObjectPointers(TList *List, const TRect &SelectionRect);

 void __fastcall CopyTo(TObjManager *copy) const;

 void __fastcall CheckObjForWrongLinks();

private:

 void __fastcall ChangeChildPosition(TMapGPerson* pr);
 void __fastcall RenderTextToImage(const TMapTextBox* tb, BMPImage *bmp);
 TMapPersonCasheData* __fastcall GetCashe(__int32 idx);
 TMapPersonCasheData* __fastcall GetPrCashe(TMapPerson* pr);
 void __fastcall DelCashe(__int32 idx);
 __int32 __fastcall  AddCashe(TMapPersonCasheData *cashe);
 void __fastcall CreateCashe(TMapPerson* pr);
 bool __fastcall CheckForErrors(TMapGPerson* mo);
 void __fastcall CreateErrStateImage(TMapGPerson* mo, TMapPersonCasheData *cashe);

 const TCollideChainShape* __fastcall FindCollideChainShape(const double& x1, const double& y1, const double& findRadius,
 															const TCollideChainShape* dragShape) const;

 TList          *triggers;
 TList          *directors;
 TList          *map_u;   //��������� �� ������� ������������� �� ����� (TMapGPerson)
 TList          *decor_b;
 TList          *decor_f;
 TList          *scripts;
 TList			*collideshapes;
 TList			*arrays[OBJ_TYPE_COUNT];

 TList *m_pCashe;
 TList *mpTempCollideNodesList;
 bool mRefreshTempCollideNodesList;
};
#endif
