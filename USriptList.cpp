//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "USriptList.h"
#include "MainUnit.h"
#include "UObjCode.h"
#include "UObjManager.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TFScriptList *FScriptList;

//---------------------------------------------------------------------------

__fastcall TFScriptList::TFScriptList(TComponent* Owner)
: TForm(Owner)
{
	sripts = new TList();
	items = new TList();
	CB_zones->Items->Assign(Form_m->ListBoxZone->Items);
	CB_zones->Items->Add(Localization::Text(GLOBAL_OJB_NAME));
	changeList = new TStringList();
	EdFindChange(NULL);

	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::SetEmulatorMode()
{
	ButtonOk->Enabled = false;
	Bt_del->Visible = false;
	Bt_edit->Visible = false;
	Bt_add->Visible = false;
	Bt_start->Top = Bt_add->Top;
	Bt_start->Left = Bt_add->Left;
	Bt_start->Visible = true;
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::SetScripts(const TObjManager* objManager)
{
	/*
	int i;
	TGScript *s,*d;
	UnicodeString str;

	if(objManager == NULL)
	{
		return;
	}

	for(i = 0; i < sripts->Count; i++)
	{
		delete (TGScript*)sripts->Items[i];
	}
	sripts->Clear();
	changeList->Clear();

	for(i = 0; i < objManager->GetScriptsCount(); i++)
	{
		s = objManager->GetScriptByIndex(i);
		d = new TGScript();
		s->CopyTo(d);
		sripts->Add(d);
		str = ((TState*)s)->name + _T("=") + ((TState*)s)->name;
		changeList->Add(str);
	}

	if(CB_zones->Items->Count > 0)
	{
		CB_zones->ItemIndex = 0;
	}

	CreateList();
    */
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::GetScripts(TObjManager* objManager, TStringList *changelist)
{
	/*
	int i;
	TGScript *s, *d;

	if(objManager == NULL)
	{
		return;
	}

	for(i = 0; i < objManager->scripts->Count; i++)
	{
		delete (TGScript*)objManager->scripts->Items[i];
	}
	objManager->scripts->Clear();

	for(i = 0; i < sripts->Count; i++)
	{
		s = (TGScript *)sripts->Items[i];
		d = new TGScript();
		s->CopyTo(d);
		objManager->scripts->Add(d);
	}

	if(changelist != NULL)
	{
		changelist->Assign(changeList);
	}
    */
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::FormDestroy(TObject */*Sender*/)
{
	int  i;
	for(i = 0; i < sripts->Count; i++)
	{
		delete (TGScript*)sripts->Items[i];
    }
	delete sripts;
	delete changeList;
	delete items;
}
//---------------------------------------------------------------------------

int __fastcall TFScriptList::GetScript(int zone, UnicodeString name, TGScript** oscript)
{
	/*
	int i;
	TGScript *s;
	for(i = 0; i < sripts->Count; i++)
	{
		s = (TGScript *)sripts->Items[i];
		if(s->Zone != zone)
		{
			continue;
		}
		if(s->Name.Compare(name) == 0)
		{
			*oscript = s;
			return i;
		}
	}
    */
	*oscript = NULL;
	return -1;

}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::AddEditDel(int mode)
{
	/*
	int idx, pos;
	TGScript *spt;
	UnicodeString oldName;

	if(Bt_start->Visible)
	{
		Bt_start->Click();
		return;
	}

	if(mode == mDel)
	{
		idx = ListBox->ItemIndex;
		if(idx < 0)
		{
			return;
		}
		pos = GetScript((int)ListBox->Items->Objects[idx], (int)items->Items[idx], &spt);
		changeList->Strings[pos] = changeList->Names[pos] + _T("=");
		delete spt;
		sripts->Delete(pos);
		CreateList();
		return;
	}

	if(sripts->Count >= MAX_SCRIPTS && mode == mAdd)
	{
		Application->MessageBox(Localization::Text(_T("mMaximumTextsExceeded")).c_str(),
							Localization::Text(_T("mError")).c_str(),
							MB_OK | MB_ICONERROR);
		return;
	}

	spt = new TGScript();

	if(mode == mEdit)
	{
		TGScript *s;

		idx = ListBox->ItemIndex;
		if(idx < 0)
		{
		  delete spt;
		  return;
		}

		idx = GetScript((int)items->Items[idx], ListBox->Items->Strings[idx], &s);

		if(idx < 0)
		{
		  delete spt;
		  return;
		}

		oldName = s->Name;
		s->CopyTo(spt);
	}

	if(mode == mAdd)
	{
		spt->Zone = CB_zones->ItemIndex;
		if(spt->Zone == CB_zones->Items->Count - 1)
		{
		   spt->Zone = LEVELS_INDEPENDENT_IDX;
		}
		else
		if(spt->Zone == CB_zones->Items->Count - 2)
		{
		   spt->Zone = ZONES_INDEPENDENT_IDX;
		}
	}

	Application->CreateForm(__classid(TFScript),&FScript);
	FScript->SetScript(spt);

	bool res;
	do
	{
		res = true;
		if(FScript->ShowModal() == mrOk)
		{
			FScript->GetScript(spt);
			if(spt->Name.IsEmpty())
			{
				Application->MessageBox(Localization::Text(_T("mScriptNameError")).c_str(),
										Localization::Text(_T("mError")).c_str(),
										MB_OK | MB_ICONERROR);
				res = false;
			}
			else
			{
			  for(pos = 0; pos < sripts->Count; pos++)
			  {
				if(((TGScript*)sripts->Items[pos])->Name.Compare(spt->Name) == 0 && pos != idx)
				{
					Application->MessageBox(Localization::Text(_T("mScriptNameExist")).c_str(),
											Localization::Text(_T("mError")).c_str(),
											MB_OK | MB_ICONERROR);
					res = false;
					break;
				}
			  }

			  if(spt->Zone == LEVELS_INDEPENDENT_IDX)
			  for(int j = 0; j < Form_m->m_pLevelList->Count; j++)
			  {
				TObjManager *mo = (TObjManager *)Form_m->m_pLevelList->Items[j];
				if(mo == Form_m->m_pObjManager)
				{
					continue;
                }
				if(mo->map != NULL)
				{
					for(pos = 0; pos < mo->scripts->Count; pos++)
					{
						if(((TGScript*)mo->scripts->Items[pos])->Name.Compare(spt->Name) == 0 && pos != idx)
						{
							Application->MessageBox(Localization::Text(_T("mScriptNameExist")).c_str(),
													Localization::Text(_T("mError")).c_str(),
													MB_OK | MB_ICONERROR);
							res = false;
							j = Form_m->m_pLevelList->Count;
							break;
						}
					}
				}
			  }
			}

			if(res)
			{
			  if(mode == mAdd)
			  {
				sripts->Add(spt);
				if(changeList->IndexOfName(spt->Name) >= 0)
				{
				  changeList->Values[spt->Name] = spt->Name;
				}
				else
				{
				  changeList->Add(spt->Name + _T("=") + spt->Name);
				}
			  }
			  else
			  if(mode == mEdit)
			  {
				  spt->CopyTo(((TGScript *)sripts->Items[idx]));
				  for(pos = 0; pos < changeList->Count; pos++)
				  {
				   if(changeList->ValueFromIndex[pos].Compare(oldName) == 0)
				   {
					 changeList->ValueFromIndex[pos] = spt->Name;
					 break;
				   }
				  }
			  }
			  CreateList();
			}
		}
		else
		{
			res = true;
		}
	}
	while(!res);
	FScript->Free();
	FScript=NULL;
	if(spt != NULL && mode != mAdd)
	{
		delete spt;
	}
    */
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::ListBoxDblClick(TObject */*Sender*/)
{
	AddEditDel(mEdit);
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::Bt_addClick(TObject */*Sender*/)
{
	AddEditDel(mAdd);
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::Bt_editClick(TObject */*Sender*/)
{
	AddEditDel(mEdit);
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::Bt_delClick(TObject */*Sender*/)
{
	AddEditDel(mDel);
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::ListBoxKeyPress(TObject */*Sender*/, char &Key)
{
	if(Key == VK_RETURN)
	{
		Key = NULL;
		AddEditDel(mEdit);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::CreateList()
{
	/*
	int i, zone;
	TGScript* s;

	zone = CB_zones->ItemIndex;
	if(zone == CB_zones->Items->Count - 1)
		zone = LEVELS_INDEPENDENT_IDX;
	else
	if(zone == CB_zones->Items->Count - 2)
		zone = ZONES_INDEPENDENT_IDX;

	ListBox->Clear();
	items->Clear();
	for(i = 0; i < sripts->Count; i++)
	{
		s = (TGScript *)sripts->Items[i];
		if(CB_zones->Enabled)
		{
			if(s->Zone == zone)
			{
				ListBox->Items->Add(s->Name);
				items->Add(reinterpret_cast<void*>(s->Zone));
			}
		}
		else
		{
			if(s->Name.Pos(EdFind->Text.UpperCase()) != 0)
			{
				ListBox->Items->Add(s->Name);
				items->Add(reinterpret_cast<void*>(s->Zone));
			}
		}
	}
    */
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::CB_zonesChange(TObject */*Sender*/)
{
	CreateList();
	ListBoxClick(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::Bt_startClick(TObject */*Sender*/)
{
	int idx = ListBox->ItemIndex;
	if(idx < 0)
	{
		return;
    }
	Form_m->OnScriptStartClick(ListBox->Items->Strings[idx]);
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::FormClose(TObject */*Sender*/, TCloseAction &/*Action*/)
{
	Form_m->OnScriptsWndHide();
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::SetZone(int zone)
{
   if(zone < 0)
   {
	  CB_zones->ItemIndex = CB_zones->Items->Count-1;
   }
   else
   {
	  CB_zones->ItemIndex = zone;
   }
   CB_zonesChange(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::ListBoxClick(TObject */*Sender*/)
{
	/*
	StatusBar->SimpleText = _T("");
	if(ListBox->ItemIndex >= 0)
	{
		for(int i=0; i < sripts->Count; i++)
		{
			TGScript* s = (TGScript *)sripts->Items[i];
			if(s->Name.Compare(ListBox->Items->Strings[ListBox->ItemIndex]) == 0)
			{
			   StatusBar->SimpleText = Localization::Text(_T("mIndex")) +_T("= ") + IntToStr(i);
			   break;
			}
		}

	}
    */
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::BtCrearFilterClick(TObject*)
{
	EdFind->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFScriptList::EdFindChange(TObject*)
{
	if(EdFind->Text.IsEmpty())
	{
		CB_zones->Enabled = true;
		CB_zones->Color = clWindow;
		CB_zonesChange(NULL);
	}
	else
	{
		CB_zones->Enabled = false;
		CB_zones->Color = clBtnFace;
		CreateList();
		ListBoxClick(NULL);
    }
}
//---------------------------------------------------------------------------

