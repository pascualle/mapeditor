﻿	//---------------------------------------------------------------------------

#pragma hdrstop

#include "platform.h"
#include "system.h"
#include <math.h>
#include "assert.h"
#include "UObjCode.h"
#include "MainUnit.h"
#include "png.h"
#include "UObjProperty.h"
#include "UObjManager.h"
#include "ULocalization.h"
#include "ULoad.h"
#include "USave.h"

//---------------------------------------------------------------------------
#ifdef _WIN64
	#ifdef _DEBUG
	#pragma link "libpng64d.lib"
	#pragma link "zlib64d.lib"
	#else
	#pragma link "libpng64.lib"
	#pragma link "zlib64.lib"
	#endif
#else
	#ifdef _DEBUG
	#pragma link "libpngd.lib"
	#pragma link "zlibd.lib"
	#else
	#pragma link "libpng.lib"
	#pragma link "zlib.lib"
	#endif
#endif

#pragma package(smart_init)

//---------------------------------------------------------------------------

static BMPImage *empty_bmp = NULL;

const int TPrpFunctions::LOCK_IMAGE_IDX = 37;

//---------------------------------------------------------------------------

namespace SpecialObjConstants
{
	const char VarObjNameInitiator[32] = "@OBJ_INITIATOR@\0";
	const char VarObjNameSecond[32] = "@OBJ_SECOND@\0";
	const char VarStateName[32] = "@ST_OPPOSITESTATE@\0";
	const char VarBlankActionName[32] = "@BLANK_ACTION@\0";
}
//---------------------------------------------------------------------------

namespace Actions
{
	const char Names[sptCOUNT][32] =
	{
		"sptONCOLLIDE",
		"sptONTILECOLLIDE",
		"sptONCHANGENODE",
		"sptONCHANGE1",
		"sptONCHANGE2",
		"sptONCHANGE3",
		"sptONENDANIM"
	};

	const static char _captionAlias[sptCOUNT][32] =
	{
		"mActNameCollide",
		"---------------------",
		"mActNameChangeNode",
		"mActNameChangeParam1",
		"mActNameChangeParam2",
		"mActNameChangeParam3",
		"mActNameEndAnimation"
	};

	UnicodeString Caption[sptCOUNT];
}
//---------------------------------------------------------------------------

namespace Movement
{
	const char XMLName[dirCOUNT][32] =
	{
		"DIR_IDLE",
		"DIR_UP",
		"DIR_LEFT",
		"DIR_RIGHT",
		"DIR_DOWN"
	};

	const static char _captionAlias[dirCOUNT][32] =
	{
		"mDirectionNameIdle",
		"mDirectionNameUp",
		"mDirectionNameLeft",
		"mDirectionNameRight",
		"mDirectionNameDown"
	};

	UnicodeString Name[dirCOUNT];
}

// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------
// ---------------------------------------------------------------------------

void LocalizeGlobalConsts()
{
	__int32 i;
	for(i = 0; i < ::Actions::sptCOUNT; i++)
	{
		::Actions::Caption[i] = Localization::Text(UnicodeString(::Actions::_captionAlias[i]));
	}
	for(i = 0; i < ::Movement::dirCOUNT; i++)
	{
		::Movement::Name[i] = Localization::Text(UnicodeString(::Movement::_captionAlias[i]));
	}
}

// ---------------------------------------------------------------------------
//BMPImage
//BMPImage
//BMPImage
//---------------------------------------------------------------------------

__fastcall BMPImage::BMPImage()
: mWidth2N(0)
, mHeight2N(0)
, mpBmp(NULL)
, mOpaqType(-1)
, mType(BMP_TYPE_DC32)
{
}
//---------------------------------------------------------------------------

__fastcall BMPImage::BMPImage(Graphics::TBitmap *ipSrc)
: mWidth2N(0)
, mHeight2N(0)
, mOpaqType(-1)
, mType(BMP_TYPE_DC32)
{
	mpBmp = ipSrc;
	if (mpBmp != NULL)
	{
		if(mpBmp->PixelFormat != pf32bit)
		{
			mpBmp->PixelFormat = pf32bit;
		}
		CalculateSize2N();
	}
}
//---------------------------------------------------------------------------

__fastcall BMPImage::~BMPImage()
{
	if (mpBmp != NULL)
		delete mpBmp;
}
//---------------------------------------------------------------------------

void __fastcall BMPImage::Assign(const BMPImage *ipSrc)
{
	if(ipSrc != NULL)
	{
		Assign(ipSrc->GetBitmapData());
	}
	else
	{
		Assign(static_cast<Graphics::TBitmap*>(NULL));
    }
}
//---------------------------------------------------------------------------

void __fastcall BMPImage::Assign(const Graphics::TBitmap *ipSrc)
{
	if (mpBmp != NULL)
	{
		mpBmp->Assign(const_cast<Graphics::TBitmap*>(ipSrc));
		if(mpBmp->PixelFormat != pf32bit)
		{
			mpBmp->PixelFormat = pf32bit;
		}
	}
	else
	{
		mpBmp = new Graphics::TBitmap();
		mpBmp->Assign(const_cast<Graphics::TBitmap*>(ipSrc));
		if(mpBmp->PixelFormat != pf32bit)
		{
			mpBmp->PixelFormat = pf32bit;
        }
	}
	CalculateSize2N();
}
//---------------------------------------------------------------------------

void __fastcall BMPImage::CalculateSize2N()
{
	mHeight2N = mWidth2N = 0;
	if (mpBmp != NULL)
	{
		if (!mpBmp->Empty)
		{
			__int32 val = 2;
			while (mpBmp->Width > val)
			{
				val <<= 1;
			}
			mWidth2N = val;
			val = 2;
			while (mpBmp->Height > val)
			{
				val <<= 1;
			}
			mHeight2N = val;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall BMPImage::SetSize(__int32 iWidth, __int32 iHeight)
{
	if (mpBmp != NULL)
	{
		mpBmp->SetSize(iWidth, iHeight);
		CalculateSize2N();
	}
}
//---------------------------------------------------------------------------

void __fastcall BMPImage::ResetAlpha()
{
	if (mpBmp != NULL)
	{
		__int32 x, y, w;
		unsigned char *p;
		w = mpBmp->Width * 4;
		for (y = 0; y < mpBmp->Height; y++)
		{
			p = (unsigned char*)mpBmp->ScanLine[y];
			for (x = 0; x < w; x += 4)
			{
				p[x + 3] = 255;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall BMPImage::FillTransparentWithColor(TColor color)
{
	if (mpBmp != NULL)
	{
		__int32 x, y, w;
		unsigned char r, g, b, *p;
		w = mpBmp->Width * 4;
		r = (unsigned char)(((__int32)color) >> 16);
		g = (unsigned char)(((__int32)color) >> 8);
		b = (unsigned char)color;
		for (y = 0; y < mpBmp->Height; y++)
		{
			p = (unsigned char*)mpBmp->ScanLine[y];
			for (x = 0; x < w; x += 4)
			{
				if (p[x + 3] == 0)
				{
					p[x] = r;
					p[x + 1] = g;
					p[x + 2] = b;
					p[x + 3] = 255;
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall BMPImage::FillColorWithTransparent(TColor color)
{
	if (mpBmp != NULL)
	{
		__int32 x, y, w;
		unsigned char r, g, b, *p;
		w = mpBmp->Width * 4;
		r = (unsigned char)(((__int32)color) >> 16);
		g = (unsigned char)(((__int32)color) >> 8);
		b = (unsigned char)color;
		for (y = 0; y < mpBmp->Height; y++)
		{
			p = (unsigned char*)mpBmp->ScanLine[y];
			for (x = 0; x < w; x += 4)
			{
				if (p[x] == r && p[x + 1] == g && p[x + 2] == b)
				{
					p[x] = 0;
					p[x + 1] = 0;
					p[x + 2] = 0;
					p[x + 3] = 0;
				}
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall BMPImage::Clear(TColor color)
{
	if (mpBmp != NULL)
	{
		__int32 x, y, w;
		unsigned char r, g, b, a, *p;
		w = mpBmp->Width * 4;
		a = (unsigned char)(((__int32)color) >> 24);
		r = (unsigned char)(((__int32)color) >> 16);
		g = (unsigned char)(((__int32)color) >> 8);
		b = (unsigned char)color;
		for (y = 0; y < mpBmp->Height; y++)
		{
			p = (unsigned char*)mpBmp->ScanLine[y];
			for (x = 0; x < w; x += 4)
			{
					p[x] = r;
					p[x + 1] = g;
					p[x + 2] = b;
					p[x + 3] = a;
			}
		}
	}
}
//---------------------------------------------------------------------------

unsigned char *__fastcall BMPImage::CreateRGBAData() const
{
	if (mpBmp != NULL && mHeight2n * mWidth2n != 0)
	{
		__int32 x, y, w;
		unsigned char rs, gs, bs, *p, *d;
		unsigned char *rgba = new unsigned char[mHeight2n * mWidth2n * 4];
		Graphics::TBitmap *bmpdata = const_cast<Graphics::TBitmap*>(GetBitmapData());
		w = Width * 4;
		for (y = 0; y < Height; y++)
		{
			p = (unsigned char*)bmpdata->ScanLine[y];
			d = rgba + y * mWidth2n * 4;
			for (x = 0; x < w; x += 4)
			{
				d[x] = p[x + 2];
				d[x + 2] = p[x];
				d[x + 1] = p[x + 1];
				d[x + 3] = p[x + 3];
			}
		}
		return rgba;
	}
	return NULL;
}
//---------------------------------------------------------------------------

const Graphics::TBitmap *__fastcall BMPImage::GetBitmapData() const
{
	return mpBmp;
}
//---------------------------------------------------------------------------

__int32 __fastcall BMPImage::GetWidth2N() const
{
	return mWidth2N;
}
//---------------------------------------------------------------------------

__int32 __fastcall BMPImage::GetHeight2N() const
{
	return mHeight2N;
}
//---------------------------------------------------------------------------

__int32 __fastcall BMPImage::GetHeight() const
{
	if (mpBmp == NULL)
	{
		return 0;
	}
	else
	{
		return mpBmp->Height;
	}
}
//---------------------------------------------------------------------------

__int32 __fastcall BMPImage::GetWidth() const
{
	if (mpBmp == NULL)
	{
		return 0;
	}
	else
	{
		return mpBmp->Width;
	}
}
//---------------------------------------------------------------------------

TCanvas *__fastcall BMPImage::GetCanvas() const
{
	if (mpBmp != NULL)
	{
		return mpBmp->Canvas;
	}
	return NULL;
}
//---------------------------------------------------------------------------

const bool __fastcall BMPImage::IsEmpty() const
{
	if (mpBmp != NULL)
	{
		return mpBmp->Empty;
	}
	return true;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//TImageFont
//TImageFont
//TImageFont
//---------------------------------------------------------------------------

__fastcall TImageFont::TImageFont()
{
	mResName = mName = _T("");
	mFont.mpAbcImage = NULL;
	mFont.mAlphabet.mpAlphabet = NULL;
	mFont.mAlphabet.mpAlphabetTextureOffsetMap = NULL;
}
//---------------------------------------------------------------------------

__fastcall TImageFont::~TImageFont()
{
	if (!mName.IsEmpty())
	{
		ReleaseFont(&mFont);
		delete mFont.mpAbcImage;
	}
}
//---------------------------------------------------------------------------

__int32 __fastcall TImageFont::GetWidth()const
{
	if (!mName.IsEmpty())
	{
		return mFont.mAlphabet.mFixedHeight;
	}
	return 0;
}
//---------------------------------------------------------------------------

const UnicodeString __fastcall TImageFont::GetName()const
{
	return mName;
}
//---------------------------------------------------------------------------

const UnicodeString __fastcall TImageFont::GetImageResourceName() const
{
	return mResName;
}
//---------------------------------------------------------------------------

const Font *__fastcall TImageFont::GetData()const
{
	if (!mName.IsEmpty())
	{
		return &mFont;
	}
	return NULL;
}
//---------------------------------------------------------------------------

bool __fastcall TImageFont::LoadFont(UnicodeString iName)
{
	FILE *f;
	UnicodeString str;
	__int32 intVal;
	_TCHAR resName[MAX_PATH];
	unsigned char *data, *d;

	str = forceCorrectFontFileExtention(Form_m->PathPngRes + iName);
	f = _wfopen(str.c_str(), _T("rb"));
	if (f == NULL)
	{
		return false;
	}

	fseek(f, 0L, SEEK_END);
	intVal = ftell(f);
	fseek(f, 0L, SEEK_SET);

	data = new unsigned char[intVal];

	fread(data, sizeof(unsigned char), intVal, f);
	fclose(f);

	d = data;
	if (d[0] != 'B' && d[1] != 'M' && d[2] != 'F' && d[3] != 3)
	{
		delete[]data;
		return false;
	}

	d += 4;
	//block1
	d++;
	intVal = _NBytesToInt(d, 4, 0);
	d += 4 + intVal;

	//block2
	d++;
	intVal = _NBytesToInt(d, 4, 0);
	d += 4 + intVal;

	//block3
	d += 5;
	intVal = 0;
	while (*d != L'.')
	{
		resName[intVal] = (char)(*d);
		intVal++;
		d++;
	}
	resName[intVal] = _T('\0');

	Graphics::TBitmap *bmp = new Graphics::TBitmap;
	mResName = UnicodeString(resName) + _T(".xxx");
	str = Form_m->PathPngRes + mResName;
	if (mFont.mpAbcImage != NULL)
	{
		delete mFont.mpAbcImage;
	}
	if (read_png(str.c_str(), bmp))
	{
		mFont.mpAbcImage = new BMPImage(bmp);
		InitFont(data, &mFont);
		mName = iName;
		mResName = forceCorrectResFileExtention(mResName);
		delete[]data;
		return true;
	}
	else
	{
		mName = _T("");
		mResName = _T("");
		delete bmp;
		delete[]data;
		mFont.mpAbcImage = NULL;
		return false;
	}
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//TBGobjProperties
//TBGobjProperties
//TBGobjProperties
//---------------------------------------------------------------------------

__fastcall TBGobjProperties::TBGobjProperties()
{
	mType = TListObjectType::LISTOBJ_TBGobjProperties;
	Clear();
}
//---------------------------------------------------------------------------

__fastcall TBGobjProperties::TBGobjProperties(const TBGobjProperties& obj)
{
    mType = TListObjectType::LISTOBJ_TBGobjProperties;
	obj.CopyTo(*this);
}
//---------------------------------------------------------------------------

void __fastcall TBGobjProperties::Clear()
{
	name = _T("");
	type = NONE_BGTYPE;
}
 //---------------------------------------------------------------------------

void __fastcall TBGobjProperties::CopyTo(TBGobjProperties &prp) const
{
	prp.name = name;
	prp.type = type;
}
 //---------------------------------------------------------------------------

bool __fastcall TBGobjProperties::SaveToFile(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h)
{
	short w_val;

	assert(f.type == TSaveLoadStreamData::TSLDataType::SaveProject ||
			f.type == TSaveLoadStreamData::TSLDataType::SaveAsBinary ||
			f.type == TSaveLoadStreamData::TSLDataType::ExportToXML);


	if(!h.IsValid())
	{
		throw Exception(_T("TBGobjProperties::SaveToFile, SaveLoadStreamHandle is not valid"));
	}

	WriteAnsiStringData(f, h, _T("name"), name);

	//BYTE	значение
	f.writeFn(&type, TSaveLoadStreamData::TSLFieldType::ftChar, _T("value"), h);

	return true;
}
 //---------------------------------------------------------------------------

bool __fastcall TBGobjProperties::LoadFromFile(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, int ver)
{
	assert(f.type == TSaveLoadStreamData::TSLDataType::LoadProject ||
			f.type == TSaveLoadStreamData::TSLDataType::LoadAsBinary);

	short w_val;

	if(!h.IsValid())
	{
		throw Exception(_T("TBGobjProperties::LoadFromFile, SaveLoadStreamHandle is not valid"));
	}

	ReadAnsiStringData(f, h, _T("name"), name, ver);

	//BYTE	значение
	f.readFn(&type, TSaveLoadStreamData::TSLFieldType::ftChar, _T("value"), h);

	return true;
}
 //---------------------------------------------------------------------------

//---------------------------------------------------------------------------
 //TState
 //TState
 //TState
 //---------------------------------------------------------------------------

__fastcall TState::TState()
{
	mType = TListObjectType::LISTOBJ_TState;
	Clear();
}
//---------------------------------------------------------------------------

__fastcall TState::TState(const TState& obj)
{
	mType = TListObjectType::LISTOBJ_TState;
	obj.CopyTo(this);
}
//---------------------------------------------------------------------------

void __fastcall TState::Clear()
{
	name = _T("");
	animation = _T("");
	direction = Movement::dirIDLE;
}
//---------------------------------------------------------------------------

void __fastcall TState::CopyTo(TState *state) const
{
	state->name = name;
	state->animation = animation;
	state->direction = direction;
}
//---------------------------------------------------------------------------

bool __fastcall TState::SaveToFile(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h)
{
	short w_val;

	assert(f.type == TSaveLoadStreamData::TSLDataType::SaveProject ||
			f.type == TSaveLoadStreamData::TSLDataType::SaveAsBinary ||
			f.type == TSaveLoadStreamData::TSLDataType::ExportToXML);

	if(!h.IsValid())
	{
		throw Exception(_T("TState::SaveToFile, SaveLoadStreamHandle is not valid"));
	}

	WriteAnsiStringData(f, h, _T("name"), name);

	WriteAnsiStringData(f, h, _T("animation"), animation);

	if(f.type != TSaveLoadStreamData::TSLDataType::ExportToXML)
	{
		f.writeFn(&direction, TSaveLoadStreamData::TSLFieldType::ftChar, _T("direction"), h);
	}
	else
	{
		int idx_d = direction / 2 - 1;
		if(idx_d < 0)
		{
			idx_d = 0;
		}
		WriteAnsiStringData(f, h, _T("direction"), UnicodeString(Movement::XMLName[idx_d]));
	}

	return true;
}
//---------------------------------------------------------------------------

bool __fastcall TState::LoadFromFile(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, int ver)
{
	short w_val;

	assert(f.type == TSaveLoadStreamData::TSLDataType::LoadProject ||
			f.type == TSaveLoadStreamData::TSLDataType::LoadAsBinary);

	if(!h.IsValid())
	{
		throw Exception(_T("TState::LoadFromFile, SaveLoadStreamHandle is not valid"));
	}

	ReadAnsiStringData(f, h, _T("name"), name, ver);

	ReadAnsiStringData(f, h, _T("animation"), animation, ver);

	if(animation.Pos(_T("ANIM_")) != 1)
	{
		animation = _T("ANIM_") + animation;
	}

	if(f.type != TSaveLoadStreamData::TSLDataType::ExportToXML)
	{
		f.readFn(&direction, TSaveLoadStreamData::TSLFieldType::ftChar, _T("direction"), h);
	}
	else
	{
		UnicodeString tname;
		ReadAnsiStringData(f, h, _T("direction"), tname, ver);

		direction = Movement::dirIDLE;
		for(int i = 0 ; i < Movement::dirCOUNT; i++)
		{
			if(UnicodeString(Movement::XMLName[i]) == tname)
			{
			   direction = static_cast<Movement::Direction>(i);
               break;
            }
		}
	}

	return true;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//TFrameObj
//TFrameObj
//TFrameObj
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
__fastcall TFrameObj::TFrameObj()
{
	Clear();
}
//---------------------------------------------------------------------------

void __fastcall TFrameObj::Clear()
{
	img_source_w = img_source_h = 0;
	x = y = 0;
	w = h = 0;
	res_idx = -1;
	bgPropertyName = _T("");
	groupId = _T("");
	img_source_name = _T("");
	initial_hash = 0;
	has_initial_hash = false;
}
//---------------------------------------------------------------------------

void __fastcall TFrameObj::setSourceRes(__int32 sw, __int32 sh, __int32 ires_idx, UnicodeString sname)
{
	img_source_name = sname;
	img_source_w = sw;
	img_source_h = sh;
	res_idx = ires_idx;
}
//---------------------------------------------------------------------------

void __fastcall TFrameObj::getSourceRes(__int32 *sw, __int32 *sh, __int32 *ores_idx, UnicodeString *sname)const
{
	if (sname != NULL)
	{
		*sname = img_source_name;
	}
	if (sw != NULL)
	{
		*sw = img_source_w;
	}
	if (sh != NULL)
	{
		*sh = img_source_h;
	}
	if (ores_idx != NULL)
	{
		*ores_idx = res_idx;
	}
}
//---------------------------------------------------------------------------

const __int32 __fastcall TFrameObj::getResIdx() const
{
	return res_idx;
}
//---------------------------------------------------------------------------

void __fastcall TFrameObj::copyTo(TFrameObj &tBGobj) const
{
	tBGobj.x = x;
	tBGobj.y = y;
	tBGobj.w = w;
	tBGobj.h = h;
	tBGobj.bgPropertyName = bgPropertyName;
	tBGobj.setSourceRes(img_source_w, img_source_h, res_idx, img_source_name);
	tBGobj.groupId = groupId;
	tBGobj.initial_hash = initial_hash;
	tBGobj.has_initial_hash = has_initial_hash;
}
//---------------------------------------------------------------------------

bool __fastcall TFrameObj::IsEmpty() const
{
	return(w == 0 && h == 0 && img_source_name.IsEmpty());
}
//---------------------------------------------------------------------------

bool __fastcall TFrameObj::compareTo(TFrameObj &tBGobj) const
{
	return(x == tBGobj.x && y == tBGobj.y && w == tBGobj.w && h == tBGobj.h &&
			 (img_source_name.Compare(tBGobj.img_source_name) == 0) &&
			 (bgPropertyName.Compare(tBGobj.bgPropertyName) == 0));
}
//---------------------------------------------------------------------------

void __fastcall TFrameObj::SetInitialHash(const hash_t& hash)
{
	initial_hash = hash;
	has_initial_hash = true;
}
//---------------------------------------------------------------------------

bool __fastcall TFrameObj::GetInitialHash(hash_t& hash) const
{
	hash = initial_hash;
	return has_initial_hash;
}
//---------------------------------------------------------------------------

bool __fastcall TFrameObj::IsValid() const
{
	if(has_initial_hash)
	{
		return initial_hash == getHash();
	}
	else
	{
    	return true;
	}
}
//---------------------------------------------------------------------------

const hash_t __fastcall TFrameObj::getHash() const
{
	const UnicodeString str = img_source_name + IntToStr(x) + IntToStr(y) +
							IntToStr(w) + IntToStr(h) + bgPropertyName +
							IntToStr(NONE_BGTYPE);
	hash_t val = 2166136261U;
	const _TCHAR *ptr = str.c_str();
	while(*ptr != _T('\0'))
	{
		val = 16777619U * val ^ (hash_t)*ptr++;
	}
	ldiv_t _Qrem = ldiv(val, 127773);
	_Qrem.rem = 16807 * _Qrem.rem - 2836 * _Qrem.quot;
	if (_Qrem.rem < 0)
		_Qrem.rem += 2147483647;
	val = ((hash_t)_Qrem.rem);
	return val;
}
//---------------------------------------------------------------------------

__fastcall TFrameObj::~TFrameObj()
{
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//TObjPattern
//TObjPattern
//TObjPattern
//---------------------------------------------------------------------------

__fastcall TObjPattern::TObjPattern()
{
	fname = _T("");
}

//---------------------------------------------------------------------------
__fastcall TObjPattern::~TObjPattern()
{
	Clear();
}
//---------------------------------------------------------------------------

void __fastcall TObjPattern::Clear()
{
	vars.clear();
	fname = _T("");
}
//---------------------------------------------------------------------------

const std::list<PropertyItem>* __fastcall TObjPattern::GetVars() const
{
	return &vars;
}
//---------------------------------------------------------------------------

void __fastcall TObjPattern::GetVars(std::list<PropertyItem>* outlist) const
{
	outlist->clear();
	std::list<PropertyItem>::const_iterator iv;
	for (iv = vars.begin(); iv != vars.end(); ++iv)
	{
		outlist->push_back(*iv);
	}
}
//---------------------------------------------------------------------------

void __fastcall TObjPattern::SetVars(const std::list<PropertyItem>* list)
{
	vars.clear();
	std::list<PropertyItem>::const_iterator iv;
	for (iv = list->begin(); iv != list->end(); ++iv)
	{
		vars.push_back(*iv);
	}
}
//---------------------------------------------------------------------------


 //загрузить шаблон из файла
bool __fastcall TObjPattern::loadPattern(UnicodeString filename, UnicodeString *mesErr)
{
	bool res;

	/*
	if ((iodata.f = _wfopen(filename.c_str(), _T("rb"))) == NULL)
	{
		const UnicodeString mes = Localization::Text(_T("mOpenObjPatternError")) + _T(" ") + filename;
		if (mesErr != NULL)
		{
			*mesErr = mes;
		}
		return false;
	}
	*/

	TSaveLoadStreamData::TSaveLoadStreamDataInit initdata;
	initdata.type = TSaveLoadStreamData::TSLDataType::LoadProject;
	initdata.fileName = filename;

	TSaveLoadStreamData *iodata = new TSaveLoadStreamData(initdata, false);
	res = iodata->Init();
	if(res)
	{
		TSaveLoadStreamHandle rootHandle;
		iodata->readFn(nullptr, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("ObjPatternData"), rootHandle);
		res = loadFromFile(*iodata, rootHandle, FILEFORMAT_VER, mesErr);
	}
	delete iodata;
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TObjPattern::loadFromFile(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, int ver, UnicodeString *mesErr)
{
	unsigned char ch[4];
	__int32 int_val;
	__int32 items_count;
	bool res;
	TObjPattern *t_pattern;

	assert(f.type == TSaveLoadStreamData::TSLDataType::LoadProject ||
					f.type == TSaveLoadStreamData::TSLDataType::LoadAsBinary);

	res = true;
	t_pattern = new TObjPattern();
	CopyTo(t_pattern);
	Clear();

	f.readFn(&ch[0], TSaveLoadStreamData::TSLFieldType::ftChar, _T("h1"), h);
	f.readFn(&ch[1], TSaveLoadStreamData::TSLFieldType::ftChar, _T("h2"), h);
	f.readFn(&ch[2], TSaveLoadStreamData::TSLFieldType::ftChar, _T("h3"), h);

	try
	{
		if (ch[0] == 'p' && ch[1] == 'f');
		else
		{
			throw Exception(Localization::Text(_T("mNonCompatibleFileTypeError")));
		}
		if (ch[2] != 2)
		{
			throw Exception(Localization::Text(_T("mNonCompatibleFileVerError")));
		}

		//[short количество графических фрагментов на объект]
		int_val = 0;
		//f.readFn(&int_val, sizeof(short), 1, f);
		f.readFn(&int_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("grfrcount"), h);

		//[short количество переменных]
		int_val = 0;
		//f.readFn(&int_val, sizeof(short), 1, f);
		f.readFn(&int_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("varscount"), h);
		items_count = int_val;

		for (__int32 i = 0; i < items_count; i++)
		{
			TSaveLoadStreamHandle itemHandle;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("var_") + IntToStr(i), itemHandle);

			double f_val;
			PropertyItem iv;

			ReadAnsiStringData(f, itemHandle, _T("name"), iv.name, ver);

			if(iv.name.Pos(_T("PRP_\0")) != 1)
			{
				iv.name = _T("PRP_\0") + iv.name;
			}

			//BYTE количество байт, занимаемой переменной (size)
			int_val = 0;
			//f.readFn(&int_val, 1, 1, f);
			f.readFn(&int_val, TSaveLoadStreamData::TSLFieldType::ftChar, _T("size"), itemHandle);

			//iv->size = int_val;
			//__int32 значение по умолчанию (def)
			f_val = 0.0f;
			if(ver < 19)
			{
				int_val = 0;
				//f.readFn(&int_val, sizeof(__int32), 1, f);
				f.readFn(&int_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("defaultvalue"), itemHandle);
				f_val = int_val;
			}
			else
			{
				//f.readFn(&f_val, sizeof(double), 1, f);
				f.readFn(&f_val, TSaveLoadStreamData::TSLFieldType::ftDouble, _T("defaultvalue"), itemHandle);
			}
			iv.SetDefaultValue(f_val);
			vars.push_back(iv);
		}
	}
	catch(Exception & e)
	{
		UnicodeString mes;
		res = false;
		Clear();
		t_pattern->CopyTo(this);
		mes = Localization::Text(_T("mOpenObjPatternError")) + _T(": ") + e.Message;
		if (mesErr != NULL)
			* mesErr = mes;
	}
	delete t_pattern;
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TObjPattern::saveToFile(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h)
{
	char ch[4];
	__int32 int_val;
    int i = 0;

	assert(f.type == TSaveLoadStreamData::TSLDataType::SaveAsBinary ||
				  f.type == TSaveLoadStreamData::TSLDataType::SaveProject ||
				  f.type == TSaveLoadStreamData::TSLDataType::ExportToXML);

	//[заголовок]
	ch[0] = 'p'; //признак уровня
	ch[1] = 'f'; //признак карты
	ch[2] = 2; //версия

	f.writeFn(&ch[0], TSaveLoadStreamData::TSLFieldType::ftChar, _T("h1"), h);
	f.writeFn(&ch[1], TSaveLoadStreamData::TSLFieldType::ftChar, _T("h2"), h);
	f.writeFn(&ch[2], TSaveLoadStreamData::TSLFieldType::ftChar, _T("h3"), h);

	//[short количество графических фрагментов на объект]
	int_val = 0; //CSEd_gr->Value;
	//f.writeFn(&int_val, sizeof(short), 1, f);
	f.writeFn(&int_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("grfrcount"), h);

	//[short количество переменных]
	int_val = vars.size();
	f.writeFn(&int_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("varscount"), h);

	std::list<PropertyItem>::const_iterator iv;
	for (iv = vars.begin(); iv != vars.end(); ++iv)
	{
		TSaveLoadStreamHandle itemHandle;
		f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("var_") + IntToStr(i), itemHandle);

		WriteAnsiStringData(f, itemHandle, _T("name"), iv->name);

		ch[0] = (BYTE)/*iv->size*/4; //BYTE количество байт, занимаемой переменной (size)
		//f.writeFn(ch_val, 1, 1, f);
		f.writeFn(&ch, TSaveLoadStreamData::TSLFieldType::ftChar, _T("size"), itemHandle);

		double f_val = iv->GetDefaultValue(); //double значение по умолчанию (def)
		//f.writeFn(&f_val, sizeof(double), 1, f);
		f.writeFn(&f_val, TSaveLoadStreamData::TSLFieldType::ftDouble, _T("defaultvalue"), itemHandle);

        i++;
	}

	return true;
}

//---------------------------------------------------------------------------
void __fastcall TObjPattern::CopyTo(TObjPattern *pt) const
{
	if (this == pt)
		return;
	pt->fname = fname;
	pt->SetVars(&vars);
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//ВСЕ ЧТО КАСАЕТСЯ Animations

__fastcall TAnimationFrameData::TAnimationFrameData()
: _frameId(0)
, type(LogicObjType::UOP_MAX_LOGIC_DRAW_OBJ)
, posX(0.0f)
, posY(0.0f)
, rw(0)
, rh(0)
, a(255)
, data(0)
{
	strm_filename = _T("");
}
//---------------------------------------------------------------------------

__fastcall TAnimationFrameData::TAnimationFrameData(const TAnimationFrameData& afd)
{
	*this = afd;
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrameData::SetFrameId(hash_t fid)
{
	_frameId = fid;
}
//---------------------------------------------------------------------------

bool __fastcall TAnimationFrameData::isStreamFrame() const
{
	return data & 0x8000;
}
//---------------------------------------------------------------------------

unsigned short __fastcall TAnimationFrameData::getTransformAngle() const
{
	return data & 0x7FFF;
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrameData::setStreamFrame(bool val)
{
	if(val)
	{
		data |= 0x8000;
	}
	else
	{
		data &= 0x7FFF;
		strm_filename = _T("");
	}
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrameData::setTransformAngle(unsigned short val)
{
	if(val > 360)
	{
		val %= 360;
	}
	if(isStreamFrame())
	{
		data = val;
		setStreamFrame(true);
	}
	else
	{
		data = val;
	}
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrameData::setData(unsigned short val)
{
	data = val;
}
//---------------------------------------------------------------------------

unsigned short __fastcall TAnimationFrameData::getData() const
{
	return data;
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrameData::Clear()
{
    type = UOP_MAX_LOGIC_DRAW_OBJ;
	_frameId = 0;
	posX = 0.0f;
	posY = 0.0f;
	rw = 0;
	rh = 0;
	a = 255;
	data = 0;
	strm_filename = _T("");
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


__fastcall TAnimationFrame::TAnimationFrame()
{
	Clear();
}
//---------------------------------------------------------------------------

__fastcall TAnimationFrame::~TAnimationFrame()
{
	Clear();
}
//---------------------------------------------------------------------------

__fastcall TAnimationFrame::TAnimationFrame(const TAnimationFrame &t)
{
	Clear();
	t.CopyTo(this);
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrame::CreateFromList(std::list<TAnimationFrameData>const &lst)
{
	__int32 minX, maxX;
	__int32 minY, maxY;
	std::list<TAnimationFrameData>::const_iterator t;
	mObjects.clear();
	mWidth = mHeight = mLeftOffset = mTopOffset = 0;
	minX = minY = INT_MAX;
	maxX = maxY = INT_MIN; //-0x80000000;
	mStreamObjWidth = mStreamObjHeight = 0;
	for (t = lst.begin(); t != lst.end(); ++t)
	{
		mObjects.push_back(*t);
		if(t->isStreamFrame())
		{
			mStreamObjWidth = t->rw;
			mStreamObjHeight = t->rh;
        }
		if (minX > (__int32)t->posX)
		{
			minX = (__int32)t->posX;
		}
		if (minY > (__int32)t->posY)
		{
			minY = (__int32)t->posY;
		}
		if (maxX < (__int32)t->posX + t->rw)
		{
			maxX = (__int32)t->posX + t->rw;
		}
		if (maxY < (__int32)t->posY + t->rh)
		{
			maxY = (__int32)t->posY + t->rh;
		}
	}
	if (!lst.empty())
	{
		mWidth = maxX - minX;
		mHeight = maxY - minY;
		mLeftOffset = minX;
		mTopOffset = minY;
	}
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrame::CopyFrameDataTo(std::list<TAnimationFrameData> &olist) const
{
	assert(olist.empty());
	std::list<TAnimationFrameData>::const_iterator t;
	for (t = mObjects.begin(); t != mObjects.end(); ++t)
	{
		olist.push_back(*t);
	}
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrame::Clear()
{
	mWidth = mHeight = mLeftOffset = mTopOffset = 0;
	mStreamObjWidth = mStreamObjHeight = 0;
	mObjects.clear();
	mCollideRect.Left = 0;
	mCollideRect.Top = 0;
	mCollideRect.Bottom = 0;
	mCollideRect.Right = 0;
	mDuration = 0;
	mEventId = _T("");
	for (__int32 i = 0; i < MAX_ANIMATON_JOIN_NODES; i++)
	{
		ZeroMemory(&mJoinNodes[i], sizeof(TPoint));
	}
}
//---------------------------------------------------------------------------

bool __fastcall TAnimationFrame::IsEmpty() const
{
	return mObjects.empty();
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrame::CopyTo(TAnimationFrame *target) const
{
	assert(target);
	target->Clear();
	std::list<TAnimationFrameData>::const_iterator t;
	for (t = mObjects.begin(); t != mObjects.end(); ++t)
	{
		target->mObjects.push_back(*t);
	}
	target->mCollideRect = mCollideRect;
	target->mWidth = mWidth;
	target->mHeight = mHeight;
	target->mLeftOffset = mLeftOffset;
	target->mTopOffset = mTopOffset;
	target->mDuration = mDuration;
	target->mStreamObjWidth = mStreamObjWidth;
	target->mStreamObjHeight = mStreamObjHeight;
	target->mEventId = mEventId;
	for (__int32 i = 0; i < MAX_ANIMATON_JOIN_NODES; i++)
	{
		target->mJoinNodes[i] = mJoinNodes[i];
	}
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrame::ReplaceBitmapFrame(const hash_t& oldHash, const TFrameObj &newObj)
{
	__int32 i;
	std::list<TAnimationFrameData> lst;
	std::list<TAnimationFrameData>::iterator t;
	hash_t h = newObj.getHash();
	CopyFrameDataTo(lst);
	for (t = lst.begin(); t != lst.end(); ++t)
	{
		if (t->frameId == oldHash)
		{
			if (newObj.w == 0 || newObj.h == 0)
			{
				 t = lst.erase(t);
				 t--;
			}
			else
			{
				t->SetFrameId(h);
				t->rw = newObj.w;
				t->rh = newObj.h;
			}
		}
	}
	CreateFromList(lst);
}
//---------------------------------------------------------------------------

__int32 __fastcall TAnimationFrame::GetDuration() const
{
	return mDuration;
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrame::SetDuration(__int32 duration)
{
	mDuration = duration;
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrame::SetCollideRect(TRect &r)
{
	mCollideRect = r;
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrame::SetAlpha(unsigned char a)
{
	__int32 i;
	std::list<TAnimationFrameData> lst;
	std::list<TAnimationFrameData>::iterator t;
	CopyFrameDataTo(lst);
	for (t = lst.begin(); t != lst.end(); ++t)
	{
		t->a = a;
	}
	CreateFromList(lst);
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TAnimationFrame::GetEventId() const
{
	return mEventId;
}
//---------------------------------------------------------------------------

void __fastcall TAnimationFrame::SetEventId(UnicodeString eventId)
{
	mEventId = eventId;
}
//---------------------------------------------------------------------------

TRect __fastcall TAnimationFrame::GetCollideRect() const
{
	return mCollideRect;
}
//---------------------------------------------------------------------------

__int32 __fastcall TAnimationFrame::GetWidth() const
{
	return mWidth;
}
//---------------------------------------------------------------------------

__int32 __fastcall TAnimationFrame::GetHeight() const
{
	return mHeight;
}
//---------------------------------------------------------------------------

__int32 __fastcall TAnimationFrame::GetLeftOffset() const
{
	return mLeftOffset;
}
//---------------------------------------------------------------------------

__int32 __fastcall TAnimationFrame::GetTopOffset() const
{
	return mTopOffset;
}
//---------------------------------------------------------------------------

bool __fastcall TAnimationFrame::SetJoinNodeCoordinate(__int32 idx, TPoint pt)
{
	if(idx >= 0 && idx < MAX_ANIMATON_JOIN_NODES)
	{
		mJoinNodes[idx] = pt;
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TAnimationFrame::GetJoinNodeCoordinate(__int32 idx, TPoint &pt) const
{
	if(idx >= 0 && idx < MAX_ANIMATON_JOIN_NODES)
	{
		pt = mJoinNodes[idx];
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TAnimationFrame::Intersect(__int32 left, __int32 top, __int32 right, __int32 bottom) const
{
	std::list<TAnimationFrameData>::const_iterator t;
	for (t = mObjects.begin(); t != mObjects.end(); ++t)
	{
		if((__int32)t->posY <= bottom && (__int32)t->posY + t->rh >= top &&
				(__int32)t->posX <= right && t->rw + (__int32)t->posX >= left)
		{
			return true;
        }
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TAnimationFrame::HasStreamObject() const
{
	return mStreamObjWidth> 0 && mStreamObjHeight > 0;
}
//---------------------------------------------------------------------------

unsigned short __fastcall TAnimationFrame::GetStreamObjectWidth() const
{
	return mStreamObjWidth;
}
//---------------------------------------------------------------------------

unsigned short __fastcall TAnimationFrame::GetStreamObjectHeight() const
{
	return mStreamObjHeight;
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

__fastcall TAnimation::TAnimation(__int32 size)
{
	Clear();
	Resize(size);
}
//---------------------------------------------------------------------------

__fastcall TAnimation::TAnimation(const TAnimation *anim)
{
	Clear();
	if(anim)
	{
		anim->CopyTo(this);
	}
}
//---------------------------------------------------------------------------

__fastcall TAnimation::TAnimation(const TAnimation &anim)
{
	Clear();
	anim.CopyTo(this);
}
//---------------------------------------------------------------------------

__fastcall TAnimation::~TAnimation()
{
	Clear();
}
//---------------------------------------------------------------------------

void __fastcall TAnimation::Resize(__int32 size)
{
	if(size < 0)
	{
		return;
	}
	__int32 dx = mFrames.size() - size;
	if (dx < 0)
	{
		while(dx)
		{
			mFrames.push_back(TAnimationFrame());
			dx++;
		}
	}
	else if(dx > 0)
	{
		std::list<TAnimationFrame>::iterator p;
		while(dx)
		{
			dx--;
			p = mFrames.end();
			p--;
			if(p != mFrames.begin())
			{
				mFrames.erase(p);
            }
		}
	}
	CheckForStreamObj();
}
//---------------------------------------------------------------------------

void __fastcall TAnimation::SetName(UnicodeString name)
{
	mName = name;
}
//---------------------------------------------------------------------------

const UnicodeString __fastcall TAnimation::GetName() const
{
	return mName;
}
//---------------------------------------------------------------------------

__int32 __fastcall TAnimation::GetFrameCount() const
{
	return mFrames.size();
}
//---------------------------------------------------------------------------

bool __fastcall TAnimation::SetFrame(const TAnimationFrame &obj, __int32 idx)
{
	if (idx >= 0 && idx < GetFrameCount())
	{
		std::list<TAnimationFrame>::iterator p;
		p = mFrames.begin();
		std::advance(p, idx);
		obj.CopyTo(&(*p));
		if(obj.HasStreamObject())
		{
			if(mStreamObjWidth == 0 && mStreamObjHeight == 0)
			{
				mStreamObjWidth = obj.GetStreamObjectWidth();
				mStreamObjHeight = obj.GetStreamObjectHeight();
            }
		}
		else
		{
			CheckForStreamObj();
        }
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TAnimation::InsertFrame(const TAnimationFrame &obj, __int32 idx)
{
	if (idx >= 0 && idx < GetFrameCount())
	{
		std::list<TAnimationFrame>::iterator p;
		p = mFrames.begin();
		std::advance(p, idx);
		mFrames.insert(p, obj);
		if(obj.HasStreamObject())
		{
			if(mStreamObjWidth == 0 && mStreamObjHeight == 0)
			{
				mStreamObjWidth = obj.GetStreamObjectWidth();
				mStreamObjHeight = obj.GetStreamObjectHeight();
            }
		}
		else
		{
			CheckForStreamObj();
        }
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TAnimation::MoveFrame(__int32 idxFrom, __int32 idxTo)
{
	if (idxFrom >= 0 && idxTo >= 0 && idxFrom < GetFrameCount() && idxTo < GetFrameCount())
	{
		std::list<TAnimationFrame>::iterator p1 = mFrames.begin();
		std::list<TAnimationFrame>::iterator p2 = mFrames.begin();
		std::advance(p1, idxFrom);
		std::advance(p2, idxTo);
		mFrames.insert(p2, *p1);
		mFrames.erase(p1);
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TAnimation::DeleteFrame(__int32 idx)
{
	if (idx >= 0 && idx < GetFrameCount())
	{
		std::list<TAnimationFrame>::iterator p;
		p = mFrames.begin();
		std::advance(p, idx);
		mFrames.erase(p);
		CheckForStreamObj();
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TAnimation::ClearFrame(__int32 idx)
{
	if (idx >= 0 && idx < GetFrameCount())
	{
		std::list<TAnimationFrame>::iterator p;
		p = mFrames.begin();
		std::advance(p, idx);
		p->Clear();
		CheckForStreamObj();
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

const TAnimationFrame* __fastcall TAnimation::GetFrame(__int32 idx) const
{
	if (idx >= 0 && idx < GetFrameCount())
	{
		std::list<TAnimationFrame>::const_iterator p;
		p = mFrames.begin();
		std::advance(p, idx);
		return &(*p);
	}
	return NULL;
}
//---------------------------------------------------------------------------

void __fastcall TAnimation::CopyTo(TAnimation *target) const
{
	assert(target);
	target->Clear();
	std::list<TAnimationFrame>::const_iterator o;
	for (o = mFrames.begin(); o != mFrames.end(); ++o)
	{
		TAnimationFrame a;
		o->CopyTo(&a);
		target->mFrames.push_back(a);
	}
	target->mName = mName;
	target->mLooped = mLooped;
	target->mViewRect = mViewRect;
	target->mStreamObjWidth = mStreamObjWidth;
	target->mStreamObjHeight = mStreamObjHeight;
}
//---------------------------------------------------------------------------

void __fastcall TAnimation::CheckForStreamObj()
{
	mStreamObjWidth = mStreamObjHeight = 0;
	std::list<TAnimationFrame>::const_iterator o;
	for (o = mFrames.begin(); o != mFrames.end(); ++o)
	{
		if(o->HasStreamObject())
		{
			mStreamObjWidth = o->GetStreamObjectWidth();
			mStreamObjHeight = o->GetStreamObjectHeight();
			break;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TAnimation::Clear()
{
	mFrames.clear();
	mLooped = true;
	mViewRect.Left = 0;
	mViewRect.Top = 0;
	mViewRect.Bottom = 0;
	mViewRect.Right = 0;
	mStreamObjWidth = mStreamObjHeight = 0;
}
//---------------------------------------------------------------------------

void __fastcall TAnimation::ReplaceBitmapFrame(const hash_t& oldHash, const TFrameObj &newObj)
{
	std::list<TAnimationFrame>::iterator o;
	for (o = mFrames.begin(); o != mFrames.end(); ++o)
	{
		o->ReplaceBitmapFrame(oldHash, newObj);
	}
}
//---------------------------------------------------------------------------

void __fastcall TAnimation::RenameEventName(UnicodeString oldName, UnicodeString newName)
{
	std::list<TAnimationFrame>::iterator o;
	for (o = mFrames.begin(); o != mFrames.end(); ++o)
	{
		if(o->GetEventId().Compare(oldName) == 0)
		{
			o->SetEventId(newName);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TAnimation::SetLooped(bool val)
{
	mLooped = val;
}
//---------------------------------------------------------------------------

bool __fastcall TAnimation::GetLooped() const
{
	return mLooped;
}
//---------------------------------------------------------------------------

void __fastcall TAnimation::SetViewRect(TRect &r)
{
	mViewRect = r;
}
//---------------------------------------------------------------------------

const TRect &__fastcall TAnimation::GetViewRect() const
{
	return mViewRect;
}
//---------------------------------------------------------------------------

bool __fastcall TAnimation::HasStreamObject() const
{
	return mStreamObjWidth > 0 && mStreamObjHeight > 0;
}
//---------------------------------------------------------------------------

unsigned short __fastcall TAnimation::GetStreamObjectWidth() const
{
	return mStreamObjWidth;
}
//---------------------------------------------------------------------------

unsigned short __fastcall TAnimation::GetStreamObjectHeight() const
{
	return mStreamObjHeight;
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

//ВСЕ ЧТО КАСАЕТСЯ GameObj

//---------------------------------------------------------------------------

__fastcall GameObj::GameObj()
{
}
//---------------------------------------------------------------------------

__fastcall GameObj::~GameObj()
{
}
//---------------------------------------------------------------------------

BMPImage *__fastcall GameObj::GetDrawDefaultBitmap()
{
	RebuildEmptyBmp();
	return empty_bmp;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//ВСЕ ЧТО КАСАЕТСЯ GMapBGObj

//---------------------------------------------------------------------------

__fastcall GMapBGObj::GMapBGObj()
{
	Create();
}
//---------------------------------------------------------------------------

__fastcall GMapBGObj::GMapBGObj(__int32 EmptyFrames)
{
	__int32 i;
	Create();
	for (i = 0; i < EmptyFrames; i++)
	{
		AssignFrameObj(NULL, i);
	}
}
//---------------------------------------------------------------------------

__fastcall GMapBGObj::GMapBGObj(const GMapBGObj& obj)
{
	Create();
	obj.CopyTo(this);
}
//---------------------------------------------------------------------------

void __fastcall GMapBGObj::Create()
{
	objId = objGBACKOBJ;
	ew = eh = 0;
	epmty_caption = _T("");
	Memo = _T("");
	bmp_lst = new TList();
}
//---------------------------------------------------------------------------

__fastcall GMapBGObj::~GMapBGObj()
{
	DelAllFrameObj();
	delete bmp_lst;
}
//---------------------------------------------------------------------------

 //пересобрать empty_bmp
void __fastcall GMapBGObj::RebuildEmptyBmp()
{
	TRect rect;

	if (empty_bmp != NULL)
	{
		delete empty_bmp;
	}
	empty_bmp = new BMPImage(new Graphics::TBitmap);

	if (ew <= 0 || eh <= 0)
		return;
	rect.Left = 0;
	rect.Top = 0;
	empty_bmp->SetSize(ew, eh);
	rect.Bottom = eh;
	rect.Right = ew;
	empty_bmp->Canvas->Brush->Color = clWhite;
	empty_bmp->Canvas->FillRect(rect);
	empty_bmp->Canvas->Font->Size = 5;
	empty_bmp->Canvas->Font->Color = clRed;
	empty_bmp->Canvas->Font->Name = _T("Small Fonts");
	empty_bmp->Canvas->TextOut(1, -1, epmty_caption);
	empty_bmp->Canvas->Brush->Color = clGray;
	empty_bmp->Canvas->FrameRect(rect);
}
//---------------------------------------------------------------------------

 //пересобрать empty_bmp
void __fastcall GMapBGObj::CreateEmptyBmp(__int32 w, __int32 h, UnicodeString caption)
{
	ew = w;
	eh = h;
	epmty_caption = caption;
}
//---------------------------------------------------------------------------

 //создать новый экземпляр
 //если fixedCopySize не 0, копируется указанное количество элементов
 //если fixedCopySize больше существующего количества массива картинок, все картинки
 //уходящие за пределы массива создаются пустыми.
 //Если fixedCopySize меньше существующего количества массива картинок
 //копируется только указанное количество
bool __fastcall GMapBGObj::CopyTo(GMapBGObj *tMapBGobj, __int32 fixedCopySize) const
{
	__int32 i, len;
	if (tMapBGobj == NULL)
	{
		return false;
	}
	tMapBGobj->DelAllFrameObj();
	len = (fixedCopySize > 0) ? fixedCopySize : bmp_lst->Count;
	for (i = 0; i < len; i++)
	{
		TFrameObj *o = NULL;
		if (bmp_lst->Count > i)
		{
			TFrameObj *to = (TFrameObj*)bmp_lst->Items[i];
			if(to)
			{
				o = new TFrameObj();
				to->copyTo(*o);
			}
		}
		tMapBGobj->AssignFrameObj(o, i);
	}
	tMapBGobj->Memo = Memo;
	return true;
}
//---------------------------------------------------------------------------

bool __fastcall GMapBGObj::CompareTo(GMapBGObj *tBGobj)
{
	__int32 i;
	bool res;
	TFrameObj *BGobj1, *BGobj2;
	if (tBGobj->bmp_lst->Count != bmp_lst->Count)
	{
		return false;
    }
	res = true;
	for (i = 0; i < bmp_lst->Count; i++)
	{
		BGobj1 = (TFrameObj*)bmp_lst->Items[i];
		BGobj2 = (TFrameObj*)tBGobj->bmp_lst->Items[i];
		if (res)
		{
			if (BGobj1 == NULL || BGobj2 == NULL)
			{
				if(BGobj1 != BGobj2)
				{
					res = false;
					break;
				}
			}
			else
			{
				res = BGobj1->compareTo(*BGobj2);
            }
		}
		else
		{
			break;
        }
	}
	return res;
}
//---------------------------------------------------------------------------

void __fastcall GMapBGObj::DelAllFrameObj()
{
	for (__int32 i = 0; i < bmp_lst->Count; i++)
	{
		if(bmp_lst->Items[i] != NULL)
		{
			delete (TFrameObj*)bmp_lst->Items[i];
        }
	}
	bmp_lst->Clear();
}
//---------------------------------------------------------------------------

__int32 __fastcall GMapBGObj::GetFrameObjCount() const
{
	return bmp_lst->Count;
}
//---------------------------------------------------------------------------

bool __fastcall GMapBGObj::IsEmpty() const
{
	TFrameObj *o;
	bool res = true;
	for (__int32 i = 0; i < GetFrameObjCount(); i++)
	{
		o = (TFrameObj*)bmp_lst->Items[i];
		if (o != NULL && !o->IsEmpty())
		{
			res = false;
			break;
		}
	}
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall GMapBGObj::AssignFrameObj(TFrameObj *obj, __int32 img_idx)
{
	__int32 i;
	__int32 l = bmp_lst->Count;
	for (i = l; i <= img_idx; i++)
	{
		bmp_lst->Add(NULL);
	}
	if(bmp_lst->Items[img_idx] != NULL)
	{
		delete (TFrameObj*)bmp_lst->Items[img_idx];
    }
	bmp_lst->Items[img_idx] = obj;
	return true;
}
//---------------------------------------------------------------------------

bool __fastcall GMapBGObj::ClearFrameObj(__int32 img_idx)
{
	__int32 maxImages = GetFrameObjCount();
	if (img_idx >= 0 && img_idx < maxImages)
	{
		if(bmp_lst->Items[img_idx] != NULL)
		{
			delete (TFrameObj*)bmp_lst->Items[img_idx];
		}
		bmp_lst->Items[img_idx] = NULL;

		__int32 empty_count = 0;
		for (__int32 i = 0; i < bmp_lst->Count; i++)
		{
			if(bmp_lst->Items[i] == NULL)
			{
				empty_count++;
			}
		}
		if(bmp_lst->Count == empty_count)
		{
			bmp_lst->Clear();
		}

		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall GMapBGObj::MoveFrameObj(__int32 from_idx, __int32 to_idx)
{
	__int32 maxImages = GetFrameObjCount();
	if (from_idx >= 0 && from_idx < maxImages && to_idx >= 0 && to_idx < maxImages)
	{
		bmp_lst->Move(from_idx, to_idx);
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

TFrameObj*__fastcall GMapBGObj::GetFrameObj(__int32 img_idx) const
{
	const __int32 maxImages = GetFrameObjCount();
	if (img_idx >= 0 && img_idx < maxImages)
	{
		return(TFrameObj*)bmp_lst->Items[img_idx];
	}
	return NULL;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//ВСЕ ЧТО КАСАЕТСЯ GPerson

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

__fastcall TMapPersonCasheData::TMapPersonCasheData()
{
	m_Err_bmp_idx = -1;
	m_Err_bmp_name = _T("\0");
	Clear();
}

__fastcall TMapPersonCasheData::~TMapPersonCasheData()
{
	Clear();
}

void __fastcall TMapPersonCasheData::Clear()
{
	for(__int32 i = 0; i < MAX_ANIMATON_JOIN_NODES; i++)
	{
		m_pChildsLinkObj[i] = NULL;
	}
	m_DynamicObjFrameHash = 0;
	m_DynamicObjResId = _T("");
	m_DynamicObjTempFilename = _T("");
	m_pParentRes = NULL;
	m_pState = NULL;
	m_stateName = _T("");
	m_pParentLinkObj = NULL;
	directors[0] = directors[1] = directors[2] = directors[3] = NULL;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

__fastcall GPerson::GPerson()
{
	objId = objGPOBJ;
	mName = _T("");
	mDefaultStateName = _T("");
	mResOptimization = true;
	mUseGraphics = true;
	mIsDecoration = false;
	mDecorType = decorNONE;
	mGroupId = 0;
	mpIconData = NULL;
}
//---------------------------------------------------------------------------

__fastcall GPerson::~GPerson()
{
	Clear();
	if(mpIconData != NULL)
	{
		delete mpIconData;
		mpIconData = NULL;
	}
}
//---------------------------------------------------------------------------

UnicodeString __fastcall GPerson::IdToName(const UnicodeString &id)
{
	static std::map<UnicodeString, UnicodeString> m =
	{
		{TRIGGER_ID, TRIGGER_NAME},
		{DIRECTOR_ID, DIRECTOR_NAME},
		{TEXTBOX_ID, TEXTBOX_NAME},
		{SOUNDSCHEME_ID, SOUNDSCHEME_NAME},
		{OBJPOOL_ID, OBJPOOL_NAME}
	};

	std::map<UnicodeString, UnicodeString>::const_iterator it = m.find(id);
	if(it != m.end())
	{
		return it->second;
	}

	return id;
}

UnicodeString __fastcall GPerson::NameToId(const UnicodeString &name)
{
	static std::map<UnicodeString, UnicodeString> m =
	{
		{TRIGGER_NAME, TRIGGER_ID},
		{DIRECTOR_NAME, DIRECTOR_ID},
		{TEXTBOX_NAME, TEXTBOX_ID},
		{SOUNDSCHEME_NAME, SOUNDSCHEME_ID},
		{OBJPOOL_NAME, OBJPOOL_ID}
	};

	std::map<UnicodeString, UnicodeString>::const_iterator it = m.find(name);
	if(it != m.end())
	{
		return it->second;
	}

	return name;
}

void __fastcall GPerson::RebuildEmptyBmp()
{
	TRect rect;

	if (empty_bmp != NULL)
	{
		delete empty_bmp;
	}
	empty_bmp = new BMPImage(new Graphics::TBitmap);

	__int32 w, h;
	empty_bmp->Canvas->Font->Size = 5;
	empty_bmp->Canvas->Font->Color = clRed;
	empty_bmp->Canvas->Font->Name = _T("Small Fonts");
	w = (empty_bmp->Canvas->TextWidth(mName) + 6);
	h = 32;
	rect.Left = 0;
	rect.Top = 0;
	empty_bmp->SetSize(w, h);
	rect.Bottom = h;
	rect.Right = w;
	empty_bmp->Canvas->Brush->Color = clWhite;
	empty_bmp->Canvas->FillRect(rect);
	empty_bmp->Canvas->Pen->Color = clBlack;
	empty_bmp->Canvas->Pen->Style = psDot;
	empty_bmp->Canvas->MoveTo(w / 2, 0);
	empty_bmp->Canvas->LineTo(w / 2, h);
	empty_bmp->Canvas->TextOut(2, h - empty_bmp->Canvas->TextHeight(_T("N")) - 5, mName);
	empty_bmp->Canvas->Brush->Color = clGray;
	empty_bmp->Canvas->FrameRect(rect);
}
//---------------------------------------------------------------------------

bool __fastcall GPerson::IsEmpty() const
{
	return mAnimMap.size() == 0;
}
//---------------------------------------------------------------------------

void __fastcall GPerson::AssignPattern(TObjPattern *newPt)
{
	GetBaseProperties().assignPattern(newPt);
}
//---------------------------------------------------------------------------

bool __fastcall GPerson::CopyTo(GPerson *Gpers) const
{
	__int32 i, len;
	if (Gpers == NULL)
	{
		return false;
	}
	Gpers->Clear();
	Gpers->GetBaseProperties().assignProperties(GetBaseProperties());
	Gpers->mName = mName;
	Gpers->mDefaultStateName = mDefaultStateName;
	Gpers->mUseGraphics = mUseGraphics;
	Gpers->mResOptimization = mResOptimization;
	Gpers->mIsDecoration = mIsDecoration;
	Gpers->mGroupId = mGroupId;
	Gpers->mDecorType = mDecorType;

	if (Gpers->mpIconData)
	{
		delete Gpers->mpIconData;
	}
	Gpers->mpIconData = NULL;
	if (mpIconData)
	{
		Gpers->mpIconData = new TAnimationFrame();
		mpIconData->CopyTo(Gpers->mpIconData);
	}

	for (std::map<const UnicodeString, TAnimation*>::const_iterator it = mAnimMap.begin(); it != mAnimMap.end(); ++it)
	{
		TAnimation *a = new TAnimation(it->second);
		Gpers->AssignAnimation(a);
	}
	return true;
}
//---------------------------------------------------------------------------

void __fastcall GPerson::SetName(UnicodeString str)
{
	mName = str;
}
//---------------------------------------------------------------------------

GPersonProperties& __fastcall GPerson::GetBaseProperties()
{
	return properties;
}
//---------------------------------------------------------------------------

const GPersonProperties& __fastcall GPerson::GetBaseProperties() const
{
	return properties;
}
//---------------------------------------------------------------------------

void __fastcall GPerson::AssignAnimation(TAnimation *obj)
{
	assert(obj);
	UnicodeString name = obj->GetName();
	assert(!name.IsEmpty());
	assert(GetAnimation(name) != obj);
	DeleteAnimation(name);
	mAnimMap[name.c_str()] = obj;
}
//---------------------------------------------------------------------------

void __fastcall GPerson::DeleteAnimation(const UnicodeString& name)
{
	std::map<const UnicodeString, TAnimation*>::iterator it;
	it = mAnimMap.find(name);
	if (it != mAnimMap.end())
	{
		assert(it->second);
		delete it->second;
		mAnimMap.erase(it);
	}
}
//---------------------------------------------------------------------------

TAnimation *__fastcall GPerson::GetAnimation(const UnicodeString& name) const
{
	std::map<const UnicodeString, TAnimation*>::const_iterator it;
	it = mAnimMap.find(name);
	if (it != mAnimMap.end())
	{
		assert(it->second);
		return it->second;
	}
	return NULL;
}
//---------------------------------------------------------------------------

void __fastcall GPerson::Clear()
{
	for (std::map<const UnicodeString, TAnimation*>::iterator it = mAnimMap.begin(); it != mAnimMap.end(); ++it)
	{
		assert(it->second);
		delete (TAnimation*)it->second;
	}
	mAnimMap.clear();
}
//---------------------------------------------------------------------------

void __fastcall GPerson::SetIconData(const TAnimationFrame *obj)
{
	if (mpIconData)
	{
		delete mpIconData;
	}
	if(obj != NULL)
	{
		mpIconData = new TAnimationFrame();
		obj->CopyTo(mpIconData);
	}
}
//---------------------------------------------------------------------------

const TAnimationFrame *__fastcall GPerson::GetIconData() const
{
	return mpIconData;
}
//---------------------------------------------------------------------------

void __fastcall GPerson::GetBitmapFrameList(TList *olist)
{
	assert(olist);
	std::list<TAnimationFrameData>::const_iterator afd;
	std::list<TAnimationFrameData> lst;
	for (std::map<const UnicodeString, TAnimation*>::iterator it = mAnimMap.begin(); it != mAnimMap.end(); ++it)
	{
		TAnimation *a = it->second;
		assert(a);
		__int32 ct = a->GetFrameCount();
		for (__int32 i = 0; i < ct; i++)
		{
			const TAnimationFrame *f = a->GetFrame(i);
			f->CopyFrameDataTo(lst);
			for (afd = lst.begin(); afd != lst.end(); ++afd)
			{
				void* pfidx = reinterpret_cast<void*>(afd->frameId);
				if (olist->IndexOf(pfidx) < 0)
				{
					olist->Add(pfidx);
				}
			}
			lst.clear();
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall GPerson::ReplaceBitmapFrame(const hash_t& oldHash, const TFrameObj &newObj)
{
	for (std::map<const UnicodeString, TAnimation*>::iterator it = mAnimMap.begin(); it != mAnimMap.end(); ++it)
	{
		TAnimation* a = it->second;
		assert(a);
		a->ReplaceBitmapFrame(oldHash, newObj);
	}
	if(mpIconData)
	{
		mpIconData->ReplaceBitmapFrame(oldHash, newObj);
    }
}
//---------------------------------------------------------------------------

void __fastcall GPerson::RenameEventNameInAnimations(const UnicodeString& oldName, const UnicodeString& newName)
{
	for (std::map<const UnicodeString, TAnimation*>::iterator it = mAnimMap.begin(); it != mAnimMap.end(); ++it)
	{
		TAnimation* a = it->second;
		assert(a);
		a->RenameEventName(oldName, newName);
	}
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//ВСЕ ЧТО КАСАЕТСЯ TMapGPerson

__fastcall TPropertyWindow::TPropertyWindow()
{
	prpWnd = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TPropertyWindow::RegisterPrpWnd(TPrpFunctions *iprpWnd)
{
	prpWnd = iprpWnd;
}
//---------------------------------------------------------------------------

void __fastcall TPropertyWindow::UnregisterPrpWnd()
{
	prpWnd = NULL;
}
//---------------------------------------------------------------------------

__fastcall TMapPerson::TMapPerson(TObjManager* owner)
{
    mEndAnimation = false;
	mpAnimation = NULL;
	mCurrentFrame = 0;
	mFixedSizes = false;
	mScreenLeft = mScreenTop = 0.0f;
	mWidth = mHeight = 0;
	InterlacedTransparency = false;
	mEnabled = true;
	mZoneIndex = -1;
	CasheDataIndex = -1;
	Memo = _T("");
	showRect = true;
	Parent_Name = _T("");
	SetVisible(true);
	SetCustomVisible(true);
	Name = _T("");
	shortHint = false;
	mpParent = owner;
	off_x = NULL;
	off_y = NULL;
	mCurrentFrameDuration = 0;
    mLocked = false;
}
//---------------------------------------------------------------------------

__fastcall TMapPerson::TMapPerson(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString GPName, TObjManager *Owner)
{
	mEndAnimation = false;
	mpAnimation = NULL;
	mCurrentFrame = 0;
	mScreenLeft = mScreenTop = 0.0f;
	mWidth = mHeight = 0;
	mFixedSizes = false;
	InterlacedTransparency = false;
	mEnabled = true;
	CasheDataIndex = -1;
	mZoneIndex = -1;
	Memo = _T("");
	showRect = true;
	Parent_Name = _T("");
	SetVisible(true);
	SetCustomVisible(true);
	Name = _T("");
	Parent_Name = GPName;
	shortHint = false;
	mpParent = Owner;
	off_x = pos_x;
	off_y = pos_y;
    mTransform.SetPosition(x, y);
	mCurrentFrameDuration = 0;
	mLocked = false;
}
//---------------------------------------------------------------------------

__fastcall TMapPerson::~TMapPerson()
{
	FreeAnimaData();
	if (mpAnimation != NULL)
	{
		delete mpAnimation;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapPerson::FreeAnimaData()
{
    mEndAnimation = false;
	mAnimaData.clear();
	ClearSizes();
}
//---------------------------------------------------------------------------

void __fastcall TMapPerson::AssignAnimation(const TAnimation *anim)
{
	if (mpAnimation != NULL)
	{
		assert(anim != mpAnimation);
		delete mpAnimation;
		mpAnimation = NULL;
	}
	if (anim != NULL)
	{
		mpAnimation = new TAnimation(anim);
	}
	RestartAnimation();
}
//---------------------------------------------------------------------------

void __fastcall TMapPerson::ApplyAnimationFrame(const TAnimationFrame *af)
{
	FreeAnimaData();
	if (af != NULL)
	{
		af->CopyFrameDataTo(mAnimaData);
		mCurrentFrameDuration = af->GetDuration();
	}
	else
	{
		mCurrentFrameDuration = 0;
    }
	inner_realign();
}
//---------------------------------------------------------------------------

void __fastcall TMapPerson::RestartAnimation()
{
	const TAnimationFrame *af = NULL;
	mCurrentFrame = 0;
	if (mpAnimation != NULL && mpAnimation->GetFrameCount() > 0)
	{
		af = mpAnimation->GetFrame(mCurrentFrame);
	}
	ApplyAnimationFrame(af);
}
//---------------------------------------------------------------------------

void __fastcall TMapPerson::CopyTo(TMapPerson *mgp) const
{
	if (mgp == NULL)
	{
		return;
	}
	assert(mgp->mObjId == mObjId);

	mgp->FreeAnimaData();
	mgp->Name = Name;
	mgp->mScreenLeft = mScreenLeft;
	mgp->mScreenTop = mScreenTop;
	mgp->mWidth = mWidth;
	mgp->mHeight = mHeight;
	mgp->mTransform = mTransform;
	mgp->mZoneIndex = mZoneIndex;
	mgp->SetVisible(Visible);
	mgp->SetCustomVisible(CustomVisible);
	mgp->Enabled = Enabled;
	mgp->ShowRect(showRect);
	mgp->Parent_Name = Parent_Name;
	mgp->off_x = off_x;
	mgp->off_y = off_y;
	mgp->OnGlobalScale();
	mgp->mHint = mHint;
	mgp->SetShortHint(shortHint);
	mgp->CasheDataIndex = CasheDataIndex;
	mgp->setRemarks(Memo);
	mgp->AssignAnimation(NULL); //will create during cashe creation
	mgp->mCurrentFrameDuration = 0;
	mgp->InterlacedTransparency = InterlacedTransparency;
	mgp->mLocked = mLocked;
}
//---------------------------------------------------------------------------

const std::list<TAnimationFrameData>& __fastcall TMapPerson::GetAnimationFrameData() const
{
	return mAnimaData;
}
//---------------------------------------------------------------------------

const TAnimationFrame* __fastcall TMapPerson::GetCurrentAnimationFrame() const
{
	const TAnimationFrame *f;
	if(GetAnimation() != NULL)
	{
		f = GetAnimation()->GetFrame(mCurrentFrame);
	}
	else
	{
		f = NULL;
	}
	return f;
}
//---------------------------------------------------------------------------

void __fastcall TMapPerson::SetVisible(bool val)
{
	mVisible = val;
}
//---------------------------------------------------------------------------

void __fastcall TMapPerson::SetCustomVisible(bool val)
{
	mCustomVisible = val;
}
//---------------------------------------------------------------------------

const TAnimation* __fastcall TMapPerson::GetAnimation() const
{
	return mpAnimation;
}
//---------------------------------------------------------------------------

void __fastcall TMapPerson::ClearSizes()
{
	mWidth = 0;
	mHeight = 0;
}
//---------------------------------------------------------------------------

void __fastcall TMapPerson::getCoordinates(double *x, double *y) const
{
	double ox, oy;
	mTransform.GetPosition(ox, oy);
	if (y != nullptr)
	{
		*y = oy;
	}
	if (x != nullptr)
	{
		*x = ox;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMapPerson::getScreenCoordinates(double *x, double *y) const
{
	if (y != NULL)
	{
		*y = mScreenTop;
	}
	if (x != NULL)
	{
		*x = mScreenLeft;
	}
}
//---------------------------------------------------------------------------

bool __fastcall TMapPerson::Intersect(__int32 left, __int32 top, __int32 right, __int32 bottom)
{
	double offScale = mFixedSizes ? 1.0f : gsGlobalScale;

	if(mpAnimation != NULL)
	{
		const TAnimationFrame *f = mpAnimation->GetFrame(mCurrentFrame);
		assert(f);
		if(!f->IsEmpty())
		{
			return f->Intersect((__int32)(((double)left - (mScreenLeft - (double)f->GetLeftOffset() * offScale)) / offScale),
								(__int32)(((double)top - (mScreenTop - (double)f->GetTopOffset() * offScale)) / offScale),
								(__int32)(((double)right - (mScreenLeft - (double)f->GetLeftOffset() * offScale)) / offScale),
								(__int32)(((double)bottom - (mScreenTop - (double)f->GetTopOffset() * offScale)) / offScale));
		}
	}

	return (__int32)mScreenTop <= bottom && (__int32)mScreenTop + mHeight >= top &&
			(__int32)mScreenLeft <= right && mWidth + (__int32)mScreenLeft >= left;
}
//---------------------------------------------------------------------------


void __fastcall TMapPerson::changeHint()
{
	double ox, oy;
	mTransform.GetPosition(ox, oy);
	const int x = static_cast<int>(ox);
	const int y = static_cast<int>(oy);
	mHint = getGParentName() + _T(":") + Name + _T("\nX:") + IntToStr(x) + _T("; Y:") + IntToStr(y);
	if (!Memo.IsEmpty() && !shortHint)
	{
		mHint += _T("\n") + Memo;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMapPerson::SetShortHint(bool val)
{
	shortHint = val;
	changeHint();
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TMapPerson::getGParentName() const
{
	return Parent_Name;
}
//---------------------------------------------------------------------------

void __fastcall TMapPerson::Lock(bool val)
{
	mLocked = val;
}
//---------------------------------------------------------------------------

void __fastcall TMapPerson::ShowInterlacedTransparency(bool val)
{
	InterlacedTransparency = val;
}
//---------------------------------------------------------------------------

const TTransformProperties& __fastcall TMapPerson::GetTransform() const
{
    return mTransform;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
__fastcall TMapGPerson::TMapGPerson(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString GPName, TObjManager *Owner)
: TMapPerson(x, y, pos_x, pos_y, GPName, Owner)
{
	mObjId = objMAPOBJ;
	mFixedSizes = false;
	InspectEndAnimation = false;
	hasLinks = false;
	showEnabled = true;
	FState_Name = _T("");
	dirList = new TStringList;
	scriptList = new TStringList();
	mZoneIndex = -1;
	TraceTriggerActions = false;
	mpChilds = new TStringList();
	ParentObj = _T("");
	NextLinkedDirector = _T("");
	DoSetCoordinates(x, y, 0, 0);
	ResetPrevTransform();
	mDecorType = decorNONE;
	changeHint();
}
//---------------------------------------------------------------------------

__fastcall TMapGPerson::TMapGPerson(TObjManager *owner)
: TMapPerson(owner)
{
	mObjId = objMAPOBJ;
	mFixedSizes = false;
	InspectEndAnimation = false;
	hasLinks = false;
	FState_Name = _T("");
	showEnabled = true;
	dirList = new TStringList();
	scriptList = new TStringList();
	mpChilds = new TStringList();
	TraceTriggerActions = false;
	ParentObj = _T("");
	NextLinkedDirector = _T("");
	ResetPrevTransform();
	mDecorType = decorNONE;
	changeHint();
}
//---------------------------------------------------------------------------

__fastcall TMapGPerson::~TMapGPerson()
{
	delete dirList;
	delete scriptList;
	delete mpChilds;
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::AssignAnimation(const TAnimation *anim)
{
	TMapPerson::AssignAnimation(anim);
	if(mpChilds->Count > 0)
	{
		mpParent->ChangeChildPosition(this);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::ClearSizes()
{
	TMapPerson::ClearSizes();
	ZeroMemory(&mCollideRect, sizeof(TRect));
	ZeroMemory(&mViewRect, sizeof(TRect));
}
//---------------------------------------------------------------------------

bool __fastcall TMapGPerson::SetDecorType(TMapObjDecorType type)
{
	if(type == mDecorType)
	{
		return false;
	}

	if(type != decorNONE)
	{
		if(!ParentObj.IsEmpty())
		{
			TMapGPerson *ch = (TMapGPerson*)mpParent->FindObjectPointer(ParentObj);
			if (ch != NULL)
			{
				for (__int32 j = 0; j < MAX_ANIMATON_JOIN_NODES; j++)
				{
					UnicodeString chname = ch->GetChild(j);
					if (!chname.IsEmpty() && Name.Compare(chname) == 0)
					{
						ch->AddChild(j, UnicodeString());
					}
				}
				mpParent->ClearCasheFor(ch);
			}
		}
		ParentObj = _T("");
		for (__int32 j = 0; j < MAX_ANIMATON_JOIN_NODES; j++)
		{
			UnicodeString chname = GetChild(j);
			if (!chname.IsEmpty())
			{
				AddChild(j, UnicodeString());
			}
		}
    }

    __int32 pos;
	switch(mDecorType)
	{
		case decorNONE:
			if((pos = mpParent->map_u->IndexOf(this)) >= 0)
			{
				mpParent->map_u->Delete(pos);
			}
			break;
		case decorBACK:
			if((pos = mpParent->decor_b->IndexOf(this)) >= 0)
			{
				mpParent->decor_b->Delete(pos);
			}
			break;
		case decorFORE:
			if((pos = mpParent->decor_f->IndexOf(this)) >= 0)
			{
				mpParent->decor_f->Delete(pos);
			}
	}
	mDecorType = type;
	switch(mDecorType)
	{
		case decorNONE:
			mpParent->map_u->Add(this);
			break;
		case decorBACK:
			mpParent->decor_b->Add(this);
			break;
		case decorFORE:
			mpParent->decor_f->Add(this);
	}
	mpParent->ClearCasheFor(this);
	return true;
}
//---------------------------------------------------------------------------

TMapObjDecorType __fastcall TMapGPerson::GetDecorType() const
{
	return mDecorType;
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::ResetPrevTransform()
{
	mPrevTransform = mTransform;
	PrevLinkedDirector = _T("");
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::setRemarks(UnicodeString str)
{
	Memo = str;
	changeHint();
	if (prpWnd != NULL)
		prpWnd->SetRemarks(str);
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::SetNodeName(UnicodeString NodeName)
{
	FNextLinkedDirector = NodeName;
	if (prpWnd != NULL)
	{
		prpWnd->SetNode(NodeName);
    }
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::SetZone(__int32 zone)
{
	mZoneIndex = zone;
	if (prpWnd != NULL)
	{
		prpWnd->SetZone(zone);
    }
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::Lock(bool val)
{
	TMapPerson::Lock(val);
	if (prpWnd != nullptr)
	{
		prpWnd->Lock(val);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::changeHint()
{
	TMapPerson::changeHint();
	if (mDecorType == decorNONE && GetObjType() == objMAPOBJ)
	{
		UnicodeString str = FState_Name;
		if(str.IsEmpty())
		{
			str = Localization::Text(_T("mUndefined"));
        }
		mHint += _T("\n") + Localization::Text(_T("mPrpObjState")) + _T(" ") + str;
	}
}
//---------------------------------------------------------------------------

bool __fastcall TMapGPerson::GetVarValue(double *val, UnicodeString var_name)
{
	return properties.getVarValue(val, var_name);
}
//---------------------------------------------------------------------------

bool __fastcall TMapGPerson::SetVarValue(double val, UnicodeString var_name)
{
	bool res = properties.setVarValue(val, var_name);
	if (prpWnd != NULL)
	{
		prpWnd->SetVarValue(val, var_name);
    }
	return res;
}
//---------------------------------------------------------------------------

bool __fastcall TMapGPerson::GetDefValue(double *val, UnicodeString var_name)
{
	return properties.getDefValue(val, var_name);
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::SetVarValuesToDef()
{
	properties.setVarValuesToDef();
	if (prpWnd != NULL)
	{
		prpWnd->refreshVars();
	}
}
//---------------------------------------------------------------------------

bool __fastcall TMapGPerson::SetCheckedValue(bool var, UnicodeString var_name)
{
	return properties.setChecked(var, var_name);
}
//---------------------------------------------------------------------------

bool __fastcall TMapGPerson::GetCheckedValue(bool *var, UnicodeString var_name)
{
	return properties.getChecked(var, var_name);
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::assignNewParent(UnicodeString iGPParent)
{
	Parent_Name = iGPParent;
	changeHint();
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::setCoordinates(double x, double y, __int32 dx, __int32 dy)
{
	if(ParentObj.IsEmpty())
	{
		DoSetCoordinates(x, y, dx, dy);
    }
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::DoSetCoordinates(double x, double y, __int32 dx, __int32 dy)
{
	mTransform.SetPosition(x + (double)dx, y + (double)dy);

	if(mpChilds->Count > 0)
	{
		mpParent->ChangeChildPosition(this);
	}
	inner_realign();
	changeHint();
	if (prpWnd != NULL)
	{
		prpWnd->SetTransform(mTransform);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::inner_realign()
{
	double ox, oy;
	mTransform.GetPosition(ox, oy);

	if (GetAnimation() != nullptr)
	{
		const TAnimationFrame *f = GetAnimation()->GetFrame(mCurrentFrame);
        assert(f);
		TRect r = f->GetCollideRect();
		mCollideRect.Left = static_cast<int>(ox) + r.Left;
		mCollideRect.Top = static_cast<int>(oy) + r.Top;
		mCollideRect.Bottom = mCollideRect.Top + r.Height();
		mCollideRect.Right = mCollideRect.Left + r.Width();

		r = GetAnimation()->GetViewRect();
		mViewRect.Left = static_cast<int>(ox) + r.Left;
		mViewRect.Top = static_cast<int>(oy) + r.Top;
		mViewRect.Bottom = mViewRect.Top + r.Height();
		mViewRect.Right = mViewRect.Left + r.Width();

		mWidth = static_cast<int>((double)f->GetWidth() * gsGlobalScale);
		mHeight = static_cast<int>((double)f->GetHeight() * gsGlobalScale);
		mOffPosX = f->GetLeftOffset();
		mOffPosY = f->GetTopOffset();

		if(f->IsEmpty()) // error case
		{
			mOffPosX = -ERROR_IMAGE_WIDTH / 2;
			mOffPosY = -ERROR_IMAGE_HEIGHT / 2;
			mWidth = static_cast<int>((double)ERROR_IMAGE_WIDTH * gsGlobalScale);
			mHeight = static_cast<int>((double)ERROR_IMAGE_HEIGHT * gsGlobalScale);
		}
	}
	else
	{
		ClearSizes();

		mOffPosX = -ERROR_IMAGE_WIDTH / 2;
		mOffPosY = -ERROR_IMAGE_HEIGHT / 2;
		mWidth = static_cast<int>((double)ERROR_IMAGE_WIDTH * gsGlobalScale);
		mHeight = static_cast<int>((double)ERROR_IMAGE_HEIGHT * gsGlobalScale);
	}

	if (off_x == nullptr || off_y == nullptr)
	{
		assert(0);
		return;
	}

	mScreenLeft = (ox + (double)mOffPosX) * gsGlobalScale - (double)*off_x;
	mScreenTop = (oy + (double)mOffPosY) * gsGlobalScale - (double)*off_y;
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::OnScrollBarChange()
{
	inner_realign();
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::ProcessAnimation(unsigned __int32 ms)
{
	mEndAnimation = false;
	mCurrentFrameDuration -= ms;
	if(mCurrentFrameDuration <= 0)
	{
		if (GetAnimation() != NULL && GetAnimation()->GetFrameCount() > 0)
		{
			bool ea = false;
			mCurrentFrame++;
			if (mCurrentFrame >= GetAnimation()->GetFrameCount())
			{
				mCurrentFrame = GetAnimation()->GetFrameCount() - 1;
				if (GetAnimation()->GetLooped())
				{
					mCurrentFrame = 0;
				}
				ea = true;
			}
			const TAnimationFrame *af = GetAnimation()->GetFrame(mCurrentFrame);
			ApplyAnimationFrame(af);
			mCurrentFrameDuration = af->GetDuration();
			mEndAnimation = ea;
		}
		else
		{
			mCurrentFrameDuration = 0;
		}
	}
	if(mpChilds->Count > 0)
	{
		mpParent->ChangeChildPosition(this);
	}
}
//---------------------------------------------------------------------------

bool __fastcall TMapGPerson::IsEndAnimation()
{
	return mEndAnimation;
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::SetStateName(UnicodeString name)
{
    double x, y;
	RestartAnimation();
	if (FState_Name.Compare(name) != 0)
	{
		InspectEndAnimation = false;
	}
	FState_Name = name;
	changeHint();
	if (prpWnd != NULL)
	{
		prpWnd->SetState(name);
    }
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::SetProperties(const GPersonProperties &new_prp)
{
	properties.assignProperties(new_prp);
	hasLinks = false;
	changeHint();
	if (prpWnd != NULL)
	{
		prpWnd->SetVariables(&new_prp);
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::OnGlobalScale()
{
	inner_realign();
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::CopyTo(TMapGPerson *mgp) const
{
	if (mgp == NULL)
	{
		return;
	}
	mgp->SetDecorType(GetDecorType());
	mgp->NextLinkedDirector = NextLinkedDirector;
	mgp->PrevLinkedDirector = PrevLinkedDirector;
	mgp->mPrevTransform = mPrevTransform;
	mgp->linkSeek = linkSeek;
	mgp->hasLinks = hasLinks;
	mgp->showRect = showRect;
	mgp->dirList->Assign(dirList);
	mgp->scriptList->Assign(scriptList);
	mgp->InterlacedTransparency = InterlacedTransparency;
	mgp->InspectEndAnimation = InspectEndAnimation;
	mgp->showEnabled = showEnabled;
	mgp->mpChilds->Assign(mpChilds);
	mgp->ParentObj = ParentObj;
	mgp->TraceTriggerActions = TraceTriggerActions;
	mgp->SetCustomVisible(CustomVisible);
	mgp->FState_Name = FState_Name;
	mgp->mLocked = mLocked;
	TMapPerson::CopyTo(mgp);
	//это исполняется в самом конце, так как влияет на changeHint()
	mgp->properties.assignProperties(properties);
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::ShowRect(bool val)
{
	showRect = val;
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::ShowEnabled(bool val)
{
	if (showEnabled != val)
	{
		showEnabled = val;
	}
}
//---------------------------------------------------------------------------

__int32 __fastcall TMapGPerson::clearDirStates()
{
	dirList->Clear();
	return 1;
}
//---------------------------------------------------------------------------

__int32 __fastcall TMapGPerson::clearScripts()
{
	scriptList->Clear();
	return 1;
}
//---------------------------------------------------------------------------

__int32 __fastcall TMapGPerson::getDirStatesCount() const
{
	return dirList->Count;
}
//---------------------------------------------------------------------------

__int32 __fastcall TMapGPerson::getScriptsCount() const
{
	return scriptList->Count;
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::setDirStateItem(__int32 idx, UnicodeString StateName)
{
	if (idx >= 0 && idx < dirList->Count)
	{
		dirList->Strings[idx] = StateName;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::deleteDirStateItem(__int32 idx)
{
	if (idx >= 0 && idx < dirList->Count)
	{
		dirList->Delete(idx);
	}
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TMapGPerson::getDirStateItem(__int32 idx) const
{
	if (idx >= 0 && idx < dirList->Count)
	{
		return dirList->Strings[idx];
	}
	return _T("");
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TMapGPerson::getScriptName(UnicodeString KeyName) const
{
	__int32 i;
	for (i = 0; i < scriptList->Count; i++)
		if (scriptList->Names[i].Compare(KeyName) == 0)
		{
			return scriptList->Values[scriptList->Names[i]];
		}
	return _T("");
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::addDirStateItem(UnicodeString StateName)
{
	dirList->Add(StateName);
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::setScriptName(UnicodeString KeyName, UnicodeString ScriptName)
{
	__int32 i;
	bool find;
	find = false;
	for (i = 0; i < scriptList->Count; i++)
		if (scriptList->Names[i].Compare(KeyName) == 0)
		{
			scriptList->Strings[i] = KeyName + _T("=") + ScriptName;
			find = true;
			break;
		}
	if (!find)
		scriptList->Add(KeyName + _T("=") + ScriptName);

	if (prpWnd != NULL)
		prpWnd->SetScript(KeyName, ScriptName);
}
//---------------------------------------------------------------------------

const TRect &__fastcall TMapGPerson::GetCollideRect() const
{
	return mCollideRect;
}
//---------------------------------------------------------------------------

const TRect &__fastcall TMapGPerson::GetViewRect() const
{
	return mViewRect;
}
//---------------------------------------------------------------------------

void __fastcall TMapGPerson::AddChild(__int32 joinnode, UnicodeString obj)
{
	while(mpChilds->Count <= joinnode)
	{
		mpChilds->Add(UnicodeString());
	}

	if(!obj.IsEmpty())
	{
		__int32 idx;
		while((idx = mpChilds->IndexOf(obj)) >= 0)
		{
			mpChilds->Strings[idx] = UnicodeString();
		}
	}

	mpChilds->Strings[joinnode] = obj;
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TMapGPerson::GetChild(__int32 joinnode) const
{
	if(mpChilds->Count <= joinnode)
	{
		return _T("");
	}
	return mpChilds->Strings[joinnode];
}
//---------------------------------------------------------------------------

bool __fastcall TMapGPerson::IsChildExist() const
{
	if(mpChilds->Count > 0)
	{
		return true;
	}
	return false;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//Класс текстового объекта на карте

__fastcall TMapTextBox::TMapTextBox(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString GParentName, TObjManager* Owner)
: TMapGPerson(x, y, pos_x, pos_y, GParentName, Owner)
{
	mObjId = objMAPTEXTBOX;
	mType = MTBT_Static;
	mTextId = _T("");
	mTextWidth = 0;
	mTextHeight = 0;
	mFixedSizes = true;
	mFontName = _T("");
	memset(&mAttributes, 0, sizeof(::TextAttributes));
	mAttributes.mCanvasColor = clTRANSPARENT;
	mAttributes.mAlignment = TEXT_ALIGNMENT_LEFT | TEXT_ALIGNMENT_TOP;
	mAttributes.mMargin = DEFAULT_TEXT_MARGIN;
	mMaxStringsCount = 0;
	mMaxCharsPerStringCount = 0;
}
//---------------------------------------------------------------------------

__fastcall TMapTextBox::TMapTextBox(TObjManager* Owner)
: TMapGPerson(Owner)
{
	mObjId = objMAPTEXTBOX;
	mType = MTBT_Static;
	mTextId = _T("");
	mTextWidth = 0;
	mTextHeight = 0;
	mFixedSizes = true;
	mFontName = _T("");
	memset(&mAttributes, 0, sizeof(::TextAttributes));
	mAttributes.mCanvasColor = clTRANSPARENT;
	mAttributes.mAlignment = TEXT_ALIGNMENT_LEFT | TEXT_ALIGNMENT_TOP;
	mAttributes.mMargin = DEFAULT_TEXT_MARGIN;
	mMaxStringsCount = 0;
	mMaxCharsPerStringCount = 0;
}
//---------------------------------------------------------------------------

__fastcall TMapTextBox::~TMapTextBox()
{
}
//---------------------------------------------------------------------------

void __fastcall TMapTextBox::CopyTo(TMapTextBox *mgp) const
{
	mgp->mType = mType;
	mgp->mTextId = mTextId;
	mgp->mTextWidth = mTextWidth;
	mgp->mTextHeight = mTextHeight;
	mgp->mAttributes = mAttributes;
	mgp->mFontName = mFontName;
	mgp->mMaxStringsCount = mMaxStringsCount;
	mgp->mMaxCharsPerStringCount = mMaxCharsPerStringCount;
	TMapGPerson::CopyTo((TMapGPerson*)mgp);
}
//---------------------------------------------------------------------------

void __fastcall TMapTextBox::inner_realign()
{
	if (GetAnimation() != nullptr)
	{
		mFixedSizes = mTextWidth <= 0 || mTextHeight <= 0;
		const TAnimationFrame *f = GetAnimation()->GetFrame(mCurrentFrame);
		if(mFixedSizes)
		{
			mWidth = f->GetWidth();
			mHeight = f->GetHeight();
		}
		else
		{
			mWidth = static_cast<int>(static_cast<double>(f->GetWidth()) * gsGlobalScale);
			mHeight = static_cast<int>(static_cast<double>(f->GetHeight()) * gsGlobalScale);
		}
		mOffPosX = f->GetLeftOffset();
		mOffPosY = f->GetTopOffset();
	}
	else
	{
		ClearSizes();
		mWidth = ERROR_IMAGE_WIDTH;
		mHeight = ERROR_IMAGE_HEIGHT;
		mOffPosX = -ERROR_IMAGE_WIDTH / 2;
		mOffPosY = -ERROR_IMAGE_HEIGHT / 2;
		mFixedSizes = true;
	}
	if (off_x == nullptr || off_y == nullptr)
	{
		assert(0);
		return;
	}

	double ox, oy;
	mTransform.GetPosition(ox, oy);

	if(mFixedSizes)
	{
		mScreenLeft = ox * gsGlobalScale + static_cast<double>(mOffPosX) - static_cast<double>(*off_x);
		mScreenTop = oy * gsGlobalScale + static_cast<double>(mOffPosY) - static_cast<double>(*off_y);
	}
	else
	{
		mScreenLeft = (ox + static_cast<double>(mOffPosX)) * gsGlobalScale - static_cast<double>(*off_x);
		mScreenTop = (oy + static_cast<double>(mOffPosY)) * gsGlobalScale - static_cast<double>(*off_y);
	}
}
//---------------------------------------------------------------------------

const __int32 __fastcall TMapTextBox::GetTextWidth() const
{
	return mTextWidth;
}
//---------------------------------------------------------------------------

const __int32 __fastcall TMapTextBox::GetTextHeight() const
{
	return mTextHeight;
}
//---------------------------------------------------------------------------

void __fastcall TMapTextBox::SetTextWidth(__int32 val)
{
	if(val < 0)
	{
		val = 0;
	}
	mTextWidth = val;
}
//---------------------------------------------------------------------------

void __fastcall TMapTextBox::SetTextHeight(__int32 val)
{
	if(val < 0)
	{
		val = 0;
	}
	mTextHeight = val;
}
//---------------------------------------------------------------------------

void __fastcall TMapTextBox::SetFillColor(TColor color)
{
	mAttributes.mCanvasColor = color;
}
//---------------------------------------------------------------------------

const TColor& __fastcall TMapTextBox::GetFillColor() const
{
	return mAttributes.mCanvasColor;
}
//---------------------------------------------------------------------------

void __fastcall TMapTextBox::SetAlignment(TextAlignment align)
{
	mAttributes.mAlignment = align;
}
//---------------------------------------------------------------------------

TextAlignment __fastcall TMapTextBox::GetAlignment() const
{
	return static_cast<TextAlignment>(mAttributes.mAlignment);
}
//---------------------------------------------------------------------------

void __fastcall TMapTextBox::SetTextId(UnicodeString id)
{
	mTextId = id;
}
//---------------------------------------------------------------------------

const UnicodeString __fastcall TMapTextBox::GetTextId() const
{
	return mTextId;
}
//---------------------------------------------------------------------------

void __fastcall TMapTextBox::SetFontName(UnicodeString name)
{
	mFontName = name;
}
//---------------------------------------------------------------------------

const UnicodeString __fastcall TMapTextBox::GetFontName() const
{
	return mFontName;
}
//---------------------------------------------------------------------------

void __fastcall TMapTextBox::SetTextType(const TMapTexBoxType type)
{
	mType = type;
}
//---------------------------------------------------------------------------

const TMapTextBox::TMapTexBoxType __fastcall TMapTextBox::GetTextType() const
{
	return mType;
}
//---------------------------------------------------------------------------

void __fastcall TMapTextBox::SetDynamicTextSizes(__int32 maxStringsCount, __int32 maxCharsPerStringCount)
{
	mMaxStringsCount = maxStringsCount;
	mMaxCharsPerStringCount = maxCharsPerStringCount;
}
//---------------------------------------------------------------------------

void __fastcall TMapTextBox::GetDynamicTextSizes(__int32 &maxStringsCount, __int32 &maxCharsPerStringCount) const
{
	maxStringsCount = mMaxStringsCount;
	maxCharsPerStringCount = mMaxCharsPerStringCount;
}
//---------------------------------------------------------------------------

const __int32 __fastcall TMapTextBox::GetMargin() const
{
	return mAttributes.mMargin;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


//все что касается GPersonProperties

__fastcall PropertyItem::PropertyItem()
:	name(_T(""))
,	def(0.0f)
,	value(0.0f)
,	checked(false)
{}
//---------------------------------------------------------------------------

void PropertyItem::SetValue(double val)
{
	value = val;
}
//---------------------------------------------------------------------------

double PropertyItem::GetValue() const
{
	return value;
}
//---------------------------------------------------------------------------

void PropertyItem::SetDefaultValue(double val)
{
	def = val;
}
//---------------------------------------------------------------------------

double PropertyItem::GetDefaultValue() const
{
	return def;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

GPersonProperties::GPersonProperties()
{
}
//---------------------------------------------------------------------------

GPersonProperties::~GPersonProperties()
{
	del_var_lst();
}
//---------------------------------------------------------------------------

void __fastcall GPersonProperties::del_var_lst()
{
	var_lst.clear();
}
//---------------------------------------------------------------------------

void __fastcall GPersonProperties::assignPattern(TObjPattern *newPt)
{
	__int32 i;
	if (newPt == NULL)
	{
		del_var_lst();
		return;
	}
	std::list<PropertyItem> temp_var_lst;
	std::list<PropertyItem>::iterator iv;
	for (iv = var_lst.begin(); iv != var_lst.end(); ++iv)
	{
		temp_var_lst.push_back(*iv);
	}
	var_lst.clear();
	newPt->GetVars(&var_lst);
	for (iv = var_lst.begin(); iv != var_lst.end(); ++iv)
	{
		setVarValue(iv->GetDefaultValue(), iv->name);
		setChecked(false, iv->name);
	}
	for (iv = temp_var_lst.begin(); iv != temp_var_lst.end(); ++iv)
	{
		setVarValue(iv->GetValue(), iv->name);
		setChecked(iv->checked, iv->name);
	}
}
//---------------------------------------------------------------------------

//установить значения переменных по умолчанию
void __fastcall GPersonProperties::setVarValuesToDef()
{
	std::list<PropertyItem>::iterator iv;
	for (iv = var_lst.begin(); iv != var_lst.end(); ++iv)
	{
		iv->SetValue(iv->GetDefaultValue());
		iv->checked = false;
	}
}
//---------------------------------------------------------------------------

//назначить новые свойства объекту
void __fastcall GPersonProperties::assignProperties(const GPersonProperties &newPrp)
{
	del_var_lst();
	std::list<PropertyItem>::const_iterator iv;
	for (iv = newPrp.var_lst.begin(); iv != newPrp.var_lst.end(); ++iv)
	{
		var_lst.push_back(*iv);
	}
}
//---------------------------------------------------------------------------

  //val (out) значение переменной var_name. Если такой переменной не найдено FALSE
bool __fastcall GPersonProperties::getVarValue(double *val, UnicodeString var_name) const
{
	std::list<PropertyItem>::const_iterator iv;
	for (iv = var_lst.begin(); iv != var_lst.end(); ++iv)
	{
		if (iv->name == var_name)
		{
			*val = iv->GetValue();
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------

  //val (out) значение переменной checked. Если такой переменной не найдено FALSE
bool __fastcall GPersonProperties::getChecked(bool *var, UnicodeString var_name) const
{
	std::list<PropertyItem>::const_iterator iv;
	for (iv = var_lst.begin(); iv != var_lst.end(); ++iv)
	{
		if (iv->name == var_name)
		{
			*var = iv->checked;
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------

  //val (out) значение переменной def. Если такой переменной не найдено FALSE
bool __fastcall GPersonProperties::getDefValue(double *val, UnicodeString var_name) const
{
	std::list<PropertyItem>::const_iterator iv;
	for (iv = var_lst.begin(); iv != var_lst.end(); ++iv)
	{
		if (iv->name == var_name)
		{
			*val = iv->GetDefaultValue();
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------

  //val (in) значение переменной var_name. Если такой переменной не найдено FALSE
bool __fastcall GPersonProperties::setVarValue(double val, UnicodeString var_name)
{
	std::list<PropertyItem>::iterator iv;
	for (iv = var_lst.begin(); iv != var_lst.end(); ++iv)
	{
		if (iv->name == var_name)
		{
			iv->SetValue(val);
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------

//val (in) значение переменной checked. Если такой переменной не найдено FALSE
bool __fastcall GPersonProperties::setChecked(bool var, UnicodeString var_name)
{
	std::list<PropertyItem>::iterator iv;
	for (iv = var_lst.begin(); iv != var_lst.end(); ++iv)
	{
		if (iv->name == var_name)
		{
			iv->checked = var;
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------

  //val (in) значение переменной def. Если такой переменной не найдено FALSE
bool __fastcall GPersonProperties::setDefValue(double val, UnicodeString var_name)
{
	std::list<PropertyItem>::iterator iv;
	for (iv = var_lst.begin(); iv != var_lst.end(); ++iv)
	{
		if (iv->name == var_name)
		{
			iv->SetDefaultValue(val);
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//TMapSpGPerson
//TMapSpGPerson
//TMapSpGPerson

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
__fastcall TMapSpGPerson::TMapSpGPerson(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString iGPParent, TObjManager* Owner)
: TMapGPerson(x, y, pos_x, pos_y, iGPParent, Owner)
{
	stateList = new TStringList();
	Clear();
	mFixedSizes = true;
}
//---------------------------------------------------------------------------

void __fastcall TMapSpGPerson::Clear()
{
	clearDirStates();
	clearScripts();
	stateList->Clear();
	SetVarValuesToDef();
	objName = _T("");
	objTeleport = _T("");
	tileTeleport = -1;
	mapDirection = trDOWN;
	mapIndex = -1;
	mapCount = 0;
	Type = trPROPERTIES;
	nearNodes[0] = _T("");
	nearNodes[1] = _T("");
	nearNodes[2] = _T("");
	nearNodes[3] = _T("");
	nodeTrigger = _T("");
}
//---------------------------------------------------------------------------

__fastcall TMapSpGPerson::TMapSpGPerson(TObjManager* Owner) : TMapGPerson(Owner)
{
	stateList = new TStringList();
	Clear();
	mFixedSizes = true;
}
//---------------------------------------------------------------------------

__fastcall TMapSpGPerson::~TMapSpGPerson()
{
	if (stateList != NULL)
	{
		delete stateList;
	}
	stateList = NULL;
}
//---------------------------------------------------------------------------

void __fastcall TMapSpGPerson::CopyTo(TMapSpGPerson *mgp) const
{
	mgp->objName = objName;
	mgp->objTeleport = objTeleport;
	mgp->mapDirection = mapDirection;
	mgp->mapIndex = mapIndex;
	mgp->mapCount = mapCount;
	mgp->Type = Type;
	mgp->stateList->AddStrings(stateList);
	mgp->tileTeleport = tileTeleport;
	mgp->nearNodes[0] = nearNodes[0];
	mgp->nearNodes[1] = nearNodes[1];
	mgp->nearNodes[2] = nearNodes[2];
	mgp->nearNodes[3] = nearNodes[3];
	mgp->nodeTrigger = nodeTrigger;
	TMapGPerson::CopyTo((TMapGPerson*)mgp);
}
//---------------------------------------------------------------------------

void __fastcall TMapSpGPerson::inner_realign()
{
	if (GetAnimation() != nullptr)
	{
		const TAnimationFrame *f = GetAnimation()->GetFrame(mCurrentFrame);
		mWidth = f->GetWidth();
		mHeight = f->GetHeight();
		mOffPosX = f->GetLeftOffset();
		mOffPosY = f->GetTopOffset();
	}
	else
	{
		ClearSizes();
	}
	if (off_x == nullptr || off_y == nullptr)
	{
		assert(0);
		return;
	}
	double ox, oy;
	mTransform.GetPosition(ox, oy);
	mScreenLeft = ox * gsGlobalScale + static_cast<double>(mOffPosX) - static_cast<double>(*off_x);
	mScreenTop = oy * gsGlobalScale + static_cast<double>(mOffPosY) - static_cast<double>(*off_y);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

__fastcall TMapTrigger::TMapTrigger(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString iGPParent, TObjManager* Owner)
: TMapSpGPerson(x, y, pos_x, pos_y, iGPParent, Owner)
{
	ShowRect(false);
	mObjId = objMAPTRIGGER;
	mZoneIndex = ZONES_INDEPENDENT_IDX;
}
//---------------------------------------------------------------------------

__fastcall TMapTrigger::TMapTrigger(TObjManager* Owner)
: TMapSpGPerson(Owner)
{
	mObjId = objMAPTRIGGER;
}
//---------------------------------------------------------------------------

__fastcall TMapDirector::TMapDirector(double x, double y, __int32 *pos_x, __int32 *pos_y, UnicodeString iGPParent, TObjManager* Owner)
: TMapSpGPerson(x, y, pos_x, pos_y, iGPParent, Owner)
{
	mObjId = objMAPDIRECTOR;
	ShowRect(false);
	mZoneIndex = ZONES_INDEPENDENT_IDX;
}
//---------------------------------------------------------------------------

__fastcall TMapDirector::TMapDirector(TObjManager* Owner)
: TMapSpGPerson(Owner)
{
	mObjId = objMAPDIRECTOR;
}
//---------------------------------------------------------------------------

void __fastcall TMapDirector::SetEventName(UnicodeString eventName)
{
	objTeleport = eventName;
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TMapDirector::GetEventName() const
{
	return objTeleport;
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

//COMMON

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

UnicodeString forceCorrectResFileExtention(UnicodeString file)
{
	switch(GlobalGraphicsType)
	{
		case GRAPHICS_TYPE_PNG:
			return ChangeFileExt(file, _T(".png"));
		case GRAPHICS_TYPE_BMP:
			return ChangeFileExt(file, _T(".bmp"));
		default:
			assert(0);
	}
	return file;
}
//---------------------------------------------------------------------------

UnicodeString forceCorrectSoundFileExtention(UnicodeString file)
{
	switch(GlobalSoundType)
	{
		case SOUND_TYPE_WAV:
			return ChangeFileExt(file, _T(".wav"));
		default:
			assert(0);
	}
	return file;
}
//---------------------------------------------------------------------------

UnicodeString forceCorrectFontFileExtention(UnicodeString file)
{
	switch(GlobalFontType)
	{
		case FONT_TYPE_FNT:
			return ChangeFileExt(file, _T(".fnt"));
		default:
			assert(0);
	}
	return file;
}
//---------------------------------------------------------------------------

bool read_png(const _TCHAR *file_name, Graphics::TBitmap *bmp_out)
{
	unsigned const char trr = (unsigned char)(((__int32)clTRANSPARENT) >> 16);
	unsigned const char trg = (unsigned char)(((__int32)clTRANSPARENT) >> 8);
	unsigned const char trb = (unsigned char)clTRANSPARENT;

	switch(GlobalGraphicsType)
	{
	case GRAPHICS_TYPE_PNG:
		{
            FILE *fp;
			const UnicodeString fname = ChangeFileExt(UnicodeString(file_name), _T(".png"));
			if ((fp = _wfopen(fname.c_str(), _T("rb"))) == NULL)
			{
				return false;
			}

			__int32 original_color_channels_count;
			bool onebit_tranparency = false;
			{
				const int PNG_HEADER_ZISE = 8; // 8 is the maximum size that can be checked
				char header[PNG_HEADER_ZISE];
				fread(header, 1, PNG_HEADER_ZISE, fp);
				if(png_sig_cmp(reinterpret_cast<png_bytep>(header), 0, PNG_HEADER_ZISE))
				{
					fclose(fp);
					return false;
				}
				png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
				if(png_ptr == NULL)
				{
					fclose(fp);
					return false;
				}
				png_infop info_ptr = png_create_info_struct(png_ptr);
				if(info_ptr == NULL)
				{
					fclose(fp);
					png_destroy_read_struct(&png_ptr, NULL, NULL);
					return false;
				}
				if(setjmp(png_jmpbuf(png_ptr)))
				{
					fclose(fp);
					png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
					return false;
				}
				png_set_option(png_ptr, PNG_SKIP_sRGB_CHECK_PROFILE, PNG_OPTION_ON);
				png_init_io(png_ptr, fp);
				png_set_sig_bytes(png_ptr, PNG_HEADER_ZISE);
				png_read_info(png_ptr, info_ptr);

				original_color_channels_count = png_get_channels(png_ptr, info_ptr);
				png_byte colortype = png_get_color_type(png_ptr, info_ptr);
				if(colortype == PNG_COLOR_TYPE_PALETTE)
				{
					if(png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
					{
						onebit_tranparency = true;
					}
				}

				png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

				fseek(fp, 0, SEEK_SET);
			}

			png_image image;
			memset(&image, 0, (sizeof image));
			image.version = PNG_IMAGE_VERSION;
			png_bytep buffer = NULL;

			if(png_image_begin_read_from_stdio(&image, fp) != 0)
			{
				png_color_struct bgcolor;
				bgcolor.red = trr;
				bgcolor.green = trg;
				bgcolor.blue = trb;

				image.format = PNG_FORMAT_BGRA;
				buffer = (png_bytep)malloc(PNG_IMAGE_SIZE(image));
				if (buffer != NULL &&
					png_image_finish_read(&image, &bgcolor, buffer,
										0/*row_stride*/, NULL/*colormap*/) != 0)
				{
					fclose(fp);
					fp = NULL;
				}
				else
				{
					fclose(fp);
					if (buffer != NULL)
					{
						free(buffer);
					}
					png_image_free(&image);
					return false;
				}
			}
			else
			{
				fclose(fp);
				return false;
			}

			assert(fp == NULL);
			assert(buffer != NULL);

			if(original_color_channels_count != 4 && onebit_tranparency == false)
			{
				__int32 x, y, w = image.width * 4;
				for (y = 0; y < (__int32)image.height; y++)
				{
					unsigned char *p = buffer + y * w;
					for (x = 0; x < w; x += 4)
					{
						if (p[x] == trr && p[x + 1] == trg && p[x + 2] == trb)
						{
							p[x] = 0;
							p[x + 1] = 0;
							p[x + 2] = 0;
							p[x + 3] = 0;
						}
						else
						{
							p[x + 3] = 255;
						}
					}
				}
			}

			const unsigned __int32 dest_bpp = 32;
			const __int32 plte_colors = 0;
			unsigned long i = sizeof(BITMAPINFO) + plte_colors * sizeof(RGBQUAD);
			BITMAPINFO *BmpHd = new BITMAPINFO[i];
			memset(BmpHd, 0, i);
			BmpHd->bmiHeader.biSize = sizeof(BITMAPINFO);
			BmpHd->bmiHeader.biWidth = image.width;
			BmpHd->bmiHeader.biHeight = (LONG)(image.height * -1.0L);
			BmpHd->bmiHeader.biPlanes = 1;
			BmpHd->bmiHeader.biBitCount = (WORD)dest_bpp;
			BmpHd->bmiHeader.biCompression = BI_RGB;
			BmpHd->bmiHeader.biClrUsed = plte_colors;

			HDC dc = GetDC(0);
			HBITMAP BMHandle = CreateDIBitmap(dc, //handle to DC
				&BmpHd->bmiHeader, //bitmap data
				CBM_INIT, //initialization option
				buffer, //initialization data
				BmpHd, //color-format data
				DIB_RGB_COLORS); //color-data usage
			ReleaseDC(0, dc);
			bmp_out->Handle = BMHandle;

			delete[] BmpHd;
			free(buffer);
			png_image_free(&image);

			if (bmp_out->Handle == NULL)
			{
				return false;
			}
		}
		break; //end PNG

	case GRAPHICS_TYPE_BMP:
		{
			UnicodeString fname = ChangeFileExt(UnicodeString(file_name), _T(".bmp"));
			try
			{
				__int32 x, y, w;
				unsigned char *p;
				bmp_out->LoadFromFile(fname);
				bool onebitalpha = bmp_out->PixelFormat != pf32bit;
				bmp_out->PixelFormat = pf32bit;
				w = bmp_out->Width * 4;
				for (y = 0; y < bmp_out->Height; y++)
				{
					p = (unsigned char*)bmp_out->ScanLine[y];
					for (x = 0; x < w; x += 4)
					{
						if(onebitalpha)
						{
							if (p[x] == trr && p[x + 1] == trg && p[x + 2] == trb)
							{
								p[x] = 0;
								p[x + 1] = 0;
								p[x + 2] = 0;
								p[x + 3] = 0;
							}
							else
							{
								p[x + 3] = 255;
							}
						}
						else
						{
							if(p[x + 3] == 0)
							{
								p[x] = 0;
								p[x + 1] = 0;
								p[x + 2] = 0;
							}
						}
					}
				}
			}
			catch(...)
			{
				bmp_out->Assign(NULL);
				return false;
			}
		}
	}
	return true;
}
//---------------------------------------------------------------------------

void __fastcall BmpVFlip(Graphics::TBitmap *bmp)
{
	__int32 w, h, y, w4, h2;
	void *pd, *ps;
	unsigned char *buf;

	if (bmp->PixelFormat != pf32bit)
		return;

	w = bmp->Width;
	h = bmp->Height;
	w4 = w + w + w + w;
	h2 = h >> 1;
	buf = new unsigned char[w4];
	for (y = 0; y < h2; y++)
	{
		pd = bmp->ScanLine[y];
		ps = bmp->ScanLine[h - y - 1];
		memcpy(buf, ps, w4);
		memcpy(ps, pd, w4);
		memcpy(pd, buf, w4);
	}
	delete[]buf;
}
//---------------------------------------------------------------------------

void __fastcall BmpHFlip(Graphics::TBitmap *bmp)
{
	__int32 x, w, h, y, w4, w2;
	unsigned char *pd, *ps, *pc;
	unsigned char buf[4];

	if (bmp->PixelFormat != pf32bit)
		return;

	w = bmp->Width;
	h = bmp->Height;
	w4 = w + w + w + w;
	w2 = w4 >> 1;
	for (y = 0; y < h; y++)
	{
		pd = (unsigned char*)bmp->ScanLine[y];
		for (x = 0; x < w2; x += 4)
		{
			ps = pd + w4 - x - 4;
			pc = pd + x;
			memcpy(buf, ps, 4);
			memcpy(ps, pc, 4);
			memcpy(pc, buf, 4);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall onChangeSpinEdit(TEdit *se, bool minusApply)
{
	__int32 i, len, selStart;
	UnicodeString str, etalon, comma;
	etalon = _T("1234567890");
	//char pch[10];
	//pch[0] = 0;
	//LCID lc = GetSystemDefaultLCID();
	//GetLocaleInfo(lc, LOCALE_SDECIMAL, pch, 10);
	comma = FormatSettings.DecimalSeparator; //UnicodeString(pch);
	etalon = etalon + comma;
	if(minusApply)
	{
		etalon = etalon + _T("-");
    }
	selStart = se->SelStart;
	str = se->Text;
	len = str.Length();
	for (i = 1; i <= len; i++)
	{
		if(etalon.Pos(str[i]) == 0)
		{
			len--;
			str.Delete(i, 1);
			if (selStart >= i && selStart - 1 >= 0)
			{
				selStart--;
			}
			i--;
		}
	}
	while(true)
	{
		i = str.LastDelimiter(comma);
		if(i > 0)
		{
			if(i == str.Pos(comma))
			{
				break;
			}
			else
			{
				str.Delete(i, 1);
				if (selStart >= i && selStart - 1 >= 0)
				{
					selStart--;
				}
			}
		}
		else
		{
        	break;
		}
	}
	while(true)
	{
		i = str.LastDelimiter(_T("-"));
		if(i > 0)
		{
			if(i == str.Pos(_T("-")) && i == 1)
			{
				break;
			}
			else
			{
				str.Delete(i, 1);
				if (selStart >= i && selStart - 1 >= 0)
				{
					selStart--;
				}
			}
		}
		else
		{
        	break;
		}
	}
	se->Text = str;
	se->SelStart = selStart;
}

//---------------------------------------------------------------------------

void __fastcall Edit_nameChangeMain(TEdit *Edit_name)
{
	__int32 i, sel, len;
	sel = Edit_name->SelStart;
	UnicodeString str = Edit_name->Text.Trim().UpperCase();
	if (str.IsEmpty())
	{
		Edit_name->Text = str;
		return;
	}
	while (str[1] >= L'0' && str[1] <= L'9')
	{
		str.Delete(1, 1);
		if (str.IsEmpty())
		{
			Edit_name->Text = str;
			return;
		}
		sel = 0;
	}
	len = str.Length();
	for (i = 1; i <= len; i++)
	{
		if ((str[i] >= L'A' && str[i] <= L'Z') || (str[i] >= L'0' && str[i] <= L'9') || str[i] == L'_')
		{
			continue;
		}
		else
		{
			str.Delete(i, 1);
			if (i == sel)
			{
				sel--;
			}
			i--;
			len--;
		}
	}
	Edit_name->Text = str;
	if (!str.IsEmpty())
	{
		if (sel < 0)
		{
			sel = 0;
		}
		Edit_name->SelStart = sel;
		Edit_name->SelLength = 0;
	}
}
//---------------------------------------------------------------------------

static inline double _calcLenght(double x0, double y0, double tx, double ty);

bool nextPointAtLine(double x0, double y0, double tx, double ty, double speed, double *pos)
{
	double dx, dy, l;
	__int32 ll;
	l = _calcLenght(x0, y0, tx, ty);
	if(l > 0)
	{
		dx = (speed * (tx - x0)) / l;
		dy = (speed * (ty - y0)) / l;
		if(((tx - pos[IX]) > 0) == (dx > 0) && ((ty - pos[IY]) > 0) == (dy > 0))
		{
			pos[IX] += dx;
			pos[IY] += dy;
			return true;
		}
	}
	return false;
}
//----------------------------------------------------------------------------------

double _calcLenght(double x0, double y0, double tx, double ty)
{
	double dx, dy;
	dx = tx - x0;
	dy = ty - y0;
	if(dx < 0)
	{
		dx = -dx;
	}
	if(dy < 0)
	{
		dy = -dy;
	}
	return sqrt(dx * dx + dy * dy);
}
// ----------------------------------------------------------------------------------

//CategoryButtons Общий код

void __fastcall CBCategoryCollapase(TObject * /* Sender */ , const TButtonCategory *Category)
{
	TButtonCategory *Category1 = (TButtonCategory*)Category;
	Category1->Collapsed = false;
}

//---------------------------------------------------------------------------
void __fastcall CBMouseDown(TObject *Sender, TMouseButton /* Button */ , TShiftState /* Shift */ , __int32 X, __int32 Y)
{
	TCategoryButtons *cb = (TCategoryButtons*)Sender;
	TButtonItem *bt = cb->GetButtonAt(X, Y, cb->Categories->Items[0]);
	cb->SelectedItem = bt;
	cb->FocusedItem = bt;
}

//---------------------------------------------------------------------------
void __fastcall CBDrawIcon(TObject *Sender, const TButtonItem *Button, TCanvas *Canvas, TRect &Rect, TButtonDrawState /* State */ , __int32 & /* TextOffset */ )
{
	TCategoryButtons *cb = (TCategoryButtons*)Sender;
	cb->Images->Draw(Canvas, Rect.Left + ((Rect.Width() - cb->Images->Width) >> 1), Rect.Top + ((Rect.Height() - cb->Images->Height) >> 1), Button->ImageIndex, dsTransparent, itImage, true);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


__fastcall TPropertiesControl::TPropertiesControl(TScrollBox *parent)
{
	mpScrollBox = parent;
	mMode = PCM_NORMAL;
	mOnValidate = NULL;
	mOnCheck = NULL;
	Clear();
}
//---------------------------------------------------------------------------

__fastcall TPropertiesControl::~TPropertiesControl()
{
	Clear();
}
//---------------------------------------------------------------------------

void __fastcall TPropertiesControl::Init(const std::list<PropertyItem>* varlist, TPropertiesControlMode mode)
{
	TLabel *lb;
	TCheckBox *cb;
	UnicodeString str;
	__int32 h, val;
	mMode = mode;
	h = 2;
	Clear();
	std::list<PropertyItem>::const_iterator iv;
	for (iv = varlist->begin(); iv != varlist->end(); ++iv)
	{
		TEdit *ed = new TEdit(mpScrollBox);
		ed->Hint = iv->name;
		ed->ShowHint = false;
		ed->Width = 90;
		ed->Text = _T("0");
		ed->Top = h;
		ed->Left = 220;
		ed->OnChange = OnSEChange;
		ed->OnEnter = OnSEEnter;
		ed->OnExit = OnSEExit;
		ed->OnKeyPress = OnSEKeyPress;
		ed->Parent = mpScrollBox;
		str = iv->name + _T(" (4 ") + Localization::Text(_T("mBytes")) + _T(")");
		if(mode == PCM_NORMAL)
		{
			lb = new TLabel(mpScrollBox);
			lb->WordWrap = false;
			lb->AutoSize = false;
			lb->Width = 200;
			lb->Caption = str;
			lb->ShowHint = false;
			lb->Tag = NativeInt(ed);
			ed->Tag = NativeInt(lb);
			lb->Top = h + (ed->Height - lb->Height) / 2;
			lb->Left = 8;
			lb->Parent = mpScrollBox;
		}
		else
		{
			cb = new TCheckBox(mpScrollBox);
			cb->WordWrap = false;
			cb->Width = 200;
			cb->Caption = str;
			cb->ShowHint = false;
			cb->Tag = NativeInt(ed);
			ed->Tag = NativeInt(cb);
			cb->Top = h + (ed->Height - cb->Height) / 2;
			cb->Left = 8;
			cb->Checked = false;
			cb->OnClick = OnCheckBoxClick;
			cb->Parent = mpScrollBox;
        }
		h += ed->Height + 2;
	}
}
//---------------------------------------------------------------------------

void __fastcall TPropertiesControl::Clear()
{
	for (__int32 i = 0; i < mpScrollBox->ControlCount; i++)
	{
		delete(TControl*)mpScrollBox->Controls[i];
		i--;
	}
	mpScrollBox->VertScrollBar->Position = 0;
	mpScrollBox->HorzScrollBar->Position = 0;
	mOldVal = _T("");
}
//---------------------------------------------------------------------------

void __fastcall TPropertiesControl::SetOnValidate(TOnPropertiesControlValueValidate fn)
{
	mOnValidate = fn;
}
//---------------------------------------------------------------------------

void __fastcall TPropertiesControl::SetOnCheck(TOnPropertiesControlOnCheck fn)
{
	mOnCheck = fn;
}
//---------------------------------------------------------------------------


void __fastcall TPropertiesControl::OnSEKeyPress(TObject *Sender, WideChar &Key)
{
	if (Key == VK_RETURN)
	{
		Key = 0;
		OnSEExit(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TPropertiesControl::OnSEChange(TObject *Sender)
{
	onChangeSpinEdit((TEdit*)Sender, true);
}
//---------------------------------------------------------------------------

void __fastcall TPropertiesControl::OnSEEnter(TObject *Sender)
{
	mOldVal = ((TEdit*)Sender)->Text;
}
//---------------------------------------------------------------------------

void __fastcall TPropertiesControl::OnSEExit(TObject *Sender)
{
	TEdit *ed = (TEdit*)Sender;
	UnicodeString str = ed->Text.Trim();
	if (mOldVal.Compare(str) != 0)
	{
		double val;
		__int32 selStart = ed->SelStart;
		try
		{
			val = (double)StrToFloat(ed->Text);
		}
		catch(...)
		{
			val = 0.0f;
			ed->Text = _T("0");
		}
		if(mOnValidate)
		{
			mOnValidate(ed->Hint, val);
		}
		ed->SelStart = selStart;
		mOldVal = ed->Text;
	}
}
//---------------------------------------------------------------------------

void __fastcall TPropertiesControl::OnCheckBoxClick(TObject *Sender)
{
	if(mMode == PCM_CHECKERS)
	{
		TCheckBox *lb = (TCheckBox*)Sender;
		TEdit *ed = (TEdit*)lb->Tag;
		if(lb->Checked)
		{
			ed->Enabled = true;
			ed->Color = clWindow;
		}
		else
		{
			ed->Enabled = false;
			ed->Color = clBtnFace;
		}
		if(mOnCheck)
		{
			mOnCheck(ed->Hint, lb->Checked);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPropertiesControl::SetValue(double val, UnicodeString var_name)
{
	TControl *c;
	for (__int32 j = 0; j < mpScrollBox->ControlCount; j++)
	{
		c = (TControl*)mpScrollBox->Controls[j];
		if (c->ClassNameIs(_T("TEdit")))
			if (var_name.Compare(c->Hint) == 0)
			{
				((TEdit*)c)->Text = FormatFloat(_T("0.000"), val);
				break;
			}
	}
}
//---------------------------------------------------------------------------

void __fastcall TPropertiesControl::SetChecked(bool val, UnicodeString var_name)
{
	if(mMode == PCM_CHECKERS)
	{
		TControl *c;
		for (__int32 j = 0; j < mpScrollBox->ControlCount; j++)
		{
			c = (TControl*)mpScrollBox->Controls[j];
			if (c->ClassNameIs(_T("TEdit")))
				if (var_name.Compare(c->Hint) == 0)
				{
					TCheckBox *cb = (TCheckBox*)c->Tag;
					cb->Checked = val;
					OnCheckBoxClick(cb);
					break;
				}
		}
	}
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

__fastcall SoundSchemePropertyItem::SoundSchemePropertyItem()
: mName(_T(""))
, mFileName(_T(""))
, mType(SSRT_NONE)
, mFileSize(0)
{
}
//---------------------------------------------------------------------------

__fastcall SoundSchemePropertyItem::SoundSchemePropertyItem(const SoundSchemePropertyItem& sspi)
{
	*this = sspi;
}
//---------------------------------------------------------------------------

__fastcall SoundSchemePropertyItem::~SoundSchemePropertyItem()
{
}
//---------------------------------------------------------------------------

__fastcall TMapSoundScheme::TMapSoundScheme(double x, double y, __int32 *pos_x, __int32 *pos_y,
									UnicodeString GParentName, TObjManager* Owner)
									: TMapGPerson(x, y, pos_x, pos_y, GParentName, Owner)
{
	mObjId = objMAPSOUNDSCHEME;
	mFixedSizes = true;
}
//---------------------------------------------------------------------------

__fastcall TMapSoundScheme::TMapSoundScheme(TObjManager* Owner)
: TMapGPerson(Owner)
{
	mObjId = objMAPSOUNDSCHEME;
	mFixedSizes = true;
}
//---------------------------------------------------------------------------

__fastcall TMapSoundScheme::~TMapSoundScheme()
{
}
//---------------------------------------------------------------------------

void __fastcall TMapSoundScheme::inner_realign()
{
	if (GetAnimation() != nullptr)
	{
		const TAnimationFrame *f = GetAnimation()->GetFrame(mCurrentFrame);
		mWidth = f->GetWidth();
		mHeight = f->GetHeight();
		mOffPosX = f->GetLeftOffset();
		mOffPosY = f->GetTopOffset();
	}
	else
	{
		ClearSizes();
	}

	if (off_x == nullptr || off_y == nullptr)
	{
		assert(0);
		return;
	}

    double ox, oy;
	mTransform.GetPosition(ox, oy);

	mScreenLeft = ox * gsGlobalScale + static_cast<double>(mOffPosX) - static_cast<double>(*off_x);
	mScreenTop = oy * gsGlobalScale + static_cast<double>(mOffPosY) - static_cast<double>(*off_y);
}
//---------------------------------------------------------------------------

void __fastcall TMapSoundScheme::CopyTo(TMapSoundScheme *mgp) const
{
	mgp->mItems.clear();
    mgp->mItems = mItems;
	TMapGPerson::CopyTo((TMapGPerson*)mgp);
}
//---------------------------------------------------------------------------

__int32 __fastcall TMapSoundScheme::GetItemsCount() const
{
	return mItems.size();
}
//---------------------------------------------------------------------------

void __fastcall TMapSoundScheme::AddItem(const SoundSchemePropertyItem& item)
{
	mItems.push_back(item);
}
//---------------------------------------------------------------------------

SoundSchemePropertyItem* __fastcall TMapSoundScheme::FindItem(UnicodeString name)
{
	std::list<SoundSchemePropertyItem>::iterator i;
	for (i = mItems.begin(); i != mItems.end(); ++i)
	{
		if(i->mName.Compare(name) == 0)
		{
			return &(*i);
		}
	}
	return NULL;
}
//---------------------------------------------------------------------------

const SoundSchemePropertyItem* __fastcall TMapSoundScheme::FindItem(UnicodeString name) const
{
	std::list<SoundSchemePropertyItem>::const_iterator i;
	for (i = mItems.begin(); i != mItems.end(); ++i)
	{
		if(i->mName.Compare(name) == 0)
		{
			return &(*i);
		}
	}
	return NULL;
}
//---------------------------------------------------------------------------

bool __fastcall TMapSoundScheme::GetItemByIndex(__int32 idx, SoundSchemePropertyItem& item) const
{
	if (idx >= 0)
	{
		std::list<SoundSchemePropertyItem>::const_iterator i;
		i = mItems.begin();
		std::advance(i, idx);
		if(i != mItems.end())
		{
			item = *(&(*i));
			return true;
        }
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TMapSoundScheme::GetItemByName(UnicodeString name, SoundSchemePropertyItem& item) const
{
	const SoundSchemePropertyItem *it = FindItem(name);
	if(it)
	{
		item = *it;
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TMapSoundScheme::ChangeItem(UnicodeString name, SoundSchemePropertyItem& item)
{
	SoundSchemePropertyItem *it = FindItem(name);
	if(it)
	{
		*it = item;
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TMapSoundScheme::DeleteItem(UnicodeString name)
{
	std::list<SoundSchemePropertyItem>::iterator i;
	for (i = mItems.begin(); i != mItems.end(); ++i)
	{
		if(i->mName.Compare(name) == 0)
		{
			mItems.erase(i);
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------

void __fastcall TMapSoundScheme::Clear()
{
	mItems.clear();
}
//---------------------------------------------------------------------------

bool validateFontResource(UnicodeString path)
{
	FILE* f;
	f = _wfopen(path.c_str(), _T("rb"));
	if(f != NULL)
	{
		fclose(f);
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

bool validateSoundResource(UnicodeString path, unsigned __int32 *size)
{
	FILE* f;
	f = _wfopen(path.c_str(), _T("rb"));
	if(f != NULL)
	{
		if(size != NULL)
		{
			fseek(f, 0, SEEK_END);
			*size = ftell(f);
			fseek(f, 0, SEEK_SET);
		}
		fclose(f);
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

__fastcall ObjPoolPropertyItem::ObjPoolPropertyItem()
: mName(_T(""))
, mCount(0)
{
}
//---------------------------------------------------------------------------

__fastcall ObjPoolPropertyItem::~ObjPoolPropertyItem()
{
}
//---------------------------------------------------------------------------

__fastcall TMapObjPool::TMapObjPool(double x, double y, __int32 *pos_x, __int32 *pos_y,
									UnicodeString GParentName, TObjManager* Owner)
									: TMapGPerson(x, y, pos_x, pos_y, GParentName, Owner)
{
	mObjId = objMAPOBJPOOL;
	mFixedSizes = true;
}
//---------------------------------------------------------------------------

__fastcall TMapObjPool::TMapObjPool(TObjManager* Owner)
: TMapGPerson(Owner)
{
	mObjId = objMAPOBJPOOL;
	mFixedSizes = true;
}
//---------------------------------------------------------------------------

__fastcall TMapObjPool::~TMapObjPool()
{
}
//---------------------------------------------------------------------------

void __fastcall TMapObjPool::inner_realign()
{
	if (GetAnimation() != nullptr)
	{
		const TAnimationFrame *f = GetAnimation()->GetFrame(mCurrentFrame);
		mWidth = f->GetWidth();
		mHeight = f->GetHeight();
		mOffPosX = f->GetLeftOffset();
		mOffPosY = f->GetTopOffset();
	}
	else
	{
		ClearSizes();
	}

	if (off_x == nullptr || off_y == nullptr)
	{
		assert(0);
		return;
	}

	double ox, oy;
	mTransform.GetPosition(ox, oy);

	mScreenLeft = ox * gsGlobalScale + static_cast<double>(mOffPosX) - static_cast<double>(*off_x);
	mScreenTop = oy * gsGlobalScale + static_cast<double>(mOffPosY) - static_cast<double>(*off_y);
}
//---------------------------------------------------------------------------

void __fastcall TMapObjPool::CopyTo(TMapObjPool *mgp) const
{
	mgp->mItems.clear();
	mgp->mItems = mItems;
	TMapGPerson::CopyTo((TMapGPerson*)mgp);
}
//---------------------------------------------------------------------------

__int32 __fastcall TMapObjPool::GetItemsCount() const
{
	return mItems.size();
}
//---------------------------------------------------------------------------

void __fastcall TMapObjPool::AddItem(ObjPoolPropertyItem& item)
{
	mItems.push_back(item);
}
//---------------------------------------------------------------------------

ObjPoolPropertyItem* __fastcall TMapObjPool::FindItem(UnicodeString name)
{
	std::list<ObjPoolPropertyItem>::iterator i;
	for (i = mItems.begin(); i != mItems.end(); ++i)
	{
		if(i->mName.Compare(name) == 0)
		{
			return &(*i);
		}
	}
	return NULL;
}
//---------------------------------------------------------------------------

const ObjPoolPropertyItem* __fastcall TMapObjPool::FindItem(UnicodeString name) const
{
	std::list<ObjPoolPropertyItem>::const_iterator i;
	for (i = mItems.begin(); i != mItems.end(); ++i)
	{
		if(i->mName.Compare(name) == 0)
		{
			return &(*i);
		}
	}
	return NULL;
}
//---------------------------------------------------------------------------

bool __fastcall TMapObjPool::GetItemByIndex(__int32 idx, ObjPoolPropertyItem& item) const
{
	if (idx >= 0)
	{
		std::list<ObjPoolPropertyItem>::const_iterator i;
		i = mItems.begin();
		std::advance(i, idx);
		if(i != mItems.end())
		{
			item = *(&(*i));
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TMapObjPool::GetItemByName(UnicodeString name, ObjPoolPropertyItem& item) const
{
	const ObjPoolPropertyItem *it = FindItem(name);
	if(it)
	{
		item = *it;
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

bool __fastcall TMapObjPool::ChangeItem(UnicodeString name, ObjPoolPropertyItem& item)
{
	ObjPoolPropertyItem *it = FindItem(name);
	if(it)
	{
		*it = item;
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

__int32 __fastcall TMapObjPool::MoveItem(__int32 idx_from, __int32 idx_to)
{
	if((size_t)idx_from >= mItems.size() || idx_from < 0 || idx_from == idx_to || mItems.size() == 1)
	{
		return -1;
	}
	std::list<ObjPoolPropertyItem>::iterator i;
	i = mItems.begin();
	std::advance(i, idx_from);
	if(i != mItems.end())
	{
		ObjPoolPropertyItem item = *i;
		mItems.erase(i);
		if(idx_to >= 0)
		{
			if(mItems.size() > (size_t)idx_to)
			{
				i = mItems.begin();
				std::advance(i, idx_to);
				mItems.insert(i, item);
				return idx_to;
			}
			else
			{
				mItems.push_back(item);
				return (__int32)(mItems.size() - 1);
			}
		}
		else
		{
			mItems.push_front(item);
			return 0;
		}
	}
	return -1;
}
//---------------------------------------------------------------------------

bool __fastcall TMapObjPool::DeleteItem(UnicodeString name)
{
	std::list<ObjPoolPropertyItem>::iterator i;
	for (i = mItems.begin(); i != mItems.end(); ++i)
	{
		if(i->mName.Compare(name) == 0)
		{
			mItems.erase(i);
			return true;
		}
	}
	return false;
}
//---------------------------------------------------------------------------

void __fastcall TMapObjPool::Clear()
{
	mItems.clear();
}
//---------------------------------------------------------------------------

__int32 __fastcall TMapObjPool::GetTolalObjCount() const
{
	__int32 count = 0;
	__int32 i_count = GetItemsCount();
	for (__int32 i = 0; i < i_count; i++)
	{
		ObjPoolPropertyItem item;
		GetItemByIndex(i, item);
		count += item.mCount;
	}
	return count;
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

s32 _NBytesToInt(const u8* m, s32 N, s32 off)
{
	s32 in = 0, x = 0;
	while(in < 4 && N > 0)
	{
		x|=((m[off+in]&0xFF)<<(in<<3));
		in++;N--;
	}
	return x;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

