//---------------------------------------------------------------------------

#ifndef USaveLoadStreamDataH
#define USaveLoadStreamDataH

#include <Classes.hpp>

//---------------------------------------------------------------------------

class TSaveLoadStreamHandle;
class TAbstractXMLNode;
class TAbstractXMLDocument;

struct TSaveLoadStreamCharArray
{
	__fastcall TSaveLoadStreamCharArray() : size(0), ptr(nullptr) {}
	__fastcall TSaveLoadStreamCharArray(size_t s, void* p) : size(s), ptr(p) {}

	size_t size;
	void* ptr;
};

class TSaveLoadStreamData
{
public:

	enum TSLDataType
	{
		SaveProject,
		ExportToXML,
		LoadProject,
		LoadAsBinary,
		SaveAsBinary,
		PasteClipboard,
		CopyClipboard
	};
	enum TSLFieldType
	{
		ftChar,
		ftS16,
		ftU16,
		ftS32,
		ftDouble,
		ftAnsiStringArray,
		ftWideStringArray,
		ftDataArray,
		ftNewSection,
        ftDebugCheckSum
	};

	struct TSaveLoadStreamDataInit
	{
		TSaveLoadStreamData::TSLDataType type;
		UnicodeString fileName;
		TMemoryStream* memoryStream;
	};

private:
	typedef unsigned int __fastcall (__closure *TWriteFn)(void*, const TSLFieldType&, UnicodeString, TSaveLoadStreamHandle&);
	typedef unsigned int __fastcall (__closure *TReadFn)(void*, const TSLFieldType&, UnicodeString, TSaveLoadStreamHandle&);

public:
	explicit __fastcall TSaveLoadStreamData(const TSaveLoadStreamDataInit& initData, bool debugmode);
	virtual __fastcall ~TSaveLoadStreamData();

	bool __fastcall Init();
	bool __fastcall Save();

	__property TSLDataType type = {read = _d.type};
	__property TWriteFn writeFn = {read = _writeFn};
	__property TReadFn readFn = {read = _readFn};

	unsigned char ch_val;
	unsigned short u16_val;
	short s16_val;
	__int32 s32_val;
	double f_val;

private:

	unsigned int __fastcall SaveToFileFn(void *ptr, const TSLFieldType& ftype, UnicodeString fieldName, TSaveLoadStreamHandle& f);
	unsigned int __fastcall LoadFromFileFn(void *ptr, const TSLFieldType& ftype, UnicodeString fieldName, TSaveLoadStreamHandle& h);

	unsigned int __fastcall SaveToFileXMLFn(void *ptr, const TSLFieldType& ftype, UnicodeString fieldName, TSaveLoadStreamHandle& f);
	unsigned int __fastcall LoadFromFileXMLFn(void *ptr, const TSLFieldType& ftype, UnicodeString fieldName, TSaveLoadStreamHandle& h);

	unsigned int __fastcall SaveStreamFn(void *ptr, const TSLFieldType& ftype, UnicodeString fieldName, TSaveLoadStreamHandle& f);
	unsigned int __fastcall LoadStreamFn(void *ptr, const TSLFieldType& ftype, UnicodeString fieldName, TSaveLoadStreamHandle& f);

	void __fastcall Release();

	TSaveLoadStreamDataInit _d;
	TWriteFn _writeFn;
	TReadFn _readFn;
	FILE* _fileStream;
    TAbstractXMLDocument* _xmlDoc;
	bool _debugmode;
    bool _inited;
};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

class TSaveLoadStreamHandle
{
friend class  TSaveLoadStreamData;

public:

	__fastcall TSaveLoadStreamHandle();

	virtual __fastcall ~TSaveLoadStreamHandle();

	bool __fastcall IsValid() const { return stream.f != nullptr; }

private:

	union
	{
		FILE* f;
		TAbstractXMLNode* node;
		TMemoryStream* memoryStream;
	} stream;
    TAbstractXMLNode* nodeInstance;
	TSaveLoadStreamData::TSLDataType type;
};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


#endif
