//---------------------------------------------------------------------------

#ifndef UnitJNCountH
#define UnitJNCountH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Samples.Spin.hpp>
//---------------------------------------------------------------------------
class TFJNCount : public TForm
{
__published:
	TButton *ButtonOk;
	TButton *ButtonCancel;
	TGroupBox *GroupBox_f;
	TLabel *Label2;
	TGroupBox *GroupBoxLayers;
	TLabel *Label14;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
private:
	void __fastcall OnSpinChange(TObject *);
public:
	TSpinEdit *CSpinCount;
	__fastcall TFJNCount(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFJNCount *FJNCount;
//---------------------------------------------------------------------------
#endif
