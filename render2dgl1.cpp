/*
tengine - cross-platform game engine
------------------------------------

Copyright (c) 2013 Papa Pascualle
Contact Email: pascualle@gmail.com

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#ifndef NITRO_SDK

#include "windows.h"
#include "UObjCode.h"
#include "render2dgl1.h"
#ifdef _DEBUG
#define SDK_DEBUG
#endif
#include <vector>

#ifdef USE_OPENGL_1_RENDER

#if defined EMSCRIPTEN_APP
 ------
 error: please use USE_OPENGL_2_RENDER
 ------
#endif

#ifdef ANDROID_NDK
 #include <GLES/gl.h>
 #ifdef USE_GL_GLEXT
  #include <GLES/glext.h>
 #endif
 typedef GLfixed GLfx32;
 typedef GLclampx GLclampfx32;
 #define glOrtho_x glOrthox
 #define glClearColor_x glClearColorx
 #define glColor4_x glColor4x
 #define glPointSize_x glPointSizex
 #define glLineWidth_x glLineWidthx
#else
 #ifdef IOS_APP
  #include "OpenGLES/ES1/gl.h"
  #ifdef USE_GL_GLEXT
   #include "OpenGLES/ES1/glext.h"
  #endif
  #undef GL_FIXED
  #define GL_FIXED GL_FLOAT
  typedef float GLfx32;
  typedef float GLclampfx32;
  #define glOrtho_x glOrthof
  #define glClearColor_x glClearColor
  #define glColor4_x glColor4f
  #define glPointSize_x glPointSize
  #define glLineWidth_x glLineWidth
 #else
  #include "platform.h"
  #include <GL/gl.h>
  #ifdef USE_GL_GLEXT
   #include "gl/glext.h"
  #endif
  #if defined WINDOWS_APP || defined NIX_APP
   #undef GL_FIXED
   #define GL_FIXED GL_FLOAT
   typedef BOOL EGLboolean;
   typedef float GLfx32;
   typedef float GLclampfx32;
   #define glOrtho_x glOrtho
   #define glClearColor_x glClearColor
   #define glColor4_x glColor4f
   #define glPointSize_x glPointSize
   #define glLineWidth_x glLineWidth
  #endif
 #endif
#endif

#if defined ANDROID_NDK
 #define GX_COLOR_CHANNEL(c) ((u8)(((c) * 255) / 31))
 #define GX_FX32_TO_GLfx32(c) ((GLfx32)(c))
 #define GX_GLfx32_TO_FX32(c) ((fx32)(c))
#else
 #define GX_COLOR_CHANNEL(c) ((float)c)
 #define GX_FX32_TO_GLfx32(c) ((float)c)
 #define GX_GLfx32_TO_FX32(c) ((float)c)
 #define FX32(c) ((float)c)
 #define FX32_ONE (1.0f)
#endif

struct TRGLColor
{
	u8 r;
	u8 g;
	u8 b;
	u8 a;
};

struct TRGLRenderItem
{
	const struct BMPImage* mpImageHeader;
	const s32 *mpClipRect;
	s32 mNext;
	u32 mImgVertexArrayIdx;
	u32 mImgTexArrayIdx;
	u32 mImgColorArrayIdx;
	s32 mVertexCt;
	s32 mColorCt;
#ifdef USE_GL_GLEXT
	GLuint mVBOBufferIdx;
#endif
	u8 mType;
};

struct TRGLBGRenderItem
{
	const struct BMPImage* mpImageHeader;
	u32 mVertexArrayIdx;
	u32 mTexArrayIdx;
	s32 mVertexCt;
	s32 mNext;
#ifdef USE_GL_GLEXT
	GLuint mVBOBufferIdx;
#endif
};

struct TRGLBGLayer
{
	s32 mRenderListSize;
	s32 mRenderListHead;
	s32 mRenderListTail;
	s32 mVertexCt;
	s32 mTexCt;
	std::vector<GLfx32> mTexCoordPool;
	std::vector<GLfx32> mVertexPool;
	std::vector<TRGLBGRenderItem> mRenderListPool;
};

static struct RenderPlane
{
	s32 mOffX;
	s32	mOffY;
	s32	mViewWidth;
	s32	mViewHeight;
	s32 mRenderListSize;
	s32 mObjTextureListSize;
	s32 mBGMaxLayers;
	s32 mBGMaxTextures;
	s32 mBGMaxElements;
	s32 mImgVertexCt;
	s32 mImgTexCt;
	s32 mImgColorCt;
	struct TRGLBGLayer *mpBGLayersList;
	std::vector<TRGLRenderItem> mRenderListPool;
	s32 mRenderListHead;
	s32 mRenderListTail;
#ifdef USE_GL_GLEXT
	GLuint* mpBGVBOBuffer;
	GLuint* mpVBOBuffer;
	GLuint mObjVBOBufferLastIdx;
#endif
	struct TRGLColor mColor;
	std::vector<GLfx32> mImgVertexPool;
	std::vector<GLfx32> mImgTexCoordPool;
	std::vector<GLbyte> mImgColorPool;
	GLfx32 mScale;
	GLfx32 mLineWidth;
	GLushort mLineStyle;
	s32 mDraw;
	BOOL mInit;
}gsPlane[BGSELECT_NUM];

static enum BGSelect gsActivePlane = BGSELECT_NUM;
static BOOL gsGraphicsInit = FALSE;

static s32 mCurrentTextureID = -1;

static s32 sgScreenWidth = -1;
static s32 sgScreenHeight = -1;
static BOOL gsLostDevice = FALSE;
static BOOL gsEnableTexure = FALSE;
static BOOL gsEnableColor = FALSE;

static void glRender_CheckForCleanup(enum BGSelect iType);
static void glRender_AddToRenderList(struct TRGLRenderItem* item);
static void glRender_CheckError(const char* op);

#ifdef USE_GL_GLEXT
#ifdef WINDOWS_APP
static PFNGLGENBUFFERSPROC glGenBuffers = NULL;
static PFNGLBINDBUFFERPROC glBindBuffer = NULL;
static PFNGLBUFFERDATAPROC glBufferData = NULL;
static PFNGLBUFFERSUBDATAPROC glBufferSubData = NULL;
static PFNGLDELETEBUFFERSPROC glDeleteBuffers = NULL;
#endif
#endif

static GLuint s_disable_caps[] =
{
	GL_FOG,
	GL_LIGHTING,
	GL_CULL_FACE,
	GL_ALPHA_TEST,
	GL_BLEND,
	GL_COLOR_LOGIC_OP,
	GL_DITHER,
	GL_STENCIL_TEST,
	GL_DEPTH_TEST,
	GL_COLOR_MATERIAL,
	GL_LINE_SMOOTH,
	GL_POINT_SMOOTH,
	0
};

static BackdropFn _backdropFn = NULL;

//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------

static void glRender_CheckError(const char* op)
{
#ifdef SDK_DEBUG
	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		OS_Warning(L"after %s() glError (0x%x)\n", op, error);
		assert(0);
	}
#else
	(void)op;
#endif
}
//----------------------------------------------------------------------------------

void glRender_Init()
{
	s32 type;
	for(type = 0; type < BGSELECT_NUM; type++)
	{
		//MI_CpuFill8(&gsPlane[type], 0, sizeof(struct RenderPlane));
		gsPlane[type].mScale = GX_FX32_TO_GLfx32(FX32_ONE);
		gsPlane[type].mLineWidth = GX_FX32_TO_GLfx32(FX32_ONE);
	}
	gsGraphicsInit = TRUE;
	sgScreenWidth = 0;
	sgScreenHeight = 0;
	mCurrentTextureID = -1;
}
//----------------------------------------------------------------------------------

void glRender_Release()
{
	s32 type;
	for(type = 0; type < BGSELECT_NUM; type++)
	{
		gsPlane[type].mScale = GX_FX32_TO_GLfx32(FX32_ONE);
		gsPlane[type].mLineWidth = GX_FX32_TO_GLfx32(FX32_ONE);
	}
	gsGraphicsInit = FALSE;
}
//----------------------------------------------------------------------------------

void glRender_LostDevice(void)
{
	gsLostDevice = TRUE;
}
//----------------------------------------------------------------------------------

void glRender_RestoreDevice(void)
{
    GLuint *start = s_disable_caps;

	gsLostDevice = FALSE;

#if defined USE_GL_GLEXT && defined WINDOWS_APP
	glGenBuffers = (PFNGLGENBUFFERSPROC)wglGetProcAddress("glGenBuffers");
	SDK_NULL_ASSERT(glGenBuffers); // unsupportet extention, please switch off USE_GL_GLEXT in crossgl.h
	glBindBuffer = (PFNGLBINDBUFFERPROC)wglGetProcAddress("glBindBuffer");
	SDK_NULL_ASSERT(glBindBuffer);
	glBufferData = (PFNGLBUFFERDATAPROC)wglGetProcAddress("glBufferData");
	SDK_NULL_ASSERT(glBufferData);
	glBufferSubData = (PFNGLBUFFERSUBDATAPROC)wglGetProcAddress("glBufferSubData");
	SDK_NULL_ASSERT(glBufferSubData);
	glDeleteBuffers = (PFNGLDELETEBUFFERSPROC)wglGetProcAddress("glDeleteBuffers");
	SDK_NULL_ASSERT(glDeleteBuffers);
#endif

    gsEnableTexure = FALSE;
    gsEnableColor = FALSE;
    glEnableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    
#ifdef SDK_DEBUG
    glClearColor_x(GX_FX32_TO_GLfx32(FX32(0.5f)), GX_FX32_TO_GLfx32(FX32(0.0f)), GX_FX32_TO_GLfx32(FX32(0.5f)), GX_FX32_TO_GLfx32(FX32_ONE));
#else
    glClearColor_x(0, 0, 0, GX_FX32_TO_GLfx32(FX32_ONE));
#endif

    while(*start)
    {
        glDisable(*start++);
        glRender_CheckError("glDisable");
    }
    glHint(GL_POINT_SMOOTH_HINT, GL_FASTEST);
    glHint(GL_LINE_SMOOTH_HINT, GL_FASTEST);
    glRender_CheckError("glHint");

	glEnable(GL_LINE_STIPPLE);

	mCurrentTextureID = -1;
}
//----------------------------------------------------------------------------------

void glRender_SetRenderPlaneScale(fx32 val, enum BGSelect iBG)
{
	SDK_ASSERT(val > 0);
	SDK_ASSERT(iBG != BGSELECT_NUM);
	gsPlane[iBG].mScale = GX_FX32_TO_GLfx32(val);
	gsPlane[iBG].mLineWidth = GX_FX32_TO_GLfx32(FX32_ONE); 
	while(gsPlane[iBG].mScale > gsPlane[iBG].mLineWidth)
	{
		gsPlane[iBG].mLineWidth += GX_FX32_TO_GLfx32(FX32_ONE);
	}
}
//----------------------------------------------------------------------------------

fx32 glRender_GetRenderPlaneScale(enum BGSelect iBG)
{
	SDK_ASSERT(iBG != BGSELECT_NUM);
	return GX_GLfx32_TO_FX32(gsPlane[iBG].mScale);
}
//----------------------------------------------------------------------------------

void glRender_Resize(s32 w, s32 h)
{
	sgScreenWidth = w;
	sgScreenHeight = h;
	if(gsLostDevice == FALSE)
	{
		//glRender_RestoreDevice();
	}
}
//----------------------------------------------------------------------------------

s32 glRender_DrawFrame()
{
	s32 result;
	BOOL clear;
	s32 plane;

	if(sgScreenWidth < 0)
	{
		return 0;
	}

	result = 0;
	clear = FALSE;

	SDK_ASSERT(gsGraphicsInit == TRUE); // please init graphics system before
    
    if(!glIsEnabled(GL_BLEND))
    {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
    
	glPushMatrix();

	for(plane = 0; plane < BGSELECT_NUM; plane++)
	{
		if(gsPlane[plane].mDraw)
		{
			gsPlane[plane].mDraw++;

			if(clear == FALSE)
			{
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				clear = TRUE;
			}

			glViewport((GLint)(gsPlane[plane].mOffX),
						//sgScreenHeight -
						//(GLint)(gsPlane[plane].mViewHeight) -
						(GLint)(gsPlane[plane].mOffY),
						(GLint)(gsPlane[plane].mViewWidth),
						(GLint)(gsPlane[plane].mViewHeight));
			glRender_CheckError("glViewport");
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho_x(0, 
						GX_FX32_TO_GLfx32(FX_Div(FX32(gsPlane[plane].mViewWidth), GX_GLfx32_TO_FX32(gsPlane[plane].mScale))),
						GX_FX32_TO_GLfx32(FX_Div(FX32(gsPlane[plane].mViewHeight), GX_GLfx32_TO_FX32(gsPlane[plane].mScale))),
						0, -GX_FX32_TO_GLfx32(FX32_ONE), 0);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			if(_backdropFn != NULL)
			{
				_backdropFn(&mCurrentTextureID);
				glRender_CheckError("_backdropFn");
			}

            glColor4_x(1.0f, 1.0f, 1.0f, 1.0f);

			if(gsPlane[plane].mBGMaxLayers > 0)
			{
				s32 j;
				if(gsEnableTexure == FALSE)
				{
					glEnable(GL_TEXTURE_2D);
					glEnableClientState(GL_TEXTURE_COORD_ARRAY);
					gsEnableTexure = TRUE;
				}
				if(gsEnableColor == TRUE)
				{
					glDisableClientState(GL_COLOR_ARRAY);
					gsEnableColor = FALSE;
				}

				for(j = 0; j < gsPlane[plane].mBGMaxLayers; j++)
				{
					const struct TRGLBGLayer *bglayer = &gsPlane[plane].mpBGLayersList[j];
					if(bglayer->mRenderListSize >= 0)
					{
						s32 curr_idx = bglayer->mRenderListHead;
						while(curr_idx >= 0)
						{
							const struct TRGLBGRenderItem *item = &bglayer->mRenderListPool.at(curr_idx);
							SDK_ASSERT(bglayer->mRenderListPool.at(bglayer->mRenderListTail).mNext == -1);
							if(item->mpImageHeader->mOpaqType != mCurrentTextureID)
							{
								mCurrentTextureID = item->mpImageHeader->mOpaqType;
								//SDK_NULL_ASSERT(mpTexture);
								glBindTexture(GL_TEXTURE_2D, mCurrentTextureID);
								glRender_CheckError("glBindTexture");
							}
#ifdef USE_GL_GLEXT
                            SDK_ASSERT(0);
							/*glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpBGVBOBuffer[gsPlane[plane].mppBGRenderList[j][i].mVBOBufferIdx + 1]);
							if(item->mCt > 0)
							{
								glBufferData(GL_ARRAY_BUFFER, item->mCt * sizeof(GLfx32), item->mpBgTexCoordinates2dPtr, GL_DYNAMIC_DRAW);
							}
							glTexCoordPointer(2, GL_FIXED, 0, NULL);
							glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpBGVBOBuffer[gsPlane[plane].mppBGRenderList[j][i].mVBOBufferIdx]);
							if(item->mCt > 0)
							{
								glBufferData(GL_ARRAY_BUFFER, item->mCt * sizeof(GLfx32), item->mpBgQuadPtr, GL_DYNAMIC_DRAW);
								gsPlane[plane].mppBGRenderList[j][i].mCt = -item->mCt;
							}
							glVertexPointer(2, GL_FIXED, 0, NULL);
							glDrawArrays(GL_TRIANGLE_STRIP, 0, -item->mCt / 2);*/
#else
							glTexCoordPointer(2, GL_FIXED, 0, &bglayer->mTexCoordPool.at(item->mTexArrayIdx));
							glRender_CheckError("glTexCoordPointer");
							glVertexPointer(2, GL_FIXED, 0, &bglayer->mVertexPool.at(item->mVertexArrayIdx));
							glRender_CheckError("glVertexPointer");
							glDrawArrays(GL_TRIANGLE_STRIP, 0, item->mVertexCt / 2);
							glRender_CheckError("glDrawArrays");
#endif
							curr_idx = item->mNext;
						}
					}
				}
			}

			if(gsPlane[plane].mRenderListSize >= 0)
			{
				s32 curr_idx = gsPlane[plane].mRenderListHead;
				while(curr_idx >= 0)
				{
					struct TRGLRenderItem *curr = &gsPlane[plane].mRenderListPool.at(curr_idx);
					SDK_ASSERT(gsPlane[plane].mRenderListPool.at(gsPlane[plane].mRenderListTail).mNext == -1);
					switch(curr->mType)
					{
						case RGL_IMAGE:
#ifdef JOBS_IN_SEPARATE_THREAD
								jobCriticalSectionBegin();	
#endif
								if(gsEnableTexure == FALSE)
								{
									glEnable(GL_TEXTURE_2D);
									glEnableClientState(GL_TEXTURE_COORD_ARRAY);
									gsEnableTexure = TRUE;
								}
								if(curr->mpImageHeader->mOpaqType != mCurrentTextureID)
								{
									mCurrentTextureID = curr->mpImageHeader->mOpaqType;
									//SDK_NULL_ASSERT(mpTexture);
									glBindTexture(GL_TEXTURE_2D, mCurrentTextureID);
									glRender_CheckError("glBindTexture");
								}
								if(curr->mpClipRect != NULL)
								{
									GLint scsrx, scsry;
									scsrx = (GLint)(((gsPlane[plane].mOffX + ((curr->mpClipRect[0] >> 16) & 0xffff)) * gsPlane[plane].mScale) / GX_FX32_TO_GLfx32(FX32_ONE));
									scsry = sgScreenHeight -
											(GLint)(((curr->mpClipRect[1] & 0xffff) * gsPlane[plane].mScale) / GX_FX32_TO_GLfx32(FX32_ONE)) -
											(GLint)(((gsPlane[plane].mOffY + (curr->mpClipRect[0] & 0xffff)) * gsPlane[plane].mScale) / GX_FX32_TO_GLfx32(FX32_ONE));
									glEnable(GL_SCISSOR_TEST);
									glScissor(scsrx, scsry,
											(GLint)((((curr->mpClipRect[1] >> 16) & 0xffff) * gsPlane[plane].mScale) / GX_FX32_TO_GLfx32(FX32_ONE)),
											(GLint)(((curr->mpClipRect[1] & 0xffff) * gsPlane[plane].mScale) / GX_FX32_TO_GLfx32(FX32_ONE)));
								}
                                if(gsEnableColor == FALSE)
								{
									glEnableClientState(GL_COLOR_ARRAY);
									gsEnableColor = TRUE;
								}
#ifdef USE_GL_GLEXT
								glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx + 2]);
								if(curr->mVertexCt > 0)
								{
									glBufferData(GL_ARRAY_BUFFER, curr->mColorCt * sizeof(GLbyte), curr->mpImgColorArrayPtr, GL_DYNAMIC_DRAW);
								}
								glColorPointer(4, GL_UNSIGNED_BYTE, 0, NULL);
								glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx + 1]);
								if(curr->mVertexCt > 0)
								{
									glBufferData(GL_ARRAY_BUFFER, curr->mVertexCt * sizeof(GLfx32), curr->mpImgTexArrayPtr, GL_DYNAMIC_DRAW);
								}
								glTexCoordPointer(2, GL_FIXED, 0, NULL);
								glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx]);
								if(curr->mVertexCt > 0)
								{
									glBufferData(GL_ARRAY_BUFFER, curr->mVertexCt * sizeof(GLfx32), curr->mpImgVertexArrayPtr, GL_DYNAMIC_DRAW);
									curr->mVertexCt = -curr->mVertexCt;
								}
								glVertexPointer(2, GL_FIXED, 0, NULL);
								glDrawArrays(GL_TRIANGLE_STRIP, 0, -curr->mVertexCt / 2);
#else
								glColorPointer(4, GL_UNSIGNED_BYTE, 0, &gsPlane[plane].mImgColorPool.at(curr->mImgColorArrayIdx));
								glRender_CheckError("glColorPointer");
								glTexCoordPointer(2, GL_FIXED, 0, &gsPlane[plane].mImgTexCoordPool.at(curr->mImgTexArrayIdx));
								glRender_CheckError("glTexCoordPointer");
								glVertexPointer(2, GL_FIXED, 0, &gsPlane[plane].mImgVertexPool.at(curr->mImgVertexArrayIdx));
								glRender_CheckError("glVertexPointer");
								glDrawArrays(GL_TRIANGLE_STRIP, 0, curr->mVertexCt / 2);
								glRender_CheckError("glDrawArrays");
#endif
								if(curr->mpClipRect != NULL)
								{
									glDisable(GL_SCISSOR_TEST);
								}
#ifdef JOBS_IN_SEPARATE_THREAD
								jobCriticalSectionEnd();	
#endif
							break;

						case RGL_LINE:
                            if(gsEnableTexure == TRUE)
                            {
								glDisable(GL_TEXTURE_2D);
								glDisableClientState(GL_TEXTURE_COORD_ARRAY);
								gsEnableTexure = FALSE;
                            }
							if(gsEnableColor == FALSE)
							{
								glEnableClientState(GL_COLOR_ARRAY);
								gsEnableColor = TRUE;
							}
							glLineStipple(1, ((GLushort)curr->mpClipRect));
#ifdef USE_GL_GLEXT
							glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx + 2]);
							if(curr->mVertexCt > 0)
							{
								glBufferData(GL_ARRAY_BUFFER, curr->mColorCt * sizeof(GLbyte), curr->mpImgColorArrayPtr, GL_DYNAMIC_DRAW);
							}
							glColorPointer(4, GL_UNSIGNED_BYTE, 0, NULL);
							glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx]);
							if(curr->mVertexCt > 0)
							{
								glBufferData(GL_ARRAY_BUFFER, curr->mVertexCt * sizeof(GLfx32), curr->mpImgVertexArrayPtr, GL_DYNAMIC_DRAW);
								curr->mVertexCt = -curr->mVertexCt;
							}
							glVertexPointer(2, GL_FIXED, 0, NULL);
							glDrawArrays(GL_LINES, 0, -curr->mVertexCt / 2);
#else
							glColorPointer(4, GL_UNSIGNED_BYTE, 0, &gsPlane[plane].mImgColorPool.at(curr->mImgColorArrayIdx));
							glVertexPointer(2, GL_FIXED, 0, &gsPlane[plane].mImgVertexPool.at(curr->mImgVertexArrayIdx));
							glDrawArrays(GL_LINES, 0, curr->mVertexCt / 2);
#endif
						break;

						case RGL_FILLRECT:
                            if(gsEnableTexure == TRUE)
                            {
								glDisable(GL_TEXTURE_2D);
								glDisableClientState(GL_TEXTURE_COORD_ARRAY);
								gsEnableTexure = FALSE;
                            }
							if(gsEnableColor == FALSE)
							{
								glEnableClientState(GL_COLOR_ARRAY);
								gsEnableColor = TRUE;
							}
#ifdef USE_GL_GLEXT
							glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx + 2]);
							if(curr->mVertexCt > 0)
							{
								glBufferData(GL_ARRAY_BUFFER, curr->mColorCt * sizeof(GLbyte), curr->mpImgColorArrayPtr, GL_DYNAMIC_DRAW);
							}
							glColorPointer(4, GL_UNSIGNED_BYTE, 0, NULL);
							glBindBuffer(GL_ARRAY_BUFFER, gsPlane[plane].mpVBOBuffer[curr->mVBOBufferIdx]);
							if(curr->mVertexCt > 0)
							{
								glBufferData(GL_ARRAY_BUFFER, curr->mVertexCt * sizeof(GLfx32), curr->mpImgVertexArrayPtr, GL_DYNAMIC_DRAW);
								curr->mVertexCt = -curr->mVertexCt;
							}
							glVertexPointer(2, GL_FIXED, 0, NULL);
							glDrawArrays(GL_TRIANGLE_STRIP, 0, -curr->mVertexCt / 2);
#else
							glColorPointer(4, GL_UNSIGNED_BYTE, 0, &gsPlane[plane].mImgColorPool.at(curr->mImgColorArrayIdx));
							glVertexPointer(2, GL_FIXED, 0, &gsPlane[plane].mImgVertexPool.at(curr->mImgVertexArrayIdx));
							glDrawArrays(GL_TRIANGLE_STRIP, 0, curr->mVertexCt / 2);

#endif
						break;

						default:
							SDK_ASSERT(0);// error sortList member type;

					}
					curr_idx = curr->mNext;
				}
			}
			result = 1;
		}
	}

	glPopMatrix();

	return result;
}
//----------------------------------------------------------------------------------

BOOL glRender_IsGraphicsInit(void)
{
	return gsGraphicsInit;
}
//----------------------------------------------------------------------------------

void glRender_PlaneInit(const struct RenderPlaneInitParams* ipParams)
{
    if(ipParams == NULL)
    {
        SDK_ASSERT(0);
        return;
    }

	SDK_ASSERT(ipParams->mBGType != BGSELECT_NUM);

	gsPlane[ipParams->mBGType].mInit = TRUE;

	//gsPlane[ipParams->mBGType].mMaxObjOnScene = (s32)ipParams->mMaxRenderObjectsOnPlane;
	//gsPlane[ipParams->mBGType].mpRenderListPool = (struct TRGLRenderItem *)MALLOC(gsPlane[ipParams->mBGType].mMaxObjOnScene * sizeof(struct TRGLRenderItem), "PlaneInit::mpRenderListPool");
	//gsPlane[ipParams->mBGType].mpImgVertexPool = (GLfx32*)MALLOC(gsPlane[ipParams->mBGType].mMaxObjOnScene * 12 * sizeof(GLfx32), "PlaneInit::mpImgVertexPool");
	//gsPlane[ipParams->mBGType].mpImgTexCoordPool = (GLfx32*)MALLOC(gsPlane[ipParams->mBGType].mMaxObjOnScene * 12 * sizeof(GLfx32), "PlaneInit::mpImgTexCoordPool");
	//gsPlane[ipParams->mBGType].mpImgColorPool = (GLbyte*)MALLOC(gsPlane[ipParams->mBGType].mMaxObjOnScene * 24 * sizeof(GLbyte), "PlaneInit::mpImgColorPool");
	gsPlane[ipParams->mBGType].mRenderListPool.clear();
	gsPlane[ipParams->mBGType].mImgVertexPool.clear();
	gsPlane[ipParams->mBGType].mImgTexCoordPool.clear();
	gsPlane[ipParams->mBGType].mImgColorPool.clear();

	gsPlane[ipParams->mBGType].mColor.r = 0;
	gsPlane[ipParams->mBGType].mColor.g = 0;
	gsPlane[ipParams->mBGType].mColor.b = 0;
	gsPlane[ipParams->mBGType].mColor.a = 0;
	gsPlane[ipParams->mBGType].mOffX = ipParams->mX; 
	gsPlane[ipParams->mBGType].mOffY = ipParams->mY;
	gsPlane[ipParams->mBGType].mViewWidth = ipParams->mSizes.mViewWidth;
	gsPlane[ipParams->mBGType].mViewHeight = ipParams->mSizes.mViewHeight;
	SDK_ASSERT(gsPlane[ipParams->mBGType].mpBGLayersList == NULL);
	gsPlane[ipParams->mBGType].mLineStyle = 0xFFFF;

	glRender_ClearFrameBuffer(ipParams->mBGType);
}
//----------------------------------------------------------------------------------

BOOL glRender_IsRenderPlaneInit(enum BGSelect iType)
{
	return gsGraphicsInit && gsPlane[iType].mInit == TRUE;
}
//----------------------------------------------------------------------------------

void glRender_PlaneResize(enum BGSelect iType, const struct RenderPlaneSizeParams* ipParams)
{
	gsPlane[iType].mViewWidth  = ipParams->mViewWidth;
	gsPlane[iType].mViewHeight = ipParams->mViewHeight;
}
//----------------------------------------------------------------------------------

void glRender_PlanePosition(enum BGSelect iType, s32 offX, s32 offY)
{
	gsPlane[iType].mOffX = offX;
	gsPlane[iType].mOffY = offY;
}
//----------------------------------------------------------------------------------

void glRender_PlaneRelease(enum BGSelect iType)
{
	SDK_ASSERT(iType != BGSELECT_NUM);
	/*
	if(gsPlane[iType].mpImgVertexPool != NULL)
	{
		FREE(gsPlane[iType].mpImgColorPool);
		FREE(gsPlane[iType].mpImgTexCoordPool);
		FREE(gsPlane[iType].mpImgVertexPool);
		FREE(gsPlane[iType].mpRenderListPool);
		gsPlane[iType].mpImgColorPool = NULL;
		gsPlane[iType].mpImgTexCoordPool = NULL;
		gsPlane[iType].mpImgVertexPool = NULL;
		gsPlane[iType].mpRenderListPool = NULL;
	}
	*/
	gsPlane[iType].mImgColorPool.clear();
	gsPlane[iType].mImgTexCoordPool.clear();
	gsPlane[iType].mImgVertexPool.clear();
	gsPlane[iType].mRenderListPool.clear();
	gsPlane[iType].mInit = FALSE;
}
//----------------------------------------------------------------------------------

void glRender_SetupBGLayersData(enum BGSelect bgType, s32 maxLayers, s32 maxTextures, s32 maxElements)
{
	s32 i;

	SDK_ASSERT(gsGraphicsInit);
	SDK_ASSERT(gsPlane[bgType].mpBGLayersList == NULL); //please call glRender_ReleaseBGLayersData before

	maxLayers = gsPlane[bgType].mBGMaxLayers > maxLayers ? gsPlane[bgType].mBGMaxLayers : maxLayers;
	maxTextures = gsPlane[bgType].mBGMaxTextures > maxTextures ? gsPlane[bgType].mBGMaxTextures : maxTextures;
	maxElements = gsPlane[bgType].mBGMaxElements > maxElements ? gsPlane[bgType].mBGMaxElements : maxElements;

	if(maxLayers)
	{
#ifdef USE_GL_GLEXT
		SDK_ASSERT(gsPlane[bgType].mpBGVBOBuffer == NULL); //please call glRender_ReleaseBGLayersData before
		gsPlane[bgType].mpBGVBOBuffer = (GLuint*)MALLOC(maxLayers * (maxTextures * sizeof(GLuint) * 2), "SetupBGLayersData:mpBGVBOBuffer");
		glGenBuffers(maxLayers * (maxTextures * 2), gsPlane[bgType].mpBGVBOBuffer);
#endif
		gsPlane[bgType].mpBGLayersList = new TRGLBGLayer[maxLayers];
	}

	for(i = 0; i < maxLayers; i++)
	{
		gsPlane[bgType].mpBGLayersList[i].mRenderListSize = -1;
		gsPlane[bgType].mpBGLayersList[i].mRenderListHead = -1;
		gsPlane[bgType].mpBGLayersList[i].mRenderListTail = -1;
		gsPlane[bgType].mpBGLayersList[i].mVertexCt = 0;
		gsPlane[bgType].mpBGLayersList[i].mTexCt = 0;
/*#ifdef USE_GL_GLEXT
		s32 j;
		for(j = 0; j < maxTextures; j++)
		{
			gsPlane[bgType].mppBGRenderList[i][j].mVBOBufferIdx = i * (maxTextures * 2) + j * 2;
			//glBindBuffer(GL_ARRAY_BUFFER, gsPlane[bgType].mpBGVBOBuffer[gsPlane[bgType].mppBGRenderList[i][j].mVBOBufferIdx]);
			//glRender_CheckError("glBindBuffer");
			//glBufferData(GL_ARRAY_BUFFER, 12 * maxElements * sizeof(GLfx32), NULL, GL_DYNAMIC_DRAW);
			//glRender_CheckError("glBufferData");
			//glBindBuffer(GL_ARRAY_BUFFER, gsPlane[bgType].mpBGVBOBuffer[gsPlane[bgType].mppBGRenderList[i][j].mVBOBufferIdx + 1]);
			//glRender_CheckError("glBindBuffer");
			//glBufferData(GL_ARRAY_BUFFER, 12 * maxElements * sizeof(GLfx32), NULL, GL_DYNAMIC_DRAW);
			//glRender_CheckError("glBufferData");
#endif*/
	}
/*#ifdef USE_GL_GLEXT
	//glBindBuffer(GL_ARRAY_BUFFER, 0);
#endif*/
	gsPlane[bgType].mBGMaxLayers = maxLayers;
	gsPlane[bgType].mBGMaxTextures = maxTextures;
	gsPlane[bgType].mBGMaxElements = maxElements;
}
//----------------------------------------------------------------------------------

void glRender_ReleaseBGLayersData(enum BGSelect bgType)
{
	if(gsPlane[bgType].mBGMaxLayers > 0)
	{
		delete[] gsPlane[bgType].mpBGLayersList;
		gsPlane[bgType].mpBGLayersList = NULL;
#ifdef USE_GL_GLEXT
#if defined ANDROID_NDK
        // do nothing
#else
		glDeleteBuffers(gsPlane[bgType].mBGMaxLayers * gsPlane[bgType].mBGMaxTextures * 2, gsPlane[bgType].mpBGVBOBuffer);
#endif
		FREE(gsPlane[bgType].mpBGVBOBuffer);
		gsPlane[bgType].mpBGVBOBuffer = NULL;
#endif
	}
	gsPlane[bgType].mBGMaxLayers = 0;
	gsPlane[bgType].mBGMaxTextures = 0;
	gsPlane[bgType].mBGMaxElements = 0;
}
//----------------------------------------------------------------------------------

void glRender_SetActiveBGForGraphics(enum BGSelect iActiveBG)
{
   if(gsActivePlane != iActiveBG)
   {
		gsActivePlane = iActiveBG;
		SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
   }
}
//----------------------------------------------------------------------------------

enum BGSelect glRender_GetActiveBGForGraphics(void)
{
	return gsActivePlane; 
}
//----------------------------------------------------------------------------------

u32 glRender_CreateTexture()
{
	u32 txt[1];
	mCurrentTextureID = -1;
	glGenTextures(1, txt);
	glRender_CheckError("glGenTextures");
	return txt[0];
}
//----------------------------------------------------------------------------------

void glRender_DeleteTexture(u32 idx)
{
	u32 txt[1];
	txt[0] = idx;
	glDeleteTextures(1, txt);
	glRender_CheckError("glDeleteTextures");
}
//----------------------------------------------------------------------------------

s32 glRender_GetViewWidth(enum BGSelect iBGType)
{
	return gsPlane[iBGType].mViewWidth;
}
//----------------------------------------------------------------------------------

s32 glRender_GetViewHeight(enum BGSelect iBGType)
{
	return gsPlane[iBGType].mViewHeight;
}
//----------------------------------------------------------------------------------

s32 glRender_GetFrameBufferWidth(enum BGSelect iBGType)
{
	return gsPlane[iBGType].mViewWidth;
}
//----------------------------------------------------------------------------------

s32 glRender_GetViewLeft(enum BGSelect iBGType)
{
	return gsPlane[iBGType].mOffX;	
}
//----------------------------------------------------------------------------------

s32 glRender_GetViewTop(enum BGSelect iBGType)
{
	return gsPlane[iBGType].mOffY;
}
//----------------------------------------------------------------------------------

s32 glRender_GetFrameBufferHeight(enum BGSelect iBGType)
{
	return gsPlane[iBGType].mViewHeight;
}
//----------------------------------------------------------------------------------

void glRender_LoadTextureToVRAM(const BMPImage *pImage, u32 resId)
{
	SDK_NULL_ASSERT(pImage);
	u8* buf8888 = pImage->CreateRGBAData();
	SDK_ASSERT(pImage->mType == BMP_TYPE_DC16 || pImage->mType == BMP_TYPE_DC32);
	mCurrentTextureID = -1;
	//SDK_NULL_ASSERT(mpTexture);
	glBindTexture(GL_TEXTURE_2D, resId);
	glRender_CheckError("glBindTexture");
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexImage2D(GL_TEXTURE_2D,
					0,
					GL_RGBA,
					pImage->mWidth2n,
					pImage->mHeight2n,
					0,
					GL_RGBA,
					GL_UNSIGNED_BYTE,
					buf8888);
	glRender_CheckError("glTexImage2D");
	if(buf8888 != NULL)
	{
		delete[] buf8888;
	}
}
//----------------------------------------------------------------------------------

void glRender_ClearFrameBuffer(enum BGSelect iType)
{
	if(iType != BGSELECT_NUM)
	{
		s32 i;
		gsPlane[iType].mRenderListSize = -1;
		gsPlane[iType].mRenderListHead = -1;
		gsPlane[iType].mImgColorCt = 0;
		gsPlane[iType].mImgVertexCt = 0;
		gsPlane[iType].mImgTexCt = 0;
		gsPlane[iType].mDraw = 0;
#ifdef USE_GL_GLEXT
		gsPlane[iType].mObjVBOBufferLastIdx = 0;
#endif
		gsPlane[iType].mRenderListPool.clear();
		gsPlane[iType].mImgVertexPool.clear();
		gsPlane[iType].mImgTexCoordPool.clear();
		gsPlane[iType].mImgColorPool.clear();

		for(i = 0; i < gsPlane[iType].mBGMaxLayers; i++)
		{
			TRGLBGLayer *bglayer = &gsPlane[iType].mpBGLayersList[i];
			bglayer->mRenderListSize = -1;
			bglayer->mRenderListHead = -1;
			bglayer->mVertexCt = 0;
			bglayer->mTexCt = 0;
			bglayer->mRenderListPool.clear();
			bglayer->mVertexPool.clear();
			bglayer->mTexCoordPool.clear();
		}
	}
}
//----------------------------------------------------------------------------------

void glRender_CheckForCleanup(enum BGSelect iType)
{
	if(gsPlane[iType].mDraw > 1)
	{
		glRender_ClearFrameBuffer(iType);
	}
}
//----------------------------------------------------------------------------------

void glRender_AddToRenderList(struct TRGLRenderItem* item)
{
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	s32 lastidx = gsPlane[gsActivePlane].mRenderListPool.size() - 1;
	switch(item->mType)
	{
		case RGL_PIXEL:
		case RGL_LINE:
		case RGL_FILLRECT:
		case RGL_IMAGE:
			if(gsPlane[gsActivePlane].mRenderListHead == -1)
			{
				item->mNext = -1;
				gsPlane[gsActivePlane].mRenderListTail = gsPlane[gsActivePlane].mRenderListHead = lastidx;
			}
			else
			{
				item->mNext = -1;
				gsPlane[gsActivePlane].mRenderListPool.at(gsPlane[gsActivePlane].mRenderListTail).mNext = lastidx;
				gsPlane[gsActivePlane].mRenderListTail = lastidx;
			}
		break;
		default:
			SDK_ASSERT(0);
	}
}
//----------------------------------------------------------------------------------------------------------------

void glRender_SetColor(u8 r, u8 g, u8 b, u8 a)
{
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	gsPlane[gsActivePlane].mColor.r = r;
	gsPlane[gsActivePlane].mColor.g = g;
	gsPlane[gsActivePlane].mColor.b = b;
	gsPlane[gsActivePlane].mColor.a = a;
}
//----------------------------------------------------------------------------------------------------------------

void glRender_SetLineStyle(u16 iStyle)
{
	gsPlane[gsActivePlane].mLineStyle = iStyle;
}
//----------------------------------------------------------------------------------------------------------------

void glRender_DrawLine(fx32 iX0, fx32 iY0, fx32 iX1, fx32 iY1)
{
	BOOL newItem;
	struct TRGLRenderItem *item;
	
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);
	
	glRender_CheckForCleanup(gsActivePlane);

	newItem = FALSE;
	if(!(gsPlane[gsActivePlane].mRenderListSize >= 0 && 
		gsPlane[gsActivePlane].mRenderListPool.at(gsPlane[gsActivePlane].mRenderListSize).mType == RGL_LINE &&
		((GLushort)gsPlane[gsActivePlane].mRenderListPool.at(gsPlane[gsActivePlane].mRenderListSize).mpClipRect) ==
			gsPlane[gsActivePlane].mLineStyle))
	{
		gsPlane[gsActivePlane].mRenderListSize++;
		newItem = TRUE;
		gsPlane[gsActivePlane].mRenderListPool.push_back(TRGLRenderItem());
	}

	/*
	SDK_ASSERT(gsPlane[gsActivePlane].mRenderListSize <= gsPlane[gsActivePlane].mMaxObjOnScene);
	if(gsPlane[gsActivePlane].mRenderListSize > gsPlane[gsActivePlane].mMaxObjOnScene)
	{
		OS_Printf("glRender: RenderListSize overflow\n");
		SDK_ASSERT(0);
		gsPlane[gsActivePlane].mRenderListSize = gsPlane[gsActivePlane].mMaxObjOnScene - 1;
		return;
	}

	SDK_ASSERT(gsPlane[gsActivePlane].mImgVertexCt + 4 <= gsPlane[gsActivePlane].mMaxObjOnScene * 12);
	if(gsPlane[gsActivePlane].mImgVertexCt + 4 > gsPlane[gsActivePlane].mMaxObjOnScene * 12)
	{
		OS_Printf("glRender: RenderList vertex pool overflow\n");
		SDK_ASSERT(0);
		return;
	}
	*/

	gsPlane[gsActivePlane].mDraw = 1;
	item = &gsPlane[gsActivePlane].mRenderListPool.at(gsPlane[gsActivePlane].mRenderListSize);
	if(newItem)
	{
		item->mpImageHeader = NULL;
		item->mImgVertexArrayIdx = gsPlane[gsActivePlane].mImgVertexCt;
		item->mImgTexArrayIdx = 0;
		item->mImgColorArrayIdx = gsPlane[gsActivePlane].mImgColorCt;
		item->mVertexCt = 0;
		item->mColorCt = 0;
#ifdef USE_GL_GLEXT
		item->mVBOBufferIdx = gsPlane[gsActivePlane].mObjVBOBufferLastIdx;
		gsPlane[gsActivePlane].mObjVBOBufferLastIdx += 3;
#endif
		item->mType = RGL_LINE;
		item->mpClipRect = reinterpret_cast<const s32*>((s32)gsPlane[gsActivePlane].mLineStyle);
		glRender_AddToRenderList(item);
	}
	gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iX0));
	gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iY0));
	gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iX1));
	gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iY1));
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.r);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.g);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.b);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.a);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.r);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.g);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.b);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.a);
	gsPlane[gsActivePlane].mImgVertexCt += 4;
	item->mVertexCt += 4;
	gsPlane[gsActivePlane].mImgColorCt += 8;
	item->mColorCt += 8;
}
//----------------------------------------------------------------------------------------------------------------

void glRender_ColorRect(fx32 iX, fx32 iY, fx32 iWidth, fx32 iHeight)
{
	BOOL newItem;
	struct TRGLRenderItem *item;

	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);

	glRender_CheckForCleanup(gsActivePlane);

	newItem = FALSE;
	if(!(gsPlane[gsActivePlane].mRenderListSize >= 0 && 
		gsPlane[gsActivePlane].mRenderListPool.at(gsPlane[gsActivePlane].mRenderListSize).mType == RGL_FILLRECT))
	{
		gsPlane[gsActivePlane].mRenderListSize++;
		newItem = TRUE;
		gsPlane[gsActivePlane].mRenderListPool.push_back(TRGLRenderItem());
	}

	/*
	SDK_ASSERT(gsPlane[gsActivePlane].mRenderListSize <= gsPlane[gsActivePlane].mMaxObjOnScene);
	if(gsPlane[gsActivePlane].mRenderListSize > gsPlane[gsActivePlane].mMaxObjOnScene)
	{
		OS_Printf("glRender: RenderListSize overflow\n");
		SDK_ASSERT(0);
		gsPlane[gsActivePlane].mRenderListSize = gsPlane[gsActivePlane].mMaxObjOnScene - 1;
		return;
	}

	SDK_ASSERT(gsPlane[gsActivePlane].mImgVertexCt + 12 <= gsPlane[gsActivePlane].mMaxObjOnScene * 12);
	if(gsPlane[gsActivePlane].mImgVertexCt + 12 > gsPlane[gsActivePlane].mMaxObjOnScene * 12)
	{
		OS_Printf("glRender: RenderList vertex pool overflow\n");
		SDK_ASSERT(0);
		return;
	}
	*/

	gsPlane[gsActivePlane].mDraw = 1;
	item = &gsPlane[gsActivePlane].mRenderListPool.at(gsPlane[gsActivePlane].mRenderListSize);

	if(newItem)
	{
		item->mpImageHeader = NULL;
		item->mImgVertexArrayIdx = gsPlane[gsActivePlane].mImgVertexCt;
		item->mImgTexArrayIdx = 0;
		item->mImgColorArrayIdx = gsPlane[gsActivePlane].mImgColorCt;
		item->mVertexCt = 0;
		item->mColorCt = 0;
#ifdef USE_GL_GLEXT
		item->mVBOBufferIdx = gsPlane[gsActivePlane].mObjVBOBufferLastIdx;
		gsPlane[gsActivePlane].mObjVBOBufferLastIdx += 3;
#endif
		item->mType = RGL_FILLRECT;
		glRender_AddToRenderList(item);
	}
			
	if(item->mVertexCt > 0)
	{				
		gsPlane[gsActivePlane].mImgVertexPool.push_back(gsPlane[gsActivePlane].mImgVertexPool[item->mImgVertexArrayIdx + 6 + item->mVertexCt - 8]);
		gsPlane[gsActivePlane].mImgVertexPool.push_back(gsPlane[gsActivePlane].mImgVertexPool[item->mImgVertexArrayIdx + 7 + item->mVertexCt - 8]);
		gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iX));
		gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iY));
		gsPlane[gsActivePlane].mImgColorPool.push_back(0);
		gsPlane[gsActivePlane].mImgColorPool.push_back(0);
		gsPlane[gsActivePlane].mImgColorPool.push_back(0);
		gsPlane[gsActivePlane].mImgColorPool.push_back(0);
		gsPlane[gsActivePlane].mImgColorPool.push_back(0);
		gsPlane[gsActivePlane].mImgColorPool.push_back(0);
		gsPlane[gsActivePlane].mImgColorPool.push_back(0);
		gsPlane[gsActivePlane].mImgColorPool.push_back(0);
		gsPlane[gsActivePlane].mImgVertexCt += 4;
		item->mVertexCt += 4;
		gsPlane[gsActivePlane].mImgColorCt += 8;
		item->mColorCt += 8;
	}
	gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iX));
	gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iY));
	gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iX));
	gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iY + iHeight));
	gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iX + iWidth));
	gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iY));
	gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iX + iWidth));
	gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(iY + iHeight));

	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.r);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.g);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.b);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.a);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.r);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.g);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.b);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.a);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.r);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.g);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.b);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.a);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.r);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.g);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.b);
	gsPlane[gsActivePlane].mImgColorPool.push_back(gsPlane[gsActivePlane].mColor.a);

	gsPlane[gsActivePlane].mImgVertexCt += 8;
	item->mVertexCt += 8;
	gsPlane[gsActivePlane].mImgColorCt += 16;
	item->mColorCt += 16;
}
//----------------------------------------------------------------------------------------------------------------

void glRender_DrawImage(const struct DrawImageFunctionData* data)
{
	SDK_ASSERT(gsActivePlane != BGSELECT_NUM);

	switch(data->mType)
	{
		case DIT_TileBG1:
		case DIT_TileBG2:
		case DIT_TileBG3:
		case DIT_TileBG4:
		case DIT_TileBG5:
		{
			if(gsPlane[gsActivePlane].mBGMaxLayers > 0)
			{
				float fx, fy, yb, xl, fw, fh;
				struct TRGLBGLayer *bglayer;
				struct TRGLBGRenderItem *item;
				BOOL newItem;

				glRender_CheckForCleanup(gsActivePlane);

				SDK_NULL_ASSERT(gsPlane[gsActivePlane].mpBGLayersList);
				bglayer = &gsPlane[gsActivePlane].mpBGLayersList[data->mType];

				newItem = FALSE;
				if(!(bglayer->mRenderListSize >= 0 &&
						bglayer->mRenderListPool.at(bglayer->mRenderListSize).mpImageHeader != NULL &&
						bglayer->mRenderListPool.at(bglayer->mRenderListSize).mpImageHeader->mOpaqType == data->mpSrcData->mOpaqType))
				{
					bglayer->mRenderListSize++;
					newItem = TRUE;
					bglayer->mRenderListPool.push_back(TRGLBGRenderItem());
				}

				item = &bglayer->mRenderListPool.at(bglayer->mRenderListSize);
				gsPlane[gsActivePlane].mDraw = 1;

				if(newItem)
				{
					item->mpImageHeader = data->mpSrcData;
					item->mVertexArrayIdx = bglayer->mVertexCt;
					item->mTexArrayIdx = bglayer->mTexCt;
					item->mVertexCt = 0;
					s32 lastidx = bglayer->mRenderListPool.size() - 1;
					if(bglayer->mRenderListHead == -1)
					{
						item->mNext = -1;
						bglayer->mRenderListTail = bglayer->mRenderListHead = lastidx;
					}
					else
					{
						item->mNext = -1;
						bglayer->mRenderListPool.at(bglayer->mRenderListTail).mNext = lastidx;
						bglayer->mRenderListTail = lastidx;
					}
				}

				if(item->mVertexCt > 0)
				{
					bglayer->mVertexPool.push_back(bglayer->mVertexPool[item->mVertexArrayIdx + 6 + item->mVertexCt - 8]);
					bglayer->mVertexPool.push_back(bglayer->mVertexPool[item->mVertexArrayIdx + 7 + item->mVertexCt - 8]);
					bglayer->mVertexPool.push_back(GX_FX32_TO_GLfx32(data->mX));
					bglayer->mVertexPool.push_back(GX_FX32_TO_GLfx32(data->mY));
					bglayer->mTexCoordPool.push_back(0);
					bglayer->mTexCoordPool.push_back(0);
					bglayer->mTexCoordPool.push_back(0);
					bglayer->mTexCoordPool.push_back(0);
					bglayer->mVertexCt += 4;
					item->mVertexCt += 4;
					bglayer->mTexCt += 4;
				}
				xl = GX_FX32_TO_GLfx32(data->mX + FX32(data->mSrcSizeData[SRC_DATA_W]) * data->mScaleX);
				yb = GX_FX32_TO_GLfx32(data->mY + FX32(data->mSrcSizeData[SRC_DATA_H]) * data->mScaleY);
				fx = GX_FX32_TO_GLfx32(data->mX);
				fy = GX_FX32_TO_GLfx32(data->mY);
				bglayer->mVertexPool.push_back(fx);
				bglayer->mVertexPool.push_back(fy);
				bglayer->mVertexPool.push_back(fx);
				bglayer->mVertexPool.push_back(yb);
				bglayer->mVertexPool.push_back(xl);
				bglayer->mVertexPool.push_back(fy);
				bglayer->mVertexPool.push_back(xl);
				bglayer->mVertexPool.push_back(yb);
				if(gsGlobalScale < 0.0f)
				{
					yb = FX32_ONE / data->mpSrcData->mHeight2n / 2;
					xl = FX32_ONE / data->mpSrcData->mWidth2n / 2;
				}
				else
				{
					yb = xl = 0.0f;
				}
				fx = GX_FX32_TO_GLfx32(FX32(data->mSrcSizeData[SRC_DATA_X]) / data->mpSrcData->mWidth2n + xl);
				fy = GX_FX32_TO_GLfx32(FX32(data->mSrcSizeData[SRC_DATA_Y]) / data->mpSrcData->mHeight2n + yb);
				fw = GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_W] + data->mSrcSizeData[SRC_DATA_X])) / data->mpSrcData->mWidth2n - xl);
				fh = GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y])) / data->mpSrcData->mHeight2n - yb);
				bglayer->mTexCoordPool.push_back(fx); // 0
				bglayer->mTexCoordPool.push_back(fy); // 1
				bglayer->mTexCoordPool.push_back(fx); // 2
				bglayer->mTexCoordPool.push_back(fh); // 3
				bglayer->mTexCoordPool.push_back(fw); // 4
				bglayer->mTexCoordPool.push_back(fy); // 5
				bglayer->mTexCoordPool.push_back(fw); // 6
				bglayer->mTexCoordPool.push_back(fh); // 7
				item->mVertexCt += 8;
				bglayer->mVertexCt += 8;
				bglayer->mTexCt += 8;
			}
		}
		break;

		case DIT_Obj:
		{
			struct TRGLRenderItem *item;
			BOOL newItem;
			u8 cr, cg, cb, ca;
			fx32 xl, yt, rt, yb;
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			fx32 x0, y0;
#endif
			glRender_CheckForCleanup(gsActivePlane);

			newItem = FALSE;
			if(!(gsPlane[gsActivePlane].mRenderListSize >= 0 &&
					gsPlane[gsActivePlane].mRenderListPool.at(gsPlane[gsActivePlane].mRenderListSize).mpImageHeader != NULL &&
					gsPlane[gsActivePlane].mRenderListPool.at(gsPlane[gsActivePlane].mRenderListSize).mpImageHeader->mOpaqType == data->mpSrcData->mOpaqType &&
					gsPlane[gsActivePlane].mRenderListPool.at(gsPlane[gsActivePlane].mRenderListSize).mpClipRect == data->mpClipRect))
			{
				gsPlane[gsActivePlane].mRenderListSize++;
				newItem = TRUE;
				gsPlane[gsActivePlane].mRenderListPool.push_back(TRGLRenderItem());
			}

			/*
			SDK_ASSERT(gsPlane[gsActivePlane].mRenderListSize <= gsPlane[gsActivePlane].mMaxObjOnScene); // "mRenderListSize > RENDERLIST_MAX";
			if(gsPlane[gsActivePlane].mRenderListSize > gsPlane[gsActivePlane].mMaxObjOnScene)
			{
				OS_Printf("glRender: RenderListSize overflow\n");
				SDK_ASSERT(0);
				gsPlane[gsActivePlane].mRenderListSize = gsPlane[gsActivePlane].mMaxObjOnScene - 1;
				return;
			}

			SDK_ASSERT(gsPlane[gsActivePlane].mImgVertexCt + 12 <= gsPlane[gsActivePlane].mMaxObjOnScene * 12);
			if(gsPlane[gsActivePlane].mImgVertexCt + 12 > gsPlane[gsActivePlane].mMaxObjOnScene * 12)
			{
				OS_Printf("glRender: RenderList vertex pool overflow\n");
				SDK_ASSERT(0);
				return;
			}
			*/

			item = &gsPlane[gsActivePlane].mRenderListPool.at(gsPlane[gsActivePlane].mRenderListSize);
			gsPlane[gsActivePlane].mDraw = 1;

			if(newItem)
			{
				item->mpImageHeader = data->mpSrcData;
				item->mpClipRect = data->mpClipRect;
				item->mImgVertexArrayIdx = gsPlane[gsActivePlane].mImgVertexCt;
				item->mImgTexArrayIdx = gsPlane[gsActivePlane].mImgTexCt;
				item->mImgColorArrayIdx = gsPlane[gsActivePlane].mImgColorCt;
				item->mVertexCt = 0;
				item->mColorCt = 0;
#ifdef USE_GL_GLEXT
				item->mVBOBufferIdx = gsPlane[gsActivePlane].mObjVBOBufferLastIdx;
				gsPlane[gsActivePlane].mObjVBOBufferLastIdx += 3;
#endif
				item->mType = RGL_IMAGE;
				glRender_AddToRenderList(item);
			}
			
			if(data->mFillWithColor != 0)
			{
				//cr = GX_COLOR_CHANNEL((data->mFillWithColor & GX_RGBA_R_MASK) >> GX_RGBA_R_SHIFT);
				//cg = GX_COLOR_CHANNEL((data->mFillWithColor & GX_RGBA_G_MASK) >> GX_RGBA_G_SHIFT);
				//cb = GX_COLOR_CHANNEL((data->mFillWithColor & GX_RGBA_B_MASK) >> GX_RGBA_B_SHIFT);
				//ca = GX_COLOR_CHANNEL(data->mAlpha);
				cr = cg = cb = 255;
				ca = 255;
			}
			else
			{
				cr = cg = cb = 255;
				ca = data->mAlpha;
			}
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			xl = FX_Mul(data->mScaleX, data->mAffineOriginX);
			yt = FX_Mul(data->mScaleY, data->mAffineOriginY);
			rt = FX_Mul(data->mScaleX, FX32(data->mSrcSizeData[SRC_DATA_W]) + data->mAffineOriginX);
			yb = FX_Mul(data->mScaleY, FX32(data->mSrcSizeData[SRC_DATA_H]) + data->mAffineOriginY);
			x0 = data->mX - data->mAffineOriginX + FX_Mul(xl, data->mCos) + FX_Mul(yt, -data->mSin);
			y0 = data->mY - data->mAffineOriginY + FX_Mul(xl, data->mSin) + FX_Mul(yt, data->mCos);
#else
			xl = data->mX;
			yt = data->mY;
			rt = data->mX + FX32(data->mSrcSizeData[SRC_DATA_W]);
			yb = data->mY + FX32(data->mSrcSizeData[SRC_DATA_H]);
#endif
			if(item->mVertexCt > 0)
			{
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
				gsPlane[gsActivePlane].mImgVertexPool.push_back(gsPlane[gsActivePlane].mImgVertexPool[item->mImgVertexArrayIdx + 6 + item->mVertexCt - 8]);
				gsPlane[gsActivePlane].mImgVertexPool.push_back(gsPlane[gsActivePlane].mImgVertexPool[item->mImgVertexArrayIdx + 7 + item->mVertexCt - 8]);
				gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(x0));
				gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(y0));
#else
				gsPlane[gsActivePlane].mImgVertexPool.push_back(gsPlane[gsActivePlane].mImgVertexPool[item->mImgVertexArrayIdx + 6 + item->mVertexCt - 8]);
				gsPlane[gsActivePlane].mImgVertexPool.push_back(gsPlane[gsActivePlane].mImgVertexPool[item->mImgVertexArrayIdx + 7 + item->mVertexCt - 8]);
				gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(xl));
				gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(yb));

#endif
				gsPlane[gsActivePlane].mImgTexCoordPool.push_back(0);
				gsPlane[gsActivePlane].mImgTexCoordPool.push_back(0);
				gsPlane[gsActivePlane].mImgTexCoordPool.push_back(0);
				gsPlane[gsActivePlane].mImgTexCoordPool.push_back(0);
				gsPlane[gsActivePlane].mImgColorPool.push_back(0);
				gsPlane[gsActivePlane].mImgColorPool.push_back(0);
				gsPlane[gsActivePlane].mImgColorPool.push_back(0);
				gsPlane[gsActivePlane].mImgColorPool.push_back(0);
				gsPlane[gsActivePlane].mImgColorPool.push_back(0);
				gsPlane[gsActivePlane].mImgColorPool.push_back(0);
				gsPlane[gsActivePlane].mImgColorPool.push_back(0);
				gsPlane[gsActivePlane].mImgColorPool.push_back(0);
				gsPlane[gsActivePlane].mImgVertexCt += 4;
				item->mVertexCt += 4;
				gsPlane[gsActivePlane].mImgTexCt += 4;
				gsPlane[gsActivePlane].mImgColorCt += 8;
				item->mColorCt += 8;
			}
#ifdef CUSTOMAFFINETRANSFORM_SUPPORT
			gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(x0));
			gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(y0));
			gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(data->mX - data->mAffineOriginX + FX_Mul(xl, data->mCos) + FX_Mul(yb, -data->mSin)));
			gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(data->mY - data->mAffineOriginY + FX_Mul(xl, data->mSin) + FX_Mul(yb, data->mCos)));
			gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(data->mX - data->mAffineOriginX + FX_Mul(rt, data->mCos) + FX_Mul(yt, -data->mSin)));
			gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(data->mY - data->mAffineOriginY + FX_Mul(rt, data->mSin) + FX_Mul(yt, data->mCos)));
			gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(data->mX - data->mAffineOriginX + FX_Mul(rt, data->mCos) + FX_Mul(yb, -data->mSin)));
			gsPlane[gsActivePlane].mImgVertexPool.push_back(GX_FX32_TO_GLfx32(data->mY - data->mAffineOriginY + FX_Mul(rt, data->mSin) + FX_Mul(yb, data->mCos)));
#else
			item->mpImgVertexArrayPtr[0 + item->mVertexCt] = item->mpImgVertexArrayPtr[2 + item->mVertexCt] = GX_FX32_TO_GLfx32(xl);
			item->mpImgVertexArrayPtr[3 + item->mVertexCt] = item->mpImgVertexArrayPtr[7 + item->mVertexCt] = GX_FX32_TO_GLfx32(yb);
			item->mpImgVertexArrayPtr[4 + item->mVertexCt] = item->mpImgVertexArrayPtr[6 + item->mVertexCt] = GX_FX32_TO_GLfx32(rt);
			item->mpImgVertexArrayPtr[1 + item->mVertexCt] = item->mpImgVertexArrayPtr[5 + item->mVertexCt] = GX_FX32_TO_GLfx32(yt);
#endif
			if(data->text) // fxFloor(data->mY) == data->mY && fxFloor(data->mX) == data->mX
			{
				//item->mpImgTexArrayPtr[0 + item->mVertexCt] = item->mpImgTexArrayPtr[2 + item->mVertexCt] = GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_X])) / data->mpSrcData->mWidth2n);
				//item->mpImgTexArrayPtr[4 + item->mVertexCt] = item->mpImgTexArrayPtr[6 + item->mVertexCt] = GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_W] + data->mSrcSizeData[SRC_DATA_X])) / data->mpSrcData->mWidth2n);
				//item->mpImgTexArrayPtr[3 + item->mVertexCt] = item->mpImgTexArrayPtr[7 + item->mVertexCt] = GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y])) / data->mpSrcData->mHeight2n);
				//item->mpImgTexArrayPtr[1 + item->mVertexCt] = item->mpImgTexArrayPtr[5 + item->mVertexCt] = GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_Y])) / data->mpSrcData->mHeight2n);
			}
			else
			{
				if(gsGlobalScale < 0.0f)
				{
					yb = FX32_ONE / data->mpSrcData->mHeight2n / 2;
					xl = FX32_ONE / data->mpSrcData->mWidth2n / 2;
				}
				else
				{
					yb = xl = 0.0f;
				}
				gsPlane[gsActivePlane].mImgTexCoordPool.push_back(GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_X])) / data->mpSrcData->mWidth2n + xl));
				gsPlane[gsActivePlane].mImgTexCoordPool.push_back(GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_Y])) / data->mpSrcData->mHeight2n + yb));
				gsPlane[gsActivePlane].mImgTexCoordPool.push_back(GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_X])) / data->mpSrcData->mWidth2n + xl));
				gsPlane[gsActivePlane].mImgTexCoordPool.push_back(GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y])) / data->mpSrcData->mHeight2n - yb));
				gsPlane[gsActivePlane].mImgTexCoordPool.push_back(GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_W] + data->mSrcSizeData[SRC_DATA_X])) / data->mpSrcData->mWidth2n - xl));
				gsPlane[gsActivePlane].mImgTexCoordPool.push_back(GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_Y])) / data->mpSrcData->mHeight2n + yb));
				gsPlane[gsActivePlane].mImgTexCoordPool.push_back(GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_W] + data->mSrcSizeData[SRC_DATA_X])) / data->mpSrcData->mWidth2n - xl));
				gsPlane[gsActivePlane].mImgTexCoordPool.push_back(GX_FX32_TO_GLfx32(FX32((data->mSrcSizeData[SRC_DATA_H] + data->mSrcSizeData[SRC_DATA_Y])) / data->mpSrcData->mHeight2n - yb));
			}
			gsPlane[gsActivePlane].mImgColorPool.push_back(cr);
			gsPlane[gsActivePlane].mImgColorPool.push_back(cg);
			gsPlane[gsActivePlane].mImgColorPool.push_back(cb);
			gsPlane[gsActivePlane].mImgColorPool.push_back(ca);
			gsPlane[gsActivePlane].mImgColorPool.push_back(cr);
			gsPlane[gsActivePlane].mImgColorPool.push_back(cg);
			gsPlane[gsActivePlane].mImgColorPool.push_back(cb);
			gsPlane[gsActivePlane].mImgColorPool.push_back(ca);
			gsPlane[gsActivePlane].mImgColorPool.push_back(cr);
			gsPlane[gsActivePlane].mImgColorPool.push_back(cg);
			gsPlane[gsActivePlane].mImgColorPool.push_back(cb);
			gsPlane[gsActivePlane].mImgColorPool.push_back(ca);
			gsPlane[gsActivePlane].mImgColorPool.push_back(cr);
			gsPlane[gsActivePlane].mImgColorPool.push_back(cg);
			gsPlane[gsActivePlane].mImgColorPool.push_back(cb);
			gsPlane[gsActivePlane].mImgColorPool.push_back(ca);
			gsPlane[gsActivePlane].mImgVertexCt += 8;
			item->mVertexCt += 8;
			gsPlane[gsActivePlane].mImgTexCt += 8;
			gsPlane[gsActivePlane].mImgColorCt += 16;
			item->mColorCt += 16;
		}
	}
}
//----------------------------------------------------------------------------------------------------------------

void glRender_DummyDraw()
{
	glRender_CheckForCleanup(gsActivePlane);
	gsPlane[gsActivePlane].mDraw = 1;
}
//----------------------------------------------------------------------------------------------------------------

BOOL glRender_IsTexturesEnabled(void)
{
	return gsEnableTexure;
}
//----------------------------------------------------------------------------------

void glRender_ForceBindTexture(u32 idx)
{
	mCurrentTextureID = idx;
	glBindTexture(GL_TEXTURE_2D, idx);
}
//----------------------------------------------------------------------------------

void glRender_SetBackdropFn(BackdropFn fn)
{
	_backdropFn = fn;
}
//----------------------------------------------------------------------------------

#endif /*USE_OPENGL_1_RENDER*/
#endif /*NITRO_SDK*/

