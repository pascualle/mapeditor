//---------------------------------------------------------------------------

#ifndef UObjPropertyH
#define UObjPropertyH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <Grids.hpp>
#include <ValEdit.hpp>
#include <Buttons.hpp>
#include "UObjCode.h"
//---------------------------------------------------------------------------

class TCommonResObjManager;
class TPropertiesControl;

class TFGOProperty : public TPrpFunctions
{
__published:
	TPageControl *PageControl;
	TTabSheet *TS_main;
	TTabSheet *TS_script;
	TScrollBox *ScrollBox;
	TValueListEditor *ValueListEditor;
	TTabSheet *TS_Memo;
	TTabSheet *TabComplexObj;
	TLabel *Label5;
	TEdit *Edit_name;
	TGroupBox *GroupBox_pos;
	TLabel *Label1;
	TComboBox *ComboBox_director;
	TBitBtn *Bt_find;
	TImage *Image;
	TComboBox *CB_dir;
	TComboBox *ComboBox_zone;
	TLabel *Label2;
	TLabel *Label3;
	TMemo *Memo;
	TStatusBar *StatusBar1;
	TLabel *Label4;
	TPopupMenu *PopupMenu;
	TMenuItem *N1;
	TMenuItem *N2;
	TBitBtn *BitBtn_find_obj;
	TEdit *CSEd_x;
	TEdit *CSEd_y;
	TGroupBox *GroupBox2;
	TBitBtn *Bt_find_own_node1;
	TBitBtn *Bt_find_own_node2;
	TBitBtn *Bt_find_own_node3;
	TLabel *Label9;
	TLabel *Label10;
	TLabel *Label11;
	TEdit *ED_CplxObjParent1;
	TEdit *ED_CplxObjParent2;
	TEdit *ED_CplxObjParent3;
	TButton *ButtonJoin;
	TImage *Image1;
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall ScrollBoxResize(TObject *Sender);
	void __fastcall ValueListEditorGetPickList(TObject *Sender,
	const UnicodeString KeyName, TStrings *Values);
	void __fastcall Edit_nameChange(TObject *Sender);
	void __fastcall CB_dirChange(TObject *Sender);
	void __fastcall ComboBox_zoneChange(TObject *Sender);
	void __fastcall ImageClick(TObject *Sender);
	void __fastcall Bt_findClick(TObject *Sender);
	void __fastcall Bt_find_parentClick(TObject *Sender);
	void __fastcall N1Click(TObject *Sender);
	void __fastcall ValueListEditorMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
	void __fastcall N2Click(TObject *Sender);
	void __fastcall ValueListEditorKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
	void __fastcall BitBtn_find_objClick(TObject *Sender);
	void __fastcall Edit_nameEnter(TObject *Sender);
	void __fastcall Edit_nameExit(TObject *Sender);
	void __fastcall CSEd_x1Change(TObject *Sender);
	void __fastcall Edit_nameKeyPress(TObject *Sender, char &Key);
	void __fastcall MemoExit(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall MemoEnter(TObject *Sender);
	void __fastcall CSEd_x1Exit(TObject *Sender);
	void __fastcall ValueListEditorValidate(TObject *Sender, int ACol, int ARow,
          const UnicodeString KeyName, const UnicodeString KeyValue);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall ComboBox_zoneExit(TObject *Sender);
	void __fastcall ComboBox_directorExit(TObject *Sender);
	void __fastcall CB_dirExit(TObject *Sender);
	void __fastcall ValueListEditorExit(TObject *Sender);
	void __fastcall ComboBox_directorKeyPress(TObject *Sender, char &Key);
	void __fastcall CSEd_x1Enter(TObject *Sender);
	void __fastcall ED_CplxObjParent1Exit(TObject *Sender);
	void __fastcall ED_CplxObjParent1KeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall ButtonJoinClick(TObject *Sender);
	void __fastcall ButtonDisconnectClick(TObject *Sender);
	void __fastcall StatusBar1DrawPanel(TStatusBar *StatusBar, TStatusPanel *Panel, const TRect &Rect);


private:

		TMapGPerson *t_MapGP;
		TCommonResObjManager	*mpCommonResObjManager;
		TPropertiesControl		*mpPropertiesControl;
		const std::list<PropertyItem>* vars;
		bool        hasLinks;
		bool        inited;
		bool        icon_show_list;
		bool        emulator_mode;
		bool		exitwitherror;
		bool        disableChanges;
		void		*mpEditField;

		UnicodeString  mOldName;
		UnicodeString  mOldVal;
		UnicodeString  NameObj;

		void __fastcall OnPropertiesControlValueValidate(const UnicodeString name, double val);
		void __fastcall FakeEnterActiveControl();

		void __fastcall SaveAllValues(TWinControl *iParent);

		void __fastcall ClearScrollBox();
		void __fastcall ShowStatePreview(UnicodeString name);
		int  __fastcall GetScriptType(int idx);
		void __fastcall EditScript(int mode);

		void __fastcall refreshCplxTab(bool editMode);

		virtual void __fastcall SetNodeU(UnicodeString NodeName) override {}
		virtual void __fastcall SetNodeD(UnicodeString NodeName) override {}
		virtual void __fastcall SetNodeL(UnicodeString NodeName) override {}
		virtual void __fastcall SetNodeR(UnicodeString NodeName) override {}
		virtual void __fastcall SetTrigger(UnicodeString triggerName) override {}
		virtual void __fastcall refreshTriggers() override {}
		virtual void __fastcall SetSizes(int , int ) override {}

public:
		__fastcall TFGOProperty(TComponent* Owner) override;

		virtual void __fastcall Init(TCommonResObjManager *resManager) override;
		virtual void __fastcall Localize() override;
		virtual void __fastcall EmulatorMode(bool mode)  override;
		virtual void __fastcall Reset() override;

		virtual void __fastcall SetRemarks(UnicodeString str) override;
		virtual void __fastcall SetTransform(const TTransformProperties& transform) override;
		virtual void __fastcall SetVariables(const GPersonProperties *prp) override;
		virtual void __fastcall SetScript(UnicodeString KeyName, UnicodeString ScriptName) override;
		virtual void __fastcall SetNode(UnicodeString NodeName) override;
		virtual void __fastcall SetZone(int zone) override;
		virtual void __fastcall SetState(UnicodeString name) override;
		virtual void __fastcall SetVarValue(double val, UnicodeString var_name) override;
		virtual void __fastcall Lock(bool value) override;
		virtual void __fastcall refreshNodes() override;
		virtual void __fastcall refreshCplxList() override;
		virtual void __fastcall refresStates() override;
		virtual void __fastcall refreshVars() override;
		virtual void __fastcall refreshScripts() override;

		bool __fastcall assignMapGobj(TMapGPerson *Gpers);
		TMapGPerson* __fastcall GetAssignedObj(){return t_MapGP;}
};
//---------------------------------------------------------------------------
extern PACKAGE TFGOProperty *FGOProperty;
//---------------------------------------------------------------------------
#endif
