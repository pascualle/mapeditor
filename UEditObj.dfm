object FEditGrObj: TFEditGrObj
  Left = 219
  Top = 154
  Caption = 'mEditGraphicFtagment'
  ClientHeight = 517
  ClientWidth = 656
  Color = clBtnFace
  Constraints.MinHeight = 496
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  Position = poOwnerFormCenter
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  OnResize = FormResize
  TextHeight = 13
  object SplitterLV: TSplitter
    Left = 80
    Top = 0
    Height = 498
    AutoSnap = False
    MinSize = 80
    OnMoved = SplitterLVMoved
    ExplicitLeft = 33
    ExplicitTop = -6
    ExplicitHeight = 420
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 498
    Width = 656
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    ExplicitTop = 499
    ExplicitWidth = 660
  end
  object Panel1: TPanel
    Left = 488
    Top = 0
    Width = 168
    Height = 498
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitLeft = 492
    ExplicitHeight = 499
    DesignSize = (
      168
      498)
    object Label5: TLabel
      Left = 12
      Top = 2
      Width = 57
      Height = 13
      Caption = 'mSourceFile'
    end
    object GroupBox_p: TGroupBox
      Left = 12
      Top = 171
      Width = 145
      Height = 204
      TabOrder = 2
      object Label1: TLabel
        Left = 7
        Top = 21
        Width = 10
        Height = 13
        Caption = 'X:'
      end
      object Label2: TLabel
        Left = 7
        Top = 45
        Width = 10
        Height = 13
        Caption = 'Y:'
      end
      object Label3: TLabel
        Left = 7
        Top = 74
        Width = 14
        Height = 13
        Caption = 'W:'
      end
      object Label4: TLabel
        Left = 7
        Top = 99
        Width = 11
        Height = 13
        Caption = 'H:'
      end
      object Bevel1: TBevel
        Left = 42
        Top = 66
        Width = 92
        Height = 132
      end
    end
    object Bt_accept: TButton
      Left = 12
      Top = 470
      Width = 145
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'mApply'
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 9
      ExplicitTop = 471
    end
    object FileListBox: TFileListBox
      Left = 12
      Top = 15
      Width = 145
      Height = 110
      ItemHeight = 13
      TabOrder = 0
      OnClick = FileListBoxClick
    end
    object GroupBox_cl: TGroupBox
      Left = 12
      Top = 126
      Width = 145
      Height = 47
      Caption = 'mBackdropTransparentColor'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      object ColorBox: TColorBox
        Left = 7
        Top = 16
        Width = 132
        Height = 22
        Selected = clWhite
        Style = [cbStandardColors, cbExtendedColors, cbPrettyNames, cbCustomColors]
        TabOrder = 0
        OnChange = ColorBoxChange
      end
    end
    object BitBt_up: TBitBtn
      Left = 88
      Top = 293
      Width = 24
      Height = 24
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD000000000
        0DDDDD80888888880DDDDD80DDDDDDD80DDDDD80DDDDDDD80DDDDD80DDD00DD8
        0DDDDD80DD800DD80DDDDD80DD800DD80DDDDD80DD800DD80DDDDD8000000000
        0DDDDD8888800888DDDDDDDDDD800DDDDDDDDDDDDD800DDDDDDDDDDD0D800D80
        DDDDDDDD8000000DDDDDDDDDD80000DDDDDDDDDDDD800DDDDDDD}
      Margin = 2
      ParentShowHint = False
      ShowHint = False
      TabOrder = 3
      OnClick = Bt_upClick
    end
    object BitBt_right: TBitBtn
      Left = 112
      Top = 317
      Width = 24
      Height = 24
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDD888888888DDDDDD0000000008DDDDDD08DDDDDD08DD
        08DD08DDDDDD08DDD08D08DDD8880888800808DD00000000000008DD00000000
        000008DDDDDD08DDD00D08DDDDDD08DD80DD0888888808DD0DDD000000000DDD
        DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      Margin = 3
      ParentShowHint = False
      ShowHint = False
      TabOrder = 5
      OnClick = Bt_rightClick
    end
    object BitBt_left: TBitBtn
      Left = 64
      Top = 317
      Width = 24
      Height = 24
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDDDDDDDDDDDDDDDDDDDD888888888DDDDDDD8000000000DD80DD80DDDD
        DD80D80DDD80DDDDDD8080088880888DDD80000000000000DD80000000000000
        DD80D00DDD80DDDDDD80DD08DD80DDDDDD80DDD0DD8088888880DDDDDDD00000
        0000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
      Margin = 0
      ParentShowHint = False
      ShowHint = False
      TabOrder = 4
      OnClick = Bt_leftClick
    end
    object BitBt_down: TBitBtn
      Left = 88
      Top = 341
      Width = 24
      Height = 24
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDD800DDD
        DDDDDDDDD80000DDDDDDDDDD8000000DDDDDDDDD0D800D80DDDDDDDDDD800DDD
        DDDDDDDDDD800DDDDDDDDD8888800888DDDDDD80000000000DDDDD80DD800DD8
        0DDDDD80DD800DD80DDDDD80DD800DD80DDDDD80DDD00DD80DDDDD80DDDDDDD8
        0DDDDD80DDDDDDD80DDDDD80888888880DDDDDD0000000000DDD}
      Margin = 2
      ParentShowHint = False
      ShowHint = False
      TabOrder = 6
      OnClick = Bt_downClick
    end
    object CheckBox_border: TCheckBox
      Left = 12
      Top = 376
      Width = 67
      Height = 17
      Caption = 'mBorder'
      Checked = True
      State = cbChecked
      TabOrder = 8
    end
    object BitBt_findselection: TBitBtn
      Left = 88
      Top = 317
      Width = 24
      Height = 24
      Action = ActionFindSelection
      Caption = 'mFindSelection'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000FC02FC004985
        D40084C6F400C7D1D100FAEFC700B08D8800B5B8BC00FCFCDA0056595B005CB6
        F10072758400868F9200F5D7B000B6C8CE00D3B39E008E787C000BAA60000000
        0000BA58A6000000000091A58A6000000000D91AF8A6000000000291A58BDDDD
        000000291AF8AA88ABD0000291BF5ECEA8FD0000265C47774F8B000006C44777
        748A00000E4C7777775800000E44477777EA00000C44477777EA00000E44C477
        745B000003C7744C4CFD000000E47444C5D000000006EEEEE300}
      Margin = 2
      ParentShowHint = False
      ShowHint = False
      TabOrder = 7
    end
  end
  object CategoryButtons_s: TCategoryButtons
    Left = 0
    Top = 0
    Width = 80
    Height = 498
    Align = alLeft
    ButtonFlow = cbfVertical
    ButtonHeight = 48
    ButtonWidth = 48
    ButtonOptions = [boGradientFill, boShowCaptions, boCaptionOnlyBorder]
    Categories = <>
    HotButtonColor = 14150380
    Images = ImageList
    PopupMenu = PopupMenu
    RegularButtonColor = 15660791
    SelectedButtonColor = 15717318
    ShowHint = True
    TabOrder = 0
    OnAfterDrawButton = CategoryButtons_sAfterDrawButton
    OnDrawIcon = CategoryButtons_sDrawIcon
    OnEnter = CategoryButtons_sEnter
    OnExit = CategoryButtons_sExit
    OnKeyDown = CategoryButtons_sKeyDown
    OnSelectedItemChange = CategoryButtons_sSelectedItemChange
    OnSelectedCategoryChange = CategoryButtons_sSelectedCategoryChange
    ExplicitHeight = 499
  end
  object Panel_main: TPanel
    Left = 83
    Top = 0
    Width = 405
    Height = 498
    Align = alClient
    BevelOuter = bvNone
    FullRepaint = False
    TabOrder = 1
    OnCanResize = Panel_mainCanResize
    ExplicitWidth = 409
    ExplicitHeight = 499
    object Splitter1: TSplitter
      Left = 0
      Top = 145
      Width = 409
      Height = 3
      Cursor = crVSplit
      Align = alTop
      AutoSnap = False
      MinSize = 5
      OnMoved = Splitter1Moved
      ExplicitWidth = 341
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 409
      Height = 145
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        405
        145)
      object ScrollBox1: TScrollBox
        Left = 42
        Top = 0
        Width = 363
        Height = 145
        HorzScrollBar.Tracking = True
        VertScrollBar.Tracking = True
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 1
        OnResize = ScrollBox1Resize
        ExplicitWidth = 367
        object Image1: TPanel
          Left = 3
          Top = 3
          Width = 334
          Height = 121
          ParentCustomHint = False
          BevelOuter = bvNone
          BiDiMode = bdLeftToRight
          Ctl3D = True
          DoubleBuffered = False
          FullRepaint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBiDiMode = False
          ParentBackground = False
          ParentCtl3D = False
          ParentDoubleBuffered = False
          ParentFont = False
          ParentShowHint = False
          ShowCaption = False
          ShowHint = False
          TabOrder = 0
        end
      end
      object TrackBar1: TTrackBar
        Tag = 1
        Left = 1
        Top = 6
        Width = 35
        Height = 80
        Hint = 'mScale'
        Max = 5
        Orientation = trVertical
        ParentShowHint = False
        PageSize = 1
        ShowHint = True
        ShowSelRange = False
        TabOrder = 0
        TickMarks = tmBoth
        OnChange = TrackBar1Change
      end
    end
    object Panel_1: TPanel
      Left = 0
      Top = 148
      Width = 405
      Height = 350
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitWidth = 409
      ExplicitHeight = 351
      object TrackBar2: TTrackBar
        Tag = 2
        Left = 1
        Top = 6
        Width = 35
        Height = 80
        Hint = 'mScale'
        Max = 5
        Orientation = trVertical
        ParentShowHint = False
        PageSize = 1
        ShowHint = True
        ShowSelRange = False
        TabOrder = 0
        TickMarks = tmBoth
        OnChange = TrackBar1Change
      end
      object PaintBox1: TPanel
        Left = 42
        Top = 4
        Width = 351
        Height = 328
        ParentCustomHint = False
        BevelOuter = bvNone
        BiDiMode = bdLeftToRight
        BorderStyle = bsSingle
        Ctl3D = True
        DoubleBuffered = False
        FullRepaint = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBiDiMode = False
        ParentBackground = False
        ParentCtl3D = False
        ParentDoubleBuffered = False
        ParentFont = False
        ParentShowHint = False
        ShowCaption = False
        ShowHint = False
        TabOrder = 1
        OnMouseDown = Sh_tMouseDown
        OnMouseEnter = PaintBox1MouseEnter
        OnMouseLeave = PaintBox1MouseLeave
        OnMouseMove = PaintBox1MouseMove
        OnMouseUp = Sh_tMouseUp
        object FakeControl: TEdit
          Left = 13
          Top = 99
          Width = 20
          Height = 21
          TabOrder = 0
          OnEnter = FakeControlEnter
          OnKeyDown = FakeControlKeyDown
          OnKeyPress = FakeControlKeyPress
        end
      end
      object HorzScrollBar: TScrollBar
        Left = 44
        Top = 331
        Width = 349
        Height = 17
        Ctl3D = False
        Max = 1
        PageSize = 0
        ParentCtl3D = False
        TabOrder = 2
        TabStop = False
        OnScroll = HorzScrollBarScroll
      end
      object VertScrollBar: TScrollBar
        Left = 393
        Top = 3
        Width = 17
        Height = 329
        Ctl3D = False
        Kind = sbVertical
        Max = 1
        PageSize = 0
        ParentCtl3D = False
        TabOrder = 3
        TabStop = False
        OnScroll = HorzScrollBarScroll
      end
    end
  end
  object ActionList1: TActionList
    Left = 208
    Top = 376
    object ActSelectAll: TAction
      Caption = 'mSelectAll'
      Hint = 'mSelectAll'
      ShortCut = 16449
      OnExecute = ActSelectAllExecute
    end
    object ActDeselectAll: TAction
      Caption = 'mDeselect'
      Hint = 'mDeselect'
      ShortCut = 16452
      OnExecute = ActDeselectAllExecute
    end
    object ActCopy: TAction
      Caption = 'mCopy'
      Hint = 'mCopy'
      ShortCut = 16451
      OnExecute = ActCopyExecute
    end
    object ActPaste: TAction
      Caption = 'mPaste'
      Hint = 'mPaste'
      ShortCut = 16470
      OnExecute = ActPasteExecute
    end
    object ActShiftRight: TAction
      Caption = 'mShiftRight'
      Hint = 'mShiftRight'
      OnExecute = ActShiftRightExecute
    end
    object ActShiftLeft: TAction
      Caption = 'mShiftLeft'
      Hint = 'mShiftLeft'
      OnExecute = ActShiftLeftExecute
    end
    object ActShiftUp: TAction
      Caption = 'mShiftUp'
      Hint = 'mShiftUp'
      OnExecute = ActShiftUpExecute
    end
    object ActShiftDown: TAction
      Caption = 'mShiftDown'
      Hint = 'mShiftDown'
      OnExecute = ActShiftDownExecute
    end
    object ActAdd: TAction
      Category = 'obj'
      Caption = 'mAdd'
      Hint = 'mAdd'
      ShortCut = 45
      OnExecute = ActAddExecute
    end
    object ActDel: TAction
      Category = 'obj'
      Caption = 'mDoDelete'
      Hint = 'mDoDelete'
      ShortCut = 46
      OnExecute = ActDelExecute
    end
    object ActClear: TAction
      Category = 'obj'
      Caption = 'mClear'
      Hint = 'mClear'
      OnExecute = ActClearExecute
    end
    object ActGroupCreate: TAction
      Category = 'obj'
      Caption = 'mCreateGroup'
      Hint = 'mCreateGroup'
      OnExecute = ActGroupCreateExecute
    end
    object ActGroupDelete: TAction
      Category = 'obj'
      Caption = 'mDeleteGroup'
      Hint = 'mDeleteGroup'
      OnExecute = ActGroupDeleteExecute
    end
    object ActBGObjShiftLeft: TAction
      Category = 'bg'
      Caption = 'mShiftLayerLeft'
      Hint = 'mShiftLayerLeft'
      OnExecute = ActBGObjShiftLeftExecute
    end
    object ActBGObjShiftRight: TAction
      Category = 'bg'
      Caption = 'mShiftLayerRight'
      Hint = 'mShiftLayerRight'
      OnExecute = ActBGObjShiftRightExecute
    end
    object ActionFindSelection: TAction
      Caption = 'mFindSelection'
      Hint = 'mFindSelection'
      ShortCut = 16454
      OnExecute = ActionFindSelectionExecute
    end
  end
  object MainMenu1: TMainMenu
    OnChange = MainMenu1Change
    Left = 144
    Top = 376
    object N1: TMenuItem
      Caption = 'mFile'
      object N3: TMenuItem
        Caption = 'mExit'
        OnClick = N3Click
      end
    end
    object N2: TMenuItem
      Caption = 'mEdit'
      object N4: TMenuItem
        Action = ActCopy
      end
      object N5: TMenuItem
        Action = ActPaste
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object N7: TMenuItem
        Action = ActSelectAll
      end
      object N8: TMenuItem
        Action = ActDeselectAll
      end
      object N26: TMenuItem
        Action = ActionFindSelection
      end
      object N9: TMenuItem
        Caption = '-'
      end
      object N10: TMenuItem
        Action = ActShiftLeft
      end
      object N11: TMenuItem
        Action = ActShiftRight
      end
      object N12: TMenuItem
        Action = ActShiftUp
      end
      object N13: TMenuItem
        Action = ActShiftDown
      end
    end
    object N14: TMenuItem
      Caption = 'mObjects'
      object N15: TMenuItem
        Action = ActAdd
      end
      object N16: TMenuItem
        Action = ActDel
      end
      object N17: TMenuItem
        Action = ActClear
      end
      object N18: TMenuItem
        Caption = '-'
      end
      object N20: TMenuItem
        Action = ActBGObjShiftLeft
      end
      object N19: TMenuItem
        Action = ActBGObjShiftRight
      end
      object N24: TMenuItem
        Action = ActGroupCreate
      end
      object N25: TMenuItem
        Action = ActGroupDelete
      end
      object NEGOMMMovetoSeparator: TMenuItem
        Caption = '-'
      end
      object NEGOMMMoveto: TMenuItem
        Caption = 'mMoveToGroup'
      end
    end
  end
  object ImageList: TImageList
    ColorDepth = cd32Bit
    Height = 48
    Masked = False
    Width = 48
    Left = 24
    Top = 24
  end
  object PopupMenu: TPopupMenu
    Images = Form_m.ImageList1
    OnPopup = PopupMenuPopup
    Left = 272
    Top = 376
    object NEdit: TMenuItem
      Action = ActAdd
    end
    object NDel: TMenuItem
      Action = ActDel
    end
    object N21: TMenuItem
      Action = ActClear
    end
    object MenuItem2: TMenuItem
      Caption = '-'
    end
    object N22: TMenuItem
      Action = ActGroupCreate
    end
    object N23: TMenuItem
      Action = ActGroupDelete
    end
    object ActBGObjShiftLeft1: TMenuItem
      Action = ActBGObjShiftLeft
    end
    object ActBGObjShiftRight1: TMenuItem
      Action = ActBGObjShiftRight
    end
    object NEGOMovetoSeparator: TMenuItem
      Caption = '-'
    end
    object NEGOMoveto: TMenuItem
      Caption = 'mMoveToGroup'
    end
  end
end
