//---------------------------------------------------------------------------

#ifndef UObjParentH
#define UObjParentH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <CategoryButtons.hpp>
#include <Grids.hpp>
#include <AppEvnts.hpp>
#include <ImgList.hpp>
#include <ActnList.hpp>
#include <Menus.hpp>
#include <Tabs.hpp>
#include <ValEdit.hpp>
#include <ToolWin.hpp>
#include "UTypes.h"
#include <System.Actions.hpp>
#include <Vcl.Dialogs.hpp>
#include <System.ImageList.hpp>
#include <vector>
//---------------------------------------------------------------------------

class TCommonResObjManager;
class GPerson;

class TFObjParent : public TForm, TRenderGLForm
{
	__published:
	TPanel *PanelAnimation;
	TCheckBox *CBAutorepeat;
	TBitBtn *BtPlay;
	TBitBtn *BtFrPrev;
	TBitBtn *BtBack;
	TBitBtn *BtFrNext;
	TBitBtn *BtStop;
	TComboBox *ComboBoxScale;
	TComboBox *CB_AniType;
	TStatusBar *StatusBar;
	TSplitter *Splitter1;
	TDrawGrid *FrameGrid;
	TImageList *ImageList;
	TActionList *ActionList1;
	TAction *ActSelectAll;
	TAction *ActDeselectAll;
	TAction *ActObjCopy;
	TAction *ActObjPaste;
	TAction *ActEdit;
	TPopupMenu *PopupMenu;
	TMenuItem *NEdit;
	TTabSet *TabSet;
	TPanel *PanelMain;
	TGroupBox *GroupBox_name;
	TEdit *Edit_name;
	TGroupBox *GroupBox_Gr;
	TCheckBox *CheckBox_decoration;
	TButton *Bt_propdef;
	TCheckBox *CheckBox_graphics;
	TCheckBox *CheckBox_res;
	TPanel *PanelLeft;
	TCategoryPanelGroup *CategoryPanelGroup;
	TCategoryPanel *CP2;
	TCategoryPanel *CP1;
	TCategoryButtons *CategoryButtons_s;
	TTabSet *TabSetGraphicsMode;
	TComboBox *CBDefState;
	TImage *Image;
	TLabel *Label4;
	TPopupMenu *PopupMenuGrid;
	TAction *ActAddEmptyFrame;
	TAction *ActDelFrame;
	TAction *ActAddCopyFrame;
	TAction *ActClearFrame;
	TMenuItem *N1;
	TMenuItem *N2;
	TMenuItem *N3;
	TMenuItem *ActDelFrame1;
	TMenuItem *N4;
	TMenuItem *N5;
	TEdit *FakeControl;
	TCategoryPanel *CP3;
	TPopupMenu *PopupMenuObj;
	TAction *ActObjAdd;
	TAction *ActObjDelete;
	TAction *ActObjEdit;
	TMenuItem *N6;
	TMenuItem *N7;
	TMenuItem *N8;
	TMenuItem *N9;
	TAction *ActObjLevelUp;
	TAction *ActObjLevelDown;
	TAction *ActObjFindParent;
	TMenuItem *N10;
	TToolBar *ToolBar;
	TToolButton *ToolButton1;
	TToolButton *ToolButton2;
	TToolButton *ToolButton3;
	TToolButton *ToolButton4;
	TToolButton *ToolButton5;
	TToolButton *ToolButton6;
	TToolButton *ToolButton7;
	TToolButton *ToolButton8;
	TToolButton *ToolButton9;
	TToolButton *ToolButton10;
	TToolButton *ToolButton11;
	TToolButton *ToolButton12;
	TMenuItem *N11;
	TMenuItem *N12;
	TMenuItem *N13;
	TMenuItem *N14;
	TMenuItem *N15;
	TMenuItem *N16;
	TAction *ActCreateIcon;
	TMenuItem *N17;
	TMenuItem *N18;
	TToolButton *ToolButton13;
	TToolButton *ToolButton14;
	TImage *Image1;
	TComboBox *CBViewMode;
	TAction *ActTabNext;
	TMenuItem *N19;
	TMenuItem *N20;
	TCheckBox *RBCollideRect;
	TCheckBox *RBViewRect;
	TCheckBox *RBNode1;
	TValueListEditor *VLECollide;
	TValueListEditor *VLEView;
	TValueListEditor *VLENode1;
	TValueListEditor *VLEObj;
	TStaticText *StaticText2;
	TStaticText *LabelCount;
	TPanel *PanelBt;
	TButton *Button1;
	TButton *Button2;
	TPanel *PaintBoxGL;
	TStaticText *ErrorGLText;
	TGroupBox *GroupBox1;
	TStaticText *StaticText5;
	TStaticText *LabelFrafments;
	TStaticText *StaticText3;
	TStaticText *LabelCurrent;
	TEdit *Edit_duration;
	TStaticText *StaticText1;
	TStaticText *StaticText7;
	TComboBox *ComboBox_Event;
	TPanel *Panel_strm;
	TTabSet *TabSetAniMode;
	TPanel *Panel2;
	TButton *Button_AutoCreateStrmAnim;
	TPanel *Panel1;
	TPanel *Panel0;
	TListView *FoldersList;
	TLabel *Label1;
	TLabel *Label5;
	TListView *FilesList;
	TToolButton *ToolButton16;
	TToolButton *ToolButton17;
	TAction *ActShowRulers;
	TToolButton *ToolButton19;
	TAction *ActAniAndLogic;
	TEdit *EditFilter;
	TStaticText *StaticText4;
	TToolButton *ToolButton15;
	TAction *ActBGColor;
	TColorDialog *ColorDialog;
	TBitBtn *BtAddNewAnimationType;
	TAction *ActCreateNewAnimType;
	TLabel *LabelGrOptHint;
	TAction *ActImportFromXML;
	TMenuItem *N21;
	TMenuItem *mActImportFromXML1;
	TPopupMenu *PopupMenuXYWH;
	TAction *ActionToCenter;
	TMenuItem *mAlignToCenter1;
	TAction *ActionToDefaultSizes;
	TMenuItem *N22;
	TMenuItem *mDefaultSizes1;
	TMenuItem *N23;
	TMenuItem *mMoreOptions1;
	TMenuItem *mAlignToCenter2;
	TMenuItem *N24;
	TMenuItem *mDefaultSizes2;

	void __fastcall FrameGridSelectCell(TObject *Sender, System::LongInt ACol, System::LongInt ARow, bool &CanSelect);
	void __fastcall BtStopClick(TObject *Sender);
	void __fastcall BtPlayClick(TObject *Sender);
	void __fastcall BtBackClick(TObject *Sender);
	void __fastcall BtFrNextClick(TObject *Sender);
	void __fastcall BtFrPrevClick(TObject *Sender);
	void __fastcall CB_AniTypeChange(TObject *Sender);
	void __fastcall CategoryButtons_sDrawIcon(TObject *Sender, const TButtonItem *Button, TCanvas *Canvas, TRect &Rect, TButtonDrawState State, int &TextOffset);
	void __fastcall CategoryButtons_sEnter(TObject *Sender);
	void __fastcall CategoryButtons_sExit(TObject *Sender);
	void __fastcall ActSelectAllExecute(TObject *Sender);
	void __fastcall ActDeselectAllExecute(TObject *Sender);
	void __fastcall ActObjCopyExecute(TObject *Sender);
	void __fastcall ActEditExecute(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall Edit_nameChange(TObject *Sender);
	void __fastcall CheckBox_decorationClick(TObject *Sender);
	void __fastcall CheckBox_graphicsClick(TObject *Sender);
	void __fastcall TabSetClick(TObject *Sender);
	void __fastcall TabSetGraphicsModeClick(TObject *Sender);
	void __fastcall Bt_propdefClick(TObject *Sender);
	void __fastcall FrameGridDrawCell(TObject *Sender, System::LongInt ACol, System::LongInt ARow, TRect &Rect, TGridDrawState State);
	void __fastcall FormMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall FormMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall FormMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall PopupMenuGridPopup(TObject *Sender);
	void __fastcall ActAddEmptyFrameExecute(TObject *Sender);
	void __fastcall ActDelFrameExecute(TObject *Sender);
	void __fastcall ActAddCopyFrameExecute(TObject *Sender);
	void __fastcall ActClearFrameExecute(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall ActObjAddExecute(TObject *Sender);
	void __fastcall ActObjDeleteExecute(TObject *Sender);
	void __fastcall ActObjEditExecute(TObject *Sender);
	void __fastcall RBCollideRectClick(TObject *Sender);
	void __fastcall VLECollideDrawCell(TObject *Sender, System::LongInt ACol, System::LongInt ARow, TRect &Rect,
          TGridDrawState State);
	void __fastcall ActObjLevelUpExecute(TObject *Sender);
	void __fastcall ActObjLevelDownExecute(TObject *Sender);
	void __fastcall N6Click(TObject *Sender);
	void __fastcall PopupMenuObjPopup(TObject *Sender);
	void __fastcall CategoryButtons_sSelectedItemChange(TObject *Sender, const TButtonItem *Button);
	void __fastcall CategoryButtons_sSelectedCategoryChange(TObject *Sender, const TButtonCategory *Category);
	void __fastcall ActObjFindParentExecute(TObject *Sender);
	void __fastcall ActCreateIconExecute(TObject *Sender);
	void __fastcall CBViewModeChange(TObject *Sender);
	void __fastcall ActTabNextExecute(TObject *Sender);
	void __fastcall VLECollideValidate(TObject *Sender, System::LongInt ACol, System::LongInt ARow, const UnicodeString KeyName,
          const UnicodeString KeyValue);
	void __fastcall VLECollideKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall Edit_durationKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall Edit_durationExit(TObject *Sender);
	void __fastcall Splitter1CanResize(TObject *Sender, int &NewSize, bool &Accept);
	void __fastcall FrameGridMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall FrameGridMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall FrameGridMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall FrameGridKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall ActObjPasteExecute(TObject *Sender);
	void __fastcall CB_AniTypeEnter(TObject *Sender);
	void __fastcall CB_AniTypeKeyPress(TObject *Sender, System::WideChar &Key);
	void __fastcall ComboBox_EventChange(TObject *Sender);
	void __fastcall CB_AniTypeExit(TObject *Sender);
	void __fastcall FoldersListResize(TObject *Sender);
	void __fastcall FoldersListClick(TObject *Sender);
	void __fastcall Button_AutoCreateStrmAnimClick(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall ActAniAndLogicExecute(TObject *Sender);
	void __fastcall ActShowRulersExecute(TObject *Sender);
	void __fastcall ToolButton15Click(TObject *Sender);
	void __fastcall CheckBox_resClick(TObject *Sender);
	void __fastcall EditFilterChange(TObject *Sender);
	void __fastcall ActImportFromXMLExecute(TObject *Sender);
	void __fastcall PopupMenuXYWHPopup(TObject *Sender);
	void __fastcall ActionToCenterExecute(TObject *Sender);
	void __fastcall ActionToDefaultSizesExecute(TObject *Sender);
	void __fastcall ComboBoxScaleChange(TObject *Sender);

  private:

	TCommonResObjManager *mpTempResObjManager;
	GPerson *mpGameObj;
	GPerson *mpTempGameObj;
	TList *mpResList;
	TAnimation *mpAnimation;
	std::list<TAnimationFrameData> mBFList;
	std::list<TAnimationFrameData*> mNRList;
	std::list<TCategoryPanel*> mDynPanels;
	std::list<TCheckBox*> mDynCheckboxes;
	std::vector<TValueListEditor*> mVLENodes;
	TList *mpDragObjList;
	TList *mpSaveDragObjList;
	TStringList *mpTempFilterList;
	BMPImage *mpCurrentStreamBmp;
	TAnimationFrameData *mpLastDragObj;
	TLMBtn mLMBtn;
	TColor mCanvasColor;
	__int32 mAnimationFrameIdx;
	__int32 mPopupX;
	__int32 mPopupY;
	__int32 mAnimatonDirection;
	__int32 mRenderDelay;
	bool mFromPopup;
	bool mDrawAnimations;
	bool mDrawLogics;
	bool mDontCareSelection;
	bool mTraceAnimation;
	bool mDisableGridControls;
	bool mCheckBoxesProcessing;
	bool mTimerEnabled;
	__int32 mTimerDuration;
	TAnimationFrameData mLogicDrawObjects[UOP_MAX_LOGIC_DRAW_OBJ];
	bool mBeginFrameGridDrag;
	bool mFrameGridDrag;
	TPoint mFrameGridPos;
	TWndMethod mOldPaintBox1WndProc;
	UnicodeString mOldCBValue;
	UnicodeString mLastStreamFolder;
	UnicodeString mLastStreamFile;
	UnicodeString mCurrentStreamFilename;
	double mScale;
	bool mJoinNodeInited;
	__int32 mJoinNodesCount;

	enum DragType
	{
		sNone, sTop, sLeft, sRight, sBottom
	};

	struct
	{
		bool mSet;
		__int32 mX0;
		__int32 mY0;
		TRect mRect;
		DragType dragType;
		bool mProrotional;
	}mCopyRect;

	virtual void __fastcall InitRenderGL();
	virtual void __fastcall ReleaseRenderGL();

	void __fastcall ClearResList(TList *lst);

	void __fastcall DrawFrame();
	void __fastcall GetRenderRect(TRect& r);
	void __fastcall DrawRulers(TCanvas *c, const TRect &r);
	void __fastcall DrawObjects(TCanvas *c, const TRect &r);
	void __fastcall DrawLogics(TCanvas *c, const TRect &r);
	void __fastcall DrawFrameObject(TCanvas *c, const TRect &r, unsigned char transparency, const TAnimationFrameData *fdata);
	void __fastcall DrawSelection(TCanvas *c, const TRect &r);

	void __fastcall CreateListView();
	void __fastcall CreateIcon();
	void* __fastcall FindReference(TAnimationFrameData *pt);
	void __fastcall CheckToSwitchDecorationAnimation();
	void __fastcall SetAniViewMode();
	void __fastcall SetDrawMode();

	void __fastcall CreateAnimationFrames(__int32 idx);
	void __fastcall OnSelectFrame(__int32 idx);
	void __fastcall DoSelectFrame(__int32 idx);
	void __fastcall SaveFrameData();
	void __fastcall SetLogicValuesToGrid(TAnimationFrameData *obj);
	void __fastcall SetObjValuesToGrid();
	void __fastcall SaveObjValuesFromGrid();
	bool __fastcall CheckAbilityToPlaceObj(__int32 &x, __int32 &y);
	void __fastcall CheckObjEditPosibility(bool checkMouse);

	bool __fastcall ProcessSelectionWithMouse(TAnimationFrameData *pt, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall ClearCopySelection();
	void __fastcall CheckCursorMode(__int32 X, __int32 Y, double scale);

	TAnimationFrameData *__fastcall FindObjectPointers(TList *List, TRect &SelectionRect);
	void __fastcall DragObjListClear();
	void __fastcall DragObjListAdd(TAnimationFrameData *pt);
	TAnimationFrameData *__fastcall DragObjListAddMulti(TRect &SelectionRect, bool UnselectDublicates);
	bool __fastcall DragObjListContains(TAnimationFrameData *pt);
	void __fastcall RemoveDragObjFromList(TAnimationFrameData *pt);
	int __fastcall DragObjListCount();
	void __fastcall DragObjListSave();
	void __fastcall DragObjListRestore();
	TAnimationFrameData* __fastcall GetDragObjFromList(__int32 idx);

	void __fastcall MoveSelectedObjects(double dx, double dy);
	void __fastcall ResizeSelectedObject(__int32 dx, __int32 dy, bool proportional);
	void __fastcall DeleteSelectedObjects();

	void __fastcall ProcessAnimation(unsigned int ms);

	void __fastcall CustomPaintBox1WndMethod(Winapi::Messages::TMessage &Message);

	void __fastcall CreateStreamFolderAndFileList(UnicodeString activeFolder, UnicodeString activeFile);
	void __fastcall CreateStreamFileList(UnicodeString activeFolder, UnicodeString activeFile);

	void __fastcall CurrentFrameInfoEnabled(bool val);
	void __fastcall InitJoinNodes();

  public:

	void __fastcall SetGameObj(GPerson *obj);
	void __fastcall ApplyChangesToGameObj();
	virtual void __fastcall DoTick(unsigned int ms);
	void __fastcall GetWndParams(TEdPrObjWndParams *params);
	void __fastcall SetWndParams(const TEdPrObjWndParams *params);
	void __fastcall ReleaseResources();
	__fastcall TFObjParent(TComponent *Owner);
};

//---------------------------------------------------------------------------
extern PACKAGE TFObjParent *FObjParent;
//---------------------------------------------------------------------------
#endif
