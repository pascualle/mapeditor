//---------------------------------------------------------------------------

#ifndef ULoadH
#define ULoadH

#include "USaveLoadStreamData.h"

//---------------------------------------------------------------------------


void ReadAnsiStringData(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, UnicodeString fieldName, UnicodeString& str, int ver);

void ReadWideStringData(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h,
			const TSaveLoadStreamData::TSLFieldType& ftype, UnicodeString fieldName, UnicodeString& str);

#endif

