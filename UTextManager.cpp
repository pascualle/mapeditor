#include <vcl.h>
#pragma hdrstop

#include "UTextManager.h"
#include "UCreateMessages.h"
#include "MainUnit.h"
#include "UObjManager.h"
#include "UObjCode.h"
#include "texts.h"
#include "ULocalization.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
TFormTextManager *FormTextManager;

//---------------------------------------------------------------------------

const _TCHAR gsTxtMngColumnAlias[] = _T("mTxtMngColumnAlias");
const _TCHAR gsTxtMngColumnText[] = _T("mTxtMngColumnText");

//---------------------------------------------------------------------------

__fastcall TFormTextManager::TFormTextManager(TComponent* Owner)
	: TForm(Owner)
{
	mpNames = new TStringList();
	mpFilteredNames = new TStringList();
	mLang = _T("");

	Localization::Localize(ActionList1);
	Localization::Localize(this);
	InitGridColumns();

	Form_m->mpTexts->GetLanguages(mpNames);
	TabSet->Tabs->Clear();
	for(int i = 0; i < mpNames->Count; i++)
	{
		if(!mpNames->Strings[i].IsEmpty())
		{
			TabSet->Tabs->Add(_T("  ") + mpNames->Strings[i] + _T("  "));
		}
	}
	mpNames->Clear();
	Form_m->mpTexts->GetNames(mpNames);
	TabSet->TabIndex = 0;
}
//---------------------------------------------------------------------------

void __fastcall TFormTextManager::InitGridColumns()
{
	TntStringGrid->RowCount = 2;
	TntStringGrid->Cells[0][0] = Localization::Text(gsTxtMngColumnAlias);
	TntStringGrid->Cells[1][0] = Localization::Text(gsTxtMngColumnText);
	TntStringGrid->Cells[0][1] = _T("");
	TntStringGrid->Cells[1][1] = _T("");
}
//---------------------------------------------------------------------------

void __fastcall TFormTextManager::FormDestroy(TObject */*Sender*/)
{
	delete mpNames;
	delete mpFilteredNames;
}
//---------------------------------------------------------------------------

void __fastcall TFormTextManager::ShowButtons(bool buttons)
{
	PanelButtons->Visible = buttons;
}
//---------------------------------------------------------------------------

void __fastcall TFormTextManager::ActEditExecute(TObject *)
{
	if(Form_m->mpTexts->GetCount() == 0)
	{
		return;
	}
	Application->CreateForm(__classid(TFMessages), &FMessages);
	FMessages->Caption = ActEdit->Caption;
	UnicodeString sel_name;
	if(EditFilter->Text.IsEmpty())
	{
		sel_name = mpNames->Strings[TntStringGrid->Row - 1];
	}
	else
	{
		sel_name = mpFilteredNames->Strings[TntStringGrid->Row - 1];
  	}
	const TUnicodeText *txt = Form_m->mpTexts->GetText(sel_name, mLang);
	if(txt == NULL)
	{
    	return;
	}
	if(FMessages->Init(const_cast<TUnicodeText*>(txt)))
	{
		FMessages->CB_List->Text = sel_name;
		for(;;)
		{
			FMessages->ShowModal();
			UnicodeString name = FMessages->CB_List->Text;
			const TUnicodeText *otxt = Form_m->mpTexts->GetText(sel_name, mLang);
			if(otxt == NULL || otxt == txt)
			{
				Form_m->mpTexts->ChangeName(sel_name, name);
				Form_m->mpTexts->UpdateText(*otxt, name, mLang);
				mpNames->Clear();
				Form_m->mpTexts->GetNames(mpNames);
				RefreshTable(name);
				break;
			}
			else
			{
				Application->MessageBox(Localization::Text(_T("mTextNameExist")).c_str(),
								Localization::Text(_T("mMessageBoxWarning")).c_str(),
								MB_OK|MB_ICONWARNING);
			}
		}
	}
	FMessages->Free();
}
//---------------------------------------------------------------------------

void __fastcall TFormTextManager::ActNewMessageExecute(TObject */*Sender*/)
{
	if(Form_m->mpTexts->GetCount() >= MAX_TXT_STRINGS)
	{
		Application->MessageBox(Localization::Text(_T("mMaximumTextsExceeded")).c_str(),
                            Localization::Text(_T("mError")).c_str(),
                            MB_OK | MB_ICONERROR);
		return;
	}

	Application->CreateForm(__classid(TFMessages), &FMessages);
	FMessages->Caption = ActNewMessage->Caption;
	TUnicodeText txt;
	if(FMessages->Init(&txt))
	{
		UnicodeString name;
		int i = 1;
		for(;;)
		{
			name = _T("TXT_STRING") + IntToStr(Form_m->mpTexts->GetCount() + i);
			if(Form_m->mpTexts->GetText(name, mLang) == NULL)
			{
				break;
			}
			i++;
		}
		FMessages->CB_List->Text = name;

		//first font
		if(FMessages->CB_Font->Items->Count > 0)
		{
			FMessages->CB_Font->ItemIndex = 0;
			FMessages->AssignFont(FMessages->CB_Font->Items->Strings[0]);
		}

		for(;;)
		{
			FMessages->ShowModal();
			name = FMessages->CB_List->Text;
      if(Form_m->mpTexts->GetText(name, mLang) == NULL)
      {
        Form_m->mpTexts->AddText(txt, name, mLang);
        mpNames->Add(name);
        RefreshTable(name);
        RefreshBar();
        break;
      }
      else
      {
        Application->MessageBox(Localization::Text(_T("mTextNameExist")).c_str(),
                                Localization::Text(_T("mMessageBoxWarning")).c_str(),
                                MB_OK|MB_ICONWARNING);
      }
		}
  }
	FMessages->Free();
}
//---------------------------------------------------------------------------

void __fastcall TFormTextManager::ActDelMessageExecute(TObject *)
{
  if(Form_m->mpTexts->GetCount() == 0)
  {
    return;
  }
	UnicodeString ws;
	if(EditFilter->Text.IsEmpty())
	{
		ws = mpNames->Strings[TntStringGrid->Row - 1];
	}
	else
	{
		ws = mpFilteredNames->Strings[TntStringGrid->Row - 1];
	}
	if(Application->MessageBox(Localization::TextWithVar(_T("mAskDeleteTextItem_s"), ws).c_str(),
                              Localization::Text(_T("mDelete")).c_str(),
                              MB_YESNO|MB_ICONQUESTION) == mrYes)
	{
		Form_m->mpTexts->DeleteText(ws);
		mpNames->Delete(mpNames->IndexOf(ws));
		RefreshTable(_T(""));
		RefreshBar();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormTextManager::RefreshTable(UnicodeString selected)
{
	if(Form_m->mpTexts == NULL)
	{
		return;
	}

	if(Form_m->mpTexts->GetCount() == 0)
	{
		InitGridColumns();
		return;
	}

	if(EditFilter->Text.IsEmpty())
	{
		TntStringGrid->RowCount = mpNames->Count + 1;
		for(int i = 0; i < mpNames->Count; i++)
		{
			UnicodeString name = mpNames->Strings[i];
			AddTextRow(i, name);
			if(!selected.IsEmpty() && selected.Compare(name) == 0)
			{
				TntStringGrid->Row = i + 1;
			}
		}
	}
	else
	{
		int i;
		mpFilteredNames->Clear();
		for(i = 0; i < mpNames->Count; i++)
		{
			UnicodeString name = mpNames->Strings[i];
			if(name.Pos(EditFilter->Text.UpperCase()) != 0)
			{
				mpFilteredNames->Add(name);
			}
		}
		TntStringGrid->RowCount = mpFilteredNames->Count + 1;
		for(i = 0; i < mpFilteredNames->Count; i++)
		{
			UnicodeString name = mpFilteredNames->Strings[i];
			AddTextRow(i, name);
			if(!selected.IsEmpty() && selected.Compare(name) == 0)
			{
				TntStringGrid->Row = i + 1;
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFormTextManager::AddTextRow(int idx, UnicodeString textName)
{
	const TUnicodeText *t;
	UnicodeString str;
	TntStringGrid->Cells[0][idx + 1] = textName;
	t = Form_m->mpTexts->GetText(textName, mLang);
	if(t->GetTextData() != NULL)
	{
		for(int j = 0; j < t->GetTextData()->mStringsCount; j++)
		{
			wchar_t* pString = t->GetTextData()->mppStrings[j];
			if(pString != NULL)
			{
				str += UnicodeString(pString) + _T("�");
      }
		}
	}
	TntStringGrid->Cells[1][idx + 1] = str;
}
//---------------------------------------------------------------------------

void __fastcall TFormTextManager::RefreshBar()
{
	StatusBar->SimpleText = Localization::Text(_T("mCount")) + _T(": ") + IntToStr(Form_m->mpTexts->GetCount());
}
//---------------------------------------------------------------------------

void __fastcall TFormTextManager::TntStringGridDblClick(TObject *)
{
	ActEdit->Execute();
}
//---------------------------------------------------------------------------

void __fastcall TFormTextManager::TntStringGridKeyPress(TObject *, wchar_t &Key)
{
  if(Key == VK_RETURN)
  {
    ActEdit->Execute();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFormTextManager::TabSetClick(TObject */*Sender*/)
{
	mLang = TabSet->Tabs->Strings[TabSet->TabIndex].Trim();
	RefreshTable(_T(""));
	RefreshBar();
}
//---------------------------------------------------------------------------

UnicodeString __fastcall TFormTextManager::GetCurrentName()
{
	if(TntStringGrid->Row - 1 >= 0)
	{
		return TntStringGrid->Cells[0][TntStringGrid->Row];
	}
	return _T("");
}
//---------------------------------------------------------------------------

bool __fastcall TFormTextManager::Find(UnicodeString name)
{
	int pos;
	if(EditFilter->Text.IsEmpty())
	{
		pos = mpNames->IndexOf(name);
	}
	else
	{
		pos = mpFilteredNames->IndexOf(name);
	}
	if(pos >= 0)
	{
		TntStringGrid->Row = pos + 1;
		return true;
	}
	return false;
}
//---------------------------------------------------------------------------

void __fastcall TFormTextManager::EditFilterChange(TObject *)
{
	const UnicodeString cn = GetCurrentName();
	RefreshTable(cn);
}
//---------------------------------------------------------------------------

