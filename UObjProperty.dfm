object FGOProperty: TFGOProperty
  Left = 293
  Top = 154
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = 'mGameObjectProperties'
  ClientHeight = 452
  ClientWidth = 376
  Color = clBtnFace
  Constraints.MinHeight = 479
  Constraints.MinWidth = 384
  DefaultMonitor = dmMainForm
  ParentFont = True
  FormStyle = fsStayOnTop
  ScreenSnap = True
  OnDestroy = FormDestroy
  OnHide = FormHide
  OnShow = FormShow
  TextHeight = 15
  object Image1: TImage
    Left = 48
    Top = 432
    Width = 105
    Height = 113
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 376
    Height = 434
    ActivePage = TS_main
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object TS_main: TTabSheet
      Caption = 'mMainProperties'
      DesignSize = (
        368
        404)
      object Label5: TLabel
        Left = 3
        Top = 6
        Width = 80
        Height = 15
        Caption = 'mPrpObjName'
      end
      object ScrollBox: TScrollBox
        Left = 3
        Top = 32
        Width = 362
        Height = 260
        HorzScrollBar.Tracking = True
        VertScrollBar.Tracking = True
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 2
        OnResize = ScrollBoxResize
        ExplicitHeight = 247
      end
      object Edit_name: TEdit
        Left = 80
        Top = 3
        Width = 261
        Height = 23
        Anchors = [akLeft, akTop, akRight]
        CharCase = ecUpperCase
        MaxLength = 64
        TabOrder = 0
        OnChange = Edit_nameChange
        OnEnter = Edit_nameEnter
        OnExit = Edit_nameExit
        OnKeyPress = Edit_nameKeyPress
      end
      object GroupBox_pos: TGroupBox
        Left = 3
        Top = 293
        Width = 362
        Height = 107
        Anchors = [akLeft, akRight, akBottom]
        TabOrder = 3
        ExplicitTop = 280
        object Label1: TLabel
          Left = 5
          Top = 49
          Width = 90
          Height = 15
          Caption = 'mPrpObjDirector'
        end
        object Image: TImage
          Left = 337
          Top = 82
          Width = 11
          Height = 10
          Hint = 'mImageStateHint'
          ParentShowHint = False
          ShowHint = True
          Transparent = True
          OnClick = ImageClick
        end
        object Label2: TLabel
          Left = 211
          Top = 16
          Width = 10
          Height = 15
          Caption = 'X:'
        end
        object Label3: TLabel
          Left = 284
          Top = 16
          Width = 10
          Height = 15
          Caption = 'Y:'
        end
        object Label4: TLabel
          Left = 5
          Top = 79
          Width = 74
          Height = 15
          Caption = 'mPrpObjState'
        end
        object ComboBox_director: TComboBox
          Left = 95
          Top = 49
          Width = 233
          Height = 23
          CharCase = ecUpperCase
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnExit = ComboBox_directorExit
          OnKeyPress = ComboBox_directorKeyPress
        end
        object Bt_find: TBitBtn
          Left = 328
          Top = 49
          Width = 24
          Height = 23
          Hint = 'mFindOnMap'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000FC02FC004985
            D40084C6F400C7D1D100FAEFC700B08D8800B5B8BC00FCFCDA0056595B005CB6
            F10072758400868F9200F5D7B000B6C8CE00D3B39E008E787C000BAA60000000
            0000BA58A6000000000091A58A6000000000D91AF8A6000000000291A58BDDDD
            000000291AF8AA88ABD0000291BF5ECEA8FD0000265C47774F8B000006C44777
            748A00000E4C7777775800000E44477777EA00000C44477777EA00000E44C477
            745B000003C7744C4CFD000000E47444C5D000000006EEEEE300}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = Bt_findClick
        end
        object CB_dir: TComboBox
          Left = 95
          Top = 76
          Width = 233
          Height = 23
          Style = csDropDownList
          TabOrder = 5
          OnChange = CB_dirChange
          OnExit = CB_dirExit
        end
        object ComboBox_zone: TComboBox
          Left = 7
          Top = 13
          Width = 194
          Height = 23
          Style = csDropDownList
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnChange = ComboBox_zoneChange
          OnExit = ComboBox_zoneExit
        end
        object CSEd_x: TEdit
          Left = 225
          Top = 13
          Width = 51
          Height = 23
          NumbersOnly = True
          TabOrder = 1
          OnChange = CSEd_x1Change
          OnEnter = CSEd_x1Enter
          OnExit = CSEd_x1Exit
        end
        object CSEd_y: TEdit
          Left = 299
          Top = 13
          Width = 51
          Height = 23
          NumbersOnly = True
          TabOrder = 2
          OnChange = CSEd_x1Change
          OnEnter = CSEd_x1Enter
          OnExit = CSEd_x1Exit
        end
      end
      object BitBtn_find_obj: TBitBtn
        Left = 341
        Top = 3
        Width = 24
        Height = 23
        Hint = 'mFindOnMap'
        Anchors = [akTop, akRight]
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000FC02FC004985
          D40084C6F400C7D1D100FAEFC700B08D8800B5B8BC00FCFCDA0056595B005CB6
          F10072758400868F9200F5D7B000B6C8CE00D3B39E008E787C000BAA60000000
          0000BA58A6000000000091A58A6000000000D91AF8A6000000000291A58BDDDD
          000000291AF8AA88ABD0000291BF5ECEA8FD0000265C47774F8B000006C44777
          748A00000E4C7777775800000E44477777EA00000C44477777EA00000E44C477
          745B000003C7744C4CFD000000E47444C5D000000006EEEEE300}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BitBtn_find_objClick
      end
    end
    object TabComplexObj: TTabSheet
      Caption = 'mCmplxProperties'
      ImageIndex = 3
      object GroupBox2: TGroupBox
        Left = 3
        Top = 23
        Width = 362
        Height = 146
        Caption = 'mCmplxCaption'
        TabOrder = 0
        DesignSize = (
          362
          146)
        object Label9: TLabel
          Left = 9
          Top = 25
          Width = 46
          Height = 15
          Caption = 'mNode1'
        end
        object Label10: TLabel
          Left = 9
          Top = 54
          Width = 46
          Height = 15
          Caption = 'mNode2'
        end
        object Label11: TLabel
          Left = 9
          Top = 83
          Width = 46
          Height = 15
          Caption = 'mNode3'
        end
        object Bt_find_own_node1: TBitBtn
          Left = 332
          Top = 22
          Width = 24
          Height = 20
          Hint = 'mFindOnMap'
          Anchors = [akTop, akRight]
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000FC02FC004985
            D40084C6F400C7D1D100FAEFC700B08D8800B5B8BC00FCFCDA0056595B005CB6
            F10072758400868F9200F5D7B000B6C8CE00D3B39E008E787C000BAA60000000
            0000BA58A6000000000091A58A6000000000D91AF8A6000000000291A58BDDDD
            000000291AF8AA88ABD0000291BF5ECEA8FD0000265C47774F8B000006C44777
            748A00000E4C7777775800000E44477777EA00000C44477777EA00000E44C477
            745B000003C7744C4CFD000000E47444C5D000000006EEEEE300}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = Bt_find_parentClick
        end
        object Bt_find_own_node2: TBitBtn
          Left = 332
          Top = 51
          Width = 24
          Height = 20
          Hint = 'mFindOnMap'
          Anchors = [akTop, akRight]
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000FC02FC004985
            D40084C6F400C7D1D100FAEFC700B08D8800B5B8BC00FCFCDA0056595B005CB6
            F10072758400868F9200F5D7B000B6C8CE00D3B39E008E787C000BAA60000000
            0000BA58A6000000000091A58A6000000000D91AF8A6000000000291A58BDDDD
            000000291AF8AA88ABD0000291BF5ECEA8FD0000265C47774F8B000006C44777
            748A00000E4C7777775800000E44477777EA00000C44477777EA00000E44C477
            745B000003C7744C4CFD000000E47444C5D000000006EEEEE300}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = Bt_find_parentClick
        end
        object Bt_find_own_node3: TBitBtn
          Left = 332
          Top = 80
          Width = 24
          Height = 20
          Hint = 'mFindOnMap'
          Anchors = [akTop, akRight]
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000FC02FC004985
            D40084C6F400C7D1D100FAEFC700B08D8800B5B8BC00FCFCDA0056595B005CB6
            F10072758400868F9200F5D7B000B6C8CE00D3B39E008E787C000BAA60000000
            0000BA58A6000000000091A58A6000000000D91AF8A6000000000291A58BDDDD
            000000291AF8AA88ABD0000291BF5ECEA8FD0000265C47774F8B000006C44777
            748A00000E4C7777775800000E44477777EA00000C44477777EA00000E44C477
            745B000003C7744C4CFD000000E47444C5D000000006EEEEE300}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = Bt_find_parentClick
        end
        object ED_CplxObjParent1: TEdit
          Left = 72
          Top = 22
          Width = 261
          Height = 23
          CharCase = ecUpperCase
          TabOrder = 0
          OnExit = ED_CplxObjParent1Exit
          OnKeyPress = ED_CplxObjParent1KeyPress
        end
        object ED_CplxObjParent2: TEdit
          Left = 72
          Top = 51
          Width = 261
          Height = 23
          CharCase = ecUpperCase
          TabOrder = 2
          OnExit = ED_CplxObjParent1Exit
          OnKeyPress = ED_CplxObjParent1KeyPress
        end
        object ED_CplxObjParent3: TEdit
          Left = 72
          Top = 80
          Width = 261
          Height = 23
          CharCase = ecUpperCase
          TabOrder = 4
          OnExit = ED_CplxObjParent1Exit
          OnKeyPress = ED_CplxObjParent1KeyPress
        end
        object ButtonJoin: TButton
          Left = 241
          Top = 111
          Width = 115
          Height = 25
          Caption = 'mApply'
          TabOrder = 6
          OnClick = ButtonJoinClick
        end
      end
    end
    object TS_script: TTabSheet
      Caption = 'mActionProperties'
      ImageIndex = 1
      DesignSize = (
        368
        404)
      object ValueListEditor: TValueListEditor
        Left = 3
        Top = 3
        Width = 362
        Height = 399
        Anchors = [akLeft, akTop, akRight, akBottom]
        DefaultRowHeight = 16
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goEditing, goThumbTracking]
        Strings.Strings = (
          '=')
        TabOrder = 0
        TitleCaptions.Strings = (
          'mEvent'
          'mActionHeaderName')
        OnExit = ValueListEditorExit
        OnGetPickList = ValueListEditorGetPickList
        OnKeyDown = ValueListEditorKeyDown
        OnMouseDown = ValueListEditorMouseDown
        OnValidate = ValueListEditorValidate
        ColWidths = (
          122
          234)
        RowHeights = (
          16
          16)
      end
    end
    object TS_Memo: TTabSheet
      Caption = 'mMemo'
      ImageIndex = 2
      DesignSize = (
        368
        404)
      object Memo: TMemo
        Left = 3
        Top = 3
        Width = 362
        Height = 399
        Anchors = [akLeft, akTop, akRight, akBottom]
        MaxLength = 200
        ScrollBars = ssVertical
        TabOrder = 0
        WordWrap = False
        OnEnter = MemoEnter
        OnExit = MemoExit
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 433
    Width = 376
    Height = 19
    Panels = <
      item
        Style = psOwnerDraw
        Width = 24
      end>
    OnDrawPanel = StatusBar1DrawPanel
    ExplicitTop = 421
    ExplicitWidth = 368
  end
  object PopupMenu: TPopupMenu
    Left = 304
    Top = 64
    object N1: TMenuItem
      Caption = 'mCreateNewMiniscript'
      ShortCut = 45
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = 'mEditMiniscript'
      ShortCut = 16453
      OnClick = N2Click
    end
  end
end
