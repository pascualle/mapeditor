//---------------------------------------------------------------------------

#ifndef UTxtObjPropertyH
#define UTxtObjPropertyH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "UObjCode.h"
#include <Dialogs.hpp>
//---------------------------------------------------------------------------

class TCommonResObjManager;
class TPropertiesControl;

class TFormTxtObjProperty : public TPrpFunctions
{
__published:	// IDE-managed Components
	TStatusBar *StatusBar1;
	TPageControl *PageControl;
	TTabSheet *TS_main;
	TLabel *Label5;
	TEdit *Edit_name;
	TGroupBox *GroupBox_pos;
	TLabel *Label2;
	TLabel *Label3;
	TEdit *CSEd_x;
	TEdit *CSEd_y;
	TBitBtn *BitBtn_find_obj;
	TTabSheet *TS_Memo;
	TMemo *Memo;
	TMemo *MemoStaticText;
	TComboBox *ComboBox_zone;
	TLabel *Label6;
	TGroupBox *GroupBox1;
	TLabel *Label8;
	TComboBox *CB_Font;
	TComboBox *ComboBoxH;
	TLabel *Label9;
	TLabel *Label10;
	TComboBox *ComboBoxV;
	TCheckBox *CheckBoxColor;
	TLabel *Label1;
	TEdit *CSEd_w;
	TLabel *Label4;
	TEdit *CSEd_h;
	TRadioButton *RBStaticMode;
	TRadioButton *RBDynamicMode;
	TBevel *Bevel4;
	TButton *Bt_3dot;
	TBevel *Bevel1;
	TButton *Bt_3dotColor;
	TShape *ShapeColor;
	TEdit *EditTextId;
	TColorDialog *ColorDialog;
	TEdit *EditDynamicTextStrings;
	TEdit *EditDynamicTextMaxChars;
	TLabel *Label7;
	TLabel *Label11;
	void __fastcall RBStaticModeClick(TObject *Sender);
	void __fastcall Bt_3dotClick(TObject *Sender);
	void __fastcall CSEd_xEnter(TObject *Sender);
	void __fastcall CSEd_xExit(TObject *Sender);
	void __fastcall ComboBox_zoneChange(TObject *Sender);
	void __fastcall ComboBox_zoneExit(TObject *Sender);
	void __fastcall Edit_nameEnter(TObject *Sender);
	void __fastcall Edit_nameExit(TObject *Sender);
	void __fastcall Edit_nameKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall ComboBox_TextIDExit(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall CSEd_wExit(TObject *Sender);
	void __fastcall CSEd_wEnter(TObject *Sender);
	void __fastcall CSEd_wKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall BitBtn_find_objClick(TObject *Sender);
	void __fastcall EditTextIdEnter(TObject *Sender);
	void __fastcall EditTextIdKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall CB_FontExit(TObject *Sender);
	void __fastcall CB_FontChange(TObject *Sender);
	void __fastcall CSEd_xKeyPress(TObject *Sender, wchar_t &Key);
	void __fastcall Bt_3dotColorClick(TObject *Sender);
	void __fastcall CheckBoxColorClick(TObject *Sender);
	void __fastcall ComboBoxHExit(TObject *Sender);
	void __fastcall ComboBoxHChange(TObject *Sender);
	void __fastcall EditDynamicTextStringsExit(TObject *Sender);
	void __fastcall StatusBar1DrawPanel(TStatusBar *StatusBar, TStatusPanel *Panel, const TRect &Rect);

private:

	TMapTextBox* mpTextObj;
	void *mpEditField;
	UnicodeString  mOldName;
	UnicodeString  mOldVal;
	UnicodeString  mNameObj;
	bool mEmulatorMode;
	bool mDisableChanges;

	virtual void __fastcall SetNodeU(UnicodeString NodeName) override {};
	virtual void __fastcall SetNodeD(UnicodeString NodeName) override {};
	virtual void __fastcall SetNodeL(UnicodeString NodeName) override {};
	virtual void __fastcall SetNodeR(UnicodeString NodeName) override {};
	virtual void __fastcall SetTrigger(UnicodeString triggerName) override {};
	virtual void __fastcall refreshTriggers() override {};
	virtual void __fastcall SetState(UnicodeString name) override {};
	virtual void __fastcall SetVarValue(double val, UnicodeString var_name) override {};
	virtual void __fastcall SetVariables(const GPersonProperties *prp) override {};
	virtual void __fastcall SetScript(UnicodeString KeyName, UnicodeString ScriptName) override {};
	virtual void __fastcall SetNode(UnicodeString NodeName) override {};
	virtual void __fastcall refreshNodes() override {};
	virtual void __fastcall refreshCplxList() override {};
	virtual void __fastcall refresStates() override {};
	virtual void __fastcall refreshVars() override {};
	virtual void __fastcall refreshScripts() override {};

	void __fastcall SaveAllValues(TWinControl *iParent);
	void __fastcall FakeEnterActiveControl();
	void __fastcall FillAttributes(UnicodeString str, bool reset);

public:
	__fastcall TFormTxtObjProperty(TComponent* Owner) override;

	virtual void __fastcall Init(TCommonResObjManager *resManager) override;
	virtual void __fastcall Localize() override;
	virtual void __fastcall EmulatorMode(bool mode) override;

	void __fastcall AssignTxtObj(TMapTextBox *Gpers);
	TMapTextBox* __fastcall GetAssignedObj(){return mpTextObj;}

	virtual void __fastcall SetRemarks(UnicodeString str) override;
	virtual void __fastcall SetTransform(const TTransformProperties& transform) override;
	virtual void __fastcall SetZone(int zone) override;
	virtual void __fastcall SetSizes(int w, int h) override;
	virtual void __fastcall Reset() override;
	virtual void __fastcall Lock(bool value) override;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormTxtObjProperty *FormTxtObjProperty;
//---------------------------------------------------------------------------
#endif
