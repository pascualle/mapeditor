object FSOPEdit: TFSOPEdit
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  ClientHeight = 172
  ClientWidth = 425
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label_name: TLabel
    Left = 16
    Top = 5
    Width = 18
    Height = 13
    Caption = 'aaa'
  end
  object Label_namefile: TLabel
    Left = 16
    Top = 47
    Width = 18
    Height = 13
    Caption = 'bbb'
  end
  object Label_type: TLabel
    Left = 16
    Top = 88
    Width = 15
    Height = 13
    Caption = 'ccc'
  end
  object Bevel1: TBevel
    Left = 382
    Top = 62
    Width = 27
    Height = 21
  end
  object EditPrefix: TEdit
    Left = 15
    Top = 21
    Width = 41
    Height = 21
    Cursor = crArrow
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    ReadOnly = True
    TabOrder = 0
    Text = 'SND_'
  end
  object Ed_name: TEdit
    Left = 59
    Top = 21
    Width = 350
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 64
    TabOrder = 1
    OnChange = Ed_nameChange
  end
  object ButtonOk: TButton
    Left = 175
    Top = 134
    Width = 75
    Height = 25
    Caption = 'mApply'
    TabOrder = 5
    OnClick = ButtonOkClick
  end
  object Edit_filename: TEdit
    Left = 16
    Top = 62
    Width = 367
    Height = 21
    TabOrder = 2
  end
  object ComboBox_type: TComboBox
    Left = 16
    Top = 103
    Width = 393
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemIndex = 0
    TabOrder = 4
    Text = 'mSFXDescription'
    Items.Strings = (
      'mSFXDescription'
      'mBGMDescription')
  end
  object BtG_3dot: TButton
    Left = 384
    Top = 63
    Width = 25
    Height = 19
    Caption = '...'
    TabOrder = 3
    OnClick = BtG_3dotClick
  end
end
