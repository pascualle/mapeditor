//---------------------------------------------------------------------------

#ifndef USaveVarListH
#define USaveVarListH

//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include <ComCtrls.hpp>
#include <Vcl.ActnList.hpp>
#include <System.Actions.hpp>

//---------------------------------------------------------------------------

class TFSaveVarList : public TForm
{
__published:
        TButton *Bt_save;
        TButton *Bt_exit;
        TMemo *Memo;
        TSaveDialog *SaveDialog;
	TStatusBar *StatusBar;
	TActionList *ActionList1;
	TAction *ActSelectAll;
        void __fastcall Bt_saveClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
	void __fastcall ActSelectAllExecute(TObject *Sender);
private:
public:
        __fastcall TFSaveVarList(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFSaveVarList *FSaveVarList;
//---------------------------------------------------------------------------
#endif
