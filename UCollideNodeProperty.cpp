//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UCollideNodeProperty.h"
#include "MainUnit.h"
#include "UObjManager.h"
#include "ULocalization.h"
#include "UMapCollideLines.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFCollideNodeProperty *FCollideNodeProperty;
//---------------------------------------------------------------------------
__fastcall TFCollideNodeProperty::TFCollideNodeProperty(TComponent* Owner)
	: TPrpFunctions(Owner)
{
	mpCollideLineNodeObj = nullptr;
	mpEditField = nullptr;
	mOldName = _T("");
	mNameObj = _T("");
	mEmulatorMode = false;
	mDisableChanges = true;
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::Localize()
{
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::Init(TCommonResObjManager *m)
{
    (void)m;
	if (mpCollideLineNodeObj != nullptr)
	{
		AssignCollideLineNodeObj(mpCollideLineNodeObj);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::BitBtn_find_objClick(TObject *Sender)
{
	(void)Sender;
	Form_m->FindAndShowObjectByName(mNameObj, false);
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::Edit_nameEnter(TObject *Sender)
{
	(void)Sender;
	mOldName = Edit_name->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::Edit_nameExit(TObject *Sender)
{
	(void)Sender;
	if (mpCollideLineNodeObj == nullptr)
	{
		return;
	}

	UnicodeString str = Edit_name->Text.Trim();
	if(str.Compare(mOldName) == 0)
	{
        return;
	}

	if (str.IsEmpty())
	{
		Application->MessageBox(Localization::Text(_T("mObjNameError")).c_str(),
						Localization::Text(_T("mError")).c_str(),
						MB_OK | MB_ICONERROR);
		Edit_name->Text = mOldName;
		return;
	}
	if (Form_m->m_pObjManager->CheckIfMGONameExists(str, mpCollideLineNodeObj) == true)
	{
		Application->MessageBox(Localization::Text(_T("mObjNameExist")).c_str(),
					Localization::Text(_T("mError")).c_str(),
					MB_OK | MB_ICONERROR);
		Edit_name->Text = mOldName;
		return;
	}
	if (!mDisableChanges && mOldName.Compare(str) != 0)
	{
		mpCollideLineNodeObj->Name = str;
		mNameObj = str;
		Form_m->OnChangeObjName(mpCollideLineNodeObj, mOldName);
		mpCollideLineNodeObj->changeHint();
		mOldName = str;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::Edit_nameKeyPress(TObject *Sender, System::WideChar &Key)
{
	(void)Sender;
	if (VK_RETURN == Key)
	{
		Key = 0;
		Edit_nameExit(nullptr);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::CSEd_xEnter(TObject *Sender)
{
	mpEditField = Sender;
	mOldVal = ((TEdit*)Sender)->Text;
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::CSEd_xExit(TObject *Sender)
{
	UnicodeString str = ((TEdit*)Sender)->Text.Trim();

	if(Sender == mpEditField)
	{
		if (!mDisableChanges && mOldVal.Compare(str) != 0)
		{
			if (mpCollideLineNodeObj != nullptr)
			{
				int x, y;
				bool res = true;
				try
				{
					x = StrToInt(CSEd_x->Text);
					y = StrToInt(CSEd_y->Text);
				}
				catch(...)
				{
					res = false;
				}
				if(res)
				{
					Form_m->OnChangeObjPosition(mpCollideLineNodeObj, x, y);
				}
			}
		}
		CSEd_xEnter(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::CSEd_xKeyPress(TObject *Sender, System::WideChar &Key)
{
	if(VK_RETURN == Key)
	{
		Key = 0;
		CSEd_xExit(Sender);
	}
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::SetTransform(const TTransformProperties& transform)
{
	if(!Visible)
	{
		return;
	}
	double x, y;
	transform.GetPosition(x, y);
	CSEd_x->Text = IntToStr(static_cast<int>(x));
	CSEd_y->Text = IntToStr(static_cast<int>(y));
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::SetRemarks(UnicodeString str)
{
	if (!Visible)
	{
		return;
    }
	Memo->Text = str;
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::AssignCollideLineNodeObj(TMapCollideLineNode *obj)
{
	double x, y;
	if (mpCollideLineNodeObj == obj)
	{
		return;
	}

	if (mpCollideLineNodeObj != nullptr)
	{
		if (Visible)
		{
			mOldVal = _T("");
			SaveAllValues(this);
		}
		mpCollideLineNodeObj->UnregisterPrpWnd();
	}

	mpCollideLineNodeObj = obj;
	if (mpCollideLineNodeObj != nullptr)
	{
		mpCollideLineNodeObj->RegisterPrpWnd(this);
	}
	else
	{
		return;
	}

	Caption = Localization::Text(_T("mGameObjectProperties")) + " [" + mpCollideLineNodeObj->getGParentName() + "]";

	mDisableChanges = true;

	mpCollideLineNodeObj->getCoordinates(&x, &y);
	CSEd_x->Text = IntToStr((int)x);
	CSEd_y->Text = IntToStr((int)y);

	UnicodeString strid = _T("");

	mNameObj = mpCollideLineNodeObj->Name;
	mOldName = mpCollideLineNodeObj->Name;
	Edit_name->Text = mpCollideLineNodeObj->Name;
	Memo->Clear();
	Memo->Text = mpCollideLineNodeObj->getRemarks();
	mDisableChanges = false;
	Lock(mpCollideLineNodeObj->IsLocked());

	if (Visible)
	{
		FakeEnterActiveControl();
	}
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::FakeEnterActiveControl()
{
	TWinControl *c = ActiveControl;
	if (c == nullptr)
	{
		return;
	}
	if (c->ClassNameIs(_T("TEdit")))
	{
		if (((TEdit*)c)->OnEnter != nullptr)
		{
			((TEdit*)c)->OnEnter(c);
		}
		return;
	}
	if (c->ClassNameIs(_T("TMemo")))
	{
		if (((TMemo*)c)->OnEnter != nullptr)
		{
			((TMemo*)c)->OnEnter(c);
		}
		return;
	}
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::SaveAllValues(TWinControl *iParent)
{
	if (iParent == nullptr)
		return;

	TControl *c;
	for (int i = 0; i < iParent->ControlCount; i++)
	{
		c = iParent->Controls[i];
		if (c == nullptr)
			continue;

		if (c->ClassNameIs(_T("TPanel")) ||
			c->ClassNameIs(_T("TGroupBox")) ||
			c->ClassNameIs(_T("TTabSheet")) ||
			c->ClassNameIs(_T("TPageControl")) ||
			c->ClassNameIs(_T("TScrollBox")) ||
			c->ClassNameIs(_T("TForm")))
		{
			SaveAllValues((TWinControl*)c);
		}
		else
		{
			if (c->ClassNameIs(_T("TEdit")))
			{
				if (((TEdit*)c)->OnExit != nullptr)
					((TEdit*)c)->OnExit(c);
			}
			else if (c->ClassNameIs(_T("TMemo")))
			{
				if (((TMemo*)c)->OnExit != nullptr)
					((TMemo*)c)->OnExit(c);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::FormShow(TObject *Sender)
{
	AssignCollideLineNodeObj(mpCollideLineNodeObj);
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::FormHide(TObject *Sender)
{
	mOldVal = _T("");
	SaveAllValues(this);
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::Reset()
{
	AssignCollideLineNodeObj(nullptr);
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::EmulatorMode(bool mode)
{
	mEmulatorMode = mode;
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::Lock(bool value)
{
	(void)value;
	StatusBar1->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall TFCollideNodeProperty::StatusBar1DrawPanel(TStatusBar *StatusBar, TStatusPanel *Panel, const TRect &Rect)
{
	(void)Panel;
	if(mpCollideLineNodeObj->IsLocked())
	{
		Form_m->ImageList1->Draw(StatusBar->Canvas, Rect.Left, Rect.Top, LOCK_IMAGE_IDX);
	}
	else
	{
		StatusBar->Canvas->Brush->Style = bsSolid;
		StatusBar->Canvas->Brush->Color = StatusBar->Color;
		StatusBar->Canvas->FillRect(Rect);
	}
}
//---------------------------------------------------------------------------

