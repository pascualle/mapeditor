//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UAbour.h"
#include "ULocalization.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm_about *Form_about;
//---------------------------------------------------------------------------
__fastcall TForm_about::TForm_about(TComponent* Owner)
        : TForm(Owner)
{
	Localization::Localize(this);
}
//---------------------------------------------------------------------------

void __fastcall TForm_about::FormKeyPress(TObject */*Sender*/, char &Key)
{
	if(Key == VK_ESCAPE)
	{
		Close();
    }
}
//---------------------------------------------------------------------------


