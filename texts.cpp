
#include "platform.h"
#include "texts.h"
#include <wchar.h>
#include "UObjCode.h"
#include "USave.h"
#include "ULoad.h"

//-----------------------------------------------------------------------------

s32 _findTextureOffset(const Font* ipFont, wchar_t iChar, s32 *oX, s32 *oY, s32 *oW, s32 *oH);
void _rebuildTextData(::Text *opText);

//-----------------------------------------------------------------------------

void InitFont(u8* ipData, Font *oFont)
{
	s32 intVal;
	char resName[128];

	SDK_NULL_ASSERT(oFont);
	SDK_NULL_ASSERT(ipData);

	SDK_ASSERT(ipData[0] == 'B' &&
		        ipData[1] == 'M' &&
		        ipData[2] == 'F' &&
		        ipData[3] == 3); //"Unsupportet font data format"

	ipData += 4;
	
	//block1
	ipData++;
	intVal = _NBytesToInt(ipData, 4, 0);
	ipData += 4 + intVal;

	//block2
	ipData++;
	intVal = _NBytesToInt(ipData, 4, 0);
	ipData += 4 + intVal;
	
	//block3
	ipData += 5;
	resName[0] = 'd';
	resName[1] = 'a';
	resName[2] = 't';
	resName[3] = 'a';
	resName[4] = '/';
	resName[5] = 0;
	intVal = 5;
	while(*ipData != '.')
	{
		resName[intVal] = (char)(*ipData);
		intVal++;
		ipData++;
	}
	resName[intVal] = '.';
	resName[intVal+1] = 'r';
	resName[intVal+2] = 'e';
	resName[intVal+3] = 's';
	resName[intVal+4] = 0;
	ipData += 5;
	
	//block4
	ipData++;
	intVal = _NBytesToInt(ipData, 4, 0);
	ipData += 4;

	SDK_ASSERT(oFont->mAlphabet.mpAlphabet == 0); //font is already assigned
	SDK_ASSERT(oFont->mAlphabet.mpAlphabetTextureOffsetMap == 0); //font is already assigned
	oFont->mAlphabet.mAlphabetSize = (u16)(intVal / 20);
	oFont->mAlphabet.mpAlphabet = (wchar_t*)MALLOC(oFont->mAlphabet.mAlphabetSize * sizeof(wchar_t), "foo");
	oFont->mAlphabet.mpAlphabetTextureOffsetMap = (AlphabetTextureOffsetMap *)MALLOC(oFont->mAlphabet.mAlphabetSize * sizeof(AlphabetTextureOffsetMap), "foo");

	for(intVal = 0; intVal < oFont->mAlphabet.mAlphabetSize; intVal++)
	{
		oFont->mAlphabet.mpAlphabet[intVal] = (wchar_t)_NBytesToInt(ipData, 4, 0);
		ipData += 4;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mX = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mY = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mW = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mH = (s16)_NBytesToInt(ipData, 2, 0);	
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mX_off = (s16)_NBytesToInt(ipData, 2, 0);
		ipData += 2;
		oFont->mAlphabet.mpAlphabetTextureOffsetMap[intVal].mY_off = (s16)_NBytesToInt(ipData, 2, 0);	
		ipData += 6;
	}

	/*{
		oFont->mpAbcBmpData = LoadDCImage(&oFont->mpAbcImage, resName);
		SDK_NULL_ASSERT(oFont->mpAbcBmpData);
	}*/
    
	if(oFont->mAlphabet.mAlphabetSize > 0)
	{
		oFont->mAlphabet.mFixedHeight = (u16)(oFont->mAlphabet.mpAlphabetTextureOffsetMap[0].mH + oFont->mAlphabet.mpAlphabetTextureOffsetMap[0].mY_off + 2);
		oFont->mAlphabet.mFixedWidth = 0;
	}
	else
	{
		oFont->mAlphabet.mFixedHeight = 0;
		oFont->mAlphabet.mFixedWidth = 0;
	}
}
//----------------------------------------------------------------------------------------------------------------

void ReleaseFont(Font *oFont)
{
	SDK_ASSERT(oFont->mAlphabet.mpAlphabet != 0); //font is not inited
	SDK_ASSERT(oFont->mAlphabet.mpAlphabetTextureOffsetMap != 0); //font is not inited
	//SDK_ASSERT(oFont->mpAbcBmpData != 0); //font is not inited
	//FREE(oFont->mpAbcBmpData);
	FREE(oFont->mAlphabet.mpAlphabet);
	FREE(oFont->mAlphabet.mpAlphabetTextureOffsetMap);
	oFont->mAlphabet.mAlphabetSize = 0;
	//oFont->mpAbcImage = NULL;
}
//----------------------------------------------------------------------------------------------------------------

void InitText(::Text *oText)
{
  oText->mStringsCount = 0;
  oText->mStringsCountMax = 0;
  oText->mAttributes.mMargin = DEFAULT_TEXT_MARGIN;
  oText->mAttributes.mAlignment = TEXT_ALIGNMENT_LEFT | TEXT_ALIGNMENT_TOP;
  oText->mAttributes.mCanvasColor = (TColor)0;
  oText->mppStrings = NULL;
  oText->mpFont = NULL;
  //oText->mpCanvas = NULL;
}
//----------------------------------------------------------------------------------------------------------------

void ReleaseText(::Text *oText)
{
  oText->mStringsCount = 0;
  oText->mAttributes.mMargin = DEFAULT_TEXT_MARGIN;
  oText->mAttributes.mAlignment = TEXT_ALIGNMENT_LEFT | TEXT_ALIGNMENT_TOP;
  oText->mAttributes.mCanvasColor = (TColor)0;
  oText->mpFont = NULL;
  if(oText->mppStrings != NULL)
  {
    int i;
    SDK_ASSERT(oText->mStringsCountMax != 0);
    for(i = 0; i < oText->mStringsCountMax; i++)
    {
      FREE(oText->mppStrings[i]);
    }
    FREE(oText->mppStrings);
    oText->mppStrings = NULL;
  }
  oText->mStringsCountMax = 0;
}
//----------------------------------------------------------------------------------------------------------------

void SetText(const wchar_t* text, ::Text *opText)
{
	s32 i, str_ct, c, beg;

	SDK_NULL_ASSERT(text);
	SDK_NULL_ASSERT(opText);

	opText->mStringsCount = 0;
	i = 0;
	
	while(text[i] != L'\0')
	{
		if(text[i] == L'\n')
		    opText->mStringsCount++;
		i++;
	}
	opText->mStringsCount++;

	if(opText->mppStrings != NULL)
	{
		if(opText->mStringsCount > opText->mStringsCountMax)
		{
			for(i = 0; i < opText->mStringsCountMax; i++)
			{
				FREE(opText->mppStrings[i]);
			}
			FREE(opText->mppStrings);
			
			opText->mppStrings = (wchar_t**)MALLOC(opText->mStringsCount * sizeof(wchar_t*), "foo");
			opText->mStringsCountMax = opText->mStringsCount;
			for(i = 0; i < opText->mStringsCount; i++)
			{
				opText->mppStrings[i] = NULL;
			}
		}
	}
	else
	{
		opText->mppStrings = (wchar_t**)MALLOC(opText->mStringsCount * sizeof(wchar_t*), "foo");
		opText->mStringsCountMax = opText->mStringsCount;
		for(i = 0; i < opText->mStringsCount; i++)
		{
			opText->mppStrings[i] = NULL;
		}
	}
	
	beg = c = i = str_ct = 0;
	
	i = -1;
	do
	{
		i++;
		if(text[i] == L'\n' || text[i] == L'\0')
		{
			if(opText->mppStrings[c] == 0)
			{
				opText->mppStrings[c] = (wchar_t*)MALLOC((str_ct + 1) * sizeof(wchar_t), "foo");
			}
			else
			{
				if(wcslen(opText->mppStrings[c]) < (size_t)str_ct)
				{
					FREE(opText->mppStrings[c]);
					opText->mppStrings[c] = (wchar_t*)MALLOC((str_ct + 1) * sizeof(wchar_t), "foo");
				}
			}
			//MI_CpuCopy8(::Text + beg, opText->mppStrings[c], str_ct * sizeof(wchar_t));
      memcpy(opText->mppStrings[c], text + beg, str_ct * sizeof(wchar_t));
			opText->mppStrings[c][str_ct] = L'\0';
			c++;
			beg = i + 1;
			str_ct = 0;
 		}
		else
		{
			if(text[i] == L'\r')
			{
				//beg += 1;
			}
			else
			{
				str_ct++;
			}
		}
	}
	while(text[i] != 0);
	
    _rebuildTextData(opText);
}

//----------------------------------------------------------------------------------------------------------------

void SetTextCanvas(u16 canvasWidth, u16 canvasHeight, ::Text *opText)
{
	if(canvasWidth > 0 && canvasHeight > 0)
	{
        SDK_NULL_ASSERT(opText->mpCanvas);
		opText->mpCanvas->SetSize(canvasWidth, canvasHeight);
	}
	_rebuildTextData(opText);
}
//----------------------------------------------------------------------------------------------------------------

s32 _findTextureOffset(const Font* ipFont, wchar_t iChar, s32 *oX, s32 *oY, s32 *oW, s32 *oH)
{
	s16 i;
	
	SDK_NULL_ASSERT(ipFont);
	SDK_NULL_ASSERT(ipFont->mAlphabet.mpAlphabet); // Current font is not assigned by data
	SDK_NULL_ASSERT(ipFont->mpAbcImage); // Current font is not assigned by data

	for(i = 0; i < ipFont->mAlphabet.mAlphabetSize; i++)
	{
		if(ipFont->mAlphabet.mpAlphabet[i] == iChar)
		{
			if(ipFont->mAlphabet.mpAlphabetTextureOffsetMap != 0)
			{
				*oX = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[i].mX;
				*oY = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[i].mY;
				*oW = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[i].mW;
				*oH = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[i].mH;
			}
			else
			{
				*oY = 0;
				*oX = i * ipFont->mAlphabet.mFixedWidth;
				if (*oX >= ipFont->mpAbcImage->mWidth)
				{
					*oY	= (*oX / ipFont->mpAbcImage->mWidth) * ipFont->mAlphabet.mFixedHeight;
					*oX = *oX % ipFont->mpAbcImage->mWidth;
				}
				SDK_ASSERT(*oY <= ipFont->mpAbcImage->mHeight); // Offset for  char is out of font-image Y-bound
				SDK_ASSERT(*oX <= ipFont->mpAbcImage->mWidth); // Offset for  char is out of font-image X-bound
				*oW = ipFont->mpAbcImage->mWidth;
				*oH = ipFont->mpAbcImage->mHeight;
			}
			return i;
		}
	}
	
	if(ipFont->mAlphabet.mpAlphabetTextureOffsetMap != 0)
	{
		*oX = 0;
		*oY = 0;
		*oW = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[0].mW;
		*oH = ipFont->mAlphabet.mpAlphabetTextureOffsetMap[0].mH;
		return 0;
	}
	else
	{
		*oY = 0;
		*oX = 0;
		*oW = ipFont->mAlphabet.mFixedWidth;
		*oH = ipFont->mAlphabet.mFixedHeight;
		SDK_ASSERT(*oY <= ipFont->mpAbcImage->mHeight); // Offset for  char is out of font-image Y-bound
		SDK_ASSERT(*oX <= ipFont->mpAbcImage->mWidth); // Offset for  char is out of font-image X-bound
		return 0;
	}
}

//----------------------------------------------------------------------------------------------------------------

void _rebuildTextData(::Text *opText)
{
	SDK_NULL_ASSERT(opText);
	//SDK_NULL_ASSERT(opText->mpFont); // Text is not assigned by font
	//SDK_NULL_ASSERT(opText->mpFont->mAlphabet.mpAlphabet); // Current font is not assigned by data

	if(opText->mpCanvas == NULL)
	{
        return;
	}

	if(opText->mAttributes.mCanvasColor == clTRANSPARENT)
	{
		opText->mpCanvas->Clear(static_cast<TColor>(0x00000000));
	}
	else
	{
		opText->mpCanvas->Clear(static_cast<TColor>(0xff000000 | opText->mAttributes.mCanvasColor));
	}

	if(opText->mpFont && opText->mpFont->mpAbcImage && opText->mppStrings != NULL)
	{
		s32 i, j, x, y, w, h, tx, ty, len, xb, xe, xs, pos;
		BLENDFUNCTION bf;
		bf.BlendOp = AC_SRC_OVER;
		bf.BlendFlags = 0;
		bf.SourceConstantAlpha = 255;
		bf.AlphaFormat = AC_SRC_ALPHA;
		//MI_CpuFill16(opText->mpCanvas->mpData, opText->mAttributes.mCanvasColor, sizeof(GXRgba) * (opText->mpCanvas->mWidth * opText->mpCanvas->mHeight));

		ty = 0;
        
		if ((opText->mAttributes.mAlignment & TEXT_ALIGNMENT_VCENTER) == TEXT_ALIGNMENT_VCENTER)
		{
			ty = (opText->mpCanvas->mHeight - opText->mpFont->mAlphabet.mFixedHeight * opText->mStringsCount) / 2;
		}
		else if ((opText->mAttributes.mAlignment & TEXT_ALIGNMENT_BOTTOM) == TEXT_ALIGNMENT_BOTTOM)
		{
			ty = opText->mpCanvas->mHeight - opText->mpFont->mAlphabet.mFixedHeight * opText->mStringsCount;
		}
        
		for(j = 0; j < opText->mStringsCount; j++)
		{
			tx = 0;
			len = (s32)wcslen(opText->mppStrings[j]);
			    			
			if ((opText->mAttributes.mAlignment & TEXT_ALIGNMENT_HCENTER) == TEXT_ALIGNMENT_HCENTER)
			{
				for (i = 0; i < len; i ++)
				{
					_findTextureOffset(opText->mpFont, opText->mppStrings[j][i], &x, &y, &w, &h);
					if ((opText->mAttributes.mAlignment & TEXT_ALIGNMENT_HCENTER) == TEXT_ALIGNMENT_HCENTER)
					{
						tx += w + opText->mAttributes.mMargin;
					}
				}
				tx = (opText->mpCanvas->mWidth - tx) / 2;
				xb = 0;
				xe = len;
				xs = 1;
			}
			else if ((opText->mAttributes.mAlignment & TEXT_ALIGNMENT_LEFT) == TEXT_ALIGNMENT_LEFT)
			{
				xb = 0;
				xe = len;
				xs = 1;
			}
			else if ((opText->mAttributes.mAlignment & TEXT_ALIGNMENT_RIGHT) == TEXT_ALIGNMENT_RIGHT)
			{
				tx = opText->mpCanvas->mWidth;
				xb = len - 1;
				xe = -1;
				xs = -1;
			}
			for (i = xb; i != xe; i += xs)
			{
				if ((pos = _findTextureOffset(opText->mpFont, opText->mppStrings[j][i], &x, &y, &w, &h)) >= 0)
				{
					if (xs < 0)
					{
						tx -= w + opText->mAttributes.mMargin;					    
				    }
				    if ((tx + w < 0 || tx > opText->mpCanvas->mWidth) && (opText->mAttributes.mAlignment & TEXT_ALIGNMENT_HCENTER) != TEXT_ALIGNMENT_HCENTER)
				    {
				    	break;
				    }
				    //DrawImageToImage(opText->mpFont->mpAbcImage, x, y, w, h, opText->mpCanvas, tx, ty + opText->mpFont->mAlphabet.mpAlphabetTextureOffsetMap[pos].mY_off);
					::AlphaBlend(opText->mpCanvas->Canvas->Handle,        // handle to destination DC
								tx,   // x-coord of destination upper-left corner
								ty + opText->mpFont->mAlphabet.mpAlphabetTextureOffsetMap[pos].mY_off,   // y-coord of destination upper-left corner
								w,     // width of destination rectangle
								h,    // height of destination rectangle
								opText->mpFont->mpAbcImage->Canvas->Handle,
								x,    // x-coord of source upper-left corner
								y,    // y-coord of source upper-left corner
								w,      // width of source rectangle
								h,     // height of source rectangle
								bf);
				}
				else if (xs < 0)
				{
					tx -= w + opText->mAttributes.mMargin;
				}
				if (xs > 0)
				{
					tx += w + opText->mAttributes.mMargin;
				}
			}
			ty += opText->mpFont->mAlphabet.mFixedHeight;
			if ((ty + h < 0 || ty > opText->mpCanvas->mHeight) && (opText->mAttributes.mAlignment & TEXT_ALIGNMENT_VCENTER) != TEXT_ALIGNMENT_VCENTER)
			{
				break;
			}
		}
	}
}
//----------------------------------------------------------------------------------------------------------------

//TUnicodeText
//TUnicodeText
//TUnicodeText

//---------------------------------------------------------------------------

__fastcall TUnicodeText::TUnicodeText()
 : mWidth(0)
 , mHeight(0)
 , mLastFontName(_T(""))
{
	::InitText(&mText);
	mText.mpCanvas = NULL;
}
//---------------------------------------------------------------------------

__fastcall TUnicodeText::TUnicodeText(const TUnicodeText &t)
{
	::InitText(&mText);
	mText.mpCanvas = NULL;
	t.CopyTo(this);
}
//---------------------------------------------------------------------------

__fastcall TUnicodeText::~TUnicodeText()
{
	Clear();
}
//---------------------------------------------------------------------------

void __fastcall TUnicodeText::Clear()
{
	::ReleaseText(&mText);
}
//---------------------------------------------------------------------------

const UnicodeString __fastcall TUnicodeText::GetFontName() const
{
	return mLastFontName;
}
//---------------------------------------------------------------------------

void __fastcall TUnicodeText::SetFontName(UnicodeString fontName)
{
	mText.mpFont = NULL;
	mLastFontName = fontName;
}
//---------------------------------------------------------------------------

void __fastcall TUnicodeText::SetText(const wchar_t *txt)
{
	::SetText(txt, &mText);
}
//---------------------------------------------------------------------------

void __fastcall TUnicodeText::SetAttributes(const ::TextAttributes attr)
{
	mText.mAttributes = attr;
	FillIfEmpty();
}
//---------------------------------------------------------------------------

const ::TextAttributes __fastcall TUnicodeText::GetAttributes() const
{
	return mText.mAttributes;
}
//---------------------------------------------------------------------------

void __fastcall TUnicodeText::SetCanvasSize(int w, int h)
{
	mWidth = w;
	mHeight = h;
	FillIfEmpty();
}
//---------------------------------------------------------------------------

int __fastcall TUnicodeText::GetCanvasWidth() const
{
	return mWidth;
}
//---------------------------------------------------------------------------

int __fastcall TUnicodeText::GetCanvasHeight() const
{
	return mHeight;
}
//---------------------------------------------------------------------------

bool __fastcall TUnicodeText::IsEmpty() const
{
	return mText.mStringsCount == 0;
}
//---------------------------------------------------------------------------

void __fastcall TUnicodeText::GetCanvasSize(int &w, int &h) const
{
	w = mWidth;
	h = mHeight;
}
//---------------------------------------------------------------------------

const ::Text *__fastcall TUnicodeText::GetTextData() const
{
	if (mText.mStringsCount != 0)
	{
		return &mText;
	}
	return NULL;
}
//---------------------------------------------------------------------------

void __fastcall TUnicodeText::FillIfEmpty()
{
  if(mText.mStringsCount == 0)
  {
    wchar_t a[1];
    a[0] = L'\0';
    ::SetText(a, &mText);
  }
}
//---------------------------------------------------------------------------

void __fastcall TUnicodeText::CopyTo(TUnicodeText *pt) const
{
	pt->Clear();
	pt->SetFontName(mLastFontName);
	pt->mText.mAttributes = mText.mAttributes;
	if (mText.mStringsCount > 0)
	{
		assert(pt->mText.mppStrings == NULL);
		pt->mText.mppStrings = (wchar_t**)MALLOC(mText.mStringsCount * sizeof(wchar_t*), "foo");
		pt->mText.mStringsCountMax = mText.mStringsCountMax;
		pt->mText.mStringsCount = mText.mStringsCount;
		for (int j = 0; j < mText.mStringsCount; j++)
		{
			if(mText.mppStrings[j] != NULL)
			{
				const int len = wcslen(mText.mppStrings[j]);
				pt->mText.mppStrings[j] = (wchar_t*)MALLOC((len + 1) * sizeof(wchar_t), "foo");
        if(len > 0)
        {
				  wcscpy(pt->mText.mppStrings[j], mText.mppStrings[j]);
        }
				pt->mText.mppStrings[j][len] = L'\0';
			}
			else
			{
				pt->mText.mppStrings[j] = NULL;
			}
		}
	}
	pt->mWidth = mWidth;
	pt->mHeight = mHeight;
}
//---------------------------------------------------------------------------

void __fastcall TUnicodeText::Save(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h) const
{
	DWORD w_val;

	if (GetTextData() == nullptr)
	{
		return;
	}

	w_val = mText.mAttributes.mMargin;
	f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftChar, _T("margin"), h);

	w_val = mText.mAttributes.mCanvasColor;
	f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("canvascolor"), h);

	w_val = mText.mAttributes.mAlignment;
	f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("alignment"), h);

	w_val = mWidth;
	f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("width"), h);

	w_val = mHeight;
	f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("height"), h);

	WriteAnsiStringData(f, h, _T("font"), mLastFontName);

	w_val = mText.mStringsCount;
	f.writeFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("stringscount"), h);

	if (w_val > 0)
	{
		for (int j = 0; j < mText.mStringsCount; j++)
		{
			TSaveLoadStreamHandle h1;
			f.writeFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("line_") + IntToStr(j), h1);

			if(mText.mppStrings[j] != nullptr)
			{
				WriteWideStringData(f, h1, TSaveLoadStreamData::TSLFieldType::ftU16, _T("text"), mText.mppStrings[j]);
			}
			else
			{
				WriteWideStringData(f, h1, TSaveLoadStreamData::TSLFieldType::ftU16, _T("text"), _T(""));
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TUnicodeText::Load(TSaveLoadStreamData& f, TSaveLoadStreamHandle &h, unsigned char ver)
{
	int width, heigh;
	DWORD w_val;

	::ReleaseText(&mText);
	mText.mpFont = nullptr;
	mLastFontName = _T("");

	w_val = 0;
	f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftChar, _T("margin"), h);
	mText.mAttributes.mMargin = static_cast<unsigned char>(w_val);

	w_val = 0;
	f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("canvascolor"), h);
	mText.mAttributes.mCanvasColor = static_cast<TColor>(w_val);

	w_val = 0;
	f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftS32, _T("alignment"), h);
	mText.mAttributes.mAlignment = w_val;

	width = 0;
	f.readFn(&width, TSaveLoadStreamData::TSLFieldType::ftU16, _T("width"), h);

	heigh = 0;
	f.readFn(&heigh, TSaveLoadStreamData::TSLFieldType::ftU16, _T("height"), h);

	ReadAnsiStringData(f, h, _T("font"), mLastFontName, ver);

	w_val = 0;
	f.readFn(&w_val, TSaveLoadStreamData::TSLFieldType::ftU16, _T("stringscount"), h);
	mText.mStringsCount = (s16)w_val;
	mWidth = width;
	mHeight = heigh;
	if (w_val > 0)
	{
		mText.mppStrings = (wchar_t**)MALLOC(mText.mStringsCount * sizeof(wchar_t*), "foo");
		mText.mStringsCountMax = mText.mStringsCount;

		for (int j = 0; j < mText.mStringsCount; j++)
		{
			TSaveLoadStreamHandle h1;
			f.readFn(&h, TSaveLoadStreamData::TSLFieldType::ftNewSection, _T("line_") + IntToStr(j), h1);

			if(!h1.IsValid())
			{
				throw Exception(_T("line_") + IntToStr(j));
			}

			UnicodeString tstr;
			ReadWideStringData(f, h1, TSaveLoadStreamData::TSLFieldType::ftU16, _T("text"), tstr);

			mText.mppStrings[j] = nullptr;

			if (tstr.IsEmpty())
			{
				mText.mppStrings[j] = (wchar_t*)MALLOC((1) * sizeof(wchar_t), "foo");
				mText.mppStrings[j][0] = L'\0';
			}
			else
			{
				mText.mppStrings[j] = (wchar_t*)MALLOC((tstr.Length() + 1) * sizeof(wchar_t), "foo");
				memcpy(mText.mppStrings[j], tstr.c_str(), tstr.Length() * sizeof(wchar_t));
				mText.mppStrings[j][tstr.Length()] = L'\0';
			}
		}
	}
	else
	{
		::ReleaseText(&mText);
	}
}
//---------------------------------------------------------------------------

