object FSpObjProperty: TFSpObjProperty
  Left = 329
  Top = 192
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'mGameObjectProperties'
  ClientHeight = 471
  ClientWidth = 390
  Color = clBtnFace
  Constraints.MinHeight = 478
  Constraints.MinWidth = 398
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  Position = poDefault
  ScreenSnap = True
  OnDestroy = FormDestroy
  OnHide = FormHide
  OnShow = FormShow
  TextHeight = 13
  object ImageLock: TImage
    Left = 3
    Top = 451
    Width = 19
    Height = 19
    Picture.Data = {
      07544269746D617032050000424D320500000000000032040000280000001000
      000010000000010008000000000000010000120B0000120B0000FF000000FF00
      00000303020026262700036902006D340E000877C6000909F300C6C0B700B520
      810035030600D68F6B000BF7FB003D3D3D007B7B7A00188FBB00526ECA00CBCB
      CA00F2A83C00490809003344AD009A9A9A0052525200C18129004B50AA004859
      CF007A6B5400333333001DA1DE00F9F7C500896C340015154C007B922E007075
      9D006E6EE800DEDCDC00F8E80600D050D8005F60240002CCFE00929393000853
      07001C1D1D003A3AAF008B8B8A0067472D00FBFBFB0094918E00FBF907002A2A
      0200F1F02200FEFEFE00FAF29D00A695570059592400808081008F704400F9BC
      02004D4D4D00BAF2F80066420800C1BDB80084C2C9004E6D710047480E00D6D6
      D400755B54004A4845002E3B9C00B1B1B000A9A9A8005656550091ACB100F1F1
      F000656564009888600030310200A2A2F4007474740079622E00292984004A47
      2B006B6B6B0019272C0001A0FC00A89D9000383837003B3B000000007B0000B7
      00000C0A9000C1F0FC00452F0000FAFAE60059595A00361E060004033500E177
      3000FBCA060090481800BE938500EFD5C500B2A5A200FCF4F200E2CCC7004C45
      4500DBC0AF00FDFBFB0054452F0018180100926E6600796A6900FDFDFD00928D
      7400EF9D2800676768004A341F00424648004C7FB4003C2C1B00DAD4CE00ACA2
      980045829100572B10006C6C310000990000D6CB14002F303000FAF26C003363
      9B009E928F00F3EDE70077DAF300716161000A9DE1008D8558006EA5E0001EAD
      E900AAAAB700665844005A54350093867A002C3D40007C736A00EBE1AB004F84
      8A0060AEF9005D9BA2008057090010151300415A5D0089544400BFB580005888
      F600DCD89E00CECD8600B3865100E7DCD900A6BF1F00B6CAF800DACDC9008F8F
      4900DDBE8200DBBEAA007F603C00BCBCB80012206100BE9F5400C75B710000CC
      000000330000CFB64F0011B01A00C19F4C000099FF0015A80D009B8F82002233
      88000FFF0300F9F5AF005F808500E8D8CD00564210002BE31300E8DBA3007F82
      8100A77B5D008C837700AC87560087BEDB0077214700BBB2A70051511B00FF9B
      1A00FB6002000D5CBF009B923700FBDC090055410F00FDFDFB0000F75600A6A9
      4C00D4F80F00DFDF8F0093934100FFFFFF0099FFFF008389BE00808F9200565B
      60007DF68400F8BC8A0033449900AAA17100C2CFFC00939AD60003BAFE008B9C
      9E0058301800412A1200DFC3B300F6F7F800F29DF700DAAB79000705C2007272
      3E009A9AFE00D8CC6200EFEFEF005C3F3E00716F6C00AA9A8B00222266005D72
      2200AF8B33008CBAE400F6D4F800FFD74C000A9CD20086CAEA00E8D09700FFFF
      FF00F0F0F000F1F1F100F2F2F200F3F3F300F4F4F400F5F5F500F6F6F600F7F7
      F700F8F8F800F9F9F900FAFAFA00FBFBFB00FCFCFC00FDFDFD00FEFEFE002323
      2323232323232323232323232323232323232D2D0C0C0C0C2D2D232323232323
      232D41540101010154412D2323232323232D54010154540101542D2323232323
      230C0101010B0B0101010C2323232323230C0101010B0B0101010C2323232323
      230C0101010B0B0101010C2323232323230C01010101010101010C2323232323
      230C01010101010101010C2323232323230C0B0B0B0B0B0B0B0B0C2323232323
      23230C4123232323410C23232323232323230C4123232323410C232323232323
      23230C4123232323410C23232323232323232D410C23230C412D232323232323
      2323232D414141412D232323232323232323232323232323232323232323}
    Transparent = True
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 390
    Height = 449
    ActivePage = TabSheet2
    Align = alTop
    TabOrder = 0
    ExplicitWidth = 382
    object TabSheet2: TTabSheet
      Caption = 'mMainProperties'
      object Label11: TLabel
        Left = 3
        Top = 9
        Width = 35
        Height = 13
        Caption = 'mName'
      end
      object GroupBoxPos: TGroupBox
        Left = 1
        Top = 28
        Width = 379
        Height = 51
        Caption = 'mPositionOnMapBottomCenterLabel'
        TabOrder = 2
        object Label12: TLabel
          Left = 5
          Top = 23
          Width = 10
          Height = 13
          Caption = 'X:'
        end
        object Label14: TLabel
          Left = 73
          Top = 23
          Width = 10
          Height = 13
          Caption = 'Y:'
        end
        object Label1: TLabel
          Left = 143
          Top = 23
          Width = 32
          Height = 13
          Caption = 'mZone'
        end
        object ComboBox_zone: TComboBox
          Left = 181
          Top = 20
          Width = 192
          Height = 21
          Style = csDropDownList
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnChange = ComboBox_zoneChange
          OnExit = ComboBox_zoneExit
        end
      end
      object Bt_find_self: TBitBtn
        Left = 356
        Top = 6
        Width = 24
        Height = 21
        Hint = 'mFindOnMap'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000FC02FC004985
          D40084C6F400C7D1D100FAEFC700B08D8800B5B8BC00FCFCDA0056595B005CB6
          F10072758400868F9200F5D7B000B6C8CE00D3B39E008E787C000BAA60000000
          0000BA58A6000000000091A58A6000000000D91AF8A6000000000291A58BDDDD
          000000291AF8AA88ABD0000291BF5ECEA8FD0000265C47774F8B000006C44777
          748A00000E4C7777775800000E44477777EA00000C44477777EA00000E44C477
          745B000003C7744C4CFD000000E47444C5D000000006EEEEE300}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = Bt_find_selfClick
      end
      object PageControl: TPageControl
        Left = 1
        Top = 89
        Width = 379
        Height = 329
        ActivePage = TS_main
        TabOrder = 3
        OnChange = PageControlChange
        object TS_main: TTabSheet
          Caption = 'mSendProperties'
          object Label13: TLabel
            Left = 3
            Top = 5
            Width = 52
            Height = 13
            Caption = 'mToObject'
          end
          object CB_obj: TComboBox
            Left = 66
            Top = 2
            Width = 272
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 0
            OnExit = CB_objExit
            OnKeyPress = CB_objKeyPress
          end
          object PageControlProperty: TPageControl
            Left = 0
            Top = 41
            Width = 371
            Height = 260
            ActivePage = TSP_props
            Align = alBottom
            TabOrder = 2
            object TSP_props: TTabSheet
              Caption = 'mProperties'
              object Label4: TLabel
                Left = 3
                Top = 3
                Width = 38
                Height = 13
                Caption = 'mAction'
              end
              object ScrollBox: TScrollBox
                Left = 3
                Top = 27
                Width = 357
                Height = 202
                HorzScrollBar.Tracking = True
                VertScrollBar.Tracking = True
                TabOrder = 1
                OnResize = ScrollBoxResize
              end
              object CB_ParamTransmitOpt: TComboBox
                Left = 72
                Top = 0
                Width = 288
                Height = 21
                Style = csDropDownList
                ItemIndex = 0
                TabOrder = 0
                Text = 'mSendPrpValues'
                OnChange = CB_ParamTransmitOptChange
                OnExit = CB_ParamTransmitOptExit
                Items.Strings = (
                  'mSendPrpValues'
                  'mSubtractPrpValues'
                  'mAddPrpValues')
              end
            end
            object TSP_State: TTabSheet
              Caption = 'mPrpObjState'
              ImageIndex = 1
              object Image1: TImage
                Left = 8
                Top = 38
                Width = 11
                Height = 10
                Transparent = True
                OnClick = Image1Click
              end
              object Label6: TLabel
                Left = 24
                Top = 238
                Width = 83
                Height = 13
                Caption = 'mImageStateHint'
              end
              object CB_states: TComboBox
                Left = 25
                Top = 32
                Width = 325
                Height = 21
                Style = csDropDownList
                TabOrder = 1
                OnChange = CB_statesChange
                OnEnter = CB_statesEnter
              end
              object CheckBox_newState: TCheckBox
                Left = 8
                Top = 7
                Width = 359
                Height = 17
                Caption = 'mRandomStateList'
                TabOrder = 0
                OnClick = CheckBox_newStateClick
              end
              object ListBox: TListBox
                Left = 25
                Top = 56
                Width = 301
                Height = 176
                ItemHeight = 13
                ParentShowHint = False
                ShowHint = False
                TabOrder = 2
                OnClick = ListBoxClick
                OnExit = ListBoxExit
              end
              object Bt_st_add: TBitBtn
                Left = 326
                Top = 55
                Width = 24
                Height = 24
                Hint = 'mAdd'
                Glyph.Data = {
                  F6000000424DF600000000000000760000002800000010000000100000000100
                  0400000000008000000000000000000000001000000000000000000000000000
                  8000008000000080800080000000800080008080000080808000C0C0C0000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                  DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD000DDDDDDDDDDDD02227DD
                  DDDDDDDDD02227DDDDDDDDD000222777DDDDDD02222222227DDDDD0222222222
                  7DDDDD02222222227DDDDDD000222777DDDDDDDDD02227DDDDDDDDDDD02227DD
                  DDDDDDDDDD777DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
                ParentShowHint = False
                ShowHint = True
                TabOrder = 3
                OnClick = Bt_st_addClick
              end
              object Bt_st_del: TBitBtn
                Left = 326
                Top = 79
                Width = 24
                Height = 24
                Hint = 'mDoDelete'
                Glyph.Data = {
                  F6000000424DF600000000000000760000002800000010000000100000000100
                  0400000000008000000000000000000000001000000000000000000000000000
                  8000008000000080800080000000800080008080000080808000C0C0C0000000
                  FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
                  DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                  DDDDDDDDDDDDDDDDDDDDDDD777777777DDDDDD09999999997DDDDD0999999999
                  7DDDDD09999999997DDDDDD000000000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
                  DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
                ParentShowHint = False
                ShowHint = True
                TabOrder = 4
                OnClick = Bt_st_delClick
              end
              object Panel1: TPanel
                Left = 326
                Top = 103
                Width = 24
                Height = 129
                TabOrder = 5
              end
            end
            object TSP_Actions: TTabSheet
              Caption = 'mRedefineActions'
              ImageIndex = 2
              object ScrollBoxActions: TScrollBox
                Left = 5
                Top = 3
                Width = 355
                Height = 227
                HorzScrollBar.Tracking = True
                VertScrollBar.Tracking = True
                TabOrder = 0
                OnResize = ScrollBoxActionsResize
              end
            end
            object TabSheet1: TTabSheet
              Caption = 'mTeleportTo'
              ImageIndex = 3
              object CB_objPos: TComboBox
                Left = 173
                Top = 67
                Width = 161
                Height = 21
                CharCase = ecUpperCase
                TabOrder = 2
                OnExit = CB_objPosExit
                OnKeyPress = CB_objKeyPress
              end
              object RB_ChPosObjCoord: TRadioButton
                Left = 12
                Top = 69
                Width = 155
                Height = 17
                Caption = 'mTeleportToDirector'
                TabOrder = 1
                OnClick = RB_ChPosNoChangeClick
              end
              object RB_ChPosTileCoord: TRadioButton
                Left = 12
                Top = 107
                Width = 218
                Height = 17
                Caption = 'mTeleportToTileNo'
                TabOrder = 4
                OnClick = RB_ChPosNoChangeClick
              end
              object RB_ChPosNoChange: TRadioButton
                Left = 12
                Top = 30
                Width = 141
                Height = 17
                Caption = 'mTeleportNone'
                Checked = True
                TabOrder = 0
                TabStop = True
                OnClick = RB_ChPosNoChangeClick
              end
              object Bt_find_node: TBitBtn
                Left = 332
                Top = 67
                Width = 24
                Height = 21
                Hint = 'mFindOnMap'
                Glyph.Data = {
                  F6000000424DF600000000000000760000002800000010000000100000000100
                  0400000000008000000000000000000000001000000000000000FC02FC004985
                  D40084C6F400C7D1D100FAEFC700B08D8800B5B8BC00FCFCDA0056595B005CB6
                  F10072758400868F9200F5D7B000B6C8CE00D3B39E008E787C000BAA60000000
                  0000BA58A6000000000091A58A6000000000D91AF8A6000000000291A58BDDDD
                  000000291AF8AA88ABD0000291BF5ECEA8FD0000265C47774F8B000006C44777
                  748A00000E4C7777775800000E44477777EA00000C44477777EA00000E44C477
                  745B000003C7744C4CFD000000E47444C5D000000006EEEEE300}
                ParentShowHint = False
                ShowHint = True
                TabOrder = 3
                OnClick = Bt_find_nodeClick
              end
            end
          end
          object Bt_find_obj: TBitBtn
            Left = 338
            Top = 2
            Width = 24
            Height = 21
            Hint = 'mFindOnMap'
            Glyph.Data = {
              F6000000424DF600000000000000760000002800000010000000100000000100
              0400000000008000000000000000000000001000000000000000FC02FC004985
              D40084C6F400C7D1D100FAEFC700B08D8800B5B8BC00FCFCDA0056595B005CB6
              F10072758400868F9200F5D7B000B6C8CE00D3B39E008E787C000BAA60000000
              0000BA58A6000000000091A58A6000000000D91AF8A6000000000291A58BDDDD
              000000291AF8AA88ABD0000291BF5ECEA8FD0000265C47774F8B000006C44777
              748A00000E4C7777775800000E44477777EA00000C44477777EA00000E44C477
              745B000003C7744C4CFD000000E47444C5D000000006EEEEE300}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = Bt_find_objClick
          end
        end
        object TS_map: TTabSheet
          Caption = 'mEditTileMap'
          ImageIndex = 1
          DesignSize = (
            371
            301)
          object Bevel: TBevel
            Left = 161
            Top = 23
            Width = 62
            Height = 58
            Hint = 'mAligment'
            Anchors = [akLeft, akBottom]
            ParentShowHint = False
            Shape = bsFrame
            ShowHint = True
            ExplicitTop = 33
          end
          object Label7: TLabel
            Left = 68
            Top = 111
            Width = 37
            Height = 13
            Caption = 'mCount'
          end
          object Label8: TLabel
            Left = 68
            Top = 139
            Width = 36
            Height = 13
            Caption = 'mIndex'
          end
          object Label9: TLabel
            Left = 155
            Top = 2
            Width = 50
            Height = 13
            Caption = 'mDirection'
          end
          object RBDown: TRadioButton
            Left = 185
            Top = 72
            Width = 13
            Height = 17
            Hint = 'mDirection'
            Anchors = [akLeft, akBottom]
            Checked = True
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            TabStop = True
            OnClick = RBLeftClick
          end
          object RBLeft: TRadioButton
            Left = 155
            Top = 43
            Width = 12
            Height = 17
            Hint = 'mDirection'
            Anchors = [akLeft, akBottom]
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = RBLeftClick
          end
          object RBUp: TRadioButton
            Left = 185
            Top = 17
            Width = 13
            Height = 15
            Hint = 'mDirection'
            Anchors = [akLeft, akBottom]
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = RBLeftClick
          end
          object RBRight: TRadioButton
            Left = 214
            Top = 43
            Width = 14
            Height = 17
            Hint = 'mDirection'
            Anchors = [akLeft, akBottom]
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = RBLeftClick
          end
        end
      end
      object Edit_name: TEdit
        Left = 49
        Top = 6
        Width = 307
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 64
        TabOrder = 0
        OnChange = Edit_nameChange
        OnEnter = Edit_nameEnter
        OnExit = Edit_nameExit
        OnKeyPress = Edit_nameKeyPress
      end
    end
    object TS_Memo: TTabSheet
      Caption = 'mMemo'
      ImageIndex = 2
      object Memo: TMemo
        Left = 3
        Top = 3
        Width = 376
        Height = 415
        MaxLength = 200
        ScrollBars = ssVertical
        TabOrder = 0
        WordWrap = False
        OnEnter = MemoEnter
        OnExit = MemoExit
      end
    end
  end
  object PopupMenu: TPopupMenu
    Left = 248
    object N1: TMenuItem
      Caption = 'mCreateNewMiniscript'
      ShortCut = 45
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = 'mEditMiniscript'
      ShortCut = 16453
      OnClick = N2Click
    end
  end
end
